using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Firestore;
using Firebase.Extensions;
using System;

public class BtnPrefabScript : MonoBehaviour
{ 
    public Button buttonPrefab;

    public static string[] imageUrlsToPhotosArray;
    public static string[] threeDmodelsArray;
    public static string[] ModeldocumentId;

    GameObject cube, sphere, capsule;
    
    List<string> clickedObjectIdArray = new List<string>();
    public static string[] threeDModeldocumentId;

    List<string> modelDocumentIdList = new List<string>();

    private void Start()
    {
        cube = GameObject.Find("cube");
        sphere = GameObject.Find("sphere");
        capsule = GameObject.Find("capsule");
    }


    public void OpenVerticalScrollView()
    {
        CategoryCilckable.verticalScrollView.SetActive(true);
        CategoryCilckable.objectFinder.SetActive(true);
        CategoryCilckable.categoryFinder.SetActive(false);
        cube.SetActive(false);
    }


    public void buttonClicked()
    {

        FetchData();
        VerticalButtonCreator.buttonCreate(imageUrlsToPhotosArray, ModeldocumentId);
 
        
    }

    private void FetchData()
    {
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;

        Query allCitiesQuery = db.CollectionGroup("renders");

        allCitiesQuery.GetSnapshotAsync().ContinueWithOnMainThread(task =>
        {
            QuerySnapshot allCitiesQuerySnapshot = task.Result;
            ProcessCities(allCitiesQuerySnapshot.Documents);
        });
    }

    private void ProcessCities(IEnumerable<DocumentSnapshot> documents)
    {
       

        foreach (DocumentSnapshot documentSnapshot in documents)
        {
            if (documentSnapshot.Id == buttonPrefab.name)
            {
                // ilk koleksiyon(renders) içindeki koleksiyonları veriyor teker teker (render1, render2, render3)
                Dictionary<string, object> city = documentSnapshot.ToDictionary();

                foreach (KeyValuePair<string, object> pair in city)
                {

                    Process3DModels(documentSnapshot);
                }
            }
        }
        
    }

    private void Process3DModels(DocumentSnapshot citySnapshot)
    {
        CollectionReference modelsCollectionRef = citySnapshot.Reference.Collection("3dModels");

        modelsCollectionRef.GetSnapshotAsync().ContinueWithOnMainThread(modelsTask =>
        {
            List<string> imageUrlsList2 = new List<string>();
            List<string> threeDmodelsUrl = new List<string>();
            QuerySnapshot modelsSnapshot = modelsTask.Result;

            ModelHelper.ProcessModels(modelsSnapshot, imageUrlsList2, modelDocumentIdList, clickedObjectIdArray, threeDmodelsUrl);

            ModeldocumentId = modelDocumentIdList.ToArray();
            imageUrlsToPhotosArray = imageUrlsList2.ToArray();
            threeDmodelsArray = threeDmodelsUrl.ToArray();
            threeDModeldocumentId = clickedObjectIdArray.ToArray();

        });
    }

}

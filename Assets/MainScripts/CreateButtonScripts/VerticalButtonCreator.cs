using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Networking;

public class VerticalButtonCreator : MonoBehaviour
{
    // Button prefab�, bu prefab kullan�larak yeni d��meler olu�turulacak
    public GameObject buttonPrefabeski;

    public static VerticalButtonCreator buttonCreator; // S�n�f ad�n� VerticalButtonCreator olarak de�i�tirin
    public Transform parentTransform;
    public Sprite backgroundImage;
    public float cornerRadius = 0.1f;

    void Start()
    {
        // Etiketle e�le�en nesneyi bul
        buttonCreator = GetComponent<VerticalButtonCreator>();
        // Dikey (Vertical) ScrollRect i�inde �al��t���m�z� varsayarsak, ScrollRect'in content objesini alal�m
        parentTransform = GetComponent<ScrollRect>().content;
        // Daha sonra CreateButton fonksiyonunu �a��rarak buttonlar� olu�turabiliriz
        
    }

    public static void buttonCreate(string[] imageUrls, string[] btnId)
    {
        for (int i = 0; i < imageUrls.Length; i++) // imageUrls dizisinin boyutuna g�re d�ng�y� ayarlay�n
        {
            buttonCreator.CreateButton(imageUrls[i], btnId[i]);
            
        }
    }

    // Button olu�turmak i�in �a��r�lacak fonksiyon
    public void CreateButton(string imageUrl, string btnId)
    {
        GameObject newButton = Instantiate(buttonPrefabeski, parentTransform);
        Button buttonComponent = newButton.GetComponent<Button>();
        Image imageComponent = newButton.GetComponent<Image>();
        buttonComponent.name = btnId;
        imageComponent.sprite = backgroundImage;
        imageComponent.type = Image.Type.Sliced;
        imageComponent.fillMethod = Image.FillMethod.Vertical; // FillMethod de�erini Vertical olarak de�i�tirin
        imageComponent.fillOrigin = (int)Image.OriginVertical.Top; // FillOrigin de�erini Top olarak de�i�tirin
        imageComponent.fillAmount = cornerRadius;
        Text textComponent = newButton.GetComponentInChildren<Text>();
        textComponent.text = btnId;
        StartCoroutine(DownloadImageExtensions.DownloadImage(imageUrl, imageComponent));
    }


}

using Firebase.Extensions;
using Firebase.Firestore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Create3DModel : MonoBehaviour
{

   
    Transform targetTransform;
    Transform placedObjectTransform;
    
    public GameObject text;
    public static Text textComponent;
    //public ObjectPlacement objectPlacement;
    public GameObject deneme;

    public GameObject placedObject;

    string assetUrl;
    GameObject assetObj;

    public static int childCount;

    private void Start()
    {
        text = GameObject.FindGameObjectWithTag("Text");
        textComponent = text.GetComponent<Text>();
        textComponent.text = "";

        placedObject = GameObject.FindGameObjectWithTag("PlacedObjects");

    }

    int GetChildCount()
    {
        if (placedObject != null)
        {
            // PlacedObjects nesnesinin çocuk sayısını döndür
            return placedObject.transform.childCount;
        }
        else
        {
            return 0;
        }
    }

    private void Update()
    {
        childCount = GetChildCount();
        targetTransform = deneme.transform;
    }

   
    public void btnClicked()
    {
        Download3dModels();
 
        CategoryCilckable.objectFinder.SetActive(true);
        CategoryCilckable.categoryFinder.SetActive(false);

    }

    public void Download3dModels()
    {
        Debug.Log(BtnPrefabScript.threeDmodelsArray.Length);
        for (int i = 0; i < BtnPrefabScript.threeDModeldocumentId.Length; i++)
        {

            if (gameObject.name == BtnPrefabScript.threeDModeldocumentId[i])
            {

                assetUrl = BtnPrefabScript.threeDmodelsArray[i];
                Debug.Log("asset url = " + assetUrl);
                StartCoroutine(DownloadAssetBundle(assetUrl));

            }
        }

    }

    IEnumerator DownloadAssetBundle(string url)
    {
        textComponent.text = "a";
        using (UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(url))
        {
            textComponent.text = "b";
            yield return request.SendWebRequest();

            IfElseExtensions.NetworkErrorsCheck(request);
           
            textComponent.text = "d";
            AssetBundle assetBundle = DownloadHandlerAssetBundle.GetContent(request);

            textComponent.text = assetBundle.name;
            string[] assetPaths = assetBundle.GetAllAssetNames();
           
            assetObj = assetBundle.LoadAsset<GameObject>(assetPaths[0]);
            assetObj.name = url;

            ForObjFalse();
            ForObjTrue();

            ObjectPlacement.bringObject(assetObj);
            Instantiate(assetObj, placedObject.transform);

            assetBundle.Unload(false);

        }
    }
    private void ForObjTrue()
    {
        GameObject[] okVCanBtn= {CategoryCilckable.parentObject, CategoryCilckable.cancelBtn, CategoryCilckable.okeyBtn};
        TrueOrFalse.MakeItVisible(okVCanBtn);
    }

    private void ForObjFalse()
    {
        GameObject[] bckVScrllVGrnPln = { CategoryCilckable.backButton, CategoryCilckable.verticalScrollView, CategoryCilckable.groundPlane };
        TrueOrFalse.MakeItInVisible(bckVScrllVGrnPln);
    }

    public void CancelBtnAction()
    {
    
        GameObject[] bckVScrllActn = { CategoryCilckable.backButton, CategoryCilckable.verticalScrollView};
        TrueOrFalse.MakeItVisible(bckVScrllActn);
        GameObject[] CanVOkyBtnfls = {CategoryCilckable.cancelBtn, CategoryCilckable.okeyBtn };
        TrueOrFalse.MakeItInVisible(CanVOkyBtnfls);
    }

    public void OkeyBtnAction()
    {

        GameObject[] bckVScrlltrue = { CategoryCilckable.backButton, CategoryCilckable.verticalScrollView};
        TrueOrFalse.MakeItVisible(bckVScrlltrue);
        GameObject[] FalseObjects = { CategoryCilckable.cancelBtn, CategoryCilckable.okeyBtn };
        TrueOrFalse.MakeItInVisible(FalseObjects);

    }
}

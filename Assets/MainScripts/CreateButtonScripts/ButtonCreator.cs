using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Networking;

public class ButtonCreator : MonoBehaviour
{
    // Button prefab�, bu prefab kullan�larak yeni d��meler olu�turulacak
    public GameObject buttonPrefabeski;

    public static ButtonCreator buttonCreator;
    public Transform parentTransform;
    public Sprite backgroundImage;
    public float cornerRadius = 0.15f;

    void Start()
    {
        buttonCreator = GetComponent<ButtonCreator>(); 
    }
   
    public static void buttonCreate(string[] imageUrls, string[] btnId)
    {
        
        for (int i = 0; i < FirestoreManager.btnCount; i++)
        {
            buttonCreator.CreateButton(imageUrls[i], btnId[i]);           
        }
        
    }

    #region Button olu�turmak i�in �a��r�lacak fonksiyon
    public void CreateButton(string imageUrl, string btnId)
    {
        GameObject newButton = Instantiate(buttonPrefabeski, parentTransform);
        Button buttonComponent = newButton.GetComponent<Button>();
        Image imageComponent = newButton.GetComponent<Image>();
        buttonComponent.name = btnId;
        imageComponent.sprite = backgroundImage;
        imageComponent.type = Image.Type.Sliced;
        imageComponent.fillMethod = Image.FillMethod.Horizontal;
        imageComponent.fillOrigin = (int)Image.OriginHorizontal.Left;
        imageComponent.fillAmount = cornerRadius;
        Text textComponent = newButton.GetComponentInChildren<Text>();
        textComponent.text = btnId;
        StartCoroutine(DownloadImageExtensions.DownloadImage(imageUrl,imageComponent));
    }
    #endregion
}





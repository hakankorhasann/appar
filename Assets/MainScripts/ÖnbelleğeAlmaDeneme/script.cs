using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script : MonoBehaviour
{
    public string BundleURL;
    public string AssetName;
    IEnumerator Start()
    {
        Debug.Log("startaGirdi");
        // Download the file from the URL. It will not be saved in the Cache
        using (WWW www = new WWW(BundleURL))
        {
            Debug.Log(www);
            yield return www;
            if (www.error != null)
                throw new Exception("WWW download had an error:" + www.error);
            AssetBundle bundle = www.assetBundle;
            Debug.Log("Assetbundle Name= "+bundle.name);
            //if (AssetName == "")
                Instantiate(bundle.mainAsset);
            
            //else
             //   Instantiate(bundle.LoadAsset(AssetName));
            // Unload the AssetBundles compressed contents to conserve memory
            bundle.Unload(false);
            Debug.Log("indirildi");
        } // memory is freed from the web stream (www.Dispose() gets called implicitly)
    }
}

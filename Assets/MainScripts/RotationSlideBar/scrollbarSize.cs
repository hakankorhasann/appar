using Firebase.Storage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scrollbarSize : MonoBehaviour
{
    public Scrollbar scrollbar;
    public static float angleValue;
    
    private void Start()
    {
        scrollbar.onValueChanged.AddListener(OnScrollbarValueChanged);
               
    }

    public void OnScrollbarValueChanged(float value)
    {
        
        float mappedValue = Mathf.Lerp(-180f, 180f, value);
        angleValue = mappedValue;
        // De�erle yap�lmas� gereken i�lemleri burada ger�ekle�tirin
        // mappedValue, Scrollbar'�n de�eri -180 ile +180 aras�nda lineer bir �ekilde haritalanm�� hali olarak kullan�labilir
        //Debug.Log("Scrollbar de�eri: " + mappedValue);

    }
}

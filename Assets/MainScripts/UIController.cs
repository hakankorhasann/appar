using UnityEngine;
using Vuforia;

public class UIController : MonoBehaviour
{

    public Animator anim; //= GetComponent<Animator>();
    

    // Update is called once per frame
    void Start()
    {
        // cpb.DuplicateStage = false;
        

    }
    void Update()
    {

        
        // anim.GetCurrentAnimatorStateInfo(0) animasyonun s�resini al�r
        /*animStateInfo bir AnimatorStateInfo nesnesidir ve Animator nesnesindeki oynat�lan bir animasyonun durum bilgilerini temsil eder.
         * normalizedTime �zelli�i, animasyonun �u anki zaman�n�, animasyonun tam uzunlu�una oranlanarak ifade eder.
         * �rne�in, bir animasyon klipinin uzunlu�u 2 saniye ise, animasyonun yar�s�ndayken normalizedTime de�eri 0.5 olacakt�r.
           Bu nedenle, if (animStateInfo.normalizedTime >= 1.0f) ko�ulu, animasyonun tamamlan�p tamamlanmad���n� kontrol eder.
           E�er normalizedTime de�eri 1.0 veya daha b�y�kse, animasyon tamamlanm�� demektir ve bu durumda ko�ul true d�nd�r�r.
           Bu ko�ul do�ru oldu�unda, animasyonun tamamlanmas�ndan sonra yap�lacak i�lemleri i�eren kod blo�u �al��t�r�labilir.*/
        if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.5f)       
        {
            GameObject[] objectsToDisable = GameObject.FindGameObjectsWithTag("planeScanImg");
     
            foreach (GameObject obj in objectsToDisable)
            {
                obj.SetActive(false);
              
                
            }
        }
    }
}

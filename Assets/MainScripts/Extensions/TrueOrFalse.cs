using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrueOrFalse 
{
    public static void MakeItVisible(GameObject[] btns)
    {
        foreach (var btn in btns)
        {
            btn.SetActive(true);
        }
    }

    public static void MakeItInVisible(GameObject[] btns)
    {
        foreach (var btn in btns)
        {
            btn.SetActive(false);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class IfElseExtensions 
{
    public static void IfElseObjectCheck(GameObject obj)
    {
        if (obj == null)
        {
            Debug.Log("IfElseObjectCheck null geldi: " + obj);
        }
        else
        {
            Debug.Log("IfElseObjectCheck Ad�: " + obj.name);
            //obj.SetActive(false);
        }
    }

    public static void NetworkErrorsCheck(UnityWebRequest request)
    {
        if (request.isNetworkError)
        {
            Debug.Log("network error" + request.error);
        }
        else if (request.isHttpError)
        {
            Debug.Log("http error " + request.error);
        }
        else
        {
            Debug.Log("Succes Controll " + request.url);
        }
    }
}

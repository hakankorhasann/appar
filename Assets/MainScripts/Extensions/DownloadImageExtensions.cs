using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public static class DownloadImageExtensions 
{
    public static IEnumerator DownloadImage(string url, Image targetImage)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            // Send Request and wait
            yield return www.SendWebRequest();

            IfElseExtensions.NetworkErrorsCheck(www);
            var texture2d = DownloadHandlerTexture.GetContent(www);
            targetImage.sprite = Sprite.Create(texture2d, new Rect(0, 0, texture2d.width, texture2d.height), Vector2.zero);
        }
    }
}

using System.Collections;
using UnityEngine;
using System;
using UnityEngine.Networking;

public static class DownloadAssetBundleExtension 
{
    public static IEnumerator DownloadAssetBundle(string url, Transform targetTransformHorizontal)
    {
        
        using (UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(url))
        {
            
            yield return request.SendWebRequest();

            if (request.result == UnityWebRequest.Result.Success)
            {
                AssetBundle assetBundle = DownloadHandlerAssetBundle.GetContent(request);

                string[] assetPaths = assetBundle.GetAllAssetNames();
                foreach (string assetPath in assetPaths)
                {
                    
                    GameObject asset = assetBundle.LoadAsset<GameObject>(assetPath);
                    
                    if (asset != null && targetTransformHorizontal != null)
                    {

                        Instantiate(asset, targetTransformHorizontal.position, targetTransformHorizontal.rotation);
                    }
                   
                }
                assetBundle.Unload(false);
            }
            else
            {
                Debug.LogError("Error downloading asset bundle: " + request.error);
            }
        }
    }

    private static void Instantiate(GameObject asset, Vector3 position, Quaternion rotation)
    {
        throw new NotImplementedException();
    }
}

using UnityEngine;
using UnityEngine.UI;
public class CreateButtonHandler 
{
    public static void CreateButton(GameObject buttonPrefab, Transform parentTransform, Sprite backgroundImage, float cornerRadius, string imageUrl, string btnId)
    {
        GameObject newButton = Object.Instantiate(buttonPrefab, parentTransform);
        Button buttonComponent = newButton.GetComponent<Button>();
        Image imageComponent = newButton.GetComponent<Image>();
        buttonComponent.name = btnId;
        imageComponent.sprite = backgroundImage;
        imageComponent.type = Image.Type.Sliced;
        imageComponent.fillMethod = Image.FillMethod.Vertical;
        imageComponent.fillOrigin = (int)Image.OriginVertical.Top;
        imageComponent.fillAmount = cornerRadius;
        Text textComponent = newButton.GetComponentInChildren<Text>();
        textComponent.text = btnId;
        MonoBehaviour monoBehaviour = newButton.GetComponent<MonoBehaviour>();
        monoBehaviour.StartCoroutine(DownloadImageExtensions.DownloadImage(imageUrl, imageComponent));
    }
}

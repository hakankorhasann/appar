using Firebase.Firestore;
using System.Collections.Generic;
public static class ModelHelper
{
    public static void ProcessModels(IEnumerable<DocumentSnapshot> modelsSnapshot, List<string> imageUrlsList, List<string> modelDocumentIdList, List<string> clickedObjectIdArray, List<string> threeDmodelsUrl)
    {

        foreach (DocumentSnapshot modelSnapshot in modelsSnapshot)
        {
            Dictionary<string, object> city = modelSnapshot.ToDictionary();

            if (modelSnapshot.ContainsField("modelLink"))
            {
                string modelLink = modelSnapshot.GetValue<string>("modelLink");
                imageUrlsList.Add(modelLink);
                modelDocumentIdList.Add(modelSnapshot.Id);
            }

            if (modelSnapshot.ContainsField("3dObjects"))
            {
                string threeDModelLink = modelSnapshot.GetValue<string>("3dObjects");
                clickedObjectIdArray.Add(modelSnapshot.Id);
                threeDmodelsUrl.Add(threeDModelLink);
            }
        }
    }
}

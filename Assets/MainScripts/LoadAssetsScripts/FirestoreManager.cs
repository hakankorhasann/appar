using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Firestore;
using Firebase.Extensions;


public class FirestoreManager : MonoBehaviour
{

    public static int btnCount, btnCount2;
    public static int copyCount;
    public static string[] imageUrlsArray;
    public static string[] imageUrlsToPhotosArray;
    public static string[] documentId;

    public static FirestoreManager Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }


    private void Start()
    {
        FetchData();
    }

    public void FetchData()
    {
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;
        Query allCitiesQuery = db.CollectionGroup("renders");

        allCitiesQuery.GetSnapshotAsync().ContinueWithOnMainThread(task =>
        {
            QuerySnapshot allCitiesQuerySnapshot = task.Result;
            ProcessCities(allCitiesQuerySnapshot.Documents);
        });
    }

    public void ProcessCities(IEnumerable<DocumentSnapshot> citySnapshots)
    {
        List<string> imageUrlsList = new List<string>();
        List<string> documentIdList = new List<string>();

        foreach (DocumentSnapshot citySnapshot in citySnapshots)
        {
            Dictionary<string, object> cityData = citySnapshot.ToDictionary();

            foreach (KeyValuePair<string, object> pair in cityData)
            {
                string imageUrl = pair.Value.ToString();
                imageUrlsList.Add(imageUrl);
                btnCount += 1;
                copyCount += 1;
                documentIdList.Add(citySnapshot.Id);
                documentId = documentIdList.ToArray();

                // Process 3D Models
                Process3DModels(citySnapshot);
            }
        }

        string[] imageUrlsArray = imageUrlsList.ToArray();
        string[] documentIdArray = documentIdList.ToArray();

        ButtonCreator.buttonCreate(imageUrlsArray, documentIdArray);
    }

    public void Process3DModels(DocumentSnapshot citySnapshot)
    {
        CollectionReference modelsCollectionRef = citySnapshot.Reference.Collection("3dModels");
        modelsCollectionRef.GetSnapshotAsync().ContinueWithOnMainThread(modelsTask =>
        {
            QuerySnapshot modelsSnapshot = modelsTask.Result;
            List<string> imageUrlsList2 = new List<string>();
            foreach (DocumentSnapshot modelSnapshot in modelsSnapshot.Documents)
            {
                Dictionary<string, object> modelData = modelSnapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in modelData)
                {
                    string imageUrl2 = pair.Value.ToString();
                    imageUrlsList2.Add(imageUrl2);
                    btnCount2 += 1;
                }
            }

            imageUrlsToPhotosArray = imageUrlsList2.ToArray();
            // Do something with imageUrlsToPhotosArray if needed
        });
    }

   
}

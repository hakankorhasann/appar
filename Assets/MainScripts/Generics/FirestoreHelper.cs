using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Firestore; // Make sure to import the appropriate namespace

public class FirestoreHelper 
{
    private FirebaseFirestore db;

    public FirestoreHelper()
    {
        db = FirebaseFirestore.DefaultInstance;
    }

    public async Task<List<string>> GetImageUrlsAsync(string collectionPath, Action<string[], string[]> onComplete)
    {
        List<string> imageUrlsList = new List<string>();
        List<string> documentIdList = new List<string>();

        QuerySnapshot allCitiesQuerySnapshot = await db.CollectionGroup(collectionPath).GetSnapshotAsync();

        foreach (DocumentSnapshot documentSnapshot in allCitiesQuerySnapshot.Documents)
        {
            Dictionary<string, object> city = documentSnapshot.ToDictionary();

            foreach (KeyValuePair<string, object> pair in city)
            {
                string imageUrl = pair.Value.ToString();
                imageUrlsList.Add(imageUrl);
                documentIdList.Add(documentSnapshot.Id);

                CollectionReference modelsCollectionRef = documentSnapshot.Reference.Collection("3dModels");
                QuerySnapshot modelsSnapshot = await modelsCollectionRef.GetSnapshotAsync();
                List<string> imageUrlsList2 = new List<string>();

                foreach (DocumentSnapshot modelSnapshot in modelsSnapshot.Documents)
                {
                    Dictionary<string, object> model = modelSnapshot.ToDictionary();
                    foreach (KeyValuePair<string, object> modelPair in model)
                    {
                        string imageUrl2 = modelPair.Value.ToString();
                        imageUrlsList2.Add(imageUrl2);
                    }
                }

                onComplete?.Invoke(imageUrlsList.ToArray(), documentIdList.ToArray());
            }
        }

        return imageUrlsList;
    }
}

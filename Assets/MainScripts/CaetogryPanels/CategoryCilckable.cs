using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class CategoryCilckable : MonoBehaviour
{

    public static GameObject categoryFinder, objectFinder;
    //kategoriler
    public GameObject cubePanel1, spherePanel1, capsulePanel1; 

    //tak�mlar�n i�eri�indeki objeleri g�r�nt�ler
    public static GameObject verticalScrollView;

    //kullan�c�n�n kendine g�re rotasyon ayarlamas�
    static GameObject rotationSliderBar;

    //back button tan�m�
    public static GameObject backButton;

    //t�klanan buttonun name ini tutmak i�in olu�turuldu
    static string btnName;

    public static GameObject planeFinder; 
    //wait time
    public float msBetweenShots = 100;
    float nextShotTime;
    
    //kategori objelerini a��p kapatmak i�in kullan�l�r
    public GameObject[] objectsToDisable;

    public static GameObject groundPlane;
    public static GameObject parentObject; //hiyerar�ide neyin alt�na getirece�ini belirler
    //Cancel and Okey Btn
    public static GameObject cancelBtn,okeyBtn;

    void Start()
    {
        categoryFinder = GameObject.FindGameObjectWithTag("categoryFinder");
        objectFinder = GameObject.FindGameObjectWithTag("objectFinder");
        objectFinder.SetActive(false);

        rotationSliderBar = GameObject.FindGameObjectWithTag("RotationSliderBar");

        IfElseExtensions.IfElseObjectCheck(rotationSliderBar);
        
        verticalScrollView = GameObject.FindGameObjectWithTag("VerticalScrollViewTag");
        IfElseExtensions.IfElseObjectCheck(verticalScrollView);

        objectsToDisable = GameObject.FindGameObjectsWithTag("Respawn");
        
        backButton = GameObject.FindGameObjectWithTag("BackButton");
        IfElseExtensions.IfElseObjectCheck(backButton);

        cancelBtn = GameObject.Find("CancelBtn");
        IfElseExtensions.IfElseObjectCheck(cancelBtn);

        okeyBtn = GameObject.Find("OkeyBtn");
        IfElseExtensions.IfElseObjectCheck(okeyBtn);

        groundPlane = GameObject.FindGameObjectWithTag("groundPlane");

        parentObject = GameObject.FindGameObjectWithTag("groundPlaneObject");
        parentObject.SetActive(false);

        planeFinder = GameObject.FindGameObjectWithTag("categoryFinder");
        //uygulamada kullan�lan kategoriler (mobilya, makina, mimarl�k vs.)
        //bunlar hiyerar�ide a��k olarak bulunuyor o y�zden hepsini g�r�nmez k�l�yoruz
        PanelsFalse();

        //�r�n yerle�tirme ve silme de kullan�lacak btnlar� false yap�yoruz..
        OkAndCancelBtnsFalse();
    }

    void Update()
    {
        
        RotationAdjust();

        #region T�klanan kategoriyi a��p di�erlerini kapat�yoruz..
        if ((Input.GetTouch(0).phase == TouchPhase.Stationary) || (Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(0).deltaPosition.magnitude < 1.2f))
        {
          
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                
                rotationSliderBar.SetActive(true);
                if (Time.time > nextShotTime && msBetweenShots <= 100)
                {
                    nextShotTime = Time.time + msBetweenShots / 1000;
                    foreach (GameObject obj in objectsToDisable)
                    {
                        obj.SetActive(false);

                    }

                }

                hit.transform.gameObject.SetActive(true);
                btnName = hit.transform.gameObject.name;
                Debug.Log("button ismi " + btnName);
                backButton.SetActive(true);
                PanelsTrue();

            }
        }
        #endregion

    }

    //kategorilerin y�nlerini kullan�c� kendine g�re ayarlayabilsain diye bir d�nd�rme methodu
    public void RotationAdjust()
    {
        
        foreach (GameObject obj in objectsToDisable)
        {
            if (obj.activeSelf)
            {
                obj.transform.rotation = Quaternion.Euler(0f, scrollbarSize.angleValue, 0f);
                //break;
            }
        }
    }

    //3. ekrandan 2. ekrana D�n��..
    //vertical scrol view = t�klanan tak�m�n modellerini g�steren butonlar� bulundurur.
    //bu fonksiyon ise vsv a��k iken geri butonuna t�klan�nca yap�lacak olay� g�sterir:
    //t�klanan kategori butonu (btnName == obj.name) hari� di�er kategorilerin setactive leri false olur
    //ve b�ylelikle kullan�c� geri gidip ba�ka bir tak�m� se�ip tekrar dizmeye ba�layabilir.
    public void BackButtonForVerticalScroll()
    {

        if (verticalScrollView.activeSelf)
        {
            objectFinder.SetActive(false);
            categoryFinder.SetActive(true);
            parentObject.SetActive(false);
            groundPlane.SetActive(true);


            verticalScrollView.SetActive(false);
            foreach (GameObject obj in objectsToDisable)
            {
                if(btnName == obj.name)
                {
                    obj.SetActive(true);
                }
                
            }
        }
        
    }

    // Birinci ekran
    // 3 Kategorinin de a��k oldu�u ekran da Geri buttpnuna ihtiya� olmad��� i�in geri buttonunu kapat�yoruz..
    public void BackButtonClose()
    {
        if (cubePanel1 == true && spherePanel1 == true && capsulePanel1 == true)
        {
            backButton.SetActive(false);
        }
    }

    // 2. ekrandan 1. ekrana ge�i�..
    public void BackButtonClicked()
    {

        FirestoreManager.copyCount = 0;
        foreach (GameObject obj in objectsToDisable)
        {
            obj.SetActive(false);
            obj.SetActive(true);
            PanelsFalse();
        }

    }
    void PanelsFalse()
    {
        cubePanel1.SetActive(false);
        spherePanel1.SetActive(false);
        capsulePanel1.SetActive(false);


    }
    void PanelsTrue()
    {
        cubePanel1.SetActive(true);
        spherePanel1.SetActive(true);
        capsulePanel1.SetActive(true);
    }

    private void OkAndCancelBtnsFalse()
    {
        cancelBtn.gameObject.SetActive(false);
        okeyBtn.gameObject.SetActive(false);
    }
    public void AppQuit()
    {
        Application.Quit();
    }

    

}

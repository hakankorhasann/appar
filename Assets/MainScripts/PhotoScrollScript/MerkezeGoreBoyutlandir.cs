using UnityEngine;
using UnityEngine.UI;

public class MerkezeGoreBoyutlandir : MonoBehaviour
{
    //ScrollView bile�eninin oldu�u obje
    [SerializeField]
    RectTransform scrollParent;
    //Referans olarak kullanaca��m�z pozisyon
    Vector3 boyutlandirmaMerkezi;

    //ScrollView alan�n�n b�y�kl���
    float alanGenisligi;
    //Objelerin genel b�y�kl���
    float altObjeGenisligi;

    /*
    Boyutland�rma i�in animationCurve kullan�yoruz.
    Bu sayede istedi�imiz mesafe i�in istedi�imi boyutu elle
    ayarlayabiliyoruz.
    */
    [SerializeField]
    AnimationCurve boyutlandirmaOrani;

    //Kontrol
    [SerializeField]
    BoyutlandirmaYonu yon = BoyutlandirmaYonu.yatay;

    //Horizontal veya Vertical kullan�labilece�inden ikisini de kontrol etmek i�in bu tipi kullan�yoruz.
    HorizontalOrVerticalLayoutGroup layoutGroup;

    void Start()
    {
        //Scrollview pozisyonu merkez noktas�nda oldu�undan 
        boyutlandirmaMerkezi = scrollParent.position;
        layoutGroup = GetComponent<HorizontalOrVerticalLayoutGroup>();
        GorunurAlanGenisligiHesapla();
        AltObjeBoyutuHesapla();
        BosluklariHesapla();
    }
    //ScrollView OnValueChange �zerinden atanmas� gereken fonksiyon. Bu sayede her kayd�rma sonucunda bu fonksiyon �a��r�lacak.
    public void ObjeleriBoyutlandir()
    {
        float yakinlikOrani;
        //her bir altobjeyi dolan�yoruz
        foreach (Transform altObje in transform)
        {
            //merkez aras�ndaki mesafeyi al�p, g�r�n�r alanda olabilece�i en uzak mesafeye oran�n� elde ediyoruz
            float mesafe = Vector3.Distance(boyutlandirmaMerkezi, altObje.position);
            mesafe = Mathf.Abs(mesafe);
            yakinlikOrani = mesafe / alanGenisligi;
            //animationcurve �zerinden belirledi�imiz orana g�re de�eri �ekmi� oluyoruz.
            //Animation curve de�erinin loop modunda olmamas�na dikkat edin. Derste bahsetmemi�tim ama �nemli bir konu
            altObje.localScale = Vector3.one * boyutlandirmaOrani.Evaluate(yakinlikOrani);
        }
    }

    //Layout group i�erisindeki ve altobjeler aras�ndaki mesafeyi ayarl�yoruz
    void BosluklariHesapla()
    {
        int yanBosluk = Mathf.FloorToInt((alanGenisligi - altObjeGenisligi) * .5f);
        if (yon == BoyutlandirmaYonu.yatay)
        {
            layoutGroup.padding.left = yanBosluk;
            layoutGroup.padding.right = yanBosluk;
        }
        else
        {
            layoutGroup.padding.top = yanBosluk/2;
            layoutGroup.padding.bottom = yanBosluk/2;
        }
        layoutGroup.spacing += altObjeGenisligi * .2f;
    }
    /*
    her eleman�n e�it boyutta oldu�unu kabul ederek
    ilk elemandan boyut de�erlerini al�yoruz.
    */
    void AltObjeBoyutuHesapla()
    {

        if (yon == BoyutlandirmaYonu.yatay)
        {
            altObjeGenisligi = transform.GetChild(0).GetComponent<RectTransform>().rect.width;
        }
        else
        {
            altObjeGenisligi = transform.GetChild(0).GetComponent<RectTransform>().rect.height;
        }
    }
    //Scroll i�erisinde objelerin g�r�n�r oldu�u alan�n boyutlar�n� al�yoruz.
    void GorunurAlanGenisligiHesapla()
    {
        if (yon == BoyutlandirmaYonu.yatay)
        {
            alanGenisligi = scrollParent.rect.width;
        }
        else
        {
            alanGenisligi = scrollParent.rect.height;
        }
    }
}
enum BoyutlandirmaYonu
{
    yatay,
    dikey
}
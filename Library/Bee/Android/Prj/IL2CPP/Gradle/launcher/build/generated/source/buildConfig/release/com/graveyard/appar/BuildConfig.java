/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.graveyard.appar;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.graveyard.appar";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 10153;
  public static final String VERSION_NAME = "10.15.3";
}

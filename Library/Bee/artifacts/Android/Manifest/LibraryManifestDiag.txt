Using template 'Assets/Plugins/Android/AndroidManifest.xml'

android.hardware.vulkan.version was added because:
	One of the graphics devices was Vulkan
android.permission.INTERNET was added because:
	UnityEngine.Networking was present in:
		Assembly-CSharp-FeaturesChecked.txt
	UnityEngine.Network was present in:
		Assembly-CSharp-FeaturesChecked.txt
	UnityEngine.WWW was present in:
		Assembly-CSharp-FeaturesChecked.txt
	UnityEngine.Networking.UnityWebRequest was present in:
		Assembly-CSharp-FeaturesChecked.txt
android.permission.CAMERA, android.hardware.camera (Auto Focus, Front) were added because:
	UnityEngine.WebCamTexture was present in:
		Vuforia.Unity.Engine-FeaturesChecked.txt
android.hardware.touchscreen.multitouch, android.hardware.touchscreen.multitouch.distinct were enabled because:
	UnityEngine.Input::get_touches was present in:
		CommonScripts-FeaturesChecked.txt
		Unity.RenderPipelines.Core.Runtime-FeaturesChecked.txt
	UnityEngine.Input::GetTouch was present in:
		Assembly-CSharp-FeaturesChecked.txt
		SamplesScripts-FeaturesChecked.txt
		Unity.VisualEffectGraph.Runtime-FeaturesChecked.txt
		UnityEngine.UI-FeaturesChecked.txt
		Vuforia.Unity.Engine-FeaturesChecked.txt
	UnityEngine.Input::get_touchCount was present in:
		SamplesScripts-FeaturesChecked.txt
		Unity.RenderPipelines.Core.Runtime-FeaturesChecked.txt
		Unity.VisualEffectGraph.Runtime-FeaturesChecked.txt
		UnityEngine.UI-FeaturesChecked.txt
		Vuforia.Unity.Engine-FeaturesChecked.txt

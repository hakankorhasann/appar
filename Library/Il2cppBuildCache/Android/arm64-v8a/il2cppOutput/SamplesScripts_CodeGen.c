﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DatasetLoader::TriggerAreaTargetLoading(DatasetUnit)
extern void DatasetLoader_TriggerAreaTargetLoading_mC8248944F4269647BEEDB93095FE1DA821E0E17B (void);
// 0x00000002 System.Void DatasetLoader::UnloadAreaTarget()
extern void DatasetLoader_UnloadAreaTarget_m866754A6E399BF99D6E0979BC41A821EC514DBA2 (void);
// 0x00000003 System.Void DatasetLoader::LoadAreaTarget(DatasetUnit)
extern void DatasetLoader_LoadAreaTarget_mBAAEC58BCDCA6118BC33F6F3FA7FB2B82B6E5FF9 (void);
// 0x00000004 UnityEngine.GameObject DatasetLoader::LoadDatasetModel(Vuforia.AreaTargetBehaviour,System.String)
extern void DatasetLoader_LoadDatasetModel_mAB210F3E9ABF3E25BD95A3D9F6E8C96DF808ED22 (void);
// 0x00000005 UnityEngine.GameObject DatasetLoader::LoadMeshSetGameObject(System.String,System.String,UnityEngine.Transform)
extern void DatasetLoader_LoadMeshSetGameObject_m356068F7075AAF7ED70C785789455AB1DC8200EF (void);
// 0x00000006 System.Void DatasetLoader::.ctor()
extern void DatasetLoader__ctor_mB593CEC5D5818630C28582C4102D88D4C0BF6A1C (void);
// 0x00000007 System.Void DatasetLoaderUI::LoadDataSetsAndCreateButtons()
extern void DatasetLoaderUI_LoadDataSetsAndCreateButtons_m00714473DA95EA1860A9AD14F6EBD8F6B7363321 (void);
// 0x00000008 System.Void DatasetLoaderUI::ClearOldButtons()
extern void DatasetLoaderUI_ClearOldButtons_m2964492A9E0CFA30A7D0AE4557E69671A37E07B6 (void);
// 0x00000009 System.Void DatasetLoaderUI::UpdateUI(System.Int32)
extern void DatasetLoaderUI_UpdateUI_m48FA0527E1DE9744A84C9F481887A44C33ADEA08 (void);
// 0x0000000A System.Void DatasetLoaderUI::.ctor()
extern void DatasetLoaderUI__ctor_m4197F4291CC171E1267A1BC607FB5186E4860707 (void);
// 0x0000000B System.Void DatasetLoaderUI/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mF61ED45BB7A184FC56949869C04F4E0159EE5930 (void);
// 0x0000000C System.Void DatasetLoaderUI/<>c__DisplayClass7_1::.ctor()
extern void U3CU3Ec__DisplayClass7_1__ctor_m086CDC9848FEDD496AEE3FB40FEFB850A8AB759D (void);
// 0x0000000D System.Void DatasetLoaderUI/<>c__DisplayClass7_1::<LoadDataSetsAndCreateButtons>b__0()
extern void U3CU3Ec__DisplayClass7_1_U3CLoadDataSetsAndCreateButtonsU3Eb__0_m0442F40599F32FD0920BA37E6197CF95197A5308 (void);
// 0x0000000E System.Void DatasetLoaderUI/<>c__DisplayClass7_1::<LoadDataSetsAndCreateButtons>b__1()
extern void U3CU3Ec__DisplayClass7_1_U3CLoadDataSetsAndCreateButtonsU3Eb__1_m0E0A9D7580676307F2800CD79458218BC582B94B (void);
// 0x0000000F System.Void DatasetUnit::.ctor()
extern void DatasetUnit__ctor_m8FCEC77D834B845E58A5DCEC58058FE8C21EE11E (void);
// 0x00000010 System.Collections.Generic.List`1<DatasetUnit> DirectoryScanner::GetDataSetsInFolder()
extern void DirectoryScanner_GetDataSetsInFolder_mC92D3CAD7E1E8CC828068FAAD52CF27548869A85 (void);
// 0x00000011 System.String DirectoryScanner::GetStorageRoot(System.String)
extern void DirectoryScanner_GetStorageRoot_mF4F1901211E933D334227246EB9B32B4AE47B0F4 (void);
// 0x00000012 System.String DirectoryScanner::ScanFor3dtFile(System.String,System.String)
extern void DirectoryScanner_ScanFor3dtFile_m39418B4B62F749AE8749FD7BA2EA4D1969A21B3E (void);
// 0x00000013 System.String DirectoryScanner::FindFileWithExtension(System.String,System.String)
extern void DirectoryScanner_FindFileWithExtension_m8FE2CC3E1AABEE676A87320EE8E3F9D63230B3D0 (void);
// 0x00000014 System.String DirectoryScanner::FindFile(System.String,System.String)
extern void DirectoryScanner_FindFile_m3D623C615BD486170120675AFEE0A2DC2F3FBAD8 (void);
// 0x00000015 System.String[] DirectoryScanner::FindFilesWithExtension(System.String,System.String)
extern void DirectoryScanner_FindFilesWithExtension_mFCB7A6A9429942C2796EDE43E17512C89483495D (void);
// 0x00000016 System.Void DirectoryScanner::.ctor()
extern void DirectoryScanner__ctor_mF68C848F732449BC9A1E5225423B0C64272E00D0 (void);
// 0x00000017 System.Void AreaTargetsFeatureCheck::Start()
extern void AreaTargetsFeatureCheck_Start_m138BECDC6F4C47BE7527CAEDA2A35C16B78F61B2 (void);
// 0x00000018 System.Void AreaTargetsFeatureCheck::OnDestroy()
extern void AreaTargetsFeatureCheck_OnDestroy_mA6250847BC04730C3E5227F4817E6B4B5E140E9F (void);
// 0x00000019 System.Void AreaTargetsFeatureCheck::OnVuforiaStarted()
extern void AreaTargetsFeatureCheck_OnVuforiaStarted_mC346384072876F632A69C9154C9A405BB50FD41B (void);
// 0x0000001A System.Void AreaTargetsFeatureCheck::GoBackToMainMenu()
extern void AreaTargetsFeatureCheck_GoBackToMainMenu_m11C8F480782803E39731BEDA36E193F55F5B2830 (void);
// 0x0000001B System.Void AreaTargetsFeatureCheck::.ctor()
extern void AreaTargetsFeatureCheck__ctor_m111FE569E94ACD863CC6C4ACEDA40E7D6FB706F8 (void);
// 0x0000001C System.Void BarcodeMenu::Awake()
extern void BarcodeMenu_Awake_m43FB46F57425C56AAF104F915DC89D1F3C4AF231 (void);
// 0x0000001D System.Void BarcodeMenu::OnDestroy()
extern void BarcodeMenu_OnDestroy_mDEAF51C72C0C931C1A829EC1AFCAF8EE4E91C225 (void);
// 0x0000001E System.Void BarcodeMenu::OnVuforiaStarted()
extern void BarcodeMenu_OnVuforiaStarted_mAEC27B9EA3B82DA7F00A27AE678924C9F5A9D369 (void);
// 0x0000001F System.Void BarcodeMenu::ShowBarcodeModeMenu(System.Boolean)
extern void BarcodeMenu_ShowBarcodeModeMenu_mB0490AF7872117BBAAC9CB9342DB24F806AC2713 (void);
// 0x00000020 System.Void BarcodeMenu::ToggleBarcodeMode1D()
extern void BarcodeMenu_ToggleBarcodeMode1D_m37C810162FEC23817E8055910A17BF2395F301D9 (void);
// 0x00000021 System.Void BarcodeMenu::ToggleBarcodeMode2D()
extern void BarcodeMenu_ToggleBarcodeMode2D_m9D0F26C24DA68F622B7D9E26991DBF296EB133CC (void);
// 0x00000022 System.Void BarcodeMenu::ToggleBarcodeModeAll()
extern void BarcodeMenu_ToggleBarcodeModeAll_m38363BC8774444BB65EB1387DCFC10B399AB9A58 (void);
// 0x00000023 System.Void BarcodeMenu::ToggleSounds()
extern void BarcodeMenu_ToggleSounds_m550A1F128FFEB1FC9141A723817B5FC9B5FB5016 (void);
// 0x00000024 System.Void BarcodeMenu::.ctor()
extern void BarcodeMenu__ctor_mF527CE2A79409FB438E52C165A1A9AA5555E094E (void);
// 0x00000025 System.Void BarcodeScanner::Awake()
extern void BarcodeScanner_Awake_mE6F4D008919FD006E681838F12CCCEAA995AA509 (void);
// 0x00000026 System.Void BarcodeScanner::OnDestroy()
extern void BarcodeScanner_OnDestroy_mB8918123DCF1076F9B97DD16FB9A0F8A6E076882 (void);
// 0x00000027 System.Void BarcodeScanner::Update()
extern void BarcodeScanner_Update_mF7A552E8C51946B76563215324BCD4DB02E97E98 (void);
// 0x00000028 Vuforia.BarcodeBehaviour BarcodeScanner::FindClosestBarcode()
extern void BarcodeScanner_FindClosestBarcode_m9B7F876FB5787E38E9EBB1814939C909ACAD5E61 (void);
// 0x00000029 System.Void BarcodeScanner::SetBarcodeScannerMode(BarcodeScanner/Mode)
extern void BarcodeScanner_SetBarcodeScannerMode_m6EC1E4A613D28AAEFBAFA95234CDD5DE7B4A8A93 (void);
// 0x0000002A System.Void BarcodeScanner::OnObserverCreated(Vuforia.ObserverBehaviour)
extern void BarcodeScanner_OnObserverCreated_mB5D2774767CA8009A91665E7FCD3539A05D7456E (void);
// 0x0000002B System.Void BarcodeScanner::OnObserverDestroyed(Vuforia.ObserverBehaviour)
extern void BarcodeScanner_OnObserverDestroyed_m598976D653CB28E624346A82899CBD352CBB5E25 (void);
// 0x0000002C System.Void BarcodeScanner::UnselectBarcode(Vuforia.BarcodeBehaviour)
extern void BarcodeScanner_UnselectBarcode_m7DAA2A8AD4DC9906FE027BA3F82BD5398CCA4A66 (void);
// 0x0000002D System.Void BarcodeScanner::SelectBarcode(Vuforia.BarcodeBehaviour)
extern void BarcodeScanner_SelectBarcode_m5EC9F6BC427BF22A4D9611087B3BA313B2586285 (void);
// 0x0000002E System.Void BarcodeScanner::.ctor()
extern void BarcodeScanner__ctor_m9A86177717F04FE4533EA168EA84E3B2640383BE (void);
// 0x0000002F System.Void BarcodeScanner/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m4C4D0D76007F727716B3ED72B4597BD28895DF06 (void);
// 0x00000030 System.Void BarcodeScanner/<>c__DisplayClass16_1::.ctor()
extern void U3CU3Ec__DisplayClass16_1__ctor_mB1023CB85A2614CD3F49D1BC27959E3BA9873F1F (void);
// 0x00000031 UnityEngine.Vector3 BarcodeScanner/<>c__DisplayClass16_1::<FindClosestBarcode>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass16_1_U3CFindClosestBarcodeU3Eb__1_m90AE351BC0F404D4081D7F7AB9BF3A87C465ED9D (void);
// 0x00000032 System.Void BarcodeScanner/<>c::.cctor()
extern void U3CU3Ec__cctor_mEDD3918D1A477D8005B23627A7B6438008883026 (void);
// 0x00000033 System.Void BarcodeScanner/<>c::.ctor()
extern void U3CU3Ec__ctor_m055F9152350C2224A38DC0DDECD128D71A88E70A (void);
// 0x00000034 System.Boolean BarcodeScanner/<>c::<FindClosestBarcode>b__16_0(Vuforia.BarcodeBehaviour)
extern void U3CU3Ec_U3CFindClosestBarcodeU3Eb__16_0_m7BF044EC216932095D3164196E7916F27AB28315 (void);
// 0x00000035 System.Void CloudContentManager::Start()
extern void CloudContentManager_Start_m6A4594B9B4A63243A628D1960ABBDACA4ECCEB3E (void);
// 0x00000036 System.Void CloudContentManager::ShowTargetInfo(System.Boolean)
extern void CloudContentManager_ShowTargetInfo_m53099E43F18CB7DC7E0CD18FE91B33BE80492672 (void);
// 0x00000037 System.Void CloudContentManager::HandleTargetFinderResult(Vuforia.CloudRecoBehaviour/CloudRecoSearchResult)
extern void CloudContentManager_HandleTargetFinderResult_m6B917ADB6A23281976DADE337E7AFCE931BA9120 (void);
// 0x00000038 System.Void CloudContentManager::.ctor()
extern void CloudContentManager__ctor_mB13B30EB245F776623F25263A0CB7F545F870B69 (void);
// 0x00000039 System.Void CloudContentManager/AugmentationObject::.ctor()
extern void AugmentationObject__ctor_mD49E3D73C093AC809E0DA49AEBC0DD107720FA10 (void);
// 0x0000003A System.Void CloudErrorHandler::Start()
extern void CloudErrorHandler_Start_mA6B792DBCBDBDACE4600FC0A056E97ED8BE10126 (void);
// 0x0000003B System.Void CloudErrorHandler::OnDestroy()
extern void CloudErrorHandler_OnDestroy_mBDF337F8DDA0102B16DB989255B43056C33A8883 (void);
// 0x0000003C System.Void CloudErrorHandler::OnInitError(Vuforia.CloudRecoBehaviour/InitError)
extern void CloudErrorHandler_OnInitError_m1D1EE6BC198D62130AB11D946781F1F8ED80C8DE (void);
// 0x0000003D System.Void CloudErrorHandler::OnUpdateError(Vuforia.CloudRecoBehaviour/QueryError)
extern void CloudErrorHandler_OnUpdateError_m6CAF377ED183B079F1C1744299FDF5FAD2149425 (void);
// 0x0000003E System.Void CloudErrorHandler::CloseDialog()
extern void CloudErrorHandler_CloseDialog_mF471A933D545991ECF701B6E54A94C43008BCEC4 (void);
// 0x0000003F System.Void CloudErrorHandler::RestartApplication()
extern void CloudErrorHandler_RestartApplication_m9CEA51B72C52C383EB222B58CA085398E5F41D7B (void);
// 0x00000040 System.Void CloudErrorHandler::.ctor()
extern void CloudErrorHandler__ctor_m653E640D7B51C5103BE13238A32C44292694BD43 (void);
// 0x00000041 System.Void CloudObserverEventHandler::Start()
extern void CloudObserverEventHandler_Start_m022C52299B459F7B5D8576132E1C26D6B05B1135 (void);
// 0x00000042 System.Void CloudObserverEventHandler::OnDestroy()
extern void CloudObserverEventHandler_OnDestroy_m40B8012AD7F910005A28F53976A7000AE4E68947 (void);
// 0x00000043 System.Void CloudObserverEventHandler::OnReset()
extern void CloudObserverEventHandler_OnReset_mA244435AD62CFA436B5D82B8B8094C973B466324 (void);
// 0x00000044 System.Void CloudObserverEventHandler::TargetCreated(Vuforia.CloudRecoBehaviour/CloudRecoSearchResult)
extern void CloudObserverEventHandler_TargetCreated_mACE2E0225FCE8FD2AC0C3E4BE3D5BFEA273D6164 (void);
// 0x00000045 System.Void CloudObserverEventHandler::OnTrackingFound()
extern void CloudObserverEventHandler_OnTrackingFound_m8C27FE295EB256FACBE99D7E2139EC11C668AEA8 (void);
// 0x00000046 System.Void CloudObserverEventHandler::OnTrackingLost()
extern void CloudObserverEventHandler_OnTrackingLost_m2305D751E39BFA1EA9B74ADD93E397B19C5198E9 (void);
// 0x00000047 System.Void CloudObserverEventHandler::OnTargetStatusChanged(Vuforia.ObserverBehaviour,Vuforia.TargetStatus)
extern void CloudObserverEventHandler_OnTargetStatusChanged_mE90C7BBAABF9C89A964B95F4D0FC24A35A9423F1 (void);
// 0x00000048 System.Collections.IEnumerator CloudObserverEventHandler::ResetCloudReco()
extern void CloudObserverEventHandler_ResetCloudReco_m4E76EBDB220865F3C1B2A998E0BCF249361C74F3 (void);
// 0x00000049 System.Void CloudObserverEventHandler::.ctor()
extern void CloudObserverEventHandler__ctor_mE5F261109E2A9537012E094E4AC97DB8EEF356D3 (void);
// 0x0000004A System.Void CloudObserverEventHandler/<ResetCloudReco>d__10::.ctor(System.Int32)
extern void U3CResetCloudRecoU3Ed__10__ctor_m575373C698A90B145DB9EE872DA7BF85589D27F1 (void);
// 0x0000004B System.Void CloudObserverEventHandler/<ResetCloudReco>d__10::System.IDisposable.Dispose()
extern void U3CResetCloudRecoU3Ed__10_System_IDisposable_Dispose_m2CCEACA4C7280F99BD15191C8EC6114AE05E97FE (void);
// 0x0000004C System.Boolean CloudObserverEventHandler/<ResetCloudReco>d__10::MoveNext()
extern void U3CResetCloudRecoU3Ed__10_MoveNext_mEE4204905A3DF2D6F67B981269723938B7C20E73 (void);
// 0x0000004D System.Object CloudObserverEventHandler/<ResetCloudReco>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetCloudRecoU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m114C443ABAF0C5CAE008D377A2DF445950DA987A (void);
// 0x0000004E System.Void CloudObserverEventHandler/<ResetCloudReco>d__10::System.Collections.IEnumerator.Reset()
extern void U3CResetCloudRecoU3Ed__10_System_Collections_IEnumerator_Reset_m50733F469EA67C34C1A288533B46B1BA0FC4033F (void);
// 0x0000004F System.Object CloudObserverEventHandler/<ResetCloudReco>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CResetCloudRecoU3Ed__10_System_Collections_IEnumerator_get_Current_m5C6E41326BAB1EB538087677298FB277881478F7 (void);
// 0x00000050 System.Void CloudRecoEventHandler::Start()
extern void CloudRecoEventHandler_Start_m1FD1F705A2346D8640E522255005CC85D6ECB375 (void);
// 0x00000051 System.Void CloudRecoEventHandler::Update()
extern void CloudRecoEventHandler_Update_mE4B7652CFF7959F35C24855F047AE6E6250FB640 (void);
// 0x00000052 System.Void CloudRecoEventHandler::OnInitialized(Vuforia.CloudRecoBehaviour)
extern void CloudRecoEventHandler_OnInitialized_m5CC08578A4BA6C93C6D91C57FF9960D7C4248718 (void);
// 0x00000053 System.Void CloudRecoEventHandler::OnStateChanged(System.Boolean)
extern void CloudRecoEventHandler_OnStateChanged_mCD363CE0484CCA7CA4FBB822DE145179A55EB6FC (void);
// 0x00000054 System.Void CloudRecoEventHandler::OnNewSearchResult(Vuforia.CloudRecoBehaviour/CloudRecoSearchResult)
extern void CloudRecoEventHandler_OnNewSearchResult_mB482D35D3F919F82DD008A7D8B22459D28D8E504 (void);
// 0x00000055 System.Void CloudRecoEventHandler::SetCloudActivityIconVisible(System.Boolean)
extern void CloudRecoEventHandler_SetCloudActivityIconVisible_m86CC28B576E00D3879A8F3EA6F3AAC53AEF012EA (void);
// 0x00000056 System.Void CloudRecoEventHandler::.ctor()
extern void CloudRecoEventHandler__ctor_mB6057DAB96909DCA132538130D0A1927D402760E (void);
// 0x00000057 System.Boolean CloudRecoScanLine::get_CloudEnabled()
extern void CloudRecoScanLine_get_CloudEnabled_mF3BAFB3F7D102B00DC6DC51B4AC197721508A5E4 (void);
// 0x00000058 System.Void CloudRecoScanLine::Start()
extern void CloudRecoScanLine_Start_m0860CCE9408F9770D4A317A16E61726917DBDD29 (void);
// 0x00000059 System.Void CloudRecoScanLine::Update()
extern void CloudRecoScanLine_Update_m910A6638E66C6829FFB6C662701BF4346EAE882D (void);
// 0x0000005A System.Void CloudRecoScanLine::.ctor()
extern void CloudRecoScanLine__ctor_mE353B0B9A92141EBE84B145B146C75F3F56AEA94 (void);
// 0x0000005B System.Void RotateAroundCylinder::Start()
extern void RotateAroundCylinder_Start_m87DC5E3559478896548F6F8F564F8609DE4102B1 (void);
// 0x0000005C System.Void RotateAroundCylinder::Update()
extern void RotateAroundCylinder_Update_m7BD6B2664B3162FA23FD16DFD5B0E6FA163FF065 (void);
// 0x0000005D System.Void RotateAroundCylinder::.ctor()
extern void RotateAroundCylinder__ctor_m175F3706C3FC4C8143F2D592B8C6F1DF3B90F985 (void);
// 0x0000005E System.Void DevicePoseManager::Start()
extern void DevicePoseManager_Start_m2138B8582A683195869ED170EE122771A0992EE1 (void);
// 0x0000005F System.Void DevicePoseManager::Update()
extern void DevicePoseManager_Update_m601A805AAC4D4AD2D5CA44C0315EB0D317D1617D (void);
// 0x00000060 System.Void DevicePoseManager::OnDestroy()
extern void DevicePoseManager_OnDestroy_m20F45703C2E88AEBB6F6A1195D9A464EC77EB625 (void);
// 0x00000061 System.Void DevicePoseManager::ResetDevicePose()
extern void DevicePoseManager_ResetDevicePose_m66AC46690CE1DB3D2F5F076EC053C7B7274384FF (void);
// 0x00000062 System.Void DevicePoseManager::TimerFinished(System.Object,System.Timers.ElapsedEventArgs)
extern void DevicePoseManager_TimerFinished_m3E40F6967244C47C715A19D23999AF654453C8C2 (void);
// 0x00000063 System.Void DevicePoseManager::OnVuforiaInitialized(Vuforia.VuforiaInitError)
extern void DevicePoseManager_OnVuforiaInitialized_m4ECED18C82362EC5E44A7AC321D9AA8C8D01F159 (void);
// 0x00000064 System.Void DevicePoseManager::OnTargetStatusChanged(Vuforia.ObserverBehaviour,Vuforia.TargetStatus)
extern void DevicePoseManager_OnTargetStatusChanged_m1CC7F3EEAA0A9E89DFB7844505799E813B4694B5 (void);
// 0x00000065 System.Void DevicePoseManager::.ctor()
extern void DevicePoseManager__ctor_m27BE9D05DE1526FF784212C44AEF48DEEE0988A7 (void);
// 0x00000066 System.Void DevicePoseManager/DevicePoseResetEvent::.ctor()
extern void DevicePoseResetEvent__ctor_m1DC036BA312BF7E76F1008A5A99AE2932AA4F9D0 (void);
// 0x00000067 System.Void DevicePoseUI::Start()
extern void DevicePoseUI_Start_m958626C6520AC1E18C7D0F114503F4877A5C173A (void);
// 0x00000068 System.Void DevicePoseUI::OnDestroy()
extern void DevicePoseUI_OnDestroy_mA62586BC246917F18C0D9D23A33906DAC19BB134 (void);
// 0x00000069 System.Void DevicePoseUI::OnTargetStatusChanged(Vuforia.ObserverBehaviour,Vuforia.TargetStatus)
extern void DevicePoseUI_OnTargetStatusChanged_m2C936DA5E8F89D38A9540061D09587EDFAE59893 (void);
// 0x0000006A System.Void DevicePoseUI::.ctor()
extern void DevicePoseUI__ctor_mC7AFF078CE305F216D4FBF75A067C552C97437CC (void);
// 0x0000006B System.Boolean GroundPlaneUI::get_SurfaceIndicatorVisibilityConditionsMet()
extern void GroundPlaneUI_get_SurfaceIndicatorVisibilityConditionsMet_mE0EFB466584EAE6F75A3E7A95AA798C183BCA804 (void);
// 0x0000006C System.Void GroundPlaneUI::Start()
extern void GroundPlaneUI_Start_m058E8C430733E2DEE74CBD426E4960DE2FF62270 (void);
// 0x0000006D System.Void GroundPlaneUI::LateUpdate()
extern void GroundPlaneUI_LateUpdate_mD83363CDBE5DD21A658371F38A2A31B4A812F3CD (void);
// 0x0000006E System.Void GroundPlaneUI::OnAnchorFound()
extern void GroundPlaneUI_OnAnchorFound_mB94DD87733B6E12F4719D4BEACF4643E1185FD4A (void);
// 0x0000006F System.Void GroundPlaneUI::OnAnchorLost()
extern void GroundPlaneUI_OnAnchorLost_mC72D9DBE97A6B71260960540232F24AFAA038BA3 (void);
// 0x00000070 System.Void GroundPlaneUI::SetSurfaceIndicatorVisible(System.Boolean)
extern void GroundPlaneUI_SetSurfaceIndicatorVisible_m263ABDB58D584CD3AA9B0AD944E58B29E126163A (void);
// 0x00000071 System.Void GroundPlaneUI::.ctor()
extern void GroundPlaneUI__ctor_mF5470B5AB6F2B0DF61FB7B518417BBDA4F44A6DF (void);
// 0x00000072 System.Void MidAirUI::Start()
extern void MidAirUI_Start_m91B7A687E6E16800706E996772D77DF552A21413 (void);
// 0x00000073 System.Void MidAirUI::OnDestroy()
extern void MidAirUI_OnDestroy_m1D0862C133CD151C1556B774576E451C1B56FF2B (void);
// 0x00000074 System.Void MidAirUI::OnTargetStatusChanged(Vuforia.ObserverBehaviour,Vuforia.TargetStatus)
extern void MidAirUI_OnTargetStatusChanged_mCF081489F26E3F7E3461F93C5F26680070D50981 (void);
// 0x00000075 System.Void MidAirUI::.ctor()
extern void MidAirUI__ctor_m8D962FBDB2A64F9D31E212B24BE7AA0B4BAB4FB3 (void);
// 0x00000076 System.Boolean ProductPlacement::get_GroundPlaneHitReceived()
extern void ProductPlacement_get_GroundPlaneHitReceived_m41955631930F58FB319DAECDF7BCDAD9F97E8628 (void);
// 0x00000077 System.Void ProductPlacement::set_GroundPlaneHitReceived(System.Boolean)
extern void ProductPlacement_set_GroundPlaneHitReceived_mFF03BCF36CD8752BC82C3672D36961933EDE5E05 (void);
// 0x00000078 UnityEngine.Vector3 ProductPlacement::get_ProductScale()
extern void ProductPlacement_get_ProductScale_m688DB2F66E59671A3DC36ACEC0447CF52C169508 (void);
// 0x00000079 System.Void ProductPlacement::Start()
extern void ProductPlacement_Start_m6191FB48F2C5B06827FCD03EB6E41C9535D42E7D (void);
// 0x0000007A System.Void ProductPlacement::Update()
extern void ProductPlacement_Update_m6E3D7ECDADA38627089C947C047AD16E3170F5A8 (void);
// 0x0000007B System.Void ProductPlacement::LateUpdate()
extern void ProductPlacement_LateUpdate_m0B4C7657A0DC311558485061D1D8D2901A9E0A5B (void);
// 0x0000007C System.Void ProductPlacement::SnapProductToMousePosition()
extern void ProductPlacement_SnapProductToMousePosition_mB9E4CD8431885A8A5FA1AC2F4E594546B396EAF4 (void);
// 0x0000007D System.Void ProductPlacement::Reset()
extern void ProductPlacement_Reset_m45D6A46D740AFF1554873BF05EC7CB040A69B002 (void);
// 0x0000007E System.Void ProductPlacement::OnContentPlaced()
extern void ProductPlacement_OnContentPlaced_m404F8049F8998928B374C0C1FDE17CE6B85D4368 (void);
// 0x0000007F System.Void ProductPlacement::OnAutomaticHitTest(Vuforia.HitTestResult)
extern void ProductPlacement_OnAutomaticHitTest_m9F06D513BA94B5FB4C7BD5D6266D809F1B4B4186 (void);
// 0x00000080 System.Void ProductPlacement::SetupMaterials()
extern void ProductPlacement_SetupMaterials_m35D7E7DFE8001B9585E6B1FC3D7F98E1D6848BFD (void);
// 0x00000081 System.Void ProductPlacement::SetupFloor()
extern void ProductPlacement_SetupFloor_m8A008AC60D0FDAAC750D666340900F5432BEEEB5 (void);
// 0x00000082 System.Void ProductPlacement::EnablePreviewModeTransparency(System.Boolean)
extern void ProductPlacement_EnablePreviewModeTransparency_mEFE5CC2E867648140439BC5551816A86197CAD0C (void);
// 0x00000083 System.Void ProductPlacement::RotateTowardsCamera(UnityEngine.GameObject)
extern void ProductPlacement_RotateTowardsCamera_m91831142F215DEB5E59CE94CDE4E57CA1565A21A (void);
// 0x00000084 System.Void ProductPlacement::.ctor()
extern void ProductPlacement__ctor_mEA30C7229A707E6AFB819FD70422162702D83169 (void);
// 0x00000085 System.Boolean TargetStatusExtension::IsTrackedAndNormal(Vuforia.TargetStatus)
extern void TargetStatusExtension_IsTrackedAndNormal_m8F9A787CC564D38579025D0D74317EC09E960C28 (void);
// 0x00000086 System.Boolean TargetStatusExtension::IsTrackedOrLimited(Vuforia.TargetStatus)
extern void TargetStatusExtension_IsTrackedOrLimited_mE3752DD045D0F1B4B6ACCB14BA1E701D680E460C (void);
// 0x00000087 System.Boolean TouchHandler::get_sIsSingleFingerStationary()
extern void TouchHandler_get_sIsSingleFingerStationary_mD7A00728817D63A2A46B12F9DD128BF110F3E847 (void);
// 0x00000088 System.Boolean TouchHandler::get_sIsSingleFingerDragging()
extern void TouchHandler_get_sIsSingleFingerDragging_mAE28D8586C8A9600722673A505DE224E843FA37A (void);
// 0x00000089 System.Void TouchHandler::EnableRotation()
extern void TouchHandler_EnableRotation_m300E4B9C1FFC01F219A8D5BC59A0D9210FB21F7E (void);
// 0x0000008A System.Void TouchHandler::DisableRotation()
extern void TouchHandler_DisableRotation_mE260BB7BEE8B43A62CE78F714E581E0D5732CC1E (void);
// 0x0000008B System.Void TouchHandler::Start()
extern void TouchHandler_Start_mAB6670B1B0D45B40F6AFD9D900943132BC1D0099 (void);
// 0x0000008C System.Void TouchHandler::Update()
extern void TouchHandler_Update_mAE71A7E2EBB373533158147F1F8F80D262EA062E (void);
// 0x0000008D System.Void TouchHandler::GetTouchAngleAndDistance(UnityEngine.Touch,UnityEngine.Touch,System.Single&,System.Single&)
extern void TouchHandler_GetTouchAngleAndDistance_m2113D615061B4C68A862E9060CB67392829B89D1 (void);
// 0x0000008E System.Boolean TouchHandler::IsSingleFingerDown()
extern void TouchHandler_IsSingleFingerDown_mA2453043B99DB1DC3DB5169D30AFC15D8CCA8881 (void);
// 0x0000008F System.Void TouchHandler::.ctor()
extern void TouchHandler__ctor_m88A81D7076286C3FDFF7C5574D1A1FE8F33EB96D (void);
// 0x00000090 System.Void VideoController::Start()
extern void VideoController_Start_m3706BECF28DA61414235E01870D8EB6A5B3809C5 (void);
// 0x00000091 System.Void VideoController::OnDestroy()
extern void VideoController_OnDestroy_m2B0D1264295A80D6BEDCEA4488EEF41D9217239E (void);
// 0x00000092 System.Void VideoController::Update()
extern void VideoController_Update_mF96A4E1A8599B57C46C0013C10039414D3BE280D (void);
// 0x00000093 System.Void VideoController::OnApplicationPause(System.Boolean)
extern void VideoController_OnApplicationPause_mBD9F1EB8167061208380252E84B05641F2E37E47 (void);
// 0x00000094 System.Void VideoController::Play()
extern void VideoController_Play_mD0E2E9E04F648ED0812900421B0756FB2E1138ED (void);
// 0x00000095 System.Void VideoController::Pause()
extern void VideoController_Pause_m676E3FF0DB3E956600C704990CE6994351E88F68 (void);
// 0x00000096 System.Void VideoController::PauseAudio(System.Boolean)
extern void VideoController_PauseAudio_mB3327B37D60D84F34AFFDFD634A1466E4DC4AA38 (void);
// 0x00000097 System.Void VideoController::ShowPlayButton(System.Boolean)
extern void VideoController_ShowPlayButton_mFCEE394B4003631EBACB99EA80D2DE2A6DBBC83E (void);
// 0x00000098 System.Void VideoController::LogClipInfo()
extern void VideoController_LogClipInfo_m89E9FA78578DB0B21A13E5524AE7008F4FB2C250 (void);
// 0x00000099 System.Void VideoController::HandleMVideoError(UnityEngine.Video.VideoPlayer,System.String)
extern void VideoController_HandleMVideoError_m201418B6B79EE24A83E3A17AAE534E0740D7C53E (void);
// 0x0000009A System.Void VideoController::HandleStartedEvent(UnityEngine.Video.VideoPlayer)
extern void VideoController_HandleStartedEvent_mB5491A94477B216C347BCDD8826EF74F325D1D08 (void);
// 0x0000009B System.Void VideoController::HandlePrepareCompleted(UnityEngine.Video.VideoPlayer)
extern void VideoController_HandlePrepareCompleted_m057A3B1A12C0415C6169B21010FB779F5FE55447 (void);
// 0x0000009C System.Void VideoController::HandleSeekCompleted(UnityEngine.Video.VideoPlayer)
extern void VideoController_HandleSeekCompleted_m4E81847150687ED082B64089B0D7DB2BA8632A1F (void);
// 0x0000009D System.Void VideoController::HandleLoopPointReached(UnityEngine.Video.VideoPlayer)
extern void VideoController_HandleLoopPointReached_mFF49BB18FF76A01543875AE46AE5A43FBD60EDA7 (void);
// 0x0000009E System.Void VideoController::.ctor()
extern void VideoController__ctor_mD256C51FDB23A228FA15C5705709A08DDBD2C392 (void);
// 0x0000009F System.Void GuideViewCycler::Awake()
extern void GuideViewCycler_Awake_mC7CD6D6AA2F70CE8C48834DCA2D7BB3A351AFDA5 (void);
// 0x000000A0 System.Void GuideViewCycler::CycleGuideView()
extern void GuideViewCycler_CycleGuideView_mFEF4CBEBD8BF904238DD510B05B7B175C171526F (void);
// 0x000000A1 System.Void GuideViewCycler::.ctor()
extern void GuideViewCycler__ctor_mA0C9410292C77BB2F5BF4E521D80EEF236459257 (void);
// 0x000000A2 System.Void MTObserverEventHandler::HandleTargetStatusChanged(Vuforia.Status,Vuforia.Status)
extern void MTObserverEventHandler_HandleTargetStatusChanged_m31FA7861A619EE9F024E55D732FD0026870B37DC (void);
// 0x000000A3 System.Void MTObserverEventHandler::EnableScaleWarningPopup()
extern void MTObserverEventHandler_EnableScaleWarningPopup_mEB52C5374B54A10E1FB7665B4C887FC5BB7B7EAB (void);
// 0x000000A4 System.Void MTObserverEventHandler::.ctor()
extern void MTObserverEventHandler__ctor_m0BDAAB9222CFEBC4A3D88D10602F4682B3EF5EC2 (void);
// 0x000000A5 System.Void CustomSessionRecorder::Awake()
extern void CustomSessionRecorder_Awake_m33D0E326455615DD324DA6C9582E651A0177B442 (void);
// 0x000000A6 System.Void CustomSessionRecorder::Update()
extern void CustomSessionRecorder_Update_mD0D26D34192EA9C7CDA242A45A5FED417FBBA0D4 (void);
// 0x000000A7 System.Void CustomSessionRecorder::RecordingStarted(Vuforia.RecordingStartError)
extern void CustomSessionRecorder_RecordingStarted_m840720A0803E8D2588F3769DA5C3085D7B0C5619 (void);
// 0x000000A8 System.Void CustomSessionRecorder::RecordingStopped(Vuforia.RecordingStatusInfo)
extern void CustomSessionRecorder_RecordingStopped_m0ED6452C8A3CA2951DC2BEF041E5AD044A0D1A21 (void);
// 0x000000A9 System.Void CustomSessionRecorder::StorageCleaned(System.Boolean)
extern void CustomSessionRecorder_StorageCleaned_mEEFD279608620C4D7E6D8AFF104C821FBF1517D4 (void);
// 0x000000AA System.Void CustomSessionRecorder::ShareRequested(System.Boolean)
extern void CustomSessionRecorder_ShareRequested_mE20C5C70A205FE437D926A5D0E3824FF5576F48F (void);
// 0x000000AB System.Void CustomSessionRecorder::ShowInfoPopup(System.String)
extern void CustomSessionRecorder_ShowInfoPopup_m96DF9C041A9905C18BC2AB6E799D5CCB3E9E21EE (void);
// 0x000000AC System.Void CustomSessionRecorder::ShowShareDialog()
extern void CustomSessionRecorder_ShowShareDialog_m9DF63FE569F7C31D16ACE7D4DAEE80A92C9D68B4 (void);
// 0x000000AD System.Void CustomSessionRecorder::StartTimer()
extern void CustomSessionRecorder_StartTimer_mF3A1E5520823C2A484832635627A2B20C70B14EC (void);
// 0x000000AE System.Void CustomSessionRecorder::UpdateTimer()
extern void CustomSessionRecorder_UpdateTimer_m6956407DFB469ED36C0D3A9A2A488A5B993A58BD (void);
// 0x000000AF System.Void CustomSessionRecorder::StopTimer()
extern void CustomSessionRecorder_StopTimer_m0F37E40CE0A4AC92E1BDD3130AC6404B598638A6 (void);
// 0x000000B0 System.Void CustomSessionRecorder::.ctor()
extern void CustomSessionRecorder__ctor_mAEE20F25B34311D48DFEB7774C360FB9EBCB67BA (void);
// 0x000000B1 System.Void MultiTargetObserverEventHandler::OnTrackingFound()
extern void MultiTargetObserverEventHandler_OnTrackingFound_m2FF6B80325970DAAF99270A59B8B1D86FBB00942 (void);
// 0x000000B2 System.Void MultiTargetObserverEventHandler::OnTrackingLost()
extern void MultiTargetObserverEventHandler_OnTrackingLost_mD2E10F7FE49DE7DE84F6E896EA3CAC81EAE4811C (void);
// 0x000000B3 System.Void MultiTargetObserverEventHandler::.ctor()
extern void MultiTargetObserverEventHandler__ctor_mEF76A7066344F9538FD1F3DF341D42DE6275AC9E (void);
// 0x000000B4 System.Void CameraPoseAligner::Start()
extern void CameraPoseAligner_Start_mB9A780712C8F6A893B206D69A0E15847DD1CE2FF (void);
// 0x000000B5 System.Void CameraPoseAligner::OnDestroy()
extern void CameraPoseAligner_OnDestroy_m2763B341E76E06B4C129D6AF676368C3B75D51F6 (void);
// 0x000000B6 System.Void CameraPoseAligner::OnVuforiaStarted()
extern void CameraPoseAligner_OnVuforiaStarted_m93B9D00D4A03DDF21136AFC0EDEB0462561BFD21 (void);
// 0x000000B7 System.Void CameraPoseAligner::LateUpdate()
extern void CameraPoseAligner_LateUpdate_mA0E8E9398335BA85B59ADE7EDAA341F174C93F28 (void);
// 0x000000B8 System.Void CameraPoseAligner::.ctor()
extern void CameraPoseAligner__ctor_m2302A6A1C435AC39AEE57CE6D64D241D692B43E5 (void);
// 0x000000B9 System.Void EditorRecordingPlayer::Awake()
extern void EditorRecordingPlayer_Awake_m1BDAB9035E117284E3EABEFC7A030E4365BECE36 (void);
// 0x000000BA System.Void EditorRecordingPlayer::RestoreOriginalConfiguration()
extern void EditorRecordingPlayer_RestoreOriginalConfiguration_m90B8799D5F822AF6EAED51E0E4766CBECFDCE291 (void);
// 0x000000BB System.Void EditorRecordingPlayer::ConfigureScene()
extern void EditorRecordingPlayer_ConfigureScene_mA638C13C8019BC5053F29C30E76757FA89A77D69 (void);
// 0x000000BC System.Boolean EditorRecordingPlayer::IsConfigurationChanged()
extern void EditorRecordingPlayer_IsConfigurationChanged_m68903501CCDA04E0F8025D86C54DB4F3526D146C (void);
// 0x000000BD System.Void EditorRecordingPlayer::.ctor()
extern void EditorRecordingPlayer__ctor_m7063319844DB28EB1CE47620C7F55D450B14B511 (void);
// 0x000000BE System.Void EditorRecordingPlayer/SceneConfiguration::.ctor()
extern void SceneConfiguration__ctor_mCC27460EDA57F25BE39031D02AC12D4A763FB184 (void);
// 0x000000BF System.Void HideMenu::Awake()
extern void HideMenu_Awake_mDC6FB6A33CAFEF8FCFBEC1FF57FEC03525FFF3C9 (void);
// 0x000000C0 System.Void HideMenu::.ctor()
extern void HideMenu__ctor_mDA9E5AC1961755CE6F7E6351C2670AD6AF36A960 (void);
// 0x000000C1 System.Void VirtualButtonEventHandler::Awake()
extern void VirtualButtonEventHandler_Awake_mB30EA2AF6F49ADFE0897748A8591812D9DDE94CC (void);
// 0x000000C2 System.Void VirtualButtonEventHandler::Destroy()
extern void VirtualButtonEventHandler_Destroy_m9398BE0FDEFA3BF450D5956B5D2230EA7E88EF9B (void);
// 0x000000C3 System.Void VirtualButtonEventHandler::OnButtonPressed(Vuforia.VirtualButtonBehaviour)
extern void VirtualButtonEventHandler_OnButtonPressed_m53723C5F46CC2FCAA177F99237F4C6928354B06B (void);
// 0x000000C4 System.Void VirtualButtonEventHandler::OnButtonReleased(Vuforia.VirtualButtonBehaviour)
extern void VirtualButtonEventHandler_OnButtonReleased_mD55065DF0503DFFC6262A192AB181960E9F2C050 (void);
// 0x000000C5 System.Void VirtualButtonEventHandler::SetVirtualButtonMaterial(UnityEngine.Material)
extern void VirtualButtonEventHandler_SetVirtualButtonMaterial_m7284BF75B3FA488A6B220E9C1B6D9F8B5F7C6FFF (void);
// 0x000000C6 System.Collections.IEnumerator VirtualButtonEventHandler::DelayOnButtonReleasedEvent(System.Single)
extern void VirtualButtonEventHandler_DelayOnButtonReleasedEvent_m192D56CE9ACE6926285B39D5573B49D36D35A18D (void);
// 0x000000C7 System.Void VirtualButtonEventHandler::.ctor()
extern void VirtualButtonEventHandler__ctor_mFD75125B918ED11794AD29293ADFF29E529F795F (void);
// 0x000000C8 System.Void VirtualButtonEventHandler/<DelayOnButtonReleasedEvent>d__11::.ctor(System.Int32)
extern void U3CDelayOnButtonReleasedEventU3Ed__11__ctor_m607B0E9390FEFF60ED66C6C189AA941DFE53F688 (void);
// 0x000000C9 System.Void VirtualButtonEventHandler/<DelayOnButtonReleasedEvent>d__11::System.IDisposable.Dispose()
extern void U3CDelayOnButtonReleasedEventU3Ed__11_System_IDisposable_Dispose_m061333F7F8B1F7D29D0D87B664A96A8942CD836C (void);
// 0x000000CA System.Boolean VirtualButtonEventHandler/<DelayOnButtonReleasedEvent>d__11::MoveNext()
extern void U3CDelayOnButtonReleasedEventU3Ed__11_MoveNext_m24A67044776CEB32A7C5769903802C8F1ECA8ADF (void);
// 0x000000CB System.Object VirtualButtonEventHandler/<DelayOnButtonReleasedEvent>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayOnButtonReleasedEventU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m498904243D7BC36EEBD67D3DBC8870869FAA444D (void);
// 0x000000CC System.Void VirtualButtonEventHandler/<DelayOnButtonReleasedEvent>d__11::System.Collections.IEnumerator.Reset()
extern void U3CDelayOnButtonReleasedEventU3Ed__11_System_Collections_IEnumerator_Reset_mEDB68124637C973F28B66CCF4195B7505CD9BA4E (void);
// 0x000000CD System.Object VirtualButtonEventHandler/<DelayOnButtonReleasedEvent>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CDelayOnButtonReleasedEventU3Ed__11_System_Collections_IEnumerator_get_Current_mE1A9569C2503720A0512AFE6DC53323847037B5B (void);
// 0x000000CE System.Void AugmentationObject::.ctor()
extern void AugmentationObject__ctor_m03DA2C19CB63048FE0F5B21734F3C7D6DA77F573 (void);
// 0x000000CF System.Void LookAtObject::Update()
extern void LookAtObject_Update_m5E7E6CE608A764F6B49B582C91C13B84591B6C5F (void);
// 0x000000D0 System.Void LookAtObject::.ctor()
extern void LookAtObject__ctor_m524A7F857015FC64C156E823BF81F69A630D3BD1 (void);
// 0x000000D1 System.Void PanelShowHide::Hide()
extern void PanelShowHide_Hide_m886F5A206A397BF534195F77AAC2A6CD2344DDEA (void);
// 0x000000D2 System.Void PanelShowHide::Show(System.String,System.String,System.String,UnityEngine.Sprite)
extern void PanelShowHide_Show_mAA11A92C09DAF58B17994DD1BC2D689E2DDDA961 (void);
// 0x000000D3 System.Void PanelShowHide::ResetShowTrigger()
extern void PanelShowHide_ResetShowTrigger_mD9DC7BA5CAE2AC646571AE261C6858C66886B93B (void);
// 0x000000D4 System.Void PanelShowHide::.ctor()
extern void PanelShowHide__ctor_m369B06B70CCE506AE90A807470805959C3D198E9 (void);
// 0x000000D5 System.Void VuMarkContentUI::Awake()
extern void VuMarkContentUI_Awake_mB0F669C85EEB587B3CA191DD080C09AF3C329667 (void);
// 0x000000D6 System.Void VuMarkContentUI::Update()
extern void VuMarkContentUI_Update_m2C8B67D5ECC11305C209FF3708DD760A4FE89832 (void);
// 0x000000D7 System.Void VuMarkContentUI::OnDestroy()
extern void VuMarkContentUI_OnDestroy_m55D5896CBE892CE6E5073D9A1DF2D4B723ADC803 (void);
// 0x000000D8 System.Void VuMarkContentUI::OnVuforiaStarted()
extern void VuMarkContentUI_OnVuforiaStarted_m3B8A6ABC5EBD3E6097062F89C9AD4BB1498E227C (void);
// 0x000000D9 System.Void VuMarkContentUI::UpdateCanvasFadeAmount()
extern void VuMarkContentUI_UpdateCanvasFadeAmount_mB3FCCB8C96F5579F4D5284067BC209D6ED4E574C (void);
// 0x000000DA System.Void VuMarkContentUI::SetVuMarkInfoCanvas(Vuforia.VuMarkBehaviour)
extern void VuMarkContentUI_SetVuMarkInfoCanvas_m3D9A542C3966E7540777234FEAE267A1B73F428F (void);
// 0x000000DB System.Void VuMarkContentUI::.ctor()
extern void VuMarkContentUI__ctor_mB45CE8C58DB9387401C17BE801C5C45F56582DA9 (void);
// 0x000000DC System.Void VuMarkHandler::Start()
extern void VuMarkHandler_Start_m71A9964FF5BB5CF96C30C95784A629772A43E5ED (void);
// 0x000000DD System.Void VuMarkHandler::OnVuforiaStarted()
extern void VuMarkHandler_OnVuforiaStarted_m56FFBE8CDD9E6084AD6E910E4B532969C81118DC (void);
// 0x000000DE System.Void VuMarkHandler::OnVuforiaStopped()
extern void VuMarkHandler_OnVuforiaStopped_m62A8C9441825559C109EBD91EE01D479389B7812 (void);
// 0x000000DF System.Void VuMarkHandler::OnDestroy()
extern void VuMarkHandler_OnDestroy_m275EAB77AB2808FF8C366F87C41BB2F11EC18F50 (void);
// 0x000000E0 System.Void VuMarkHandler::OnVuMarkFound(Vuforia.VuMarkBehaviour)
extern void VuMarkHandler_OnVuMarkFound_m272E2F13892B967D401D61E47FA63B60C524AB2A (void);
// 0x000000E1 System.Void VuMarkHandler::OnVuMarkLost(Vuforia.VuMarkBehaviour)
extern void VuMarkHandler_OnVuMarkLost_m9FA9E6FFA51ED816D13419F3CE7498B88B4B4FA3 (void);
// 0x000000E2 System.String VuMarkHandler::GetNumericVuMarkDescription(Vuforia.VuMarkBehaviour)
extern void VuMarkHandler_GetNumericVuMarkDescription_m1036920AB9A0C42972D23A6929181AF10A5BFB5E (void);
// 0x000000E3 UnityEngine.Texture2D VuMarkHandler::RetrieveStoredTextureForVuMarkTarget(Vuforia.VuMarkBehaviour)
extern void VuMarkHandler_RetrieveStoredTextureForVuMarkTarget_mF6C7DADDFBE1DB854C9409F9E9481E888BDA144A (void);
// 0x000000E4 UnityEngine.GameObject VuMarkHandler::GetVuMarkAugmentation(System.String)
extern void VuMarkHandler_GetVuMarkAugmentation_m8CB19FA2D1CAAE0F81164A510071005ED0C35655 (void);
// 0x000000E5 System.Void VuMarkHandler::DestroyChildAugmentations(Vuforia.VuMarkBehaviour)
extern void VuMarkHandler_DestroyChildAugmentations_m43E6AE0ABCA859D7679F28C3B1109DCED90A7F79 (void);
// 0x000000E6 System.Void VuMarkHandler::.ctor()
extern void VuMarkHandler__ctor_m6F1CF4132E7BBCE0C69201743D56A6D22300AD20 (void);
// 0x000000E7 System.Void VuMarkOutline::Awake()
extern void VuMarkOutline_Awake_mED5BB28E0E12ECA4B4C65D1A5B6F52C22AE98D82 (void);
// 0x000000E8 System.Void VuMarkOutline::OnVuMarkFound(Vuforia.VuMarkBehaviour)
extern void VuMarkOutline_OnVuMarkFound_mAABCCFA74DAB3A691D11866FF59585F3FAFF4478 (void);
// 0x000000E9 System.Void VuMarkOutline::OnVuMarkLost(Vuforia.VuMarkBehaviour)
extern void VuMarkOutline_OnVuMarkLost_mA576F6B5CEF7445F0A0C38FE2DE7342E04D3322C (void);
// 0x000000EA System.Void VuMarkOutline::OnTargetStatusChanged(Vuforia.ObserverBehaviour,Vuforia.TargetStatus)
extern void VuMarkOutline_OnTargetStatusChanged_m9179A0DB8A28A0700E40EBD51C0870C5FD00BD4A (void);
// 0x000000EB System.Void VuMarkOutline::UpdateLineRenderer(Vuforia.TargetStatus)
extern void VuMarkOutline_UpdateLineRenderer_m1EF42FBABB496205F191D45F77E61D835BEC00DF (void);
// 0x000000EC System.Void VuMarkOutline::SetupVuMarkBorderOutline()
extern void VuMarkOutline_SetupVuMarkBorderOutline_m9948BD1269138366196ABC6A65262A9ECBA94AC1 (void);
// 0x000000ED System.Void VuMarkOutline::SetBorderPositions(Vuforia.VuMarkBehaviour)
extern void VuMarkOutline_SetBorderPositions_m0242E2F2832F64A329773955EC2CA2ABAA99BA74 (void);
// 0x000000EE System.Void VuMarkOutline::.ctor()
extern void VuMarkOutline__ctor_m77C6FFCEB3ECF05DC05BBD39A2B4B5220C27EF02 (void);
// 0x000000EF System.Void VuMarkUI::Start()
extern void VuMarkUI_Start_m251EFA50CD7CA405CC3C0DE8BA816DD8F777F130 (void);
// 0x000000F0 System.Void VuMarkUI::Update()
extern void VuMarkUI_Update_m25CB39C4EF21523E68992F07ED5F4D508181E598 (void);
// 0x000000F1 System.Void VuMarkUI::UpdateClosestTarget()
extern void VuMarkUI_UpdateClosestTarget_mEFD66B662DFD231112BB5527F2D58D708A2CC077 (void);
// 0x000000F2 System.Collections.IEnumerator VuMarkUI::ShowPanelAfter(System.Single,System.String,System.String,System.String,UnityEngine.Sprite)
extern void VuMarkUI_ShowPanelAfter_mACDCB247DBF61B3EBB8BB4E36017C3BBEDCB9FA9 (void);
// 0x000000F3 System.Void VuMarkUI::.ctor()
extern void VuMarkUI__ctor_m3B105988B67EFFC156C6DE94EC7FC96260E8E487 (void);
// 0x000000F4 System.Void VuMarkUI/<ShowPanelAfter>d__6::.ctor(System.Int32)
extern void U3CShowPanelAfterU3Ed__6__ctor_m06A477DBE347E475B6AD1F0F12E05E23CEE3DE0B (void);
// 0x000000F5 System.Void VuMarkUI/<ShowPanelAfter>d__6::System.IDisposable.Dispose()
extern void U3CShowPanelAfterU3Ed__6_System_IDisposable_Dispose_m4BA0A7083C5F11B60773C093AE665A448D14D956 (void);
// 0x000000F6 System.Boolean VuMarkUI/<ShowPanelAfter>d__6::MoveNext()
extern void U3CShowPanelAfterU3Ed__6_MoveNext_mAA38A2708F8FB6FC8124B2726AD02E092923FD93 (void);
// 0x000000F7 System.Object VuMarkUI/<ShowPanelAfter>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPanelAfterU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m004C1013489EEB9A74A6397A974DCB749F187C11 (void);
// 0x000000F8 System.Void VuMarkUI/<ShowPanelAfter>d__6::System.Collections.IEnumerator.Reset()
extern void U3CShowPanelAfterU3Ed__6_System_Collections_IEnumerator_Reset_mD268BF011AA7E7CAFD487202E6459A9EAB0ECBD1 (void);
// 0x000000F9 System.Object VuMarkUI/<ShowPanelAfter>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CShowPanelAfterU3Ed__6_System_Collections_IEnumerator_get_Current_m4940524F3D2E89946C1D8B8EEB316D6709C28BA1 (void);
// 0x000000FA System.String VuMarkUtilities::GetVuMarkId(Vuforia.VuMarkBehaviour)
extern void VuMarkUtilities_GetVuMarkId_m9DADEE5A3FE502D35049BEACF7989731565465A6 (void);
// 0x000000FB System.String VuMarkUtilities::GetVuMarkDataType(Vuforia.VuMarkBehaviour)
extern void VuMarkUtilities_GetVuMarkDataType_m434E0A5E05104BE16D0F96394C162BAE3CEC9F82 (void);
// 0x000000FC UnityEngine.Texture2D VuMarkUtilities::GenerateTextureFromVuMarkInstanceImage(Vuforia.VuMarkBehaviour)
extern void VuMarkUtilities_GenerateTextureFromVuMarkInstanceImage_m3C7D46B220FBA8E8D8A38852AB84A9CE3FBB8696 (void);
static Il2CppMethodPointer s_methodPointers[252] = 
{
	DatasetLoader_TriggerAreaTargetLoading_mC8248944F4269647BEEDB93095FE1DA821E0E17B,
	DatasetLoader_UnloadAreaTarget_m866754A6E399BF99D6E0979BC41A821EC514DBA2,
	DatasetLoader_LoadAreaTarget_mBAAEC58BCDCA6118BC33F6F3FA7FB2B82B6E5FF9,
	DatasetLoader_LoadDatasetModel_mAB210F3E9ABF3E25BD95A3D9F6E8C96DF808ED22,
	DatasetLoader_LoadMeshSetGameObject_m356068F7075AAF7ED70C785789455AB1DC8200EF,
	DatasetLoader__ctor_mB593CEC5D5818630C28582C4102D88D4C0BF6A1C,
	DatasetLoaderUI_LoadDataSetsAndCreateButtons_m00714473DA95EA1860A9AD14F6EBD8F6B7363321,
	DatasetLoaderUI_ClearOldButtons_m2964492A9E0CFA30A7D0AE4557E69671A37E07B6,
	DatasetLoaderUI_UpdateUI_m48FA0527E1DE9744A84C9F481887A44C33ADEA08,
	DatasetLoaderUI__ctor_m4197F4291CC171E1267A1BC607FB5186E4860707,
	U3CU3Ec__DisplayClass7_0__ctor_mF61ED45BB7A184FC56949869C04F4E0159EE5930,
	U3CU3Ec__DisplayClass7_1__ctor_m086CDC9848FEDD496AEE3FB40FEFB850A8AB759D,
	U3CU3Ec__DisplayClass7_1_U3CLoadDataSetsAndCreateButtonsU3Eb__0_m0442F40599F32FD0920BA37E6197CF95197A5308,
	U3CU3Ec__DisplayClass7_1_U3CLoadDataSetsAndCreateButtonsU3Eb__1_m0E0A9D7580676307F2800CD79458218BC582B94B,
	DatasetUnit__ctor_m8FCEC77D834B845E58A5DCEC58058FE8C21EE11E,
	DirectoryScanner_GetDataSetsInFolder_mC92D3CAD7E1E8CC828068FAAD52CF27548869A85,
	DirectoryScanner_GetStorageRoot_mF4F1901211E933D334227246EB9B32B4AE47B0F4,
	DirectoryScanner_ScanFor3dtFile_m39418B4B62F749AE8749FD7BA2EA4D1969A21B3E,
	DirectoryScanner_FindFileWithExtension_m8FE2CC3E1AABEE676A87320EE8E3F9D63230B3D0,
	DirectoryScanner_FindFile_m3D623C615BD486170120675AFEE0A2DC2F3FBAD8,
	DirectoryScanner_FindFilesWithExtension_mFCB7A6A9429942C2796EDE43E17512C89483495D,
	DirectoryScanner__ctor_mF68C848F732449BC9A1E5225423B0C64272E00D0,
	AreaTargetsFeatureCheck_Start_m138BECDC6F4C47BE7527CAEDA2A35C16B78F61B2,
	AreaTargetsFeatureCheck_OnDestroy_mA6250847BC04730C3E5227F4817E6B4B5E140E9F,
	AreaTargetsFeatureCheck_OnVuforiaStarted_mC346384072876F632A69C9154C9A405BB50FD41B,
	AreaTargetsFeatureCheck_GoBackToMainMenu_m11C8F480782803E39731BEDA36E193F55F5B2830,
	AreaTargetsFeatureCheck__ctor_m111FE569E94ACD863CC6C4ACEDA40E7D6FB706F8,
	BarcodeMenu_Awake_m43FB46F57425C56AAF104F915DC89D1F3C4AF231,
	BarcodeMenu_OnDestroy_mDEAF51C72C0C931C1A829EC1AFCAF8EE4E91C225,
	BarcodeMenu_OnVuforiaStarted_mAEC27B9EA3B82DA7F00A27AE678924C9F5A9D369,
	BarcodeMenu_ShowBarcodeModeMenu_mB0490AF7872117BBAAC9CB9342DB24F806AC2713,
	BarcodeMenu_ToggleBarcodeMode1D_m37C810162FEC23817E8055910A17BF2395F301D9,
	BarcodeMenu_ToggleBarcodeMode2D_m9D0F26C24DA68F622B7D9E26991DBF296EB133CC,
	BarcodeMenu_ToggleBarcodeModeAll_m38363BC8774444BB65EB1387DCFC10B399AB9A58,
	BarcodeMenu_ToggleSounds_m550A1F128FFEB1FC9141A723817B5FC9B5FB5016,
	BarcodeMenu__ctor_mF527CE2A79409FB438E52C165A1A9AA5555E094E,
	BarcodeScanner_Awake_mE6F4D008919FD006E681838F12CCCEAA995AA509,
	BarcodeScanner_OnDestroy_mB8918123DCF1076F9B97DD16FB9A0F8A6E076882,
	BarcodeScanner_Update_mF7A552E8C51946B76563215324BCD4DB02E97E98,
	BarcodeScanner_FindClosestBarcode_m9B7F876FB5787E38E9EBB1814939C909ACAD5E61,
	BarcodeScanner_SetBarcodeScannerMode_m6EC1E4A613D28AAEFBAFA95234CDD5DE7B4A8A93,
	BarcodeScanner_OnObserverCreated_mB5D2774767CA8009A91665E7FCD3539A05D7456E,
	BarcodeScanner_OnObserverDestroyed_m598976D653CB28E624346A82899CBD352CBB5E25,
	BarcodeScanner_UnselectBarcode_m7DAA2A8AD4DC9906FE027BA3F82BD5398CCA4A66,
	BarcodeScanner_SelectBarcode_m5EC9F6BC427BF22A4D9611087B3BA313B2586285,
	BarcodeScanner__ctor_m9A86177717F04FE4533EA168EA84E3B2640383BE,
	U3CU3Ec__DisplayClass16_0__ctor_m4C4D0D76007F727716B3ED72B4597BD28895DF06,
	U3CU3Ec__DisplayClass16_1__ctor_mB1023CB85A2614CD3F49D1BC27959E3BA9873F1F,
	U3CU3Ec__DisplayClass16_1_U3CFindClosestBarcodeU3Eb__1_m90AE351BC0F404D4081D7F7AB9BF3A87C465ED9D,
	U3CU3Ec__cctor_mEDD3918D1A477D8005B23627A7B6438008883026,
	U3CU3Ec__ctor_m055F9152350C2224A38DC0DDECD128D71A88E70A,
	U3CU3Ec_U3CFindClosestBarcodeU3Eb__16_0_m7BF044EC216932095D3164196E7916F27AB28315,
	CloudContentManager_Start_m6A4594B9B4A63243A628D1960ABBDACA4ECCEB3E,
	CloudContentManager_ShowTargetInfo_m53099E43F18CB7DC7E0CD18FE91B33BE80492672,
	CloudContentManager_HandleTargetFinderResult_m6B917ADB6A23281976DADE337E7AFCE931BA9120,
	CloudContentManager__ctor_mB13B30EB245F776623F25263A0CB7F545F870B69,
	AugmentationObject__ctor_mD49E3D73C093AC809E0DA49AEBC0DD107720FA10,
	CloudErrorHandler_Start_mA6B792DBCBDBDACE4600FC0A056E97ED8BE10126,
	CloudErrorHandler_OnDestroy_mBDF337F8DDA0102B16DB989255B43056C33A8883,
	CloudErrorHandler_OnInitError_m1D1EE6BC198D62130AB11D946781F1F8ED80C8DE,
	CloudErrorHandler_OnUpdateError_m6CAF377ED183B079F1C1744299FDF5FAD2149425,
	CloudErrorHandler_CloseDialog_mF471A933D545991ECF701B6E54A94C43008BCEC4,
	CloudErrorHandler_RestartApplication_m9CEA51B72C52C383EB222B58CA085398E5F41D7B,
	CloudErrorHandler__ctor_m653E640D7B51C5103BE13238A32C44292694BD43,
	CloudObserverEventHandler_Start_m022C52299B459F7B5D8576132E1C26D6B05B1135,
	CloudObserverEventHandler_OnDestroy_m40B8012AD7F910005A28F53976A7000AE4E68947,
	CloudObserverEventHandler_OnReset_mA244435AD62CFA436B5D82B8B8094C973B466324,
	CloudObserverEventHandler_TargetCreated_mACE2E0225FCE8FD2AC0C3E4BE3D5BFEA273D6164,
	CloudObserverEventHandler_OnTrackingFound_m8C27FE295EB256FACBE99D7E2139EC11C668AEA8,
	CloudObserverEventHandler_OnTrackingLost_m2305D751E39BFA1EA9B74ADD93E397B19C5198E9,
	CloudObserverEventHandler_OnTargetStatusChanged_mE90C7BBAABF9C89A964B95F4D0FC24A35A9423F1,
	CloudObserverEventHandler_ResetCloudReco_m4E76EBDB220865F3C1B2A998E0BCF249361C74F3,
	CloudObserverEventHandler__ctor_mE5F261109E2A9537012E094E4AC97DB8EEF356D3,
	U3CResetCloudRecoU3Ed__10__ctor_m575373C698A90B145DB9EE872DA7BF85589D27F1,
	U3CResetCloudRecoU3Ed__10_System_IDisposable_Dispose_m2CCEACA4C7280F99BD15191C8EC6114AE05E97FE,
	U3CResetCloudRecoU3Ed__10_MoveNext_mEE4204905A3DF2D6F67B981269723938B7C20E73,
	U3CResetCloudRecoU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m114C443ABAF0C5CAE008D377A2DF445950DA987A,
	U3CResetCloudRecoU3Ed__10_System_Collections_IEnumerator_Reset_m50733F469EA67C34C1A288533B46B1BA0FC4033F,
	U3CResetCloudRecoU3Ed__10_System_Collections_IEnumerator_get_Current_m5C6E41326BAB1EB538087677298FB277881478F7,
	CloudRecoEventHandler_Start_m1FD1F705A2346D8640E522255005CC85D6ECB375,
	CloudRecoEventHandler_Update_mE4B7652CFF7959F35C24855F047AE6E6250FB640,
	CloudRecoEventHandler_OnInitialized_m5CC08578A4BA6C93C6D91C57FF9960D7C4248718,
	CloudRecoEventHandler_OnStateChanged_mCD363CE0484CCA7CA4FBB822DE145179A55EB6FC,
	CloudRecoEventHandler_OnNewSearchResult_mB482D35D3F919F82DD008A7D8B22459D28D8E504,
	CloudRecoEventHandler_SetCloudActivityIconVisible_m86CC28B576E00D3879A8F3EA6F3AAC53AEF012EA,
	CloudRecoEventHandler__ctor_mB6057DAB96909DCA132538130D0A1927D402760E,
	CloudRecoScanLine_get_CloudEnabled_mF3BAFB3F7D102B00DC6DC51B4AC197721508A5E4,
	CloudRecoScanLine_Start_m0860CCE9408F9770D4A317A16E61726917DBDD29,
	CloudRecoScanLine_Update_m910A6638E66C6829FFB6C662701BF4346EAE882D,
	CloudRecoScanLine__ctor_mE353B0B9A92141EBE84B145B146C75F3F56AEA94,
	RotateAroundCylinder_Start_m87DC5E3559478896548F6F8F564F8609DE4102B1,
	RotateAroundCylinder_Update_m7BD6B2664B3162FA23FD16DFD5B0E6FA163FF065,
	RotateAroundCylinder__ctor_m175F3706C3FC4C8143F2D592B8C6F1DF3B90F985,
	DevicePoseManager_Start_m2138B8582A683195869ED170EE122771A0992EE1,
	DevicePoseManager_Update_m601A805AAC4D4AD2D5CA44C0315EB0D317D1617D,
	DevicePoseManager_OnDestroy_m20F45703C2E88AEBB6F6A1195D9A464EC77EB625,
	DevicePoseManager_ResetDevicePose_m66AC46690CE1DB3D2F5F076EC053C7B7274384FF,
	DevicePoseManager_TimerFinished_m3E40F6967244C47C715A19D23999AF654453C8C2,
	DevicePoseManager_OnVuforiaInitialized_m4ECED18C82362EC5E44A7AC321D9AA8C8D01F159,
	DevicePoseManager_OnTargetStatusChanged_m1CC7F3EEAA0A9E89DFB7844505799E813B4694B5,
	DevicePoseManager__ctor_m27BE9D05DE1526FF784212C44AEF48DEEE0988A7,
	DevicePoseResetEvent__ctor_m1DC036BA312BF7E76F1008A5A99AE2932AA4F9D0,
	DevicePoseUI_Start_m958626C6520AC1E18C7D0F114503F4877A5C173A,
	DevicePoseUI_OnDestroy_mA62586BC246917F18C0D9D23A33906DAC19BB134,
	DevicePoseUI_OnTargetStatusChanged_m2C936DA5E8F89D38A9540061D09587EDFAE59893,
	DevicePoseUI__ctor_mC7AFF078CE305F216D4FBF75A067C552C97437CC,
	GroundPlaneUI_get_SurfaceIndicatorVisibilityConditionsMet_mE0EFB466584EAE6F75A3E7A95AA798C183BCA804,
	GroundPlaneUI_Start_m058E8C430733E2DEE74CBD426E4960DE2FF62270,
	GroundPlaneUI_LateUpdate_mD83363CDBE5DD21A658371F38A2A31B4A812F3CD,
	GroundPlaneUI_OnAnchorFound_mB94DD87733B6E12F4719D4BEACF4643E1185FD4A,
	GroundPlaneUI_OnAnchorLost_mC72D9DBE97A6B71260960540232F24AFAA038BA3,
	GroundPlaneUI_SetSurfaceIndicatorVisible_m263ABDB58D584CD3AA9B0AD944E58B29E126163A,
	GroundPlaneUI__ctor_mF5470B5AB6F2B0DF61FB7B518417BBDA4F44A6DF,
	MidAirUI_Start_m91B7A687E6E16800706E996772D77DF552A21413,
	MidAirUI_OnDestroy_m1D0862C133CD151C1556B774576E451C1B56FF2B,
	MidAirUI_OnTargetStatusChanged_mCF081489F26E3F7E3461F93C5F26680070D50981,
	MidAirUI__ctor_m8D962FBDB2A64F9D31E212B24BE7AA0B4BAB4FB3,
	ProductPlacement_get_GroundPlaneHitReceived_m41955631930F58FB319DAECDF7BCDAD9F97E8628,
	ProductPlacement_set_GroundPlaneHitReceived_mFF03BCF36CD8752BC82C3672D36961933EDE5E05,
	ProductPlacement_get_ProductScale_m688DB2F66E59671A3DC36ACEC0447CF52C169508,
	ProductPlacement_Start_m6191FB48F2C5B06827FCD03EB6E41C9535D42E7D,
	ProductPlacement_Update_m6E3D7ECDADA38627089C947C047AD16E3170F5A8,
	ProductPlacement_LateUpdate_m0B4C7657A0DC311558485061D1D8D2901A9E0A5B,
	ProductPlacement_SnapProductToMousePosition_mB9E4CD8431885A8A5FA1AC2F4E594546B396EAF4,
	ProductPlacement_Reset_m45D6A46D740AFF1554873BF05EC7CB040A69B002,
	ProductPlacement_OnContentPlaced_m404F8049F8998928B374C0C1FDE17CE6B85D4368,
	ProductPlacement_OnAutomaticHitTest_m9F06D513BA94B5FB4C7BD5D6266D809F1B4B4186,
	ProductPlacement_SetupMaterials_m35D7E7DFE8001B9585E6B1FC3D7F98E1D6848BFD,
	ProductPlacement_SetupFloor_m8A008AC60D0FDAAC750D666340900F5432BEEEB5,
	ProductPlacement_EnablePreviewModeTransparency_mEFE5CC2E867648140439BC5551816A86197CAD0C,
	ProductPlacement_RotateTowardsCamera_m91831142F215DEB5E59CE94CDE4E57CA1565A21A,
	ProductPlacement__ctor_mEA30C7229A707E6AFB819FD70422162702D83169,
	TargetStatusExtension_IsTrackedAndNormal_m8F9A787CC564D38579025D0D74317EC09E960C28,
	TargetStatusExtension_IsTrackedOrLimited_mE3752DD045D0F1B4B6ACCB14BA1E701D680E460C,
	TouchHandler_get_sIsSingleFingerStationary_mD7A00728817D63A2A46B12F9DD128BF110F3E847,
	TouchHandler_get_sIsSingleFingerDragging_mAE28D8586C8A9600722673A505DE224E843FA37A,
	TouchHandler_EnableRotation_m300E4B9C1FFC01F219A8D5BC59A0D9210FB21F7E,
	TouchHandler_DisableRotation_mE260BB7BEE8B43A62CE78F714E581E0D5732CC1E,
	TouchHandler_Start_mAB6670B1B0D45B40F6AFD9D900943132BC1D0099,
	TouchHandler_Update_mAE71A7E2EBB373533158147F1F8F80D262EA062E,
	TouchHandler_GetTouchAngleAndDistance_m2113D615061B4C68A862E9060CB67392829B89D1,
	TouchHandler_IsSingleFingerDown_mA2453043B99DB1DC3DB5169D30AFC15D8CCA8881,
	TouchHandler__ctor_m88A81D7076286C3FDFF7C5574D1A1FE8F33EB96D,
	VideoController_Start_m3706BECF28DA61414235E01870D8EB6A5B3809C5,
	VideoController_OnDestroy_m2B0D1264295A80D6BEDCEA4488EEF41D9217239E,
	VideoController_Update_mF96A4E1A8599B57C46C0013C10039414D3BE280D,
	VideoController_OnApplicationPause_mBD9F1EB8167061208380252E84B05641F2E37E47,
	VideoController_Play_mD0E2E9E04F648ED0812900421B0756FB2E1138ED,
	VideoController_Pause_m676E3FF0DB3E956600C704990CE6994351E88F68,
	VideoController_PauseAudio_mB3327B37D60D84F34AFFDFD634A1466E4DC4AA38,
	VideoController_ShowPlayButton_mFCEE394B4003631EBACB99EA80D2DE2A6DBBC83E,
	VideoController_LogClipInfo_m89E9FA78578DB0B21A13E5524AE7008F4FB2C250,
	VideoController_HandleMVideoError_m201418B6B79EE24A83E3A17AAE534E0740D7C53E,
	VideoController_HandleStartedEvent_mB5491A94477B216C347BCDD8826EF74F325D1D08,
	VideoController_HandlePrepareCompleted_m057A3B1A12C0415C6169B21010FB779F5FE55447,
	VideoController_HandleSeekCompleted_m4E81847150687ED082B64089B0D7DB2BA8632A1F,
	VideoController_HandleLoopPointReached_mFF49BB18FF76A01543875AE46AE5A43FBD60EDA7,
	VideoController__ctor_mD256C51FDB23A228FA15C5705709A08DDBD2C392,
	GuideViewCycler_Awake_mC7CD6D6AA2F70CE8C48834DCA2D7BB3A351AFDA5,
	GuideViewCycler_CycleGuideView_mFEF4CBEBD8BF904238DD510B05B7B175C171526F,
	GuideViewCycler__ctor_mA0C9410292C77BB2F5BF4E521D80EEF236459257,
	MTObserverEventHandler_HandleTargetStatusChanged_m31FA7861A619EE9F024E55D732FD0026870B37DC,
	MTObserverEventHandler_EnableScaleWarningPopup_mEB52C5374B54A10E1FB7665B4C887FC5BB7B7EAB,
	MTObserverEventHandler__ctor_m0BDAAB9222CFEBC4A3D88D10602F4682B3EF5EC2,
	CustomSessionRecorder_Awake_m33D0E326455615DD324DA6C9582E651A0177B442,
	CustomSessionRecorder_Update_mD0D26D34192EA9C7CDA242A45A5FED417FBBA0D4,
	CustomSessionRecorder_RecordingStarted_m840720A0803E8D2588F3769DA5C3085D7B0C5619,
	CustomSessionRecorder_RecordingStopped_m0ED6452C8A3CA2951DC2BEF041E5AD044A0D1A21,
	CustomSessionRecorder_StorageCleaned_mEEFD279608620C4D7E6D8AFF104C821FBF1517D4,
	CustomSessionRecorder_ShareRequested_mE20C5C70A205FE437D926A5D0E3824FF5576F48F,
	CustomSessionRecorder_ShowInfoPopup_m96DF9C041A9905C18BC2AB6E799D5CCB3E9E21EE,
	CustomSessionRecorder_ShowShareDialog_m9DF63FE569F7C31D16ACE7D4DAEE80A92C9D68B4,
	CustomSessionRecorder_StartTimer_mF3A1E5520823C2A484832635627A2B20C70B14EC,
	CustomSessionRecorder_UpdateTimer_m6956407DFB469ED36C0D3A9A2A488A5B993A58BD,
	CustomSessionRecorder_StopTimer_m0F37E40CE0A4AC92E1BDD3130AC6404B598638A6,
	CustomSessionRecorder__ctor_mAEE20F25B34311D48DFEB7774C360FB9EBCB67BA,
	MultiTargetObserverEventHandler_OnTrackingFound_m2FF6B80325970DAAF99270A59B8B1D86FBB00942,
	MultiTargetObserverEventHandler_OnTrackingLost_mD2E10F7FE49DE7DE84F6E896EA3CAC81EAE4811C,
	MultiTargetObserverEventHandler__ctor_mEF76A7066344F9538FD1F3DF341D42DE6275AC9E,
	CameraPoseAligner_Start_mB9A780712C8F6A893B206D69A0E15847DD1CE2FF,
	CameraPoseAligner_OnDestroy_m2763B341E76E06B4C129D6AF676368C3B75D51F6,
	CameraPoseAligner_OnVuforiaStarted_m93B9D00D4A03DDF21136AFC0EDEB0462561BFD21,
	CameraPoseAligner_LateUpdate_mA0E8E9398335BA85B59ADE7EDAA341F174C93F28,
	CameraPoseAligner__ctor_m2302A6A1C435AC39AEE57CE6D64D241D692B43E5,
	EditorRecordingPlayer_Awake_m1BDAB9035E117284E3EABEFC7A030E4365BECE36,
	EditorRecordingPlayer_RestoreOriginalConfiguration_m90B8799D5F822AF6EAED51E0E4766CBECFDCE291,
	EditorRecordingPlayer_ConfigureScene_mA638C13C8019BC5053F29C30E76757FA89A77D69,
	EditorRecordingPlayer_IsConfigurationChanged_m68903501CCDA04E0F8025D86C54DB4F3526D146C,
	EditorRecordingPlayer__ctor_m7063319844DB28EB1CE47620C7F55D450B14B511,
	SceneConfiguration__ctor_mCC27460EDA57F25BE39031D02AC12D4A763FB184,
	HideMenu_Awake_mDC6FB6A33CAFEF8FCFBEC1FF57FEC03525FFF3C9,
	HideMenu__ctor_mDA9E5AC1961755CE6F7E6351C2670AD6AF36A960,
	VirtualButtonEventHandler_Awake_mB30EA2AF6F49ADFE0897748A8591812D9DDE94CC,
	VirtualButtonEventHandler_Destroy_m9398BE0FDEFA3BF450D5956B5D2230EA7E88EF9B,
	VirtualButtonEventHandler_OnButtonPressed_m53723C5F46CC2FCAA177F99237F4C6928354B06B,
	VirtualButtonEventHandler_OnButtonReleased_mD55065DF0503DFFC6262A192AB181960E9F2C050,
	VirtualButtonEventHandler_SetVirtualButtonMaterial_m7284BF75B3FA488A6B220E9C1B6D9F8B5F7C6FFF,
	VirtualButtonEventHandler_DelayOnButtonReleasedEvent_m192D56CE9ACE6926285B39D5573B49D36D35A18D,
	VirtualButtonEventHandler__ctor_mFD75125B918ED11794AD29293ADFF29E529F795F,
	U3CDelayOnButtonReleasedEventU3Ed__11__ctor_m607B0E9390FEFF60ED66C6C189AA941DFE53F688,
	U3CDelayOnButtonReleasedEventU3Ed__11_System_IDisposable_Dispose_m061333F7F8B1F7D29D0D87B664A96A8942CD836C,
	U3CDelayOnButtonReleasedEventU3Ed__11_MoveNext_m24A67044776CEB32A7C5769903802C8F1ECA8ADF,
	U3CDelayOnButtonReleasedEventU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m498904243D7BC36EEBD67D3DBC8870869FAA444D,
	U3CDelayOnButtonReleasedEventU3Ed__11_System_Collections_IEnumerator_Reset_mEDB68124637C973F28B66CCF4195B7505CD9BA4E,
	U3CDelayOnButtonReleasedEventU3Ed__11_System_Collections_IEnumerator_get_Current_mE1A9569C2503720A0512AFE6DC53323847037B5B,
	AugmentationObject__ctor_m03DA2C19CB63048FE0F5B21734F3C7D6DA77F573,
	LookAtObject_Update_m5E7E6CE608A764F6B49B582C91C13B84591B6C5F,
	LookAtObject__ctor_m524A7F857015FC64C156E823BF81F69A630D3BD1,
	PanelShowHide_Hide_m886F5A206A397BF534195F77AAC2A6CD2344DDEA,
	PanelShowHide_Show_mAA11A92C09DAF58B17994DD1BC2D689E2DDDA961,
	PanelShowHide_ResetShowTrigger_mD9DC7BA5CAE2AC646571AE261C6858C66886B93B,
	PanelShowHide__ctor_m369B06B70CCE506AE90A807470805959C3D198E9,
	VuMarkContentUI_Awake_mB0F669C85EEB587B3CA191DD080C09AF3C329667,
	VuMarkContentUI_Update_m2C8B67D5ECC11305C209FF3708DD760A4FE89832,
	VuMarkContentUI_OnDestroy_m55D5896CBE892CE6E5073D9A1DF2D4B723ADC803,
	VuMarkContentUI_OnVuforiaStarted_m3B8A6ABC5EBD3E6097062F89C9AD4BB1498E227C,
	VuMarkContentUI_UpdateCanvasFadeAmount_mB3FCCB8C96F5579F4D5284067BC209D6ED4E574C,
	VuMarkContentUI_SetVuMarkInfoCanvas_m3D9A542C3966E7540777234FEAE267A1B73F428F,
	VuMarkContentUI__ctor_mB45CE8C58DB9387401C17BE801C5C45F56582DA9,
	VuMarkHandler_Start_m71A9964FF5BB5CF96C30C95784A629772A43E5ED,
	VuMarkHandler_OnVuforiaStarted_m56FFBE8CDD9E6084AD6E910E4B532969C81118DC,
	VuMarkHandler_OnVuforiaStopped_m62A8C9441825559C109EBD91EE01D479389B7812,
	VuMarkHandler_OnDestroy_m275EAB77AB2808FF8C366F87C41BB2F11EC18F50,
	VuMarkHandler_OnVuMarkFound_m272E2F13892B967D401D61E47FA63B60C524AB2A,
	VuMarkHandler_OnVuMarkLost_m9FA9E6FFA51ED816D13419F3CE7498B88B4B4FA3,
	VuMarkHandler_GetNumericVuMarkDescription_m1036920AB9A0C42972D23A6929181AF10A5BFB5E,
	VuMarkHandler_RetrieveStoredTextureForVuMarkTarget_mF6C7DADDFBE1DB854C9409F9E9481E888BDA144A,
	VuMarkHandler_GetVuMarkAugmentation_m8CB19FA2D1CAAE0F81164A510071005ED0C35655,
	VuMarkHandler_DestroyChildAugmentations_m43E6AE0ABCA859D7679F28C3B1109DCED90A7F79,
	VuMarkHandler__ctor_m6F1CF4132E7BBCE0C69201743D56A6D22300AD20,
	VuMarkOutline_Awake_mED5BB28E0E12ECA4B4C65D1A5B6F52C22AE98D82,
	VuMarkOutline_OnVuMarkFound_mAABCCFA74DAB3A691D11866FF59585F3FAFF4478,
	VuMarkOutline_OnVuMarkLost_mA576F6B5CEF7445F0A0C38FE2DE7342E04D3322C,
	VuMarkOutline_OnTargetStatusChanged_m9179A0DB8A28A0700E40EBD51C0870C5FD00BD4A,
	VuMarkOutline_UpdateLineRenderer_m1EF42FBABB496205F191D45F77E61D835BEC00DF,
	VuMarkOutline_SetupVuMarkBorderOutline_m9948BD1269138366196ABC6A65262A9ECBA94AC1,
	VuMarkOutline_SetBorderPositions_m0242E2F2832F64A329773955EC2CA2ABAA99BA74,
	VuMarkOutline__ctor_m77C6FFCEB3ECF05DC05BBD39A2B4B5220C27EF02,
	VuMarkUI_Start_m251EFA50CD7CA405CC3C0DE8BA816DD8F777F130,
	VuMarkUI_Update_m25CB39C4EF21523E68992F07ED5F4D508181E598,
	VuMarkUI_UpdateClosestTarget_mEFD66B662DFD231112BB5527F2D58D708A2CC077,
	VuMarkUI_ShowPanelAfter_mACDCB247DBF61B3EBB8BB4E36017C3BBEDCB9FA9,
	VuMarkUI__ctor_m3B105988B67EFFC156C6DE94EC7FC96260E8E487,
	U3CShowPanelAfterU3Ed__6__ctor_m06A477DBE347E475B6AD1F0F12E05E23CEE3DE0B,
	U3CShowPanelAfterU3Ed__6_System_IDisposable_Dispose_m4BA0A7083C5F11B60773C093AE665A448D14D956,
	U3CShowPanelAfterU3Ed__6_MoveNext_mAA38A2708F8FB6FC8124B2726AD02E092923FD93,
	U3CShowPanelAfterU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m004C1013489EEB9A74A6397A974DCB749F187C11,
	U3CShowPanelAfterU3Ed__6_System_Collections_IEnumerator_Reset_mD268BF011AA7E7CAFD487202E6459A9EAB0ECBD1,
	U3CShowPanelAfterU3Ed__6_System_Collections_IEnumerator_get_Current_m4940524F3D2E89946C1D8B8EEB316D6709C28BA1,
	VuMarkUtilities_GetVuMarkId_m9DADEE5A3FE502D35049BEACF7989731565465A6,
	VuMarkUtilities_GetVuMarkDataType_m434E0A5E05104BE16D0F96394C162BAE3CEC9F82,
	VuMarkUtilities_GenerateTextureFromVuMarkInstanceImage_m3C7D46B220FBA8E8D8A38852AB84A9CE3FBB8696,
};
static const int32_t s_InvokerIndices[252] = 
{
	2856,
	3595,
	2856,
	1264,
	830,
	3595,
	3595,
	3595,
	2842,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	5377,
	5160,
	4731,
	4731,
	4731,
	4731,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2797,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3533,
	2842,
	2856,
	2856,
	2856,
	2856,
	3595,
	3595,
	3595,
	2640,
	5396,
	3595,
	2080,
	3595,
	2797,
	2856,
	3595,
	3595,
	3595,
	3595,
	2842,
	2842,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2856,
	3595,
	3595,
	1587,
	3533,
	3595,
	2842,
	3595,
	3478,
	3533,
	3595,
	3533,
	3595,
	3595,
	2856,
	2797,
	2856,
	2797,
	3595,
	3478,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	1582,
	2842,
	1587,
	3595,
	3595,
	3595,
	3595,
	1587,
	3595,
	3478,
	3595,
	3595,
	3595,
	3595,
	2797,
	3595,
	3595,
	3595,
	1587,
	3595,
	3478,
	2797,
	3590,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2856,
	3595,
	3595,
	2797,
	2856,
	3595,
	4973,
	4973,
	5363,
	5363,
	3595,
	3595,
	3595,
	3595,
	650,
	5363,
	3595,
	3595,
	3595,
	3595,
	2797,
	3595,
	3595,
	2797,
	2797,
	3595,
	1582,
	2856,
	2856,
	2856,
	2856,
	3595,
	3595,
	3595,
	3595,
	1457,
	3595,
	3595,
	3595,
	3595,
	2842,
	2842,
	2797,
	2797,
	2856,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3478,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2856,
	2856,
	2856,
	2553,
	3595,
	2842,
	3595,
	3478,
	3533,
	3595,
	3533,
	3595,
	3595,
	3595,
	3595,
	638,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2856,
	3595,
	3595,
	3595,
	3595,
	3595,
	2856,
	2856,
	5160,
	5160,
	2550,
	2856,
	3595,
	3595,
	2856,
	2856,
	1587,
	2898,
	3595,
	2856,
	3595,
	3595,
	3595,
	3595,
	276,
	3595,
	2842,
	3595,
	3478,
	3533,
	3595,
	3533,
	5160,
	5160,
	5160,
};
extern const CustomAttributesCacheGenerator g_SamplesScripts_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_SamplesScripts_CodeGenModule;
const Il2CppCodeGenModule g_SamplesScripts_CodeGenModule = 
{
	"SamplesScripts.dll",
	252,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_SamplesScripts_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

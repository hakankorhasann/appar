﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.AssetBundle::.ctor()
extern void AssetBundle__ctor_mCE6DB7758AAD0EDDB044FC67C5BC7EC987BF3F71 (void);
// 0x00000002 UnityEngine.Object UnityEngine.AssetBundle::get_mainAsset()
extern void AssetBundle_get_mainAsset_mDAF39948DFB48C4C8DA324C11A6E7C9FF2F7F58A (void);
// 0x00000003 UnityEngine.Object UnityEngine.AssetBundle::returnMainAsset(UnityEngine.AssetBundle)
extern void AssetBundle_returnMainAsset_mEADFB842D3D38FDAFF6052F93E178302C0A41FD6 (void);
// 0x00000004 UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromMemory_Internal(System.Byte[],System.UInt32)
extern void AssetBundle_LoadFromMemory_Internal_mBF6BF6BFDE5DA4A76735766C969591424BB9649A (void);
// 0x00000005 UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromMemory(System.Byte[])
extern void AssetBundle_LoadFromMemory_m435A5FC269478E3E4809AB7355878B166D712E14 (void);
// 0x00000006 UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String)
extern void AssetBundle_LoadAsset_m598BB4E86B07C2BADAE6ED6FFBE5BB5E7A3392D5 (void);
// 0x00000007 T UnityEngine.AssetBundle::LoadAsset(System.String)
// 0x00000008 UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String,System.Type)
extern void AssetBundle_LoadAsset_m9139320F8B6D3E43B7D29AA7A60030306AE0A2C6 (void);
// 0x00000009 UnityEngine.Object UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)
extern void AssetBundle_LoadAsset_Internal_mFB165539087545C4B5763BA8B590D84318C6FE1B (void);
// 0x0000000A System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern void AssetBundle_Unload_m0DEBACB284F6CECA8DF21486D1BBE1189F6A5D66 (void);
// 0x0000000B System.String[] UnityEngine.AssetBundle::GetAllAssetNames()
extern void AssetBundle_GetAllAssetNames_m7012B92C4E0BDF3975EBC22DE2515AFEB0E9D409 (void);
static Il2CppMethodPointer s_methodPointers[11] = 
{
	AssetBundle__ctor_mCE6DB7758AAD0EDDB044FC67C5BC7EC987BF3F71,
	AssetBundle_get_mainAsset_mDAF39948DFB48C4C8DA324C11A6E7C9FF2F7F58A,
	AssetBundle_returnMainAsset_mEADFB842D3D38FDAFF6052F93E178302C0A41FD6,
	AssetBundle_LoadFromMemory_Internal_mBF6BF6BFDE5DA4A76735766C969591424BB9649A,
	AssetBundle_LoadFromMemory_m435A5FC269478E3E4809AB7355878B166D712E14,
	AssetBundle_LoadAsset_m598BB4E86B07C2BADAE6ED6FFBE5BB5E7A3392D5,
	NULL,
	AssetBundle_LoadAsset_m9139320F8B6D3E43B7D29AA7A60030306AE0A2C6,
	AssetBundle_LoadAsset_Internal_mFB165539087545C4B5763BA8B590D84318C6FE1B,
	AssetBundle_Unload_m0DEBACB284F6CECA8DF21486D1BBE1189F6A5D66,
	AssetBundle_GetAllAssetNames_m7012B92C4E0BDF3975EBC22DE2515AFEB0E9D409,
};
static const int32_t s_InvokerIndices[11] = 
{
	3595,
	3533,
	5160,
	4735,
	5160,
	2550,
	-1,
	1264,
	1264,
	2797,
	3533,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000007, { 0, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)1, 76 },
	{ (Il2CppRGCTXDataType)2, 76 },
};
extern const CustomAttributesCacheGenerator g_UnityEngine_AssetBundleModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_AssetBundleModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AssetBundleModule_CodeGenModule = 
{
	"UnityEngine.AssetBundleModule.dll",
	11,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
	g_UnityEngine_AssetBundleModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

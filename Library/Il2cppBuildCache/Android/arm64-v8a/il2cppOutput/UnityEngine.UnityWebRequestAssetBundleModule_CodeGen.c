﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAssetBundle::GetAssetBundle(System.String)
extern void UnityWebRequestAssetBundle_GetAssetBundle_m971810CE24C704BA4843A5835FAF5C537176FCD1 (void);
// 0x00000002 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAssetBundle::GetAssetBundle(System.String,System.UInt32)
extern void UnityWebRequestAssetBundle_GetAssetBundle_mE699AD7C26820C67DFE72328C4F519CB74CF5618 (void);
// 0x00000003 UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAssetBundle::GetAssetBundle(System.String,UnityEngine.CachedAssetBundle,System.UInt32)
extern void UnityWebRequestAssetBundle_GetAssetBundle_mBA370F920121A9D589BC6993777B9F8BFD4BF3EC (void);
// 0x00000004 System.IntPtr UnityEngine.Networking.DownloadHandlerAssetBundle::Create(UnityEngine.Networking.DownloadHandlerAssetBundle,System.String,System.UInt32)
extern void DownloadHandlerAssetBundle_Create_m816E2667DD5ABAAEFB0BC938824FA5341E41581B (void);
// 0x00000005 System.IntPtr UnityEngine.Networking.DownloadHandlerAssetBundle::CreateCached(UnityEngine.Networking.DownloadHandlerAssetBundle,System.String,System.String,UnityEngine.Hash128,System.UInt32)
extern void DownloadHandlerAssetBundle_CreateCached_mEC39E8C1DFB7894CDF139A3F9CDDA3E5B7B5F47F (void);
// 0x00000006 System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::InternalCreateAssetBundle(System.String,System.UInt32)
extern void DownloadHandlerAssetBundle_InternalCreateAssetBundle_m1D465F51ED31011F21CE0843354F7FAC076C43C4 (void);
// 0x00000007 System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::InternalCreateAssetBundleCached(System.String,System.String,UnityEngine.Hash128,System.UInt32)
extern void DownloadHandlerAssetBundle_InternalCreateAssetBundleCached_m2F4C9DD99E26350A7DC685CD918DE5792D9B7106 (void);
// 0x00000008 System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::.ctor(System.String,System.UInt32)
extern void DownloadHandlerAssetBundle__ctor_m791F6D9641819E85F7C2172BBFFBB105CCFC810B (void);
// 0x00000009 System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::.ctor(System.String,UnityEngine.CachedAssetBundle,System.UInt32)
extern void DownloadHandlerAssetBundle__ctor_m491425D31D005ED9B500067D776DE58FD1A41D6E (void);
// 0x0000000A System.Byte[] UnityEngine.Networking.DownloadHandlerAssetBundle::GetData()
extern void DownloadHandlerAssetBundle_GetData_m559C00EFAE6DAEFB753A247C6B842F32191923D3 (void);
// 0x0000000B UnityEngine.AssetBundle UnityEngine.Networking.DownloadHandlerAssetBundle::get_assetBundle()
extern void DownloadHandlerAssetBundle_get_assetBundle_m083E8230E8A5644AE6176135C86B4E1A6283190D (void);
// 0x0000000C UnityEngine.AssetBundle UnityEngine.Networking.DownloadHandlerAssetBundle::GetContent(UnityEngine.Networking.UnityWebRequest)
extern void DownloadHandlerAssetBundle_GetContent_m5FA159457BAE73F42B3FA6CEBC256608A346BA1C (void);
// 0x0000000D System.IntPtr UnityEngine.Networking.DownloadHandlerAssetBundle::CreateCached_Injected(UnityEngine.Networking.DownloadHandlerAssetBundle,System.String,System.String,UnityEngine.Hash128&,System.UInt32)
extern void DownloadHandlerAssetBundle_CreateCached_Injected_mB68C7D9BAAA3DC5B66CBBE661D7B33CEAB1A1963 (void);
static Il2CppMethodPointer s_methodPointers[13] = 
{
	UnityWebRequestAssetBundle_GetAssetBundle_m971810CE24C704BA4843A5835FAF5C537176FCD1,
	UnityWebRequestAssetBundle_GetAssetBundle_mE699AD7C26820C67DFE72328C4F519CB74CF5618,
	UnityWebRequestAssetBundle_GetAssetBundle_mBA370F920121A9D589BC6993777B9F8BFD4BF3EC,
	DownloadHandlerAssetBundle_Create_m816E2667DD5ABAAEFB0BC938824FA5341E41581B,
	DownloadHandlerAssetBundle_CreateCached_mEC39E8C1DFB7894CDF139A3F9CDDA3E5B7B5F47F,
	DownloadHandlerAssetBundle_InternalCreateAssetBundle_m1D465F51ED31011F21CE0843354F7FAC076C43C4,
	DownloadHandlerAssetBundle_InternalCreateAssetBundleCached_m2F4C9DD99E26350A7DC685CD918DE5792D9B7106,
	DownloadHandlerAssetBundle__ctor_m791F6D9641819E85F7C2172BBFFBB105CCFC810B,
	DownloadHandlerAssetBundle__ctor_m491425D31D005ED9B500067D776DE58FD1A41D6E,
	DownloadHandlerAssetBundle_GetData_m559C00EFAE6DAEFB753A247C6B842F32191923D3,
	DownloadHandlerAssetBundle_get_assetBundle_m083E8230E8A5644AE6176135C86B4E1A6283190D,
	DownloadHandlerAssetBundle_GetContent_m5FA159457BAE73F42B3FA6CEBC256608A346BA1C,
	DownloadHandlerAssetBundle_CreateCached_Injected_mB68C7D9BAAA3DC5B66CBBE661D7B33CEAB1A1963,
};
static const int32_t s_InvokerIndices[13] = 
{
	5160,
	4735,
	4371,
	4346,
	3903,
	1589,
	629,
	1589,
	945,
	3533,
	3533,
	5160,
	3902,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_UnityWebRequestAssetBundleModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestAssetBundleModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestAssetBundleModule_CodeGenModule = 
{
	"UnityEngine.UnityWebRequestAssetBundleModule.dll",
	13,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_UnityWebRequestAssetBundleModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

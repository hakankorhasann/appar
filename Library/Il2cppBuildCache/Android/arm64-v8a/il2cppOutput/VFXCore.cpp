﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Action`1<System.Boolean>
struct Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83;
// System.Action`1<Vuforia.ObserverBehaviour>
struct Action_1_t221A5ED598F56CFD324E6E493CD7F6F9BCF68775;
// System.Action`1<Vuforia.VuforiaEngineError>
struct Action_1_t9E70012332B31C37A3F555CFDAAD89E86C55B69E;
// System.Action`1<Vuforia.VuforiaInitError>
struct Action_1_tAA1D1EEFC680CD9A5C6DDD77BD612C0BBD43C306;
// System.Action`2<System.Object,Vuforia.TargetStatus>
struct Action_2_tEC03432E1591AF6C19EAE8E64F8502FBEEAAB87C;
// System.Action`2<Vuforia.ObserverBehaviour,Vuforia.TargetStatus>
struct Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4;
// System.Collections.Generic.List`1<Vuforia.VirtualButtonBehaviour>
struct List_1_t992A4FEC1847167339E43DBB7C36DBBF3688D9AF;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// VFX.DynamicColor[]
struct DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E;
// VFX.DynamicScalarProperty[]
struct DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE;
// VFX.DynamicVectorProperty[]
struct DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F;
// UnityEngine.Material[]
struct MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.Renderer[]
struct RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// VFX.StaticColor[]
struct StaticColorU5BU5D_tB852F8A761474DEA5054061DD1F8B267C56E6FF8;
// VFX.StaticScalarProperty[]
struct StaticScalarPropertyU5BU5D_t81263A8DC17430EDE9ED5D9BDC3D590C8D5555C9;
// VFX.StaticVectorProperty[]
struct StaticVectorPropertyU5BU5D_tFED79A0033EA7A3C25F29A20C8DDF49E6B945A36;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// Vuforia.AreaTargetBehaviour
struct AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787;
// Vuforia.Internal.Observers.AreaTargetObserver
struct AreaTargetObserver_t9CD4110DA64ABE157D3EFDCFEB9CA90C1B642D89;
// Vuforia.AreaTargetPreview
struct AreaTargetPreview_tFE128AA8F81FB158A0D30043FEF8E8F6F1B02291;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// VFX.CameraDepthMode
struct CameraDepthMode_t0B03DACDEA955D5DB48A8D455B28E8A945B45FC4;
// Vuforia.CameraDevice
struct CameraDevice_t5A659FEC1FF047A9D0BE5A49CE224EECCF96697F;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3;
// Vuforia.Internal.Observers.CylinderTargetObserver
struct CylinderTargetObserver_t1C0445916F65052B70F5BA7F78CB7888851939B1;
// Vuforia.CylinderTargetPreview
struct CylinderTargetPreview_t4B429587671F9E94F9A326DCBD68B191FE50BB7F;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// Vuforia.DevicePoseBehaviour
struct DevicePoseBehaviour_t228722CAB359AE9B3CB16CCE4DFA70DB951C35E1;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// Vuforia.Internal.Core.IEngine
struct IEngine_t708DCFFA8A0A96615D4AC23FF9621DA3A280AC3E;
// Vuforia.Internal.Observers.IObserver
struct IObserver_tDE0C8AF5F4AAABB85ACC09BC42BE7ABD6FCC961F;
// Vuforia.IObserverRegistry
struct IObserverRegistry_t87FE1BAE02D5523E9DF8B8C6B3E8A40556C01D4A;
// Vuforia.IRuntimeMeshBehaviour
struct IRuntimeMeshBehaviour_tB52E361F9A1E67F9A2B3F6068B30FD8890C8BB13;
// Vuforia.Image
struct Image_tEA90BDBB0131379427A1282935D21313F94DFDB1;
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34;
// Vuforia.Internal.Observers.ImageTargetObserver
struct ImageTargetObserver_t19BC37E4B4C30995FC064B94A719434AA250750F;
// Vuforia.ImageTargetPreview
struct ImageTargetPreview_tA100928CE44BCD50271453877087251759D7D2E2;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Vuforia.ModelTargetBehaviour
struct ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275;
// Vuforia.Internal.Observers.ModelTargetObserver
struct ModelTargetObserver_tA26329FCDA360C3E807B9DCF906995F775C132A0;
// Vuforia.ModelTargetPreview
struct ModelTargetPreview_t2AF671789D44F5710B5694062F0DD7D6FA5AD3C0;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// Vuforia.ObserverBehaviour
struct ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274;
// Vuforia.ObserverFactory
struct ObserverFactory_t8F8A436F3E3074002EAC9BC33F75BB80335496F5;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// VFX.ShaderVFX
struct ShaderVFX_t0CA406226914047B533232E406E31767AF23E763;
// System.String
struct String_t;
// VFX.TargetVFX
struct TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// Vuforia.VideoBackground
struct VideoBackground_tABAA05A8DC2011C5B98D09B8DF0C36853D585713;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Vuforia.VuMarkBehaviour
struct VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2;
// Vuforia.VuMarkInstanceId
struct VuMarkInstanceId_t5C200552393A64E29B6A074B462E7E5B71EE5DF5;
// Vuforia.VuMarkInstancesManager
struct VuMarkInstancesManager_t901C2384A8FD62E9C62E6D18B8ED116FB9EFBC1F;
// Vuforia.Internal.Observers.VuMarkObserver
struct VuMarkObserver_tD0936F103C0041228AD63B954C3E88815B03E479;
// Vuforia.VuMarkPreview
struct VuMarkPreview_t7745561F9348795064F637DD5D2E14D07F40E8E2;
// Vuforia.VuforiaApplication
struct VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407;
// Vuforia.World
struct World_tD34189E7DB459CEB10A1D3C0D7C94B25197AD14F;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;

IL2CPP_EXTERN_C RuntimeClass* Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Material_t8927C00353A72755313F046D0CE85178AE8218EE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VuforiaCameraUtil_t2170A5AB3BE377F2E409B7BAC5EB8663062E2275_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0B3C116817E94141082E7E559AE582C6E6A169FF;
IL2CPP_EXTERN_C String_t* _stringLiteral441BAC8B283322B34D840002AA78BE51874447BB;
IL2CPP_EXTERN_C String_t* _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE;
IL2CPP_EXTERN_C String_t* _stringLiteral54F48823060A5886D12558C4BBB0CDBB0E83F58A;
IL2CPP_EXTERN_C String_t* _stringLiteral787984D270B549500FD6EE450785085D7058DF70;
IL2CPP_EXTERN_C String_t* _stringLiteral79D0BA8792ECF6CF89F7E01C15E87AE01EF1D0A0;
IL2CPP_EXTERN_C String_t* _stringLiteral8ACFE282183F2EC454241D1009D4791A8AA31840;
IL2CPP_EXTERN_C String_t* _stringLiteralB2A2A23C0F95CC4BF23BDB0980D559F59E054C4B;
IL2CPP_EXTERN_C String_t* _stringLiteralF78A3EB3B3AD1250763ED45FCFB5C38360A66955;
IL2CPP_EXTERN_C const RuntimeMethod* Action_2__ctor_m6702F5465C5156B1FB05A678988FF9A80D94AE06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInParent_TisObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274_mCE544666A6285AE628139A24DDEF8131B0CAE6AA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mDF527A06E3315D1A7F828664F20B35662B0A52A6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mDFDC7E58FE89016C2A8E421AEA268944FC0F1FD6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TargetVFX_TargetStatusChanged_m4BCF8695AF732A32C4E4DC69A551DBAE1E6BB465_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E;
struct DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE;
struct DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F;
struct MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492;
struct RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7;
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
struct StaticColorU5BU5D_tB852F8A761474DEA5054061DD1F8B267C56E6FF8;
struct StaticScalarPropertyU5BU5D_t81263A8DC17430EDE9ED5D9BDC3D590C8D5555C9;
struct StaticVectorPropertyU5BU5D_tFED79A0033EA7A3C25F29A20C8DDF49E6B945A36;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t07DFF9780FD16F46B06E944A39C91F7D3CCA5DD2 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// VFX.CameraUtil
struct CameraUtil_tB53D789B77D42F2329FF916C67136F6DCBEC8C79  : public RuntimeObject
{
public:

public:
};


// VFX.MaterialUtil
struct MaterialUtil_tE66C7569D939D14C541339BE152511835748D995  : public RuntimeObject
{
public:

public:
};


// VFX.MatrixUtil
struct MatrixUtil_tBB6620D95B9FD32D5BCC64411D4A898857DAF486  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// VFX.VuforiaCameraUtil
struct VuforiaCameraUtil_t2170A5AB3BE377F2E409B7BAC5EB8663062E2275  : public RuntimeObject
{
public:

public:
};


// VFX.VuforiaObserverUtil
struct VuforiaObserverUtil_tDE67D0EBF2E511107478A74B8D524296E54E8C9D  : public RuntimeObject
{
public:

public:
};


// System.Nullable`1<System.Int32>
struct Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// VFX.DynamicScalarProperty
struct DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41 
{
public:
	// System.String VFX.DynamicScalarProperty::name
	String_t* ___name_0;
	// System.Single VFX.DynamicScalarProperty::startValue
	float ___startValue_1;
	// System.Single VFX.DynamicScalarProperty::endValue
	float ___endValue_2;
	// System.Single VFX.DynamicScalarProperty::delay
	float ___delay_3;
	// System.Single VFX.DynamicScalarProperty::duration
	float ___duration_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_startValue_1() { return static_cast<int32_t>(offsetof(DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41, ___startValue_1)); }
	inline float get_startValue_1() const { return ___startValue_1; }
	inline float* get_address_of_startValue_1() { return &___startValue_1; }
	inline void set_startValue_1(float value)
	{
		___startValue_1 = value;
	}

	inline static int32_t get_offset_of_endValue_2() { return static_cast<int32_t>(offsetof(DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41, ___endValue_2)); }
	inline float get_endValue_2() const { return ___endValue_2; }
	inline float* get_address_of_endValue_2() { return &___endValue_2; }
	inline void set_endValue_2(float value)
	{
		___endValue_2 = value;
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}
};

// Native definition for P/Invoke marshalling of VFX.DynamicScalarProperty
struct DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshaled_pinvoke
{
	char* ___name_0;
	float ___startValue_1;
	float ___endValue_2;
	float ___delay_3;
	float ___duration_4;
};
// Native definition for COM marshalling of VFX.DynamicScalarProperty
struct DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshaled_com
{
	Il2CppChar* ___name_0;
	float ___startValue_1;
	float ___endValue_2;
	float ___delay_3;
	float ___duration_4;
};

// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Mathf
struct Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194__padding[1];
	};

public:
};


// UnityEngine.Matrix4x4
struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};


// UnityEngine.Rect
struct Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// VFX.StaticScalarProperty
struct StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E 
{
public:
	// System.String VFX.StaticScalarProperty::name
	String_t* ___name_0;
	// System.Single VFX.StaticScalarProperty::value
	float ___value_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E, ___value_1)); }
	inline float get_value_1() const { return ___value_1; }
	inline float* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(float value)
	{
		___value_1 = value;
	}
};

// Native definition for P/Invoke marshalling of VFX.StaticScalarProperty
struct StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshaled_pinvoke
{
	char* ___name_0;
	float ___value_1;
};
// Native definition for COM marshalling of VFX.StaticScalarProperty
struct StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshaled_com
{
	Il2CppChar* ___name_0;
	float ___value_1;
};

// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.Bounds
struct Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Center_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Extents_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Extents_1 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.DepthTextureMode
struct DepthTextureMode_t4A8E08C41731918FB0D7CA5C6848E3864A0DC09A 
{
public:
	// System.Int32 UnityEngine.DepthTextureMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DepthTextureMode_t4A8E08C41731918FB0D7CA5C6848E3864A0DC09A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// VFX.DynamicColor
struct DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E 
{
public:
	// System.String VFX.DynamicColor::name
	String_t* ___name_0;
	// UnityEngine.Color VFX.DynamicColor::startColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___startColor_1;
	// UnityEngine.Color VFX.DynamicColor::endColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endColor_2;
	// System.Single VFX.DynamicColor::delay
	float ___delay_3;
	// System.Single VFX.DynamicColor::duration
	float ___duration_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_startColor_1() { return static_cast<int32_t>(offsetof(DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E, ___startColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_startColor_1() const { return ___startColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_startColor_1() { return &___startColor_1; }
	inline void set_startColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___startColor_1 = value;
	}

	inline static int32_t get_offset_of_endColor_2() { return static_cast<int32_t>(offsetof(DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E, ___endColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_endColor_2() const { return ___endColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_endColor_2() { return &___endColor_2; }
	inline void set_endColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___endColor_2 = value;
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}
};

// Native definition for P/Invoke marshalling of VFX.DynamicColor
struct DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshaled_pinvoke
{
	char* ___name_0;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___startColor_1;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endColor_2;
	float ___delay_3;
	float ___duration_4;
};
// Native definition for COM marshalling of VFX.DynamicColor
struct DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshaled_com
{
	Il2CppChar* ___name_0;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___startColor_1;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endColor_2;
	float ___delay_3;
	float ___duration_4;
};

// VFX.DynamicVectorProperty
struct DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE 
{
public:
	// System.String VFX.DynamicVectorProperty::name
	String_t* ___name_0;
	// UnityEngine.Vector3 VFX.DynamicVectorProperty::startValue
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___startValue_1;
	// UnityEngine.Vector3 VFX.DynamicVectorProperty::endValue
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValue_2;
	// System.Single VFX.DynamicVectorProperty::delay
	float ___delay_3;
	// System.Single VFX.DynamicVectorProperty::duration
	float ___duration_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_startValue_1() { return static_cast<int32_t>(offsetof(DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE, ___startValue_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_startValue_1() const { return ___startValue_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_startValue_1() { return &___startValue_1; }
	inline void set_startValue_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___startValue_1 = value;
	}

	inline static int32_t get_offset_of_endValue_2() { return static_cast<int32_t>(offsetof(DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE, ___endValue_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_endValue_2() const { return ___endValue_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_endValue_2() { return &___endValue_2; }
	inline void set_endValue_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___endValue_2 = value;
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}
};

// Native definition for P/Invoke marshalling of VFX.DynamicVectorProperty
struct DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshaled_pinvoke
{
	char* ___name_0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___startValue_1;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValue_2;
	float ___delay_3;
	float ___duration_4;
};
// Native definition for COM marshalling of VFX.DynamicVectorProperty
struct DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshaled_com
{
	Il2CppChar* ___name_0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___startValue_1;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValue_2;
	float ___delay_3;
	float ___duration_4;
};

// VFX.FilePathMode
struct FilePathMode_t152EE606BBBC1BF53246294B3BFBE819D2C104BB 
{
public:
	// System.Int32 VFX.FilePathMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilePathMode_t152EE606BBBC1BF53246294B3BFBE819D2C104BB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.ImageTargetType
struct ImageTargetType_t661C350C5700751D0D48A013E8A2410ACDEB4016 
{
public:
	// System.Int32 Vuforia.ImageTargetType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImageTargetType_t661C350C5700751D0D48A013E8A2410ACDEB4016, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.InstanceIdType
struct InstanceIdType_t06C7820219EB8F3B808C53E7E2D5314CFA391038 
{
public:
	// System.Int32 Vuforia.InstanceIdType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InstanceIdType_t06C7820219EB8F3B808C53E7E2D5314CFA391038, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.Internal.ModelTargetTrackingMode
struct ModelTargetTrackingMode_t45084A9081C04D568FB4F614A0412DBF4F183AF0 
{
public:
	// System.Int32 Vuforia.Internal.ModelTargetTrackingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModelTargetTrackingMode_t45084A9081C04D568FB4F614A0412DBF4F183AF0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// VFX.StaticColor
struct StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F 
{
public:
	// System.String VFX.StaticColor::name
	String_t* ___name_0;
	// UnityEngine.Color VFX.StaticColor::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_color_1() { return static_cast<int32_t>(offsetof(StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F, ___color_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_1() const { return ___color_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_1() { return &___color_1; }
	inline void set_color_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_1 = value;
	}
};

// Native definition for P/Invoke marshalling of VFX.StaticColor
struct StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshaled_pinvoke
{
	char* ___name_0;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_1;
};
// Native definition for COM marshalling of VFX.StaticColor
struct StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshaled_com
{
	Il2CppChar* ___name_0;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_1;
};

// VFX.StaticVectorProperty
struct StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE 
{
public:
	// System.String VFX.StaticVectorProperty::name
	String_t* ___name_0;
	// UnityEngine.Vector3 VFX.StaticVectorProperty::value
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE, ___value_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_value_1() const { return ___value_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___value_1 = value;
	}
};

// Native definition for P/Invoke marshalling of VFX.StaticVectorProperty
struct StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshaled_pinvoke
{
	char* ___name_0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value_1;
};
// Native definition for COM marshalling of VFX.StaticVectorProperty
struct StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshaled_com
{
	Il2CppChar* ___name_0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value_1;
};

// Vuforia.Status
struct Status_t507773DC9329777DFF23BBE12880E1A5331F8826 
{
public:
	// System.Int32 Vuforia.Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t507773DC9329777DFF23BBE12880E1A5331F8826, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.StatusInfo
struct StatusInfo_t2D3913E705BD464CB7101343C8E44C3140AAEBFD 
{
public:
	// System.Int32 Vuforia.StatusInfo::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StatusInfo_t2D3913E705BD464CB7101343C8E44C3140AAEBFD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.StorageType
struct StorageType_t227CA13679D919374D61F656AAA142A5A72E2B14 
{
public:
	// System.Int32 Vuforia.StorageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StorageType_t227CA13679D919374D61F656AAA142A5A72E2B14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.Internal.TargetMotionHint
struct TargetMotionHint_tD9D2E0D1A9C9F1C72C90B3F4A635203991D7DBA6 
{
public:
	// System.Int32 Vuforia.Internal.TargetMotionHint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TargetMotionHint_tD9D2E0D1A9C9F1C72C90B3F4A635203991D7DBA6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.TrackingOptimization
struct TrackingOptimization_t6158BE46D8A80EE93C68BAC235BBB406D0F3B326 
{
public:
	// System.Int32 Vuforia.TrackingOptimization::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingOptimization_t6158BE46D8A80EE93C68BAC235BBB406D0F3B326, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// VFX.TransitionMode
struct TransitionMode_t342D36A3FD733F8BBEEE7071B4950B5E18630E80 
{
public:
	// System.Int32 VFX.TransitionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransitionMode_t342D36A3FD733F8BBEEE7071B4950B5E18630E80, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// VFX.VFXCenterMode
struct VFXCenterMode_t8AADBFFB257A909CE6A887F1641DD4A6B696635C 
{
public:
	// System.Int32 VFX.VFXCenterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VFXCenterMode_t8AADBFFB257A909CE6A887F1641DD4A6B696635C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.VuforiaInitError
struct VuforiaInitError_t8EDBA61A511F9B1518C1EA806C4769BA8E788774 
{
public:
	// System.Int32 Vuforia.VuforiaInitError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VuforiaInitError_t8EDBA61A511F9B1518C1EA806C4769BA8E788774, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.WorldCenterMode
struct WorldCenterMode_t849D0D9F3EBCAE808E7E9757919CE34031A15587 
{
public:
	// System.Int32 Vuforia.WorldCenterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WorldCenterMode_t849D0D9F3EBCAE808E7E9757919CE34031A15587, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.ModelTargetBehaviour/GuideViewDisplayMode
struct GuideViewDisplayMode_tBFF254723E24000128A231D541A5EA00CEF31EA9 
{
public:
	// System.Int32 Vuforia.ModelTargetBehaviour/GuideViewDisplayMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GuideViewDisplayMode_tBFF254723E24000128A231D541A5EA00CEF31EA9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// Vuforia.TargetStatus
struct TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1 
{
public:
	// Vuforia.Status Vuforia.TargetStatus::<Status>k__BackingField
	int32_t ___U3CStatusU3Ek__BackingField_0;
	// Vuforia.StatusInfo Vuforia.TargetStatus::<StatusInfo>k__BackingField
	int32_t ___U3CStatusInfoU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CStatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1, ___U3CStatusU3Ek__BackingField_0)); }
	inline int32_t get_U3CStatusU3Ek__BackingField_0() const { return ___U3CStatusU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CStatusU3Ek__BackingField_0() { return &___U3CStatusU3Ek__BackingField_0; }
	inline void set_U3CStatusU3Ek__BackingField_0(int32_t value)
	{
		___U3CStatusU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStatusInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1, ___U3CStatusInfoU3Ek__BackingField_1)); }
	inline int32_t get_U3CStatusInfoU3Ek__BackingField_1() const { return ___U3CStatusInfoU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CStatusInfoU3Ek__BackingField_1() { return &___U3CStatusInfoU3Ek__BackingField_1; }
	inline void set_U3CStatusInfoU3Ek__BackingField_1(int32_t value)
	{
		___U3CStatusInfoU3Ek__BackingField_1 = value;
	}
};


// Vuforia.VuforiaApplication
struct VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1  : public RuntimeObject
{
public:
	// Vuforia.VuforiaInitError Vuforia.VuforiaApplication::mInitError
	int32_t ___mInitError_1;
	// System.Action Vuforia.VuforiaApplication::mOnBeforeVuforiaInitialized
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___mOnBeforeVuforiaInitialized_2;
	// System.Action`1<Vuforia.VuforiaInitError> Vuforia.VuforiaApplication::mOnVuforiaInitialized
	Action_1_tAA1D1EEFC680CD9A5C6DDD77BD612C0BBD43C306 * ___mOnVuforiaInitialized_3;
	// System.Action Vuforia.VuforiaApplication::mOnVuforiaStarted
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___mOnVuforiaStarted_4;
	// System.Action`1<Vuforia.VuforiaEngineError> Vuforia.VuforiaApplication::OnVuforiaError
	Action_1_t9E70012332B31C37A3F555CFDAAD89E86C55B69E * ___OnVuforiaError_5;
	// System.Action`1<System.Boolean> Vuforia.VuforiaApplication::OnVuforiaPaused
	Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83 * ___OnVuforiaPaused_6;
	// System.Action Vuforia.VuforiaApplication::OnVuforiaStopped
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___OnVuforiaStopped_7;
	// System.Action Vuforia.VuforiaApplication::OnVuforiaDeinitialized
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___OnVuforiaDeinitialized_8;

public:
	inline static int32_t get_offset_of_mInitError_1() { return static_cast<int32_t>(offsetof(VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1, ___mInitError_1)); }
	inline int32_t get_mInitError_1() const { return ___mInitError_1; }
	inline int32_t* get_address_of_mInitError_1() { return &___mInitError_1; }
	inline void set_mInitError_1(int32_t value)
	{
		___mInitError_1 = value;
	}

	inline static int32_t get_offset_of_mOnBeforeVuforiaInitialized_2() { return static_cast<int32_t>(offsetof(VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1, ___mOnBeforeVuforiaInitialized_2)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_mOnBeforeVuforiaInitialized_2() const { return ___mOnBeforeVuforiaInitialized_2; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_mOnBeforeVuforiaInitialized_2() { return &___mOnBeforeVuforiaInitialized_2; }
	inline void set_mOnBeforeVuforiaInitialized_2(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___mOnBeforeVuforiaInitialized_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mOnBeforeVuforiaInitialized_2), (void*)value);
	}

	inline static int32_t get_offset_of_mOnVuforiaInitialized_3() { return static_cast<int32_t>(offsetof(VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1, ___mOnVuforiaInitialized_3)); }
	inline Action_1_tAA1D1EEFC680CD9A5C6DDD77BD612C0BBD43C306 * get_mOnVuforiaInitialized_3() const { return ___mOnVuforiaInitialized_3; }
	inline Action_1_tAA1D1EEFC680CD9A5C6DDD77BD612C0BBD43C306 ** get_address_of_mOnVuforiaInitialized_3() { return &___mOnVuforiaInitialized_3; }
	inline void set_mOnVuforiaInitialized_3(Action_1_tAA1D1EEFC680CD9A5C6DDD77BD612C0BBD43C306 * value)
	{
		___mOnVuforiaInitialized_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mOnVuforiaInitialized_3), (void*)value);
	}

	inline static int32_t get_offset_of_mOnVuforiaStarted_4() { return static_cast<int32_t>(offsetof(VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1, ___mOnVuforiaStarted_4)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_mOnVuforiaStarted_4() const { return ___mOnVuforiaStarted_4; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_mOnVuforiaStarted_4() { return &___mOnVuforiaStarted_4; }
	inline void set_mOnVuforiaStarted_4(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___mOnVuforiaStarted_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mOnVuforiaStarted_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnVuforiaError_5() { return static_cast<int32_t>(offsetof(VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1, ___OnVuforiaError_5)); }
	inline Action_1_t9E70012332B31C37A3F555CFDAAD89E86C55B69E * get_OnVuforiaError_5() const { return ___OnVuforiaError_5; }
	inline Action_1_t9E70012332B31C37A3F555CFDAAD89E86C55B69E ** get_address_of_OnVuforiaError_5() { return &___OnVuforiaError_5; }
	inline void set_OnVuforiaError_5(Action_1_t9E70012332B31C37A3F555CFDAAD89E86C55B69E * value)
	{
		___OnVuforiaError_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnVuforiaError_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnVuforiaPaused_6() { return static_cast<int32_t>(offsetof(VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1, ___OnVuforiaPaused_6)); }
	inline Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83 * get_OnVuforiaPaused_6() const { return ___OnVuforiaPaused_6; }
	inline Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83 ** get_address_of_OnVuforiaPaused_6() { return &___OnVuforiaPaused_6; }
	inline void set_OnVuforiaPaused_6(Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83 * value)
	{
		___OnVuforiaPaused_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnVuforiaPaused_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnVuforiaStopped_7() { return static_cast<int32_t>(offsetof(VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1, ___OnVuforiaStopped_7)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_OnVuforiaStopped_7() const { return ___OnVuforiaStopped_7; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_OnVuforiaStopped_7() { return &___OnVuforiaStopped_7; }
	inline void set_OnVuforiaStopped_7(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___OnVuforiaStopped_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnVuforiaStopped_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnVuforiaDeinitialized_8() { return static_cast<int32_t>(offsetof(VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1, ___OnVuforiaDeinitialized_8)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_OnVuforiaDeinitialized_8() const { return ___OnVuforiaDeinitialized_8; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_OnVuforiaDeinitialized_8() { return &___OnVuforiaDeinitialized_8; }
	inline void set_OnVuforiaDeinitialized_8(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___OnVuforiaDeinitialized_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnVuforiaDeinitialized_8), (void*)value);
	}
};


// System.Action`2<Vuforia.ObserverBehaviour,Vuforia.TargetStatus>
struct Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// VFX.CameraDepthMode
struct CameraDepthMode_t0B03DACDEA955D5DB48A8D455B28E8A945B45FC4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.DepthTextureMode VFX.CameraDepthMode::depthMode
	int32_t ___depthMode_4;

public:
	inline static int32_t get_offset_of_depthMode_4() { return static_cast<int32_t>(offsetof(CameraDepthMode_t0B03DACDEA955D5DB48A8D455B28E8A945B45FC4, ___depthMode_4)); }
	inline int32_t get_depthMode_4() const { return ___depthMode_4; }
	inline int32_t* get_address_of_depthMode_4() { return &___depthMode_4; }
	inline void set_depthMode_4(int32_t value)
	{
		___depthMode_4 = value;
	}
};


// VFX.ShaderVFX
struct ShaderVFX_t0CA406226914047B533232E406E31767AF23E763  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject VFX.ShaderVFX::TargetObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___TargetObject_4;
	// UnityEngine.Material VFX.ShaderVFX::EffectMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___EffectMaterial_5;
	// System.Boolean VFX.ShaderVFX::PlayOnAwake
	bool ___PlayOnAwake_6;
	// System.Boolean VFX.ShaderVFX::Loop
	bool ___Loop_7;
	// System.Boolean VFX.ShaderVFX::AnimationAutoRewind
	bool ___AnimationAutoRewind_8;
	// VFX.TransitionMode VFX.ShaderVFX::TransitionMode
	int32_t ___TransitionMode_9;
	// VFX.StaticScalarProperty[] VFX.ShaderVFX::StaticScalarProperties
	StaticScalarPropertyU5BU5D_t81263A8DC17430EDE9ED5D9BDC3D590C8D5555C9* ___StaticScalarProperties_10;
	// VFX.StaticVectorProperty[] VFX.ShaderVFX::StaticVectorProperties
	StaticVectorPropertyU5BU5D_tFED79A0033EA7A3C25F29A20C8DDF49E6B945A36* ___StaticVectorProperties_11;
	// VFX.StaticColor[] VFX.ShaderVFX::StaticColors
	StaticColorU5BU5D_tB852F8A761474DEA5054061DD1F8B267C56E6FF8* ___StaticColors_12;
	// VFX.DynamicScalarProperty[] VFX.ShaderVFX::DynamicScalarProperties
	DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE* ___DynamicScalarProperties_13;
	// VFX.DynamicVectorProperty[] VFX.ShaderVFX::DynamicVectorProperties
	DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F* ___DynamicVectorProperties_14;
	// VFX.DynamicColor[] VFX.ShaderVFX::DynamicColors
	DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E* ___DynamicColors_15;
	// UnityEngine.Events.UnityEvent VFX.ShaderVFX::OnEffectStarted
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnEffectStarted_16;
	// UnityEngine.Events.UnityEvent VFX.ShaderVFX::OnReachedEnd
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnReachedEnd_17;
	// UnityEngine.Material VFX.ShaderVFX::mSharedMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___mSharedMaterial_18;
	// System.Single VFX.ShaderVFX::mAnimationTime
	float ___mAnimationTime_19;
	// System.Boolean VFX.ShaderVFX::mIsPlaying
	bool ___mIsPlaying_20;

public:
	inline static int32_t get_offset_of_TargetObject_4() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___TargetObject_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_TargetObject_4() const { return ___TargetObject_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_TargetObject_4() { return &___TargetObject_4; }
	inline void set_TargetObject_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___TargetObject_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetObject_4), (void*)value);
	}

	inline static int32_t get_offset_of_EffectMaterial_5() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___EffectMaterial_5)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_EffectMaterial_5() const { return ___EffectMaterial_5; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_EffectMaterial_5() { return &___EffectMaterial_5; }
	inline void set_EffectMaterial_5(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___EffectMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EffectMaterial_5), (void*)value);
	}

	inline static int32_t get_offset_of_PlayOnAwake_6() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___PlayOnAwake_6)); }
	inline bool get_PlayOnAwake_6() const { return ___PlayOnAwake_6; }
	inline bool* get_address_of_PlayOnAwake_6() { return &___PlayOnAwake_6; }
	inline void set_PlayOnAwake_6(bool value)
	{
		___PlayOnAwake_6 = value;
	}

	inline static int32_t get_offset_of_Loop_7() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___Loop_7)); }
	inline bool get_Loop_7() const { return ___Loop_7; }
	inline bool* get_address_of_Loop_7() { return &___Loop_7; }
	inline void set_Loop_7(bool value)
	{
		___Loop_7 = value;
	}

	inline static int32_t get_offset_of_AnimationAutoRewind_8() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___AnimationAutoRewind_8)); }
	inline bool get_AnimationAutoRewind_8() const { return ___AnimationAutoRewind_8; }
	inline bool* get_address_of_AnimationAutoRewind_8() { return &___AnimationAutoRewind_8; }
	inline void set_AnimationAutoRewind_8(bool value)
	{
		___AnimationAutoRewind_8 = value;
	}

	inline static int32_t get_offset_of_TransitionMode_9() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___TransitionMode_9)); }
	inline int32_t get_TransitionMode_9() const { return ___TransitionMode_9; }
	inline int32_t* get_address_of_TransitionMode_9() { return &___TransitionMode_9; }
	inline void set_TransitionMode_9(int32_t value)
	{
		___TransitionMode_9 = value;
	}

	inline static int32_t get_offset_of_StaticScalarProperties_10() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___StaticScalarProperties_10)); }
	inline StaticScalarPropertyU5BU5D_t81263A8DC17430EDE9ED5D9BDC3D590C8D5555C9* get_StaticScalarProperties_10() const { return ___StaticScalarProperties_10; }
	inline StaticScalarPropertyU5BU5D_t81263A8DC17430EDE9ED5D9BDC3D590C8D5555C9** get_address_of_StaticScalarProperties_10() { return &___StaticScalarProperties_10; }
	inline void set_StaticScalarProperties_10(StaticScalarPropertyU5BU5D_t81263A8DC17430EDE9ED5D9BDC3D590C8D5555C9* value)
	{
		___StaticScalarProperties_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StaticScalarProperties_10), (void*)value);
	}

	inline static int32_t get_offset_of_StaticVectorProperties_11() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___StaticVectorProperties_11)); }
	inline StaticVectorPropertyU5BU5D_tFED79A0033EA7A3C25F29A20C8DDF49E6B945A36* get_StaticVectorProperties_11() const { return ___StaticVectorProperties_11; }
	inline StaticVectorPropertyU5BU5D_tFED79A0033EA7A3C25F29A20C8DDF49E6B945A36** get_address_of_StaticVectorProperties_11() { return &___StaticVectorProperties_11; }
	inline void set_StaticVectorProperties_11(StaticVectorPropertyU5BU5D_tFED79A0033EA7A3C25F29A20C8DDF49E6B945A36* value)
	{
		___StaticVectorProperties_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StaticVectorProperties_11), (void*)value);
	}

	inline static int32_t get_offset_of_StaticColors_12() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___StaticColors_12)); }
	inline StaticColorU5BU5D_tB852F8A761474DEA5054061DD1F8B267C56E6FF8* get_StaticColors_12() const { return ___StaticColors_12; }
	inline StaticColorU5BU5D_tB852F8A761474DEA5054061DD1F8B267C56E6FF8** get_address_of_StaticColors_12() { return &___StaticColors_12; }
	inline void set_StaticColors_12(StaticColorU5BU5D_tB852F8A761474DEA5054061DD1F8B267C56E6FF8* value)
	{
		___StaticColors_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StaticColors_12), (void*)value);
	}

	inline static int32_t get_offset_of_DynamicScalarProperties_13() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___DynamicScalarProperties_13)); }
	inline DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE* get_DynamicScalarProperties_13() const { return ___DynamicScalarProperties_13; }
	inline DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE** get_address_of_DynamicScalarProperties_13() { return &___DynamicScalarProperties_13; }
	inline void set_DynamicScalarProperties_13(DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE* value)
	{
		___DynamicScalarProperties_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DynamicScalarProperties_13), (void*)value);
	}

	inline static int32_t get_offset_of_DynamicVectorProperties_14() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___DynamicVectorProperties_14)); }
	inline DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F* get_DynamicVectorProperties_14() const { return ___DynamicVectorProperties_14; }
	inline DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F** get_address_of_DynamicVectorProperties_14() { return &___DynamicVectorProperties_14; }
	inline void set_DynamicVectorProperties_14(DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F* value)
	{
		___DynamicVectorProperties_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DynamicVectorProperties_14), (void*)value);
	}

	inline static int32_t get_offset_of_DynamicColors_15() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___DynamicColors_15)); }
	inline DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E* get_DynamicColors_15() const { return ___DynamicColors_15; }
	inline DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E** get_address_of_DynamicColors_15() { return &___DynamicColors_15; }
	inline void set_DynamicColors_15(DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E* value)
	{
		___DynamicColors_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DynamicColors_15), (void*)value);
	}

	inline static int32_t get_offset_of_OnEffectStarted_16() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___OnEffectStarted_16)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnEffectStarted_16() const { return ___OnEffectStarted_16; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnEffectStarted_16() { return &___OnEffectStarted_16; }
	inline void set_OnEffectStarted_16(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnEffectStarted_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnEffectStarted_16), (void*)value);
	}

	inline static int32_t get_offset_of_OnReachedEnd_17() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___OnReachedEnd_17)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnReachedEnd_17() const { return ___OnReachedEnd_17; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnReachedEnd_17() { return &___OnReachedEnd_17; }
	inline void set_OnReachedEnd_17(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnReachedEnd_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnReachedEnd_17), (void*)value);
	}

	inline static int32_t get_offset_of_mSharedMaterial_18() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___mSharedMaterial_18)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_mSharedMaterial_18() const { return ___mSharedMaterial_18; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_mSharedMaterial_18() { return &___mSharedMaterial_18; }
	inline void set_mSharedMaterial_18(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___mSharedMaterial_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mSharedMaterial_18), (void*)value);
	}

	inline static int32_t get_offset_of_mAnimationTime_19() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___mAnimationTime_19)); }
	inline float get_mAnimationTime_19() const { return ___mAnimationTime_19; }
	inline float* get_address_of_mAnimationTime_19() { return &___mAnimationTime_19; }
	inline void set_mAnimationTime_19(float value)
	{
		___mAnimationTime_19 = value;
	}

	inline static int32_t get_offset_of_mIsPlaying_20() { return static_cast<int32_t>(offsetof(ShaderVFX_t0CA406226914047B533232E406E31767AF23E763, ___mIsPlaying_20)); }
	inline bool get_mIsPlaying_20() const { return ___mIsPlaying_20; }
	inline bool* get_address_of_mIsPlaying_20() { return &___mIsPlaying_20; }
	inline void set_mIsPlaying_20(bool value)
	{
		___mIsPlaying_20 = value;
	}
};


// Vuforia.VuforiaMonoBehaviour
struct VuforiaMonoBehaviour_t172ED03DD8996A3B90FCEDB8A62EB98DBDCB0932  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Vuforia.ObserverBehaviour
struct ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274  : public VuforiaMonoBehaviour_t172ED03DD8996A3B90FCEDB8A62EB98DBDCB0932
{
public:
	// System.Boolean Vuforia.ObserverBehaviour::PreviewVisible
	bool ___PreviewVisible_4;
	// System.Boolean Vuforia.ObserverBehaviour::RuntimeOcclusion
	bool ___RuntimeOcclusion_5;
	// System.Boolean Vuforia.ObserverBehaviour::RuntimeCollider
	bool ___RuntimeCollider_6;
	// System.String Vuforia.ObserverBehaviour::mTrackableName
	String_t* ___mTrackableName_7;
	// System.Boolean Vuforia.ObserverBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_8;
	// System.Action`2<Vuforia.ObserverBehaviour,Vuforia.TargetStatus> Vuforia.ObserverBehaviour::OnTargetStatusChanged
	Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4 * ___OnTargetStatusChanged_9;
	// System.Action`1<Vuforia.ObserverBehaviour> Vuforia.ObserverBehaviour::OnBehaviourDestroyed
	Action_1_t221A5ED598F56CFD324E6E493CD7F6F9BCF68775 * ___OnBehaviourDestroyed_10;
	// Vuforia.Internal.Observers.IObserver Vuforia.ObserverBehaviour::mObserver
	RuntimeObject* ___mObserver_11;
	// Vuforia.TargetStatus Vuforia.ObserverBehaviour::<TargetStatus>k__BackingField
	TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  ___U3CTargetStatusU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_PreviewVisible_4() { return static_cast<int32_t>(offsetof(ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274, ___PreviewVisible_4)); }
	inline bool get_PreviewVisible_4() const { return ___PreviewVisible_4; }
	inline bool* get_address_of_PreviewVisible_4() { return &___PreviewVisible_4; }
	inline void set_PreviewVisible_4(bool value)
	{
		___PreviewVisible_4 = value;
	}

	inline static int32_t get_offset_of_RuntimeOcclusion_5() { return static_cast<int32_t>(offsetof(ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274, ___RuntimeOcclusion_5)); }
	inline bool get_RuntimeOcclusion_5() const { return ___RuntimeOcclusion_5; }
	inline bool* get_address_of_RuntimeOcclusion_5() { return &___RuntimeOcclusion_5; }
	inline void set_RuntimeOcclusion_5(bool value)
	{
		___RuntimeOcclusion_5 = value;
	}

	inline static int32_t get_offset_of_RuntimeCollider_6() { return static_cast<int32_t>(offsetof(ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274, ___RuntimeCollider_6)); }
	inline bool get_RuntimeCollider_6() const { return ___RuntimeCollider_6; }
	inline bool* get_address_of_RuntimeCollider_6() { return &___RuntimeCollider_6; }
	inline void set_RuntimeCollider_6(bool value)
	{
		___RuntimeCollider_6 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_7() { return static_cast<int32_t>(offsetof(ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274, ___mTrackableName_7)); }
	inline String_t* get_mTrackableName_7() const { return ___mTrackableName_7; }
	inline String_t** get_address_of_mTrackableName_7() { return &___mTrackableName_7; }
	inline void set_mTrackableName_7(String_t* value)
	{
		___mTrackableName_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mTrackableName_7), (void*)value);
	}

	inline static int32_t get_offset_of_mInitializedInEditor_8() { return static_cast<int32_t>(offsetof(ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274, ___mInitializedInEditor_8)); }
	inline bool get_mInitializedInEditor_8() const { return ___mInitializedInEditor_8; }
	inline bool* get_address_of_mInitializedInEditor_8() { return &___mInitializedInEditor_8; }
	inline void set_mInitializedInEditor_8(bool value)
	{
		___mInitializedInEditor_8 = value;
	}

	inline static int32_t get_offset_of_OnTargetStatusChanged_9() { return static_cast<int32_t>(offsetof(ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274, ___OnTargetStatusChanged_9)); }
	inline Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4 * get_OnTargetStatusChanged_9() const { return ___OnTargetStatusChanged_9; }
	inline Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4 ** get_address_of_OnTargetStatusChanged_9() { return &___OnTargetStatusChanged_9; }
	inline void set_OnTargetStatusChanged_9(Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4 * value)
	{
		___OnTargetStatusChanged_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTargetStatusChanged_9), (void*)value);
	}

	inline static int32_t get_offset_of_OnBehaviourDestroyed_10() { return static_cast<int32_t>(offsetof(ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274, ___OnBehaviourDestroyed_10)); }
	inline Action_1_t221A5ED598F56CFD324E6E493CD7F6F9BCF68775 * get_OnBehaviourDestroyed_10() const { return ___OnBehaviourDestroyed_10; }
	inline Action_1_t221A5ED598F56CFD324E6E493CD7F6F9BCF68775 ** get_address_of_OnBehaviourDestroyed_10() { return &___OnBehaviourDestroyed_10; }
	inline void set_OnBehaviourDestroyed_10(Action_1_t221A5ED598F56CFD324E6E493CD7F6F9BCF68775 * value)
	{
		___OnBehaviourDestroyed_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnBehaviourDestroyed_10), (void*)value);
	}

	inline static int32_t get_offset_of_mObserver_11() { return static_cast<int32_t>(offsetof(ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274, ___mObserver_11)); }
	inline RuntimeObject* get_mObserver_11() const { return ___mObserver_11; }
	inline RuntimeObject** get_address_of_mObserver_11() { return &___mObserver_11; }
	inline void set_mObserver_11(RuntimeObject* value)
	{
		___mObserver_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mObserver_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTargetStatusU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274, ___U3CTargetStatusU3Ek__BackingField_12)); }
	inline TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  get_U3CTargetStatusU3Ek__BackingField_12() const { return ___U3CTargetStatusU3Ek__BackingField_12; }
	inline TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1 * get_address_of_U3CTargetStatusU3Ek__BackingField_12() { return &___U3CTargetStatusU3Ek__BackingField_12; }
	inline void set_U3CTargetStatusU3Ek__BackingField_12(TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  value)
	{
		___U3CTargetStatusU3Ek__BackingField_12 = value;
	}
};


// VFX.TargetVFX
struct TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36  : public ShaderVFX_t0CA406226914047B533232E406E31767AF23E763
{
public:
	// Vuforia.ObserverBehaviour VFX.TargetVFX::Observer
	ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * ___Observer_21;
	// System.Boolean VFX.TargetVFX::IsTargetShownOnTargetFound
	bool ___IsTargetShownOnTargetFound_22;
	// System.Single VFX.TargetVFX::ShowDelay
	float ___ShowDelay_23;
	// System.String VFX.TargetVFX::ShaderCenter
	String_t* ___ShaderCenter_24;
	// System.String VFX.TargetVFX::ShaderAxisX
	String_t* ___ShaderAxisX_25;
	// System.String VFX.TargetVFX::ShaderAxisY
	String_t* ___ShaderAxisY_26;
	// System.String VFX.TargetVFX::ShaderAxisZ
	String_t* ___ShaderAxisZ_27;
	// System.String VFX.TargetVFX::ShaderScale
	String_t* ___ShaderScale_28;
	// UnityEngine.Vector3 VFX.TargetVFX::Center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___Center_29;
	// VFX.VFXCenterMode VFX.TargetVFX::CenterMode
	int32_t ___CenterMode_30;
	// Vuforia.ObserverBehaviour VFX.TargetVFX::mObserver
	ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * ___mObserver_31;
	// Vuforia.TargetStatus VFX.TargetVFX::mPreviousTargetStatus
	TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  ___mPreviousTargetStatus_32;

public:
	inline static int32_t get_offset_of_Observer_21() { return static_cast<int32_t>(offsetof(TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36, ___Observer_21)); }
	inline ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * get_Observer_21() const { return ___Observer_21; }
	inline ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 ** get_address_of_Observer_21() { return &___Observer_21; }
	inline void set_Observer_21(ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * value)
	{
		___Observer_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Observer_21), (void*)value);
	}

	inline static int32_t get_offset_of_IsTargetShownOnTargetFound_22() { return static_cast<int32_t>(offsetof(TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36, ___IsTargetShownOnTargetFound_22)); }
	inline bool get_IsTargetShownOnTargetFound_22() const { return ___IsTargetShownOnTargetFound_22; }
	inline bool* get_address_of_IsTargetShownOnTargetFound_22() { return &___IsTargetShownOnTargetFound_22; }
	inline void set_IsTargetShownOnTargetFound_22(bool value)
	{
		___IsTargetShownOnTargetFound_22 = value;
	}

	inline static int32_t get_offset_of_ShowDelay_23() { return static_cast<int32_t>(offsetof(TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36, ___ShowDelay_23)); }
	inline float get_ShowDelay_23() const { return ___ShowDelay_23; }
	inline float* get_address_of_ShowDelay_23() { return &___ShowDelay_23; }
	inline void set_ShowDelay_23(float value)
	{
		___ShowDelay_23 = value;
	}

	inline static int32_t get_offset_of_ShaderCenter_24() { return static_cast<int32_t>(offsetof(TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36, ___ShaderCenter_24)); }
	inline String_t* get_ShaderCenter_24() const { return ___ShaderCenter_24; }
	inline String_t** get_address_of_ShaderCenter_24() { return &___ShaderCenter_24; }
	inline void set_ShaderCenter_24(String_t* value)
	{
		___ShaderCenter_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShaderCenter_24), (void*)value);
	}

	inline static int32_t get_offset_of_ShaderAxisX_25() { return static_cast<int32_t>(offsetof(TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36, ___ShaderAxisX_25)); }
	inline String_t* get_ShaderAxisX_25() const { return ___ShaderAxisX_25; }
	inline String_t** get_address_of_ShaderAxisX_25() { return &___ShaderAxisX_25; }
	inline void set_ShaderAxisX_25(String_t* value)
	{
		___ShaderAxisX_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShaderAxisX_25), (void*)value);
	}

	inline static int32_t get_offset_of_ShaderAxisY_26() { return static_cast<int32_t>(offsetof(TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36, ___ShaderAxisY_26)); }
	inline String_t* get_ShaderAxisY_26() const { return ___ShaderAxisY_26; }
	inline String_t** get_address_of_ShaderAxisY_26() { return &___ShaderAxisY_26; }
	inline void set_ShaderAxisY_26(String_t* value)
	{
		___ShaderAxisY_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShaderAxisY_26), (void*)value);
	}

	inline static int32_t get_offset_of_ShaderAxisZ_27() { return static_cast<int32_t>(offsetof(TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36, ___ShaderAxisZ_27)); }
	inline String_t* get_ShaderAxisZ_27() const { return ___ShaderAxisZ_27; }
	inline String_t** get_address_of_ShaderAxisZ_27() { return &___ShaderAxisZ_27; }
	inline void set_ShaderAxisZ_27(String_t* value)
	{
		___ShaderAxisZ_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShaderAxisZ_27), (void*)value);
	}

	inline static int32_t get_offset_of_ShaderScale_28() { return static_cast<int32_t>(offsetof(TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36, ___ShaderScale_28)); }
	inline String_t* get_ShaderScale_28() const { return ___ShaderScale_28; }
	inline String_t** get_address_of_ShaderScale_28() { return &___ShaderScale_28; }
	inline void set_ShaderScale_28(String_t* value)
	{
		___ShaderScale_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShaderScale_28), (void*)value);
	}

	inline static int32_t get_offset_of_Center_29() { return static_cast<int32_t>(offsetof(TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36, ___Center_29)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_Center_29() const { return ___Center_29; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_Center_29() { return &___Center_29; }
	inline void set_Center_29(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___Center_29 = value;
	}

	inline static int32_t get_offset_of_CenterMode_30() { return static_cast<int32_t>(offsetof(TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36, ___CenterMode_30)); }
	inline int32_t get_CenterMode_30() const { return ___CenterMode_30; }
	inline int32_t* get_address_of_CenterMode_30() { return &___CenterMode_30; }
	inline void set_CenterMode_30(int32_t value)
	{
		___CenterMode_30 = value;
	}

	inline static int32_t get_offset_of_mObserver_31() { return static_cast<int32_t>(offsetof(TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36, ___mObserver_31)); }
	inline ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * get_mObserver_31() const { return ___mObserver_31; }
	inline ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 ** get_address_of_mObserver_31() { return &___mObserver_31; }
	inline void set_mObserver_31(ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * value)
	{
		___mObserver_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mObserver_31), (void*)value);
	}

	inline static int32_t get_offset_of_mPreviousTargetStatus_32() { return static_cast<int32_t>(offsetof(TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36, ___mPreviousTargetStatus_32)); }
	inline TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  get_mPreviousTargetStatus_32() const { return ___mPreviousTargetStatus_32; }
	inline TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1 * get_address_of_mPreviousTargetStatus_32() { return &___mPreviousTargetStatus_32; }
	inline void set_mPreviousTargetStatus_32(TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  value)
	{
		___mPreviousTargetStatus_32 = value;
	}
};


// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407  : public VuforiaMonoBehaviour_t172ED03DD8996A3B90FCEDB8A62EB98DBDCB0932
{
public:
	// Vuforia.Internal.Core.IEngine Vuforia.VuforiaBehaviour::mEngine
	RuntimeObject* ___mEngine_5;
	// Vuforia.IObserverRegistry Vuforia.VuforiaBehaviour::mObserversRegistry
	RuntimeObject* ___mObserversRegistry_6;
	// Vuforia.WorldCenterMode Vuforia.VuforiaBehaviour::mWorldCenterMode
	int32_t ___mWorldCenterMode_7;
	// Vuforia.ObserverBehaviour Vuforia.VuforiaBehaviour::mWorldCenter
	ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * ___mWorldCenter_8;
	// Vuforia.CameraDevice Vuforia.VuforiaBehaviour::<CameraDevice>k__BackingField
	CameraDevice_t5A659FEC1FF047A9D0BE5A49CE224EECCF96697F * ___U3CCameraDeviceU3Ek__BackingField_9;
	// Vuforia.ObserverFactory Vuforia.VuforiaBehaviour::<ObserverFactory>k__BackingField
	ObserverFactory_t8F8A436F3E3074002EAC9BC33F75BB80335496F5 * ___U3CObserverFactoryU3Ek__BackingField_10;
	// Vuforia.DevicePoseBehaviour Vuforia.VuforiaBehaviour::<DevicePoseBehaviour>k__BackingField
	DevicePoseBehaviour_t228722CAB359AE9B3CB16CCE4DFA70DB951C35E1 * ___U3CDevicePoseBehaviourU3Ek__BackingField_11;
	// Vuforia.VideoBackground Vuforia.VuforiaBehaviour::<VideoBackground>k__BackingField
	VideoBackground_tABAA05A8DC2011C5B98D09B8DF0C36853D585713 * ___U3CVideoBackgroundU3Ek__BackingField_12;
	// Vuforia.World Vuforia.VuforiaBehaviour::<World>k__BackingField
	World_tD34189E7DB459CEB10A1D3C0D7C94B25197AD14F * ___U3CWorldU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_mEngine_5() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407, ___mEngine_5)); }
	inline RuntimeObject* get_mEngine_5() const { return ___mEngine_5; }
	inline RuntimeObject** get_address_of_mEngine_5() { return &___mEngine_5; }
	inline void set_mEngine_5(RuntimeObject* value)
	{
		___mEngine_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mEngine_5), (void*)value);
	}

	inline static int32_t get_offset_of_mObserversRegistry_6() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407, ___mObserversRegistry_6)); }
	inline RuntimeObject* get_mObserversRegistry_6() const { return ___mObserversRegistry_6; }
	inline RuntimeObject** get_address_of_mObserversRegistry_6() { return &___mObserversRegistry_6; }
	inline void set_mObserversRegistry_6(RuntimeObject* value)
	{
		___mObserversRegistry_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mObserversRegistry_6), (void*)value);
	}

	inline static int32_t get_offset_of_mWorldCenterMode_7() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407, ___mWorldCenterMode_7)); }
	inline int32_t get_mWorldCenterMode_7() const { return ___mWorldCenterMode_7; }
	inline int32_t* get_address_of_mWorldCenterMode_7() { return &___mWorldCenterMode_7; }
	inline void set_mWorldCenterMode_7(int32_t value)
	{
		___mWorldCenterMode_7 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_8() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407, ___mWorldCenter_8)); }
	inline ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * get_mWorldCenter_8() const { return ___mWorldCenter_8; }
	inline ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 ** get_address_of_mWorldCenter_8() { return &___mWorldCenter_8; }
	inline void set_mWorldCenter_8(ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * value)
	{
		___mWorldCenter_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mWorldCenter_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCameraDeviceU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407, ___U3CCameraDeviceU3Ek__BackingField_9)); }
	inline CameraDevice_t5A659FEC1FF047A9D0BE5A49CE224EECCF96697F * get_U3CCameraDeviceU3Ek__BackingField_9() const { return ___U3CCameraDeviceU3Ek__BackingField_9; }
	inline CameraDevice_t5A659FEC1FF047A9D0BE5A49CE224EECCF96697F ** get_address_of_U3CCameraDeviceU3Ek__BackingField_9() { return &___U3CCameraDeviceU3Ek__BackingField_9; }
	inline void set_U3CCameraDeviceU3Ek__BackingField_9(CameraDevice_t5A659FEC1FF047A9D0BE5A49CE224EECCF96697F * value)
	{
		___U3CCameraDeviceU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCameraDeviceU3Ek__BackingField_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CObserverFactoryU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407, ___U3CObserverFactoryU3Ek__BackingField_10)); }
	inline ObserverFactory_t8F8A436F3E3074002EAC9BC33F75BB80335496F5 * get_U3CObserverFactoryU3Ek__BackingField_10() const { return ___U3CObserverFactoryU3Ek__BackingField_10; }
	inline ObserverFactory_t8F8A436F3E3074002EAC9BC33F75BB80335496F5 ** get_address_of_U3CObserverFactoryU3Ek__BackingField_10() { return &___U3CObserverFactoryU3Ek__BackingField_10; }
	inline void set_U3CObserverFactoryU3Ek__BackingField_10(ObserverFactory_t8F8A436F3E3074002EAC9BC33F75BB80335496F5 * value)
	{
		___U3CObserverFactoryU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CObserverFactoryU3Ek__BackingField_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDevicePoseBehaviourU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407, ___U3CDevicePoseBehaviourU3Ek__BackingField_11)); }
	inline DevicePoseBehaviour_t228722CAB359AE9B3CB16CCE4DFA70DB951C35E1 * get_U3CDevicePoseBehaviourU3Ek__BackingField_11() const { return ___U3CDevicePoseBehaviourU3Ek__BackingField_11; }
	inline DevicePoseBehaviour_t228722CAB359AE9B3CB16CCE4DFA70DB951C35E1 ** get_address_of_U3CDevicePoseBehaviourU3Ek__BackingField_11() { return &___U3CDevicePoseBehaviourU3Ek__BackingField_11; }
	inline void set_U3CDevicePoseBehaviourU3Ek__BackingField_11(DevicePoseBehaviour_t228722CAB359AE9B3CB16CCE4DFA70DB951C35E1 * value)
	{
		___U3CDevicePoseBehaviourU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDevicePoseBehaviourU3Ek__BackingField_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3CVideoBackgroundU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407, ___U3CVideoBackgroundU3Ek__BackingField_12)); }
	inline VideoBackground_tABAA05A8DC2011C5B98D09B8DF0C36853D585713 * get_U3CVideoBackgroundU3Ek__BackingField_12() const { return ___U3CVideoBackgroundU3Ek__BackingField_12; }
	inline VideoBackground_tABAA05A8DC2011C5B98D09B8DF0C36853D585713 ** get_address_of_U3CVideoBackgroundU3Ek__BackingField_12() { return &___U3CVideoBackgroundU3Ek__BackingField_12; }
	inline void set_U3CVideoBackgroundU3Ek__BackingField_12(VideoBackground_tABAA05A8DC2011C5B98D09B8DF0C36853D585713 * value)
	{
		___U3CVideoBackgroundU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CVideoBackgroundU3Ek__BackingField_12), (void*)value);
	}

	inline static int32_t get_offset_of_U3CWorldU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407, ___U3CWorldU3Ek__BackingField_13)); }
	inline World_tD34189E7DB459CEB10A1D3C0D7C94B25197AD14F * get_U3CWorldU3Ek__BackingField_13() const { return ___U3CWorldU3Ek__BackingField_13; }
	inline World_tD34189E7DB459CEB10A1D3C0D7C94B25197AD14F ** get_address_of_U3CWorldU3Ek__BackingField_13() { return &___U3CWorldU3Ek__BackingField_13; }
	inline void set_U3CWorldU3Ek__BackingField_13(World_tD34189E7DB459CEB10A1D3C0D7C94B25197AD14F * value)
	{
		___U3CWorldU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CWorldU3Ek__BackingField_13), (void*)value);
	}
};


// Vuforia.DataSetTrackableBehaviour
struct DataSetTrackableBehaviour_tDACD5B676DD2E9EE2943323AC60A131390E676BA  : public ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274
{
public:
	// System.String Vuforia.DataSetTrackableBehaviour::mDataSetPath
	String_t* ___mDataSetPath_14;

public:
	inline static int32_t get_offset_of_mDataSetPath_14() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_tDACD5B676DD2E9EE2943323AC60A131390E676BA, ___mDataSetPath_14)); }
	inline String_t* get_mDataSetPath_14() const { return ___mDataSetPath_14; }
	inline String_t** get_address_of_mDataSetPath_14() { return &___mDataSetPath_14; }
	inline void set_mDataSetPath_14(String_t* value)
	{
		___mDataSetPath_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mDataSetPath_14), (void*)value);
	}
};


// Vuforia.AreaTargetBehaviour
struct AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787  : public DataSetTrackableBehaviour_tDACD5B676DD2E9EE2943323AC60A131390E676BA
{
public:
	// System.String Vuforia.AreaTargetBehaviour::AuthoringModelPath
	String_t* ___AuthoringModelPath_15;
	// System.String Vuforia.AreaTargetBehaviour::OcclusionModelPath
	String_t* ___OcclusionModelPath_16;
	// System.Single Vuforia.AreaTargetBehaviour::MaxClippingHeight
	float ___MaxClippingHeight_17;
	// System.Single Vuforia.AreaTargetBehaviour::MinClippingHeight
	float ___MinClippingHeight_18;
	// UnityEngine.Vector3 Vuforia.AreaTargetBehaviour::mSize
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___mSize_19;
	// UnityEngine.Bounds Vuforia.AreaTargetBehaviour::mBoundingBox
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___mBoundingBox_20;
	// System.String Vuforia.AreaTargetBehaviour::mNavMeshModelPath
	String_t* ___mNavMeshModelPath_21;
	// System.String Vuforia.AreaTargetBehaviour::mVersion
	String_t* ___mVersion_22;
	// UnityEngine.Vector3 Vuforia.AreaTargetBehaviour::mBBoxMin
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___mBBoxMin_23;
	// UnityEngine.Vector3 Vuforia.AreaTargetBehaviour::mBBoxMax
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___mBBoxMax_24;
	// UnityEngine.GameObject Vuforia.AreaTargetBehaviour::mModelMesh
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___mModelMesh_25;
	// System.Boolean Vuforia.AreaTargetBehaviour::mRequiresExternalPositions
	bool ___mRequiresExternalPositions_26;
	// Vuforia.AreaTargetPreview Vuforia.AreaTargetBehaviour::mPreview
	AreaTargetPreview_tFE128AA8F81FB158A0D30043FEF8E8F6F1B02291 * ___mPreview_27;
	// Vuforia.StorageType Vuforia.AreaTargetBehaviour::OcclusionModelStorageType
	int32_t ___OcclusionModelStorageType_28;
	// Vuforia.Internal.Observers.AreaTargetObserver Vuforia.AreaTargetBehaviour::mAreaTargetObserver
	AreaTargetObserver_t9CD4110DA64ABE157D3EFDCFEB9CA90C1B642D89 * ___mAreaTargetObserver_29;
	// Vuforia.IRuntimeMeshBehaviour Vuforia.AreaTargetBehaviour::mRuntimeMeshBehaviour
	RuntimeObject* ___mRuntimeMeshBehaviour_30;

public:
	inline static int32_t get_offset_of_AuthoringModelPath_15() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___AuthoringModelPath_15)); }
	inline String_t* get_AuthoringModelPath_15() const { return ___AuthoringModelPath_15; }
	inline String_t** get_address_of_AuthoringModelPath_15() { return &___AuthoringModelPath_15; }
	inline void set_AuthoringModelPath_15(String_t* value)
	{
		___AuthoringModelPath_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AuthoringModelPath_15), (void*)value);
	}

	inline static int32_t get_offset_of_OcclusionModelPath_16() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___OcclusionModelPath_16)); }
	inline String_t* get_OcclusionModelPath_16() const { return ___OcclusionModelPath_16; }
	inline String_t** get_address_of_OcclusionModelPath_16() { return &___OcclusionModelPath_16; }
	inline void set_OcclusionModelPath_16(String_t* value)
	{
		___OcclusionModelPath_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OcclusionModelPath_16), (void*)value);
	}

	inline static int32_t get_offset_of_MaxClippingHeight_17() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___MaxClippingHeight_17)); }
	inline float get_MaxClippingHeight_17() const { return ___MaxClippingHeight_17; }
	inline float* get_address_of_MaxClippingHeight_17() { return &___MaxClippingHeight_17; }
	inline void set_MaxClippingHeight_17(float value)
	{
		___MaxClippingHeight_17 = value;
	}

	inline static int32_t get_offset_of_MinClippingHeight_18() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___MinClippingHeight_18)); }
	inline float get_MinClippingHeight_18() const { return ___MinClippingHeight_18; }
	inline float* get_address_of_MinClippingHeight_18() { return &___MinClippingHeight_18; }
	inline void set_MinClippingHeight_18(float value)
	{
		___MinClippingHeight_18 = value;
	}

	inline static int32_t get_offset_of_mSize_19() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___mSize_19)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_mSize_19() const { return ___mSize_19; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_mSize_19() { return &___mSize_19; }
	inline void set_mSize_19(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___mSize_19 = value;
	}

	inline static int32_t get_offset_of_mBoundingBox_20() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___mBoundingBox_20)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_mBoundingBox_20() const { return ___mBoundingBox_20; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_mBoundingBox_20() { return &___mBoundingBox_20; }
	inline void set_mBoundingBox_20(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___mBoundingBox_20 = value;
	}

	inline static int32_t get_offset_of_mNavMeshModelPath_21() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___mNavMeshModelPath_21)); }
	inline String_t* get_mNavMeshModelPath_21() const { return ___mNavMeshModelPath_21; }
	inline String_t** get_address_of_mNavMeshModelPath_21() { return &___mNavMeshModelPath_21; }
	inline void set_mNavMeshModelPath_21(String_t* value)
	{
		___mNavMeshModelPath_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mNavMeshModelPath_21), (void*)value);
	}

	inline static int32_t get_offset_of_mVersion_22() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___mVersion_22)); }
	inline String_t* get_mVersion_22() const { return ___mVersion_22; }
	inline String_t** get_address_of_mVersion_22() { return &___mVersion_22; }
	inline void set_mVersion_22(String_t* value)
	{
		___mVersion_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mVersion_22), (void*)value);
	}

	inline static int32_t get_offset_of_mBBoxMin_23() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___mBBoxMin_23)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_mBBoxMin_23() const { return ___mBBoxMin_23; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_mBBoxMin_23() { return &___mBBoxMin_23; }
	inline void set_mBBoxMin_23(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___mBBoxMin_23 = value;
	}

	inline static int32_t get_offset_of_mBBoxMax_24() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___mBBoxMax_24)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_mBBoxMax_24() const { return ___mBBoxMax_24; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_mBBoxMax_24() { return &___mBBoxMax_24; }
	inline void set_mBBoxMax_24(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___mBBoxMax_24 = value;
	}

	inline static int32_t get_offset_of_mModelMesh_25() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___mModelMesh_25)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_mModelMesh_25() const { return ___mModelMesh_25; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_mModelMesh_25() { return &___mModelMesh_25; }
	inline void set_mModelMesh_25(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___mModelMesh_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mModelMesh_25), (void*)value);
	}

	inline static int32_t get_offset_of_mRequiresExternalPositions_26() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___mRequiresExternalPositions_26)); }
	inline bool get_mRequiresExternalPositions_26() const { return ___mRequiresExternalPositions_26; }
	inline bool* get_address_of_mRequiresExternalPositions_26() { return &___mRequiresExternalPositions_26; }
	inline void set_mRequiresExternalPositions_26(bool value)
	{
		___mRequiresExternalPositions_26 = value;
	}

	inline static int32_t get_offset_of_mPreview_27() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___mPreview_27)); }
	inline AreaTargetPreview_tFE128AA8F81FB158A0D30043FEF8E8F6F1B02291 * get_mPreview_27() const { return ___mPreview_27; }
	inline AreaTargetPreview_tFE128AA8F81FB158A0D30043FEF8E8F6F1B02291 ** get_address_of_mPreview_27() { return &___mPreview_27; }
	inline void set_mPreview_27(AreaTargetPreview_tFE128AA8F81FB158A0D30043FEF8E8F6F1B02291 * value)
	{
		___mPreview_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mPreview_27), (void*)value);
	}

	inline static int32_t get_offset_of_OcclusionModelStorageType_28() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___OcclusionModelStorageType_28)); }
	inline int32_t get_OcclusionModelStorageType_28() const { return ___OcclusionModelStorageType_28; }
	inline int32_t* get_address_of_OcclusionModelStorageType_28() { return &___OcclusionModelStorageType_28; }
	inline void set_OcclusionModelStorageType_28(int32_t value)
	{
		___OcclusionModelStorageType_28 = value;
	}

	inline static int32_t get_offset_of_mAreaTargetObserver_29() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___mAreaTargetObserver_29)); }
	inline AreaTargetObserver_t9CD4110DA64ABE157D3EFDCFEB9CA90C1B642D89 * get_mAreaTargetObserver_29() const { return ___mAreaTargetObserver_29; }
	inline AreaTargetObserver_t9CD4110DA64ABE157D3EFDCFEB9CA90C1B642D89 ** get_address_of_mAreaTargetObserver_29() { return &___mAreaTargetObserver_29; }
	inline void set_mAreaTargetObserver_29(AreaTargetObserver_t9CD4110DA64ABE157D3EFDCFEB9CA90C1B642D89 * value)
	{
		___mAreaTargetObserver_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mAreaTargetObserver_29), (void*)value);
	}

	inline static int32_t get_offset_of_mRuntimeMeshBehaviour_30() { return static_cast<int32_t>(offsetof(AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787, ___mRuntimeMeshBehaviour_30)); }
	inline RuntimeObject* get_mRuntimeMeshBehaviour_30() const { return ___mRuntimeMeshBehaviour_30; }
	inline RuntimeObject** get_address_of_mRuntimeMeshBehaviour_30() { return &___mRuntimeMeshBehaviour_30; }
	inline void set_mRuntimeMeshBehaviour_30(RuntimeObject* value)
	{
		___mRuntimeMeshBehaviour_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mRuntimeMeshBehaviour_30), (void*)value);
	}
};


// Vuforia.ScalableDataSetTrackableBehaviour
struct ScalableDataSetTrackableBehaviour_t4703A60B9EF85DCBEDB83ED8EE286EB319831E64  : public DataSetTrackableBehaviour_tDACD5B676DD2E9EE2943323AC60A131390E676BA
{
public:

public:
};


// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3  : public ScalableDataSetTrackableBehaviour_t4703A60B9EF85DCBEDB83ED8EE286EB319831E64
{
public:
	// System.Single Vuforia.CylinderTargetBehaviour::mTopDiameterRatio
	float ___mTopDiameterRatio_15;
	// System.Single Vuforia.CylinderTargetBehaviour::mBottomDiameterRatio
	float ___mBottomDiameterRatio_16;
	// System.Single Vuforia.CylinderTargetBehaviour::mSideLength
	float ___mSideLength_17;
	// System.Single Vuforia.CylinderTargetBehaviour::mTopDiameter
	float ___mTopDiameter_18;
	// System.Single Vuforia.CylinderTargetBehaviour::mBottomDiameter
	float ___mBottomDiameter_19;
	// Vuforia.Internal.TargetMotionHint Vuforia.CylinderTargetBehaviour::mMotionHint
	int32_t ___mMotionHint_20;
	// Vuforia.TrackingOptimization Vuforia.CylinderTargetBehaviour::mTrackingOptimization
	int32_t ___mTrackingOptimization_21;
	// System.Boolean Vuforia.CylinderTargetBehaviour::mTrackingOptimizationNeedsUpgrade
	bool ___mTrackingOptimizationNeedsUpgrade_22;
	// Vuforia.CylinderTargetPreview Vuforia.CylinderTargetBehaviour::mPreview
	CylinderTargetPreview_t4B429587671F9E94F9A326DCBD68B191FE50BB7F * ___mPreview_23;
	// UnityEngine.Vector3 Vuforia.CylinderTargetBehaviour::mLastSize
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___mLastSize_24;
	// Vuforia.Internal.Observers.CylinderTargetObserver Vuforia.CylinderTargetBehaviour::mCylinderTargetObserver
	CylinderTargetObserver_t1C0445916F65052B70F5BA7F78CB7888851939B1 * ___mCylinderTargetObserver_25;

public:
	inline static int32_t get_offset_of_mTopDiameterRatio_15() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3, ___mTopDiameterRatio_15)); }
	inline float get_mTopDiameterRatio_15() const { return ___mTopDiameterRatio_15; }
	inline float* get_address_of_mTopDiameterRatio_15() { return &___mTopDiameterRatio_15; }
	inline void set_mTopDiameterRatio_15(float value)
	{
		___mTopDiameterRatio_15 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameterRatio_16() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3, ___mBottomDiameterRatio_16)); }
	inline float get_mBottomDiameterRatio_16() const { return ___mBottomDiameterRatio_16; }
	inline float* get_address_of_mBottomDiameterRatio_16() { return &___mBottomDiameterRatio_16; }
	inline void set_mBottomDiameterRatio_16(float value)
	{
		___mBottomDiameterRatio_16 = value;
	}

	inline static int32_t get_offset_of_mSideLength_17() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3, ___mSideLength_17)); }
	inline float get_mSideLength_17() const { return ___mSideLength_17; }
	inline float* get_address_of_mSideLength_17() { return &___mSideLength_17; }
	inline void set_mSideLength_17(float value)
	{
		___mSideLength_17 = value;
	}

	inline static int32_t get_offset_of_mTopDiameter_18() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3, ___mTopDiameter_18)); }
	inline float get_mTopDiameter_18() const { return ___mTopDiameter_18; }
	inline float* get_address_of_mTopDiameter_18() { return &___mTopDiameter_18; }
	inline void set_mTopDiameter_18(float value)
	{
		___mTopDiameter_18 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameter_19() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3, ___mBottomDiameter_19)); }
	inline float get_mBottomDiameter_19() const { return ___mBottomDiameter_19; }
	inline float* get_address_of_mBottomDiameter_19() { return &___mBottomDiameter_19; }
	inline void set_mBottomDiameter_19(float value)
	{
		___mBottomDiameter_19 = value;
	}

	inline static int32_t get_offset_of_mMotionHint_20() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3, ___mMotionHint_20)); }
	inline int32_t get_mMotionHint_20() const { return ___mMotionHint_20; }
	inline int32_t* get_address_of_mMotionHint_20() { return &___mMotionHint_20; }
	inline void set_mMotionHint_20(int32_t value)
	{
		___mMotionHint_20 = value;
	}

	inline static int32_t get_offset_of_mTrackingOptimization_21() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3, ___mTrackingOptimization_21)); }
	inline int32_t get_mTrackingOptimization_21() const { return ___mTrackingOptimization_21; }
	inline int32_t* get_address_of_mTrackingOptimization_21() { return &___mTrackingOptimization_21; }
	inline void set_mTrackingOptimization_21(int32_t value)
	{
		___mTrackingOptimization_21 = value;
	}

	inline static int32_t get_offset_of_mTrackingOptimizationNeedsUpgrade_22() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3, ___mTrackingOptimizationNeedsUpgrade_22)); }
	inline bool get_mTrackingOptimizationNeedsUpgrade_22() const { return ___mTrackingOptimizationNeedsUpgrade_22; }
	inline bool* get_address_of_mTrackingOptimizationNeedsUpgrade_22() { return &___mTrackingOptimizationNeedsUpgrade_22; }
	inline void set_mTrackingOptimizationNeedsUpgrade_22(bool value)
	{
		___mTrackingOptimizationNeedsUpgrade_22 = value;
	}

	inline static int32_t get_offset_of_mPreview_23() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3, ___mPreview_23)); }
	inline CylinderTargetPreview_t4B429587671F9E94F9A326DCBD68B191FE50BB7F * get_mPreview_23() const { return ___mPreview_23; }
	inline CylinderTargetPreview_t4B429587671F9E94F9A326DCBD68B191FE50BB7F ** get_address_of_mPreview_23() { return &___mPreview_23; }
	inline void set_mPreview_23(CylinderTargetPreview_t4B429587671F9E94F9A326DCBD68B191FE50BB7F * value)
	{
		___mPreview_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mPreview_23), (void*)value);
	}

	inline static int32_t get_offset_of_mLastSize_24() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3, ___mLastSize_24)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_mLastSize_24() const { return ___mLastSize_24; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_mLastSize_24() { return &___mLastSize_24; }
	inline void set_mLastSize_24(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___mLastSize_24 = value;
	}

	inline static int32_t get_offset_of_mCylinderTargetObserver_25() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3, ___mCylinderTargetObserver_25)); }
	inline CylinderTargetObserver_t1C0445916F65052B70F5BA7F78CB7888851939B1 * get_mCylinderTargetObserver_25() const { return ___mCylinderTargetObserver_25; }
	inline CylinderTargetObserver_t1C0445916F65052B70F5BA7F78CB7888851939B1 ** get_address_of_mCylinderTargetObserver_25() { return &___mCylinderTargetObserver_25; }
	inline void set_mCylinderTargetObserver_25(CylinderTargetObserver_t1C0445916F65052B70F5BA7F78CB7888851939B1 * value)
	{
		___mCylinderTargetObserver_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCylinderTargetObserver_25), (void*)value);
	}
};


// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34  : public ScalableDataSetTrackableBehaviour_t4703A60B9EF85DCBEDB83ED8EE286EB319831E64
{
public:
	// System.Single Vuforia.ImageTargetBehaviour::mAspectRatio
	float ___mAspectRatio_15;
	// Vuforia.ImageTargetType Vuforia.ImageTargetBehaviour::mImageTargetType
	int32_t ___mImageTargetType_16;
	// System.Single Vuforia.ImageTargetBehaviour::mWidth
	float ___mWidth_17;
	// System.Single Vuforia.ImageTargetBehaviour::mHeight
	float ___mHeight_18;
	// UnityEngine.Texture2D Vuforia.ImageTargetBehaviour::mRuntimeTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___mRuntimeTexture_19;
	// Vuforia.Internal.TargetMotionHint Vuforia.ImageTargetBehaviour::mMotionHint
	int32_t ___mMotionHint_20;
	// Vuforia.TrackingOptimization Vuforia.ImageTargetBehaviour::mTrackingOptimization
	int32_t ___mTrackingOptimization_21;
	// System.Boolean Vuforia.ImageTargetBehaviour::mTrackingOptimizationNeedsUpgrade
	bool ___mTrackingOptimizationNeedsUpgrade_22;
	// Vuforia.ImageTargetPreview Vuforia.ImageTargetBehaviour::mPreview
	ImageTargetPreview_tA100928CE44BCD50271453877087251759D7D2E2 * ___mPreview_23;
	// UnityEngine.Vector2 Vuforia.ImageTargetBehaviour::mLastSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___mLastSize_24;
	// System.Collections.Generic.List`1<Vuforia.VirtualButtonBehaviour> Vuforia.ImageTargetBehaviour::mVirtualButtonBehaviours
	List_1_t992A4FEC1847167339E43DBB7C36DBBF3688D9AF * ___mVirtualButtonBehaviours_25;
	// Vuforia.Internal.Observers.ImageTargetObserver Vuforia.ImageTargetBehaviour::mImageTargetObserver
	ImageTargetObserver_t19BC37E4B4C30995FC064B94A719434AA250750F * ___mImageTargetObserver_26;

public:
	inline static int32_t get_offset_of_mAspectRatio_15() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34, ___mAspectRatio_15)); }
	inline float get_mAspectRatio_15() const { return ___mAspectRatio_15; }
	inline float* get_address_of_mAspectRatio_15() { return &___mAspectRatio_15; }
	inline void set_mAspectRatio_15(float value)
	{
		___mAspectRatio_15 = value;
	}

	inline static int32_t get_offset_of_mImageTargetType_16() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34, ___mImageTargetType_16)); }
	inline int32_t get_mImageTargetType_16() const { return ___mImageTargetType_16; }
	inline int32_t* get_address_of_mImageTargetType_16() { return &___mImageTargetType_16; }
	inline void set_mImageTargetType_16(int32_t value)
	{
		___mImageTargetType_16 = value;
	}

	inline static int32_t get_offset_of_mWidth_17() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34, ___mWidth_17)); }
	inline float get_mWidth_17() const { return ___mWidth_17; }
	inline float* get_address_of_mWidth_17() { return &___mWidth_17; }
	inline void set_mWidth_17(float value)
	{
		___mWidth_17 = value;
	}

	inline static int32_t get_offset_of_mHeight_18() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34, ___mHeight_18)); }
	inline float get_mHeight_18() const { return ___mHeight_18; }
	inline float* get_address_of_mHeight_18() { return &___mHeight_18; }
	inline void set_mHeight_18(float value)
	{
		___mHeight_18 = value;
	}

	inline static int32_t get_offset_of_mRuntimeTexture_19() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34, ___mRuntimeTexture_19)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_mRuntimeTexture_19() const { return ___mRuntimeTexture_19; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_mRuntimeTexture_19() { return &___mRuntimeTexture_19; }
	inline void set_mRuntimeTexture_19(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___mRuntimeTexture_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mRuntimeTexture_19), (void*)value);
	}

	inline static int32_t get_offset_of_mMotionHint_20() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34, ___mMotionHint_20)); }
	inline int32_t get_mMotionHint_20() const { return ___mMotionHint_20; }
	inline int32_t* get_address_of_mMotionHint_20() { return &___mMotionHint_20; }
	inline void set_mMotionHint_20(int32_t value)
	{
		___mMotionHint_20 = value;
	}

	inline static int32_t get_offset_of_mTrackingOptimization_21() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34, ___mTrackingOptimization_21)); }
	inline int32_t get_mTrackingOptimization_21() const { return ___mTrackingOptimization_21; }
	inline int32_t* get_address_of_mTrackingOptimization_21() { return &___mTrackingOptimization_21; }
	inline void set_mTrackingOptimization_21(int32_t value)
	{
		___mTrackingOptimization_21 = value;
	}

	inline static int32_t get_offset_of_mTrackingOptimizationNeedsUpgrade_22() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34, ___mTrackingOptimizationNeedsUpgrade_22)); }
	inline bool get_mTrackingOptimizationNeedsUpgrade_22() const { return ___mTrackingOptimizationNeedsUpgrade_22; }
	inline bool* get_address_of_mTrackingOptimizationNeedsUpgrade_22() { return &___mTrackingOptimizationNeedsUpgrade_22; }
	inline void set_mTrackingOptimizationNeedsUpgrade_22(bool value)
	{
		___mTrackingOptimizationNeedsUpgrade_22 = value;
	}

	inline static int32_t get_offset_of_mPreview_23() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34, ___mPreview_23)); }
	inline ImageTargetPreview_tA100928CE44BCD50271453877087251759D7D2E2 * get_mPreview_23() const { return ___mPreview_23; }
	inline ImageTargetPreview_tA100928CE44BCD50271453877087251759D7D2E2 ** get_address_of_mPreview_23() { return &___mPreview_23; }
	inline void set_mPreview_23(ImageTargetPreview_tA100928CE44BCD50271453877087251759D7D2E2 * value)
	{
		___mPreview_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mPreview_23), (void*)value);
	}

	inline static int32_t get_offset_of_mLastSize_24() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34, ___mLastSize_24)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_mLastSize_24() const { return ___mLastSize_24; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_mLastSize_24() { return &___mLastSize_24; }
	inline void set_mLastSize_24(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___mLastSize_24 = value;
	}

	inline static int32_t get_offset_of_mVirtualButtonBehaviours_25() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34, ___mVirtualButtonBehaviours_25)); }
	inline List_1_t992A4FEC1847167339E43DBB7C36DBBF3688D9AF * get_mVirtualButtonBehaviours_25() const { return ___mVirtualButtonBehaviours_25; }
	inline List_1_t992A4FEC1847167339E43DBB7C36DBBF3688D9AF ** get_address_of_mVirtualButtonBehaviours_25() { return &___mVirtualButtonBehaviours_25; }
	inline void set_mVirtualButtonBehaviours_25(List_1_t992A4FEC1847167339E43DBB7C36DBBF3688D9AF * value)
	{
		___mVirtualButtonBehaviours_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mVirtualButtonBehaviours_25), (void*)value);
	}

	inline static int32_t get_offset_of_mImageTargetObserver_26() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34, ___mImageTargetObserver_26)); }
	inline ImageTargetObserver_t19BC37E4B4C30995FC064B94A719434AA250750F * get_mImageTargetObserver_26() const { return ___mImageTargetObserver_26; }
	inline ImageTargetObserver_t19BC37E4B4C30995FC064B94A719434AA250750F ** get_address_of_mImageTargetObserver_26() { return &___mImageTargetObserver_26; }
	inline void set_mImageTargetObserver_26(ImageTargetObserver_t19BC37E4B4C30995FC064B94A719434AA250750F * value)
	{
		___mImageTargetObserver_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mImageTargetObserver_26), (void*)value);
	}
};


// Vuforia.ModelTargetBehaviour
struct ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275  : public ScalableDataSetTrackableBehaviour_t4703A60B9EF85DCBEDB83ED8EE286EB319831E64
{
public:
	// UnityEngine.GameObject Vuforia.ModelTargetBehaviour::mGuideViewRenderer
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___mGuideViewRenderer_16;
	// System.Single Vuforia.ModelTargetBehaviour::mBaseSize
	float ___mBaseSize_17;
	// System.Single Vuforia.ModelTargetBehaviour::mAspectRatioXY
	float ___mAspectRatioXY_18;
	// System.Single Vuforia.ModelTargetBehaviour::mAspectRatioXZ
	float ___mAspectRatioXZ_19;
	// System.Boolean Vuforia.ModelTargetBehaviour::mShowBoundingBox
	bool ___mShowBoundingBox_20;
	// System.Boolean Vuforia.ModelTargetBehaviour::mOverrideSnappingPose
	bool ___mOverrideSnappingPose_21;
	// UnityEngine.Vector3 Vuforia.ModelTargetBehaviour::mBBoxMin
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___mBBoxMin_22;
	// UnityEngine.Vector3 Vuforia.ModelTargetBehaviour::mBBoxMax
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___mBBoxMax_23;
	// UnityEngine.Texture2D Vuforia.ModelTargetBehaviour::mPreviewImage
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___mPreviewImage_24;
	// System.Single Vuforia.ModelTargetBehaviour::mLength
	float ___mLength_25;
	// System.Single Vuforia.ModelTargetBehaviour::mWidth
	float ___mWidth_26;
	// System.Single Vuforia.ModelTargetBehaviour::mHeight
	float ___mHeight_27;
	// UnityEngine.GameObject Vuforia.ModelTargetBehaviour::m3DGuideViewModel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m3DGuideViewModel_28;
	// UnityEngine.Texture2D Vuforia.ModelTargetBehaviour::m2DGuideViewImage
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___m2DGuideViewImage_29;
	// Vuforia.ModelTargetBehaviour/GuideViewDisplayMode Vuforia.ModelTargetBehaviour::mGuideViewDisplayMode
	int32_t ___mGuideViewDisplayMode_30;
	// UnityEngine.Material Vuforia.ModelTargetBehaviour::m2DGuideViewMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m2DGuideViewMaterial_31;
	// UnityEngine.Material Vuforia.ModelTargetBehaviour::m3DGuideViewMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m3DGuideViewMaterial_32;
	// System.String Vuforia.ModelTargetBehaviour::mSelectedGuideView
	String_t* ___mSelectedGuideView_33;
	// Vuforia.Internal.TargetMotionHint Vuforia.ModelTargetBehaviour::mMotionHint
	int32_t ___mMotionHint_34;
	// Vuforia.Internal.ModelTargetTrackingMode Vuforia.ModelTargetBehaviour::mTrackingMode
	int32_t ___mTrackingMode_35;
	// Vuforia.TrackingOptimization Vuforia.ModelTargetBehaviour::mTrackingOptimization
	int32_t ___mTrackingOptimization_36;
	// System.Boolean Vuforia.ModelTargetBehaviour::mTrackingOptimizationNeedsUpgrade
	bool ___mTrackingOptimizationNeedsUpgrade_37;
	// System.Boolean Vuforia.ModelTargetBehaviour::mHasRealisticTextures
	bool ___mHasRealisticTextures_38;
	// Vuforia.ModelTargetPreview Vuforia.ModelTargetBehaviour::mPreview
	ModelTargetPreview_t2AF671789D44F5710B5694062F0DD7D6FA5AD3C0 * ___mPreview_39;
	// System.String Vuforia.ModelTargetBehaviour::mAutoValidationDataSetName
	String_t* ___mAutoValidationDataSetName_40;
	// UnityEngine.Vector3 Vuforia.ModelTargetBehaviour::mLastSize
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___mLastSize_41;
	// Vuforia.Internal.Observers.ModelTargetObserver Vuforia.ModelTargetBehaviour::mModelTargetObserver
	ModelTargetObserver_tA26329FCDA360C3E807B9DCF906995F775C132A0 * ___mModelTargetObserver_42;
	// Vuforia.IRuntimeMeshBehaviour Vuforia.ModelTargetBehaviour::mRuntimeMeshBehaviour
	RuntimeObject* ___mRuntimeMeshBehaviour_43;

public:
	inline static int32_t get_offset_of_mGuideViewRenderer_16() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mGuideViewRenderer_16)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_mGuideViewRenderer_16() const { return ___mGuideViewRenderer_16; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_mGuideViewRenderer_16() { return &___mGuideViewRenderer_16; }
	inline void set_mGuideViewRenderer_16(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___mGuideViewRenderer_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mGuideViewRenderer_16), (void*)value);
	}

	inline static int32_t get_offset_of_mBaseSize_17() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mBaseSize_17)); }
	inline float get_mBaseSize_17() const { return ___mBaseSize_17; }
	inline float* get_address_of_mBaseSize_17() { return &___mBaseSize_17; }
	inline void set_mBaseSize_17(float value)
	{
		___mBaseSize_17 = value;
	}

	inline static int32_t get_offset_of_mAspectRatioXY_18() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mAspectRatioXY_18)); }
	inline float get_mAspectRatioXY_18() const { return ___mAspectRatioXY_18; }
	inline float* get_address_of_mAspectRatioXY_18() { return &___mAspectRatioXY_18; }
	inline void set_mAspectRatioXY_18(float value)
	{
		___mAspectRatioXY_18 = value;
	}

	inline static int32_t get_offset_of_mAspectRatioXZ_19() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mAspectRatioXZ_19)); }
	inline float get_mAspectRatioXZ_19() const { return ___mAspectRatioXZ_19; }
	inline float* get_address_of_mAspectRatioXZ_19() { return &___mAspectRatioXZ_19; }
	inline void set_mAspectRatioXZ_19(float value)
	{
		___mAspectRatioXZ_19 = value;
	}

	inline static int32_t get_offset_of_mShowBoundingBox_20() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mShowBoundingBox_20)); }
	inline bool get_mShowBoundingBox_20() const { return ___mShowBoundingBox_20; }
	inline bool* get_address_of_mShowBoundingBox_20() { return &___mShowBoundingBox_20; }
	inline void set_mShowBoundingBox_20(bool value)
	{
		___mShowBoundingBox_20 = value;
	}

	inline static int32_t get_offset_of_mOverrideSnappingPose_21() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mOverrideSnappingPose_21)); }
	inline bool get_mOverrideSnappingPose_21() const { return ___mOverrideSnappingPose_21; }
	inline bool* get_address_of_mOverrideSnappingPose_21() { return &___mOverrideSnappingPose_21; }
	inline void set_mOverrideSnappingPose_21(bool value)
	{
		___mOverrideSnappingPose_21 = value;
	}

	inline static int32_t get_offset_of_mBBoxMin_22() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mBBoxMin_22)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_mBBoxMin_22() const { return ___mBBoxMin_22; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_mBBoxMin_22() { return &___mBBoxMin_22; }
	inline void set_mBBoxMin_22(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___mBBoxMin_22 = value;
	}

	inline static int32_t get_offset_of_mBBoxMax_23() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mBBoxMax_23)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_mBBoxMax_23() const { return ___mBBoxMax_23; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_mBBoxMax_23() { return &___mBBoxMax_23; }
	inline void set_mBBoxMax_23(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___mBBoxMax_23 = value;
	}

	inline static int32_t get_offset_of_mPreviewImage_24() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mPreviewImage_24)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_mPreviewImage_24() const { return ___mPreviewImage_24; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_mPreviewImage_24() { return &___mPreviewImage_24; }
	inline void set_mPreviewImage_24(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___mPreviewImage_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mPreviewImage_24), (void*)value);
	}

	inline static int32_t get_offset_of_mLength_25() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mLength_25)); }
	inline float get_mLength_25() const { return ___mLength_25; }
	inline float* get_address_of_mLength_25() { return &___mLength_25; }
	inline void set_mLength_25(float value)
	{
		___mLength_25 = value;
	}

	inline static int32_t get_offset_of_mWidth_26() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mWidth_26)); }
	inline float get_mWidth_26() const { return ___mWidth_26; }
	inline float* get_address_of_mWidth_26() { return &___mWidth_26; }
	inline void set_mWidth_26(float value)
	{
		___mWidth_26 = value;
	}

	inline static int32_t get_offset_of_mHeight_27() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mHeight_27)); }
	inline float get_mHeight_27() const { return ___mHeight_27; }
	inline float* get_address_of_mHeight_27() { return &___mHeight_27; }
	inline void set_mHeight_27(float value)
	{
		___mHeight_27 = value;
	}

	inline static int32_t get_offset_of_m3DGuideViewModel_28() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___m3DGuideViewModel_28)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m3DGuideViewModel_28() const { return ___m3DGuideViewModel_28; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m3DGuideViewModel_28() { return &___m3DGuideViewModel_28; }
	inline void set_m3DGuideViewModel_28(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m3DGuideViewModel_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m3DGuideViewModel_28), (void*)value);
	}

	inline static int32_t get_offset_of_m2DGuideViewImage_29() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___m2DGuideViewImage_29)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_m2DGuideViewImage_29() const { return ___m2DGuideViewImage_29; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_m2DGuideViewImage_29() { return &___m2DGuideViewImage_29; }
	inline void set_m2DGuideViewImage_29(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___m2DGuideViewImage_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m2DGuideViewImage_29), (void*)value);
	}

	inline static int32_t get_offset_of_mGuideViewDisplayMode_30() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mGuideViewDisplayMode_30)); }
	inline int32_t get_mGuideViewDisplayMode_30() const { return ___mGuideViewDisplayMode_30; }
	inline int32_t* get_address_of_mGuideViewDisplayMode_30() { return &___mGuideViewDisplayMode_30; }
	inline void set_mGuideViewDisplayMode_30(int32_t value)
	{
		___mGuideViewDisplayMode_30 = value;
	}

	inline static int32_t get_offset_of_m2DGuideViewMaterial_31() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___m2DGuideViewMaterial_31)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m2DGuideViewMaterial_31() const { return ___m2DGuideViewMaterial_31; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m2DGuideViewMaterial_31() { return &___m2DGuideViewMaterial_31; }
	inline void set_m2DGuideViewMaterial_31(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m2DGuideViewMaterial_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m2DGuideViewMaterial_31), (void*)value);
	}

	inline static int32_t get_offset_of_m3DGuideViewMaterial_32() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___m3DGuideViewMaterial_32)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m3DGuideViewMaterial_32() const { return ___m3DGuideViewMaterial_32; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m3DGuideViewMaterial_32() { return &___m3DGuideViewMaterial_32; }
	inline void set_m3DGuideViewMaterial_32(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m3DGuideViewMaterial_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m3DGuideViewMaterial_32), (void*)value);
	}

	inline static int32_t get_offset_of_mSelectedGuideView_33() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mSelectedGuideView_33)); }
	inline String_t* get_mSelectedGuideView_33() const { return ___mSelectedGuideView_33; }
	inline String_t** get_address_of_mSelectedGuideView_33() { return &___mSelectedGuideView_33; }
	inline void set_mSelectedGuideView_33(String_t* value)
	{
		___mSelectedGuideView_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mSelectedGuideView_33), (void*)value);
	}

	inline static int32_t get_offset_of_mMotionHint_34() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mMotionHint_34)); }
	inline int32_t get_mMotionHint_34() const { return ___mMotionHint_34; }
	inline int32_t* get_address_of_mMotionHint_34() { return &___mMotionHint_34; }
	inline void set_mMotionHint_34(int32_t value)
	{
		___mMotionHint_34 = value;
	}

	inline static int32_t get_offset_of_mTrackingMode_35() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mTrackingMode_35)); }
	inline int32_t get_mTrackingMode_35() const { return ___mTrackingMode_35; }
	inline int32_t* get_address_of_mTrackingMode_35() { return &___mTrackingMode_35; }
	inline void set_mTrackingMode_35(int32_t value)
	{
		___mTrackingMode_35 = value;
	}

	inline static int32_t get_offset_of_mTrackingOptimization_36() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mTrackingOptimization_36)); }
	inline int32_t get_mTrackingOptimization_36() const { return ___mTrackingOptimization_36; }
	inline int32_t* get_address_of_mTrackingOptimization_36() { return &___mTrackingOptimization_36; }
	inline void set_mTrackingOptimization_36(int32_t value)
	{
		___mTrackingOptimization_36 = value;
	}

	inline static int32_t get_offset_of_mTrackingOptimizationNeedsUpgrade_37() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mTrackingOptimizationNeedsUpgrade_37)); }
	inline bool get_mTrackingOptimizationNeedsUpgrade_37() const { return ___mTrackingOptimizationNeedsUpgrade_37; }
	inline bool* get_address_of_mTrackingOptimizationNeedsUpgrade_37() { return &___mTrackingOptimizationNeedsUpgrade_37; }
	inline void set_mTrackingOptimizationNeedsUpgrade_37(bool value)
	{
		___mTrackingOptimizationNeedsUpgrade_37 = value;
	}

	inline static int32_t get_offset_of_mHasRealisticTextures_38() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mHasRealisticTextures_38)); }
	inline bool get_mHasRealisticTextures_38() const { return ___mHasRealisticTextures_38; }
	inline bool* get_address_of_mHasRealisticTextures_38() { return &___mHasRealisticTextures_38; }
	inline void set_mHasRealisticTextures_38(bool value)
	{
		___mHasRealisticTextures_38 = value;
	}

	inline static int32_t get_offset_of_mPreview_39() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mPreview_39)); }
	inline ModelTargetPreview_t2AF671789D44F5710B5694062F0DD7D6FA5AD3C0 * get_mPreview_39() const { return ___mPreview_39; }
	inline ModelTargetPreview_t2AF671789D44F5710B5694062F0DD7D6FA5AD3C0 ** get_address_of_mPreview_39() { return &___mPreview_39; }
	inline void set_mPreview_39(ModelTargetPreview_t2AF671789D44F5710B5694062F0DD7D6FA5AD3C0 * value)
	{
		___mPreview_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mPreview_39), (void*)value);
	}

	inline static int32_t get_offset_of_mAutoValidationDataSetName_40() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mAutoValidationDataSetName_40)); }
	inline String_t* get_mAutoValidationDataSetName_40() const { return ___mAutoValidationDataSetName_40; }
	inline String_t** get_address_of_mAutoValidationDataSetName_40() { return &___mAutoValidationDataSetName_40; }
	inline void set_mAutoValidationDataSetName_40(String_t* value)
	{
		___mAutoValidationDataSetName_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mAutoValidationDataSetName_40), (void*)value);
	}

	inline static int32_t get_offset_of_mLastSize_41() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mLastSize_41)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_mLastSize_41() const { return ___mLastSize_41; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_mLastSize_41() { return &___mLastSize_41; }
	inline void set_mLastSize_41(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___mLastSize_41 = value;
	}

	inline static int32_t get_offset_of_mModelTargetObserver_42() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mModelTargetObserver_42)); }
	inline ModelTargetObserver_tA26329FCDA360C3E807B9DCF906995F775C132A0 * get_mModelTargetObserver_42() const { return ___mModelTargetObserver_42; }
	inline ModelTargetObserver_tA26329FCDA360C3E807B9DCF906995F775C132A0 ** get_address_of_mModelTargetObserver_42() { return &___mModelTargetObserver_42; }
	inline void set_mModelTargetObserver_42(ModelTargetObserver_tA26329FCDA360C3E807B9DCF906995F775C132A0 * value)
	{
		___mModelTargetObserver_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mModelTargetObserver_42), (void*)value);
	}

	inline static int32_t get_offset_of_mRuntimeMeshBehaviour_43() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275, ___mRuntimeMeshBehaviour_43)); }
	inline RuntimeObject* get_mRuntimeMeshBehaviour_43() const { return ___mRuntimeMeshBehaviour_43; }
	inline RuntimeObject** get_address_of_mRuntimeMeshBehaviour_43() { return &___mRuntimeMeshBehaviour_43; }
	inline void set_mRuntimeMeshBehaviour_43(RuntimeObject* value)
	{
		___mRuntimeMeshBehaviour_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mRuntimeMeshBehaviour_43), (void*)value);
	}
};


// Vuforia.VuMarkBehaviour
struct VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2  : public ScalableDataSetTrackableBehaviour_t4703A60B9EF85DCBEDB83ED8EE286EB319831E64
{
public:
	// Vuforia.Internal.TargetMotionHint Vuforia.VuMarkBehaviour::mMotionHint
	int32_t ___mMotionHint_15;
	// Vuforia.TrackingOptimization Vuforia.VuMarkBehaviour::mTrackingOptimization
	int32_t ___mTrackingOptimization_16;
	// System.Boolean Vuforia.VuMarkBehaviour::mTrackingOptimizationNeedsUpgrade
	bool ___mTrackingOptimizationNeedsUpgrade_17;
	// Vuforia.VuMarkInstanceId Vuforia.VuMarkBehaviour::<InstanceId>k__BackingField
	RuntimeObject* ___U3CInstanceIdU3Ek__BackingField_18;
	// Vuforia.Image Vuforia.VuMarkBehaviour::<InstanceImage>k__BackingField
	Image_tEA90BDBB0131379427A1282935D21313F94DFDB1 * ___U3CInstanceImageU3Ek__BackingField_19;
	// System.String Vuforia.VuMarkBehaviour::<VuMarkUserData>k__BackingField
	String_t* ___U3CVuMarkUserDataU3Ek__BackingField_20;
	// System.Single Vuforia.VuMarkBehaviour::mAspectRatio
	float ___mAspectRatio_21;
	// System.Single Vuforia.VuMarkBehaviour::mWidth
	float ___mWidth_22;
	// System.Single Vuforia.VuMarkBehaviour::mHeight
	float ___mHeight_23;
	// System.String Vuforia.VuMarkBehaviour::mPreviewImage
	String_t* ___mPreviewImage_24;
	// Vuforia.InstanceIdType Vuforia.VuMarkBehaviour::mIdType
	int32_t ___mIdType_25;
	// System.Int32 Vuforia.VuMarkBehaviour::mIdLength
	int32_t ___mIdLength_26;
	// UnityEngine.Vector2 Vuforia.VuMarkBehaviour::mOrigin
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___mOrigin_27;
	// UnityEngine.Rect Vuforia.VuMarkBehaviour::mBoundingBox
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___mBoundingBox_28;
	// System.Boolean Vuforia.VuMarkBehaviour::mTrackingFromRuntimeAppearance
	bool ___mTrackingFromRuntimeAppearance_29;
	// System.Nullable`1<System.Int32> Vuforia.VuMarkBehaviour::<RuntimeId>k__BackingField
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  ___U3CRuntimeIdU3Ek__BackingField_30;
	// Vuforia.VuMarkInstancesManager Vuforia.VuMarkBehaviour::mInstancesManager
	VuMarkInstancesManager_t901C2384A8FD62E9C62E6D18B8ED116FB9EFBC1F * ___mInstancesManager_31;
	// Vuforia.VuMarkPreview Vuforia.VuMarkBehaviour::mPreview
	VuMarkPreview_t7745561F9348795064F637DD5D2E14D07F40E8E2 * ___mPreview_32;
	// Vuforia.Internal.Observers.VuMarkObserver Vuforia.VuMarkBehaviour::mVuMarkObserver
	VuMarkObserver_tD0936F103C0041228AD63B954C3E88815B03E479 * ___mVuMarkObserver_33;
	// UnityEngine.Vector2 Vuforia.VuMarkBehaviour::mLastSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___mLastSize_34;

public:
	inline static int32_t get_offset_of_mMotionHint_15() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mMotionHint_15)); }
	inline int32_t get_mMotionHint_15() const { return ___mMotionHint_15; }
	inline int32_t* get_address_of_mMotionHint_15() { return &___mMotionHint_15; }
	inline void set_mMotionHint_15(int32_t value)
	{
		___mMotionHint_15 = value;
	}

	inline static int32_t get_offset_of_mTrackingOptimization_16() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mTrackingOptimization_16)); }
	inline int32_t get_mTrackingOptimization_16() const { return ___mTrackingOptimization_16; }
	inline int32_t* get_address_of_mTrackingOptimization_16() { return &___mTrackingOptimization_16; }
	inline void set_mTrackingOptimization_16(int32_t value)
	{
		___mTrackingOptimization_16 = value;
	}

	inline static int32_t get_offset_of_mTrackingOptimizationNeedsUpgrade_17() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mTrackingOptimizationNeedsUpgrade_17)); }
	inline bool get_mTrackingOptimizationNeedsUpgrade_17() const { return ___mTrackingOptimizationNeedsUpgrade_17; }
	inline bool* get_address_of_mTrackingOptimizationNeedsUpgrade_17() { return &___mTrackingOptimizationNeedsUpgrade_17; }
	inline void set_mTrackingOptimizationNeedsUpgrade_17(bool value)
	{
		___mTrackingOptimizationNeedsUpgrade_17 = value;
	}

	inline static int32_t get_offset_of_U3CInstanceIdU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___U3CInstanceIdU3Ek__BackingField_18)); }
	inline RuntimeObject* get_U3CInstanceIdU3Ek__BackingField_18() const { return ___U3CInstanceIdU3Ek__BackingField_18; }
	inline RuntimeObject** get_address_of_U3CInstanceIdU3Ek__BackingField_18() { return &___U3CInstanceIdU3Ek__BackingField_18; }
	inline void set_U3CInstanceIdU3Ek__BackingField_18(RuntimeObject* value)
	{
		___U3CInstanceIdU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceIdU3Ek__BackingField_18), (void*)value);
	}

	inline static int32_t get_offset_of_U3CInstanceImageU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___U3CInstanceImageU3Ek__BackingField_19)); }
	inline Image_tEA90BDBB0131379427A1282935D21313F94DFDB1 * get_U3CInstanceImageU3Ek__BackingField_19() const { return ___U3CInstanceImageU3Ek__BackingField_19; }
	inline Image_tEA90BDBB0131379427A1282935D21313F94DFDB1 ** get_address_of_U3CInstanceImageU3Ek__BackingField_19() { return &___U3CInstanceImageU3Ek__BackingField_19; }
	inline void set_U3CInstanceImageU3Ek__BackingField_19(Image_tEA90BDBB0131379427A1282935D21313F94DFDB1 * value)
	{
		___U3CInstanceImageU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceImageU3Ek__BackingField_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CVuMarkUserDataU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___U3CVuMarkUserDataU3Ek__BackingField_20)); }
	inline String_t* get_U3CVuMarkUserDataU3Ek__BackingField_20() const { return ___U3CVuMarkUserDataU3Ek__BackingField_20; }
	inline String_t** get_address_of_U3CVuMarkUserDataU3Ek__BackingField_20() { return &___U3CVuMarkUserDataU3Ek__BackingField_20; }
	inline void set_U3CVuMarkUserDataU3Ek__BackingField_20(String_t* value)
	{
		___U3CVuMarkUserDataU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CVuMarkUserDataU3Ek__BackingField_20), (void*)value);
	}

	inline static int32_t get_offset_of_mAspectRatio_21() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mAspectRatio_21)); }
	inline float get_mAspectRatio_21() const { return ___mAspectRatio_21; }
	inline float* get_address_of_mAspectRatio_21() { return &___mAspectRatio_21; }
	inline void set_mAspectRatio_21(float value)
	{
		___mAspectRatio_21 = value;
	}

	inline static int32_t get_offset_of_mWidth_22() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mWidth_22)); }
	inline float get_mWidth_22() const { return ___mWidth_22; }
	inline float* get_address_of_mWidth_22() { return &___mWidth_22; }
	inline void set_mWidth_22(float value)
	{
		___mWidth_22 = value;
	}

	inline static int32_t get_offset_of_mHeight_23() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mHeight_23)); }
	inline float get_mHeight_23() const { return ___mHeight_23; }
	inline float* get_address_of_mHeight_23() { return &___mHeight_23; }
	inline void set_mHeight_23(float value)
	{
		___mHeight_23 = value;
	}

	inline static int32_t get_offset_of_mPreviewImage_24() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mPreviewImage_24)); }
	inline String_t* get_mPreviewImage_24() const { return ___mPreviewImage_24; }
	inline String_t** get_address_of_mPreviewImage_24() { return &___mPreviewImage_24; }
	inline void set_mPreviewImage_24(String_t* value)
	{
		___mPreviewImage_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mPreviewImage_24), (void*)value);
	}

	inline static int32_t get_offset_of_mIdType_25() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mIdType_25)); }
	inline int32_t get_mIdType_25() const { return ___mIdType_25; }
	inline int32_t* get_address_of_mIdType_25() { return &___mIdType_25; }
	inline void set_mIdType_25(int32_t value)
	{
		___mIdType_25 = value;
	}

	inline static int32_t get_offset_of_mIdLength_26() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mIdLength_26)); }
	inline int32_t get_mIdLength_26() const { return ___mIdLength_26; }
	inline int32_t* get_address_of_mIdLength_26() { return &___mIdLength_26; }
	inline void set_mIdLength_26(int32_t value)
	{
		___mIdLength_26 = value;
	}

	inline static int32_t get_offset_of_mOrigin_27() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mOrigin_27)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_mOrigin_27() const { return ___mOrigin_27; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_mOrigin_27() { return &___mOrigin_27; }
	inline void set_mOrigin_27(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___mOrigin_27 = value;
	}

	inline static int32_t get_offset_of_mBoundingBox_28() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mBoundingBox_28)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_mBoundingBox_28() const { return ___mBoundingBox_28; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_mBoundingBox_28() { return &___mBoundingBox_28; }
	inline void set_mBoundingBox_28(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___mBoundingBox_28 = value;
	}

	inline static int32_t get_offset_of_mTrackingFromRuntimeAppearance_29() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mTrackingFromRuntimeAppearance_29)); }
	inline bool get_mTrackingFromRuntimeAppearance_29() const { return ___mTrackingFromRuntimeAppearance_29; }
	inline bool* get_address_of_mTrackingFromRuntimeAppearance_29() { return &___mTrackingFromRuntimeAppearance_29; }
	inline void set_mTrackingFromRuntimeAppearance_29(bool value)
	{
		___mTrackingFromRuntimeAppearance_29 = value;
	}

	inline static int32_t get_offset_of_U3CRuntimeIdU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___U3CRuntimeIdU3Ek__BackingField_30)); }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  get_U3CRuntimeIdU3Ek__BackingField_30() const { return ___U3CRuntimeIdU3Ek__BackingField_30; }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * get_address_of_U3CRuntimeIdU3Ek__BackingField_30() { return &___U3CRuntimeIdU3Ek__BackingField_30; }
	inline void set_U3CRuntimeIdU3Ek__BackingField_30(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  value)
	{
		___U3CRuntimeIdU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_mInstancesManager_31() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mInstancesManager_31)); }
	inline VuMarkInstancesManager_t901C2384A8FD62E9C62E6D18B8ED116FB9EFBC1F * get_mInstancesManager_31() const { return ___mInstancesManager_31; }
	inline VuMarkInstancesManager_t901C2384A8FD62E9C62E6D18B8ED116FB9EFBC1F ** get_address_of_mInstancesManager_31() { return &___mInstancesManager_31; }
	inline void set_mInstancesManager_31(VuMarkInstancesManager_t901C2384A8FD62E9C62E6D18B8ED116FB9EFBC1F * value)
	{
		___mInstancesManager_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mInstancesManager_31), (void*)value);
	}

	inline static int32_t get_offset_of_mPreview_32() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mPreview_32)); }
	inline VuMarkPreview_t7745561F9348795064F637DD5D2E14D07F40E8E2 * get_mPreview_32() const { return ___mPreview_32; }
	inline VuMarkPreview_t7745561F9348795064F637DD5D2E14D07F40E8E2 ** get_address_of_mPreview_32() { return &___mPreview_32; }
	inline void set_mPreview_32(VuMarkPreview_t7745561F9348795064F637DD5D2E14D07F40E8E2 * value)
	{
		___mPreview_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mPreview_32), (void*)value);
	}

	inline static int32_t get_offset_of_mVuMarkObserver_33() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mVuMarkObserver_33)); }
	inline VuMarkObserver_tD0936F103C0041228AD63B954C3E88815B03E479 * get_mVuMarkObserver_33() const { return ___mVuMarkObserver_33; }
	inline VuMarkObserver_tD0936F103C0041228AD63B954C3E88815B03E479 ** get_address_of_mVuMarkObserver_33() { return &___mVuMarkObserver_33; }
	inline void set_mVuMarkObserver_33(VuMarkObserver_tD0936F103C0041228AD63B954C3E88815B03E479 * value)
	{
		___mVuMarkObserver_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mVuMarkObserver_33), (void*)value);
	}

	inline static int32_t get_offset_of_mLastSize_34() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2, ___mLastSize_34)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_mLastSize_34() const { return ___mLastSize_34; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_mLastSize_34() { return &___mLastSize_34; }
	inline void set_mLastSize_34(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___mLastSize_34 = value;
	}
};


// <Module>


// <Module>


// System.Object


// System.Object

struct Il2CppArrayBounds;

// System.Array

struct Il2CppArrayBounds;

// System.Array


// VFX.CameraUtil


// VFX.CameraUtil


// VFX.MaterialUtil


// VFX.MaterialUtil


// VFX.MatrixUtil


// VFX.MatrixUtil


// System.String

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.String


// UnityEngine.Events.UnityEventBase


// UnityEngine.Events.UnityEventBase


// System.ValueType


// System.ValueType


// VFX.VuforiaCameraUtil

struct VuforiaCameraUtil_t2170A5AB3BE377F2E409B7BAC5EB8663062E2275_StaticFields
{
public:
	// UnityEngine.Camera VFX.VuforiaCameraUtil::mVuforiaBehaviourCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___mVuforiaBehaviourCamera_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviourCamera_0() { return static_cast<int32_t>(offsetof(VuforiaCameraUtil_t2170A5AB3BE377F2E409B7BAC5EB8663062E2275_StaticFields, ___mVuforiaBehaviourCamera_0)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_mVuforiaBehaviourCamera_0() const { return ___mVuforiaBehaviourCamera_0; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_mVuforiaBehaviourCamera_0() { return &___mVuforiaBehaviourCamera_0; }
	inline void set_mVuforiaBehaviourCamera_0(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___mVuforiaBehaviourCamera_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mVuforiaBehaviourCamera_0), (void*)value);
	}
};


// VFX.VuforiaCameraUtil


// VFX.VuforiaObserverUtil


// VFX.VuforiaObserverUtil


// System.Nullable`1<System.Int32>


// System.Nullable`1<System.Int32>


// System.Boolean

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Boolean


// UnityEngine.Color


// UnityEngine.Color


// VFX.DynamicScalarProperty


// VFX.DynamicScalarProperty


// System.Enum

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};


// System.Enum


// System.Int32


// System.Int32


// System.IntPtr

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.IntPtr


// UnityEngine.Mathf

struct Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_StaticFields
{
public:
	// System.Single UnityEngine.Mathf::Epsilon
	float ___Epsilon_0;

public:
	inline static int32_t get_offset_of_Epsilon_0() { return static_cast<int32_t>(offsetof(Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_StaticFields, ___Epsilon_0)); }
	inline float get_Epsilon_0() const { return ___Epsilon_0; }
	inline float* get_address_of_Epsilon_0() { return &___Epsilon_0; }
	inline void set_Epsilon_0(float value)
	{
		___Epsilon_0 = value;
	}
};


// UnityEngine.Mathf


// UnityEngine.Matrix4x4

struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.Matrix4x4


// UnityEngine.Quaternion

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Quaternion


// UnityEngine.Rect


// UnityEngine.Rect


// System.Single


// System.Single


// VFX.StaticScalarProperty


// VFX.StaticScalarProperty


// UnityEngine.Events.UnityEvent


// UnityEngine.Events.UnityEvent


// UnityEngine.Vector2

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector2


// UnityEngine.Vector3

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector3


// UnityEngine.Vector4

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// UnityEngine.Vector4


// System.Void


// System.Void


// UnityEngine.Bounds


// UnityEngine.Bounds


// System.Delegate


// System.Delegate


// UnityEngine.DepthTextureMode


// UnityEngine.DepthTextureMode


// VFX.DynamicColor


// VFX.DynamicColor


// VFX.DynamicVectorProperty


// VFX.DynamicVectorProperty


// VFX.FilePathMode


// VFX.FilePathMode


// Vuforia.ImageTargetType


// Vuforia.ImageTargetType


// Vuforia.InstanceIdType


// Vuforia.InstanceIdType


// Vuforia.Internal.ModelTargetTrackingMode


// Vuforia.Internal.ModelTargetTrackingMode


// UnityEngine.Object

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};


// UnityEngine.Object


// VFX.StaticColor


// VFX.StaticColor


// VFX.StaticVectorProperty


// VFX.StaticVectorProperty


// Vuforia.Status


// Vuforia.Status


// Vuforia.StatusInfo


// Vuforia.StatusInfo


// Vuforia.StorageType


// Vuforia.StorageType


// Vuforia.Internal.TargetMotionHint


// Vuforia.Internal.TargetMotionHint


// Vuforia.TrackingOptimization


// Vuforia.TrackingOptimization


// VFX.TransitionMode


// VFX.TransitionMode


// VFX.VFXCenterMode


// VFX.VFXCenterMode


// Vuforia.VuforiaInitError


// Vuforia.VuforiaInitError


// Vuforia.WorldCenterMode


// Vuforia.WorldCenterMode


// Vuforia.ModelTargetBehaviour/GuideViewDisplayMode


// Vuforia.ModelTargetBehaviour/GuideViewDisplayMode


// UnityEngine.Component


// UnityEngine.Component


// UnityEngine.GameObject


// UnityEngine.GameObject


// UnityEngine.Material


// UnityEngine.Material


// System.MulticastDelegate


// System.MulticastDelegate


// Vuforia.TargetStatus


// Vuforia.TargetStatus


// Vuforia.VuforiaApplication

struct VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1_StaticFields
{
public:
	// Vuforia.VuforiaApplication Vuforia.VuforiaApplication::sInstance
	VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1_StaticFields, ___sInstance_0)); }
	inline VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1 * get_sInstance_0() const { return ___sInstance_0; }
	inline VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sInstance_0), (void*)value);
	}
};


// Vuforia.VuforiaApplication


// System.Action`2<Vuforia.ObserverBehaviour,Vuforia.TargetStatus>


// System.Action`2<Vuforia.ObserverBehaviour,Vuforia.TargetStatus>


// UnityEngine.Behaviour


// UnityEngine.Behaviour


// UnityEngine.Renderer


// UnityEngine.Renderer


// UnityEngine.Transform


// UnityEngine.Transform


// UnityEngine.Camera

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.Camera


// UnityEngine.MonoBehaviour


// UnityEngine.MonoBehaviour


// VFX.CameraDepthMode


// VFX.CameraDepthMode


// VFX.ShaderVFX


// VFX.ShaderVFX


// Vuforia.VuforiaMonoBehaviour


// Vuforia.VuforiaMonoBehaviour


// Vuforia.ObserverBehaviour


// Vuforia.ObserverBehaviour


// VFX.TargetVFX


// VFX.TargetVFX


// Vuforia.VuforiaBehaviour

struct VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407_StaticFields
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::sInstance
	VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407 * ___sInstance_4;

public:
	inline static int32_t get_offset_of_sInstance_4() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407_StaticFields, ___sInstance_4)); }
	inline VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407 * get_sInstance_4() const { return ___sInstance_4; }
	inline VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407 ** get_address_of_sInstance_4() { return &___sInstance_4; }
	inline void set_sInstance_4(VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407 * value)
	{
		___sInstance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sInstance_4), (void*)value);
	}
};


// Vuforia.VuforiaBehaviour


// Vuforia.DataSetTrackableBehaviour

struct DataSetTrackableBehaviour_tDACD5B676DD2E9EE2943323AC60A131390E676BA_StaticFields
{
public:
	// System.Single Vuforia.DataSetTrackableBehaviour::VirtualSceneScaleFactor
	float ___VirtualSceneScaleFactor_13;

public:
	inline static int32_t get_offset_of_VirtualSceneScaleFactor_13() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_tDACD5B676DD2E9EE2943323AC60A131390E676BA_StaticFields, ___VirtualSceneScaleFactor_13)); }
	inline float get_VirtualSceneScaleFactor_13() const { return ___VirtualSceneScaleFactor_13; }
	inline float* get_address_of_VirtualSceneScaleFactor_13() { return &___VirtualSceneScaleFactor_13; }
	inline void set_VirtualSceneScaleFactor_13(float value)
	{
		___VirtualSceneScaleFactor_13 = value;
	}
};


// Vuforia.DataSetTrackableBehaviour


// Vuforia.AreaTargetBehaviour


// Vuforia.AreaTargetBehaviour


// Vuforia.ScalableDataSetTrackableBehaviour


// Vuforia.ScalableDataSetTrackableBehaviour


// Vuforia.CylinderTargetBehaviour


// Vuforia.CylinderTargetBehaviour


// Vuforia.ImageTargetBehaviour


// Vuforia.ImageTargetBehaviour


// Vuforia.ModelTargetBehaviour

struct ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275_StaticFields
{
public:
	// System.String Vuforia.ModelTargetBehaviour::GUIDE_VIEW_RENDERER_NAME
	String_t* ___GUIDE_VIEW_RENDERER_NAME_15;

public:
	inline static int32_t get_offset_of_GUIDE_VIEW_RENDERER_NAME_15() { return static_cast<int32_t>(offsetof(ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275_StaticFields, ___GUIDE_VIEW_RENDERER_NAME_15)); }
	inline String_t* get_GUIDE_VIEW_RENDERER_NAME_15() const { return ___GUIDE_VIEW_RENDERER_NAME_15; }
	inline String_t** get_address_of_GUIDE_VIEW_RENDERER_NAME_15() { return &___GUIDE_VIEW_RENDERER_NAME_15; }
	inline void set_GUIDE_VIEW_RENDERER_NAME_15(String_t* value)
	{
		___GUIDE_VIEW_RENDERER_NAME_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GUIDE_VIEW_RENDERER_NAME_15), (void*)value);
	}
};


// Vuforia.ModelTargetBehaviour


// Vuforia.VuMarkBehaviour


// Vuforia.VuMarkBehaviour

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Renderer[]
struct RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * m_Items[1];

public:
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Material[]
struct MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Material_t8927C00353A72755313F046D0CE85178AE8218EE * m_Items[1];

public:
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// VFX.DynamicColor[]
struct DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  m_Items[1];

public:
	inline DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
	}
	inline DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
	}
};
// VFX.DynamicVectorProperty[]
struct DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  m_Items[1];

public:
	inline DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
	}
	inline DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
	}
};
// VFX.DynamicScalarProperty[]
struct DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  m_Items[1];

public:
	inline DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
	}
	inline DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
	}
};
// VFX.StaticScalarProperty[]
struct StaticScalarPropertyU5BU5D_t81263A8DC17430EDE9ED5D9BDC3D590C8D5555C9  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E  m_Items[1];

public:
	inline StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
	}
	inline StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
	}
};
// VFX.StaticVectorProperty[]
struct StaticVectorPropertyU5BU5D_tFED79A0033EA7A3C25F29A20C8DDF49E6B945A36  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE  m_Items[1];

public:
	inline StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
	}
	inline StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
	}
};
// VFX.StaticColor[]
struct StaticColorU5BU5D_tB852F8A761474DEA5054061DD1F8B267C56E6FF8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F  m_Items[1];

public:
	inline StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
	}
	inline StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___name_0), (void*)NULL);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* GameObject_GetComponentsInChildren_TisRuntimeObject_m6662AE3C936281A25097CCBD9098A9F85C69279A_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* GameObject_GetComponentsInChildren_TisRuntimeObject_m1162B29A5E7F58C6D71090AE00D175818BC0B955_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___includeInactive0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInParent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponentInParent_TisRuntimeObject_mADA186D1675BEA6779C469918206294354385EC3_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void System.Action`2<System.Object,Vuforia.TargetStatus>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m524A56D70AB0876D7D763AAF48D8A7D051AB0635_gshared (Action_2_tEC03432E1591AF6C19EAE8E64F8502FBEEAAB87C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);

// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_depthTextureMode_m2D4631800947438BE9A7697778E2CB0E38083CF1 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Camera_get_projectionMatrix_mDB77E3A7F71CEF085797BCE58FAC78058C5D6756 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, const RuntimeMethod* method);
// System.Single VFX.MatrixUtil::GetHorizontalFovRadians(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MatrixUtil_GetHorizontalFovRadians_m5D87D6BC1CE7C2074DDB718CB68EBF898158BD0D (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___projectionMatrix0, const RuntimeMethod* method);
// System.Single VFX.MatrixUtil::GetVerticalFovRadians(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MatrixUtil_GetVerticalFovRadians_mA0E8FE025A354CCB596100AA17BEF147C210C4DB (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___projectionMatrix0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void VFX.MaterialUtil::ApplyMaterial(UnityEngine.Renderer,UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialUtil_ApplyMaterial_m7831BB04523583FA950AC3A85DCA217388750CD3 (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ___aRenderer0, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material1, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Renderer>()
inline RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* GameObject_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mDFDC7E58FE89016C2A8E421AEA268944FC0F1FD6 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m6662AE3C936281A25097CCBD9098A9F85C69279A_gshared)(__this, method);
}
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_sharedMaterial_m1E66766F93E95F692C3C9C2C09AFD795B156678B (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___value0, const RuntimeMethod* method);
// UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* Renderer_get_sharedMaterials_m9B2D432CA8AD8CEC4348E61789CC1BB0C3A00AFD (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_sharedMaterials_m9838EC09412E988925C4670E8E355E5EEFE35A25 (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* ___value0, const RuntimeMethod* method);
// System.Void VFX.MaterialUtil::ApplyAlpha(UnityEngine.Renderer,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialUtil_ApplyAlpha_mB27BEAF53795DBF0962AA2CD8DC91668F6B31B38 (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ___aRenderer0, float ___alpha1, const RuntimeMethod* method);
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * Renderer_get_sharedMaterial_m42DF538F0C6EA249B1FB626485D45D083BA74FCC (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Material::HasProperty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Material_HasProperty_mB6F155CD45C688DA232B56BD1A74474C224BE37E (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, String_t* ___name0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Material::get_color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Material_get_color_m7926F7BE68B4D000306738C1EAABEB7ADFB97821 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_set_color_mC3C88E2389B7132EBF3EB0D1F040545176B795C0 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetVector(System.String,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetVector_mCB22CD5FDA6D8C7C282D7998A9244E12CC293F0D (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, String_t* ___name0, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetFloat_mBE01E05D49E5C7045E010F49A38E96B101D82768 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, String_t* ___name0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetColor_m5CAAF4A8D7F839597B4E14588E341462EEB81698 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, String_t* ___name0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value1, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Matrix4x4_get_identity_m8E1969E6DB24BE34842F2F2D10D7E3D0AF15007A (const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Transform_get_localToWorldMatrix_m6B810B0F20BA5DE48009461A4D662DD8BFF6A3CC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Transform_get_worldToLocalMatrix_mE22FDE24767E1DE402D3E7A1C9803379B2E8399D (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Matrix4x4_op_Multiply_mB760A95995CDC5E31F18D76CE8806DF2C160DD4B (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___lhs0, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  Matrix4x4_GetColumn_m5CAA237D7FD65AA772B84A1134E8B0551F9F8480 (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * __this, int32_t ___index0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector4_op_Implicit_mFAF4066991B0091223DB22E35C4290C43E5913AB (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___v0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_LookRotation_m57B6FBE5D29E0EA56C7537456F8E30F182134B39 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forward0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upwards1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Matrix4x4_get_Item_mA0E634EF5A723EA9DD824391D4C62F4100C64813 (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void VFX.ShaderVFX::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_Play_m72C1E7E60D6C926DE3F1BB3B521C5D545790FBC9 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mF558623BBB4EE65C8810243B05ED204A9E8D6FD1 (const RuntimeMethod* method);
// System.Void VFX.ShaderVFX::UpdateDynamicScalarProperties(System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_UpdateDynamicScalarProperties_m37DF39590DE58AD7E4DE8798A8D3B5CC5EC04EC0 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, float* ___maxTime0, const RuntimeMethod* method);
// System.Void VFX.ShaderVFX::UpdateDynamicVectorProperties(System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_UpdateDynamicVectorProperties_mFC501F25780E0ACC10326775FA29DF680DF7B667 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, float* ___maxTime0, const RuntimeMethod* method);
// System.Void VFX.ShaderVFX::UpdateDynamicColors(System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_UpdateDynamicColors_m141A35CB6F0A1E6C2C7706232FDF412DF2696012 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, float* ___maxTime0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, const RuntimeMethod* method);
// System.Void VFX.ShaderVFX::Rewind()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_Rewind_mEDF042F61293E7EE2E5EB624CDBAA0A16023E31A (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method);
// System.Void VFX.ShaderVFX::Pause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_Pause_m1AD628112529913D6169E3A230CFA1604A3A3A56 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method);
// System.Void VFX.ShaderVFX::InitMaterial()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_InitMaterial_mCF4D5E111B66F2B7853AA266F450BDF3D7005EE5 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method);
// System.Void VFX.ShaderVFX::InitStaticProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_InitStaticProperties_m3AB1B50B9F3F3FD9D5783815F5CB48D82103807C (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method);
// System.Void VFX.ShaderVFX::InitDynamicProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_InitDynamicProperties_m6AEFC94FBC6B76E1FCCD7BFA07D2F5E6F63DEE02 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method);
// System.Single VFX.ShaderVFX::SetMaxTimeAndGetLerpValue(System.Single&,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ShaderVFX_SetMaxTimeAndGetLerpValue_m6E10A14B06144F9F242E15ECDF3BB90ACC0A6483 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, float* ___maxTime0, float ___delay1, float ___duration2, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_Lerp_m6320057807E1F335970F168403C601EBD2B92062 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___a0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void VFX.MaterialUtil::TrySetColorProperty(UnityEngine.Material,System.String,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialUtil_TrySetColorProperty_mB063CAB2A94E153D8C0272E2E3F2950E4EB420C7 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material0, String_t* ___propertyName1, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m5E223DB365EAC8F6625C169E927527FFB8CC88DB_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method);
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  Vector4_op_Implicit_mA9B2E82825C2543A2B3F6207EDAC76614A77EA1E (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method);
// System.Void VFX.MaterialUtil::TrySetVector4Property(UnityEngine.Material,System.String,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialUtil_TrySetVector4Property_m2036427B77E884AED74B6654D2C353C2485F7F86 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material0, String_t* ___propertyName1, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___value2, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Lerp_m04D5C368C4E4F1AB78230C6809A6651951A52C86 (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method);
// System.Void VFX.MaterialUtil::TrySetFloatProperty(UnityEngine.Material,System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialUtil_TrySetFloatProperty_m82611514F3F1235245B165AF05F3930AD40EB0DE (Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material0, String_t* ___propertyName1, float ___value2, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Max_m5C96B726079E95BB1A1DC60532553CB723D24C79 (float ___a0, float ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m831CBA1D198C3CDE660E8172A67A4E41BD0D0171 (float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::SmoothStep(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_SmoothStep_m90F874839A4085EBA0AF938ADCD2C05F77F7A963 (float ___from0, float ___to1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m5952BCE5DC0EF798B31FA983B9CE42A5A1F82DE1 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material__ctor_mD0C3D9CFAFE0FB858D864092467387D7FA178245 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___source0, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
inline RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* GameObject_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mDF527A06E3315D1A7F828664F20B35662B0A52A6 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___includeInactive0, const RuntimeMethod* method)
{
	return ((  RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, bool, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m1162B29A5E7F58C6D71090AE00D175818BC0B955_gshared)(__this, ___includeInactive0, method);
}
// System.Void VFX.ShaderVFX::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_Awake_m7AA549E6EF3CDE422212410D6C601DA96942D7AA (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInParent<Vuforia.ObserverBehaviour>()
inline ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * Component_GetComponentInParent_TisObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274_mCE544666A6285AE628139A24DDEF8131B0CAE6AA (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentInParent_TisRuntimeObject_mADA186D1675BEA6779C469918206294354385EC3_gshared)(__this, method);
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_mA477FDA9C0B96C627C085E9EB431EB394B2EBBE0 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void System.Action`2<Vuforia.ObserverBehaviour,Vuforia.TargetStatus>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m6702F5465C5156B1FB05A678988FF9A80D94AE06 (Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_2__ctor_m524A56D70AB0876D7D763AAF48D8A7D051AB0635_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Vuforia.ObserverBehaviour::add_OnTargetStatusChanged(System.Action`2<Vuforia.ObserverBehaviour,Vuforia.TargetStatus>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObserverBehaviour_add_OnTargetStatusChanged_m35984866CE532D434875F398D32362E05D90197F (ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * __this, Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4 * ___value0, const RuntimeMethod* method);
// System.Void VFX.ShaderVFX::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_Update_m40A13C7DC127AB3AA63AF22A1980F951449E48D9 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method);
// Vuforia.VuforiaApplication Vuforia.VuforiaApplication::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1 * VuforiaApplication_get_Instance_mAE3192D808C9F2B8F7EACDC490DD5F2E72DF8A75 (const RuntimeMethod* method);
// System.Boolean Vuforia.VuforiaApplication::get_IsRunning()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VuforiaApplication_get_IsRunning_mE574E58FDF23D44974AD60CD2B3707A9EA86BC02 (VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1 * __this, const RuntimeMethod* method);
// System.Void VFX.TargetVFX::UpdateShaderCenterAndAxis()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVFX_UpdateShaderCenterAndAxis_m6D4F079A02F16ABA5F6640F21A3A909BACE643B5 (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, const RuntimeMethod* method);
// System.Void VFX.TargetVFX::UpdateShaderScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVFX_UpdateShaderScale_m14C9D840F99D8B1DE1FB87A01A012070B740B82C (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, const RuntimeMethod* method);
// System.Void Vuforia.ObserverBehaviour::remove_OnTargetStatusChanged(System.Action`2<Vuforia.ObserverBehaviour,Vuforia.TargetStatus>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObserverBehaviour_remove_OnTargetStatusChanged_m0D9E06F1D07D727D888A974EFCF948AAC02F5BCC (ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * __this, Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4 * ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 VFX.VuforiaObserverUtil::GetTargetSize(Vuforia.ObserverBehaviour)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  VuforiaObserverUtil_GetTargetSize_mBF9FA09D93599B7000922B6C7C6D852E07C74949 (ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * ___observer0, const RuntimeMethod* method);
// System.Boolean VFX.TargetVFX::IsUnTracked(Vuforia.TargetStatus)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TargetVFX_IsUnTracked_m706016647B2DF9E29105F351BB08DC15DF89F764 (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  ___targetStatus0, const RuntimeMethod* method);
// System.Boolean VFX.TargetVFX::IsTracked(Vuforia.TargetStatus)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TargetVFX_IsTracked_m885C810BE91774EC58936F694042A78B41F28478 (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  ___targetStatus0, const RuntimeMethod* method);
// System.Void VFX.TargetVFX::OnTargetFound()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVFX_OnTargetFound_mD2BB51BCFAF8C3066A27ACEF54DD0FF216BB1D2C (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, const RuntimeMethod* method);
// Vuforia.Status Vuforia.TargetStatus::get_Status()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t TargetStatus_get_Status_mD745DEB3CFE4D18A1E973178AA8CA9BB73178D5E_inline (TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_Invoke_m4AAB759653B1C6FB0653527F4DDC72D1E9162CC4 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, String_t* ___methodName0, float ___time1, const RuntimeMethod* method);
// UnityEngine.Vector3 VFX.TargetVFX::GetCenterPointWCS()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  TargetVFX_GetCenterPointWCS_m6A62C57D93BF95E1F93585E65E072512C88E4950 (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, const RuntimeMethod* method);
// System.Void VFX.TargetVFX::GetAxisVectors(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVFX_GetAxisVectors_mDAEF2F3A57F51777FA1CA49CB9315796D2633CED (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___axisX0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___axisY1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___axisZ2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_right_m60959C1C1EF0F694D71E1569160D40B1DA768931 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_up_mD84FDFCD32FC48C865A89FD4251232E2A9D7015A (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_forward_mA6722B0932DA770D5C34C9E28D0E40220F099D50 (const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Camera VFX.VuforiaCameraUtil::GetCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * VuforiaCameraUtil_GetCamera_m19816CEAC1F906DE62B392F97D8053884E271200 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_TransformPoint_m68AF95765A9279192E601208A9C5170027A5F0D2 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m92B6E46FC9A637D20B3A4C1FFAEABFCE095DD4C6 (const RuntimeMethod* method);
// Vuforia.TargetStatus Vuforia.TargetStatus::get_NotObserved()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  TargetStatus_get_NotObserved_mB76C66781AFB21647114F43B899E57422A61EE90 (const RuntimeMethod* method);
// System.Void VFX.ShaderVFX::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX__ctor_m5BEEA87E5640310F903984FB48972724AA17A475 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_m6D7EBC0E6D7E0CE1E9671D21DE14C9158AFB88B2 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407 * VuforiaBehaviour_get_Instance_mF563DFB36205C62EAEC611673162CF5016DA5A03 (const RuntimeMethod* method);
// UnityEngine.Vector2 Vuforia.ImageTargetBehaviour::GetSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ImageTargetBehaviour_GetSize_mA174549F8B005D0ED36D782B91D22E5D668EBD34 (ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_mC91C21911FDB26792369C645DAABBC43B3DE9932_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method);
// System.Single Vuforia.CylinderTargetBehaviour::get_SideLength()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float CylinderTargetBehaviour_get_SideLength_m6189CDEA133898BED4791772B39C9DA596C21755_inline (CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3 * __this, const RuntimeMethod* method);
// System.Single Vuforia.CylinderTargetBehaviour::get_BottomDiameter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CylinderTargetBehaviour_get_BottomDiameter_mF34465750334C97B4F115D3190CA2D7CECF6C57A (CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3 * __this, const RuntimeMethod* method);
// System.Single Vuforia.CylinderTargetBehaviour::get_TopDiameter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CylinderTargetBehaviour_get_TopDiameter_m7E190963401492CDE34330527C15EEC393B41C94 (CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3 * __this, const RuntimeMethod* method);
// UnityEngine.Bounds Vuforia.ModelTargetBehaviour::GetBoundingBox()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ModelTargetBehaviour_GetBoundingBox_m829B1E301CFBB50F375C59A1B40710449FAB56F8 (ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, const RuntimeMethod* method);
// UnityEngine.Bounds Vuforia.AreaTargetBehaviour::GetBoundingBox()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  AreaTargetBehaviour_GetBoundingBox_m7F45017B673F5C2CF4BB3B3F8117C2DC34F88DA9_inline (AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 Vuforia.VuMarkBehaviour::GetSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  VuMarkBehaviour_GetSize_m113E22E6E5B01275F70F64673B95D753C95E2A2E (VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_one_mFA8E564BB81364E4E65551816F3631176E7F58E7 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VFX.CameraDepthMode::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraDepthMode_Awake_m768DC20EEE4DAE32CAA73052E0467F50A77266C8 (CameraDepthMode_t0B03DACDEA955D5DB48A8D455B28E8A945B45FC4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GetComponent<Camera>().depthTextureMode = depthMode;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0;
		L_0 = Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320(__this, /*hidden argument*/Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320_RuntimeMethod_var);
		int32_t L_1 = __this->get_depthMode_4();
		NullCheck(L_0);
		Camera_set_depthTextureMode_m2D4631800947438BE9A7697778E2CB0E38083CF1(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VFX.CameraDepthMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraDepthMode__ctor_m8C044FEB0BD6994E281E5E078AD91D2D1A6A2732 (CameraDepthMode_t0B03DACDEA955D5DB48A8D455B28E8A945B45FC4 * __this, const RuntimeMethod* method)
{
	{
		// public DepthTextureMode depthMode = DepthTextureMode.Depth;
		__this->set_depthMode_4(1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single VFX.CameraUtil::GetHorizontalFovRadians(UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CameraUtil_GetHorizontalFovRadians_m1D9984182F0FA5E5B51D03EB819B79D9EA4B83A2 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___camera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return camera != null ? MatrixUtil.GetHorizontalFovRadians(camera.projectionMatrix) : 0;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = ___camera0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (0.0f);
	}

IL_000f:
	{
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_2 = ___camera0;
		NullCheck(L_2);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_3;
		L_3 = Camera_get_projectionMatrix_mDB77E3A7F71CEF085797BCE58FAC78058C5D6756(L_2, /*hidden argument*/NULL);
		float L_4;
		L_4 = MatrixUtil_GetHorizontalFovRadians_m5D87D6BC1CE7C2074DDB718CB68EBF898158BD0D(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single VFX.CameraUtil::GetVerticalFovRadians(UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CameraUtil_GetVerticalFovRadians_m807C945267F641096E42436DA6D27E72D99D657B (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___camera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return camera != null ? MatrixUtil.GetVerticalFovRadians(camera.projectionMatrix) : 0;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = ___camera0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (0.0f);
	}

IL_000f:
	{
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_2 = ___camera0;
		NullCheck(L_2);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_3;
		L_3 = Camera_get_projectionMatrix_mDB77E3A7F71CEF085797BCE58FAC78058C5D6756(L_2, /*hidden argument*/NULL);
		float L_4;
		L_4 = MatrixUtil_GetVerticalFovRadians_mA0E8FE025A354CCB596100AA17BEF147C210C4DB(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: VFX.DynamicColor
IL2CPP_EXTERN_C void DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshal_pinvoke(const DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E& unmarshaled, DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	marshaled.___startColor_1 = unmarshaled.get_startColor_1();
	marshaled.___endColor_2 = unmarshaled.get_endColor_2();
	marshaled.___delay_3 = unmarshaled.get_delay_3();
	marshaled.___duration_4 = unmarshaled.get_duration_4();
}
IL2CPP_EXTERN_C void DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshal_pinvoke_back(const DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshaled_pinvoke& marshaled, DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  unmarshaled_startColor_temp_1;
	memset((&unmarshaled_startColor_temp_1), 0, sizeof(unmarshaled_startColor_temp_1));
	unmarshaled_startColor_temp_1 = marshaled.___startColor_1;
	unmarshaled.set_startColor_1(unmarshaled_startColor_temp_1);
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  unmarshaled_endColor_temp_2;
	memset((&unmarshaled_endColor_temp_2), 0, sizeof(unmarshaled_endColor_temp_2));
	unmarshaled_endColor_temp_2 = marshaled.___endColor_2;
	unmarshaled.set_endColor_2(unmarshaled_endColor_temp_2);
	float unmarshaled_delay_temp_3 = 0.0f;
	unmarshaled_delay_temp_3 = marshaled.___delay_3;
	unmarshaled.set_delay_3(unmarshaled_delay_temp_3);
	float unmarshaled_duration_temp_4 = 0.0f;
	unmarshaled_duration_temp_4 = marshaled.___duration_4;
	unmarshaled.set_duration_4(unmarshaled_duration_temp_4);
}
// Conversion method for clean up from marshalling of: VFX.DynamicColor
IL2CPP_EXTERN_C void DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshal_pinvoke_cleanup(DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// Conversion methods for marshalling of: VFX.DynamicColor
IL2CPP_EXTERN_C void DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshal_com(const DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E& unmarshaled, DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	marshaled.___startColor_1 = unmarshaled.get_startColor_1();
	marshaled.___endColor_2 = unmarshaled.get_endColor_2();
	marshaled.___delay_3 = unmarshaled.get_delay_3();
	marshaled.___duration_4 = unmarshaled.get_duration_4();
}
IL2CPP_EXTERN_C void DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshal_com_back(const DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshaled_com& marshaled, DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  unmarshaled_startColor_temp_1;
	memset((&unmarshaled_startColor_temp_1), 0, sizeof(unmarshaled_startColor_temp_1));
	unmarshaled_startColor_temp_1 = marshaled.___startColor_1;
	unmarshaled.set_startColor_1(unmarshaled_startColor_temp_1);
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  unmarshaled_endColor_temp_2;
	memset((&unmarshaled_endColor_temp_2), 0, sizeof(unmarshaled_endColor_temp_2));
	unmarshaled_endColor_temp_2 = marshaled.___endColor_2;
	unmarshaled.set_endColor_2(unmarshaled_endColor_temp_2);
	float unmarshaled_delay_temp_3 = 0.0f;
	unmarshaled_delay_temp_3 = marshaled.___delay_3;
	unmarshaled.set_delay_3(unmarshaled_delay_temp_3);
	float unmarshaled_duration_temp_4 = 0.0f;
	unmarshaled_duration_temp_4 = marshaled.___duration_4;
	unmarshaled.set_duration_4(unmarshaled_duration_temp_4);
}
// Conversion method for clean up from marshalling of: VFX.DynamicColor
IL2CPP_EXTERN_C void DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshal_com_cleanup(DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: VFX.DynamicScalarProperty
IL2CPP_EXTERN_C void DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshal_pinvoke(const DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41& unmarshaled, DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	marshaled.___startValue_1 = unmarshaled.get_startValue_1();
	marshaled.___endValue_2 = unmarshaled.get_endValue_2();
	marshaled.___delay_3 = unmarshaled.get_delay_3();
	marshaled.___duration_4 = unmarshaled.get_duration_4();
}
IL2CPP_EXTERN_C void DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshal_pinvoke_back(const DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshaled_pinvoke& marshaled, DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	float unmarshaled_startValue_temp_1 = 0.0f;
	unmarshaled_startValue_temp_1 = marshaled.___startValue_1;
	unmarshaled.set_startValue_1(unmarshaled_startValue_temp_1);
	float unmarshaled_endValue_temp_2 = 0.0f;
	unmarshaled_endValue_temp_2 = marshaled.___endValue_2;
	unmarshaled.set_endValue_2(unmarshaled_endValue_temp_2);
	float unmarshaled_delay_temp_3 = 0.0f;
	unmarshaled_delay_temp_3 = marshaled.___delay_3;
	unmarshaled.set_delay_3(unmarshaled_delay_temp_3);
	float unmarshaled_duration_temp_4 = 0.0f;
	unmarshaled_duration_temp_4 = marshaled.___duration_4;
	unmarshaled.set_duration_4(unmarshaled_duration_temp_4);
}
// Conversion method for clean up from marshalling of: VFX.DynamicScalarProperty
IL2CPP_EXTERN_C void DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshal_pinvoke_cleanup(DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// Conversion methods for marshalling of: VFX.DynamicScalarProperty
IL2CPP_EXTERN_C void DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshal_com(const DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41& unmarshaled, DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	marshaled.___startValue_1 = unmarshaled.get_startValue_1();
	marshaled.___endValue_2 = unmarshaled.get_endValue_2();
	marshaled.___delay_3 = unmarshaled.get_delay_3();
	marshaled.___duration_4 = unmarshaled.get_duration_4();
}
IL2CPP_EXTERN_C void DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshal_com_back(const DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshaled_com& marshaled, DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	float unmarshaled_startValue_temp_1 = 0.0f;
	unmarshaled_startValue_temp_1 = marshaled.___startValue_1;
	unmarshaled.set_startValue_1(unmarshaled_startValue_temp_1);
	float unmarshaled_endValue_temp_2 = 0.0f;
	unmarshaled_endValue_temp_2 = marshaled.___endValue_2;
	unmarshaled.set_endValue_2(unmarshaled_endValue_temp_2);
	float unmarshaled_delay_temp_3 = 0.0f;
	unmarshaled_delay_temp_3 = marshaled.___delay_3;
	unmarshaled.set_delay_3(unmarshaled_delay_temp_3);
	float unmarshaled_duration_temp_4 = 0.0f;
	unmarshaled_duration_temp_4 = marshaled.___duration_4;
	unmarshaled.set_duration_4(unmarshaled_duration_temp_4);
}
// Conversion method for clean up from marshalling of: VFX.DynamicScalarProperty
IL2CPP_EXTERN_C void DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshal_com_cleanup(DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: VFX.DynamicVectorProperty
IL2CPP_EXTERN_C void DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshal_pinvoke(const DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE& unmarshaled, DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	marshaled.___startValue_1 = unmarshaled.get_startValue_1();
	marshaled.___endValue_2 = unmarshaled.get_endValue_2();
	marshaled.___delay_3 = unmarshaled.get_delay_3();
	marshaled.___duration_4 = unmarshaled.get_duration_4();
}
IL2CPP_EXTERN_C void DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshal_pinvoke_back(const DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshaled_pinvoke& marshaled, DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  unmarshaled_startValue_temp_1;
	memset((&unmarshaled_startValue_temp_1), 0, sizeof(unmarshaled_startValue_temp_1));
	unmarshaled_startValue_temp_1 = marshaled.___startValue_1;
	unmarshaled.set_startValue_1(unmarshaled_startValue_temp_1);
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  unmarshaled_endValue_temp_2;
	memset((&unmarshaled_endValue_temp_2), 0, sizeof(unmarshaled_endValue_temp_2));
	unmarshaled_endValue_temp_2 = marshaled.___endValue_2;
	unmarshaled.set_endValue_2(unmarshaled_endValue_temp_2);
	float unmarshaled_delay_temp_3 = 0.0f;
	unmarshaled_delay_temp_3 = marshaled.___delay_3;
	unmarshaled.set_delay_3(unmarshaled_delay_temp_3);
	float unmarshaled_duration_temp_4 = 0.0f;
	unmarshaled_duration_temp_4 = marshaled.___duration_4;
	unmarshaled.set_duration_4(unmarshaled_duration_temp_4);
}
// Conversion method for clean up from marshalling of: VFX.DynamicVectorProperty
IL2CPP_EXTERN_C void DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshal_pinvoke_cleanup(DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// Conversion methods for marshalling of: VFX.DynamicVectorProperty
IL2CPP_EXTERN_C void DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshal_com(const DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE& unmarshaled, DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	marshaled.___startValue_1 = unmarshaled.get_startValue_1();
	marshaled.___endValue_2 = unmarshaled.get_endValue_2();
	marshaled.___delay_3 = unmarshaled.get_delay_3();
	marshaled.___duration_4 = unmarshaled.get_duration_4();
}
IL2CPP_EXTERN_C void DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshal_com_back(const DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshaled_com& marshaled, DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  unmarshaled_startValue_temp_1;
	memset((&unmarshaled_startValue_temp_1), 0, sizeof(unmarshaled_startValue_temp_1));
	unmarshaled_startValue_temp_1 = marshaled.___startValue_1;
	unmarshaled.set_startValue_1(unmarshaled_startValue_temp_1);
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  unmarshaled_endValue_temp_2;
	memset((&unmarshaled_endValue_temp_2), 0, sizeof(unmarshaled_endValue_temp_2));
	unmarshaled_endValue_temp_2 = marshaled.___endValue_2;
	unmarshaled.set_endValue_2(unmarshaled_endValue_temp_2);
	float unmarshaled_delay_temp_3 = 0.0f;
	unmarshaled_delay_temp_3 = marshaled.___delay_3;
	unmarshaled.set_delay_3(unmarshaled_delay_temp_3);
	float unmarshaled_duration_temp_4 = 0.0f;
	unmarshaled_duration_temp_4 = marshaled.___duration_4;
	unmarshaled.set_duration_4(unmarshaled_duration_temp_4);
}
// Conversion method for clean up from marshalling of: VFX.DynamicVectorProperty
IL2CPP_EXTERN_C void DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshal_com_cleanup(DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VFX.MaterialUtil::ApplyMaterial(UnityEngine.GameObject,UnityEngine.Material,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialUtil_ApplyMaterial_m14E588D20F59FF1961AD91860E973022A2365EA0 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___gameObject0, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material1, bool ___applyToChildren2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mDFDC7E58FE89016C2A8E421AEA268944FC0F1FD6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * V_0 = NULL;
	RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* V_1 = NULL;
	int32_t V_2 = 0;
	{
		// if (!gameObject || !material)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___gameObject0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = ___material1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0011;
		}
	}

IL_0010:
	{
		// return;
		return;
	}

IL_0011:
	{
		// var aRenderer = gameObject.GetComponent<Renderer>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = ___gameObject0;
		NullCheck(L_4);
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_5;
		L_5 = GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466_RuntimeMethod_var);
		V_0 = L_5;
		// if (aRenderer)
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0027;
		}
	}
	{
		// ApplyMaterial(aRenderer, material);
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_8 = V_0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_9 = ___material1;
		MaterialUtil_ApplyMaterial_m7831BB04523583FA950AC3A85DCA217388750CD3(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0027:
	{
		// if (applyToChildren)
		bool L_10 = ___applyToChildren2;
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		// var renderers = gameObject.GetComponentsInChildren<Renderer>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11 = ___gameObject0;
		NullCheck(L_11);
		RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* L_12;
		L_12 = GameObject_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mDFDC7E58FE89016C2A8E421AEA268944FC0F1FD6(L_11, /*hidden argument*/GameObject_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mDFDC7E58FE89016C2A8E421AEA268944FC0F1FD6_RuntimeMethod_var);
		// foreach (var childRenderer in renderers)
		V_1 = L_12;
		V_2 = 0;
		goto IL_0042;
	}

IL_0035:
	{
		// foreach (var childRenderer in renderers)
		RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* L_13 = V_1;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		// ApplyMaterial(childRenderer, material);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_17 = ___material1;
		MaterialUtil_ApplyMaterial_m7831BB04523583FA950AC3A85DCA217388750CD3(L_16, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0042:
	{
		// foreach (var childRenderer in renderers)
		int32_t L_19 = V_2;
		RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* L_20 = V_1;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length))))))
		{
			goto IL_0035;
		}
	}

IL_0048:
	{
		// }
		return;
	}
}
// System.Void VFX.MaterialUtil::ApplyMaterial(UnityEngine.Renderer,UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialUtil_ApplyMaterial_m7831BB04523583FA950AC3A85DCA217388750CD3 (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ___aRenderer0, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// if (!aRenderer || !material)
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_0 = ___aRenderer0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = ___material1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0011;
		}
	}

IL_0010:
	{
		// return;
		return;
	}

IL_0011:
	{
		// aRenderer.sharedMaterial = material;
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_4 = ___aRenderer0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_5 = ___material1;
		NullCheck(L_4);
		Renderer_set_sharedMaterial_m1E66766F93E95F692C3C9C2C09AFD795B156678B(L_4, L_5, /*hidden argument*/NULL);
		// if (aRenderer.sharedMaterials != null && aRenderer.sharedMaterials.Length > 0)
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_6 = ___aRenderer0;
		NullCheck(L_6);
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_7;
		L_7 = Renderer_get_sharedMaterials_m9B2D432CA8AD8CEC4348E61789CC1BB0C3A00AFD(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_8 = ___aRenderer0;
		NullCheck(L_8);
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_9;
		L_9 = Renderer_get_sharedMaterials_m9B2D432CA8AD8CEC4348E61789CC1BB0C3A00AFD(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		if (!(((RuntimeArray*)L_9)->max_length))
		{
			goto IL_0050;
		}
	}
	{
		// var materials = new Material[aRenderer.sharedMaterials.Length];
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_10 = ___aRenderer0;
		NullCheck(L_10);
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_11;
		L_11 = Renderer_get_sharedMaterials_m9B2D432CA8AD8CEC4348E61789CC1BB0C3A00AFD(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_12 = (MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492*)(MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492*)SZArrayNew(MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length))));
		V_0 = L_12;
		// for (int i = 0; i < materials.Length; i++)
		V_1 = 0;
		goto IL_0043;
	}

IL_003b:
	{
		// materials[i] = material;
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_13 = V_0;
		int32_t L_14 = V_1;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_15 = ___material1;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Material_t8927C00353A72755313F046D0CE85178AE8218EE *)L_15);
		// for (int i = 0; i < materials.Length; i++)
		int32_t L_16 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_0043:
	{
		// for (int i = 0; i < materials.Length; i++)
		int32_t L_17 = V_1;
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_18 = V_0;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length))))))
		{
			goto IL_003b;
		}
	}
	{
		// aRenderer.sharedMaterials = materials;
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_19 = ___aRenderer0;
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_20 = V_0;
		NullCheck(L_19);
		Renderer_set_sharedMaterials_m9838EC09412E988925C4670E8E355E5EEFE35A25(L_19, L_20, /*hidden argument*/NULL);
	}

IL_0050:
	{
		// }
		return;
	}
}
// System.Void VFX.MaterialUtil::ApplyAlpha(UnityEngine.GameObject,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialUtil_ApplyAlpha_m69FE6BC56298EBB7CAF3E9D84CE8BD93CEB2D7B1 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___gameObject0, float ___alpha1, bool ___applyToChildren2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mDFDC7E58FE89016C2A8E421AEA268944FC0F1FD6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * V_0 = NULL;
	RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* V_1 = NULL;
	int32_t V_2 = 0;
	{
		// if (!gameObject)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___gameObject0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// var aRenderer = gameObject.GetComponent<Renderer>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = ___gameObject0;
		NullCheck(L_2);
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_3;
		L_3 = GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466(L_2, /*hidden argument*/GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466_RuntimeMethod_var);
		V_0 = L_3;
		// if (aRenderer)
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_001f;
		}
	}
	{
		// ApplyAlpha(aRenderer, alpha);
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_6 = V_0;
		float L_7 = ___alpha1;
		MaterialUtil_ApplyAlpha_mB27BEAF53795DBF0962AA2CD8DC91668F6B31B38(L_6, L_7, /*hidden argument*/NULL);
	}

IL_001f:
	{
		// if (applyToChildren)
		bool L_8 = ___applyToChildren2;
		if (!L_8)
		{
			goto IL_0040;
		}
	}
	{
		// var renderers = gameObject.GetComponentsInChildren<Renderer>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9 = ___gameObject0;
		NullCheck(L_9);
		RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* L_10;
		L_10 = GameObject_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mDFDC7E58FE89016C2A8E421AEA268944FC0F1FD6(L_9, /*hidden argument*/GameObject_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mDFDC7E58FE89016C2A8E421AEA268944FC0F1FD6_RuntimeMethod_var);
		// foreach (var childRenderer in renderers)
		V_1 = L_10;
		V_2 = 0;
		goto IL_003a;
	}

IL_002d:
	{
		// foreach (var childRenderer in renderers)
		RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* L_11 = V_1;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		// ApplyAlpha(childRenderer, alpha);
		float L_15 = ___alpha1;
		MaterialUtil_ApplyAlpha_mB27BEAF53795DBF0962AA2CD8DC91668F6B31B38(L_14, L_15, /*hidden argument*/NULL);
		int32_t L_16 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_003a:
	{
		// foreach (var childRenderer in renderers)
		int32_t L_17 = V_2;
		RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* L_18 = V_1;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length))))))
		{
			goto IL_002d;
		}
	}

IL_0040:
	{
		// }
		return;
	}
}
// System.Void VFX.MaterialUtil::ApplyAlpha(UnityEngine.Renderer,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialUtil_ApplyAlpha_mB27BEAF53795DBF0962AA2CD8DC91668F6B31B38 (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ___aRenderer0, float ___alpha1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		s_Il2CppMethodInitialized = true;
	}
	MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* V_0 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_1;
	memset((&V_1), 0, sizeof(V_1));
	MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* V_2 = NULL;
	int32_t V_3 = 0;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * V_4 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_5;
	memset((&V_5), 0, sizeof(V_5));
	{
		// if (aRenderer.sharedMaterial && aRenderer.sharedMaterial.HasProperty("_Color"))
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_0 = ___aRenderer0;
		NullCheck(L_0);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_1;
		L_1 = Renderer_get_sharedMaterial_m42DF538F0C6EA249B1FB626485D45D083BA74FCC(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004e;
		}
	}
	{
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_3 = ___aRenderer0;
		NullCheck(L_3);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_4;
		L_4 = Renderer_get_sharedMaterial_m42DF538F0C6EA249B1FB626485D45D083BA74FCC(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5;
		L_5 = Material_HasProperty_mB6F155CD45C688DA232B56BD1A74474C224BE37E(L_4, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004e;
		}
	}
	{
		// var color = aRenderer.sharedMaterial.color;
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_6 = ___aRenderer0;
		NullCheck(L_6);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_7;
		L_7 = Renderer_get_sharedMaterial_m42DF538F0C6EA249B1FB626485D45D083BA74FCC(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_8;
		L_8 = Material_get_color_m7926F7BE68B4D000306738C1EAABEB7ADFB97821(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		// aRenderer.sharedMaterial.color = new Color(color.r, color.g, color.b, alpha);
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_9 = ___aRenderer0;
		NullCheck(L_9);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_10;
		L_10 = Renderer_get_sharedMaterial_m42DF538F0C6EA249B1FB626485D45D083BA74FCC(L_9, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_11 = V_1;
		float L_12 = L_11.get_r_0();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_13 = V_1;
		float L_14 = L_13.get_g_1();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_15 = V_1;
		float L_16 = L_15.get_b_2();
		float L_17 = ___alpha1;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_18;
		memset((&L_18), 0, sizeof(L_18));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_18), L_12, L_14, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_10);
		Material_set_color_mC3C88E2389B7132EBF3EB0D1F040545176B795C0(L_10, L_18, /*hidden argument*/NULL);
	}

IL_004e:
	{
		// var materials = aRenderer.sharedMaterials;
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_19 = ___aRenderer0;
		NullCheck(L_19);
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_20;
		L_20 = Renderer_get_sharedMaterials_m9B2D432CA8AD8CEC4348E61789CC1BB0C3A00AFD(L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		// if (materials != null && materials.Length > 0)
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_21 = V_0;
		if (!L_21)
		{
			goto IL_00aa;
		}
	}
	{
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_22 = V_0;
		NullCheck(L_22);
		if (!(((RuntimeArray*)L_22)->max_length))
		{
			goto IL_00aa;
		}
	}
	{
		// foreach (var material in materials)
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_23 = V_0;
		V_2 = L_23;
		V_3 = 0;
		goto IL_00a4;
	}

IL_0062:
	{
		// foreach (var material in materials)
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_24 = V_2;
		int32_t L_25 = V_3;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		V_4 = L_27;
		// if (material.HasProperty("_Color"))
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_28 = V_4;
		NullCheck(L_28);
		bool L_29;
		L_29 = Material_HasProperty_mB6F155CD45C688DA232B56BD1A74474C224BE37E(L_28, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00a0;
		}
	}
	{
		// var color = material.color;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_30 = V_4;
		NullCheck(L_30);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_31;
		L_31 = Material_get_color_m7926F7BE68B4D000306738C1EAABEB7ADFB97821(L_30, /*hidden argument*/NULL);
		V_5 = L_31;
		// material.color = new Color(color.r, color.g, color.b, alpha);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_32 = V_4;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_33 = V_5;
		float L_34 = L_33.get_r_0();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_35 = V_5;
		float L_36 = L_35.get_g_1();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_37 = V_5;
		float L_38 = L_37.get_b_2();
		float L_39 = ___alpha1;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_40;
		memset((&L_40), 0, sizeof(L_40));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_40), L_34, L_36, L_38, L_39, /*hidden argument*/NULL);
		NullCheck(L_32);
		Material_set_color_mC3C88E2389B7132EBF3EB0D1F040545176B795C0(L_32, L_40, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		int32_t L_41 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_41, (int32_t)1));
	}

IL_00a4:
	{
		// foreach (var material in materials)
		int32_t L_42 = V_3;
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_43 = V_2;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_43)->max_length))))))
		{
			goto IL_0062;
		}
	}

IL_00aa:
	{
		// }
		return;
	}
}
// System.Void VFX.MaterialUtil::TrySetVector4Property(UnityEngine.Material,System.String,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialUtil_TrySetVector4Property_m2036427B77E884AED74B6654D2C353C2485F7F86 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material0, String_t* ___propertyName1, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___value2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (material != null && material.HasProperty(propertyName))
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_0 = ___material0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = ___material0;
		String_t* L_3 = ___propertyName1;
		NullCheck(L_2);
		bool L_4;
		L_4 = Material_HasProperty_mB6F155CD45C688DA232B56BD1A74474C224BE37E(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001a;
		}
	}
	{
		// material.SetVector(propertyName, value);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_5 = ___material0;
		String_t* L_6 = ___propertyName1;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_7 = ___value2;
		NullCheck(L_5);
		Material_SetVector_mCB22CD5FDA6D8C7C282D7998A9244E12CC293F0D(L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_001a:
	{
		// }
		return;
	}
}
// System.Void VFX.MaterialUtil::TrySetFloatProperty(UnityEngine.Material,System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialUtil_TrySetFloatProperty_m82611514F3F1235245B165AF05F3930AD40EB0DE (Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material0, String_t* ___propertyName1, float ___value2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (material != null && material.HasProperty(propertyName))
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_0 = ___material0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = ___material0;
		String_t* L_3 = ___propertyName1;
		NullCheck(L_2);
		bool L_4;
		L_4 = Material_HasProperty_mB6F155CD45C688DA232B56BD1A74474C224BE37E(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001a;
		}
	}
	{
		// material.SetFloat(propertyName, value);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_5 = ___material0;
		String_t* L_6 = ___propertyName1;
		float L_7 = ___value2;
		NullCheck(L_5);
		Material_SetFloat_mBE01E05D49E5C7045E010F49A38E96B101D82768(L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_001a:
	{
		// }
		return;
	}
}
// System.Void VFX.MaterialUtil::TrySetColorProperty(UnityEngine.Material,System.String,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialUtil_TrySetColorProperty_mB063CAB2A94E153D8C0272E2E3F2950E4EB420C7 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material0, String_t* ___propertyName1, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (material && material.HasProperty(propertyName))
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_0 = ___material0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = ___material0;
		String_t* L_3 = ___propertyName1;
		NullCheck(L_2);
		bool L_4;
		L_4 = Material_HasProperty_mB6F155CD45C688DA232B56BD1A74474C224BE37E(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0019;
		}
	}
	{
		// material.SetColor(propertyName, color);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_5 = ___material0;
		String_t* L_6 = ___propertyName1;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_7 = ___color2;
		NullCheck(L_5);
		Material_SetColor_m5CAAF4A8D7F839597B4E14588E341462EEB81698(L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Matrix4x4 VFX.MatrixUtil::MatrixFromArray(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  MatrixUtil_MatrixFromArray_m946E83A5FCB42DB3966D16EBC7DE919A42371ACE (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___array0, const RuntimeMethod* method)
{
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var matrix = new Matrix4x4();
		il2cpp_codegen_initobj((&V_0), sizeof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 ));
		// matrix.m00 = array[0];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_0 = ___array0;
		NullCheck(L_0);
		int32_t L_1 = 0;
		float L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		(&V_0)->set_m00_0(L_2);
		// matrix.m10 = array[1];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_3 = ___array0;
		NullCheck(L_3);
		int32_t L_4 = 1;
		float L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		(&V_0)->set_m10_1(L_5);
		// matrix.m20 = array[2];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_6 = ___array0;
		NullCheck(L_6);
		int32_t L_7 = 2;
		float L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		(&V_0)->set_m20_2(L_8);
		// matrix.m30 = array[3];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = 3;
		float L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		(&V_0)->set_m30_3(L_11);
		// matrix.m01 = array[4];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_12 = ___array0;
		NullCheck(L_12);
		int32_t L_13 = 4;
		float L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		(&V_0)->set_m01_4(L_14);
		// matrix.m11 = array[5];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_15 = ___array0;
		NullCheck(L_15);
		int32_t L_16 = 5;
		float L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		(&V_0)->set_m11_5(L_17);
		// matrix.m21 = array[6];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_18 = ___array0;
		NullCheck(L_18);
		int32_t L_19 = 6;
		float L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		(&V_0)->set_m21_6(L_20);
		// matrix.m31 = array[7];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_21 = ___array0;
		NullCheck(L_21);
		int32_t L_22 = 7;
		float L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		(&V_0)->set_m31_7(L_23);
		// matrix.m02 = array[8];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_24 = ___array0;
		NullCheck(L_24);
		int32_t L_25 = 8;
		float L_26 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		(&V_0)->set_m02_8(L_26);
		// matrix.m12 = array[9];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_27 = ___array0;
		NullCheck(L_27);
		int32_t L_28 = ((int32_t)9);
		float L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		(&V_0)->set_m12_9(L_29);
		// matrix.m22 = array[10];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_30 = ___array0;
		NullCheck(L_30);
		int32_t L_31 = ((int32_t)10);
		float L_32 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		(&V_0)->set_m22_10(L_32);
		// matrix.m32 = array[11];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_33 = ___array0;
		NullCheck(L_33);
		int32_t L_34 = ((int32_t)11);
		float L_35 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		(&V_0)->set_m32_11(L_35);
		// matrix.m03 = array[12];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_36 = ___array0;
		NullCheck(L_36);
		int32_t L_37 = ((int32_t)12);
		float L_38 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		(&V_0)->set_m03_12(L_38);
		// matrix.m13 = array[13];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_39 = ___array0;
		NullCheck(L_39);
		int32_t L_40 = ((int32_t)13);
		float L_41 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		(&V_0)->set_m13_13(L_41);
		// matrix.m23 = array[14];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_42 = ___array0;
		NullCheck(L_42);
		int32_t L_43 = ((int32_t)14);
		float L_44 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		(&V_0)->set_m23_14(L_44);
		// matrix.m33 = array[15];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_45 = ___array0;
		NullCheck(L_45);
		int32_t L_46 = ((int32_t)15);
		float L_47 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		(&V_0)->set_m33_15(L_47);
		// return matrix;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_48 = V_0;
		return L_48;
	}
}
// UnityEngine.Matrix4x4 VFX.MatrixUtil::GetFromToMatrix(UnityEngine.Transform,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  MatrixUtil_GetFromToMatrix_m199EAB1081834A5130D0057E142983A97DB0DFFA (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___from0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___to1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  G_B6_0;
	memset((&G_B6_0), 0, sizeof(G_B6_0));
	{
		// var m1 = from ? from.localToWorldMatrix : Matrix4x4.identity;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___from0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_2;
		L_2 = Matrix4x4_get_identity_m8E1969E6DB24BE34842F2F2D10D7E3D0AF15007A(/*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0015;
	}

IL_000f:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = ___from0;
		NullCheck(L_3);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_4;
		L_4 = Transform_get_localToWorldMatrix_m6B810B0F20BA5DE48009461A4D662DD8BFF6A3CC(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0015:
	{
		V_0 = G_B3_0;
		// var m2 = to ? to.worldToLocalMatrix : Matrix4x4.identity;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = ___to1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0025;
		}
	}
	{
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_7;
		L_7 = Matrix4x4_get_identity_m8E1969E6DB24BE34842F2F2D10D7E3D0AF15007A(/*hidden argument*/NULL);
		G_B6_0 = L_7;
		goto IL_002b;
	}

IL_0025:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8 = ___to1;
		NullCheck(L_8);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_9;
		L_9 = Transform_get_worldToLocalMatrix_mE22FDE24767E1DE402D3E7A1C9803379B2E8399D(L_8, /*hidden argument*/NULL);
		G_B6_0 = L_9;
	}

IL_002b:
	{
		// return m2 * m1;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_10 = V_0;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_11;
		L_11 = Matrix4x4_op_Multiply_mB760A95995CDC5E31F18D76CE8806DF2C160DD4B(G_B6_0, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// UnityEngine.Quaternion VFX.MatrixUtil::GetRotation(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  MatrixUtil_GetRotation_mE36F82D3ADB6A6347C2DE322ADD2340F0298A949 (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___matrix0, const RuntimeMethod* method)
{
	{
		// return Quaternion.LookRotation(matrix.GetColumn(2), matrix.GetColumn(1));
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_0;
		L_0 = Matrix4x4_GetColumn_m5CAA237D7FD65AA772B84A1134E8B0551F9F8480((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)(&___matrix0), 2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector4_op_Implicit_mFAF4066991B0091223DB22E35C4290C43E5913AB(L_0, /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_2;
		L_2 = Matrix4x4_GetColumn_m5CAA237D7FD65AA772B84A1134E8B0551F9F8480((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)(&___matrix0), 1, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Vector4_op_Implicit_mFAF4066991B0091223DB22E35C4290C43E5913AB(L_2, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_4;
		L_4 = Quaternion_LookRotation_m57B6FBE5D29E0EA56C7537456F8E30F182134B39(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector3 VFX.MatrixUtil::GetPosition(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  MatrixUtil_GetPosition_m2FC6AAEB0FDD729E8C6D7FB59A87AE771DAA109A (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___matrix0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// var x = matrix.m03;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_0 = ___matrix0;
		float L_1 = L_0.get_m03_12();
		// var y = matrix.m13;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_2 = ___matrix0;
		float L_3 = L_2.get_m13_13();
		V_0 = L_3;
		// var z = matrix.m23;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_4 = ___matrix0;
		float L_5 = L_4.get_m23_14();
		V_1 = L_5;
		// return new Vector3(x, y, z);
		float L_6 = V_0;
		float L_7 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_8), L_1, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector3 VFX.MatrixUtil::GetScale(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  MatrixUtil_GetScale_mE023A02C901A5C88CC4D1CD4D33529CCE000ED7B (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___matrix0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// var x = Mathf.Sqrt(matrix.m00 * matrix.m00 + matrix.m01 * matrix.m01 + matrix.m02 * matrix.m02);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_0 = ___matrix0;
		float L_1 = L_0.get_m00_0();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_2 = ___matrix0;
		float L_3 = L_2.get_m00_0();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_4 = ___matrix0;
		float L_5 = L_4.get_m01_4();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_6 = ___matrix0;
		float L_7 = L_6.get_m01_4();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_8 = ___matrix0;
		float L_9 = L_8.get_m02_8();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_10 = ___matrix0;
		float L_11 = L_10.get_m02_8();
		float L_12;
		L_12 = sqrtf(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)))));
		// var z = Mathf.Sqrt(matrix.m10 * matrix.m10 + matrix.m11 * matrix.m11 + matrix.m12 * matrix.m12);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_13 = ___matrix0;
		float L_14 = L_13.get_m10_1();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_15 = ___matrix0;
		float L_16 = L_15.get_m10_1();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_17 = ___matrix0;
		float L_18 = L_17.get_m11_5();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_19 = ___matrix0;
		float L_20 = L_19.get_m11_5();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_21 = ___matrix0;
		float L_22 = L_21.get_m12_9();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_23 = ___matrix0;
		float L_24 = L_23.get_m12_9();
		float L_25;
		L_25 = sqrtf(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_14, (float)L_16)), (float)((float)il2cpp_codegen_multiply((float)L_18, (float)L_20)))), (float)((float)il2cpp_codegen_multiply((float)L_22, (float)L_24)))));
		V_0 = L_25;
		// var y = Mathf.Sqrt(matrix.m20 * matrix.m20 + matrix.m21 * matrix.m21 + matrix.m22 * matrix.m22);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_26 = ___matrix0;
		float L_27 = L_26.get_m20_2();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_28 = ___matrix0;
		float L_29 = L_28.get_m20_2();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_30 = ___matrix0;
		float L_31 = L_30.get_m21_6();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_32 = ___matrix0;
		float L_33 = L_32.get_m21_6();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_34 = ___matrix0;
		float L_35 = L_34.get_m22_10();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_36 = ___matrix0;
		float L_37 = L_36.get_m22_10();
		float L_38;
		L_38 = sqrtf(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_27, (float)L_29)), (float)((float)il2cpp_codegen_multiply((float)L_31, (float)L_33)))), (float)((float)il2cpp_codegen_multiply((float)L_35, (float)L_37)))));
		V_1 = L_38;
		// return new Vector3(x, y, z);
		float L_39 = V_1;
		float L_40 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_41;
		memset((&L_41), 0, sizeof(L_41));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_41), L_12, L_39, L_40, /*hidden argument*/NULL);
		return L_41;
	}
}
// System.Single VFX.MatrixUtil::GetHorizontalFovRadians(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MatrixUtil_GetHorizontalFovRadians_m5D87D6BC1CE7C2074DDB718CB68EBF898158BD0D (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___projectionMatrix0, const RuntimeMethod* method)
{
	{
		// return 2 * Mathf.Atan(1.0f / projectionMatrix[0]);
		float L_0;
		L_0 = Matrix4x4_get_Item_mA0E634EF5A723EA9DD824391D4C62F4100C64813((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)(&___projectionMatrix0), 0, /*hidden argument*/NULL);
		float L_1;
		L_1 = atanf(((float)((float)(1.0f)/(float)L_0)));
		return ((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_1));
	}
}
// System.Single VFX.MatrixUtil::GetHorizontalFovDegrees(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MatrixUtil_GetHorizontalFovDegrees_m082F68DFA5934B930CEC0EC1C5285E48D6E9D51C (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___projectionMatrix0, const RuntimeMethod* method)
{
	{
		// return GetHorizontalFovRadians(projectionMatrix) * Mathf.Rad2Deg;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_0 = ___projectionMatrix0;
		float L_1;
		L_1 = MatrixUtil_GetHorizontalFovRadians_m5D87D6BC1CE7C2074DDB718CB68EBF898158BD0D(L_0, /*hidden argument*/NULL);
		return ((float)il2cpp_codegen_multiply((float)L_1, (float)(57.2957802f)));
	}
}
// System.Single VFX.MatrixUtil::GetVerticalFovRadians(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MatrixUtil_GetVerticalFovRadians_mA0E8FE025A354CCB596100AA17BEF147C210C4DB (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___projectionMatrix0, const RuntimeMethod* method)
{
	{
		// return 2 * Mathf.Atan(1.0f / projectionMatrix[5]);
		float L_0;
		L_0 = Matrix4x4_get_Item_mA0E634EF5A723EA9DD824391D4C62F4100C64813((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)(&___projectionMatrix0), 5, /*hidden argument*/NULL);
		float L_1;
		L_1 = atanf(((float)((float)(1.0f)/(float)L_0)));
		return ((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_1));
	}
}
// System.Single VFX.MatrixUtil::GetVerticalFovDegrees(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MatrixUtil_GetVerticalFovDegrees_m48A11682D3CAA973F688AFDE60B1CBEC96C5089E (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___projectionMatrix0, const RuntimeMethod* method)
{
	{
		// return GetVerticalFovRadians(projectionMatrix) * Mathf.Rad2Deg;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_0 = ___projectionMatrix0;
		float L_1;
		L_1 = MatrixUtil_GetVerticalFovRadians_mA0E8FE025A354CCB596100AA17BEF147C210C4DB(L_0, /*hidden argument*/NULL);
		return ((float)il2cpp_codegen_multiply((float)L_1, (float)(57.2957802f)));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VFX.ShaderVFX::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_Awake_m7AA549E6EF3CDE422212410D6C601DA96942D7AA (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method)
{
	{
		// mAnimationTime = 0;
		__this->set_mAnimationTime_19((0.0f));
		// mIsPlaying = false;
		__this->set_mIsPlaying_20((bool)0);
		// if (PlayOnAwake)
		bool L_0 = __this->get_PlayOnAwake_6();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		// Play();
		ShaderVFX_Play_m72C1E7E60D6C926DE3F1BB3B521C5D545790FBC9(__this, /*hidden argument*/NULL);
	}

IL_0020:
	{
		// }
		return;
	}
}
// System.Void VFX.ShaderVFX::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_Update_m40A13C7DC127AB3AA63AF22A1980F951449E48D9 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B5_0 = NULL;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B4_0 = NULL;
	{
		// if (!mIsPlaying)
		bool L_0 = __this->get_mIsPlaying_20();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// mAnimationTime += Time.deltaTime;
		float L_1 = __this->get_mAnimationTime_19();
		float L_2;
		L_2 = Time_get_deltaTime_mF558623BBB4EE65C8810243B05ED204A9E8D6FD1(/*hidden argument*/NULL);
		__this->set_mAnimationTime_19(((float)il2cpp_codegen_add((float)L_1, (float)L_2)));
		// var maxTime = 0f;
		V_0 = (0.0f);
		// UpdateDynamicScalarProperties(ref maxTime);
		ShaderVFX_UpdateDynamicScalarProperties_m37DF39590DE58AD7E4DE8798A8D3B5CC5EC04EC0(__this, (float*)(&V_0), /*hidden argument*/NULL);
		// UpdateDynamicVectorProperties(ref maxTime);
		ShaderVFX_UpdateDynamicVectorProperties_mFC501F25780E0ACC10326775FA29DF680DF7B667(__this, (float*)(&V_0), /*hidden argument*/NULL);
		// UpdateDynamicColors(ref maxTime);
		ShaderVFX_UpdateDynamicColors_m141A35CB6F0A1E6C2C7706232FDF412DF2696012(__this, (float*)(&V_0), /*hidden argument*/NULL);
		// if (mAnimationTime > maxTime)
		float L_3 = __this->get_mAnimationTime_19();
		float L_4 = V_0;
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0068;
		}
	}
	{
		// OnReachedEnd?.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_5 = __this->get_OnReachedEnd_17();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_6 = L_5;
		G_B4_0 = L_6;
		if (L_6)
		{
			G_B5_0 = L_6;
			goto IL_004e;
		}
	}
	{
		goto IL_0053;
	}

IL_004e:
	{
		NullCheck(G_B5_0);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(G_B5_0, /*hidden argument*/NULL);
	}

IL_0053:
	{
		// if (Loop)
		bool L_7 = __this->get_Loop_7();
		if (!L_7)
		{
			goto IL_0062;
		}
	}
	{
		// Rewind();
		ShaderVFX_Rewind_mEDF042F61293E7EE2E5EB624CDBAA0A16023E31A(__this, /*hidden argument*/NULL);
		return;
	}

IL_0062:
	{
		// Pause();
		ShaderVFX_Pause_m1AD628112529913D6169E3A230CFA1604A3A3A56(__this, /*hidden argument*/NULL);
	}

IL_0068:
	{
		// }
		return;
	}
}
// System.Void VFX.ShaderVFX::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_Play_m72C1E7E60D6C926DE3F1BB3B521C5D545790FBC9 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method)
{
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B6_0 = NULL;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B5_0 = NULL;
	{
		// if (mIsPlaying)
		bool L_0 = __this->get_mIsPlaying_20();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// InitMaterial();
		ShaderVFX_InitMaterial_mCF4D5E111B66F2B7853AA266F450BDF3D7005EE5(__this, /*hidden argument*/NULL);
		// InitStaticProperties();
		ShaderVFX_InitStaticProperties_m3AB1B50B9F3F3FD9D5783815F5CB48D82103807C(__this, /*hidden argument*/NULL);
		// InitDynamicProperties();
		ShaderVFX_InitDynamicProperties_m6AEFC94FBC6B76E1FCCD7BFA07D2F5E6F63DEE02(__this, /*hidden argument*/NULL);
		// if (AnimationAutoRewind)
		bool L_1 = __this->get_AnimationAutoRewind_8();
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		// Rewind();
		ShaderVFX_Rewind_mEDF042F61293E7EE2E5EB624CDBAA0A16023E31A(__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		// mIsPlaying = true;
		__this->set_mIsPlaying_20((bool)1);
		// OnEffectStarted?.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_2 = __this->get_OnEffectStarted_16();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_3 = L_2;
		G_B5_0 = L_3;
		if (L_3)
		{
			G_B6_0 = L_3;
			goto IL_003b;
		}
	}
	{
		return;
	}

IL_003b:
	{
		NullCheck(G_B6_0);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(G_B6_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VFX.ShaderVFX::Pause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_Pause_m1AD628112529913D6169E3A230CFA1604A3A3A56 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method)
{
	{
		// mIsPlaying = false;
		__this->set_mIsPlaying_20((bool)0);
		// }
		return;
	}
}
// System.Void VFX.ShaderVFX::Rewind()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_Rewind_mEDF042F61293E7EE2E5EB624CDBAA0A16023E31A (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method)
{
	{
		// mAnimationTime = 0;
		__this->set_mAnimationTime_19((0.0f));
		// }
		return;
	}
}
// System.Void VFX.ShaderVFX::RewindAndPlay()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_RewindAndPlay_m0BA24237FD8ACA7A257AA6B59DA6A667F7059C6E (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method)
{
	{
		// Rewind();
		ShaderVFX_Rewind_mEDF042F61293E7EE2E5EB624CDBAA0A16023E31A(__this, /*hidden argument*/NULL);
		// Play();
		ShaderVFX_Play_m72C1E7E60D6C926DE3F1BB3B521C5D545790FBC9(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VFX.ShaderVFX::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_Stop_mBCA727EAABBEE4C680E441082ABE3E595C5D3CC4 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method)
{
	{
		// mIsPlaying = false;
		__this->set_mIsPlaying_20((bool)0);
		// mAnimationTime = 0;
		__this->set_mAnimationTime_19((0.0f));
		// }
		return;
	}
}
// System.Void VFX.ShaderVFX::UpdateDynamicColors(System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_UpdateDynamicColors_m141A35CB6F0A1E6C2C7706232FDF412DF2696012 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, float* ___maxTime0, const RuntimeMethod* method)
{
	DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E* V_0 = NULL;
	int32_t V_1 = 0;
	DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	float V_3 = 0.0f;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// foreach (var property in DynamicColors)
		DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E* L_0 = __this->get_DynamicColors_15();
		V_0 = L_0;
		V_1 = 0;
		goto IL_0052;
	}

IL_000b:
	{
		// foreach (var property in DynamicColors)
		DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = L_4;
		// var currentTime = SetMaxTimeAndGetLerpValue(ref maxTime, property.delay, property.duration);
		float* L_5 = ___maxTime0;
		DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  L_6 = V_2;
		float L_7 = L_6.get_delay_3();
		DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  L_8 = V_2;
		float L_9 = L_8.get_duration_4();
		float L_10;
		L_10 = ShaderVFX_SetMaxTimeAndGetLerpValue_m6E10A14B06144F9F242E15ECDF3BB90ACC0A6483(__this, (float*)L_5, L_7, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		// var lerpPropColor = Color.Lerp(property.startColor, property.endColor, currentTime);
		DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  L_11 = V_2;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_12 = L_11.get_startColor_1();
		DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  L_13 = V_2;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_14 = L_13.get_endColor_2();
		float L_15 = V_3;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_16;
		L_16 = Color_Lerp_m6320057807E1F335970F168403C601EBD2B92062(L_12, L_14, L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		// MaterialUtil.TrySetColorProperty(mSharedMaterial, property.name, lerpPropColor);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_17 = __this->get_mSharedMaterial_18();
		DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  L_18 = V_2;
		String_t* L_19 = L_18.get_name_0();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_20 = V_4;
		MaterialUtil_TrySetColorProperty_mB063CAB2A94E153D8C0272E2E3F2950E4EB420C7(L_17, L_19, L_20, /*hidden argument*/NULL);
		int32_t L_21 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_0052:
	{
		// foreach (var property in DynamicColors)
		int32_t L_22 = V_1;
		DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E* L_23 = V_0;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void VFX.ShaderVFX::UpdateDynamicVectorProperties(System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_UpdateDynamicVectorProperties_mFC501F25780E0ACC10326775FA29DF680DF7B667 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, float* ___maxTime0, const RuntimeMethod* method)
{
	DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F* V_0 = NULL;
	int32_t V_1 = 0;
	DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  V_2;
	memset((&V_2), 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// foreach (var property in DynamicVectorProperties)
		DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F* L_0 = __this->get_DynamicVectorProperties_14();
		V_0 = L_0;
		V_1 = 0;
		goto IL_0057;
	}

IL_000b:
	{
		// foreach (var property in DynamicVectorProperties)
		DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = L_4;
		// var currentTime = SetMaxTimeAndGetLerpValue(ref maxTime, property.delay, property.duration);
		float* L_5 = ___maxTime0;
		DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  L_6 = V_2;
		float L_7 = L_6.get_delay_3();
		DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  L_8 = V_2;
		float L_9 = L_8.get_duration_4();
		float L_10;
		L_10 = ShaderVFX_SetMaxTimeAndGetLerpValue_m6E10A14B06144F9F242E15ECDF3BB90ACC0A6483(__this, (float*)L_5, L_7, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		// var lerpValue = Vector3.Lerp(property.startValue, property.endValue, currentTime);
		DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  L_11 = V_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = L_11.get_startValue_1();
		DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  L_13 = V_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = L_13.get_endValue_2();
		float L_15 = V_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Vector3_Lerp_m5E223DB365EAC8F6625C169E927527FFB8CC88DB_inline(L_12, L_14, L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		// MaterialUtil.TrySetVector4Property(mSharedMaterial, property.name, lerpValue);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_17 = __this->get_mSharedMaterial_18();
		DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  L_18 = V_2;
		String_t* L_19 = L_18.get_name_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = V_4;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_21;
		L_21 = Vector4_op_Implicit_mA9B2E82825C2543A2B3F6207EDAC76614A77EA1E(L_20, /*hidden argument*/NULL);
		MaterialUtil_TrySetVector4Property_m2036427B77E884AED74B6654D2C353C2485F7F86(L_17, L_19, L_21, /*hidden argument*/NULL);
		int32_t L_22 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0057:
	{
		// foreach (var property in DynamicVectorProperties)
		int32_t L_23 = V_1;
		DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F* L_24 = V_0;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void VFX.ShaderVFX::UpdateDynamicScalarProperties(System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_UpdateDynamicScalarProperties_m37DF39590DE58AD7E4DE8798A8D3B5CC5EC04EC0 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, float* ___maxTime0, const RuntimeMethod* method)
{
	DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE* V_0 = NULL;
	int32_t V_1 = 0;
	DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  V_2;
	memset((&V_2), 0, sizeof(V_2));
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		// foreach (var property in DynamicScalarProperties)
		DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE* L_0 = __this->get_DynamicScalarProperties_13();
		V_0 = L_0;
		V_1 = 0;
		goto IL_0052;
	}

IL_000b:
	{
		// foreach (var property in DynamicScalarProperties)
		DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = L_4;
		// var currentTime = SetMaxTimeAndGetLerpValue(ref maxTime, property.delay, property.duration);
		float* L_5 = ___maxTime0;
		DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  L_6 = V_2;
		float L_7 = L_6.get_delay_3();
		DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  L_8 = V_2;
		float L_9 = L_8.get_duration_4();
		float L_10;
		L_10 = ShaderVFX_SetMaxTimeAndGetLerpValue_m6E10A14B06144F9F242E15ECDF3BB90ACC0A6483(__this, (float*)L_5, L_7, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		// var lerpValue = Mathf.Lerp(property.startValue, property.endValue, currentTime);
		DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  L_11 = V_2;
		float L_12 = L_11.get_startValue_1();
		DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  L_13 = V_2;
		float L_14 = L_13.get_endValue_2();
		float L_15 = V_3;
		float L_16;
		L_16 = Mathf_Lerp_m04D5C368C4E4F1AB78230C6809A6651951A52C86(L_12, L_14, L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		// MaterialUtil.TrySetFloatProperty(mSharedMaterial, property.name, lerpValue);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_17 = __this->get_mSharedMaterial_18();
		DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  L_18 = V_2;
		String_t* L_19 = L_18.get_name_0();
		float L_20 = V_4;
		MaterialUtil_TrySetFloatProperty_m82611514F3F1235245B165AF05F3930AD40EB0DE(L_17, L_19, L_20, /*hidden argument*/NULL);
		int32_t L_21 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_0052:
	{
		// foreach (var property in DynamicScalarProperties)
		int32_t L_22 = V_1;
		DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE* L_23 = V_0;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		// }
		return;
	}
}
// System.Single VFX.ShaderVFX::SetMaxTimeAndGetLerpValue(System.Single&,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ShaderVFX_SetMaxTimeAndGetLerpValue_m6E10A14B06144F9F242E15ECDF3BB90ACC0A6483 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, float* ___maxTime0, float ___delay1, float ___duration2, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float G_B5_0 = 0.0f;
	{
		// if (delay + duration > maxTime)
		float L_0 = ___delay1;
		float L_1 = ___duration2;
		float* L_2 = ___maxTime0;
		float L_3 = *((float*)L_2);
		if ((!(((float)((float)il2cpp_codegen_add((float)L_0, (float)L_1))) > ((float)L_3))))
		{
			goto IL_000c;
		}
	}
	{
		// maxTime = delay + duration;
		float* L_4 = ___maxTime0;
		float L_5 = ___delay1;
		float L_6 = ___duration2;
		*((float*)L_4) = (float)((float)il2cpp_codegen_add((float)L_5, (float)L_6));
	}

IL_000c:
	{
		// var propAnimTime = Mathf.Max(0, mAnimationTime - delay);
		float L_7 = __this->get_mAnimationTime_19();
		float L_8 = ___delay1;
		float L_9;
		L_9 = Mathf_Max_m5C96B726079E95BB1A1DC60532553CB723D24C79((0.0f), ((float)il2cpp_codegen_subtract((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		// var currentTime = duration == 0 ? 1 : Mathf.Clamp01(propAnimTime / duration);
		float L_10 = ___duration2;
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0031;
		}
	}
	{
		float L_11 = V_0;
		float L_12 = ___duration2;
		float L_13;
		L_13 = Mathf_Clamp01_m831CBA1D198C3CDE660E8172A67A4E41BD0D0171(((float)((float)L_11/(float)L_12)), /*hidden argument*/NULL);
		G_B5_0 = L_13;
		goto IL_0036;
	}

IL_0031:
	{
		G_B5_0 = (1.0f);
	}

IL_0036:
	{
		V_1 = G_B5_0;
		// if (TransitionMode == TransitionMode.CUBIC)
		int32_t L_14 = __this->get_TransitionMode_9();
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_0051;
		}
	}
	{
		// currentTime = Mathf.SmoothStep(0, 1, currentTime);
		float L_15 = V_1;
		float L_16;
		L_16 = Mathf_SmoothStep_m90F874839A4085EBA0AF938ADCD2C05F77F7A963((0.0f), (1.0f), L_15, /*hidden argument*/NULL);
		V_1 = L_16;
	}

IL_0051:
	{
		// return currentTime;
		float L_17 = V_1;
		return L_17;
	}
}
// System.Void VFX.ShaderVFX::InitMaterial()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_InitMaterial_mCF4D5E111B66F2B7853AA266F450BDF3D7005EE5 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mDF527A06E3315D1A7F828664F20B35662B0A52A6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Material_t8927C00353A72755313F046D0CE85178AE8218EE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0B3C116817E94141082E7E559AE582C6E6A169FF);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// if (!TargetObject)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_TargetObject_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		// Debug.LogError("Failed to init renderers material, target object not set!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m5952BCE5DC0EF798B31FA983B9CE42A5A1F82DE1(_stringLiteral0B3C116817E94141082E7E559AE582C6E6A169FF, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0018:
	{
		// if (!mSharedMaterial && EffectMaterial)
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = __this->get_mSharedMaterial_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0043;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_4 = __this->get_EffectMaterial_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		// mSharedMaterial = new Material(EffectMaterial);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_6 = __this->get_EffectMaterial_5();
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_7 = (Material_t8927C00353A72755313F046D0CE85178AE8218EE *)il2cpp_codegen_object_new(Material_t8927C00353A72755313F046D0CE85178AE8218EE_il2cpp_TypeInfo_var);
		Material__ctor_mD0C3D9CFAFE0FB858D864092467387D7FA178245(L_7, L_6, /*hidden argument*/NULL);
		__this->set_mSharedMaterial_18(L_7);
	}

IL_0043:
	{
		// if (mSharedMaterial)
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_8 = __this->get_mSharedMaterial_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0079;
		}
	}
	{
		// var renderers = TargetObject.GetComponentsInChildren<Renderer>(includeInactive: true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10 = __this->get_TargetObject_4();
		NullCheck(L_10);
		RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* L_11;
		L_11 = GameObject_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mDF527A06E3315D1A7F828664F20B35662B0A52A6(L_10, (bool)1, /*hidden argument*/GameObject_GetComponentsInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mDF527A06E3315D1A7F828664F20B35662B0A52A6_RuntimeMethod_var);
		// foreach (var aRenderer in renderers)
		V_0 = L_11;
		V_1 = 0;
		goto IL_0073;
	}

IL_0061:
	{
		// foreach (var aRenderer in renderers)
		RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* L_12 = V_0;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		// aRenderer.sharedMaterial = mSharedMaterial;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_16 = __this->get_mSharedMaterial_18();
		NullCheck(L_15);
		Renderer_set_sharedMaterial_m1E66766F93E95F692C3C9C2C09AFD795B156678B(L_15, L_16, /*hidden argument*/NULL);
		int32_t L_17 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1));
	}

IL_0073:
	{
		// foreach (var aRenderer in renderers)
		int32_t L_18 = V_1;
		RendererU5BU5D_tE2D3C4350893C593CA40DE876B9F2F0EBBEC49B7* L_19 = V_0;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_19)->max_length))))))
		{
			goto IL_0061;
		}
	}

IL_0079:
	{
		// }
		return;
	}
}
// System.Void VFX.ShaderVFX::InitStaticProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_InitStaticProperties_m3AB1B50B9F3F3FD9D5783815F5CB48D82103807C (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method)
{
	StaticScalarPropertyU5BU5D_t81263A8DC17430EDE9ED5D9BDC3D590C8D5555C9* V_0 = NULL;
	int32_t V_1 = 0;
	StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	StaticVectorPropertyU5BU5D_tFED79A0033EA7A3C25F29A20C8DDF49E6B945A36* V_3 = NULL;
	StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE  V_4;
	memset((&V_4), 0, sizeof(V_4));
	StaticColorU5BU5D_tB852F8A761474DEA5054061DD1F8B267C56E6FF8* V_5 = NULL;
	StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F  V_6;
	memset((&V_6), 0, sizeof(V_6));
	{
		// foreach (var property in StaticScalarProperties)
		StaticScalarPropertyU5BU5D_t81263A8DC17430EDE9ED5D9BDC3D590C8D5555C9* L_0 = __this->get_StaticScalarProperties_10();
		V_0 = L_0;
		V_1 = 0;
		goto IL_002e;
	}

IL_000b:
	{
		// foreach (var property in StaticScalarProperties)
		StaticScalarPropertyU5BU5D_t81263A8DC17430EDE9ED5D9BDC3D590C8D5555C9* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = L_4;
		// MaterialUtil.TrySetFloatProperty(mSharedMaterial, property.name, property.value);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_5 = __this->get_mSharedMaterial_18();
		StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E  L_6 = V_2;
		String_t* L_7 = L_6.get_name_0();
		StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E  L_8 = V_2;
		float L_9 = L_8.get_value_1();
		MaterialUtil_TrySetFloatProperty_m82611514F3F1235245B165AF05F3930AD40EB0DE(L_5, L_7, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_002e:
	{
		// foreach (var property in StaticScalarProperties)
		int32_t L_11 = V_1;
		StaticScalarPropertyU5BU5D_t81263A8DC17430EDE9ED5D9BDC3D590C8D5555C9* L_12 = V_0;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		// foreach (var property in StaticVectorProperties)
		StaticVectorPropertyU5BU5D_tFED79A0033EA7A3C25F29A20C8DDF49E6B945A36* L_13 = __this->get_StaticVectorProperties_11();
		V_3 = L_13;
		V_1 = 0;
		goto IL_006a;
	}

IL_003f:
	{
		// foreach (var property in StaticVectorProperties)
		StaticVectorPropertyU5BU5D_tFED79A0033EA7A3C25F29A20C8DDF49E6B945A36* L_14 = V_3;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_4 = L_17;
		// MaterialUtil.TrySetVector4Property(mSharedMaterial, property.name, property.value);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_18 = __this->get_mSharedMaterial_18();
		StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE  L_19 = V_4;
		String_t* L_20 = L_19.get_name_0();
		StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE  L_21 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22 = L_21.get_value_1();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_23;
		L_23 = Vector4_op_Implicit_mA9B2E82825C2543A2B3F6207EDAC76614A77EA1E(L_22, /*hidden argument*/NULL);
		MaterialUtil_TrySetVector4Property_m2036427B77E884AED74B6654D2C353C2485F7F86(L_18, L_20, L_23, /*hidden argument*/NULL);
		int32_t L_24 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
	}

IL_006a:
	{
		// foreach (var property in StaticVectorProperties)
		int32_t L_25 = V_1;
		StaticVectorPropertyU5BU5D_tFED79A0033EA7A3C25F29A20C8DDF49E6B945A36* L_26 = V_3;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_26)->max_length))))))
		{
			goto IL_003f;
		}
	}
	{
		// foreach (var color in StaticColors)
		StaticColorU5BU5D_tB852F8A761474DEA5054061DD1F8B267C56E6FF8* L_27 = __this->get_StaticColors_12();
		V_5 = L_27;
		V_1 = 0;
		goto IL_00a3;
	}

IL_007c:
	{
		// foreach (var color in StaticColors)
		StaticColorU5BU5D_tB852F8A761474DEA5054061DD1F8B267C56E6FF8* L_28 = V_5;
		int32_t L_29 = V_1;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F  L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		V_6 = L_31;
		// MaterialUtil.TrySetColorProperty(mSharedMaterial, color.name, color.color);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_32 = __this->get_mSharedMaterial_18();
		StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F  L_33 = V_6;
		String_t* L_34 = L_33.get_name_0();
		StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F  L_35 = V_6;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_36 = L_35.get_color_1();
		MaterialUtil_TrySetColorProperty_mB063CAB2A94E153D8C0272E2E3F2950E4EB420C7(L_32, L_34, L_36, /*hidden argument*/NULL);
		int32_t L_37 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_37, (int32_t)1));
	}

IL_00a3:
	{
		// foreach (var color in StaticColors)
		int32_t L_38 = V_1;
		StaticColorU5BU5D_tB852F8A761474DEA5054061DD1F8B267C56E6FF8* L_39 = V_5;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_39)->max_length))))))
		{
			goto IL_007c;
		}
	}
	{
		// }
		return;
	}
}
// System.Void VFX.ShaderVFX::InitDynamicProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX_InitDynamicProperties_m6AEFC94FBC6B76E1FCCD7BFA07D2F5E6F63DEE02 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method)
{
	DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE* V_0 = NULL;
	int32_t V_1 = 0;
	DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  V_2;
	memset((&V_2), 0, sizeof(V_2));
	DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F* V_3 = NULL;
	DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  V_4;
	memset((&V_4), 0, sizeof(V_4));
	DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E* V_5 = NULL;
	DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  V_6;
	memset((&V_6), 0, sizeof(V_6));
	{
		// foreach (var property in DynamicScalarProperties)
		DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE* L_0 = __this->get_DynamicScalarProperties_13();
		V_0 = L_0;
		V_1 = 0;
		goto IL_002e;
	}

IL_000b:
	{
		// foreach (var property in DynamicScalarProperties)
		DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = L_4;
		// MaterialUtil.TrySetFloatProperty(mSharedMaterial, property.name, property.startValue);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_5 = __this->get_mSharedMaterial_18();
		DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  L_6 = V_2;
		String_t* L_7 = L_6.get_name_0();
		DynamicScalarProperty_t317802401271090F70A5E05E1CFE8923FDA51D41  L_8 = V_2;
		float L_9 = L_8.get_startValue_1();
		MaterialUtil_TrySetFloatProperty_m82611514F3F1235245B165AF05F3930AD40EB0DE(L_5, L_7, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_002e:
	{
		// foreach (var property in DynamicScalarProperties)
		int32_t L_11 = V_1;
		DynamicScalarPropertyU5BU5D_t94FDE96EBF70429836C03E1669642C5CFCFE93CE* L_12 = V_0;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		// foreach (var property in DynamicVectorProperties)
		DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F* L_13 = __this->get_DynamicVectorProperties_14();
		V_3 = L_13;
		V_1 = 0;
		goto IL_006a;
	}

IL_003f:
	{
		// foreach (var property in DynamicVectorProperties)
		DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F* L_14 = V_3;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_4 = L_17;
		// MaterialUtil.TrySetVector4Property(mSharedMaterial, property.name, property.startValue);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_18 = __this->get_mSharedMaterial_18();
		DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  L_19 = V_4;
		String_t* L_20 = L_19.get_name_0();
		DynamicVectorProperty_t5F9280D5E2F238B228C6004735D6890F8E8A2ECE  L_21 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22 = L_21.get_startValue_1();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_23;
		L_23 = Vector4_op_Implicit_mA9B2E82825C2543A2B3F6207EDAC76614A77EA1E(L_22, /*hidden argument*/NULL);
		MaterialUtil_TrySetVector4Property_m2036427B77E884AED74B6654D2C353C2485F7F86(L_18, L_20, L_23, /*hidden argument*/NULL);
		int32_t L_24 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
	}

IL_006a:
	{
		// foreach (var property in DynamicVectorProperties)
		int32_t L_25 = V_1;
		DynamicVectorPropertyU5BU5D_tF307AA5EFB20EE50A1CCAD79443EEEAEDE8C059F* L_26 = V_3;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_26)->max_length))))))
		{
			goto IL_003f;
		}
	}
	{
		// foreach (var color in DynamicColors)
		DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E* L_27 = __this->get_DynamicColors_15();
		V_5 = L_27;
		V_1 = 0;
		goto IL_00a3;
	}

IL_007c:
	{
		// foreach (var color in DynamicColors)
		DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E* L_28 = V_5;
		int32_t L_29 = V_1;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		V_6 = L_31;
		// MaterialUtil.TrySetColorProperty(mSharedMaterial, color.name, color.startColor);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_32 = __this->get_mSharedMaterial_18();
		DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  L_33 = V_6;
		String_t* L_34 = L_33.get_name_0();
		DynamicColor_tBF9D1AF707D798940FD3D969A8D08150F652095E  L_35 = V_6;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_36 = L_35.get_startColor_1();
		MaterialUtil_TrySetColorProperty_mB063CAB2A94E153D8C0272E2E3F2950E4EB420C7(L_32, L_34, L_36, /*hidden argument*/NULL);
		int32_t L_37 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_37, (int32_t)1));
	}

IL_00a3:
	{
		// foreach (var color in DynamicColors)
		int32_t L_38 = V_1;
		DynamicColorU5BU5D_tD1211CD346D6975944A0BFD9E836B4CCEE7D7F2E* L_39 = V_5;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_39)->max_length))))))
		{
			goto IL_007c;
		}
	}
	{
		// }
		return;
	}
}
// System.Void VFX.ShaderVFX::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShaderVFX__ctor_m5BEEA87E5640310F903984FB48972724AA17A475 (ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 * __this, const RuntimeMethod* method)
{
	{
		// public bool PlayOnAwake = true;
		__this->set_PlayOnAwake_6((bool)1);
		// public bool AnimationAutoRewind = true;
		__this->set_AnimationAutoRewind_8((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: VFX.StaticColor
IL2CPP_EXTERN_C void StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshal_pinvoke(const StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F& unmarshaled, StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	marshaled.___color_1 = unmarshaled.get_color_1();
}
IL2CPP_EXTERN_C void StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshal_pinvoke_back(const StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshaled_pinvoke& marshaled, StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  unmarshaled_color_temp_1;
	memset((&unmarshaled_color_temp_1), 0, sizeof(unmarshaled_color_temp_1));
	unmarshaled_color_temp_1 = marshaled.___color_1;
	unmarshaled.set_color_1(unmarshaled_color_temp_1);
}
// Conversion method for clean up from marshalling of: VFX.StaticColor
IL2CPP_EXTERN_C void StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshal_pinvoke_cleanup(StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// Conversion methods for marshalling of: VFX.StaticColor
IL2CPP_EXTERN_C void StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshal_com(const StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F& unmarshaled, StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	marshaled.___color_1 = unmarshaled.get_color_1();
}
IL2CPP_EXTERN_C void StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshal_com_back(const StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshaled_com& marshaled, StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  unmarshaled_color_temp_1;
	memset((&unmarshaled_color_temp_1), 0, sizeof(unmarshaled_color_temp_1));
	unmarshaled_color_temp_1 = marshaled.___color_1;
	unmarshaled.set_color_1(unmarshaled_color_temp_1);
}
// Conversion method for clean up from marshalling of: VFX.StaticColor
IL2CPP_EXTERN_C void StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshal_com_cleanup(StaticColor_t076BD28252349EE3D73FEF452CDCC6C541F4847F_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: VFX.StaticScalarProperty
IL2CPP_EXTERN_C void StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshal_pinvoke(const StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E& unmarshaled, StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	marshaled.___value_1 = unmarshaled.get_value_1();
}
IL2CPP_EXTERN_C void StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshal_pinvoke_back(const StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshaled_pinvoke& marshaled, StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	float unmarshaled_value_temp_1 = 0.0f;
	unmarshaled_value_temp_1 = marshaled.___value_1;
	unmarshaled.set_value_1(unmarshaled_value_temp_1);
}
// Conversion method for clean up from marshalling of: VFX.StaticScalarProperty
IL2CPP_EXTERN_C void StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshal_pinvoke_cleanup(StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// Conversion methods for marshalling of: VFX.StaticScalarProperty
IL2CPP_EXTERN_C void StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshal_com(const StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E& unmarshaled, StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	marshaled.___value_1 = unmarshaled.get_value_1();
}
IL2CPP_EXTERN_C void StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshal_com_back(const StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshaled_com& marshaled, StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	float unmarshaled_value_temp_1 = 0.0f;
	unmarshaled_value_temp_1 = marshaled.___value_1;
	unmarshaled.set_value_1(unmarshaled_value_temp_1);
}
// Conversion method for clean up from marshalling of: VFX.StaticScalarProperty
IL2CPP_EXTERN_C void StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshal_com_cleanup(StaticScalarProperty_tA1599E79C4F76DDC3ECDB572A00F88B6368F902E_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: VFX.StaticVectorProperty
IL2CPP_EXTERN_C void StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshal_pinvoke(const StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE& unmarshaled, StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	marshaled.___value_1 = unmarshaled.get_value_1();
}
IL2CPP_EXTERN_C void StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshal_pinvoke_back(const StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshaled_pinvoke& marshaled, StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  unmarshaled_value_temp_1;
	memset((&unmarshaled_value_temp_1), 0, sizeof(unmarshaled_value_temp_1));
	unmarshaled_value_temp_1 = marshaled.___value_1;
	unmarshaled.set_value_1(unmarshaled_value_temp_1);
}
// Conversion method for clean up from marshalling of: VFX.StaticVectorProperty
IL2CPP_EXTERN_C void StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshal_pinvoke_cleanup(StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// Conversion methods for marshalling of: VFX.StaticVectorProperty
IL2CPP_EXTERN_C void StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshal_com(const StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE& unmarshaled, StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	marshaled.___value_1 = unmarshaled.get_value_1();
}
IL2CPP_EXTERN_C void StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshal_com_back(const StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshaled_com& marshaled, StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  unmarshaled_value_temp_1;
	memset((&unmarshaled_value_temp_1), 0, sizeof(unmarshaled_value_temp_1));
	unmarshaled_value_temp_1 = marshaled.___value_1;
	unmarshaled.set_value_1(unmarshaled_value_temp_1);
}
// Conversion method for clean up from marshalling of: VFX.StaticVectorProperty
IL2CPP_EXTERN_C void StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshal_com_cleanup(StaticVectorProperty_tCFAD1426D4CDE4033E2BC360FA916F6166ABA9DE_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VFX.TargetVFX::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVFX_Awake_m3C692394F93453E2D821EB12A62F0F2DF79CAB50 (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2__ctor_m6702F5465C5156B1FB05A678988FF9A80D94AE06_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInParent_TisObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274_mCE544666A6285AE628139A24DDEF8131B0CAE6AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TargetVFX_TargetStatusChanged_m4BCF8695AF732A32C4E4DC69A551DBAE1E6BB465_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral441BAC8B283322B34D840002AA78BE51874447BB);
		s_Il2CppMethodInitialized = true;
	}
	ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * V_0 = NULL;
	{
		// base.Awake();
		ShaderVFX_Awake_m7AA549E6EF3CDE422212410D6C601DA96942D7AA(__this, /*hidden argument*/NULL);
		// if (Observer)
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_0 = __this->get_Observer_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		// mObserver = Observer;
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_2 = __this->get_Observer_21();
		__this->set_mObserver_31(L_2);
		goto IL_0037;
	}

IL_0021:
	{
		// var parentObserver = GetComponentInParent<ObserverBehaviour>();
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_3;
		L_3 = Component_GetComponentInParent_TisObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274_mCE544666A6285AE628139A24DDEF8131B0CAE6AA(__this, /*hidden argument*/Component_GetComponentInParent_TisObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274_mCE544666A6285AE628139A24DDEF8131B0CAE6AA_RuntimeMethod_var);
		V_0 = L_3;
		// if (parentObserver)
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		// mObserver = parentObserver;
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_6 = V_0;
		__this->set_mObserver_31(L_6);
	}

IL_0037:
	{
		// if (!mObserver)
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_7 = __this->get_mObserver_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004e;
		}
	}
	{
		// Debug.LogWarning("Vuforia Observer not found!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogWarning_mA477FDA9C0B96C627C085E9EB431EB394B2EBBE0(_stringLiteral441BAC8B283322B34D840002AA78BE51874447BB, /*hidden argument*/NULL);
	}

IL_004e:
	{
		// mObserver.OnTargetStatusChanged += TargetStatusChanged;
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_9 = __this->get_mObserver_31();
		Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4 * L_10 = (Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4 *)il2cpp_codegen_object_new(Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4_il2cpp_TypeInfo_var);
		Action_2__ctor_m6702F5465C5156B1FB05A678988FF9A80D94AE06(L_10, __this, (intptr_t)((intptr_t)TargetVFX_TargetStatusChanged_m4BCF8695AF732A32C4E4DC69A551DBAE1E6BB465_RuntimeMethod_var), /*hidden argument*/Action_2__ctor_m6702F5465C5156B1FB05A678988FF9A80D94AE06_RuntimeMethod_var);
		NullCheck(L_9);
		ObserverBehaviour_add_OnTargetStatusChanged_m35984866CE532D434875F398D32362E05D90197F(L_9, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VFX.TargetVFX::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVFX_Update_m66C4A427F20682E60BEDFF684163FB56C648A3B0 (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Update();
		ShaderVFX_Update_m40A13C7DC127AB3AA63AF22A1980F951449E48D9(__this, /*hidden argument*/NULL);
		// if (!mObserver || !VuforiaApplication.Instance.IsRunning)
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_0 = __this->get_mObserver_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		VuforiaApplication_tDC756FC5605334FDF0FF6F95CC0FEE4E134D64F1 * L_2;
		L_2 = VuforiaApplication_get_Instance_mAE3192D808C9F2B8F7EACDC490DD5F2E72DF8A75(/*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3;
		L_3 = VuforiaApplication_get_IsRunning_mE574E58FDF23D44974AD60CD2B3707A9EA86BC02(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0020;
		}
	}

IL_001f:
	{
		// return;
		return;
	}

IL_0020:
	{
		// UpdateShaderCenterAndAxis();
		TargetVFX_UpdateShaderCenterAndAxis_m6D4F079A02F16ABA5F6640F21A3A909BACE643B5(__this, /*hidden argument*/NULL);
		// UpdateShaderScale();
		TargetVFX_UpdateShaderScale_m14C9D840F99D8B1DE1FB87A01A012070B740B82C(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VFX.TargetVFX::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVFX_OnDestroy_mFFE073F460381C34016D865A8B3DC9394E441F36 (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2__ctor_m6702F5465C5156B1FB05A678988FF9A80D94AE06_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TargetVFX_TargetStatusChanged_m4BCF8695AF732A32C4E4DC69A551DBAE1E6BB465_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (mObserver != null)
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_0 = __this->get_mObserver_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		// mObserver.OnTargetStatusChanged -= TargetStatusChanged;
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_2 = __this->get_mObserver_31();
		Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4 * L_3 = (Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4 *)il2cpp_codegen_object_new(Action_2_tBEC7B0597650F8D41DD8126DFAC07D2EA63976A4_il2cpp_TypeInfo_var);
		Action_2__ctor_m6702F5465C5156B1FB05A678988FF9A80D94AE06(L_3, __this, (intptr_t)((intptr_t)TargetVFX_TargetStatusChanged_m4BCF8695AF732A32C4E4DC69A551DBAE1E6BB465_RuntimeMethod_var), /*hidden argument*/Action_2__ctor_m6702F5465C5156B1FB05A678988FF9A80D94AE06_RuntimeMethod_var);
		NullCheck(L_2);
		ObserverBehaviour_remove_OnTargetStatusChanged_m0D9E06F1D07D727D888A974EFCF948AAC02F5BCC(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0025:
	{
		// }
		return;
	}
}
// System.Void VFX.TargetVFX::UpdateShaderScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVFX_UpdateShaderScale_m14C9D840F99D8B1DE1FB87A01A012070B740B82C (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, const RuntimeMethod* method)
{
	{
		// MaterialUtil.TrySetVector4Property(mSharedMaterial, ShaderScale, VuforiaObserverUtil.GetTargetSize(mObserver));
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_0 = ((ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 *)__this)->get_mSharedMaterial_18();
		String_t* L_1 = __this->get_ShaderScale_28();
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_2 = __this->get_mObserver_31();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = VuforiaObserverUtil_GetTargetSize_mBF9FA09D93599B7000922B6C7C6D852E07C74949(L_2, /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_4;
		L_4 = Vector4_op_Implicit_mA9B2E82825C2543A2B3F6207EDAC76614A77EA1E(L_3, /*hidden argument*/NULL);
		MaterialUtil_TrySetVector4Property_m2036427B77E884AED74B6654D2C353C2485F7F86(L_0, L_1, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VFX.TargetVFX::TargetStatusChanged(Vuforia.ObserverBehaviour,Vuforia.TargetStatus)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVFX_TargetStatusChanged_m4BCF8695AF732A32C4E4DC69A551DBAE1E6BB465 (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * ___behaviour0, TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  ___status1, const RuntimeMethod* method)
{
	{
		// if (IsUnTracked(mPreviousTargetStatus) && IsTracked(status))
		TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  L_0 = __this->get_mPreviousTargetStatus_32();
		bool L_1;
		L_1 = TargetVFX_IsUnTracked_m706016647B2DF9E29105F351BB08DC15DF89F764(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  L_2 = ___status1;
		bool L_3;
		L_3 = TargetVFX_IsTracked_m885C810BE91774EC58936F694042A78B41F28478(__this, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		// OnTargetFound();
		TargetVFX_OnTargetFound_mD2BB51BCFAF8C3066A27ACEF54DD0FF216BB1D2C(__this, /*hidden argument*/NULL);
	}

IL_001d:
	{
		// mPreviousTargetStatus = status;
		TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  L_4 = ___status1;
		__this->set_mPreviousTargetStatus_32(L_4);
		// }
		return;
	}
}
// System.Boolean VFX.TargetVFX::IsTracked(Vuforia.TargetStatus)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TargetVFX_IsTracked_m885C810BE91774EC58936F694042A78B41F28478 (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  ___targetStatus0, const RuntimeMethod* method)
{
	{
		// return targetStatus.Status == Status.TRACKED || targetStatus.Status == Status.EXTENDED_TRACKED ||
		//        targetStatus.Status == Status.LIMITED;
		int32_t L_0;
		L_0 = TargetStatus_get_Status_mD745DEB3CFE4D18A1E973178AA8CA9BB73178D5E_inline((TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1 *)(&___targetStatus0), /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_1;
		L_1 = TargetStatus_get_Status_mD745DEB3CFE4D18A1E973178AA8CA9BB73178D5E_inline((TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1 *)(&___targetStatus0), /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_2;
		L_2 = TargetStatus_get_Status_mD745DEB3CFE4D18A1E973178AA8CA9BB73178D5E_inline((TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1 *)(&___targetStatus0), /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)1))? 1 : 0);
	}

IL_001f:
	{
		return (bool)1;
	}
}
// System.Boolean VFX.TargetVFX::IsUnTracked(Vuforia.TargetStatus)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TargetVFX_IsUnTracked_m706016647B2DF9E29105F351BB08DC15DF89F764 (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  ___targetStatus0, const RuntimeMethod* method)
{
	{
		// return targetStatus.Status == Status.NO_POSE;
		int32_t L_0;
		L_0 = TargetStatus_get_Status_mD745DEB3CFE4D18A1E973178AA8CA9BB73178D5E_inline((TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1 *)(&___targetStatus0), /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void VFX.TargetVFX::OnTargetFound()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVFX_OnTargetFound_mD2BB51BCFAF8C3066A27ACEF54DD0FF216BB1D2C (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral54F48823060A5886D12558C4BBB0CDBB0E83F58A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (IsTargetShownOnTargetFound)
		bool L_0 = __this->get_IsTargetShownOnTargetFound_22();
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		// if (ShowDelay > Mathf.Epsilon)
		float L_1 = __this->get_ShowDelay_23();
		float L_2 = ((Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_StaticFields*)il2cpp_codegen_static_fields_for(Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var))->get_Epsilon_0();
		if ((!(((float)L_1) > ((float)L_2))))
		{
			goto IL_0027;
		}
	}
	{
		// Invoke(nameof(Play), ShowDelay);
		float L_3 = __this->get_ShowDelay_23();
		MonoBehaviour_Invoke_m4AAB759653B1C6FB0653527F4DDC72D1E9162CC4(__this, _stringLiteral54F48823060A5886D12558C4BBB0CDBB0E83F58A, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0027:
	{
		// Play();
		ShaderVFX_Play_m72C1E7E60D6C926DE3F1BB3B521C5D545790FBC9(__this, /*hidden argument*/NULL);
	}

IL_002d:
	{
		// }
		return;
	}
}
// System.Void VFX.TargetVFX::UpdateShaderCenterAndAxis()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVFX_UpdateShaderCenterAndAxis_m6D4F079A02F16ABA5F6640F21A3A909BACE643B5 (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// if (!mObserver || !mSharedMaterial)
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_0 = __this->get_mObserver_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = ((ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 *)__this)->get_mSharedMaterial_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001b;
		}
	}

IL_001a:
	{
		// return;
		return;
	}

IL_001b:
	{
		// MaterialUtil.TrySetVector4Property(mSharedMaterial, ShaderCenter, GetCenterPointWCS());
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_4 = ((ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 *)__this)->get_mSharedMaterial_18();
		String_t* L_5 = __this->get_ShaderCenter_24();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = TargetVFX_GetCenterPointWCS_m6A62C57D93BF95E1F93585E65E072512C88E4950(__this, /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_7;
		L_7 = Vector4_op_Implicit_mA9B2E82825C2543A2B3F6207EDAC76614A77EA1E(L_6, /*hidden argument*/NULL);
		MaterialUtil_TrySetVector4Property_m2036427B77E884AED74B6654D2C353C2485F7F86(L_4, L_5, L_7, /*hidden argument*/NULL);
		// GetAxisVectors(out Vector3 aX, out Vector3 aY, out Vector3 aZ);
		TargetVFX_GetAxisVectors_mDAEF2F3A57F51777FA1CA49CB9315796D2633CED(__this, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_1), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_2), /*hidden argument*/NULL);
		// MaterialUtil.TrySetVector4Property(mSharedMaterial, ShaderAxisX, aX);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_8 = ((ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 *)__this)->get_mSharedMaterial_18();
		String_t* L_9 = __this->get_ShaderAxisX_25();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_11;
		L_11 = Vector4_op_Implicit_mA9B2E82825C2543A2B3F6207EDAC76614A77EA1E(L_10, /*hidden argument*/NULL);
		MaterialUtil_TrySetVector4Property_m2036427B77E884AED74B6654D2C353C2485F7F86(L_8, L_9, L_11, /*hidden argument*/NULL);
		// MaterialUtil.TrySetVector4Property(mSharedMaterial, ShaderAxisY, aY);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_12 = ((ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 *)__this)->get_mSharedMaterial_18();
		String_t* L_13 = __this->get_ShaderAxisY_26();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = V_1;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_15;
		L_15 = Vector4_op_Implicit_mA9B2E82825C2543A2B3F6207EDAC76614A77EA1E(L_14, /*hidden argument*/NULL);
		MaterialUtil_TrySetVector4Property_m2036427B77E884AED74B6654D2C353C2485F7F86(L_12, L_13, L_15, /*hidden argument*/NULL);
		// MaterialUtil.TrySetVector4Property(mSharedMaterial, ShaderAxisZ, aZ);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_16 = ((ShaderVFX_t0CA406226914047B533232E406E31767AF23E763 *)__this)->get_mSharedMaterial_18();
		String_t* L_17 = __this->get_ShaderAxisZ_27();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = V_2;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_19;
		L_19 = Vector4_op_Implicit_mA9B2E82825C2543A2B3F6207EDAC76614A77EA1E(L_18, /*hidden argument*/NULL);
		MaterialUtil_TrySetVector4Property_m2036427B77E884AED74B6654D2C353C2485F7F86(L_16, L_17, L_19, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void VFX.TargetVFX::GetAxisVectors(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVFX_GetAxisVectors_mDAEF2F3A57F51777FA1CA49CB9315796D2633CED (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___axisX0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___axisY1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___axisZ2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * V_0 = NULL;
	{
		// axisX = Vector3.right;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = ___axisX0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector3_get_right_m60959C1C1EF0F694D71E1569160D40B1DA768931(/*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_0 = L_1;
		// axisY = Vector3.up;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_2 = ___axisY1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Vector3_get_up_mD84FDFCD32FC48C865A89FD4251232E2A9D7015A(/*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_2 = L_3;
		// axisZ = Vector3.forward;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_4 = ___axisZ2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_get_forward_mA6722B0932DA770D5C34C9E28D0E40220F099D50(/*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_4 = L_5;
		// if (CenterMode == VFXCenterMode.TARGET && mObserver != null)
		int32_t L_6 = __this->get_CenterMode_30();
		if (L_6)
		{
			goto IL_007a;
		}
	}
	{
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_7 = __this->get_mObserver_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6(L_7, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_007a;
		}
	}
	{
		// axisX = mObserver.transform.right;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_9 = ___axisX0;
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_10 = __this->get_mObserver_31();
		NullCheck(L_10);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_11, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_9 = L_12;
		// axisY = mObserver.transform.up;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_13 = ___axisY1;
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_14 = __this->get_mObserver_31();
		NullCheck(L_14);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_15, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_13 = L_16;
		// axisZ = mObserver.transform.forward;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_17 = ___axisZ2;
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_18 = __this->get_mObserver_31();
		NullCheck(L_18);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19;
		L_19 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_19, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_17 = L_20;
		// }
		return;
	}

IL_007a:
	{
		// else if (CenterMode == VFXCenterMode.VIEWER && (cam = VuforiaCameraUtil.GetCamera()) != null)
		int32_t L_21 = __this->get_CenterMode_30();
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_00c5;
		}
	}
	{
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_22;
		L_22 = VuforiaCameraUtil_GetCamera_m19816CEAC1F906DE62B392F97D8053884E271200(/*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_23 = L_22;
		V_0 = L_23;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_24;
		L_24 = Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6(L_23, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00c5;
		}
	}
	{
		// axisX = cam.transform.right;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_25 = ___axisX0;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_26 = V_0;
		NullCheck(L_26);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_27;
		L_27 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_27, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_25 = L_28;
		// axisY = cam.transform.up;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_29 = ___axisY1;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_30 = V_0;
		NullCheck(L_30);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_31;
		L_31 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_31, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_29 = L_32;
		// axisZ = cam.transform.forward;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_33 = ___axisZ2;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_34 = V_0;
		NullCheck(L_34);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_35;
		L_35 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36;
		L_36 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_35, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_33 = L_36;
	}

IL_00c5:
	{
		// }
		return;
	}
}
// UnityEngine.Vector3 VFX.TargetVFX::GetCenterPointWCS()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  TargetVFX_GetCenterPointWCS_m6A62C57D93BF95E1F93585E65E072512C88E4950 (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * V_1 = NULL;
	{
		// var centerPointWCS = Center;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = __this->get_Center_29();
		V_0 = L_0;
		// if (CenterMode == VFXCenterMode.TARGET && mObserver != null)
		int32_t L_1 = __this->get_CenterMode_30();
		if (L_1)
		{
			goto IL_0036;
		}
	}
	{
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_2 = __this->get_mObserver_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		// centerPointWCS = mObserver.transform.TransformPoint(Center);
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_4 = __this->get_mObserver_31();
		NullCheck(L_4);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = __this->get_Center_29();
		NullCheck(L_5);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_TransformPoint_m68AF95765A9279192E601208A9C5170027A5F0D2(L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0060;
	}

IL_0036:
	{
		// else if (CenterMode == VFXCenterMode.VIEWER && (cam = VuforiaCameraUtil.GetCamera()) != null)
		int32_t L_8 = __this->get_CenterMode_30();
		if ((!(((uint32_t)L_8) == ((uint32_t)1))))
		{
			goto IL_0060;
		}
	}
	{
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_9;
		L_9 = VuforiaCameraUtil_GetCamera_m19816CEAC1F906DE62B392F97D8053884E271200(/*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_10 = L_9;
		V_1 = L_10;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6(L_10, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0060;
		}
	}
	{
		// centerPointWCS = cam.transform.TransformPoint(Center);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_12 = V_1;
		NullCheck(L_12);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13;
		L_13 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_12, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = __this->get_Center_29();
		NullCheck(L_13);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Transform_TransformPoint_m68AF95765A9279192E601208A9C5170027A5F0D2(L_13, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
	}

IL_0060:
	{
		// return centerPointWCS;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = V_0;
		return L_16;
	}
}
// System.Void VFX.TargetVFX::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVFX__ctor_mA959CD832A2400C0F6587055E926E18DFB74AF9F (TargetVFX_t6AC76B5316EBFEB98C345E134A1C1E6EF6B13F36 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral787984D270B549500FD6EE450785085D7058DF70);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79D0BA8792ECF6CF89F7E01C15E87AE01EF1D0A0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8ACFE282183F2EC454241D1009D4791A8AA31840);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB2A2A23C0F95CC4BF23BDB0980D559F59E054C4B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF78A3EB3B3AD1250763ED45FCFB5C38360A66955);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public bool IsTargetShownOnTargetFound = true;
		__this->set_IsTargetShownOnTargetFound_22((bool)1);
		// public string ShaderCenter = "_Center";
		__this->set_ShaderCenter_24(_stringLiteral787984D270B549500FD6EE450785085D7058DF70);
		// public string ShaderAxisX = "_AxisX";
		__this->set_ShaderAxisX_25(_stringLiteralF78A3EB3B3AD1250763ED45FCFB5C38360A66955);
		// public string ShaderAxisY = "_AxisY";
		__this->set_ShaderAxisY_26(_stringLiteral8ACFE282183F2EC454241D1009D4791A8AA31840);
		// public string ShaderAxisZ = "_AxisZ";
		__this->set_ShaderAxisZ_27(_stringLiteral79D0BA8792ECF6CF89F7E01C15E87AE01EF1D0A0);
		// public string ShaderScale = "_Scale";
		__this->set_ShaderScale_28(_stringLiteralB2A2A23C0F95CC4BF23BDB0980D559F59E054C4B);
		// public Vector3 Center = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = Vector3_get_zero_m92B6E46FC9A637D20B3A4C1FFAEABFCE095DD4C6(/*hidden argument*/NULL);
		__this->set_Center_29(L_0);
		// TargetStatus mPreviousTargetStatus = TargetStatus.NotObserved;
		TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1  L_1;
		L_1 = TargetStatus_get_NotObserved_mB76C66781AFB21647114F43B899E57422A61EE90(/*hidden argument*/NULL);
		__this->set_mPreviousTargetStatus_32(L_1);
		ShaderVFX__ctor_m5BEEA87E5640310F903984FB48972724AA17A475(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Camera VFX.VuforiaCameraUtil::GetCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * VuforiaCameraUtil_GetCamera_m19816CEAC1F906DE62B392F97D8053884E271200 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&VuforiaCameraUtil_t2170A5AB3BE377F2E409B7BAC5EB8663062E2275_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (mVuforiaBehaviourCamera == null)
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0 = ((VuforiaCameraUtil_t2170A5AB3BE377F2E409B7BAC5EB8663062E2275_StaticFields*)il2cpp_codegen_static_fields_for(VuforiaCameraUtil_t2170A5AB3BE377F2E409B7BAC5EB8663062E2275_il2cpp_TypeInfo_var))->get_mVuforiaBehaviourCamera_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_m6D7EBC0E6D7E0CE1E9671D21DE14C9158AFB88B2(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		// mVuforiaBehaviourCamera = VuforiaBehaviour.Instance.GetComponent<Camera>();
		VuforiaBehaviour_t6114F20D1970225E083A4FBAFB269FA524FEF407 * L_2;
		L_2 = VuforiaBehaviour_get_Instance_mF563DFB36205C62EAEC611673162CF5016DA5A03(/*hidden argument*/NULL);
		NullCheck(L_2);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_3;
		L_3 = Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320(L_2, /*hidden argument*/Component_GetComponent_TisCamera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_mE17146EF5B0D8E9F9D2D2D94567BF211AD00D320_RuntimeMethod_var);
		((VuforiaCameraUtil_t2170A5AB3BE377F2E409B7BAC5EB8663062E2275_StaticFields*)il2cpp_codegen_static_fields_for(VuforiaCameraUtil_t2170A5AB3BE377F2E409B7BAC5EB8663062E2275_il2cpp_TypeInfo_var))->set_mVuforiaBehaviourCamera_0(L_3);
	}

IL_001c:
	{
		// return mVuforiaBehaviourCamera;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_4 = ((VuforiaCameraUtil_t2170A5AB3BE377F2E409B7BAC5EB8663062E2275_StaticFields*)il2cpp_codegen_static_fields_for(VuforiaCameraUtil_t2170A5AB3BE377F2E409B7BAC5EB8663062E2275_il2cpp_TypeInfo_var))->get_mVuforiaBehaviourCamera_0();
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 VFX.VuforiaObserverUtil::GetTargetSize(Vuforia.ObserverBehaviour)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  VuforiaObserverUtil_GetTargetSize_mBF9FA09D93599B7000922B6C7C6D852E07C74949 (ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * ___observer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34 * V_0 = NULL;
	CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3 * V_1 = NULL;
	ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275 * V_2 = NULL;
	AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787 * V_3 = NULL;
	VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2 * V_4 = NULL;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_7;
	memset((&V_7), 0, sizeof(V_7));
	{
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_0 = ___observer0;
		V_0 = ((ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34 *)IsInstClass((RuntimeObject*)L_0, ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34_il2cpp_TypeInfo_var));
		ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34 * L_1 = V_0;
		if (L_1)
		{
			goto IL_0036;
		}
	}
	{
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_2 = ___observer0;
		V_1 = ((CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3 *)IsInstClass((RuntimeObject*)L_2, CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3_il2cpp_TypeInfo_var));
		CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3 * L_3 = V_1;
		if (L_3)
		{
			goto IL_0042;
		}
	}
	{
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_4 = ___observer0;
		V_2 = ((ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275 *)IsInstClass((RuntimeObject*)L_4, ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275_il2cpp_TypeInfo_var));
		ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275 * L_5 = V_2;
		if (L_5)
		{
			goto IL_0069;
		}
	}
	{
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_6 = ___observer0;
		V_3 = ((AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787 *)IsInstClass((RuntimeObject*)L_6, AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787_il2cpp_TypeInfo_var));
		AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787 * L_7 = V_3;
		if (L_7)
		{
			goto IL_0079;
		}
	}
	{
		ObserverBehaviour_tE7AD12CD804A13F758F693B5A1C130E332042274 * L_8 = ___observer0;
		V_4 = ((VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2 *)IsInstClass((RuntimeObject*)L_8, VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2_il2cpp_TypeInfo_var));
		VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2 * L_9 = V_4;
		if (L_9)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_0096;
	}

IL_0036:
	{
		// return imageTarget.GetSize();
		ImageTargetBehaviour_t27A2C2A9CACD079997511A154C4097D348AF4A34 * L_10 = V_0;
		NullCheck(L_10);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_11;
		L_11 = ImageTargetBehaviour_GetSize_mA174549F8B005D0ED36D782B91D22E5D668EBD34(L_10, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector2_op_Implicit_mC91C21911FDB26792369C645DAABBC43B3DE9932_inline(L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_0042:
	{
		// var vertSize = cylinderTarget.SideLength;
		CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3 * L_13 = V_1;
		NullCheck(L_13);
		float L_14;
		L_14 = CylinderTargetBehaviour_get_SideLength_m6189CDEA133898BED4791772B39C9DA596C21755_inline(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		// var horizSize = Mathf.Max(cylinderTarget.BottomDiameter, cylinderTarget.TopDiameter);
		CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3 * L_15 = V_1;
		NullCheck(L_15);
		float L_16;
		L_16 = CylinderTargetBehaviour_get_BottomDiameter_mF34465750334C97B4F115D3190CA2D7CECF6C57A(L_15, /*hidden argument*/NULL);
		CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3 * L_17 = V_1;
		NullCheck(L_17);
		float L_18;
		L_18 = CylinderTargetBehaviour_get_TopDiameter_m7E190963401492CDE34330527C15EEC393B41C94(L_17, /*hidden argument*/NULL);
		float L_19;
		L_19 = Mathf_Max_m5C96B726079E95BB1A1DC60532553CB723D24C79(L_16, L_18, /*hidden argument*/NULL);
		V_6 = L_19;
		// return new Vector3(horizSize, vertSize, horizSize);
		float L_20 = V_6;
		float L_21 = V_5;
		float L_22 = V_6;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), L_20, L_21, L_22, /*hidden argument*/NULL);
		return L_23;
	}

IL_0069:
	{
		// return modelTarget.GetBoundingBox().size;
		ModelTargetBehaviour_tE74C22102A1002F6E127C63AE5A15FCDD8CFD275 * L_24 = V_2;
		NullCheck(L_24);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_25;
		L_25 = ModelTargetBehaviour_GetBoundingBox_m829B1E301CFBB50F375C59A1B40710449FAB56F8(L_24, /*hidden argument*/NULL);
		V_7 = L_25;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26;
		L_26 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_7), /*hidden argument*/NULL);
		return L_26;
	}

IL_0079:
	{
		// return areaTarget.GetBoundingBox().size;
		AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787 * L_27 = V_3;
		NullCheck(L_27);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_28;
		L_28 = AreaTargetBehaviour_GetBoundingBox_m7F45017B673F5C2CF4BB3B3F8117C2DC34F88DA9_inline(L_27, /*hidden argument*/NULL);
		V_7 = L_28;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_7), /*hidden argument*/NULL);
		return L_29;
	}

IL_0089:
	{
		// return vuMark.GetSize();
		VuMarkBehaviour_t0AA6A311FEEA9FC34EE628BE9CBB2374B68B5EB2 * L_30 = V_4;
		NullCheck(L_30);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_31;
		L_31 = VuMarkBehaviour_GetSize_m113E22E6E5B01275F70F64673B95D753C95E2A2E(L_30, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Vector2_op_Implicit_mC91C21911FDB26792369C645DAABBC43B3DE9932_inline(L_31, /*hidden argument*/NULL);
		return L_32;
	}

IL_0096:
	{
		// return Vector3.one;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		L_33 = Vector3_get_one_mFA8E564BB81364E4E65551816F3631176E7F58E7(/*hidden argument*/NULL);
		return L_33;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m5E223DB365EAC8F6625C169E927527FFB8CC88DB_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		float L_1;
		L_1 = Mathf_Clamp01_m831CBA1D198C3CDE660E8172A67A4E41BD0D0171(L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___b1;
		float L_5 = L_4.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_x_2();
		float L_8 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ___a0;
		float L_10 = L_9.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ___b1;
		float L_12 = L_11.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = ___a0;
		float L_14 = L_13.get_y_3();
		float L_15 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = ___a0;
		float L_17 = L_16.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = ___b1;
		float L_19 = L_18.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = ___a0;
		float L_21 = L_20.get_z_4();
		float L_22 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), ((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), (float)L_8)))), ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_14)), (float)L_15)))), ((float)il2cpp_codegen_add((float)L_17, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_19, (float)L_21)), (float)L_22)))), /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0053;
	}

IL_0053:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		return L_24;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t TargetStatus_get_Status_mD745DEB3CFE4D18A1E973178AA8CA9BB73178D5E_inline (TargetStatus_t6840B338FC8C013F0E06F9E3530E7177434C7AF1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CStatusU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_mC91C21911FDB26792369C645DAABBC43B3DE9932_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_4), L_1, L_3, (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float CylinderTargetBehaviour_get_SideLength_m6189CDEA133898BED4791772B39C9DA596C21755_inline (CylinderTargetBehaviour_tF2AA0F76979253825AAC2D17965D75BB814A47E3 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_mSideLength_17();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  AreaTargetBehaviour_GetBoundingBox_m7F45017B673F5C2CF4BB3B3F8117C2DC34F88DA9_inline (AreaTargetBehaviour_t2E84336789F8D426D07F8A16DD99B2C84105F787 * __this, const RuntimeMethod* method)
{
	{
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_0 = __this->get_mBoundingBox_20();
		return L_0;
	}
}

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Single VFX.CameraUtil::GetHorizontalFovRadians(UnityEngine.Camera)
extern void CameraUtil_GetHorizontalFovRadians_m1D9984182F0FA5E5B51D03EB819B79D9EA4B83A2 (void);
// 0x00000002 System.Single VFX.CameraUtil::GetVerticalFovRadians(UnityEngine.Camera)
extern void CameraUtil_GetVerticalFovRadians_m807C945267F641096E42436DA6D27E72D99D657B (void);
// 0x00000003 System.Void VFX.MaterialUtil::ApplyMaterial(UnityEngine.GameObject,UnityEngine.Material,System.Boolean)
extern void MaterialUtil_ApplyMaterial_m14E588D20F59FF1961AD91860E973022A2365EA0 (void);
// 0x00000004 System.Void VFX.MaterialUtil::ApplyMaterial(UnityEngine.Renderer,UnityEngine.Material)
extern void MaterialUtil_ApplyMaterial_m7831BB04523583FA950AC3A85DCA217388750CD3 (void);
// 0x00000005 System.Void VFX.MaterialUtil::ApplyAlpha(UnityEngine.GameObject,System.Single,System.Boolean)
extern void MaterialUtil_ApplyAlpha_m69FE6BC56298EBB7CAF3E9D84CE8BD93CEB2D7B1 (void);
// 0x00000006 System.Void VFX.MaterialUtil::ApplyAlpha(UnityEngine.Renderer,System.Single)
extern void MaterialUtil_ApplyAlpha_mB27BEAF53795DBF0962AA2CD8DC91668F6B31B38 (void);
// 0x00000007 System.Void VFX.MaterialUtil::TrySetVector4Property(UnityEngine.Material,System.String,UnityEngine.Vector4)
extern void MaterialUtil_TrySetVector4Property_m2036427B77E884AED74B6654D2C353C2485F7F86 (void);
// 0x00000008 System.Void VFX.MaterialUtil::TrySetFloatProperty(UnityEngine.Material,System.String,System.Single)
extern void MaterialUtil_TrySetFloatProperty_m82611514F3F1235245B165AF05F3930AD40EB0DE (void);
// 0x00000009 System.Void VFX.MaterialUtil::TrySetColorProperty(UnityEngine.Material,System.String,UnityEngine.Color)
extern void MaterialUtil_TrySetColorProperty_mB063CAB2A94E153D8C0272E2E3F2950E4EB420C7 (void);
// 0x0000000A UnityEngine.Matrix4x4 VFX.MatrixUtil::MatrixFromArray(System.Single[])
extern void MatrixUtil_MatrixFromArray_m946E83A5FCB42DB3966D16EBC7DE919A42371ACE (void);
// 0x0000000B UnityEngine.Matrix4x4 VFX.MatrixUtil::GetFromToMatrix(UnityEngine.Transform,UnityEngine.Transform)
extern void MatrixUtil_GetFromToMatrix_m199EAB1081834A5130D0057E142983A97DB0DFFA (void);
// 0x0000000C UnityEngine.Quaternion VFX.MatrixUtil::GetRotation(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetRotation_mE36F82D3ADB6A6347C2DE322ADD2340F0298A949 (void);
// 0x0000000D UnityEngine.Vector3 VFX.MatrixUtil::GetPosition(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetPosition_m2FC6AAEB0FDD729E8C6D7FB59A87AE771DAA109A (void);
// 0x0000000E UnityEngine.Vector3 VFX.MatrixUtil::GetScale(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetScale_mE023A02C901A5C88CC4D1CD4D33529CCE000ED7B (void);
// 0x0000000F System.Single VFX.MatrixUtil::GetHorizontalFovRadians(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetHorizontalFovRadians_m5D87D6BC1CE7C2074DDB718CB68EBF898158BD0D (void);
// 0x00000010 System.Single VFX.MatrixUtil::GetHorizontalFovDegrees(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetHorizontalFovDegrees_m082F68DFA5934B930CEC0EC1C5285E48D6E9D51C (void);
// 0x00000011 System.Single VFX.MatrixUtil::GetVerticalFovRadians(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetVerticalFovRadians_mA0E8FE025A354CCB596100AA17BEF147C210C4DB (void);
// 0x00000012 System.Single VFX.MatrixUtil::GetVerticalFovDegrees(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetVerticalFovDegrees_m48A11682D3CAA973F688AFDE60B1CBEC96C5089E (void);
// 0x00000013 System.Void VFX.CameraDepthMode::Awake()
extern void CameraDepthMode_Awake_m768DC20EEE4DAE32CAA73052E0467F50A77266C8 (void);
// 0x00000014 System.Void VFX.CameraDepthMode::.ctor()
extern void CameraDepthMode__ctor_m8C044FEB0BD6994E281E5E078AD91D2D1A6A2732 (void);
// 0x00000015 System.Void VFX.ShaderVFX::Awake()
extern void ShaderVFX_Awake_m7AA549E6EF3CDE422212410D6C601DA96942D7AA (void);
// 0x00000016 System.Void VFX.ShaderVFX::Update()
extern void ShaderVFX_Update_m40A13C7DC127AB3AA63AF22A1980F951449E48D9 (void);
// 0x00000017 System.Void VFX.ShaderVFX::Play()
extern void ShaderVFX_Play_m72C1E7E60D6C926DE3F1BB3B521C5D545790FBC9 (void);
// 0x00000018 System.Void VFX.ShaderVFX::Pause()
extern void ShaderVFX_Pause_m1AD628112529913D6169E3A230CFA1604A3A3A56 (void);
// 0x00000019 System.Void VFX.ShaderVFX::Rewind()
extern void ShaderVFX_Rewind_mEDF042F61293E7EE2E5EB624CDBAA0A16023E31A (void);
// 0x0000001A System.Void VFX.ShaderVFX::RewindAndPlay()
extern void ShaderVFX_RewindAndPlay_m0BA24237FD8ACA7A257AA6B59DA6A667F7059C6E (void);
// 0x0000001B System.Void VFX.ShaderVFX::Stop()
extern void ShaderVFX_Stop_mBCA727EAABBEE4C680E441082ABE3E595C5D3CC4 (void);
// 0x0000001C System.Void VFX.ShaderVFX::UpdateDynamicColors(System.Single&)
extern void ShaderVFX_UpdateDynamicColors_m141A35CB6F0A1E6C2C7706232FDF412DF2696012 (void);
// 0x0000001D System.Void VFX.ShaderVFX::UpdateDynamicVectorProperties(System.Single&)
extern void ShaderVFX_UpdateDynamicVectorProperties_mFC501F25780E0ACC10326775FA29DF680DF7B667 (void);
// 0x0000001E System.Void VFX.ShaderVFX::UpdateDynamicScalarProperties(System.Single&)
extern void ShaderVFX_UpdateDynamicScalarProperties_m37DF39590DE58AD7E4DE8798A8D3B5CC5EC04EC0 (void);
// 0x0000001F System.Single VFX.ShaderVFX::SetMaxTimeAndGetLerpValue(System.Single&,System.Single,System.Single)
extern void ShaderVFX_SetMaxTimeAndGetLerpValue_m6E10A14B06144F9F242E15ECDF3BB90ACC0A6483 (void);
// 0x00000020 System.Void VFX.ShaderVFX::InitMaterial()
extern void ShaderVFX_InitMaterial_mCF4D5E111B66F2B7853AA266F450BDF3D7005EE5 (void);
// 0x00000021 System.Void VFX.ShaderVFX::InitStaticProperties()
extern void ShaderVFX_InitStaticProperties_m3AB1B50B9F3F3FD9D5783815F5CB48D82103807C (void);
// 0x00000022 System.Void VFX.ShaderVFX::InitDynamicProperties()
extern void ShaderVFX_InitDynamicProperties_m6AEFC94FBC6B76E1FCCD7BFA07D2F5E6F63DEE02 (void);
// 0x00000023 System.Void VFX.ShaderVFX::.ctor()
extern void ShaderVFX__ctor_m5BEEA87E5640310F903984FB48972724AA17A475 (void);
// 0x00000024 System.Void VFX.TargetVFX::Awake()
extern void TargetVFX_Awake_m3C692394F93453E2D821EB12A62F0F2DF79CAB50 (void);
// 0x00000025 System.Void VFX.TargetVFX::Update()
extern void TargetVFX_Update_m66C4A427F20682E60BEDFF684163FB56C648A3B0 (void);
// 0x00000026 System.Void VFX.TargetVFX::OnDestroy()
extern void TargetVFX_OnDestroy_mFFE073F460381C34016D865A8B3DC9394E441F36 (void);
// 0x00000027 System.Void VFX.TargetVFX::UpdateShaderScale()
extern void TargetVFX_UpdateShaderScale_m14C9D840F99D8B1DE1FB87A01A012070B740B82C (void);
// 0x00000028 System.Void VFX.TargetVFX::TargetStatusChanged(Vuforia.ObserverBehaviour,Vuforia.TargetStatus)
extern void TargetVFX_TargetStatusChanged_m4BCF8695AF732A32C4E4DC69A551DBAE1E6BB465 (void);
// 0x00000029 System.Boolean VFX.TargetVFX::IsTracked(Vuforia.TargetStatus)
extern void TargetVFX_IsTracked_m885C810BE91774EC58936F694042A78B41F28478 (void);
// 0x0000002A System.Boolean VFX.TargetVFX::IsUnTracked(Vuforia.TargetStatus)
extern void TargetVFX_IsUnTracked_m706016647B2DF9E29105F351BB08DC15DF89F764 (void);
// 0x0000002B System.Void VFX.TargetVFX::OnTargetFound()
extern void TargetVFX_OnTargetFound_mD2BB51BCFAF8C3066A27ACEF54DD0FF216BB1D2C (void);
// 0x0000002C System.Void VFX.TargetVFX::UpdateShaderCenterAndAxis()
extern void TargetVFX_UpdateShaderCenterAndAxis_m6D4F079A02F16ABA5F6640F21A3A909BACE643B5 (void);
// 0x0000002D System.Void VFX.TargetVFX::GetAxisVectors(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void TargetVFX_GetAxisVectors_mDAEF2F3A57F51777FA1CA49CB9315796D2633CED (void);
// 0x0000002E UnityEngine.Vector3 VFX.TargetVFX::GetCenterPointWCS()
extern void TargetVFX_GetCenterPointWCS_m6A62C57D93BF95E1F93585E65E072512C88E4950 (void);
// 0x0000002F System.Void VFX.TargetVFX::.ctor()
extern void TargetVFX__ctor_mA959CD832A2400C0F6587055E926E18DFB74AF9F (void);
// 0x00000030 UnityEngine.Camera VFX.VuforiaCameraUtil::GetCamera()
extern void VuforiaCameraUtil_GetCamera_m19816CEAC1F906DE62B392F97D8053884E271200 (void);
// 0x00000031 UnityEngine.Vector3 VFX.VuforiaObserverUtil::GetTargetSize(Vuforia.ObserverBehaviour)
extern void VuforiaObserverUtil_GetTargetSize_mBF9FA09D93599B7000922B6C7C6D852E07C74949 (void);
static Il2CppMethodPointer s_methodPointers[49] = 
{
	CameraUtil_GetHorizontalFovRadians_m1D9984182F0FA5E5B51D03EB819B79D9EA4B83A2,
	CameraUtil_GetVerticalFovRadians_m807C945267F641096E42436DA6D27E72D99D657B,
	MaterialUtil_ApplyMaterial_m14E588D20F59FF1961AD91860E973022A2365EA0,
	MaterialUtil_ApplyMaterial_m7831BB04523583FA950AC3A85DCA217388750CD3,
	MaterialUtil_ApplyAlpha_m69FE6BC56298EBB7CAF3E9D84CE8BD93CEB2D7B1,
	MaterialUtil_ApplyAlpha_mB27BEAF53795DBF0962AA2CD8DC91668F6B31B38,
	MaterialUtil_TrySetVector4Property_m2036427B77E884AED74B6654D2C353C2485F7F86,
	MaterialUtil_TrySetFloatProperty_m82611514F3F1235245B165AF05F3930AD40EB0DE,
	MaterialUtil_TrySetColorProperty_mB063CAB2A94E153D8C0272E2E3F2950E4EB420C7,
	MatrixUtil_MatrixFromArray_m946E83A5FCB42DB3966D16EBC7DE919A42371ACE,
	MatrixUtil_GetFromToMatrix_m199EAB1081834A5130D0057E142983A97DB0DFFA,
	MatrixUtil_GetRotation_mE36F82D3ADB6A6347C2DE322ADD2340F0298A949,
	MatrixUtil_GetPosition_m2FC6AAEB0FDD729E8C6D7FB59A87AE771DAA109A,
	MatrixUtil_GetScale_mE023A02C901A5C88CC4D1CD4D33529CCE000ED7B,
	MatrixUtil_GetHorizontalFovRadians_m5D87D6BC1CE7C2074DDB718CB68EBF898158BD0D,
	MatrixUtil_GetHorizontalFovDegrees_m082F68DFA5934B930CEC0EC1C5285E48D6E9D51C,
	MatrixUtil_GetVerticalFovRadians_mA0E8FE025A354CCB596100AA17BEF147C210C4DB,
	MatrixUtil_GetVerticalFovDegrees_m48A11682D3CAA973F688AFDE60B1CBEC96C5089E,
	CameraDepthMode_Awake_m768DC20EEE4DAE32CAA73052E0467F50A77266C8,
	CameraDepthMode__ctor_m8C044FEB0BD6994E281E5E078AD91D2D1A6A2732,
	ShaderVFX_Awake_m7AA549E6EF3CDE422212410D6C601DA96942D7AA,
	ShaderVFX_Update_m40A13C7DC127AB3AA63AF22A1980F951449E48D9,
	ShaderVFX_Play_m72C1E7E60D6C926DE3F1BB3B521C5D545790FBC9,
	ShaderVFX_Pause_m1AD628112529913D6169E3A230CFA1604A3A3A56,
	ShaderVFX_Rewind_mEDF042F61293E7EE2E5EB624CDBAA0A16023E31A,
	ShaderVFX_RewindAndPlay_m0BA24237FD8ACA7A257AA6B59DA6A667F7059C6E,
	ShaderVFX_Stop_mBCA727EAABBEE4C680E441082ABE3E595C5D3CC4,
	ShaderVFX_UpdateDynamicColors_m141A35CB6F0A1E6C2C7706232FDF412DF2696012,
	ShaderVFX_UpdateDynamicVectorProperties_mFC501F25780E0ACC10326775FA29DF680DF7B667,
	ShaderVFX_UpdateDynamicScalarProperties_m37DF39590DE58AD7E4DE8798A8D3B5CC5EC04EC0,
	ShaderVFX_SetMaxTimeAndGetLerpValue_m6E10A14B06144F9F242E15ECDF3BB90ACC0A6483,
	ShaderVFX_InitMaterial_mCF4D5E111B66F2B7853AA266F450BDF3D7005EE5,
	ShaderVFX_InitStaticProperties_m3AB1B50B9F3F3FD9D5783815F5CB48D82103807C,
	ShaderVFX_InitDynamicProperties_m6AEFC94FBC6B76E1FCCD7BFA07D2F5E6F63DEE02,
	ShaderVFX__ctor_m5BEEA87E5640310F903984FB48972724AA17A475,
	TargetVFX_Awake_m3C692394F93453E2D821EB12A62F0F2DF79CAB50,
	TargetVFX_Update_m66C4A427F20682E60BEDFF684163FB56C648A3B0,
	TargetVFX_OnDestroy_mFFE073F460381C34016D865A8B3DC9394E441F36,
	TargetVFX_UpdateShaderScale_m14C9D840F99D8B1DE1FB87A01A012070B740B82C,
	TargetVFX_TargetStatusChanged_m4BCF8695AF732A32C4E4DC69A551DBAE1E6BB465,
	TargetVFX_IsTracked_m885C810BE91774EC58936F694042A78B41F28478,
	TargetVFX_IsUnTracked_m706016647B2DF9E29105F351BB08DC15DF89F764,
	TargetVFX_OnTargetFound_mD2BB51BCFAF8C3066A27ACEF54DD0FF216BB1D2C,
	TargetVFX_UpdateShaderCenterAndAxis_m6D4F079A02F16ABA5F6640F21A3A909BACE643B5,
	TargetVFX_GetAxisVectors_mDAEF2F3A57F51777FA1CA49CB9315796D2633CED,
	TargetVFX_GetCenterPointWCS_m6A62C57D93BF95E1F93585E65E072512C88E4950,
	TargetVFX__ctor_mA959CD832A2400C0F6587055E926E18DFB74AF9F,
	VuforiaCameraUtil_GetCamera_m19816CEAC1F906DE62B392F97D8053884E271200,
	VuforiaObserverUtil_GetTargetSize_mBF9FA09D93599B7000922B6C7C6D852E07C74949,
};
static const int32_t s_InvokerIndices[49] = 
{
	5216,
	5216,
	4497,
	4866,
	4508,
	4869,
	4507,
	4503,
	4498,
	5083,
	4701,
	5184,
	5275,
	5275,
	5215,
	5215,
	5215,
	5215,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2793,
	2793,
	2793,
	877,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	1587,
	2119,
	2119,
	3595,
	3595,
	889,
	3590,
	3595,
	5377,
	5276,
};
extern const CustomAttributesCacheGenerator g_VFXCore_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_VFXCore_CodeGenModule;
const Il2CppCodeGenModule g_VFXCore_CodeGenModule = 
{
	"VFXCore.dll",
	49,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_VFXCore_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

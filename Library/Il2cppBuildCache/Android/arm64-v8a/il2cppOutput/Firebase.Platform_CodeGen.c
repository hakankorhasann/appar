﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Collections.Generic.List`1<System.Exception> Firebase.ExceptionAggregator::get_Exceptions()
extern void ExceptionAggregator_get_Exceptions_m7151F9FF0E2168208A9E2981FB3DAB015DAF1E7B (void);
// 0x00000002 System.Exception Firebase.ExceptionAggregator::GetAndClearPendingExceptions()
extern void ExceptionAggregator_GetAndClearPendingExceptions_m448ABC9A2AE8264581AA8C324799D3E826D0BC47 (void);
// 0x00000003 System.Void Firebase.ExceptionAggregator::ThrowAndClearPendingExceptions()
extern void ExceptionAggregator_ThrowAndClearPendingExceptions_m0EB19F8E705293D9978B62B63EDFBC7ED4A2403E (void);
// 0x00000004 System.Exception Firebase.ExceptionAggregator::LogException(System.Exception)
extern void ExceptionAggregator_LogException_m86E9668A4AF70331361FDAA86F83A90125FE2B18 (void);
// 0x00000005 System.Void Firebase.ExceptionAggregator::Wrap(System.Action)
extern void ExceptionAggregator_Wrap_m286D2940E6E4C43E3C9B5E23A249A63B731390B4 (void);
// 0x00000006 System.Void Firebase.ExceptionAggregator::.cctor()
extern void ExceptionAggregator__cctor_m2C0E0D699411728432F1AA96407CFFCABFB90EF9 (void);
// 0x00000007 System.Void Firebase.Dispatcher::.ctor()
extern void Dispatcher__ctor_mE99B93199AD38F0A53946F925713CC84099C35B3 (void);
// 0x00000008 TResult Firebase.Dispatcher::Run(System.Func`1<TResult>)
// 0x00000009 System.Threading.Tasks.Task`1<TResult> Firebase.Dispatcher::RunAsync(System.Func`1<TResult>)
// 0x0000000A System.Threading.Tasks.Task`1<TResult> Firebase.Dispatcher::RunAsyncNow(System.Func`1<TResult>)
// 0x0000000B System.Boolean Firebase.Dispatcher::ManagesThisThread()
extern void Dispatcher_ManagesThisThread_mDD4799366D040E2A27D9501C6AE8D2C88AAE086A (void);
// 0x0000000C System.Void Firebase.Dispatcher::PollJobs()
extern void Dispatcher_PollJobs_m4DD47E8B63F33D975BC1A7D670A509F597F8E9E0 (void);
// 0x0000000D TResult Firebase.Dispatcher/CallbackStorage`1::get_Result()
// 0x0000000E System.Void Firebase.Dispatcher/CallbackStorage`1::set_Result(TResult)
// 0x0000000F System.Exception Firebase.Dispatcher/CallbackStorage`1::get_Exception()
// 0x00000010 System.Void Firebase.Dispatcher/CallbackStorage`1::set_Exception(System.Exception)
// 0x00000011 System.Void Firebase.Dispatcher/CallbackStorage`1::.ctor()
// 0x00000012 System.Void Firebase.Dispatcher/<>c__DisplayClass4_0`1::.ctor()
// 0x00000013 System.Void Firebase.Dispatcher/<>c__DisplayClass4_0`1::<Run>b__0()
// 0x00000014 System.Void Firebase.Dispatcher/<>c__DisplayClass5_0`1::.ctor()
// 0x00000015 System.Void Firebase.Dispatcher/<>c__DisplayClass5_0`1::<RunAsync>b__0()
// 0x00000016 Firebase.Unity.UnityLoggingService Firebase.Unity.UnityLoggingService::get_Instance()
extern void UnityLoggingService_get_Instance_mBE89DCE5CD59076E6242362A75CDA3C47390D704 (void);
// 0x00000017 System.Void Firebase.Unity.UnityLoggingService::.ctor()
extern void UnityLoggingService__ctor_m40EA0D43E2C571DD040E6239AFFA67D4A16A258D (void);
// 0x00000018 System.Void Firebase.Unity.UnityLoggingService::.cctor()
extern void UnityLoggingService__cctor_m50B3E83067D7E2E1B89E3A81B46DEF7E2C38B00E (void);
// 0x00000019 System.Void Firebase.Unity.UnityPlatformServices::SetupServices()
extern void UnityPlatformServices_SetupServices_mAE2AAD154596FA336453CB1823F53845F6A8DB44 (void);
// 0x0000001A System.Void Firebase.Unity.UnitySynchronizationContext::.ctor(UnityEngine.GameObject)
extern void UnitySynchronizationContext__ctor_m16FA79368D74F4CCBF96D397B8C334CCC8A40720 (void);
// 0x0000001B System.Void Firebase.Unity.UnitySynchronizationContext::Create(UnityEngine.GameObject)
extern void UnitySynchronizationContext_Create_m3835F4FC47D7841007E8AA336FB350F1B1A08C8A (void);
// 0x0000001C System.Void Firebase.Unity.UnitySynchronizationContext::Destroy()
extern void UnitySynchronizationContext_Destroy_m356D29922F6C827A245669A9D4179F73E52BABB4 (void);
// 0x0000001D System.Threading.ManualResetEvent Firebase.Unity.UnitySynchronizationContext::GetThreadEvent()
extern void UnitySynchronizationContext_GetThreadEvent_mC98D54E3250279ACF4A26392835855D729D729F0 (void);
// 0x0000001E System.Void Firebase.Unity.UnitySynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Post_mFFC9993A213F2DE9D2AB3003272BD0D62486C6E6 (void);
// 0x0000001F System.Void Firebase.Unity.UnitySynchronizationContext::Send(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Send_m0BDC4069F41293598BA84281FCBBA4C46DAC00A4 (void);
// 0x00000020 System.Void Firebase.Unity.UnitySynchronizationContext::.cctor()
extern void UnitySynchronizationContext__cctor_mC98553E4C6F16C7BD01A85C244D976CBEE91C690 (void);
// 0x00000021 System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>> Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir::get_CallbackQueue()
extern void SynchronizationContextBehavoir_get_CallbackQueue_m0E34CE99C7CA3A50D0B5960110C97B35F1464181 (void);
// 0x00000022 System.Collections.IEnumerator Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir::Start()
extern void SynchronizationContextBehavoir_Start_mC0020EEF1D711C56559978F6B05D5980607B1415 (void);
// 0x00000023 System.Void Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir::.ctor()
extern void SynchronizationContextBehavoir__ctor_mB7DAD13667448BFE9813A53C0AEEA01D2ABB23E7 (void);
// 0x00000024 System.Void Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>d__3::.ctor(System.Int32)
extern void U3CStartU3Ed__3__ctor_mE286810BB74A14A6CA89DBD541556E73EE6F1873 (void);
// 0x00000025 System.Void Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>d__3::System.IDisposable.Dispose()
extern void U3CStartU3Ed__3_System_IDisposable_Dispose_mA3F0338173978F9E1F1C95BCD8A5C65C7DA5464B (void);
// 0x00000026 System.Boolean Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>d__3::MoveNext()
extern void U3CStartU3Ed__3_MoveNext_mF7311EB49D00661ED24EBB2AD6A43FC8FB69451E (void);
// 0x00000027 System.Object Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB792B39126660A53266DF372A408359EFCE5CCA (void);
// 0x00000028 System.Void Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>d__3::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__3_System_Collections_IEnumerator_Reset_m0700DB59F5C1825F093EE05044976C7C512A8026 (void);
// 0x00000029 System.Object Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__3_System_Collections_IEnumerator_get_Current_mDE0E18D92FEA57AF47CB64A273477B674A114DFD (void);
// 0x0000002A System.Void Firebase.Unity.UnitySynchronizationContext/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_mBF75F76D3921A9186C2576BDC0158F2748855D64 (void);
// 0x0000002B System.Void Firebase.Unity.UnitySynchronizationContext/<>c__DisplayClass14_1::.ctor()
extern void U3CU3Ec__DisplayClass14_1__ctor_m7A98D58E7972709AD2351CD86F0C8EF10E75A5D1 (void);
// 0x0000002C System.Void Firebase.Unity.UnitySynchronizationContext/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m864C9D4FA34AF22ECD795F3344D81BEDBA8BAE66 (void);
// 0x0000002D System.Void Firebase.Unity.UnitySynchronizationContext/<>c__DisplayClass16_1::.ctor()
extern void U3CU3Ec__DisplayClass16_1__ctor_mB967C90C84AC0EC6AE041D0ECEF6D1AB66069C80 (void);
// 0x0000002E System.Void Firebase.Unity.UnitySynchronizationContext/<>c__DisplayClass16_1::<Send>b__0(System.Object)
extern void U3CU3Ec__DisplayClass16_1_U3CSendU3Eb__0_mAC9FDB5AD8982ABA072566F9C51D439E1E01674B (void);
// 0x0000002F Firebase.Platform.DebugLogger Firebase.Platform.DebugLogger::get_Instance()
extern void DebugLogger_get_Instance_m4D1D9AE0E96B51CD737215A91B33D02358D8887C (void);
// 0x00000030 System.Void Firebase.Platform.DebugLogger::.ctor()
extern void DebugLogger__ctor_mD6357FC79350285A4D8372BC692899939F6D36C4 (void);
// 0x00000031 System.Void Firebase.Platform.DebugLogger::.cctor()
extern void DebugLogger__cctor_mBFBBB23B99D8FD84196BA0C728EF0C59FE96FFDA (void);
// 0x00000032 System.Void Firebase.Platform.Services::.cctor()
extern void Services__cctor_m417FD7FA552048871A01D0082B4A0BC771A6C55A (void);
// 0x00000033 System.Void Firebase.Platform.Services::set_AppConfig(Firebase.Platform.IAppConfigExtensions)
extern void Services_set_AppConfig_mA505D0464F293497554B104DF818E7E44F26FBBA (void);
// 0x00000034 System.Void Firebase.Platform.Services::set_Clock(Firebase.Platform.IClockService)
extern void Services_set_Clock_mA7A638A08B57FCF25A5A314C31ED6DA07FE990DE (void);
// 0x00000035 System.Void Firebase.Platform.Services::set_Logging(Firebase.Platform.ILoggingService)
extern void Services_set_Logging_mDAF2637B76938F809228599C7A73ADB13DC4B477 (void);
// 0x00000036 System.Void Firebase.Platform.IFirebaseAppUtils::TranslateDllNotFoundException(System.Action)
// 0x00000037 System.Void Firebase.Platform.IFirebaseAppUtils::PollCallbacks()
// 0x00000038 Firebase.Platform.PlatformLogLevel Firebase.Platform.IFirebaseAppUtils::GetLogLevel()
// 0x00000039 Firebase.Platform.FirebaseAppUtilsStub Firebase.Platform.FirebaseAppUtilsStub::get_Instance()
extern void FirebaseAppUtilsStub_get_Instance_mCA9D44EBABA8387059AB3995A06E769148CED85C (void);
// 0x0000003A System.Void Firebase.Platform.FirebaseAppUtilsStub::TranslateDllNotFoundException(System.Action)
extern void FirebaseAppUtilsStub_TranslateDllNotFoundException_m05B83DDC2DB8B21CEFC55D0AA990CA16A9390C8E (void);
// 0x0000003B System.Void Firebase.Platform.FirebaseAppUtilsStub::PollCallbacks()
extern void FirebaseAppUtilsStub_PollCallbacks_m13A423E04EC1F3EDFD0D7FE63F18E0D3036346E1 (void);
// 0x0000003C Firebase.Platform.PlatformLogLevel Firebase.Platform.FirebaseAppUtilsStub::GetLogLevel()
extern void FirebaseAppUtilsStub_GetLogLevel_m6082A2182D62F889C842C09F4B5A15074B8F4127 (void);
// 0x0000003D System.Void Firebase.Platform.FirebaseAppUtilsStub::.ctor()
extern void FirebaseAppUtilsStub__ctor_m3EC6F08162223655B3A0A2FA44BA7060AE3884AD (void);
// 0x0000003E System.Void Firebase.Platform.FirebaseAppUtilsStub::.cctor()
extern void FirebaseAppUtilsStub__cctor_mD5A91BFC2B1478924F9C6E908CADAA1A10E14D28 (void);
// 0x0000003F System.Void Firebase.Platform.MainThreadProperty`1::.ctor(System.Func`1<T>)
// 0x00000040 T Firebase.Platform.MainThreadProperty`1::get_Value()
// 0x00000041 T Firebase.Platform.MainThreadProperty`1::<get_Value>b__5_0()
// 0x00000042 Firebase.Platform.IFirebaseAppUtils Firebase.Platform.FirebaseHandler::get_AppUtils()
extern void FirebaseHandler_get_AppUtils_m83D618A43DEDAF4C820368D4B4BB18B517E9529D (void);
// 0x00000043 System.Void Firebase.Platform.FirebaseHandler::set_AppUtils(Firebase.Platform.IFirebaseAppUtils)
extern void FirebaseHandler_set_AppUtils_mEE811D9E5C40519DFB1C9D4E43AF98B0F301DCDF (void);
// 0x00000044 System.Int32 Firebase.Platform.FirebaseHandler::get_TickCount()
extern void FirebaseHandler_get_TickCount_mA6B835A8EDF1DD8A818B74100923D18BB19DF1AC (void);
// 0x00000045 Firebase.Dispatcher Firebase.Platform.FirebaseHandler::get_ThreadDispatcher()
extern void FirebaseHandler_get_ThreadDispatcher_m0704B04915111466D31C391AAF23D593AF6914C3 (void);
// 0x00000046 System.Void Firebase.Platform.FirebaseHandler::set_ThreadDispatcher(Firebase.Dispatcher)
extern void FirebaseHandler_set_ThreadDispatcher_m425931A1B890B0FDE72E207295145220F8F20BC7 (void);
// 0x00000047 System.Boolean Firebase.Platform.FirebaseHandler::get_IsPlayMode()
extern void FirebaseHandler_get_IsPlayMode_m59AFE9F48F2EA1AADCFBD94DBE010730359B3B40 (void);
// 0x00000048 System.Void Firebase.Platform.FirebaseHandler::set_IsPlayMode(System.Boolean)
extern void FirebaseHandler_set_IsPlayMode_m832E6FCD6DA3ACDEA0B59D346EDD8585381C3B15 (void);
// 0x00000049 System.Void Firebase.Platform.FirebaseHandler::.cctor()
extern void FirebaseHandler__cctor_m0D9B254E4259799D9BAFADFD71AD0E2EA35FD74E (void);
// 0x0000004A System.Void Firebase.Platform.FirebaseHandler::.ctor()
extern void FirebaseHandler__ctor_mD79B2E4B647013911BE3A8602014EB108DD1D054 (void);
// 0x0000004B System.Void Firebase.Platform.FirebaseHandler::StartMonoBehaviour()
extern void FirebaseHandler_StartMonoBehaviour_m3258056C31C8D32C690329363587720C60B35E0A (void);
// 0x0000004C System.Void Firebase.Platform.FirebaseHandler::StopMonoBehaviour()
extern void FirebaseHandler_StopMonoBehaviour_m1CB49D4E683D48586AEF5F8B515EAC41F2F502BE (void);
// 0x0000004D TResult Firebase.Platform.FirebaseHandler::RunOnMainThread(System.Func`1<TResult>)
// 0x0000004E System.Threading.Tasks.Task`1<TResult> Firebase.Platform.FirebaseHandler::RunOnMainThreadAsync(System.Func`1<TResult>)
// 0x0000004F Firebase.Platform.FirebaseHandler Firebase.Platform.FirebaseHandler::get_DefaultInstance()
extern void FirebaseHandler_get_DefaultInstance_m4D523A41A2FFB235AAEA0C61DD0E13A759478CC7 (void);
// 0x00000050 System.Void Firebase.Platform.FirebaseHandler::CreatePartialOnMainThread(Firebase.Platform.IFirebaseAppUtils)
extern void FirebaseHandler_CreatePartialOnMainThread_mAE4C7FDD94E3CE3333D6209E4C455F2C8C37EEEC (void);
// 0x00000051 System.Void Firebase.Platform.FirebaseHandler::Create(Firebase.Platform.IFirebaseAppUtils)
extern void FirebaseHandler_Create_m993EB16D9DDC68CC41514B48FD319AE7E7ECB434 (void);
// 0x00000052 System.Void Firebase.Platform.FirebaseHandler::Update()
extern void FirebaseHandler_Update_m054C867D529EEC80853232EEAFBCAD18CC287817 (void);
// 0x00000053 System.Void Firebase.Platform.FirebaseHandler::OnApplicationFocus(System.Boolean)
extern void FirebaseHandler_OnApplicationFocus_mC750E0727206EA64FC16F8448D149EBF82C59F41 (void);
// 0x00000054 System.Void Firebase.Platform.FirebaseHandler::OnMonoBehaviourDestroyed(Firebase.Platform.FirebaseMonoBehaviour)
extern void FirebaseHandler_OnMonoBehaviourDestroyed_m608B6F7487509B972415DA4566AF8A1EDCB73A82 (void);
// 0x00000055 System.Void Firebase.Platform.FirebaseHandler::<Update>b__36_0()
extern void FirebaseHandler_U3CUpdateU3Eb__36_0_mDC0BB815941FD6EF6E26F6B31745BBB7E10B5335 (void);
// 0x00000056 System.Void Firebase.Platform.FirebaseHandler/ApplicationFocusChangedEventArgs::set_HasFocus(System.Boolean)
extern void ApplicationFocusChangedEventArgs_set_HasFocus_mDB3B76E13D221D0A0838877B9EE94299135B4CA1 (void);
// 0x00000057 System.Void Firebase.Platform.FirebaseHandler/ApplicationFocusChangedEventArgs::.ctor()
extern void ApplicationFocusChangedEventArgs__ctor_m2B84BC26D7A933044EAA1FBD78A65CB767E1C55C (void);
// 0x00000058 System.Void Firebase.Platform.FirebaseHandler/<>c::.cctor()
extern void U3CU3Ec__cctor_m00C5A757BDD5F4FEF8F9279F4A1152449222EE61 (void);
// 0x00000059 System.Void Firebase.Platform.FirebaseHandler/<>c::.ctor()
extern void U3CU3Ec__ctor_m85629195FA2478A2E4784E8A66606324E6613A78 (void);
// 0x0000005A System.Boolean Firebase.Platform.FirebaseHandler/<>c::<StopMonoBehaviour>b__19_0()
extern void U3CU3Ec_U3CStopMonoBehaviourU3Eb__19_0_mF7FF24CAF2DBC2B8F724320506C358775DF8E7AE (void);
// 0x0000005B System.Void Firebase.Platform.FirebaseHandler/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mC8EDEEF9AF53EC004805D7A0148D08C948F08796 (void);
// 0x0000005C System.Void Firebase.Platform.FirebaseHandler/<>c__DisplayClass34_0::<CreatePartialOnMainThread>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CCreatePartialOnMainThreadU3Eb__0_m1DDB57163B50F831CB7EC76D9D56CC066D858A1D (void);
// 0x0000005D System.Boolean Firebase.Platform.PlatformInformation::get_IsAndroid()
extern void PlatformInformation_get_IsAndroid_mE35EA3552598C8AA7038D748ABE0A38C9F093796 (void);
// 0x0000005E System.Boolean Firebase.Platform.PlatformInformation::get_IsIOS()
extern void PlatformInformation_get_IsIOS_mA72376D7AAF6F90968CE142FC8020B09C2B1F873 (void);
// 0x0000005F System.String Firebase.Platform.PlatformInformation::get_DefaultConfigLocation()
extern void PlatformInformation_get_DefaultConfigLocation_m05C745B6F791D15D8C35BE44EDA4F28050D82631 (void);
// 0x00000060 System.Single Firebase.Platform.PlatformInformation::get_RealtimeSinceStartup()
extern void PlatformInformation_get_RealtimeSinceStartup_m798A15F4EC9F5F403C3870CEEA68859317A3DE91 (void);
// 0x00000061 System.Void Firebase.Platform.PlatformInformation::set_RealtimeSinceStartupSafe(System.Single)
extern void PlatformInformation_set_RealtimeSinceStartupSafe_m9224EA19C7F150EFC991C03B7483830D3394E8CD (void);
// 0x00000062 System.String Firebase.Platform.PlatformInformation::get_RuntimeName()
extern void PlatformInformation_get_RuntimeName_m63F2F68069E88740605E1059F088632507972CCB (void);
// 0x00000063 System.String Firebase.Platform.PlatformInformation::get_RuntimeVersion()
extern void PlatformInformation_get_RuntimeVersion_m8AA7D36DC7D5D8B7C35C9AE6BDC77D1B7B54A8EA (void);
// 0x00000064 System.Void Firebase.Platform.PlatformInformation/<>c::.cctor()
extern void U3CU3Ec__cctor_m2F8AF71C806C79DF85DB11AA12D3C1A8D6A03A55 (void);
// 0x00000065 System.Void Firebase.Platform.PlatformInformation/<>c::.ctor()
extern void U3CU3Ec__ctor_m100F86B3556423BD4A74464C139ECD9E6EEE7BC7 (void);
// 0x00000066 System.String Firebase.Platform.PlatformInformation/<>c::<get_DefaultConfigLocation>b__6_0()
extern void U3CU3Ec_U3Cget_DefaultConfigLocationU3Eb__6_0_m06C1966148257FDD30A2A8044489552FF4AC2366 (void);
// 0x00000067 System.String Firebase.Platform.PlatformInformation/<>c::<get_RuntimeVersion>b__18_0()
extern void U3CU3Ec_U3Cget_RuntimeVersionU3Eb__18_0_mBEBDF5B525CDCDB0120B5D7BECE6720DBB573738 (void);
// 0x00000068 System.Boolean Firebase.Platform.FirebaseLogger::IsStackTraceLogTypeIncompatibleWithNativeLogs(UnityEngine.StackTraceLogType)
extern void FirebaseLogger_IsStackTraceLogTypeIncompatibleWithNativeLogs_m25E0C5DF3E4D66A6CF62BB1DD07CACF19B1D5D03 (void);
// 0x00000069 System.Boolean Firebase.Platform.FirebaseLogger::CurrentStackTraceLogTypeIsIncompatibleWithNativeLogs()
extern void FirebaseLogger_CurrentStackTraceLogTypeIsIncompatibleWithNativeLogs_m85FC9ADE57166E78AB0D70B761569C98C089E2B9 (void);
// 0x0000006A System.Boolean Firebase.Platform.FirebaseLogger::get_CanRedirectNativeLogs()
extern void FirebaseLogger_get_CanRedirectNativeLogs_mC1C3FD7D58663DA799277F6B542FBCCFFB1199D1 (void);
// 0x0000006B System.Void Firebase.Platform.FirebaseLogger::LogMessage(Firebase.Platform.PlatformLogLevel,System.String)
extern void FirebaseLogger_LogMessage_m166B6D71F340942254E68AE169FDABFFE78398A2 (void);
// 0x0000006C System.Void Firebase.Platform.FirebaseLogger::.cctor()
extern void FirebaseLogger__cctor_mA175E9EB45F98E057F71EF310A4F84D427D5A02E (void);
// 0x0000006D Firebase.Platform.FirebaseHandler Firebase.Platform.FirebaseMonoBehaviour::GetFirebaseHandlerOrDestroyGameObject()
extern void FirebaseMonoBehaviour_GetFirebaseHandlerOrDestroyGameObject_mE01D6AF26DA880E3BF6D8DAF231C270F0C9C002F (void);
// 0x0000006E System.Void Firebase.Platform.FirebaseMonoBehaviour::OnEnable()
extern void FirebaseMonoBehaviour_OnEnable_m18519EA6271E80120F01D31B3EE592127B48B72C (void);
// 0x0000006F System.Void Firebase.Platform.FirebaseMonoBehaviour::Update()
extern void FirebaseMonoBehaviour_Update_m8338C29185F7348D4FC338194CBD3F285645A773 (void);
// 0x00000070 System.Void Firebase.Platform.FirebaseMonoBehaviour::OnApplicationFocus(System.Boolean)
extern void FirebaseMonoBehaviour_OnApplicationFocus_mC5EA1995ED7D3CB5F83902CFA519EC507E05EA8D (void);
// 0x00000071 System.Void Firebase.Platform.FirebaseMonoBehaviour::OnDestroy()
extern void FirebaseMonoBehaviour_OnDestroy_m3FD94734285C9254480EF29135FA354D0E2292B8 (void);
// 0x00000072 System.Void Firebase.Platform.FirebaseMonoBehaviour::.ctor()
extern void FirebaseMonoBehaviour__ctor_m17C407059420FFEA2371846AD4279B80AF15B62E (void);
// 0x00000073 System.Type Firebase.Platform.FirebaseEditorDispatcher::get_EditorApplicationType()
extern void FirebaseEditorDispatcher_get_EditorApplicationType_m29D9E404DE4ABFFC8998479ED4D4CC848068DF28 (void);
// 0x00000074 System.Boolean Firebase.Platform.FirebaseEditorDispatcher::get_EditorIsPlaying()
extern void FirebaseEditorDispatcher_get_EditorIsPlaying_mCE1E655847C039146E1495A8312D631E3D03FD48 (void);
// 0x00000075 System.Boolean Firebase.Platform.FirebaseEditorDispatcher::get_EditorIsPlayingOrWillChangePlaymode()
extern void FirebaseEditorDispatcher_get_EditorIsPlayingOrWillChangePlaymode_m9AE6D33BD130094853BA242E6ACD2029DAF1059A (void);
// 0x00000076 System.Void Firebase.Platform.FirebaseEditorDispatcher::StartEditorUpdate()
extern void FirebaseEditorDispatcher_StartEditorUpdate_m2F0D910C96FE39C373D1165E711062D654BB5959 (void);
// 0x00000077 System.Void Firebase.Platform.FirebaseEditorDispatcher::StopEditorUpdate()
extern void FirebaseEditorDispatcher_StopEditorUpdate_m35C5C47A39B1C9DD53364233A2AB434E70CC684D (void);
// 0x00000078 System.Void Firebase.Platform.FirebaseEditorDispatcher::Update()
extern void FirebaseEditorDispatcher_Update_m751CFAC95750018DD2D2C243C126B8637BD37FCB (void);
// 0x00000079 System.Void Firebase.Platform.FirebaseEditorDispatcher::ListenToPlayState(System.Boolean)
extern void FirebaseEditorDispatcher_ListenToPlayState_mF2B81D98157993EDA8DE53E08E1B96F91442FDDF (void);
// 0x0000007A System.Void Firebase.Platform.FirebaseEditorDispatcher::PlayModeStateChanged()
extern void FirebaseEditorDispatcher_PlayModeStateChanged_mCFC2D47C0C350AC436317C110CFADEB8DC1017BA (void);
// 0x0000007B System.Void Firebase.Platform.FirebaseEditorDispatcher::PlayModeStateChangedWithArg(T)
// 0x0000007C System.Void Firebase.Platform.FirebaseEditorDispatcher::AddRemoveCallbackToField(System.Reflection.FieldInfo,System.Action,System.Object,System.Boolean,System.String)
extern void FirebaseEditorDispatcher_AddRemoveCallbackToField_m153B2DEA97E57B991D8BF2DF3F531A41B0D45102 (void);
// 0x0000007D Firebase.Platform.IAppConfigExtensions Firebase.Platform.Default.AppConfigExtensions::get_Instance()
extern void AppConfigExtensions_get_Instance_mCF62AA28EED4C0E620C57F9319D6A6A7DC197E29 (void);
// 0x0000007E System.Void Firebase.Platform.Default.AppConfigExtensions::.ctor()
extern void AppConfigExtensions__ctor_m04A8807DCFD232502FB1E0C74B4E29ABF16F091C (void);
// 0x0000007F System.Void Firebase.Platform.Default.AppConfigExtensions::.cctor()
extern void AppConfigExtensions__cctor_m50CD337289B34B25FB769832D7EA1B4471083A5B (void);
// 0x00000080 System.Void Firebase.Platform.Default.SystemClock::.ctor()
extern void SystemClock__ctor_m99B061010072193DAF3F270722EEA129C960068A (void);
// 0x00000081 System.Void Firebase.Platform.Default.SystemClock::.cctor()
extern void SystemClock__cctor_m7394B65D9E2061279D261BC051CD2452678DD141 (void);
// 0x00000082 Firebase.Platform.IAppConfigExtensions Firebase.Platform.Default.UnityConfigExtensions::get_DefaultInstance()
extern void UnityConfigExtensions_get_DefaultInstance_m170335DABF384B72294A3803C11931CB27D99204 (void);
// 0x00000083 System.Void Firebase.Platform.Default.UnityConfigExtensions::.ctor()
extern void UnityConfigExtensions__ctor_m1068123E08F326815ADFDB2A0104A7D83AD36B2C (void);
// 0x00000084 System.Void Firebase.Platform.Default.UnityConfigExtensions::.cctor()
extern void UnityConfigExtensions__cctor_m869A1A1B871B1D3E1790539CCF9DBBA7E2C73621 (void);
static Il2CppMethodPointer s_methodPointers[132] = 
{
	ExceptionAggregator_get_Exceptions_m7151F9FF0E2168208A9E2981FB3DAB015DAF1E7B,
	ExceptionAggregator_GetAndClearPendingExceptions_m448ABC9A2AE8264581AA8C324799D3E826D0BC47,
	ExceptionAggregator_ThrowAndClearPendingExceptions_m0EB19F8E705293D9978B62B63EDFBC7ED4A2403E,
	ExceptionAggregator_LogException_m86E9668A4AF70331361FDAA86F83A90125FE2B18,
	ExceptionAggregator_Wrap_m286D2940E6E4C43E3C9B5E23A249A63B731390B4,
	ExceptionAggregator__cctor_m2C0E0D699411728432F1AA96407CFFCABFB90EF9,
	Dispatcher__ctor_mE99B93199AD38F0A53946F925713CC84099C35B3,
	NULL,
	NULL,
	NULL,
	Dispatcher_ManagesThisThread_mDD4799366D040E2A27D9501C6AE8D2C88AAE086A,
	Dispatcher_PollJobs_m4DD47E8B63F33D975BC1A7D670A509F597F8E9E0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UnityLoggingService_get_Instance_mBE89DCE5CD59076E6242362A75CDA3C47390D704,
	UnityLoggingService__ctor_m40EA0D43E2C571DD040E6239AFFA67D4A16A258D,
	UnityLoggingService__cctor_m50B3E83067D7E2E1B89E3A81B46DEF7E2C38B00E,
	UnityPlatformServices_SetupServices_mAE2AAD154596FA336453CB1823F53845F6A8DB44,
	UnitySynchronizationContext__ctor_m16FA79368D74F4CCBF96D397B8C334CCC8A40720,
	UnitySynchronizationContext_Create_m3835F4FC47D7841007E8AA336FB350F1B1A08C8A,
	UnitySynchronizationContext_Destroy_m356D29922F6C827A245669A9D4179F73E52BABB4,
	UnitySynchronizationContext_GetThreadEvent_mC98D54E3250279ACF4A26392835855D729D729F0,
	UnitySynchronizationContext_Post_mFFC9993A213F2DE9D2AB3003272BD0D62486C6E6,
	UnitySynchronizationContext_Send_m0BDC4069F41293598BA84281FCBBA4C46DAC00A4,
	UnitySynchronizationContext__cctor_mC98553E4C6F16C7BD01A85C244D976CBEE91C690,
	SynchronizationContextBehavoir_get_CallbackQueue_m0E34CE99C7CA3A50D0B5960110C97B35F1464181,
	SynchronizationContextBehavoir_Start_mC0020EEF1D711C56559978F6B05D5980607B1415,
	SynchronizationContextBehavoir__ctor_mB7DAD13667448BFE9813A53C0AEEA01D2ABB23E7,
	U3CStartU3Ed__3__ctor_mE286810BB74A14A6CA89DBD541556E73EE6F1873,
	U3CStartU3Ed__3_System_IDisposable_Dispose_mA3F0338173978F9E1F1C95BCD8A5C65C7DA5464B,
	U3CStartU3Ed__3_MoveNext_mF7311EB49D00661ED24EBB2AD6A43FC8FB69451E,
	U3CStartU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB792B39126660A53266DF372A408359EFCE5CCA,
	U3CStartU3Ed__3_System_Collections_IEnumerator_Reset_m0700DB59F5C1825F093EE05044976C7C512A8026,
	U3CStartU3Ed__3_System_Collections_IEnumerator_get_Current_mDE0E18D92FEA57AF47CB64A273477B674A114DFD,
	U3CU3Ec__DisplayClass14_0__ctor_mBF75F76D3921A9186C2576BDC0158F2748855D64,
	U3CU3Ec__DisplayClass14_1__ctor_m7A98D58E7972709AD2351CD86F0C8EF10E75A5D1,
	U3CU3Ec__DisplayClass16_0__ctor_m864C9D4FA34AF22ECD795F3344D81BEDBA8BAE66,
	U3CU3Ec__DisplayClass16_1__ctor_mB967C90C84AC0EC6AE041D0ECEF6D1AB66069C80,
	U3CU3Ec__DisplayClass16_1_U3CSendU3Eb__0_mAC9FDB5AD8982ABA072566F9C51D439E1E01674B,
	DebugLogger_get_Instance_m4D1D9AE0E96B51CD737215A91B33D02358D8887C,
	DebugLogger__ctor_mD6357FC79350285A4D8372BC692899939F6D36C4,
	DebugLogger__cctor_mBFBBB23B99D8FD84196BA0C728EF0C59FE96FFDA,
	Services__cctor_m417FD7FA552048871A01D0082B4A0BC771A6C55A,
	Services_set_AppConfig_mA505D0464F293497554B104DF818E7E44F26FBBA,
	Services_set_Clock_mA7A638A08B57FCF25A5A314C31ED6DA07FE990DE,
	Services_set_Logging_mDAF2637B76938F809228599C7A73ADB13DC4B477,
	NULL,
	NULL,
	NULL,
	FirebaseAppUtilsStub_get_Instance_mCA9D44EBABA8387059AB3995A06E769148CED85C,
	FirebaseAppUtilsStub_TranslateDllNotFoundException_m05B83DDC2DB8B21CEFC55D0AA990CA16A9390C8E,
	FirebaseAppUtilsStub_PollCallbacks_m13A423E04EC1F3EDFD0D7FE63F18E0D3036346E1,
	FirebaseAppUtilsStub_GetLogLevel_m6082A2182D62F889C842C09F4B5A15074B8F4127,
	FirebaseAppUtilsStub__ctor_m3EC6F08162223655B3A0A2FA44BA7060AE3884AD,
	FirebaseAppUtilsStub__cctor_mD5A91BFC2B1478924F9C6E908CADAA1A10E14D28,
	NULL,
	NULL,
	NULL,
	FirebaseHandler_get_AppUtils_m83D618A43DEDAF4C820368D4B4BB18B517E9529D,
	FirebaseHandler_set_AppUtils_mEE811D9E5C40519DFB1C9D4E43AF98B0F301DCDF,
	FirebaseHandler_get_TickCount_mA6B835A8EDF1DD8A818B74100923D18BB19DF1AC,
	FirebaseHandler_get_ThreadDispatcher_m0704B04915111466D31C391AAF23D593AF6914C3,
	FirebaseHandler_set_ThreadDispatcher_m425931A1B890B0FDE72E207295145220F8F20BC7,
	FirebaseHandler_get_IsPlayMode_m59AFE9F48F2EA1AADCFBD94DBE010730359B3B40,
	FirebaseHandler_set_IsPlayMode_m832E6FCD6DA3ACDEA0B59D346EDD8585381C3B15,
	FirebaseHandler__cctor_m0D9B254E4259799D9BAFADFD71AD0E2EA35FD74E,
	FirebaseHandler__ctor_mD79B2E4B647013911BE3A8602014EB108DD1D054,
	FirebaseHandler_StartMonoBehaviour_m3258056C31C8D32C690329363587720C60B35E0A,
	FirebaseHandler_StopMonoBehaviour_m1CB49D4E683D48586AEF5F8B515EAC41F2F502BE,
	NULL,
	NULL,
	FirebaseHandler_get_DefaultInstance_m4D523A41A2FFB235AAEA0C61DD0E13A759478CC7,
	FirebaseHandler_CreatePartialOnMainThread_mAE4C7FDD94E3CE3333D6209E4C455F2C8C37EEEC,
	FirebaseHandler_Create_m993EB16D9DDC68CC41514B48FD319AE7E7ECB434,
	FirebaseHandler_Update_m054C867D529EEC80853232EEAFBCAD18CC287817,
	FirebaseHandler_OnApplicationFocus_mC750E0727206EA64FC16F8448D149EBF82C59F41,
	FirebaseHandler_OnMonoBehaviourDestroyed_m608B6F7487509B972415DA4566AF8A1EDCB73A82,
	FirebaseHandler_U3CUpdateU3Eb__36_0_mDC0BB815941FD6EF6E26F6B31745BBB7E10B5335,
	ApplicationFocusChangedEventArgs_set_HasFocus_mDB3B76E13D221D0A0838877B9EE94299135B4CA1,
	ApplicationFocusChangedEventArgs__ctor_m2B84BC26D7A933044EAA1FBD78A65CB767E1C55C,
	U3CU3Ec__cctor_m00C5A757BDD5F4FEF8F9279F4A1152449222EE61,
	U3CU3Ec__ctor_m85629195FA2478A2E4784E8A66606324E6613A78,
	U3CU3Ec_U3CStopMonoBehaviourU3Eb__19_0_mF7FF24CAF2DBC2B8F724320506C358775DF8E7AE,
	U3CU3Ec__DisplayClass34_0__ctor_mC8EDEEF9AF53EC004805D7A0148D08C948F08796,
	U3CU3Ec__DisplayClass34_0_U3CCreatePartialOnMainThreadU3Eb__0_m1DDB57163B50F831CB7EC76D9D56CC066D858A1D,
	PlatformInformation_get_IsAndroid_mE35EA3552598C8AA7038D748ABE0A38C9F093796,
	PlatformInformation_get_IsIOS_mA72376D7AAF6F90968CE142FC8020B09C2B1F873,
	PlatformInformation_get_DefaultConfigLocation_m05C745B6F791D15D8C35BE44EDA4F28050D82631,
	PlatformInformation_get_RealtimeSinceStartup_m798A15F4EC9F5F403C3870CEEA68859317A3DE91,
	PlatformInformation_set_RealtimeSinceStartupSafe_m9224EA19C7F150EFC991C03B7483830D3394E8CD,
	PlatformInformation_get_RuntimeName_m63F2F68069E88740605E1059F088632507972CCB,
	PlatformInformation_get_RuntimeVersion_m8AA7D36DC7D5D8B7C35C9AE6BDC77D1B7B54A8EA,
	U3CU3Ec__cctor_m2F8AF71C806C79DF85DB11AA12D3C1A8D6A03A55,
	U3CU3Ec__ctor_m100F86B3556423BD4A74464C139ECD9E6EEE7BC7,
	U3CU3Ec_U3Cget_DefaultConfigLocationU3Eb__6_0_m06C1966148257FDD30A2A8044489552FF4AC2366,
	U3CU3Ec_U3Cget_RuntimeVersionU3Eb__18_0_mBEBDF5B525CDCDB0120B5D7BECE6720DBB573738,
	FirebaseLogger_IsStackTraceLogTypeIncompatibleWithNativeLogs_m25E0C5DF3E4D66A6CF62BB1DD07CACF19B1D5D03,
	FirebaseLogger_CurrentStackTraceLogTypeIsIncompatibleWithNativeLogs_m85FC9ADE57166E78AB0D70B761569C98C089E2B9,
	FirebaseLogger_get_CanRedirectNativeLogs_mC1C3FD7D58663DA799277F6B542FBCCFFB1199D1,
	FirebaseLogger_LogMessage_m166B6D71F340942254E68AE169FDABFFE78398A2,
	FirebaseLogger__cctor_mA175E9EB45F98E057F71EF310A4F84D427D5A02E,
	FirebaseMonoBehaviour_GetFirebaseHandlerOrDestroyGameObject_mE01D6AF26DA880E3BF6D8DAF231C270F0C9C002F,
	FirebaseMonoBehaviour_OnEnable_m18519EA6271E80120F01D31B3EE592127B48B72C,
	FirebaseMonoBehaviour_Update_m8338C29185F7348D4FC338194CBD3F285645A773,
	FirebaseMonoBehaviour_OnApplicationFocus_mC5EA1995ED7D3CB5F83902CFA519EC507E05EA8D,
	FirebaseMonoBehaviour_OnDestroy_m3FD94734285C9254480EF29135FA354D0E2292B8,
	FirebaseMonoBehaviour__ctor_m17C407059420FFEA2371846AD4279B80AF15B62E,
	FirebaseEditorDispatcher_get_EditorApplicationType_m29D9E404DE4ABFFC8998479ED4D4CC848068DF28,
	FirebaseEditorDispatcher_get_EditorIsPlaying_mCE1E655847C039146E1495A8312D631E3D03FD48,
	FirebaseEditorDispatcher_get_EditorIsPlayingOrWillChangePlaymode_m9AE6D33BD130094853BA242E6ACD2029DAF1059A,
	FirebaseEditorDispatcher_StartEditorUpdate_m2F0D910C96FE39C373D1165E711062D654BB5959,
	FirebaseEditorDispatcher_StopEditorUpdate_m35C5C47A39B1C9DD53364233A2AB434E70CC684D,
	FirebaseEditorDispatcher_Update_m751CFAC95750018DD2D2C243C126B8637BD37FCB,
	FirebaseEditorDispatcher_ListenToPlayState_mF2B81D98157993EDA8DE53E08E1B96F91442FDDF,
	FirebaseEditorDispatcher_PlayModeStateChanged_mCFC2D47C0C350AC436317C110CFADEB8DC1017BA,
	NULL,
	FirebaseEditorDispatcher_AddRemoveCallbackToField_m153B2DEA97E57B991D8BF2DF3F531A41B0D45102,
	AppConfigExtensions_get_Instance_mCF62AA28EED4C0E620C57F9319D6A6A7DC197E29,
	AppConfigExtensions__ctor_m04A8807DCFD232502FB1E0C74B4E29ABF16F091C,
	AppConfigExtensions__cctor_m50CD337289B34B25FB769832D7EA1B4471083A5B,
	SystemClock__ctor_m99B061010072193DAF3F270722EEA129C960068A,
	SystemClock__cctor_m7394B65D9E2061279D261BC051CD2452678DD141,
	UnityConfigExtensions_get_DefaultInstance_m170335DABF384B72294A3803C11931CB27D99204,
	UnityConfigExtensions__ctor_m1068123E08F326815ADFDB2A0104A7D83AD36B2C,
	UnityConfigExtensions__cctor_m869A1A1B871B1D3E1790539CCF9DBBA7E2C73621,
};
static const int32_t s_InvokerIndices[132] = 
{
	5377,
	5377,
	5396,
	5160,
	5298,
	5396,
	3595,
	-1,
	-1,
	-1,
	3478,
	3595,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5377,
	3595,
	5396,
	5396,
	2856,
	5298,
	5396,
	3533,
	1582,
	1582,
	5396,
	3533,
	3533,
	3595,
	2842,
	3595,
	3478,
	3533,
	3595,
	3533,
	3595,
	3595,
	3595,
	3595,
	2856,
	5377,
	3595,
	5396,
	5396,
	5298,
	5298,
	5298,
	2856,
	3595,
	3518,
	5377,
	2856,
	3595,
	3518,
	3595,
	5396,
	-1,
	-1,
	-1,
	5377,
	5298,
	5371,
	5377,
	5298,
	3478,
	2797,
	5396,
	3595,
	3595,
	3595,
	-1,
	-1,
	5377,
	5298,
	5298,
	3595,
	2797,
	5298,
	3595,
	2797,
	3595,
	5396,
	3595,
	3478,
	3595,
	3595,
	5363,
	5363,
	5377,
	5386,
	5302,
	5377,
	5377,
	5396,
	3595,
	3533,
	3533,
	4966,
	5363,
	5363,
	4847,
	5396,
	3533,
	3595,
	3595,
	2797,
	3595,
	3595,
	5377,
	5363,
	5363,
	5396,
	5396,
	5396,
	5290,
	5396,
	-1,
	3957,
	5377,
	3595,
	5396,
	3595,
	5396,
	5377,
	3595,
	5396,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x02000005, { 21, 3 } },
	{ 0x02000006, { 24, 3 } },
	{ 0x02000019, { 27, 6 } },
	{ 0x06000008, { 0, 8 } },
	{ 0x06000009, { 8, 7 } },
	{ 0x0600000A, { 15, 6 } },
	{ 0x0600004D, { 33, 2 } },
	{ 0x0600004E, { 35, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[37] = 
{
	{ (Il2CppRGCTXDataType)2, 1326 },
	{ (Il2CppRGCTXDataType)3, 200 },
	{ (Il2CppRGCTXDataType)3, 10571 },
	{ (Il2CppRGCTXDataType)2, 1679 },
	{ (Il2CppRGCTXDataType)3, 2350 },
	{ (Il2CppRGCTXDataType)3, 201 },
	{ (Il2CppRGCTXDataType)3, 2351 },
	{ (Il2CppRGCTXDataType)3, 2352 },
	{ (Il2CppRGCTXDataType)2, 1332 },
	{ (Il2CppRGCTXDataType)3, 224 },
	{ (Il2CppRGCTXDataType)3, 26257 },
	{ (Il2CppRGCTXDataType)2, 5013 },
	{ (Il2CppRGCTXDataType)3, 20875 },
	{ (Il2CppRGCTXDataType)3, 225 },
	{ (Il2CppRGCTXDataType)3, 20876 },
	{ (Il2CppRGCTXDataType)3, 10570 },
	{ (Il2CppRGCTXDataType)3, 27370 },
	{ (Il2CppRGCTXDataType)2, 5014 },
	{ (Il2CppRGCTXDataType)3, 20877 },
	{ (Il2CppRGCTXDataType)3, 20878 },
	{ (Il2CppRGCTXDataType)3, 20879 },
	{ (Il2CppRGCTXDataType)3, 10583 },
	{ (Il2CppRGCTXDataType)3, 2354 },
	{ (Il2CppRGCTXDataType)3, 2353 },
	{ (Il2CppRGCTXDataType)3, 10584 },
	{ (Il2CppRGCTXDataType)3, 20886 },
	{ (Il2CppRGCTXDataType)3, 20885 },
	{ (Il2CppRGCTXDataType)3, 18305 },
	{ (Il2CppRGCTXDataType)2, 2405 },
	{ (Il2CppRGCTXDataType)3, 10580 },
	{ (Il2CppRGCTXDataType)3, 26754 },
	{ (Il2CppRGCTXDataType)3, 21030 },
	{ (Il2CppRGCTXDataType)3, 10581 },
	{ (Il2CppRGCTXDataType)3, 26251 },
	{ (Il2CppRGCTXDataType)3, 10574 },
	{ (Il2CppRGCTXDataType)3, 26254 },
	{ (Il2CppRGCTXDataType)3, 26258 },
};
extern const CustomAttributesCacheGenerator g_Firebase_Platform_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Firebase_Platform_CodeGenModule;
const Il2CppCodeGenModule g_Firebase_Platform_CodeGenModule = 
{
	"Firebase.Platform.dll",
	132,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	37,
	s_rgctxValues,
	NULL,
	g_Firebase_Platform_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

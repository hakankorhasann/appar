﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CategoryCilckable::Start()
extern void CategoryCilckable_Start_m44CB2BDCBF4F83DE7F3C2CAE1A7EA145247F9E65 (void);
// 0x00000002 System.Void CategoryCilckable::RotationAdjust()
extern void CategoryCilckable_RotationAdjust_m082796DF06BEBC8B69EBC1B69E7C61214354F78F (void);
// 0x00000003 System.Void CategoryCilckable::BackButtonForVerticalScroll()
extern void CategoryCilckable_BackButtonForVerticalScroll_m35B1CD281E89E573312B090D04871D5A6EFDB05E (void);
// 0x00000004 System.Void CategoryCilckable::BackButtonClicked()
extern void CategoryCilckable_BackButtonClicked_m7AEDBC53C789211B102156BD0EED2EDAF879A972 (void);
// 0x00000005 System.Void CategoryCilckable::PanelsFalse()
extern void CategoryCilckable_PanelsFalse_mA1432BEE745FF8EA38FA2E8021753B2F66DDDDB7 (void);
// 0x00000006 System.Void CategoryCilckable::PanelsTrue()
extern void CategoryCilckable_PanelsTrue_m7D63789E4047FE5DBB934AB836F88DC1875D99C8 (void);
// 0x00000007 System.Void CategoryCilckable::Update()
extern void CategoryCilckable_Update_m61A7EE66B89983D51CD1CF881E420039872A1AA0 (void);
// 0x00000008 System.Void CategoryCilckable::.ctor()
extern void CategoryCilckable__ctor_m9FBD09C62AEECFCF1B4EC48714FC2DCE89D133CF (void);
// 0x00000009 System.Void BtnPrefabScript::buttonClicked()
extern void BtnPrefabScript_buttonClicked_m33B8C1771B33709BDB4E38DC59A9E845735BD322 (void);
// 0x0000000A System.Void BtnPrefabScript::OpenVerticalScrollView()
extern void BtnPrefabScript_OpenVerticalScrollView_m881D4A78C434485D963721F9ABF564A7D02BD34C (void);
// 0x0000000B System.Void BtnPrefabScript::DownloadVerticalButtonsPhotos()
extern void BtnPrefabScript_DownloadVerticalButtonsPhotos_m34DD81EC38964DB7C5516FB8D1688CBF975723C1 (void);
// 0x0000000C System.Void BtnPrefabScript::.ctor()
extern void BtnPrefabScript__ctor_mB3709B7B608DE119D2AB6AF9B255810C5A5DD9CF (void);
// 0x0000000D System.Void BtnPrefabScript::<DownloadVerticalButtonsPhotos>b__12_0(System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot>)
extern void BtnPrefabScript_U3CDownloadVerticalButtonsPhotosU3Eb__12_0_mC76C117FC1139565C8B47F663D4B1FC4B7445A9C (void);
// 0x0000000E System.Void BtnPrefabScript/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mC445B829227E4ECDDFAC5D926EB75081778B8A09 (void);
// 0x0000000F System.Void BtnPrefabScript/<>c__DisplayClass12_0::<DownloadVerticalButtonsPhotos>b__1(System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot>)
extern void U3CU3Ec__DisplayClass12_0_U3CDownloadVerticalButtonsPhotosU3Eb__1_mC03463554EFC0E6989FFC0E55EB34CD75AE2547E (void);
// 0x00000010 System.Void ButtonCreator::Start()
extern void ButtonCreator_Start_m1E8C4DCCC795FDFC486E4FD0D0F8C243729B78A2 (void);
// 0x00000011 System.Void ButtonCreator::buttonCreate(System.String[],System.String[])
extern void ButtonCreator_buttonCreate_mBC0219603823035DE041C60F23BAD700C4CBA007 (void);
// 0x00000012 System.Void ButtonCreator::CreateButton(System.String,System.String)
extern void ButtonCreator_CreateButton_m78AAAB644F3181E47BB1F16280FA98EC82FAF59B (void);
// 0x00000013 System.Void ButtonCreator::.ctor()
extern void ButtonCreator__ctor_m4640AC6AD151973D88347D4AFE311ADD1123F8DA (void);
// 0x00000014 System.Void Create3DModel::Start()
extern void Create3DModel_Start_mA9E38DAB07B9CE9A08BFE2420E1C528B9C91C2E4 (void);
// 0x00000015 System.Void Create3DModel::Update()
extern void Create3DModel_Update_m8B4027FBBEB6C8EA0001C527C62338905ACE037B (void);
// 0x00000016 System.Void Create3DModel::touchAndArrange(UnityEngine.GameObject)
extern void Create3DModel_touchAndArrange_m2451124D3DDC33C531D2D97BC67286C6589027B0 (void);
// 0x00000017 System.Void Create3DModel::btnClicked()
extern void Create3DModel_btnClicked_m6EB4C43971849300179AAB682C8975D7577BEA3D (void);
// 0x00000018 System.Void Create3DModel::Download3dModels()
extern void Create3DModel_Download3dModels_mDC674B76828E9981066E7629FE8AFC5F0B472961 (void);
// 0x00000019 System.Collections.IEnumerator Create3DModel::DownloadAssetBundle(System.String)
extern void Create3DModel_DownloadAssetBundle_mCA1A34D5EE51EC4AC314555016F106BCA535C08E (void);
// 0x0000001A System.Void Create3DModel::.ctor()
extern void Create3DModel__ctor_m51186D9603B7A0C2D8C75A10CB77F125AF3F724D (void);
// 0x0000001B System.Void Create3DModel/<DownloadAssetBundle>d__11::.ctor(System.Int32)
extern void U3CDownloadAssetBundleU3Ed__11__ctor_mD5B96E18A4598751D9D323F9DE32E1256E85CD52 (void);
// 0x0000001C System.Void Create3DModel/<DownloadAssetBundle>d__11::System.IDisposable.Dispose()
extern void U3CDownloadAssetBundleU3Ed__11_System_IDisposable_Dispose_m3247B77C318D87D59E280129CC1EBDE8DC812930 (void);
// 0x0000001D System.Boolean Create3DModel/<DownloadAssetBundle>d__11::MoveNext()
extern void U3CDownloadAssetBundleU3Ed__11_MoveNext_m94DFD564DB5788E48D90EC15DAE59A7BF5BEF3B9 (void);
// 0x0000001E System.Void Create3DModel/<DownloadAssetBundle>d__11::<>m__Finally1()
extern void U3CDownloadAssetBundleU3Ed__11_U3CU3Em__Finally1_mA3E9186EAE5B265A05524EC406C5655171D72A4F (void);
// 0x0000001F System.Object Create3DModel/<DownloadAssetBundle>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadAssetBundleU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9989BD403603E7630A3FC51367E31B1E2117DBBD (void);
// 0x00000020 System.Void Create3DModel/<DownloadAssetBundle>d__11::System.Collections.IEnumerator.Reset()
extern void U3CDownloadAssetBundleU3Ed__11_System_Collections_IEnumerator_Reset_mE49F30B824501D89E1C8CA52A2127FA44CEF97BA (void);
// 0x00000021 System.Object Create3DModel/<DownloadAssetBundle>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadAssetBundleU3Ed__11_System_Collections_IEnumerator_get_Current_m280482882ED846B0B6EF97FE3E980C350FE1055A (void);
// 0x00000022 System.Void VerticalButtonCreator::Start()
extern void VerticalButtonCreator_Start_m638E627422D5DE771FC41950F84ECEEBF712E122 (void);
// 0x00000023 System.Void VerticalButtonCreator::buttonCreate(System.String[],System.String[])
extern void VerticalButtonCreator_buttonCreate_mA13042D0673B3B9B438BCBA10CD7CAD68307E0E7 (void);
// 0x00000024 System.Void VerticalButtonCreator::CreateButton(System.String,System.String)
extern void VerticalButtonCreator_CreateButton_m2DFC2865177D2AB5C50AEB9C541FFADA68E96F26 (void);
// 0x00000025 System.Void VerticalButtonCreator::.ctor()
extern void VerticalButtonCreator__ctor_m2A1310B531247B193017B695FC367ECFDC035240 (void);
// 0x00000026 System.Collections.IEnumerator DownloadAssetBundleExtension::DownloadAssetBundle(System.String,UnityEngine.Transform)
extern void DownloadAssetBundleExtension_DownloadAssetBundle_m402969E102F8C322D4BFBB7282CC99E1C04A14EC (void);
// 0x00000027 System.Void DownloadAssetBundleExtension::Instantiate(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void DownloadAssetBundleExtension_Instantiate_m1C344BAC9321BDA69E4BC9FFD546E5F98175385A (void);
// 0x00000028 System.Void DownloadAssetBundleExtension/<DownloadAssetBundle>d__0::.ctor(System.Int32)
extern void U3CDownloadAssetBundleU3Ed__0__ctor_m7B2BF0DF356348556FC7042A3AE4A685755D5DE0 (void);
// 0x00000029 System.Void DownloadAssetBundleExtension/<DownloadAssetBundle>d__0::System.IDisposable.Dispose()
extern void U3CDownloadAssetBundleU3Ed__0_System_IDisposable_Dispose_m7AFBF52E380519A03B91A866EE543235CCE30139 (void);
// 0x0000002A System.Boolean DownloadAssetBundleExtension/<DownloadAssetBundle>d__0::MoveNext()
extern void U3CDownloadAssetBundleU3Ed__0_MoveNext_m67D6E8E978187E1402416F98365BBE72F6A68E4B (void);
// 0x0000002B System.Void DownloadAssetBundleExtension/<DownloadAssetBundle>d__0::<>m__Finally1()
extern void U3CDownloadAssetBundleU3Ed__0_U3CU3Em__Finally1_m35453CEAEC2DBBFF1F3547C4E7BCDDE9F43B38E0 (void);
// 0x0000002C System.Object DownloadAssetBundleExtension/<DownloadAssetBundle>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadAssetBundleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6BA65CE24E036AEB241CC688912C5C0BBF77F321 (void);
// 0x0000002D System.Void DownloadAssetBundleExtension/<DownloadAssetBundle>d__0::System.Collections.IEnumerator.Reset()
extern void U3CDownloadAssetBundleU3Ed__0_System_Collections_IEnumerator_Reset_m95EEC0E276163815A0621D083A5245BA80D0E806 (void);
// 0x0000002E System.Object DownloadAssetBundleExtension/<DownloadAssetBundle>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadAssetBundleU3Ed__0_System_Collections_IEnumerator_get_Current_m2376DACBEBE824FDE4BC450E4AB22942DB8AAE7A (void);
// 0x0000002F System.Collections.IEnumerator DownloadImageExtensions::DownloadImage(System.String,UnityEngine.UI.Image)
extern void DownloadImageExtensions_DownloadImage_m425EFA7E5A50937CE8EEE15F9D0067CF80BFE8A6 (void);
// 0x00000030 System.Void DownloadImageExtensions/<DownloadImage>d__0::.ctor(System.Int32)
extern void U3CDownloadImageU3Ed__0__ctor_m24345C0A4F0AF3E8FE9AE84E784DBB6034C9E009 (void);
// 0x00000031 System.Void DownloadImageExtensions/<DownloadImage>d__0::System.IDisposable.Dispose()
extern void U3CDownloadImageU3Ed__0_System_IDisposable_Dispose_m565B46EF4ABF21CEF4DC0689E0FC9FA6BB94F7FF (void);
// 0x00000032 System.Boolean DownloadImageExtensions/<DownloadImage>d__0::MoveNext()
extern void U3CDownloadImageU3Ed__0_MoveNext_m27CA7C0071033899F713E97DA022264D098E62D5 (void);
// 0x00000033 System.Void DownloadImageExtensions/<DownloadImage>d__0::<>m__Finally1()
extern void U3CDownloadImageU3Ed__0_U3CU3Em__Finally1_mE924B9CD499AEDF33AE59AA08DFC31DF3B303138 (void);
// 0x00000034 System.Object DownloadImageExtensions/<DownloadImage>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadImageU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDF12CBB291BAB27BBF99AF1DC0B7EBC3F89DC9D (void);
// 0x00000035 System.Void DownloadImageExtensions/<DownloadImage>d__0::System.Collections.IEnumerator.Reset()
extern void U3CDownloadImageU3Ed__0_System_Collections_IEnumerator_Reset_mDA3A2F9B4C6CC57A732C0CE780504A4C486E0245 (void);
// 0x00000036 System.Object DownloadImageExtensions/<DownloadImage>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadImageU3Ed__0_System_Collections_IEnumerator_get_Current_mD8EAA09FDD726A953D8984E16355CA7FA8A6FB4C (void);
// 0x00000037 System.Void ObjectPlacement::Start()
extern void ObjectPlacement_Start_mB67483D63BAFD07354C67356B889FC744A70BC66 (void);
// 0x00000038 System.Void ObjectPlacement::bringObject(UnityEngine.GameObject)
extern void ObjectPlacement_bringObject_m609946B7355C09C891B433EEE4354B22AC8B6041 (void);
// 0x00000039 System.Void ObjectPlacement::Update()
extern void ObjectPlacement_Update_mB3B08845401FFE539BA1C5F02003DDD320AE7C3E (void);
// 0x0000003A System.Void ObjectPlacement::.ctor()
extern void ObjectPlacement__ctor_mBBF4544AF5B68FA0E7C132D7E1E1AFB2B80C8563 (void);
// 0x0000003B System.Void AssetBundleAugmenter::Start()
extern void AssetBundleAugmenter_Start_m2D67C6266C8037B5938D34C1DE3777E1C6A93E76 (void);
// 0x0000003C System.Collections.IEnumerator AssetBundleAugmenter::DownloadAndCache()
extern void AssetBundleAugmenter_DownloadAndCache_m827A785F9BA8DF539DDF5F123704DF6676456951 (void);
// 0x0000003D System.Void AssetBundleAugmenter::LoadAssetToTarget()
extern void AssetBundleAugmenter_LoadAssetToTarget_m0DEB6BBE336969E86CD8830FB8DC4B3EBACAD8C7 (void);
// 0x0000003E System.Void AssetBundleAugmenter::.ctor()
extern void AssetBundleAugmenter__ctor_m919795AD2BDDEF4086D6F081D459BB93F5B1007A (void);
// 0x0000003F System.Void AssetBundleAugmenter/<DownloadAndCache>d__6::.ctor(System.Int32)
extern void U3CDownloadAndCacheU3Ed__6__ctor_mC207A786AC388526B65793D9B7A3CD21637E7FE7 (void);
// 0x00000040 System.Void AssetBundleAugmenter/<DownloadAndCache>d__6::System.IDisposable.Dispose()
extern void U3CDownloadAndCacheU3Ed__6_System_IDisposable_Dispose_m1232F60243AEDD6A732706374CA4B4291EA423EC (void);
// 0x00000041 System.Boolean AssetBundleAugmenter/<DownloadAndCache>d__6::MoveNext()
extern void U3CDownloadAndCacheU3Ed__6_MoveNext_m44E3EDA27A7FC0CE2223446E673D8D054621AB70 (void);
// 0x00000042 System.Void AssetBundleAugmenter/<DownloadAndCache>d__6::<>m__Finally1()
extern void U3CDownloadAndCacheU3Ed__6_U3CU3Em__Finally1_mAB69839ABD6A749E6C0B758E7E5A8D06560531E4 (void);
// 0x00000043 System.Object AssetBundleAugmenter/<DownloadAndCache>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadAndCacheU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m45D9FC362C3EF9B098B0D929053584864B3D3801 (void);
// 0x00000044 System.Void AssetBundleAugmenter/<DownloadAndCache>d__6::System.Collections.IEnumerator.Reset()
extern void U3CDownloadAndCacheU3Ed__6_System_Collections_IEnumerator_Reset_m1C26776B3F6459BAC1EEBCB840B1CEB3A0F0B5CB (void);
// 0x00000045 System.Object AssetBundleAugmenter/<DownloadAndCache>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadAndCacheU3Ed__6_System_Collections_IEnumerator_get_Current_mD133B8114A8DF8E11DD7D96976E3AF9257A12C56 (void);
// 0x00000046 System.Void Deneme::Start()
extern void Deneme_Start_m8D2FCF29229F1A6C11285CEAE09EAE1F93022EB7 (void);
// 0x00000047 System.Void Deneme::.ctor()
extern void Deneme__ctor_m5EFE3594C4E19B41C31BFFA1E293C15D62A36C89 (void);
// 0x00000048 System.Void Deneme/<>c::.cctor()
extern void U3CU3Ec__cctor_mC94144175F6F799EFCF1B9CBE542AF194D0DC4CA (void);
// 0x00000049 System.Void Deneme/<>c::.ctor()
extern void U3CU3Ec__ctor_m07158E4768BF189B5B31B10614CDE230C787552C (void);
// 0x0000004A System.Void Deneme/<>c::<Start>b__7_0(System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot>)
extern void U3CU3Ec_U3CStartU3Eb__7_0_m1C9CB0D592AA4A21F19A3FC589451D3CE9D17F0B (void);
// 0x0000004B System.Void Deneme/<>c::<Start>b__7_1(System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot>)
extern void U3CU3Ec_U3CStartU3Eb__7_1_mA2EB8276F6C28CEEE40CA1426C08460FDD13451E (void);
// 0x0000004C System.Void MerkezeGoreBoyutlandir::Start()
extern void MerkezeGoreBoyutlandir_Start_m22BC74C1EE7037BD77C7DF8C68436C93D5F132D8 (void);
// 0x0000004D System.Void MerkezeGoreBoyutlandir::ObjeleriBoyutlandir()
extern void MerkezeGoreBoyutlandir_ObjeleriBoyutlandir_mDAEFC87082607875FA26B57F853CAA25ECE22A03 (void);
// 0x0000004E System.Void MerkezeGoreBoyutlandir::BosluklariHesapla()
extern void MerkezeGoreBoyutlandir_BosluklariHesapla_m0771885A0456B0054BD53F3F7953361E932ED67F (void);
// 0x0000004F System.Void MerkezeGoreBoyutlandir::AltObjeBoyutuHesapla()
extern void MerkezeGoreBoyutlandir_AltObjeBoyutuHesapla_mC089C8AE46FEE6CD94A3F30A5D005B06AA586F7F (void);
// 0x00000050 System.Void MerkezeGoreBoyutlandir::GorunurAlanGenisligiHesapla()
extern void MerkezeGoreBoyutlandir_GorunurAlanGenisligiHesapla_mE09C6B0B8E2A4C8CA3DA92EF096D276CD536EB91 (void);
// 0x00000051 System.Void MerkezeGoreBoyutlandir::.ctor()
extern void MerkezeGoreBoyutlandir__ctor_mCD7FB7346CE7A766E0ACAD717BCFFCA3299C5B61 (void);
// 0x00000052 System.Void svipe_Menu::Update()
extern void svipe_Menu_Update_m106B29A557639E8495FBC755101BD8946D79428F (void);
// 0x00000053 System.Void svipe_Menu::.ctor()
extern void svipe_Menu__ctor_m1CEE30A2D37C70BE8CB315AE25CE27F3005F1D56 (void);
// 0x00000054 System.Void scrollbarSize::Start()
extern void scrollbarSize_Start_mFEF17A93AEF627972F96F8AA02E07CA919C41F6A (void);
// 0x00000055 System.Void scrollbarSize::OnScrollbarValueChanged(System.Single)
extern void scrollbarSize_OnScrollbarValueChanged_m4ADFA0678BDEE8C7952D4996C981AC7F83184E0D (void);
// 0x00000056 System.Void scrollbarSize::.ctor()
extern void scrollbarSize__ctor_m6667E78AA68F95246DE1AA23186340A273B1DAB7 (void);
// 0x00000057 System.Void UIController::UygulamayiKapat()
extern void UIController_UygulamayiKapat_m916D271A320DC2CF3CB23A6130277C588D6B0FC3 (void);
// 0x00000058 System.Void UIController::Update()
extern void UIController_Update_m01A435E125093AE9631843DBDD904FEF5C82D282 (void);
// 0x00000059 System.Void UIController::.ctor()
extern void UIController__ctor_m57DAF9FADDD58C79489BCF6474E55D3514E9BA21 (void);
// 0x0000005A System.Void cubePanel::Update()
extern void cubePanel_Update_mE78AAE8BE17482DF4A1C37DB30290D00E521EDA2 (void);
// 0x0000005B System.Void cubePanel::.ctor()
extern void cubePanel__ctor_m4A6848E06A5ACADC4197A9A955440AD8190A5A82 (void);
// 0x0000005C System.Void CachingLoadExample::Start()
extern void CachingLoadExample_Start_m0E4328C933351A08F0114734780E574F40F677A2 (void);
// 0x0000005D System.Collections.IEnumerator CachingLoadExample::DownloadAndCache()
extern void CachingLoadExample_DownloadAndCache_mF4487DC82E54CF46DD031998ACFF4DC7370E882B (void);
// 0x0000005E System.Void CachingLoadExample::.ctor()
extern void CachingLoadExample__ctor_m6541B40CFAB80BC52E98CAABE77CEC79CC16AE30 (void);
// 0x0000005F System.Void CachingLoadExample/<DownloadAndCache>d__4::.ctor(System.Int32)
extern void U3CDownloadAndCacheU3Ed__4__ctor_m4F21C539BBFCA4BA9FB7EC3A671F6138DEE5E249 (void);
// 0x00000060 System.Void CachingLoadExample/<DownloadAndCache>d__4::System.IDisposable.Dispose()
extern void U3CDownloadAndCacheU3Ed__4_System_IDisposable_Dispose_mD02B8042829A3C35444BC73F7665F90E5D91D0DD (void);
// 0x00000061 System.Boolean CachingLoadExample/<DownloadAndCache>d__4::MoveNext()
extern void U3CDownloadAndCacheU3Ed__4_MoveNext_m6244A66405DCB297C14ED0D99970F4DAB47DBF7E (void);
// 0x00000062 System.Void CachingLoadExample/<DownloadAndCache>d__4::<>m__Finally1()
extern void U3CDownloadAndCacheU3Ed__4_U3CU3Em__Finally1_mE3C5DFE6975579E92092EBECB02F03E8D9D8145B (void);
// 0x00000063 System.Object CachingLoadExample/<DownloadAndCache>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadAndCacheU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1EBBB1958D5DD1DA2D998CA242D213D6A60A8D76 (void);
// 0x00000064 System.Void CachingLoadExample/<DownloadAndCache>d__4::System.Collections.IEnumerator.Reset()
extern void U3CDownloadAndCacheU3Ed__4_System_Collections_IEnumerator_Reset_m6E4ED7FA6DD400F4E6B4ABB2556E685C2A4132C8 (void);
// 0x00000065 System.Object CachingLoadExample/<DownloadAndCache>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadAndCacheU3Ed__4_System_Collections_IEnumerator_get_Current_mA1313E5FECCB8B642D0B69ADA6546EA3CEE028BA (void);
// 0x00000066 System.Collections.IEnumerator script::Start()
extern void script_Start_mBDC0E6710997FCBF56EB900A4B5F22EB9B94A1AE (void);
// 0x00000067 System.Void script::.ctor()
extern void script__ctor_mD50FA119904ACC39BA11429A564B84811DAB459A (void);
// 0x00000068 System.Void script/<Start>d__2::.ctor(System.Int32)
extern void U3CStartU3Ed__2__ctor_mF5705C0C398CD8684E58523E9DA12AA6C0B51E2A (void);
// 0x00000069 System.Void script/<Start>d__2::System.IDisposable.Dispose()
extern void U3CStartU3Ed__2_System_IDisposable_Dispose_m0D240E2FDBED87E1E915A2689C41CF809807E00A (void);
// 0x0000006A System.Boolean script/<Start>d__2::MoveNext()
extern void U3CStartU3Ed__2_MoveNext_m86F0E51F0A7A74472A61FD07A6D9560504F84E47 (void);
// 0x0000006B System.Void script/<Start>d__2::<>m__Finally1()
extern void U3CStartU3Ed__2_U3CU3Em__Finally1_mA2A2B1E35264FD143C274AE82EFCD55CD67542A3 (void);
// 0x0000006C System.Object script/<Start>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0B3B79FF648B8964506DD8D39E66DEC99CD268ED (void);
// 0x0000006D System.Void script/<Start>d__2::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m1453297B53E3241F93D9F46A973072628FD12E5C (void);
// 0x0000006E System.Object script/<Start>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mCF672EEEF34AAC75D10821A8CC8FFE11F2589942 (void);
// 0x0000006F System.Void GaugeValueDisplay::Update()
extern void GaugeValueDisplay_Update_mE91E680A1950AC4A12C089D20ADA2D7C070ABAE8 (void);
// 0x00000070 System.Void GaugeValueDisplay::SetPointerToValue(System.Single)
extern void GaugeValueDisplay_SetPointerToValue_mB731FE780495869810FB2663CC30FD7BC92083FB (void);
// 0x00000071 System.Void GaugeValueDisplay::.ctor()
extern void GaugeValueDisplay__ctor_m4FBA2CE4780DF3C397F87C0982B551421930A04B (void);
// 0x00000072 System.Void ImageValueDisplay::OnValueChanged(System.Single)
extern void ImageValueDisplay_OnValueChanged_m7BD87F160987A6380FAA14D243DE9F89E906BA8C (void);
// 0x00000073 System.Void ImageValueDisplay::Update()
extern void ImageValueDisplay_Update_m7D4127512568FDA46155D2B913E777517B078896 (void);
// 0x00000074 System.Void ImageValueDisplay::.ctor()
extern void ImageValueDisplay__ctor_m602FE7ABF3528C3A943D21B6CC8B47CEC9DF0CB2 (void);
// 0x00000075 System.Void LineRendererPatternShifter::Awake()
extern void LineRendererPatternShifter_Awake_m86C568F01E05AF491719AA1A883CB9142C0DFAA6 (void);
// 0x00000076 System.Void LineRendererPatternShifter::Update()
extern void LineRendererPatternShifter_Update_mBA1904CC1541882868B6FCB7B0A333D2DECC3430 (void);
// 0x00000077 System.Void LineRendererPatternShifter::.ctor()
extern void LineRendererPatternShifter__ctor_mF6ED3D63CBECD11E6189E2D59A1DC00B9C50C68F (void);
// 0x00000078 System.Void ModularBillBoard::Awake()
extern void ModularBillBoard_Awake_mD401E0E7260C606B16E2FB5E6E016F87B8A1D2D9 (void);
// 0x00000079 System.Void ModularBillBoard::CalculateDerivedValues()
extern void ModularBillBoard_CalculateDerivedValues_m91F478F691E187D2CA1CB36C52DD0D13FC3C556C (void);
// 0x0000007A System.Void ModularBillBoard::CalculateMaskSize()
extern void ModularBillBoard_CalculateMaskSize_m2C31861508A3FA5F6AA3BFE5972B5C472A776EB5 (void);
// 0x0000007B System.Void ModularBillBoard::CalculateLandmarkPosition()
extern void ModularBillBoard_CalculateLandmarkPosition_m5A4BDD5ACC2A23C75315C0FFBA629464F091E014 (void);
// 0x0000007C System.Void ModularBillBoard::Update()
extern void ModularBillBoard_Update_m51909956B004A80FC6176AB4E027980BACCA0647 (void);
// 0x0000007D System.Void ModularBillBoard::Opening(System.Single)
extern void ModularBillBoard_Opening_m587FDC68FA97938F0893F9FA8A44CEEC38CE5204 (void);
// 0x0000007E System.Void ModularBillBoard::Closed()
extern void ModularBillBoard_Closed_mF2023740D5E6B99E6985A86468AAEB330A69F6B2 (void);
// 0x0000007F System.Void ModularBillBoard::FullyOpened()
extern void ModularBillBoard_FullyOpened_mFF3C616C8FE9378D87112CAB5853AE3C6F4F4D6E (void);
// 0x00000080 System.Void ModularBillBoard::TurnContentVisible()
extern void ModularBillBoard_TurnContentVisible_m477117F799F4EF8936573DF05BE60BBA7560BDC2 (void);
// 0x00000081 System.Void ModularBillBoard::RollOutTheBackgroundForContent()
extern void ModularBillBoard_RollOutTheBackgroundForContent_m4ED29CF38ECB73E2A8CFD40E12E4253648E6B1EF (void);
// 0x00000082 System.Void ModularBillBoard::SwitchedToOpen()
extern void ModularBillBoard_SwitchedToOpen_mF1BE74DF25E278322445A5F7366A6DBFDC449D24 (void);
// 0x00000083 System.Void ModularBillBoard::SwitchToClosed()
extern void ModularBillBoard_SwitchToClosed_m25F0A2D3800DC19ACB7D2B2A7B36E736B64DCD0B (void);
// 0x00000084 System.Void ModularBillBoard::SwitchtoOpening()
extern void ModularBillBoard_SwitchtoOpening_m1BC5052B7875012D6AF6D63BFCC0E32E9973205E (void);
// 0x00000085 System.Void ModularBillBoard::.ctor()
extern void ModularBillBoard__ctor_m48C0399548B635F08A0458BB73537B6DC6AD5E37 (void);
// 0x00000086 System.Void NavMeshManager::Awake()
extern void NavMeshManager_Awake_mD390E682832B423AF07C032BDC04125407FADEE2 (void);
// 0x00000087 System.Void NavMeshManager::Update()
extern void NavMeshManager_Update_m233039547B9F773AA54E472D33521798B2183FE8 (void);
// 0x00000088 System.Void NavMeshManager::UpdateNavigationAgentPosition()
extern void NavMeshManager_UpdateNavigationAgentPosition_mAD604E8C56BFB5C57FF23BBEE09F4B34899363AC (void);
// 0x00000089 System.Void NavMeshManager::UpdateNavigationLineVisibility()
extern void NavMeshManager_UpdateNavigationLineVisibility_m40CACE72DE03B2635D6358138FDEA8ED33A586BF (void);
// 0x0000008A System.Void NavMeshManager::UpdateNavigationLinePath()
extern void NavMeshManager_UpdateNavigationLinePath_m5117B7D9C3CD19E6B8C20788B1BD6053841CA9B3 (void);
// 0x0000008B System.Void NavMeshManager::NavigateTo(UnityEngine.Transform)
extern void NavMeshManager_NavigateTo_m8A72D32C317D261A11E1284ADCE3C9CD137CCC6E (void);
// 0x0000008C System.Void NavMeshManager::DrawPath()
extern void NavMeshManager_DrawPath_m07D444A078675E536359865506EBA89ACFAF4144 (void);
// 0x0000008D System.Void NavMeshManager::OnAreaTargetFound()
extern void NavMeshManager_OnAreaTargetFound_m5D6FA3259709EE47D553DACDAE792EB606FF42C9 (void);
// 0x0000008E System.Void NavMeshManager::OnAreaTargetLost()
extern void NavMeshManager_OnAreaTargetLost_m53856E894FC9FC97186E89BBC44A5C9127B03F9D (void);
// 0x0000008F System.Void NavMeshManager::.ctor()
extern void NavMeshManager__ctor_m73560A259F94E72C87E807F2F2147A0C839FBBD1 (void);
// 0x00000090 System.Void TextValueDisplay::Update()
extern void TextValueDisplay_Update_m86B10390EA34E2D7DECCBA1E800C04017BEDEC0C (void);
// 0x00000091 System.Void TextValueDisplay::.ctor()
extern void TextValueDisplay__ctor_m2E850FAA6791CDF29E0F97044ED7AB49E5807665 (void);
// 0x00000092 System.Boolean Astronaut::get_IsDrilling()
extern void Astronaut_get_IsDrilling_m98CF696B0FE7E68FFD56F7B4ED955F924E82F86E (void);
// 0x00000093 System.Void Astronaut::set_IsDrilling(System.Boolean)
extern void Astronaut_set_IsDrilling_mA8089A13FBE2F75B01F81DCE8C8F48B835099C97 (void);
// 0x00000094 System.Boolean Astronaut::get_IsWaving()
extern void Astronaut_get_IsWaving_m902D9080DF3A04208A0F89CB8FD8112C096C3FB2 (void);
// 0x00000095 System.Void Astronaut::set_IsWaving(System.Boolean)
extern void Astronaut_set_IsWaving_m50DEC15A2C43DC8718D1DA0693D39D2FF548FBD7 (void);
// 0x00000096 System.Void Astronaut::OnEnter()
extern void Astronaut_OnEnter_m5F190ADB9008D372A0B7F9BFC28BC7293C72FF02 (void);
// 0x00000097 System.Void Astronaut::OnExit()
extern void Astronaut_OnExit_m8D8491A9ABD6374B795B85F0879079070AD5DAC9 (void);
// 0x00000098 System.Void Astronaut::StartDrilling()
extern void Astronaut_StartDrilling_m58B45F5272D6A9B8FA28CB41BA3C461DCB491397 (void);
// 0x00000099 System.Void Astronaut::SetAnimationIsDrillingToTrue()
extern void Astronaut_SetAnimationIsDrillingToTrue_m4582F7219B45C590A63636A93825A3E32751279F (void);
// 0x0000009A System.Void Astronaut::SetAnimationIsDrillingToFalse()
extern void Astronaut_SetAnimationIsDrillingToFalse_mEF0179B84F72734799C626209B25FE68E0AA68C7 (void);
// 0x0000009B System.Void Astronaut::SetAnimationDrillEffectOn()
extern void Astronaut_SetAnimationDrillEffectOn_m06B21579F14EE3F167F7BC37F151A6391A12C953 (void);
// 0x0000009C System.Void Astronaut::SetAnimationDrillEffectOff()
extern void Astronaut_SetAnimationDrillEffectOff_m48401173565F35D8DDB9AA564761C6530EB0F2B3 (void);
// 0x0000009D System.Void Astronaut::SetDrillEffect(System.Boolean)
extern void Astronaut_SetDrillEffect_m4FDBBB338E27B81CB43BE37AA6193AB85AD7450B (void);
// 0x0000009E System.Void Astronaut::SetAnimationWavingOff()
extern void Astronaut_SetAnimationWavingOff_mC337FC9B8823702F473673D774C1A9BDF6A057AF (void);
// 0x0000009F System.Void Astronaut::HandleVirtualButtonPressed()
extern void Astronaut_HandleVirtualButtonPressed_m6234C237CFB5A39BD0C1D4AB42FE0693F9DC15A7 (void);
// 0x000000A0 System.Void Astronaut::HandleVirtualButtonReleased()
extern void Astronaut_HandleVirtualButtonReleased_m5E519A1FFBC5744DD64EE15DD72FF97C13AB7E99 (void);
// 0x000000A1 System.Void Astronaut::.ctor()
extern void Astronaut__ctor_m4ED1461848F024D98F8D6FD1BBE5D91FCFE27120 (void);
// 0x000000A2 System.Void Augmentation::Start()
extern void Augmentation_Start_mFE6630D85EF8BB30DADE798418F7A75BC392367B (void);
// 0x000000A3 System.Void Augmentation::Disable()
extern void Augmentation_Disable_m66F76F03488ADFF2933612025B1781B57BB4B244 (void);
// 0x000000A4 System.Void Augmentation::Restore()
extern void Augmentation_Restore_m9AAD8F1C7D1D280634F42B9A9D302FDC493C4AC8 (void);
// 0x000000A5 System.Void Augmentation::OnEnter()
extern void Augmentation_OnEnter_m09D083AD0396009FFA0A87DB6B03438F98D06074 (void);
// 0x000000A6 System.Void Augmentation::OnExit()
extern void Augmentation_OnExit_m631B31C38FE9831C670877585F91D8461D5B39E3 (void);
// 0x000000A7 System.Void Augmentation::SetRenderersEnabled(System.Boolean)
extern void Augmentation_SetRenderersEnabled_mEC6577AC7CCF00C2EC6E66B80624263D446B7DAF (void);
// 0x000000A8 System.Void Augmentation::SetCollidersEnabled(System.Boolean)
extern void Augmentation_SetCollidersEnabled_m608249D621FE1907D915B2134D4100126FB9943A (void);
// 0x000000A9 System.Collections.IEnumerator Augmentation::WaitForThen(System.Single,System.Action)
extern void Augmentation_WaitForThen_mA4589585D567DF3C14D9A5A0552310FC18D8CE7B (void);
// 0x000000AA System.Void Augmentation::.ctor()
extern void Augmentation__ctor_mB51C794A6322B4057872DB223F1EEB1457636AD0 (void);
// 0x000000AB System.Void Augmentation/<WaitForThen>d__11::.ctor(System.Int32)
extern void U3CWaitForThenU3Ed__11__ctor_mFFFBEE7F2D8F86205B3CBEAD655B12E45CD95F1D (void);
// 0x000000AC System.Void Augmentation/<WaitForThen>d__11::System.IDisposable.Dispose()
extern void U3CWaitForThenU3Ed__11_System_IDisposable_Dispose_m7F60964BF9A4B184C86603C54221EE480F58660B (void);
// 0x000000AD System.Boolean Augmentation/<WaitForThen>d__11::MoveNext()
extern void U3CWaitForThenU3Ed__11_MoveNext_mD7B203E81E7948205857E428EF6BE907E71C85E3 (void);
// 0x000000AE System.Object Augmentation/<WaitForThen>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForThenU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1626491025BBF0AB6C9162787B3EA2E7B1DB49FD (void);
// 0x000000AF System.Void Augmentation/<WaitForThen>d__11::System.Collections.IEnumerator.Reset()
extern void U3CWaitForThenU3Ed__11_System_Collections_IEnumerator_Reset_m2B4FEFC0BC9F8B009BD0D522ECBABDAF9400ECBB (void);
// 0x000000B0 System.Object Augmentation/<WaitForThen>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForThenU3Ed__11_System_Collections_IEnumerator_get_Current_mE570029CC6814163A3D05CC00A50545D1455B2C6 (void);
// 0x000000B1 System.Void Augmentation/<>c::.cctor()
extern void U3CU3Ec__cctor_mF013DCF0DA2EC76F9F87723221A9B735719E1C8C (void);
// 0x000000B2 System.Void Augmentation/<>c::.ctor()
extern void U3CU3Ec__ctor_m4A01B2DB8AB2410D89E2C732CA15DA2D581442A1 (void);
// 0x000000B3 System.Void Augmentation/<>c::<.ctor>b__12_0()
extern void U3CU3Ec_U3C_ctorU3Eb__12_0_m2E706E1270C853367C4494D821F16BC687F55400 (void);
// 0x000000B4 System.Void Augmentation/<>c::<.ctor>b__12_1()
extern void U3CU3Ec_U3C_ctorU3Eb__12_1_m099F6AB3E05C14E7E28880C692EF25242B420FA9 (void);
// 0x000000B5 System.Boolean Drone::get_IsFacingObject()
extern void Drone_get_IsFacingObject_m3BFFD80E0436C4DE018AAC13C454F84556FAD2F0 (void);
// 0x000000B6 System.Void Drone::set_IsFacingObject(System.Boolean)
extern void Drone_set_IsFacingObject_m2C0070C48C21F9E91BCCCCD65725BDE7F0529B74 (void);
// 0x000000B7 System.Boolean Drone::get_IsScanning()
extern void Drone_get_IsScanning_m23B1594A820CF43D33167058728B8BB23C6DD5FF (void);
// 0x000000B8 System.Void Drone::set_IsScanning(System.Boolean)
extern void Drone_set_IsScanning_mF4332563EC817A2277DABEE3217F37FB15EB5793 (void);
// 0x000000B9 System.Boolean Drone::get_IsShowingLaser()
extern void Drone_get_IsShowingLaser_m84D0B03E2A795E41CA049FD6AF1108D8680C1221 (void);
// 0x000000BA System.Void Drone::set_IsShowingLaser(System.Boolean)
extern void Drone_set_IsShowingLaser_m9C68026F9465A5C454C939B00FA7142BEC5F4885 (void);
// 0x000000BB System.Void Drone::OnEnter()
extern void Drone_OnEnter_mC1CCC647CEE7F08F5AD560BC9D1935D0C42BD7DD (void);
// 0x000000BC System.Void Drone::OnExit()
extern void Drone_OnExit_m6C1D3439AA0A4C11DA1AE0889F452933A7EFC88C (void);
// 0x000000BD System.Void Drone::SetAnimationScanning(System.Boolean)
extern void Drone_SetAnimationScanning_m9BE7C98706BD9677B2749EED785AE724F3F8096A (void);
// 0x000000BE System.Void Drone::HandleVirtualButtonPressed()
extern void Drone_HandleVirtualButtonPressed_m74535C3EEE75E62DADB368C7E3B448DEF277B42B (void);
// 0x000000BF System.Void Drone::HandleVirtualButtonReleased()
extern void Drone_HandleVirtualButtonReleased_m1EEB615C408CF498E5EE6D14BB47CD80D63FA048 (void);
// 0x000000C0 System.Void Drone::.ctor()
extern void Drone__ctor_m57868EB05281636E8DE502240E749FAEDBEE5B15 (void);
// 0x000000C1 System.Void Fissure::HandleVirtualButtonPressed()
extern void Fissure_HandleVirtualButtonPressed_m503641CDEE1998AA3A3BD7E3CB2BC5FF4E205558 (void);
// 0x000000C2 System.Void Fissure::HandleVirtualButtonReleased()
extern void Fissure_HandleVirtualButtonReleased_m1721268658F13F7E73BC5C092136953941B2D389 (void);
// 0x000000C3 System.Void Fissure::.ctor()
extern void Fissure__ctor_m4D56FF0F86AF82B05AD498668C2851E29B7A886E (void);
// 0x000000C4 System.Boolean OxygenTank::get_IsDetailOn()
extern void OxygenTank_get_IsDetailOn_mAB2D512497D95CD76F6402A99D8061DE0161CF9F (void);
// 0x000000C5 System.Void OxygenTank::set_IsDetailOn(System.Boolean)
extern void OxygenTank_set_IsDetailOn_m803BB6E9EE5DC6F8C20D7C85BE74A3B42B07B30C (void);
// 0x000000C6 System.Void OxygenTank::OnEnter()
extern void OxygenTank_OnEnter_mCE9C7D59B3CAF97F3E9864E414DB2B23E1E7681C (void);
// 0x000000C7 System.Void OxygenTank::ShowDetail()
extern void OxygenTank_ShowDetail_mFAE0E8A05E65969E74EC5E20BED811EAE00FB5F3 (void);
// 0x000000C8 System.Void OxygenTank::HideDetail()
extern void OxygenTank_HideDetail_m1329FC311F64C036C62C6EBD00682BF065DC040B (void);
// 0x000000C9 System.Void OxygenTank::HandleVirtualButtonPressed()
extern void OxygenTank_HandleVirtualButtonPressed_m20E76E17914005DF0BC3ACA2308D6EDFEB11A9CD (void);
// 0x000000CA System.Void OxygenTank::HandleVirtualButtonReleased()
extern void OxygenTank_HandleVirtualButtonReleased_mC8848CD0F624DC05530D569EF9535C3844E8A48C (void);
// 0x000000CB System.Void OxygenTank::DoEnter()
extern void OxygenTank_DoEnter_mB1625C9D6BF4FD05A07CACFEA219E2D07E3DA27A (void);
// 0x000000CC System.Void OxygenTank::.ctor()
extern void OxygenTank__ctor_mD76F19EE50C68B7B3E09A8991C78AF345D72AA64 (void);
// 0x000000CD System.Void RockPileController::Awake()
extern void RockPileController_Awake_m4706AA45C105CF518970F338F34A634AB33D8725 (void);
// 0x000000CE System.Void RockPileController::FadeOut()
extern void RockPileController_FadeOut_mC5BACEDF666C11B6CDA170580EFC758B588CDBDA (void);
// 0x000000CF System.Void RockPileController::FadeIn()
extern void RockPileController_FadeIn_mDEE8C82757268757C534572FD8997F67C20E8C2C (void);
// 0x000000D0 System.Void RockPileController::.ctor()
extern void RockPileController__ctor_mB3509A94C44E9B8E9D409B9AE4ACBEE970E4C413 (void);
// 0x000000D1 System.Void DrillController::Update()
extern void DrillController_Update_mEEB9B83A80471648A2387847674129C4EF25B424 (void);
// 0x000000D2 System.Void DrillController::.ctor()
extern void DrillController__ctor_m092A584980F7AFF94A412DF044A392BBFB763178 (void);
// 0x000000D3 System.Void FadeObject::Awake()
extern void FadeObject_Awake_m9F2EFA48FB04B43FDE0546028389B5FC664C0327 (void);
// 0x000000D4 System.Void FadeObject::Update()
extern void FadeObject_Update_mD5683A44C69A511EEB25704103DB88AEB14C8F4C (void);
// 0x000000D5 System.Void FadeObject::SetInitialOpacity(System.Single)
extern void FadeObject_SetInitialOpacity_mC9343B1DFBA09DC8B47773D62906B784C6C25BED (void);
// 0x000000D6 System.Void FadeObject::SetOpacity(System.Single)
extern void FadeObject_SetOpacity_mE5EBA0DDBB615229F19D85C1BDFBF8A92B21F74D (void);
// 0x000000D7 System.Void FadeObject::SetOpaque()
extern void FadeObject_SetOpaque_m90CFBFAE094B9FE70F370957A326D560D5F48BFE (void);
// 0x000000D8 System.Void FadeObject::SetTransparent()
extern void FadeObject_SetTransparent_m9A03B29087EE80BFAB8FB30F335F788525C48994 (void);
// 0x000000D9 System.Void FadeObject::.ctor()
extern void FadeObject__ctor_mFAE79F46A8C8DA86FB65C30E411A1846C7649370 (void);
// 0x000000DA System.Void AstronautStateMachineBehaviour::DoStateEvent(UnityEngine.Animator,System.String)
extern void AstronautStateMachineBehaviour_DoStateEvent_m21D63B23E87D4CD636EBAD449FAF5CEA9CE6BC31 (void);
// 0x000000DB System.Type AstronautStateMachineBehaviour::GetTargetType()
extern void AstronautStateMachineBehaviour_GetTargetType_mED99E6CCF3C2ADB438D86E78ED66DB8C3BC10A8A (void);
// 0x000000DC System.Void AstronautStateMachineBehaviour::.ctor()
extern void AstronautStateMachineBehaviour__ctor_m48D9478F3B6EE45B43E109A07C85082EF828528C (void);
// 0x000000DD System.Void AugmentationStateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void AugmentationStateMachineBehaviour_OnStateEnter_m3EDD1F10C58738EE14CCA9C0761B76DF89BB8251 (void);
// 0x000000DE System.Void AugmentationStateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void AugmentationStateMachineBehaviour_OnStateExit_m8695AB4EE46C81F480BE6725DFDEB8F8FA2E16E9 (void);
// 0x000000DF System.Void AugmentationStateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void AugmentationStateMachineBehaviour_OnStateUpdate_m8F7C0AE28152B5E30AFE53B11598F0566FE6C861 (void);
// 0x000000E0 System.Void AugmentationStateMachineBehaviour::DoStateEvent(UnityEngine.Animator,System.String)
// 0x000000E1 System.Type AugmentationStateMachineBehaviour::GetTargetType()
// 0x000000E2 System.Action`1<T> AugmentationStateMachineBehaviour::GetMethod(T,System.String)
// 0x000000E3 System.Void AugmentationStateMachineBehaviour::AddDelegateToCache(System.Action`1<T>,System.String)
// 0x000000E4 System.Void AugmentationStateMachineBehaviour::.ctor()
extern void AugmentationStateMachineBehaviour__ctor_m3D4B8BF6A12DAD01888B550AA4DEDE1A4463602F (void);
// 0x000000E5 System.Void indirmedeneme::Start()
extern void indirmedeneme_Start_m4072EB6F8C63345A0549E839ECFBD5CBA4F0832A (void);
// 0x000000E6 System.Void indirmedeneme::Update()
extern void indirmedeneme_Update_mBB4515177963E20EFC71E372E593009650F7F8B8 (void);
// 0x000000E7 System.Collections.IEnumerator indirmedeneme::DownloadAssetBundle(System.String)
extern void indirmedeneme_DownloadAssetBundle_m5BBEF88A094AB650B20E49253205735D38EC8A58 (void);
// 0x000000E8 System.Void indirmedeneme::.ctor()
extern void indirmedeneme__ctor_m1AE8B151675144ED46BBA214BB9B4BF0A273BA96 (void);
// 0x000000E9 System.Void indirmedeneme/<DownloadAssetBundle>d__3::.ctor(System.Int32)
extern void U3CDownloadAssetBundleU3Ed__3__ctor_mFF9F5075D624368E8FC900F023EB07A1078D442D (void);
// 0x000000EA System.Void indirmedeneme/<DownloadAssetBundle>d__3::System.IDisposable.Dispose()
extern void U3CDownloadAssetBundleU3Ed__3_System_IDisposable_Dispose_m7C1D676C06CF11FCF991A7ACD7EEC1BAB2247A1B (void);
// 0x000000EB System.Boolean indirmedeneme/<DownloadAssetBundle>d__3::MoveNext()
extern void U3CDownloadAssetBundleU3Ed__3_MoveNext_mBA7924912E15CCE05BE398964DAEA26702D44002 (void);
// 0x000000EC System.Void indirmedeneme/<DownloadAssetBundle>d__3::<>m__Finally1()
extern void U3CDownloadAssetBundleU3Ed__3_U3CU3Em__Finally1_m55301D0AD1E77B64505CE2ABF704BEFCB492CA2C (void);
// 0x000000ED System.Object indirmedeneme/<DownloadAssetBundle>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadAssetBundleU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m08AA6ED27E147482E5FE7BBD37F785AEB95FB2FE (void);
// 0x000000EE System.Void indirmedeneme/<DownloadAssetBundle>d__3::System.Collections.IEnumerator.Reset()
extern void U3CDownloadAssetBundleU3Ed__3_System_Collections_IEnumerator_Reset_m302FCF7F3EBEB51FAA89277A789E5E3BC301D403 (void);
// 0x000000EF System.Object indirmedeneme/<DownloadAssetBundle>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadAssetBundleU3Ed__3_System_Collections_IEnumerator_get_Current_m4DC90CC5A391D8FE9697C8BECA79B7F3EFF7B193 (void);
static Il2CppMethodPointer s_methodPointers[239] = 
{
	CategoryCilckable_Start_m44CB2BDCBF4F83DE7F3C2CAE1A7EA145247F9E65,
	CategoryCilckable_RotationAdjust_m082796DF06BEBC8B69EBC1B69E7C61214354F78F,
	CategoryCilckable_BackButtonForVerticalScroll_m35B1CD281E89E573312B090D04871D5A6EFDB05E,
	CategoryCilckable_BackButtonClicked_m7AEDBC53C789211B102156BD0EED2EDAF879A972,
	CategoryCilckable_PanelsFalse_mA1432BEE745FF8EA38FA2E8021753B2F66DDDDB7,
	CategoryCilckable_PanelsTrue_m7D63789E4047FE5DBB934AB836F88DC1875D99C8,
	CategoryCilckable_Update_m61A7EE66B89983D51CD1CF881E420039872A1AA0,
	CategoryCilckable__ctor_m9FBD09C62AEECFCF1B4EC48714FC2DCE89D133CF,
	BtnPrefabScript_buttonClicked_m33B8C1771B33709BDB4E38DC59A9E845735BD322,
	BtnPrefabScript_OpenVerticalScrollView_m881D4A78C434485D963721F9ABF564A7D02BD34C,
	BtnPrefabScript_DownloadVerticalButtonsPhotos_m34DD81EC38964DB7C5516FB8D1688CBF975723C1,
	BtnPrefabScript__ctor_mB3709B7B608DE119D2AB6AF9B255810C5A5DD9CF,
	BtnPrefabScript_U3CDownloadVerticalButtonsPhotosU3Eb__12_0_mC76C117FC1139565C8B47F663D4B1FC4B7445A9C,
	U3CU3Ec__DisplayClass12_0__ctor_mC445B829227E4ECDDFAC5D926EB75081778B8A09,
	U3CU3Ec__DisplayClass12_0_U3CDownloadVerticalButtonsPhotosU3Eb__1_mC03463554EFC0E6989FFC0E55EB34CD75AE2547E,
	ButtonCreator_Start_m1E8C4DCCC795FDFC486E4FD0D0F8C243729B78A2,
	ButtonCreator_buttonCreate_mBC0219603823035DE041C60F23BAD700C4CBA007,
	ButtonCreator_CreateButton_m78AAAB644F3181E47BB1F16280FA98EC82FAF59B,
	ButtonCreator__ctor_m4640AC6AD151973D88347D4AFE311ADD1123F8DA,
	Create3DModel_Start_mA9E38DAB07B9CE9A08BFE2420E1C528B9C91C2E4,
	Create3DModel_Update_m8B4027FBBEB6C8EA0001C527C62338905ACE037B,
	Create3DModel_touchAndArrange_m2451124D3DDC33C531D2D97BC67286C6589027B0,
	Create3DModel_btnClicked_m6EB4C43971849300179AAB682C8975D7577BEA3D,
	Create3DModel_Download3dModels_mDC674B76828E9981066E7629FE8AFC5F0B472961,
	Create3DModel_DownloadAssetBundle_mCA1A34D5EE51EC4AC314555016F106BCA535C08E,
	Create3DModel__ctor_m51186D9603B7A0C2D8C75A10CB77F125AF3F724D,
	U3CDownloadAssetBundleU3Ed__11__ctor_mD5B96E18A4598751D9D323F9DE32E1256E85CD52,
	U3CDownloadAssetBundleU3Ed__11_System_IDisposable_Dispose_m3247B77C318D87D59E280129CC1EBDE8DC812930,
	U3CDownloadAssetBundleU3Ed__11_MoveNext_m94DFD564DB5788E48D90EC15DAE59A7BF5BEF3B9,
	U3CDownloadAssetBundleU3Ed__11_U3CU3Em__Finally1_mA3E9186EAE5B265A05524EC406C5655171D72A4F,
	U3CDownloadAssetBundleU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9989BD403603E7630A3FC51367E31B1E2117DBBD,
	U3CDownloadAssetBundleU3Ed__11_System_Collections_IEnumerator_Reset_mE49F30B824501D89E1C8CA52A2127FA44CEF97BA,
	U3CDownloadAssetBundleU3Ed__11_System_Collections_IEnumerator_get_Current_m280482882ED846B0B6EF97FE3E980C350FE1055A,
	VerticalButtonCreator_Start_m638E627422D5DE771FC41950F84ECEEBF712E122,
	VerticalButtonCreator_buttonCreate_mA13042D0673B3B9B438BCBA10CD7CAD68307E0E7,
	VerticalButtonCreator_CreateButton_m2DFC2865177D2AB5C50AEB9C541FFADA68E96F26,
	VerticalButtonCreator__ctor_m2A1310B531247B193017B695FC367ECFDC035240,
	DownloadAssetBundleExtension_DownloadAssetBundle_m402969E102F8C322D4BFBB7282CC99E1C04A14EC,
	DownloadAssetBundleExtension_Instantiate_m1C344BAC9321BDA69E4BC9FFD546E5F98175385A,
	U3CDownloadAssetBundleU3Ed__0__ctor_m7B2BF0DF356348556FC7042A3AE4A685755D5DE0,
	U3CDownloadAssetBundleU3Ed__0_System_IDisposable_Dispose_m7AFBF52E380519A03B91A866EE543235CCE30139,
	U3CDownloadAssetBundleU3Ed__0_MoveNext_m67D6E8E978187E1402416F98365BBE72F6A68E4B,
	U3CDownloadAssetBundleU3Ed__0_U3CU3Em__Finally1_m35453CEAEC2DBBFF1F3547C4E7BCDDE9F43B38E0,
	U3CDownloadAssetBundleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6BA65CE24E036AEB241CC688912C5C0BBF77F321,
	U3CDownloadAssetBundleU3Ed__0_System_Collections_IEnumerator_Reset_m95EEC0E276163815A0621D083A5245BA80D0E806,
	U3CDownloadAssetBundleU3Ed__0_System_Collections_IEnumerator_get_Current_m2376DACBEBE824FDE4BC450E4AB22942DB8AAE7A,
	DownloadImageExtensions_DownloadImage_m425EFA7E5A50937CE8EEE15F9D0067CF80BFE8A6,
	U3CDownloadImageU3Ed__0__ctor_m24345C0A4F0AF3E8FE9AE84E784DBB6034C9E009,
	U3CDownloadImageU3Ed__0_System_IDisposable_Dispose_m565B46EF4ABF21CEF4DC0689E0FC9FA6BB94F7FF,
	U3CDownloadImageU3Ed__0_MoveNext_m27CA7C0071033899F713E97DA022264D098E62D5,
	U3CDownloadImageU3Ed__0_U3CU3Em__Finally1_mE924B9CD499AEDF33AE59AA08DFC31DF3B303138,
	U3CDownloadImageU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDF12CBB291BAB27BBF99AF1DC0B7EBC3F89DC9D,
	U3CDownloadImageU3Ed__0_System_Collections_IEnumerator_Reset_mDA3A2F9B4C6CC57A732C0CE780504A4C486E0245,
	U3CDownloadImageU3Ed__0_System_Collections_IEnumerator_get_Current_mD8EAA09FDD726A953D8984E16355CA7FA8A6FB4C,
	ObjectPlacement_Start_mB67483D63BAFD07354C67356B889FC744A70BC66,
	ObjectPlacement_bringObject_m609946B7355C09C891B433EEE4354B22AC8B6041,
	ObjectPlacement_Update_mB3B08845401FFE539BA1C5F02003DDD320AE7C3E,
	ObjectPlacement__ctor_mBBF4544AF5B68FA0E7C132D7E1E1AFB2B80C8563,
	AssetBundleAugmenter_Start_m2D67C6266C8037B5938D34C1DE3777E1C6A93E76,
	AssetBundleAugmenter_DownloadAndCache_m827A785F9BA8DF539DDF5F123704DF6676456951,
	AssetBundleAugmenter_LoadAssetToTarget_m0DEB6BBE336969E86CD8830FB8DC4B3EBACAD8C7,
	AssetBundleAugmenter__ctor_m919795AD2BDDEF4086D6F081D459BB93F5B1007A,
	U3CDownloadAndCacheU3Ed__6__ctor_mC207A786AC388526B65793D9B7A3CD21637E7FE7,
	U3CDownloadAndCacheU3Ed__6_System_IDisposable_Dispose_m1232F60243AEDD6A732706374CA4B4291EA423EC,
	U3CDownloadAndCacheU3Ed__6_MoveNext_m44E3EDA27A7FC0CE2223446E673D8D054621AB70,
	U3CDownloadAndCacheU3Ed__6_U3CU3Em__Finally1_mAB69839ABD6A749E6C0B758E7E5A8D06560531E4,
	U3CDownloadAndCacheU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m45D9FC362C3EF9B098B0D929053584864B3D3801,
	U3CDownloadAndCacheU3Ed__6_System_Collections_IEnumerator_Reset_m1C26776B3F6459BAC1EEBCB840B1CEB3A0F0B5CB,
	U3CDownloadAndCacheU3Ed__6_System_Collections_IEnumerator_get_Current_mD133B8114A8DF8E11DD7D96976E3AF9257A12C56,
	Deneme_Start_m8D2FCF29229F1A6C11285CEAE09EAE1F93022EB7,
	Deneme__ctor_m5EFE3594C4E19B41C31BFFA1E293C15D62A36C89,
	U3CU3Ec__cctor_mC94144175F6F799EFCF1B9CBE542AF194D0DC4CA,
	U3CU3Ec__ctor_m07158E4768BF189B5B31B10614CDE230C787552C,
	U3CU3Ec_U3CStartU3Eb__7_0_m1C9CB0D592AA4A21F19A3FC589451D3CE9D17F0B,
	U3CU3Ec_U3CStartU3Eb__7_1_mA2EB8276F6C28CEEE40CA1426C08460FDD13451E,
	MerkezeGoreBoyutlandir_Start_m22BC74C1EE7037BD77C7DF8C68436C93D5F132D8,
	MerkezeGoreBoyutlandir_ObjeleriBoyutlandir_mDAEFC87082607875FA26B57F853CAA25ECE22A03,
	MerkezeGoreBoyutlandir_BosluklariHesapla_m0771885A0456B0054BD53F3F7953361E932ED67F,
	MerkezeGoreBoyutlandir_AltObjeBoyutuHesapla_mC089C8AE46FEE6CD94A3F30A5D005B06AA586F7F,
	MerkezeGoreBoyutlandir_GorunurAlanGenisligiHesapla_mE09C6B0B8E2A4C8CA3DA92EF096D276CD536EB91,
	MerkezeGoreBoyutlandir__ctor_mCD7FB7346CE7A766E0ACAD717BCFFCA3299C5B61,
	svipe_Menu_Update_m106B29A557639E8495FBC755101BD8946D79428F,
	svipe_Menu__ctor_m1CEE30A2D37C70BE8CB315AE25CE27F3005F1D56,
	scrollbarSize_Start_mFEF17A93AEF627972F96F8AA02E07CA919C41F6A,
	scrollbarSize_OnScrollbarValueChanged_m4ADFA0678BDEE8C7952D4996C981AC7F83184E0D,
	scrollbarSize__ctor_m6667E78AA68F95246DE1AA23186340A273B1DAB7,
	UIController_UygulamayiKapat_m916D271A320DC2CF3CB23A6130277C588D6B0FC3,
	UIController_Update_m01A435E125093AE9631843DBDD904FEF5C82D282,
	UIController__ctor_m57DAF9FADDD58C79489BCF6474E55D3514E9BA21,
	cubePanel_Update_mE78AAE8BE17482DF4A1C37DB30290D00E521EDA2,
	cubePanel__ctor_m4A6848E06A5ACADC4197A9A955440AD8190A5A82,
	CachingLoadExample_Start_m0E4328C933351A08F0114734780E574F40F677A2,
	CachingLoadExample_DownloadAndCache_mF4487DC82E54CF46DD031998ACFF4DC7370E882B,
	CachingLoadExample__ctor_m6541B40CFAB80BC52E98CAABE77CEC79CC16AE30,
	U3CDownloadAndCacheU3Ed__4__ctor_m4F21C539BBFCA4BA9FB7EC3A671F6138DEE5E249,
	U3CDownloadAndCacheU3Ed__4_System_IDisposable_Dispose_mD02B8042829A3C35444BC73F7665F90E5D91D0DD,
	U3CDownloadAndCacheU3Ed__4_MoveNext_m6244A66405DCB297C14ED0D99970F4DAB47DBF7E,
	U3CDownloadAndCacheU3Ed__4_U3CU3Em__Finally1_mE3C5DFE6975579E92092EBECB02F03E8D9D8145B,
	U3CDownloadAndCacheU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1EBBB1958D5DD1DA2D998CA242D213D6A60A8D76,
	U3CDownloadAndCacheU3Ed__4_System_Collections_IEnumerator_Reset_m6E4ED7FA6DD400F4E6B4ABB2556E685C2A4132C8,
	U3CDownloadAndCacheU3Ed__4_System_Collections_IEnumerator_get_Current_mA1313E5FECCB8B642D0B69ADA6546EA3CEE028BA,
	script_Start_mBDC0E6710997FCBF56EB900A4B5F22EB9B94A1AE,
	script__ctor_mD50FA119904ACC39BA11429A564B84811DAB459A,
	U3CStartU3Ed__2__ctor_mF5705C0C398CD8684E58523E9DA12AA6C0B51E2A,
	U3CStartU3Ed__2_System_IDisposable_Dispose_m0D240E2FDBED87E1E915A2689C41CF809807E00A,
	U3CStartU3Ed__2_MoveNext_m86F0E51F0A7A74472A61FD07A6D9560504F84E47,
	U3CStartU3Ed__2_U3CU3Em__Finally1_mA2A2B1E35264FD143C274AE82EFCD55CD67542A3,
	U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0B3B79FF648B8964506DD8D39E66DEC99CD268ED,
	U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m1453297B53E3241F93D9F46A973072628FD12E5C,
	U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mCF672EEEF34AAC75D10821A8CC8FFE11F2589942,
	GaugeValueDisplay_Update_mE91E680A1950AC4A12C089D20ADA2D7C070ABAE8,
	GaugeValueDisplay_SetPointerToValue_mB731FE780495869810FB2663CC30FD7BC92083FB,
	GaugeValueDisplay__ctor_m4FBA2CE4780DF3C397F87C0982B551421930A04B,
	ImageValueDisplay_OnValueChanged_m7BD87F160987A6380FAA14D243DE9F89E906BA8C,
	ImageValueDisplay_Update_m7D4127512568FDA46155D2B913E777517B078896,
	ImageValueDisplay__ctor_m602FE7ABF3528C3A943D21B6CC8B47CEC9DF0CB2,
	LineRendererPatternShifter_Awake_m86C568F01E05AF491719AA1A883CB9142C0DFAA6,
	LineRendererPatternShifter_Update_mBA1904CC1541882868B6FCB7B0A333D2DECC3430,
	LineRendererPatternShifter__ctor_mF6ED3D63CBECD11E6189E2D59A1DC00B9C50C68F,
	ModularBillBoard_Awake_mD401E0E7260C606B16E2FB5E6E016F87B8A1D2D9,
	ModularBillBoard_CalculateDerivedValues_m91F478F691E187D2CA1CB36C52DD0D13FC3C556C,
	ModularBillBoard_CalculateMaskSize_m2C31861508A3FA5F6AA3BFE5972B5C472A776EB5,
	ModularBillBoard_CalculateLandmarkPosition_m5A4BDD5ACC2A23C75315C0FFBA629464F091E014,
	ModularBillBoard_Update_m51909956B004A80FC6176AB4E027980BACCA0647,
	ModularBillBoard_Opening_m587FDC68FA97938F0893F9FA8A44CEEC38CE5204,
	ModularBillBoard_Closed_mF2023740D5E6B99E6985A86468AAEB330A69F6B2,
	ModularBillBoard_FullyOpened_mFF3C616C8FE9378D87112CAB5853AE3C6F4F4D6E,
	ModularBillBoard_TurnContentVisible_m477117F799F4EF8936573DF05BE60BBA7560BDC2,
	ModularBillBoard_RollOutTheBackgroundForContent_m4ED29CF38ECB73E2A8CFD40E12E4253648E6B1EF,
	ModularBillBoard_SwitchedToOpen_mF1BE74DF25E278322445A5F7366A6DBFDC449D24,
	ModularBillBoard_SwitchToClosed_m25F0A2D3800DC19ACB7D2B2A7B36E736B64DCD0B,
	ModularBillBoard_SwitchtoOpening_m1BC5052B7875012D6AF6D63BFCC0E32E9973205E,
	ModularBillBoard__ctor_m48C0399548B635F08A0458BB73537B6DC6AD5E37,
	NavMeshManager_Awake_mD390E682832B423AF07C032BDC04125407FADEE2,
	NavMeshManager_Update_m233039547B9F773AA54E472D33521798B2183FE8,
	NavMeshManager_UpdateNavigationAgentPosition_mAD604E8C56BFB5C57FF23BBEE09F4B34899363AC,
	NavMeshManager_UpdateNavigationLineVisibility_m40CACE72DE03B2635D6358138FDEA8ED33A586BF,
	NavMeshManager_UpdateNavigationLinePath_m5117B7D9C3CD19E6B8C20788B1BD6053841CA9B3,
	NavMeshManager_NavigateTo_m8A72D32C317D261A11E1284ADCE3C9CD137CCC6E,
	NavMeshManager_DrawPath_m07D444A078675E536359865506EBA89ACFAF4144,
	NavMeshManager_OnAreaTargetFound_m5D6FA3259709EE47D553DACDAE792EB606FF42C9,
	NavMeshManager_OnAreaTargetLost_m53856E894FC9FC97186E89BBC44A5C9127B03F9D,
	NavMeshManager__ctor_m73560A259F94E72C87E807F2F2147A0C839FBBD1,
	TextValueDisplay_Update_m86B10390EA34E2D7DECCBA1E800C04017BEDEC0C,
	TextValueDisplay__ctor_m2E850FAA6791CDF29E0F97044ED7AB49E5807665,
	Astronaut_get_IsDrilling_m98CF696B0FE7E68FFD56F7B4ED955F924E82F86E,
	Astronaut_set_IsDrilling_mA8089A13FBE2F75B01F81DCE8C8F48B835099C97,
	Astronaut_get_IsWaving_m902D9080DF3A04208A0F89CB8FD8112C096C3FB2,
	Astronaut_set_IsWaving_m50DEC15A2C43DC8718D1DA0693D39D2FF548FBD7,
	Astronaut_OnEnter_m5F190ADB9008D372A0B7F9BFC28BC7293C72FF02,
	Astronaut_OnExit_m8D8491A9ABD6374B795B85F0879079070AD5DAC9,
	Astronaut_StartDrilling_m58B45F5272D6A9B8FA28CB41BA3C461DCB491397,
	Astronaut_SetAnimationIsDrillingToTrue_m4582F7219B45C590A63636A93825A3E32751279F,
	Astronaut_SetAnimationIsDrillingToFalse_mEF0179B84F72734799C626209B25FE68E0AA68C7,
	Astronaut_SetAnimationDrillEffectOn_m06B21579F14EE3F167F7BC37F151A6391A12C953,
	Astronaut_SetAnimationDrillEffectOff_m48401173565F35D8DDB9AA564761C6530EB0F2B3,
	Astronaut_SetDrillEffect_m4FDBBB338E27B81CB43BE37AA6193AB85AD7450B,
	Astronaut_SetAnimationWavingOff_mC337FC9B8823702F473673D774C1A9BDF6A057AF,
	Astronaut_HandleVirtualButtonPressed_m6234C237CFB5A39BD0C1D4AB42FE0693F9DC15A7,
	Astronaut_HandleVirtualButtonReleased_m5E519A1FFBC5744DD64EE15DD72FF97C13AB7E99,
	Astronaut__ctor_m4ED1461848F024D98F8D6FD1BBE5D91FCFE27120,
	Augmentation_Start_mFE6630D85EF8BB30DADE798418F7A75BC392367B,
	Augmentation_Disable_m66F76F03488ADFF2933612025B1781B57BB4B244,
	Augmentation_Restore_m9AAD8F1C7D1D280634F42B9A9D302FDC493C4AC8,
	Augmentation_OnEnter_m09D083AD0396009FFA0A87DB6B03438F98D06074,
	Augmentation_OnExit_m631B31C38FE9831C670877585F91D8461D5B39E3,
	Augmentation_SetRenderersEnabled_mEC6577AC7CCF00C2EC6E66B80624263D446B7DAF,
	Augmentation_SetCollidersEnabled_m608249D621FE1907D915B2134D4100126FB9943A,
	Augmentation_WaitForThen_mA4589585D567DF3C14D9A5A0552310FC18D8CE7B,
	Augmentation__ctor_mB51C794A6322B4057872DB223F1EEB1457636AD0,
	U3CWaitForThenU3Ed__11__ctor_mFFFBEE7F2D8F86205B3CBEAD655B12E45CD95F1D,
	U3CWaitForThenU3Ed__11_System_IDisposable_Dispose_m7F60964BF9A4B184C86603C54221EE480F58660B,
	U3CWaitForThenU3Ed__11_MoveNext_mD7B203E81E7948205857E428EF6BE907E71C85E3,
	U3CWaitForThenU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1626491025BBF0AB6C9162787B3EA2E7B1DB49FD,
	U3CWaitForThenU3Ed__11_System_Collections_IEnumerator_Reset_m2B4FEFC0BC9F8B009BD0D522ECBABDAF9400ECBB,
	U3CWaitForThenU3Ed__11_System_Collections_IEnumerator_get_Current_mE570029CC6814163A3D05CC00A50545D1455B2C6,
	U3CU3Ec__cctor_mF013DCF0DA2EC76F9F87723221A9B735719E1C8C,
	U3CU3Ec__ctor_m4A01B2DB8AB2410D89E2C732CA15DA2D581442A1,
	U3CU3Ec_U3C_ctorU3Eb__12_0_m2E706E1270C853367C4494D821F16BC687F55400,
	U3CU3Ec_U3C_ctorU3Eb__12_1_m099F6AB3E05C14E7E28880C692EF25242B420FA9,
	Drone_get_IsFacingObject_m3BFFD80E0436C4DE018AAC13C454F84556FAD2F0,
	Drone_set_IsFacingObject_m2C0070C48C21F9E91BCCCCD65725BDE7F0529B74,
	Drone_get_IsScanning_m23B1594A820CF43D33167058728B8BB23C6DD5FF,
	Drone_set_IsScanning_mF4332563EC817A2277DABEE3217F37FB15EB5793,
	Drone_get_IsShowingLaser_m84D0B03E2A795E41CA049FD6AF1108D8680C1221,
	Drone_set_IsShowingLaser_m9C68026F9465A5C454C939B00FA7142BEC5F4885,
	Drone_OnEnter_mC1CCC647CEE7F08F5AD560BC9D1935D0C42BD7DD,
	Drone_OnExit_m6C1D3439AA0A4C11DA1AE0889F452933A7EFC88C,
	Drone_SetAnimationScanning_m9BE7C98706BD9677B2749EED785AE724F3F8096A,
	Drone_HandleVirtualButtonPressed_m74535C3EEE75E62DADB368C7E3B448DEF277B42B,
	Drone_HandleVirtualButtonReleased_m1EEB615C408CF498E5EE6D14BB47CD80D63FA048,
	Drone__ctor_m57868EB05281636E8DE502240E749FAEDBEE5B15,
	Fissure_HandleVirtualButtonPressed_m503641CDEE1998AA3A3BD7E3CB2BC5FF4E205558,
	Fissure_HandleVirtualButtonReleased_m1721268658F13F7E73BC5C092136953941B2D389,
	Fissure__ctor_m4D56FF0F86AF82B05AD498668C2851E29B7A886E,
	OxygenTank_get_IsDetailOn_mAB2D512497D95CD76F6402A99D8061DE0161CF9F,
	OxygenTank_set_IsDetailOn_m803BB6E9EE5DC6F8C20D7C85BE74A3B42B07B30C,
	OxygenTank_OnEnter_mCE9C7D59B3CAF97F3E9864E414DB2B23E1E7681C,
	OxygenTank_ShowDetail_mFAE0E8A05E65969E74EC5E20BED811EAE00FB5F3,
	OxygenTank_HideDetail_m1329FC311F64C036C62C6EBD00682BF065DC040B,
	OxygenTank_HandleVirtualButtonPressed_m20E76E17914005DF0BC3ACA2308D6EDFEB11A9CD,
	OxygenTank_HandleVirtualButtonReleased_mC8848CD0F624DC05530D569EF9535C3844E8A48C,
	OxygenTank_DoEnter_mB1625C9D6BF4FD05A07CACFEA219E2D07E3DA27A,
	OxygenTank__ctor_mD76F19EE50C68B7B3E09A8991C78AF345D72AA64,
	RockPileController_Awake_m4706AA45C105CF518970F338F34A634AB33D8725,
	RockPileController_FadeOut_mC5BACEDF666C11B6CDA170580EFC758B588CDBDA,
	RockPileController_FadeIn_mDEE8C82757268757C534572FD8997F67C20E8C2C,
	RockPileController__ctor_mB3509A94C44E9B8E9D409B9AE4ACBEE970E4C413,
	DrillController_Update_mEEB9B83A80471648A2387847674129C4EF25B424,
	DrillController__ctor_m092A584980F7AFF94A412DF044A392BBFB763178,
	FadeObject_Awake_m9F2EFA48FB04B43FDE0546028389B5FC664C0327,
	FadeObject_Update_mD5683A44C69A511EEB25704103DB88AEB14C8F4C,
	FadeObject_SetInitialOpacity_mC9343B1DFBA09DC8B47773D62906B784C6C25BED,
	FadeObject_SetOpacity_mE5EBA0DDBB615229F19D85C1BDFBF8A92B21F74D,
	FadeObject_SetOpaque_m90CFBFAE094B9FE70F370957A326D560D5F48BFE,
	FadeObject_SetTransparent_m9A03B29087EE80BFAB8FB30F335F788525C48994,
	FadeObject__ctor_mFAE79F46A8C8DA86FB65C30E411A1846C7649370,
	AstronautStateMachineBehaviour_DoStateEvent_m21D63B23E87D4CD636EBAD449FAF5CEA9CE6BC31,
	AstronautStateMachineBehaviour_GetTargetType_mED99E6CCF3C2ADB438D86E78ED66DB8C3BC10A8A,
	AstronautStateMachineBehaviour__ctor_m48D9478F3B6EE45B43E109A07C85082EF828528C,
	AugmentationStateMachineBehaviour_OnStateEnter_m3EDD1F10C58738EE14CCA9C0761B76DF89BB8251,
	AugmentationStateMachineBehaviour_OnStateExit_m8695AB4EE46C81F480BE6725DFDEB8F8FA2E16E9,
	AugmentationStateMachineBehaviour_OnStateUpdate_m8F7C0AE28152B5E30AFE53B11598F0566FE6C861,
	NULL,
	NULL,
	NULL,
	NULL,
	AugmentationStateMachineBehaviour__ctor_m3D4B8BF6A12DAD01888B550AA4DEDE1A4463602F,
	indirmedeneme_Start_m4072EB6F8C63345A0549E839ECFBD5CBA4F0832A,
	indirmedeneme_Update_mBB4515177963E20EFC71E372E593009650F7F8B8,
	indirmedeneme_DownloadAssetBundle_m5BBEF88A094AB650B20E49253205735D38EC8A58,
	indirmedeneme__ctor_m1AE8B151675144ED46BBA214BB9B4BF0A273BA96,
	U3CDownloadAssetBundleU3Ed__3__ctor_mFF9F5075D624368E8FC900F023EB07A1078D442D,
	U3CDownloadAssetBundleU3Ed__3_System_IDisposable_Dispose_m7C1D676C06CF11FCF991A7ACD7EEC1BAB2247A1B,
	U3CDownloadAssetBundleU3Ed__3_MoveNext_mBA7924912E15CCE05BE398964DAEA26702D44002,
	U3CDownloadAssetBundleU3Ed__3_U3CU3Em__Finally1_m55301D0AD1E77B64505CE2ABF704BEFCB492CA2C,
	U3CDownloadAssetBundleU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m08AA6ED27E147482E5FE7BBD37F785AEB95FB2FE,
	U3CDownloadAssetBundleU3Ed__3_System_Collections_IEnumerator_Reset_m302FCF7F3EBEB51FAA89277A789E5E3BC301D403,
	U3CDownloadAssetBundleU3Ed__3_System_Collections_IEnumerator_get_Current_m4DC90CC5A391D8FE9697C8BECA79B7F3EFF7B193,
};
static const int32_t s_InvokerIndices[239] = 
{
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2856,
	3595,
	2856,
	3595,
	4866,
	1582,
	3595,
	3595,
	3595,
	2856,
	3595,
	3595,
	2550,
	3595,
	2842,
	3595,
	3478,
	3595,
	3533,
	3595,
	3533,
	3595,
	4866,
	1582,
	3595,
	4731,
	4511,
	2842,
	3595,
	3478,
	3595,
	3533,
	3595,
	3533,
	4731,
	2842,
	3595,
	3478,
	3595,
	3533,
	3595,
	3533,
	3595,
	5298,
	3595,
	3595,
	3595,
	3533,
	3595,
	3595,
	2842,
	3595,
	3478,
	3595,
	3533,
	3595,
	3533,
	3595,
	3595,
	5396,
	3595,
	2856,
	2856,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2883,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3533,
	3595,
	2842,
	3595,
	3478,
	3595,
	3533,
	3595,
	3533,
	3533,
	3595,
	2842,
	3595,
	3478,
	3595,
	3533,
	3595,
	3533,
	3595,
	2883,
	3595,
	2883,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2883,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2856,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3478,
	2797,
	3478,
	2797,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2797,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2797,
	2797,
	1267,
	3595,
	2842,
	3595,
	3478,
	3533,
	3595,
	3533,
	5396,
	3595,
	3595,
	3595,
	3478,
	2797,
	3478,
	2797,
	3478,
	2797,
	3595,
	3595,
	2797,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3478,
	2797,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2883,
	2883,
	3595,
	3595,
	3595,
	1582,
	3533,
	3595,
	939,
	939,
	939,
	1582,
	3533,
	-1,
	-1,
	3595,
	3595,
	3595,
	2550,
	3595,
	2842,
	3595,
	3478,
	3595,
	3533,
	3595,
	3533,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x060000E2, { 0, 5 } },
	{ 0x060000E3, { 5, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[6] = 
{
	{ (Il2CppRGCTXDataType)1, 82 },
	{ (Il2CppRGCTXDataType)2, 1394 },
	{ (Il2CppRGCTXDataType)2, 82 },
	{ (Il2CppRGCTXDataType)1, 1394 },
	{ (Il2CppRGCTXDataType)3, 26044 },
	{ (Il2CppRGCTXDataType)1, 83 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	239,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	6,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* LogUtil_LogMessageFromCallback_m04F3C7908BCA7A583B183A9551387E5F04B21994_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingApplicationException_m0829F0837975040087642D6A0EF77DE0F908FCEE_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentException_mB89C3689EC192CAD485B74904289BC70D393B9B3_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentNullException_mE67B9DA5FB8088FD758246C75CACB5A5521B5D26_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3B25880532D77834352308A7841325D00B402D4C_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArithmeticException_m43E86638D478FE1109B49BB4F8844EF4EDD34414_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingDivideByZeroException_m2B34635F25839BAFD46740D0964E237A1582578B_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIOException_m024FB3DF66F596B41BAD3EB701F4DED5057FE49C_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m7BC82194E760DFF75E2F56936BC89321263F7253_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidCastException_m71BAB12378AF0F55B0C013BB0146B9A0AFD0B3DF_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidOperationException_m782571C94E167C84E015BA1F883EB335A1719E45_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingNullReferenceException_mEA2CE40C44CD6C5FE044BAB6FFA434AC5CCF4E45_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOutOfMemoryException_m72E3606F191DCDDEF234BAAF117CC916E1CBD5BC_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOverflowException_m8CD04C3E8A76FACEA1FFC6A074090902A335BD85_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingSystemException_m7BB7BA403702277AEB0FD8DEB51798845CCBD063_RuntimeMethod_var;
extern const RuntimeMethod* SWIGStringHelper_CreateString_mFB66507B0E5F5D74116BC8CADAC859CD8614D642_RuntimeMethod_var;



// 0x00000001 System.Void Firebase.FirebaseException::.ctor(System.Int32,System.String)
extern void FirebaseException__ctor_mCB919E722DF4F366C4E0D9278716CBED5DD8907C (void);
// 0x00000002 System.Void Firebase.FirebaseException::set_ErrorCode(System.Int32)
extern void FirebaseException_set_ErrorCode_mE216C4C0EBAACC7ADA04ED328DEC1474680F9B5A (void);
// 0x00000003 System.Void Firebase.InitializationException::set_InitResult(Firebase.InitResult)
extern void InitializationException_set_InitResult_mD6E68B41830F64CB54B3BF7FFE915263D8AD8E34 (void);
// 0x00000004 System.Void Firebase.InitializationException::.ctor(Firebase.InitResult,System.String)
extern void InitializationException__ctor_m4F5649529A9F0863B359E63E74F8B5331F138A14 (void);
// 0x00000005 System.Void Firebase.InitializationException::.ctor(Firebase.InitResult,System.String,System.Exception)
extern void InitializationException__ctor_m56641135A502F6D70F771A69157A0D674EB99DD3 (void);
// 0x00000006 System.String Firebase.ErrorMessages::get_DependencyNotFoundErrorMessage()
extern void ErrorMessages_get_DependencyNotFoundErrorMessage_mB1677E85E4CFF5FE580A09B706B1EF9EFBB05DDE (void);
// 0x00000007 System.String Firebase.ErrorMessages::get_DllNotFoundExceptionErrorMessage()
extern void ErrorMessages_get_DllNotFoundExceptionErrorMessage_mFF21A563C38214BE449D6115FCE52DD4E94033F0 (void);
// 0x00000008 System.Void Firebase.ErrorMessages::.cctor()
extern void ErrorMessages__cctor_m936EA2AD91AAD13B58BCE6D60861C6936D6EF963 (void);
// 0x00000009 System.Void Firebase.LogUtil::.cctor()
extern void LogUtil__cctor_mBB82D51ADECA24464B7C848D04E6DD047A5FBEA7 (void);
// 0x0000000A System.Void Firebase.LogUtil::InitializeLogging()
extern void LogUtil_InitializeLogging_m0B74F188359DDD87A4870321573D37C4593AF8CB (void);
// 0x0000000B Firebase.Platform.PlatformLogLevel Firebase.LogUtil::ConvertLogLevel(Firebase.LogLevel)
extern void LogUtil_ConvertLogLevel_m84040323CAAA7E32FE4B104B7042D7254D55BD2E (void);
// 0x0000000C System.Void Firebase.LogUtil::LogMessage(Firebase.LogLevel,System.String)
extern void LogUtil_LogMessage_mB9917DEE02B6375ED692EE4FF48F04F56004D135 (void);
// 0x0000000D System.Void Firebase.LogUtil::LogMessageFromCallback(Firebase.LogLevel,System.String)
extern void LogUtil_LogMessageFromCallback_m04F3C7908BCA7A583B183A9551387E5F04B21994 (void);
// 0x0000000E System.Void Firebase.LogUtil::.ctor()
extern void LogUtil__ctor_mE6F41CDC7EFF92D76E3D07B5F8350BF7D5A4983D (void);
// 0x0000000F System.Void Firebase.LogUtil::Finalize()
extern void LogUtil_Finalize_mD288B9870DD8D7AF744044513FCA62F9AB42A85C (void);
// 0x00000010 System.Void Firebase.LogUtil::Dispose()
extern void LogUtil_Dispose_mB12D003420083CAA79A613F44A635DA5418C989D (void);
// 0x00000011 System.Void Firebase.LogUtil::Dispose(System.Boolean)
extern void LogUtil_Dispose_m3E431D1105B6EBDC3183FA2B55AA7608607BBC6D (void);
// 0x00000012 System.Void Firebase.LogUtil::<.ctor>b__9_0(System.Object,System.EventArgs)
extern void LogUtil_U3C_ctorU3Eb__9_0_m3CCEC7958C25A61458C58F678BCD77AA29A551B6 (void);
// 0x00000013 System.Void Firebase.LogUtil/LogMessageDelegate::.ctor(System.Object,System.IntPtr)
extern void LogMessageDelegate__ctor_mEBA3FFB53CCE522DBB1B5571A5623A649E6643F0 (void);
// 0x00000014 System.Void Firebase.LogUtil/LogMessageDelegate::Invoke(Firebase.LogLevel,System.String)
extern void LogMessageDelegate_Invoke_mB54C38843065556AF65D1E42C9DDC9AFAFA5C5E8 (void);
// 0x00000015 System.IAsyncResult Firebase.LogUtil/LogMessageDelegate::BeginInvoke(Firebase.LogLevel,System.String,System.AsyncCallback,System.Object)
extern void LogMessageDelegate_BeginInvoke_m9A9B00026484A1266F2E8E1101699C83D2755654 (void);
// 0x00000016 System.Void Firebase.LogUtil/LogMessageDelegate::EndInvoke(System.IAsyncResult)
extern void LogMessageDelegate_EndInvoke_m13C568B3D481DF381F37AAEF2FA598025FDB8C95 (void);
// 0x00000017 System.Void Firebase.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern void MonoPInvokeCallbackAttribute__ctor_mD5BA102663DCE67244B79EF374E5641E765AB5CB (void);
// 0x00000018 System.Void Firebase.FutureBase::.ctor(System.IntPtr,System.Boolean)
extern void FutureBase__ctor_m69C88EC69B422C5752B2E249303D92F649B8C8AC (void);
// 0x00000019 System.Void Firebase.FutureBase::Finalize()
extern void FutureBase_Finalize_m02E7843DEC68FBDDCA2B009E905FE4657C2B04AC (void);
// 0x0000001A System.Void Firebase.FutureBase::Dispose()
extern void FutureBase_Dispose_m2C0FDC1F8EF2499A1E52D6CFEA94348388784BDB (void);
// 0x0000001B System.Void Firebase.FutureBase::Dispose(System.Boolean)
extern void FutureBase_Dispose_mD92D3FE1E216E3FFBE40723A1F3871452931B2AB (void);
// 0x0000001C Firebase.FutureStatus Firebase.FutureBase::status()
extern void FutureBase_status_m478C1E6AF62FB15C218A7C422CF5DC8CA1486CAA (void);
// 0x0000001D System.Int32 Firebase.FutureBase::error()
extern void FutureBase_error_mBA8200B272D3DB91D1EE78ECE0A10AAB84771C03 (void);
// 0x0000001E System.String Firebase.FutureBase::error_message()
extern void FutureBase_error_message_m6E9B30EF5EC5EE999B91077E60E3B96978DE4774 (void);
// 0x0000001F System.Void Firebase.StringStringMap::.ctor(System.IntPtr,System.Boolean)
extern void StringStringMap__ctor_mB3137F09FFB1EC3F5621EA75DBBEF82F9487D366 (void);
// 0x00000020 System.Runtime.InteropServices.HandleRef Firebase.StringStringMap::getCPtr(Firebase.StringStringMap)
extern void StringStringMap_getCPtr_mFE16C8CAD699AA366EA3D0A81EDFB984471F1AC8 (void);
// 0x00000021 System.Void Firebase.StringStringMap::Finalize()
extern void StringStringMap_Finalize_m54B7DA5EEEDF3E469B05EC36A6FCE520DE2AF925 (void);
// 0x00000022 System.Void Firebase.StringStringMap::Dispose()
extern void StringStringMap_Dispose_mA58EFAC215289EDDB29347D7FB6EC3E9E84FADDF (void);
// 0x00000023 System.Void Firebase.StringStringMap::Dispose(System.Boolean)
extern void StringStringMap_Dispose_m33B2F2642A9D60E6A52283A65374B2A4BA868C1F (void);
// 0x00000024 System.String Firebase.StringStringMap::get_Item(System.String)
extern void StringStringMap_get_Item_mB384423BD033B98EE3457212BE65092712C56789 (void);
// 0x00000025 System.Void Firebase.StringStringMap::set_Item(System.String,System.String)
extern void StringStringMap_set_Item_m8A9BD489465331D5D800240D38CBB7965925F1F0 (void);
// 0x00000026 System.Boolean Firebase.StringStringMap::TryGetValue(System.String,System.String&)
extern void StringStringMap_TryGetValue_m6EF0C9A964D2D834925293A31A337070066BFE31 (void);
// 0x00000027 System.Int32 Firebase.StringStringMap::get_Count()
extern void StringStringMap_get_Count_m907D4DD769A90CAA0683591119FDC3728EEA5BB3 (void);
// 0x00000028 System.Boolean Firebase.StringStringMap::get_IsReadOnly()
extern void StringStringMap_get_IsReadOnly_m96FF99B82D36FD2E3BA738A31E98197987F9AA01 (void);
// 0x00000029 System.Collections.Generic.ICollection`1<System.String> Firebase.StringStringMap::get_Keys()
extern void StringStringMap_get_Keys_m913897400DEF518DFD7D6E0CCEB1FE2026A2B2D3 (void);
// 0x0000002A System.Collections.Generic.ICollection`1<System.String> Firebase.StringStringMap::get_Values()
extern void StringStringMap_get_Values_m56432463067868C092985B3D5A2066463C3FB7AC (void);
// 0x0000002B System.Void Firebase.StringStringMap::Add(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void StringStringMap_Add_m47B75C347EDA853CA95EFE263E2B645A0241D430 (void);
// 0x0000002C System.Boolean Firebase.StringStringMap::Remove(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void StringStringMap_Remove_m3443C8BF4781ADB1C64089E6DA44095D341329D2 (void);
// 0x0000002D System.Boolean Firebase.StringStringMap::Contains(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void StringStringMap_Contains_mE32677A31CE147FDD041B47D9D48914312DB8AF4 (void);
// 0x0000002E System.Void Firebase.StringStringMap::CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,System.String>[],System.Int32)
extern void StringStringMap_CopyTo_mD1D62132C4D6B43C716FBDD75A6628BD7DF618F8 (void);
// 0x0000002F System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Firebase.StringStringMap::global::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<System.String,System.String>>.GetEnumerator()
extern void StringStringMap_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_GetEnumerator_mC7628FC9F836BA4B0C341737ED653595000DAFD4 (void);
// 0x00000030 System.Collections.IEnumerator Firebase.StringStringMap::global::System.Collections.IEnumerable.GetEnumerator()
extern void StringStringMap_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_mE9D90D7B85A3208AE71B68D31C8BA56540BA8633 (void);
// 0x00000031 Firebase.StringStringMap/StringStringMapEnumerator Firebase.StringStringMap::GetEnumerator()
extern void StringStringMap_GetEnumerator_m55C288A948D94A096AD2FA8D042B8B816E6A6C9E (void);
// 0x00000032 System.Void Firebase.StringStringMap::.ctor()
extern void StringStringMap__ctor_mDD72D1A9A58D226FEA04CB062E9240FBF4B0949E (void);
// 0x00000033 System.UInt32 Firebase.StringStringMap::size()
extern void StringStringMap_size_mBB463BF4F9C6625F91343E1E8F38418A790F1D00 (void);
// 0x00000034 System.Void Firebase.StringStringMap::Clear()
extern void StringStringMap_Clear_mB1FFA2B7D6635E95F3A55805C39BE0AD3B14F21E (void);
// 0x00000035 System.String Firebase.StringStringMap::getitem(System.String)
extern void StringStringMap_getitem_m9E38CEC82B11AC188D75FA0BBB8A8751AC30D6D4 (void);
// 0x00000036 System.Void Firebase.StringStringMap::setitem(System.String,System.String)
extern void StringStringMap_setitem_m17A4FFDF71A27F385C53EC0C18EC02BE031CEE0F (void);
// 0x00000037 System.Boolean Firebase.StringStringMap::ContainsKey(System.String)
extern void StringStringMap_ContainsKey_mFF9F5FC22F5DF7900847009A2587F31967F11CCF (void);
// 0x00000038 System.Void Firebase.StringStringMap::Add(System.String,System.String)
extern void StringStringMap_Add_m9D2175007BBA9E8488671E9C13A08F247D5C2AED (void);
// 0x00000039 System.Boolean Firebase.StringStringMap::Remove(System.String)
extern void StringStringMap_Remove_m3E16E65DA11460219114402300F67FA76D87B408 (void);
// 0x0000003A System.IntPtr Firebase.StringStringMap::create_iterator_begin()
extern void StringStringMap_create_iterator_begin_m3F118062973941F86B02039418B2A2E5475F4377 (void);
// 0x0000003B System.String Firebase.StringStringMap::get_next_key(System.IntPtr)
extern void StringStringMap_get_next_key_m5748FA89B2C25838202E9E28D1DFB4FB8C33CE73 (void);
// 0x0000003C System.Void Firebase.StringStringMap::destroy_iterator(System.IntPtr)
extern void StringStringMap_destroy_iterator_m345F0B0BBC506B42F149223404B1A9F205232F87 (void);
// 0x0000003D System.Void Firebase.StringStringMap/StringStringMapEnumerator::.ctor(Firebase.StringStringMap)
extern void StringStringMapEnumerator__ctor_m765FC216E9A67EA91E8D360C32ED59B5EF59A43A (void);
// 0x0000003E System.Collections.Generic.KeyValuePair`2<System.String,System.String> Firebase.StringStringMap/StringStringMapEnumerator::get_Current()
extern void StringStringMapEnumerator_get_Current_m76DE586A735687200DC7ACF803FEF75527302F06 (void);
// 0x0000003F System.Object Firebase.StringStringMap/StringStringMapEnumerator::global::System.Collections.IEnumerator.get_Current()
extern void StringStringMapEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_m5AC18877BA31B175CFB4CE0564BD6D59B835207C (void);
// 0x00000040 System.Boolean Firebase.StringStringMap/StringStringMapEnumerator::MoveNext()
extern void StringStringMapEnumerator_MoveNext_m22BD0722C730E02430014E65174344AA499F336F (void);
// 0x00000041 System.Void Firebase.StringStringMap/StringStringMapEnumerator::Reset()
extern void StringStringMapEnumerator_Reset_m71559FB9DF7506B0DBD523E327E02E6B4405A84E (void);
// 0x00000042 System.Void Firebase.StringStringMap/StringStringMapEnumerator::Dispose()
extern void StringStringMapEnumerator_Dispose_m1B7713DE735B05E182D29A076ED15B7720E91BD6 (void);
// 0x00000043 System.Void Firebase.StringList::.ctor(System.IntPtr,System.Boolean)
extern void StringList__ctor_mE8B8481A314CC754CF22966A396EA63207F8A44B (void);
// 0x00000044 System.Runtime.InteropServices.HandleRef Firebase.StringList::getCPtr(Firebase.StringList)
extern void StringList_getCPtr_m6FC803782DE21FD97DF7F0DC43C55EFF974BC69F (void);
// 0x00000045 System.Void Firebase.StringList::Finalize()
extern void StringList_Finalize_m79827A0C46ECCAD9DB336D480B7AA54BB03B2B35 (void);
// 0x00000046 System.Void Firebase.StringList::Dispose()
extern void StringList_Dispose_mBA4E30F327D0B2581FED88BF87521AC290F26696 (void);
// 0x00000047 System.Void Firebase.StringList::Dispose(System.Boolean)
extern void StringList_Dispose_mC9E9D3DD3DB71D12C8CBCDA47783403F9C37AA2A (void);
// 0x00000048 System.Boolean Firebase.StringList::get_IsReadOnly()
extern void StringList_get_IsReadOnly_m9344BB3E297157D14A875E8595524AB59559516F (void);
// 0x00000049 System.String Firebase.StringList::get_Item(System.Int32)
extern void StringList_get_Item_mEBA85AB35A3F646D755A24FC0D24118B44224D89 (void);
// 0x0000004A System.Void Firebase.StringList::set_Item(System.Int32,System.String)
extern void StringList_set_Item_mA26263399D7DF77D487AE314B2131479403D6EC3 (void);
// 0x0000004B System.Int32 Firebase.StringList::get_Count()
extern void StringList_get_Count_m3D8D769FA302FCB31E5A157C0EABFFC1FE20D24F (void);
// 0x0000004C System.Void Firebase.StringList::CopyTo(System.String[],System.Int32)
extern void StringList_CopyTo_mD692F361A2C5F2D5B96C2633EA18825C9CE20D27 (void);
// 0x0000004D System.Void Firebase.StringList::CopyTo(System.Int32,System.String[],System.Int32,System.Int32)
extern void StringList_CopyTo_m0A3A36516421CD77E1C3FA1EC95BBF6385F391D8 (void);
// 0x0000004E System.Collections.Generic.IEnumerator`1<System.String> Firebase.StringList::global::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void StringList_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m9FDF2D5121BF290D240D703CC1C87EC274C029BB (void);
// 0x0000004F System.Collections.IEnumerator Firebase.StringList::global::System.Collections.IEnumerable.GetEnumerator()
extern void StringList_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_mA54DC2A36A7B9040510C3423852499ABE42BB84D (void);
// 0x00000050 System.Void Firebase.StringList::Clear()
extern void StringList_Clear_m82FB1CC4351B0ABF3D59C2684C26480D2A8C679B (void);
// 0x00000051 System.Void Firebase.StringList::Add(System.String)
extern void StringList_Add_m3CF7B8A5AF9295D001B02046D9C0C798968B0103 (void);
// 0x00000052 System.UInt32 Firebase.StringList::size()
extern void StringList_size_m26EC4497B274231ACC1D117E49C85E33D099F716 (void);
// 0x00000053 System.Void Firebase.StringList::.ctor()
extern void StringList__ctor_m95E70415E0B866B0620AF11568ADFD93E3DD112C (void);
// 0x00000054 System.String Firebase.StringList::getitemcopy(System.Int32)
extern void StringList_getitemcopy_mDC3C5EFA13D96E01F95E254D4065A2083DA83D67 (void);
// 0x00000055 System.String Firebase.StringList::getitem(System.Int32)
extern void StringList_getitem_mCAAFB8C789F633D75FF24D9CD534E835EA31716B (void);
// 0x00000056 System.Void Firebase.StringList::setitem(System.Int32,System.String)
extern void StringList_setitem_m3377D2A1A7CCCE23DC5B0BAE705F57F493C54E6B (void);
// 0x00000057 System.Void Firebase.StringList::Insert(System.Int32,System.String)
extern void StringList_Insert_mE01DFBF1964AF4D6F7163B440E4D1F47E4324D7E (void);
// 0x00000058 System.Void Firebase.StringList::RemoveAt(System.Int32)
extern void StringList_RemoveAt_mE300BDB9B78983E06CF3212E83878FC6E9599FFB (void);
// 0x00000059 System.Boolean Firebase.StringList::Contains(System.String)
extern void StringList_Contains_m45135739E8C495AA6A1D571EE54B299066A263FE (void);
// 0x0000005A System.Int32 Firebase.StringList::IndexOf(System.String)
extern void StringList_IndexOf_mD053F9A376F1C9968A74D6000CAA4A60A29B7217 (void);
// 0x0000005B System.Boolean Firebase.StringList::Remove(System.String)
extern void StringList_Remove_mD0348272B896C09C2128F4F049F7763E1D4E46B2 (void);
// 0x0000005C System.Void Firebase.StringList/StringListEnumerator::.ctor(Firebase.StringList)
extern void StringListEnumerator__ctor_m9C605A00C571C1D36C197B35104916E77E4E24F2 (void);
// 0x0000005D System.String Firebase.StringList/StringListEnumerator::get_Current()
extern void StringListEnumerator_get_Current_m9CA8E365AD4B78B9D17442CA31BC6CA0EBD585DF (void);
// 0x0000005E System.Object Firebase.StringList/StringListEnumerator::global::System.Collections.IEnumerator.get_Current()
extern void StringListEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_mA97F140652F707D1CC004E24C7BB902439C3B188 (void);
// 0x0000005F System.Boolean Firebase.StringList/StringListEnumerator::MoveNext()
extern void StringListEnumerator_MoveNext_m9D9BDA9A01A682ABEB2AAAE67A09A07390E936CA (void);
// 0x00000060 System.Void Firebase.StringList/StringListEnumerator::Reset()
extern void StringListEnumerator_Reset_mBE034B8BF2A4A5D10CB9920A2CDD498DF33E9FCD (void);
// 0x00000061 System.Void Firebase.StringList/StringListEnumerator::Dispose()
extern void StringListEnumerator_Dispose_m011212072122F01364D073B434712AAC5CC8A95E (void);
// 0x00000062 System.Void Firebase.AppOptionsInternal::.ctor(System.IntPtr,System.Boolean)
extern void AppOptionsInternal__ctor_mBA3D4999FA58E5D0E10F920846372ACD71CDEDD0 (void);
// 0x00000063 System.Void Firebase.AppOptionsInternal::Finalize()
extern void AppOptionsInternal_Finalize_m98E79714FD18F1FBD487CE442C7CAFCECFA572A5 (void);
// 0x00000064 System.Void Firebase.AppOptionsInternal::Dispose()
extern void AppOptionsInternal_Dispose_m923FE7958D4317ED8825DBAD4254B533825AEAA1 (void);
// 0x00000065 System.Void Firebase.AppOptionsInternal::Dispose(System.Boolean)
extern void AppOptionsInternal_Dispose_m7550240FA9B391FAA540F6B8087CA1C195CD25E9 (void);
// 0x00000066 System.String Firebase.AppOptionsInternal::get_ProjectId()
extern void AppOptionsInternal_get_ProjectId_m3FFDDF11A39C948719985A512855D7C9740143CB (void);
// 0x00000067 System.Void Firebase.FirebaseApp::.ctor(System.IntPtr,System.Boolean)
extern void FirebaseApp__ctor_mB94B4439B6474A9B3420A388F4C847153BB7DA2B (void);
// 0x00000068 System.Runtime.InteropServices.HandleRef Firebase.FirebaseApp::getCPtr(Firebase.FirebaseApp)
extern void FirebaseApp_getCPtr_mA17E448C13FB7553AF1914C14CBDC250F0CB2772 (void);
// 0x00000069 System.Void Firebase.FirebaseApp::Finalize()
extern void FirebaseApp_Finalize_mBA9B05FC454D571B021370352E3DAA24A927C964 (void);
// 0x0000006A System.Void Firebase.FirebaseApp::Dispose()
extern void FirebaseApp_Dispose_mD97452CFB97FC00105EB0369582537ED1C457A93 (void);
// 0x0000006B System.Void Firebase.FirebaseApp::Dispose(System.Boolean)
extern void FirebaseApp_Dispose_m05D2841662AD52390E0D09C5A81366D751FF8B38 (void);
// 0x0000006C System.Void Firebase.FirebaseApp::.cctor()
extern void FirebaseApp__cctor_m089DBE5321221ADCEBC17C3DB5057999D75C1042 (void);
// 0x0000006D System.Void Firebase.FirebaseApp::TranslateDllNotFoundException(System.Action)
extern void FirebaseApp_TranslateDllNotFoundException_mAC1EBE66BEE7869030ABDF79698C3F5B62FBCE6C (void);
// 0x0000006E Firebase.FirebaseApp Firebase.FirebaseApp::get_DefaultInstance()
extern void FirebaseApp_get_DefaultInstance_mBF6A8D928FEC9E3B18EFC29A85E8BBF62FCC70F0 (void);
// 0x0000006F Firebase.FirebaseApp Firebase.FirebaseApp::GetInstance(System.String)
extern void FirebaseApp_GetInstance_m754B7224D202231D11838329C0B141E243D06CBF (void);
// 0x00000070 Firebase.FirebaseApp Firebase.FirebaseApp::Create()
extern void FirebaseApp_Create_mB2276D7EAD55D961ECE48E140645007F1C72D98E (void);
// 0x00000071 System.String Firebase.FirebaseApp::get_Name()
extern void FirebaseApp_get_Name_m5945BBCED21D565E2D871D4CC00D03BB6EDB60B8 (void);
// 0x00000072 Firebase.LogLevel Firebase.FirebaseApp::get_LogLevel()
extern void FirebaseApp_get_LogLevel_m8D2BE127B0EFF6334588C426EDC67FDD1144E014 (void);
// 0x00000073 System.Void Firebase.FirebaseApp::add_AppDisposed(System.EventHandler)
extern void FirebaseApp_add_AppDisposed_m72EFA96351AC82CC8DF3A42ABEFB313B0D9E7D79 (void);
// 0x00000074 System.Void Firebase.FirebaseApp::remove_AppDisposed(System.EventHandler)
extern void FirebaseApp_remove_AppDisposed_m1E390E9068C2247AFF44122B61195EC3627F7812 (void);
// 0x00000075 System.Void Firebase.FirebaseApp::AddReference()
extern void FirebaseApp_AddReference_m1B148D579E5DBB9E6608813E714769757F33142B (void);
// 0x00000076 System.Void Firebase.FirebaseApp::RemoveReference()
extern void FirebaseApp_RemoveReference_mF1E15EE01A41D7674FAFDAB777CF637071F907E0 (void);
// 0x00000077 System.Void Firebase.FirebaseApp::ThrowIfNull()
extern void FirebaseApp_ThrowIfNull_m408BCFBB4ED87F4FF412F374897F083E87897315 (void);
// 0x00000078 System.Void Firebase.FirebaseApp::InitializeAppUtilCallbacks()
extern void FirebaseApp_InitializeAppUtilCallbacks_mF98F56F2513D3FEEA93D059CE57F13721D1E3617 (void);
// 0x00000079 System.Void Firebase.FirebaseApp::OnAllAppsDestroyed()
extern void FirebaseApp_OnAllAppsDestroyed_m1CCBDEDF6290B544DE085AAC017D94A606547B5F (void);
// 0x0000007A System.Boolean Firebase.FirebaseApp::InitializeCrashlyticsIfPresent()
extern void FirebaseApp_InitializeCrashlyticsIfPresent_mE36628943ED00C5D03C2AD561B6950E0CCE8C697 (void);
// 0x0000007B Firebase.FirebaseApp Firebase.FirebaseApp::CreateAndTrack(Firebase.FirebaseApp/CreateDelegate,Firebase.FirebaseApp)
extern void FirebaseApp_CreateAndTrack_mD755E5670398197F0E362BA3128AE66A81F061E9 (void);
// 0x0000007C System.Void Firebase.FirebaseApp::ThrowIfCheckDependenciesRunning()
extern void FirebaseApp_ThrowIfCheckDependenciesRunning_m4D5BB8F1A9121F7328BD9F3DF22AFAC97F62E441 (void);
// 0x0000007D System.Boolean Firebase.FirebaseApp::IsCheckDependenciesRunning()
extern void FirebaseApp_IsCheckDependenciesRunning_m7A31B8F0F36137615EF27844C0686EAAEA39AFC9 (void);
// 0x0000007E Firebase.AppOptionsInternal Firebase.FirebaseApp::options()
extern void FirebaseApp_options_m246546AF14ED87EA3EDB8095EA99C5AA2396E8B4 (void);
// 0x0000007F System.String Firebase.FirebaseApp::get_NameInternal()
extern void FirebaseApp_get_NameInternal_m89112573EBE5801863EA50B17FBAE20CBE855C75 (void);
// 0x00000080 Firebase.FirebaseApp Firebase.FirebaseApp::CreateInternal()
extern void FirebaseApp_CreateInternal_m701D4A79799B12C5532B0B12826943B58DFCADDD (void);
// 0x00000081 System.Void Firebase.FirebaseApp::ReleaseReferenceInternal(Firebase.FirebaseApp)
extern void FirebaseApp_ReleaseReferenceInternal_m695A4178B58AD3B445FCEB4567A414DBB66A4632 (void);
// 0x00000082 System.Void Firebase.FirebaseApp::RegisterLibrariesInternal(Firebase.StringStringMap)
extern void FirebaseApp_RegisterLibrariesInternal_m813089397FAF99BD61FFCB95C059F41B6DB32B20 (void);
// 0x00000083 System.Void Firebase.FirebaseApp::LogHeartbeatInternal(Firebase.FirebaseApp)
extern void FirebaseApp_LogHeartbeatInternal_m6C2088EDAD68B733D483AE0309073200FE3D68C9 (void);
// 0x00000084 System.Void Firebase.FirebaseApp::AppSetDefaultConfigPath(System.String)
extern void FirebaseApp_AppSetDefaultConfigPath_m54AE67D90C3392F1884FDD05F1EF8DCC600080F9 (void);
// 0x00000085 System.String Firebase.FirebaseApp::get_DefaultName()
extern void FirebaseApp_get_DefaultName_m6F7C0AE32C0E880830129BFBC914E23F412E7CAF (void);
// 0x00000086 System.String Firebase.FirebaseApp/EnableModuleParams::get_CppModuleName()
extern void EnableModuleParams_get_CppModuleName_mE57521DAC3F8972C81AFBC72DB70FE79A9F946B3 (void);
// 0x00000087 System.Void Firebase.FirebaseApp/EnableModuleParams::set_CppModuleName(System.String)
extern void EnableModuleParams_set_CppModuleName_m780B77AD33765B83D0675C02876BB379B9EFCCEB (void);
// 0x00000088 System.String Firebase.FirebaseApp/EnableModuleParams::get_CSharpClassName()
extern void EnableModuleParams_get_CSharpClassName_m5C21BC47A020FE24984E1A282267CE62CF09080B (void);
// 0x00000089 System.Void Firebase.FirebaseApp/EnableModuleParams::set_CSharpClassName(System.String)
extern void EnableModuleParams_set_CSharpClassName_mB1413BCF93E8A3B658798ED556E586C47981F018 (void);
// 0x0000008A System.Boolean Firebase.FirebaseApp/EnableModuleParams::get_AlwaysEnable()
extern void EnableModuleParams_get_AlwaysEnable_m76B3B18100019E68E79EA0A0B320B1EAE0AB8260 (void);
// 0x0000008B System.Void Firebase.FirebaseApp/EnableModuleParams::set_AlwaysEnable(System.Boolean)
extern void EnableModuleParams_set_AlwaysEnable_m38C379905DD5810F629E35AD34DD0F677990ACFE (void);
// 0x0000008C System.Void Firebase.FirebaseApp/EnableModuleParams::.ctor(System.String,System.String,System.Boolean)
extern void EnableModuleParams__ctor_mC9619ED6B8BE82D305359BB53BDE6274A8A2D2C2 (void);
// 0x0000008D System.Void Firebase.FirebaseApp/CreateDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateDelegate__ctor_m9B61AF9F4EFF9CCA9FC10B8BDB5E8AD7130E4DE1 (void);
// 0x0000008E Firebase.FirebaseApp Firebase.FirebaseApp/CreateDelegate::Invoke()
extern void CreateDelegate_Invoke_m9FC551133A4F9301FB4F107B90F7C98A66F95BE9 (void);
// 0x0000008F System.IAsyncResult Firebase.FirebaseApp/CreateDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void CreateDelegate_BeginInvoke_mE55A9209FBD926992A08A1F0C0DCAC9083CC66A5 (void);
// 0x00000090 Firebase.FirebaseApp Firebase.FirebaseApp/CreateDelegate::EndInvoke(System.IAsyncResult)
extern void CreateDelegate_EndInvoke_mA8FE3700C59A150FAA37B17BDAA7A44319C1116E (void);
// 0x00000091 System.Void Firebase.FirebaseApp/<>c::.cctor()
extern void U3CU3Ec__cctor_m6CEB22FFC45F6E85A173A787465FCDE4EBA19206 (void);
// 0x00000092 System.Void Firebase.FirebaseApp/<>c::.ctor()
extern void U3CU3Ec__ctor_m58F4F61972161EC4DA21A6B10024C4D7C71DC42A (void);
// 0x00000093 Firebase.FirebaseApp Firebase.FirebaseApp/<>c::<Create>b__15_0()
extern void U3CU3Ec_U3CCreateU3Eb__15_0_m88AD2B39B4B74F4D45D16198A1910301A7255079 (void);
// 0x00000094 System.Boolean Firebase.FirebaseApp/<>c::<CreateAndTrack>b__48_0()
extern void U3CU3Ec_U3CCreateAndTrackU3Eb__48_0_m0F69C58EFA46937CB73BD5245B64D5F8C18D0C17 (void);
// 0x00000095 System.Void Firebase.AppUtilPINVOKE::.cctor()
extern void AppUtilPINVOKE__cctor_mFD78B877244B4AE4B4ECB1A5F1A90986222A7B1B (void);
// 0x00000096 System.Void Firebase.AppUtilPINVOKE::delete_FutureBase(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_delete_FutureBase_m32A337F75D98D136CA48473B07F0D041350A3ABC (void);
// 0x00000097 System.Int32 Firebase.AppUtilPINVOKE::FutureBase_status(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FutureBase_status_m92C9BDAD44005F5776E7B8AB3A831A66415E997B (void);
// 0x00000098 System.Int32 Firebase.AppUtilPINVOKE::FutureBase_error(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FutureBase_error_m3811E5F983BB73714C62724B57DC5DF4DCAC13CA (void);
// 0x00000099 System.String Firebase.AppUtilPINVOKE::FutureBase_error_message(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FutureBase_error_message_m158A38CCC09A2363F58865F3FF7BA538C6D50625 (void);
// 0x0000009A System.IntPtr Firebase.AppUtilPINVOKE::new_StringStringMap__SWIG_0()
extern void AppUtilPINVOKE_new_StringStringMap__SWIG_0_mCE10310D4C42EC884A82A69FD9295A658E405EAF (void);
// 0x0000009B System.UInt32 Firebase.AppUtilPINVOKE::StringStringMap_size(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_StringStringMap_size_m064A6F37DB06E96A55C0BA8B5B16B5DBD6572418 (void);
// 0x0000009C System.Void Firebase.AppUtilPINVOKE::StringStringMap_Clear(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_StringStringMap_Clear_m6AED672F614EC3F031249C51053917AC20F870C6 (void);
// 0x0000009D System.String Firebase.AppUtilPINVOKE::StringStringMap_getitem(System.Runtime.InteropServices.HandleRef,System.String)
extern void AppUtilPINVOKE_StringStringMap_getitem_m4B9D9EB3CC3EA41090CEE9A8E41C7E9530119F33 (void);
// 0x0000009E System.Void Firebase.AppUtilPINVOKE::StringStringMap_setitem(System.Runtime.InteropServices.HandleRef,System.String,System.String)
extern void AppUtilPINVOKE_StringStringMap_setitem_m5E8DA70BC3CA15BE84AA72CF2E2F2F8C5311E8EC (void);
// 0x0000009F System.Boolean Firebase.AppUtilPINVOKE::StringStringMap_ContainsKey(System.Runtime.InteropServices.HandleRef,System.String)
extern void AppUtilPINVOKE_StringStringMap_ContainsKey_m96BD9C32FFEB55C52DBC79128234E8158A90157D (void);
// 0x000000A0 System.Void Firebase.AppUtilPINVOKE::StringStringMap_Add(System.Runtime.InteropServices.HandleRef,System.String,System.String)
extern void AppUtilPINVOKE_StringStringMap_Add_m33D3FF6AAD8725197ED64C2388C1837E04A91DF4 (void);
// 0x000000A1 System.Boolean Firebase.AppUtilPINVOKE::StringStringMap_Remove(System.Runtime.InteropServices.HandleRef,System.String)
extern void AppUtilPINVOKE_StringStringMap_Remove_m3E0CFC2194307512821D54D2E377BF1411FAE308 (void);
// 0x000000A2 System.IntPtr Firebase.AppUtilPINVOKE::StringStringMap_create_iterator_begin(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_StringStringMap_create_iterator_begin_m4A7CEFD5594BF712ADF477B7D6CE847145D7077B (void);
// 0x000000A3 System.String Firebase.AppUtilPINVOKE::StringStringMap_get_next_key(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void AppUtilPINVOKE_StringStringMap_get_next_key_mD42451EE4AE529A4345694AC9E2545B781E3CFC5 (void);
// 0x000000A4 System.Void Firebase.AppUtilPINVOKE::StringStringMap_destroy_iterator(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void AppUtilPINVOKE_StringStringMap_destroy_iterator_m181F8DFB09848A11C80AA806A81FFC93840EF756 (void);
// 0x000000A5 System.Void Firebase.AppUtilPINVOKE::delete_StringStringMap(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_delete_StringStringMap_m52D99E016CC34299C1B789C9E0121EC5A8FF9F64 (void);
// 0x000000A6 System.Void Firebase.AppUtilPINVOKE::StringList_Clear(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_StringList_Clear_m95F9B093FC1E3ED61D41A9B04E041AE36EAEC3CF (void);
// 0x000000A7 System.Void Firebase.AppUtilPINVOKE::StringList_Add(System.Runtime.InteropServices.HandleRef,System.String)
extern void AppUtilPINVOKE_StringList_Add_m86712B6D21BEF4BD37B2C168B66DD726974E233A (void);
// 0x000000A8 System.UInt32 Firebase.AppUtilPINVOKE::StringList_size(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_StringList_size_m6DAD77BDC909994D88328C4D1FDEBFCD4C186481 (void);
// 0x000000A9 System.IntPtr Firebase.AppUtilPINVOKE::new_StringList__SWIG_0()
extern void AppUtilPINVOKE_new_StringList__SWIG_0_m311EE8221AF25B96E1C3F30C07C7B83519ADBF5F (void);
// 0x000000AA System.String Firebase.AppUtilPINVOKE::StringList_getitemcopy(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void AppUtilPINVOKE_StringList_getitemcopy_mE737BDBCF5A6AB012DB3CC8EBCDBCB2B05CBDB8E (void);
// 0x000000AB System.String Firebase.AppUtilPINVOKE::StringList_getitem(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void AppUtilPINVOKE_StringList_getitem_m8C7B0FEC3E53F4C0C7D20B005FF75CCCFA592B99 (void);
// 0x000000AC System.Void Firebase.AppUtilPINVOKE::StringList_setitem(System.Runtime.InteropServices.HandleRef,System.Int32,System.String)
extern void AppUtilPINVOKE_StringList_setitem_m45D872D5D3F584E9DAA7C4ABE119BBA6E697C0CA (void);
// 0x000000AD System.Void Firebase.AppUtilPINVOKE::StringList_Insert(System.Runtime.InteropServices.HandleRef,System.Int32,System.String)
extern void AppUtilPINVOKE_StringList_Insert_m980FEBD0EA50E405B21C93DAD5491149D23F3B86 (void);
// 0x000000AE System.Void Firebase.AppUtilPINVOKE::StringList_RemoveAt(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void AppUtilPINVOKE_StringList_RemoveAt_m4E4D7B4908AF7A612E59BB909735F3497EC012F3 (void);
// 0x000000AF System.Boolean Firebase.AppUtilPINVOKE::StringList_Contains(System.Runtime.InteropServices.HandleRef,System.String)
extern void AppUtilPINVOKE_StringList_Contains_m3A4DF6B80C378203111CBB0B2BB0FE12D43F6071 (void);
// 0x000000B0 System.Int32 Firebase.AppUtilPINVOKE::StringList_IndexOf(System.Runtime.InteropServices.HandleRef,System.String)
extern void AppUtilPINVOKE_StringList_IndexOf_m459DFE45808E2EDEF4599FBAC87F8099FB4F65E8 (void);
// 0x000000B1 System.Boolean Firebase.AppUtilPINVOKE::StringList_Remove(System.Runtime.InteropServices.HandleRef,System.String)
extern void AppUtilPINVOKE_StringList_Remove_mE63FB40C5BEAEA814A59F7ECEB4E0911F3DEDDFA (void);
// 0x000000B2 System.Void Firebase.AppUtilPINVOKE::delete_StringList(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_delete_StringList_m25578115258E3E3987F3B98CB5AD97729F00A37F (void);
// 0x000000B3 System.String Firebase.AppUtilPINVOKE::AppOptionsInternal_ProjectId_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_AppOptionsInternal_ProjectId_get_m443BAE7C78EA890A59DA4BBBC482C3759EE7C4AD (void);
// 0x000000B4 System.Void Firebase.AppUtilPINVOKE::delete_AppOptionsInternal(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_delete_AppOptionsInternal_m2989EEA9A280DBA5CF7BA1DD494D7075261E66B6 (void);
// 0x000000B5 System.IntPtr Firebase.AppUtilPINVOKE::FirebaseApp_options(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FirebaseApp_options_mC39783D9DFE182B5266FB5FCEAC12C0B99DA21BC (void);
// 0x000000B6 System.String Firebase.AppUtilPINVOKE::FirebaseApp_NameInternal_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FirebaseApp_NameInternal_get_m12A197E54EC181A89A1A954E3F1178D3778CF9D6 (void);
// 0x000000B7 System.IntPtr Firebase.AppUtilPINVOKE::FirebaseApp_CreateInternal__SWIG_0()
extern void AppUtilPINVOKE_FirebaseApp_CreateInternal__SWIG_0_m6DA2C8A7648278BD0C312C5EFE387E3F6C10C0C7 (void);
// 0x000000B8 System.Void Firebase.AppUtilPINVOKE::FirebaseApp_ReleaseReferenceInternal(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FirebaseApp_ReleaseReferenceInternal_mC8A038946726600A71193DFC3F7489B3E5821932 (void);
// 0x000000B9 System.Int32 Firebase.AppUtilPINVOKE::FirebaseApp_GetLogLevelInternal()
extern void AppUtilPINVOKE_FirebaseApp_GetLogLevelInternal_m796D5D97039485FC06B5F705E8A0E063A2AC238B (void);
// 0x000000BA System.Void Firebase.AppUtilPINVOKE::FirebaseApp_RegisterLibrariesInternal(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FirebaseApp_RegisterLibrariesInternal_m02CDD2FB7B903F6E3801ED0908CB9DFA214F8EC2 (void);
// 0x000000BB System.Void Firebase.AppUtilPINVOKE::FirebaseApp_LogHeartbeatInternal(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FirebaseApp_LogHeartbeatInternal_m9B9F4E51812EBC89CECB543E3E2E31BB5503A990 (void);
// 0x000000BC System.Void Firebase.AppUtilPINVOKE::FirebaseApp_AppSetDefaultConfigPath(System.String)
extern void AppUtilPINVOKE_FirebaseApp_AppSetDefaultConfigPath_mCFC987839BF745850B0EE9D337B6D88BAD4D955D (void);
// 0x000000BD System.String Firebase.AppUtilPINVOKE::FirebaseApp_DefaultName_get()
extern void AppUtilPINVOKE_FirebaseApp_DefaultName_get_m89727C45DF2C2E541190B80B3C4C51F79A40AC75 (void);
// 0x000000BE System.Void Firebase.AppUtilPINVOKE::PollCallbacks()
extern void AppUtilPINVOKE_PollCallbacks_m854E2979EC7E9B1109222D67FC42A947C957BDD6 (void);
// 0x000000BF System.Void Firebase.AppUtilPINVOKE::AppEnableLogCallback(System.Boolean)
extern void AppUtilPINVOKE_AppEnableLogCallback_m77EF6A5E3B6980BB0FC27D7073DF37C2EB601DF7 (void);
// 0x000000C0 System.Void Firebase.AppUtilPINVOKE::SetEnabledAllAppCallbacks(System.Boolean)
extern void AppUtilPINVOKE_SetEnabledAllAppCallbacks_m6732FA8A6DB8BA6AD9F155025C4DC82AED5D4DC4 (void);
// 0x000000C1 System.Void Firebase.AppUtilPINVOKE::SetEnabledAppCallbackByName(System.String,System.Boolean)
extern void AppUtilPINVOKE_SetEnabledAppCallbackByName_mFA4EF5EB8A51F10D3CFAF1FA431B086ACB65FE20 (void);
// 0x000000C2 System.Boolean Firebase.AppUtilPINVOKE::GetEnabledAppCallbackByName(System.String)
extern void AppUtilPINVOKE_GetEnabledAppCallbackByName_m66EFD9115DE665251D4B0579AACEB7741770702C (void);
// 0x000000C3 System.Void Firebase.AppUtilPINVOKE::SetLogFunction(Firebase.LogUtil/LogMessageDelegate)
extern void AppUtilPINVOKE_SetLogFunction_m5DFEC25E7A372B96EF4BB908488CBD68202BA656 (void);
// 0x000000C4 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AppUtil_m1CD8200ED7114166960CCBC6B6A300E962EAF9A6 (void);
// 0x000000C5 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AppUtil_mC2B5A2343D9A5417117D7727F225C77E413FBE61 (void);
// 0x000000C6 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern void SWIGExceptionHelper_SetPendingApplicationException_m0829F0837975040087642D6A0EF77DE0F908FCEE (void);
// 0x000000C7 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern void SWIGExceptionHelper_SetPendingArithmeticException_m43E86638D478FE1109B49BB4F8844EF4EDD34414 (void);
// 0x000000C8 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern void SWIGExceptionHelper_SetPendingDivideByZeroException_m2B34635F25839BAFD46740D0964E237A1582578B (void);
// 0x000000C9 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m7BC82194E760DFF75E2F56936BC89321263F7253 (void);
// 0x000000CA System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidCastException_m71BAB12378AF0F55B0C013BB0146B9A0AFD0B3DF (void);
// 0x000000CB System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidOperationException_m782571C94E167C84E015BA1F883EB335A1719E45 (void);
// 0x000000CC System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
extern void SWIGExceptionHelper_SetPendingIOException_m024FB3DF66F596B41BAD3EB701F4DED5057FE49C (void);
// 0x000000CD System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern void SWIGExceptionHelper_SetPendingNullReferenceException_mEA2CE40C44CD6C5FE044BAB6FFA434AC5CCF4E45 (void);
// 0x000000CE System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern void SWIGExceptionHelper_SetPendingOutOfMemoryException_m72E3606F191DCDDEF234BAAF117CC916E1CBD5BC (void);
// 0x000000CF System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern void SWIGExceptionHelper_SetPendingOverflowException_m8CD04C3E8A76FACEA1FFC6A074090902A335BD85 (void);
// 0x000000D0 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
extern void SWIGExceptionHelper_SetPendingSystemException_m7BB7BA403702277AEB0FD8DEB51798845CCBD063 (void);
// 0x000000D1 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentException_mB89C3689EC192CAD485B74904289BC70D393B9B3 (void);
// 0x000000D2 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentNullException_mE67B9DA5FB8088FD758246C75CACB5A5521B5D26 (void);
// 0x000000D3 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3B25880532D77834352308A7841325D00B402D4C (void);
// 0x000000D4 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::.cctor()
extern void SWIGExceptionHelper__cctor_m7B61D49340AC58F5296E054755FED58DF220EF06 (void);
// 0x000000D5 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::.ctor()
extern void SWIGExceptionHelper__ctor_m06C48C4611CDA458CA1AF651ED06BF7FF7EDF536 (void);
// 0x000000D6 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionDelegate__ctor_m4E04BD56501AA698F333F3189D232E0DD8BE66A0 (void);
// 0x000000D7 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
extern void ExceptionDelegate_Invoke_mE907915DC5B6A911DE7F253DF0E0D82F63B23A06 (void);
// 0x000000D8 System.IAsyncResult Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ExceptionDelegate_BeginInvoke_m72D31AEE58624296E481B8F6C28EDF28C445F92B (void);
// 0x000000D9 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::EndInvoke(System.IAsyncResult)
extern void ExceptionDelegate_EndInvoke_mCD778A944D0755D6227785C17547B6F3FCCB9D59 (void);
// 0x000000DA System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionArgumentDelegate__ctor_m9B64B0E9472C1DDAA639843324FD57FBCCE07E08 (void);
// 0x000000DB System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern void ExceptionArgumentDelegate_Invoke_mD10622418D792C1CDA2D02B0117C251187C52D74 (void);
// 0x000000DC System.IAsyncResult Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void ExceptionArgumentDelegate_BeginInvoke_m0410594AB6ABF10A9740F06B324A5A6C059E39B9 (void);
// 0x000000DD System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::EndInvoke(System.IAsyncResult)
extern void ExceptionArgumentDelegate_EndInvoke_mE24172C5085232AD4E4A4EAC36FBF77A79A93C31 (void);
// 0x000000DE System.Boolean Firebase.AppUtilPINVOKE/SWIGPendingException::get_Pending()
extern void SWIGPendingException_get_Pending_m71433EDDEE96D742319A495AD3BCE593564E61FB (void);
// 0x000000DF System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::Set(System.Exception)
extern void SWIGPendingException_Set_m68BF3F45B3D611F6908B1CE30A6C73DA2B5A6D56 (void);
// 0x000000E0 System.Exception Firebase.AppUtilPINVOKE/SWIGPendingException::Retrieve()
extern void SWIGPendingException_Retrieve_m024B64F5669D05A7C47EF731D60755C70D1D5FE6 (void);
// 0x000000E1 System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::.cctor()
extern void SWIGPendingException__cctor_m5A9E40C6C867D7C2D46025ECF58EE3DF714A57ED (void);
// 0x000000E2 System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_AppUtil(Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern void SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_mE1F3702E085E5899748BE17BA0D1E4505AA33EA1 (void);
// 0x000000E3 System.String Firebase.AppUtilPINVOKE/SWIGStringHelper::CreateString(System.String)
extern void SWIGStringHelper_CreateString_mFB66507B0E5F5D74116BC8CADAC859CD8614D642 (void);
// 0x000000E4 System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.cctor()
extern void SWIGStringHelper__cctor_mE822684D4A43792B6C07FFD6C8BE6A858BE9AB61 (void);
// 0x000000E5 System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.ctor()
extern void SWIGStringHelper__ctor_m9F305BAB06F185B49FD5AC05A407928C69D672F6 (void);
// 0x000000E6 System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIGStringDelegate__ctor_mED39AF7AB0675F58D7C5E732BB50C419BF321299 (void);
// 0x000000E7 System.String Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
extern void SWIGStringDelegate_Invoke_mE2D5B14F87E5528B7095C2B08CFD4B10A4926BDF (void);
// 0x000000E8 System.IAsyncResult Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void SWIGStringDelegate_BeginInvoke_m071087F52EF4FF4FB5914D2D986CD4607DFDE8E7 (void);
// 0x000000E9 System.String Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::EndInvoke(System.IAsyncResult)
extern void SWIGStringDelegate_EndInvoke_m4A38AD7D0C3965603592F826BC582D78A7FB7AA0 (void);
// 0x000000EA System.Void Firebase.AppUtil::PollCallbacks()
extern void AppUtil_PollCallbacks_m54B425D6913C258E717CAA36D9C649BDFDAF138C (void);
// 0x000000EB System.Void Firebase.AppUtil::AppEnableLogCallback(System.Boolean)
extern void AppUtil_AppEnableLogCallback_m5130BB1ADDCE046948126A13C715E71DBC01AB6E (void);
// 0x000000EC System.Void Firebase.AppUtil::SetEnabledAllAppCallbacks(System.Boolean)
extern void AppUtil_SetEnabledAllAppCallbacks_mD8D5DDABD6E4E3E1505CF38EFD9171828D2B7FB4 (void);
// 0x000000ED System.Void Firebase.AppUtil::SetEnabledAppCallbackByName(System.String,System.Boolean)
extern void AppUtil_SetEnabledAppCallbackByName_mB57B6366CBE1D055F849AC6E44AB3B14C580956C (void);
// 0x000000EE System.Boolean Firebase.AppUtil::GetEnabledAppCallbackByName(System.String)
extern void AppUtil_GetEnabledAppCallbackByName_m9DB078FC33838160DD0C051DD16B9E4FDB4A016D (void);
// 0x000000EF System.Void Firebase.AppUtil::SetLogFunction(Firebase.LogUtil/LogMessageDelegate)
extern void AppUtil_SetLogFunction_mFD8CE5F344A4BAC2B5BF19537651D25A171A5A28 (void);
// 0x000000F0 System.String Firebase.VersionInfo::get_SdkVersion()
extern void VersionInfo_get_SdkVersion_m36955800253C856C17C25993EA80CD1EE9E58EEB (void);
// 0x000000F1 System.String Firebase.VersionInfo::get_BuildSource()
extern void VersionInfo_get_BuildSource_mFA66DD38308E651A428C323254007E6ECFD7A988 (void);
// 0x000000F2 Firebase.Platform.FirebaseAppUtils Firebase.Platform.FirebaseAppUtils::get_Instance()
extern void FirebaseAppUtils_get_Instance_m479C17BD7ED35574386FD00136C22CF2D90AF61D (void);
// 0x000000F3 System.Void Firebase.Platform.FirebaseAppUtils::TranslateDllNotFoundException(System.Action)
extern void FirebaseAppUtils_TranslateDllNotFoundException_m6E4E3109BD0827EE0E675EE9CD50E9C6EE343356 (void);
// 0x000000F4 System.Void Firebase.Platform.FirebaseAppUtils::PollCallbacks()
extern void FirebaseAppUtils_PollCallbacks_m0A88A069EC5477A1CC9BFCEDC55DED431EE64E91 (void);
// 0x000000F5 Firebase.Platform.PlatformLogLevel Firebase.Platform.FirebaseAppUtils::GetLogLevel()
extern void FirebaseAppUtils_GetLogLevel_m9E036F13631BCE81463AA50592FBCD54E9CB09D7 (void);
// 0x000000F6 System.Void Firebase.Platform.FirebaseAppUtils::.ctor()
extern void FirebaseAppUtils__ctor_m69CA1CBCD58CB3128DF35E9560A3D1D38005845F (void);
// 0x000000F7 System.Void Firebase.Platform.FirebaseAppUtils::.cctor()
extern void FirebaseAppUtils__cctor_mAF324E88E10F8B1214A8A5CDFB73794B99517B03 (void);
static Il2CppMethodPointer s_methodPointers[247] = 
{
	FirebaseException__ctor_mCB919E722DF4F366C4E0D9278716CBED5DD8907C,
	FirebaseException_set_ErrorCode_mE216C4C0EBAACC7ADA04ED328DEC1474680F9B5A,
	InitializationException_set_InitResult_mD6E68B41830F64CB54B3BF7FFE915263D8AD8E34,
	InitializationException__ctor_m4F5649529A9F0863B359E63E74F8B5331F138A14,
	InitializationException__ctor_m56641135A502F6D70F771A69157A0D674EB99DD3,
	ErrorMessages_get_DependencyNotFoundErrorMessage_mB1677E85E4CFF5FE580A09B706B1EF9EFBB05DDE,
	ErrorMessages_get_DllNotFoundExceptionErrorMessage_mFF21A563C38214BE449D6115FCE52DD4E94033F0,
	ErrorMessages__cctor_m936EA2AD91AAD13B58BCE6D60861C6936D6EF963,
	LogUtil__cctor_mBB82D51ADECA24464B7C848D04E6DD047A5FBEA7,
	LogUtil_InitializeLogging_m0B74F188359DDD87A4870321573D37C4593AF8CB,
	LogUtil_ConvertLogLevel_m84040323CAAA7E32FE4B104B7042D7254D55BD2E,
	LogUtil_LogMessage_mB9917DEE02B6375ED692EE4FF48F04F56004D135,
	LogUtil_LogMessageFromCallback_m04F3C7908BCA7A583B183A9551387E5F04B21994,
	LogUtil__ctor_mE6F41CDC7EFF92D76E3D07B5F8350BF7D5A4983D,
	LogUtil_Finalize_mD288B9870DD8D7AF744044513FCA62F9AB42A85C,
	LogUtil_Dispose_mB12D003420083CAA79A613F44A635DA5418C989D,
	LogUtil_Dispose_m3E431D1105B6EBDC3183FA2B55AA7608607BBC6D,
	LogUtil_U3C_ctorU3Eb__9_0_m3CCEC7958C25A61458C58F678BCD77AA29A551B6,
	LogMessageDelegate__ctor_mEBA3FFB53CCE522DBB1B5571A5623A649E6643F0,
	LogMessageDelegate_Invoke_mB54C38843065556AF65D1E42C9DDC9AFAFA5C5E8,
	LogMessageDelegate_BeginInvoke_m9A9B00026484A1266F2E8E1101699C83D2755654,
	LogMessageDelegate_EndInvoke_m13C568B3D481DF381F37AAEF2FA598025FDB8C95,
	MonoPInvokeCallbackAttribute__ctor_mD5BA102663DCE67244B79EF374E5641E765AB5CB,
	FutureBase__ctor_m69C88EC69B422C5752B2E249303D92F649B8C8AC,
	FutureBase_Finalize_m02E7843DEC68FBDDCA2B009E905FE4657C2B04AC,
	FutureBase_Dispose_m2C0FDC1F8EF2499A1E52D6CFEA94348388784BDB,
	FutureBase_Dispose_mD92D3FE1E216E3FFBE40723A1F3871452931B2AB,
	FutureBase_status_m478C1E6AF62FB15C218A7C422CF5DC8CA1486CAA,
	FutureBase_error_mBA8200B272D3DB91D1EE78ECE0A10AAB84771C03,
	FutureBase_error_message_m6E9B30EF5EC5EE999B91077E60E3B96978DE4774,
	StringStringMap__ctor_mB3137F09FFB1EC3F5621EA75DBBEF82F9487D366,
	StringStringMap_getCPtr_mFE16C8CAD699AA366EA3D0A81EDFB984471F1AC8,
	StringStringMap_Finalize_m54B7DA5EEEDF3E469B05EC36A6FCE520DE2AF925,
	StringStringMap_Dispose_mA58EFAC215289EDDB29347D7FB6EC3E9E84FADDF,
	StringStringMap_Dispose_m33B2F2642A9D60E6A52283A65374B2A4BA868C1F,
	StringStringMap_get_Item_mB384423BD033B98EE3457212BE65092712C56789,
	StringStringMap_set_Item_m8A9BD489465331D5D800240D38CBB7965925F1F0,
	StringStringMap_TryGetValue_m6EF0C9A964D2D834925293A31A337070066BFE31,
	StringStringMap_get_Count_m907D4DD769A90CAA0683591119FDC3728EEA5BB3,
	StringStringMap_get_IsReadOnly_m96FF99B82D36FD2E3BA738A31E98197987F9AA01,
	StringStringMap_get_Keys_m913897400DEF518DFD7D6E0CCEB1FE2026A2B2D3,
	StringStringMap_get_Values_m56432463067868C092985B3D5A2066463C3FB7AC,
	StringStringMap_Add_m47B75C347EDA853CA95EFE263E2B645A0241D430,
	StringStringMap_Remove_m3443C8BF4781ADB1C64089E6DA44095D341329D2,
	StringStringMap_Contains_mE32677A31CE147FDD041B47D9D48914312DB8AF4,
	StringStringMap_CopyTo_mD1D62132C4D6B43C716FBDD75A6628BD7DF618F8,
	StringStringMap_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_GetEnumerator_mC7628FC9F836BA4B0C341737ED653595000DAFD4,
	StringStringMap_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_mE9D90D7B85A3208AE71B68D31C8BA56540BA8633,
	StringStringMap_GetEnumerator_m55C288A948D94A096AD2FA8D042B8B816E6A6C9E,
	StringStringMap__ctor_mDD72D1A9A58D226FEA04CB062E9240FBF4B0949E,
	StringStringMap_size_mBB463BF4F9C6625F91343E1E8F38418A790F1D00,
	StringStringMap_Clear_mB1FFA2B7D6635E95F3A55805C39BE0AD3B14F21E,
	StringStringMap_getitem_m9E38CEC82B11AC188D75FA0BBB8A8751AC30D6D4,
	StringStringMap_setitem_m17A4FFDF71A27F385C53EC0C18EC02BE031CEE0F,
	StringStringMap_ContainsKey_mFF9F5FC22F5DF7900847009A2587F31967F11CCF,
	StringStringMap_Add_m9D2175007BBA9E8488671E9C13A08F247D5C2AED,
	StringStringMap_Remove_m3E16E65DA11460219114402300F67FA76D87B408,
	StringStringMap_create_iterator_begin_m3F118062973941F86B02039418B2A2E5475F4377,
	StringStringMap_get_next_key_m5748FA89B2C25838202E9E28D1DFB4FB8C33CE73,
	StringStringMap_destroy_iterator_m345F0B0BBC506B42F149223404B1A9F205232F87,
	StringStringMapEnumerator__ctor_m765FC216E9A67EA91E8D360C32ED59B5EF59A43A,
	StringStringMapEnumerator_get_Current_m76DE586A735687200DC7ACF803FEF75527302F06,
	StringStringMapEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_m5AC18877BA31B175CFB4CE0564BD6D59B835207C,
	StringStringMapEnumerator_MoveNext_m22BD0722C730E02430014E65174344AA499F336F,
	StringStringMapEnumerator_Reset_m71559FB9DF7506B0DBD523E327E02E6B4405A84E,
	StringStringMapEnumerator_Dispose_m1B7713DE735B05E182D29A076ED15B7720E91BD6,
	StringList__ctor_mE8B8481A314CC754CF22966A396EA63207F8A44B,
	StringList_getCPtr_m6FC803782DE21FD97DF7F0DC43C55EFF974BC69F,
	StringList_Finalize_m79827A0C46ECCAD9DB336D480B7AA54BB03B2B35,
	StringList_Dispose_mBA4E30F327D0B2581FED88BF87521AC290F26696,
	StringList_Dispose_mC9E9D3DD3DB71D12C8CBCDA47783403F9C37AA2A,
	StringList_get_IsReadOnly_m9344BB3E297157D14A875E8595524AB59559516F,
	StringList_get_Item_mEBA85AB35A3F646D755A24FC0D24118B44224D89,
	StringList_set_Item_mA26263399D7DF77D487AE314B2131479403D6EC3,
	StringList_get_Count_m3D8D769FA302FCB31E5A157C0EABFFC1FE20D24F,
	StringList_CopyTo_mD692F361A2C5F2D5B96C2633EA18825C9CE20D27,
	StringList_CopyTo_m0A3A36516421CD77E1C3FA1EC95BBF6385F391D8,
	StringList_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m9FDF2D5121BF290D240D703CC1C87EC274C029BB,
	StringList_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_mA54DC2A36A7B9040510C3423852499ABE42BB84D,
	StringList_Clear_m82FB1CC4351B0ABF3D59C2684C26480D2A8C679B,
	StringList_Add_m3CF7B8A5AF9295D001B02046D9C0C798968B0103,
	StringList_size_m26EC4497B274231ACC1D117E49C85E33D099F716,
	StringList__ctor_m95E70415E0B866B0620AF11568ADFD93E3DD112C,
	StringList_getitemcopy_mDC3C5EFA13D96E01F95E254D4065A2083DA83D67,
	StringList_getitem_mCAAFB8C789F633D75FF24D9CD534E835EA31716B,
	StringList_setitem_m3377D2A1A7CCCE23DC5B0BAE705F57F493C54E6B,
	StringList_Insert_mE01DFBF1964AF4D6F7163B440E4D1F47E4324D7E,
	StringList_RemoveAt_mE300BDB9B78983E06CF3212E83878FC6E9599FFB,
	StringList_Contains_m45135739E8C495AA6A1D571EE54B299066A263FE,
	StringList_IndexOf_mD053F9A376F1C9968A74D6000CAA4A60A29B7217,
	StringList_Remove_mD0348272B896C09C2128F4F049F7763E1D4E46B2,
	StringListEnumerator__ctor_m9C605A00C571C1D36C197B35104916E77E4E24F2,
	StringListEnumerator_get_Current_m9CA8E365AD4B78B9D17442CA31BC6CA0EBD585DF,
	StringListEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_mA97F140652F707D1CC004E24C7BB902439C3B188,
	StringListEnumerator_MoveNext_m9D9BDA9A01A682ABEB2AAAE67A09A07390E936CA,
	StringListEnumerator_Reset_mBE034B8BF2A4A5D10CB9920A2CDD498DF33E9FCD,
	StringListEnumerator_Dispose_m011212072122F01364D073B434712AAC5CC8A95E,
	AppOptionsInternal__ctor_mBA3D4999FA58E5D0E10F920846372ACD71CDEDD0,
	AppOptionsInternal_Finalize_m98E79714FD18F1FBD487CE442C7CAFCECFA572A5,
	AppOptionsInternal_Dispose_m923FE7958D4317ED8825DBAD4254B533825AEAA1,
	AppOptionsInternal_Dispose_m7550240FA9B391FAA540F6B8087CA1C195CD25E9,
	AppOptionsInternal_get_ProjectId_m3FFDDF11A39C948719985A512855D7C9740143CB,
	FirebaseApp__ctor_mB94B4439B6474A9B3420A388F4C847153BB7DA2B,
	FirebaseApp_getCPtr_mA17E448C13FB7553AF1914C14CBDC250F0CB2772,
	FirebaseApp_Finalize_mBA9B05FC454D571B021370352E3DAA24A927C964,
	FirebaseApp_Dispose_mD97452CFB97FC00105EB0369582537ED1C457A93,
	FirebaseApp_Dispose_m05D2841662AD52390E0D09C5A81366D751FF8B38,
	FirebaseApp__cctor_m089DBE5321221ADCEBC17C3DB5057999D75C1042,
	FirebaseApp_TranslateDllNotFoundException_mAC1EBE66BEE7869030ABDF79698C3F5B62FBCE6C,
	FirebaseApp_get_DefaultInstance_mBF6A8D928FEC9E3B18EFC29A85E8BBF62FCC70F0,
	FirebaseApp_GetInstance_m754B7224D202231D11838329C0B141E243D06CBF,
	FirebaseApp_Create_mB2276D7EAD55D961ECE48E140645007F1C72D98E,
	FirebaseApp_get_Name_m5945BBCED21D565E2D871D4CC00D03BB6EDB60B8,
	FirebaseApp_get_LogLevel_m8D2BE127B0EFF6334588C426EDC67FDD1144E014,
	FirebaseApp_add_AppDisposed_m72EFA96351AC82CC8DF3A42ABEFB313B0D9E7D79,
	FirebaseApp_remove_AppDisposed_m1E390E9068C2247AFF44122B61195EC3627F7812,
	FirebaseApp_AddReference_m1B148D579E5DBB9E6608813E714769757F33142B,
	FirebaseApp_RemoveReference_mF1E15EE01A41D7674FAFDAB777CF637071F907E0,
	FirebaseApp_ThrowIfNull_m408BCFBB4ED87F4FF412F374897F083E87897315,
	FirebaseApp_InitializeAppUtilCallbacks_mF98F56F2513D3FEEA93D059CE57F13721D1E3617,
	FirebaseApp_OnAllAppsDestroyed_m1CCBDEDF6290B544DE085AAC017D94A606547B5F,
	FirebaseApp_InitializeCrashlyticsIfPresent_mE36628943ED00C5D03C2AD561B6950E0CCE8C697,
	FirebaseApp_CreateAndTrack_mD755E5670398197F0E362BA3128AE66A81F061E9,
	FirebaseApp_ThrowIfCheckDependenciesRunning_m4D5BB8F1A9121F7328BD9F3DF22AFAC97F62E441,
	FirebaseApp_IsCheckDependenciesRunning_m7A31B8F0F36137615EF27844C0686EAAEA39AFC9,
	FirebaseApp_options_m246546AF14ED87EA3EDB8095EA99C5AA2396E8B4,
	FirebaseApp_get_NameInternal_m89112573EBE5801863EA50B17FBAE20CBE855C75,
	FirebaseApp_CreateInternal_m701D4A79799B12C5532B0B12826943B58DFCADDD,
	FirebaseApp_ReleaseReferenceInternal_m695A4178B58AD3B445FCEB4567A414DBB66A4632,
	FirebaseApp_RegisterLibrariesInternal_m813089397FAF99BD61FFCB95C059F41B6DB32B20,
	FirebaseApp_LogHeartbeatInternal_m6C2088EDAD68B733D483AE0309073200FE3D68C9,
	FirebaseApp_AppSetDefaultConfigPath_m54AE67D90C3392F1884FDD05F1EF8DCC600080F9,
	FirebaseApp_get_DefaultName_m6F7C0AE32C0E880830129BFBC914E23F412E7CAF,
	EnableModuleParams_get_CppModuleName_mE57521DAC3F8972C81AFBC72DB70FE79A9F946B3,
	EnableModuleParams_set_CppModuleName_m780B77AD33765B83D0675C02876BB379B9EFCCEB,
	EnableModuleParams_get_CSharpClassName_m5C21BC47A020FE24984E1A282267CE62CF09080B,
	EnableModuleParams_set_CSharpClassName_mB1413BCF93E8A3B658798ED556E586C47981F018,
	EnableModuleParams_get_AlwaysEnable_m76B3B18100019E68E79EA0A0B320B1EAE0AB8260,
	EnableModuleParams_set_AlwaysEnable_m38C379905DD5810F629E35AD34DD0F677990ACFE,
	EnableModuleParams__ctor_mC9619ED6B8BE82D305359BB53BDE6274A8A2D2C2,
	CreateDelegate__ctor_m9B61AF9F4EFF9CCA9FC10B8BDB5E8AD7130E4DE1,
	CreateDelegate_Invoke_m9FC551133A4F9301FB4F107B90F7C98A66F95BE9,
	CreateDelegate_BeginInvoke_mE55A9209FBD926992A08A1F0C0DCAC9083CC66A5,
	CreateDelegate_EndInvoke_mA8FE3700C59A150FAA37B17BDAA7A44319C1116E,
	U3CU3Ec__cctor_m6CEB22FFC45F6E85A173A787465FCDE4EBA19206,
	U3CU3Ec__ctor_m58F4F61972161EC4DA21A6B10024C4D7C71DC42A,
	U3CU3Ec_U3CCreateU3Eb__15_0_m88AD2B39B4B74F4D45D16198A1910301A7255079,
	U3CU3Ec_U3CCreateAndTrackU3Eb__48_0_m0F69C58EFA46937CB73BD5245B64D5F8C18D0C17,
	AppUtilPINVOKE__cctor_mFD78B877244B4AE4B4ECB1A5F1A90986222A7B1B,
	AppUtilPINVOKE_delete_FutureBase_m32A337F75D98D136CA48473B07F0D041350A3ABC,
	AppUtilPINVOKE_FutureBase_status_m92C9BDAD44005F5776E7B8AB3A831A66415E997B,
	AppUtilPINVOKE_FutureBase_error_m3811E5F983BB73714C62724B57DC5DF4DCAC13CA,
	AppUtilPINVOKE_FutureBase_error_message_m158A38CCC09A2363F58865F3FF7BA538C6D50625,
	AppUtilPINVOKE_new_StringStringMap__SWIG_0_mCE10310D4C42EC884A82A69FD9295A658E405EAF,
	AppUtilPINVOKE_StringStringMap_size_m064A6F37DB06E96A55C0BA8B5B16B5DBD6572418,
	AppUtilPINVOKE_StringStringMap_Clear_m6AED672F614EC3F031249C51053917AC20F870C6,
	AppUtilPINVOKE_StringStringMap_getitem_m4B9D9EB3CC3EA41090CEE9A8E41C7E9530119F33,
	AppUtilPINVOKE_StringStringMap_setitem_m5E8DA70BC3CA15BE84AA72CF2E2F2F8C5311E8EC,
	AppUtilPINVOKE_StringStringMap_ContainsKey_m96BD9C32FFEB55C52DBC79128234E8158A90157D,
	AppUtilPINVOKE_StringStringMap_Add_m33D3FF6AAD8725197ED64C2388C1837E04A91DF4,
	AppUtilPINVOKE_StringStringMap_Remove_m3E0CFC2194307512821D54D2E377BF1411FAE308,
	AppUtilPINVOKE_StringStringMap_create_iterator_begin_m4A7CEFD5594BF712ADF477B7D6CE847145D7077B,
	AppUtilPINVOKE_StringStringMap_get_next_key_mD42451EE4AE529A4345694AC9E2545B781E3CFC5,
	AppUtilPINVOKE_StringStringMap_destroy_iterator_m181F8DFB09848A11C80AA806A81FFC93840EF756,
	AppUtilPINVOKE_delete_StringStringMap_m52D99E016CC34299C1B789C9E0121EC5A8FF9F64,
	AppUtilPINVOKE_StringList_Clear_m95F9B093FC1E3ED61D41A9B04E041AE36EAEC3CF,
	AppUtilPINVOKE_StringList_Add_m86712B6D21BEF4BD37B2C168B66DD726974E233A,
	AppUtilPINVOKE_StringList_size_m6DAD77BDC909994D88328C4D1FDEBFCD4C186481,
	AppUtilPINVOKE_new_StringList__SWIG_0_m311EE8221AF25B96E1C3F30C07C7B83519ADBF5F,
	AppUtilPINVOKE_StringList_getitemcopy_mE737BDBCF5A6AB012DB3CC8EBCDBCB2B05CBDB8E,
	AppUtilPINVOKE_StringList_getitem_m8C7B0FEC3E53F4C0C7D20B005FF75CCCFA592B99,
	AppUtilPINVOKE_StringList_setitem_m45D872D5D3F584E9DAA7C4ABE119BBA6E697C0CA,
	AppUtilPINVOKE_StringList_Insert_m980FEBD0EA50E405B21C93DAD5491149D23F3B86,
	AppUtilPINVOKE_StringList_RemoveAt_m4E4D7B4908AF7A612E59BB909735F3497EC012F3,
	AppUtilPINVOKE_StringList_Contains_m3A4DF6B80C378203111CBB0B2BB0FE12D43F6071,
	AppUtilPINVOKE_StringList_IndexOf_m459DFE45808E2EDEF4599FBAC87F8099FB4F65E8,
	AppUtilPINVOKE_StringList_Remove_mE63FB40C5BEAEA814A59F7ECEB4E0911F3DEDDFA,
	AppUtilPINVOKE_delete_StringList_m25578115258E3E3987F3B98CB5AD97729F00A37F,
	AppUtilPINVOKE_AppOptionsInternal_ProjectId_get_m443BAE7C78EA890A59DA4BBBC482C3759EE7C4AD,
	AppUtilPINVOKE_delete_AppOptionsInternal_m2989EEA9A280DBA5CF7BA1DD494D7075261E66B6,
	AppUtilPINVOKE_FirebaseApp_options_mC39783D9DFE182B5266FB5FCEAC12C0B99DA21BC,
	AppUtilPINVOKE_FirebaseApp_NameInternal_get_m12A197E54EC181A89A1A954E3F1178D3778CF9D6,
	AppUtilPINVOKE_FirebaseApp_CreateInternal__SWIG_0_m6DA2C8A7648278BD0C312C5EFE387E3F6C10C0C7,
	AppUtilPINVOKE_FirebaseApp_ReleaseReferenceInternal_mC8A038946726600A71193DFC3F7489B3E5821932,
	AppUtilPINVOKE_FirebaseApp_GetLogLevelInternal_m796D5D97039485FC06B5F705E8A0E063A2AC238B,
	AppUtilPINVOKE_FirebaseApp_RegisterLibrariesInternal_m02CDD2FB7B903F6E3801ED0908CB9DFA214F8EC2,
	AppUtilPINVOKE_FirebaseApp_LogHeartbeatInternal_m9B9F4E51812EBC89CECB543E3E2E31BB5503A990,
	AppUtilPINVOKE_FirebaseApp_AppSetDefaultConfigPath_mCFC987839BF745850B0EE9D337B6D88BAD4D955D,
	AppUtilPINVOKE_FirebaseApp_DefaultName_get_m89727C45DF2C2E541190B80B3C4C51F79A40AC75,
	AppUtilPINVOKE_PollCallbacks_m854E2979EC7E9B1109222D67FC42A947C957BDD6,
	AppUtilPINVOKE_AppEnableLogCallback_m77EF6A5E3B6980BB0FC27D7073DF37C2EB601DF7,
	AppUtilPINVOKE_SetEnabledAllAppCallbacks_m6732FA8A6DB8BA6AD9F155025C4DC82AED5D4DC4,
	AppUtilPINVOKE_SetEnabledAppCallbackByName_mFA4EF5EB8A51F10D3CFAF1FA431B086ACB65FE20,
	AppUtilPINVOKE_GetEnabledAppCallbackByName_m66EFD9115DE665251D4B0579AACEB7741770702C,
	AppUtilPINVOKE_SetLogFunction_m5DFEC25E7A372B96EF4BB908488CBD68202BA656,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AppUtil_m1CD8200ED7114166960CCBC6B6A300E962EAF9A6,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AppUtil_mC2B5A2343D9A5417117D7727F225C77E413FBE61,
	SWIGExceptionHelper_SetPendingApplicationException_m0829F0837975040087642D6A0EF77DE0F908FCEE,
	SWIGExceptionHelper_SetPendingArithmeticException_m43E86638D478FE1109B49BB4F8844EF4EDD34414,
	SWIGExceptionHelper_SetPendingDivideByZeroException_m2B34635F25839BAFD46740D0964E237A1582578B,
	SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m7BC82194E760DFF75E2F56936BC89321263F7253,
	SWIGExceptionHelper_SetPendingInvalidCastException_m71BAB12378AF0F55B0C013BB0146B9A0AFD0B3DF,
	SWIGExceptionHelper_SetPendingInvalidOperationException_m782571C94E167C84E015BA1F883EB335A1719E45,
	SWIGExceptionHelper_SetPendingIOException_m024FB3DF66F596B41BAD3EB701F4DED5057FE49C,
	SWIGExceptionHelper_SetPendingNullReferenceException_mEA2CE40C44CD6C5FE044BAB6FFA434AC5CCF4E45,
	SWIGExceptionHelper_SetPendingOutOfMemoryException_m72E3606F191DCDDEF234BAAF117CC916E1CBD5BC,
	SWIGExceptionHelper_SetPendingOverflowException_m8CD04C3E8A76FACEA1FFC6A074090902A335BD85,
	SWIGExceptionHelper_SetPendingSystemException_m7BB7BA403702277AEB0FD8DEB51798845CCBD063,
	SWIGExceptionHelper_SetPendingArgumentException_mB89C3689EC192CAD485B74904289BC70D393B9B3,
	SWIGExceptionHelper_SetPendingArgumentNullException_mE67B9DA5FB8088FD758246C75CACB5A5521B5D26,
	SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3B25880532D77834352308A7841325D00B402D4C,
	SWIGExceptionHelper__cctor_m7B61D49340AC58F5296E054755FED58DF220EF06,
	SWIGExceptionHelper__ctor_m06C48C4611CDA458CA1AF651ED06BF7FF7EDF536,
	ExceptionDelegate__ctor_m4E04BD56501AA698F333F3189D232E0DD8BE66A0,
	ExceptionDelegate_Invoke_mE907915DC5B6A911DE7F253DF0E0D82F63B23A06,
	ExceptionDelegate_BeginInvoke_m72D31AEE58624296E481B8F6C28EDF28C445F92B,
	ExceptionDelegate_EndInvoke_mCD778A944D0755D6227785C17547B6F3FCCB9D59,
	ExceptionArgumentDelegate__ctor_m9B64B0E9472C1DDAA639843324FD57FBCCE07E08,
	ExceptionArgumentDelegate_Invoke_mD10622418D792C1CDA2D02B0117C251187C52D74,
	ExceptionArgumentDelegate_BeginInvoke_m0410594AB6ABF10A9740F06B324A5A6C059E39B9,
	ExceptionArgumentDelegate_EndInvoke_mE24172C5085232AD4E4A4EAC36FBF77A79A93C31,
	SWIGPendingException_get_Pending_m71433EDDEE96D742319A495AD3BCE593564E61FB,
	SWIGPendingException_Set_m68BF3F45B3D611F6908B1CE30A6C73DA2B5A6D56,
	SWIGPendingException_Retrieve_m024B64F5669D05A7C47EF731D60755C70D1D5FE6,
	SWIGPendingException__cctor_m5A9E40C6C867D7C2D46025ECF58EE3DF714A57ED,
	SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_mE1F3702E085E5899748BE17BA0D1E4505AA33EA1,
	SWIGStringHelper_CreateString_mFB66507B0E5F5D74116BC8CADAC859CD8614D642,
	SWIGStringHelper__cctor_mE822684D4A43792B6C07FFD6C8BE6A858BE9AB61,
	SWIGStringHelper__ctor_m9F305BAB06F185B49FD5AC05A407928C69D672F6,
	SWIGStringDelegate__ctor_mED39AF7AB0675F58D7C5E732BB50C419BF321299,
	SWIGStringDelegate_Invoke_mE2D5B14F87E5528B7095C2B08CFD4B10A4926BDF,
	SWIGStringDelegate_BeginInvoke_m071087F52EF4FF4FB5914D2D986CD4607DFDE8E7,
	SWIGStringDelegate_EndInvoke_m4A38AD7D0C3965603592F826BC582D78A7FB7AA0,
	AppUtil_PollCallbacks_m54B425D6913C258E717CAA36D9C649BDFDAF138C,
	AppUtil_AppEnableLogCallback_m5130BB1ADDCE046948126A13C715E71DBC01AB6E,
	AppUtil_SetEnabledAllAppCallbacks_mD8D5DDABD6E4E3E1505CF38EFD9171828D2B7FB4,
	AppUtil_SetEnabledAppCallbackByName_mB57B6366CBE1D055F849AC6E44AB3B14C580956C,
	AppUtil_GetEnabledAppCallbackByName_m9DB078FC33838160DD0C051DD16B9E4FDB4A016D,
	AppUtil_SetLogFunction_mFD8CE5F344A4BAC2B5BF19537651D25A171A5A28,
	VersionInfo_get_SdkVersion_m36955800253C856C17C25993EA80CD1EE9E58EEB,
	VersionInfo_get_BuildSource_mFA66DD38308E651A428C323254007E6ECFD7A988,
	FirebaseAppUtils_get_Instance_m479C17BD7ED35574386FD00136C22CF2D90AF61D,
	FirebaseAppUtils_TranslateDllNotFoundException_m6E4E3109BD0827EE0E675EE9CD50E9C6EE343356,
	FirebaseAppUtils_PollCallbacks_m0A88A069EC5477A1CC9BFCEDC55DED431EE64E91,
	FirebaseAppUtils_GetLogLevel_m9E036F13631BCE81463AA50592FBCD54E9CB09D7,
	FirebaseAppUtils__ctor_m69CA1CBCD58CB3128DF35E9560A3D1D38005845F,
	FirebaseAppUtils__cctor_mAF324E88E10F8B1214A8A5CDFB73794B99517B03,
};
static const int32_t s_InvokerIndices[247] = 
{
	1470,
	2842,
	2842,
	1470,
	921,
	5377,
	5377,
	5396,
	5396,
	5396,
	5048,
	4847,
	4847,
	3595,
	3595,
	3595,
	2797,
	1582,
	1581,
	1470,
	464,
	2856,
	2856,
	1560,
	3595,
	3595,
	2797,
	3518,
	3518,
	3533,
	1560,
	5026,
	3595,
	3595,
	2797,
	2550,
	1582,
	1058,
	3518,
	3478,
	3533,
	3533,
	2736,
	1921,
	1921,
	1579,
	3533,
	3533,
	3533,
	3595,
	3586,
	3595,
	2550,
	1582,
	2080,
	1582,
	2080,
	3520,
	2549,
	2844,
	2856,
	3413,
	3533,
	3478,
	3595,
	3595,
	1560,
	5026,
	3595,
	3595,
	2797,
	3478,
	2547,
	1470,
	3518,
	1579,
	591,
	3533,
	3533,
	3595,
	2856,
	3586,
	3595,
	2547,
	2547,
	1470,
	1470,
	2842,
	2080,
	2414,
	2080,
	2856,
	3533,
	3533,
	3478,
	3595,
	3595,
	1560,
	3595,
	3595,
	2797,
	3533,
	1560,
	5026,
	3595,
	3595,
	2797,
	5396,
	5298,
	5377,
	5160,
	5377,
	3533,
	5371,
	2856,
	2856,
	3595,
	3595,
	3595,
	5396,
	5396,
	5363,
	4731,
	5396,
	5363,
	3533,
	3533,
	5377,
	5298,
	5298,
	5298,
	5298,
	5377,
	3533,
	2856,
	3533,
	2856,
	3478,
	2797,
	961,
	1581,
	3533,
	1264,
	2550,
	5396,
	3595,
	3533,
	3478,
	5396,
	5293,
	5046,
	5046,
	5154,
	5373,
	5246,
	5293,
	4710,
	4452,
	4558,
	4452,
	4558,
	5075,
	4709,
	4841,
	5293,
	5293,
	4842,
	5246,
	5373,
	4708,
	4708,
	4451,
	4451,
	4839,
	4558,
	4650,
	4558,
	5293,
	5154,
	5293,
	5075,
	5154,
	5373,
	5293,
	5371,
	5293,
	5293,
	5298,
	5377,
	5396,
	5290,
	5290,
	4856,
	4969,
	5298,
	3686,
	4502,
	5298,
	5298,
	5298,
	5298,
	5298,
	5298,
	5298,
	5298,
	5298,
	5298,
	5298,
	4866,
	4866,
	4866,
	5396,
	3595,
	1581,
	2856,
	830,
	2856,
	1581,
	1582,
	498,
	2856,
	5363,
	5298,
	5377,
	5396,
	5298,
	5160,
	5396,
	3595,
	1581,
	2550,
	830,
	2550,
	5396,
	5290,
	5290,
	4856,
	4969,
	5298,
	5377,
	5377,
	5377,
	2856,
	3595,
	3518,
	3595,
	5396,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[16] = 
{
	{ 0x0600000D, 9,  (void**)&LogUtil_LogMessageFromCallback_m04F3C7908BCA7A583B183A9551387E5F04B21994_RuntimeMethod_var, 0 },
	{ 0x060000C6, 20,  (void**)&SWIGExceptionHelper_SetPendingApplicationException_m0829F0837975040087642D6A0EF77DE0F908FCEE_RuntimeMethod_var, 0 },
	{ 0x060000C7, 24,  (void**)&SWIGExceptionHelper_SetPendingArithmeticException_m43E86638D478FE1109B49BB4F8844EF4EDD34414_RuntimeMethod_var, 0 },
	{ 0x060000C8, 25,  (void**)&SWIGExceptionHelper_SetPendingDivideByZeroException_m2B34635F25839BAFD46740D0964E237A1582578B_RuntimeMethod_var, 0 },
	{ 0x060000C9, 27,  (void**)&SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m7BC82194E760DFF75E2F56936BC89321263F7253_RuntimeMethod_var, 0 },
	{ 0x060000CA, 28,  (void**)&SWIGExceptionHelper_SetPendingInvalidCastException_m71BAB12378AF0F55B0C013BB0146B9A0AFD0B3DF_RuntimeMethod_var, 0 },
	{ 0x060000CB, 29,  (void**)&SWIGExceptionHelper_SetPendingInvalidOperationException_m782571C94E167C84E015BA1F883EB335A1719E45_RuntimeMethod_var, 0 },
	{ 0x060000CC, 26,  (void**)&SWIGExceptionHelper_SetPendingIOException_m024FB3DF66F596B41BAD3EB701F4DED5057FE49C_RuntimeMethod_var, 0 },
	{ 0x060000CD, 30,  (void**)&SWIGExceptionHelper_SetPendingNullReferenceException_mEA2CE40C44CD6C5FE044BAB6FFA434AC5CCF4E45_RuntimeMethod_var, 0 },
	{ 0x060000CE, 31,  (void**)&SWIGExceptionHelper_SetPendingOutOfMemoryException_m72E3606F191DCDDEF234BAAF117CC916E1CBD5BC_RuntimeMethod_var, 0 },
	{ 0x060000CF, 32,  (void**)&SWIGExceptionHelper_SetPendingOverflowException_m8CD04C3E8A76FACEA1FFC6A074090902A335BD85_RuntimeMethod_var, 0 },
	{ 0x060000D0, 33,  (void**)&SWIGExceptionHelper_SetPendingSystemException_m7BB7BA403702277AEB0FD8DEB51798845CCBD063_RuntimeMethod_var, 0 },
	{ 0x060000D1, 21,  (void**)&SWIGExceptionHelper_SetPendingArgumentException_mB89C3689EC192CAD485B74904289BC70D393B9B3_RuntimeMethod_var, 0 },
	{ 0x060000D2, 22,  (void**)&SWIGExceptionHelper_SetPendingArgumentNullException_mE67B9DA5FB8088FD758246C75CACB5A5521B5D26_RuntimeMethod_var, 0 },
	{ 0x060000D3, 23,  (void**)&SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3B25880532D77834352308A7841325D00B402D4C_RuntimeMethod_var, 0 },
	{ 0x060000E3, 34,  (void**)&SWIGStringHelper_CreateString_mFB66507B0E5F5D74116BC8CADAC859CD8614D642_RuntimeMethod_var, 0 },
};
extern const CustomAttributesCacheGenerator g_Firebase_App_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Firebase_App_CodeGenModule;
const Il2CppCodeGenModule g_Firebase_App_CodeGenModule = 
{
	"Firebase.App.dll",
	247,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	16,
	s_reversePInvokeIndices,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Firebase_App_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

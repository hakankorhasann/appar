﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.String
struct String_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

struct unitytls_errorstate_t0015D496F47B84E1D98D31D5132B27FADB38F499 ;
struct unitytls_key_ref_t7EFBA70561D0E9FD8517038EBC0CC9FCF9AE6B61 ;
struct unitytls_tlsctx_tA5DB674E2A83ADDD03624096501FCDD29E9DB7FA ;
struct unitytls_x509list_ref_tE4376B9592E1AF7E02BB0BB2CE110D8219832D4D ;
struct unitytls_x509name_tC19C2F27FF70AD438A79A5F66E4C5FFA2613EDA6 ;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Mono.Unity.UnityTls/unitytls_x509list_ref
struct unitytls_x509list_ref_tE4376B9592E1AF7E02BB0BB2CE110D8219832D4D 
{
public:
	// System.UInt64 Mono.Unity.UnityTls/unitytls_x509list_ref::handle
	uint64_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(unitytls_x509list_ref_tE4376B9592E1AF7E02BB0BB2CE110D8219832D4D, ___handle_0)); }
	inline uint64_t get_handle_0() const { return ___handle_0; }
	inline uint64_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(uint64_t value)
	{
		___handle_0 = value;
	}
};


// Firebase.Firestore.FirestoreError
struct FirestoreError_tA438E5A5FFD9AE5DD5FA837046B3792EC0D3840D 
{
public:
	// System.Int32 Firebase.Firestore.FirestoreError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FirestoreError_tA438E5A5FFD9AE5DD5FA837046B3792EC0D3840D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Firebase.LogLevel
struct LogLevel_t308AFEEB17156412A4E9F577B863B2B20533D200 
{
public:
	// System.Int32 Firebase.LogLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogLevel_t308AFEEB17156412A4E9F577B863B2B20533D200, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.VuCloudImageTargetQueryError
struct VuCloudImageTargetQueryError_t6CE6DD4866A263F93CEC82518C43C7E7E98352A3 
{
public:
	// System.Int32 Vuforia.VuCloudImageTargetQueryError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VuCloudImageTargetQueryError_t6CE6DD4866A263F93CEC82518C43C7E7E98352A3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Vuforia.VuEngineError
struct VuEngineError_t0297AC9C8D5D48298EB31F272F13022ABA15B9CD 
{
public:
	// System.Int32 Vuforia.VuEngineError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VuEngineError_t0297AC9C8D5D48298EB31F272F13022ABA15B9CD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Mono.Unity.UnityTls/unitytls_x509verify_result
struct unitytls_x509verify_result_t3CE5D0E50DA56D0A6561757039E6F1F292996B84 
{
public:
	// System.UInt32 Mono.Unity.UnityTls/unitytls_x509verify_result::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(unitytls_x509verify_result_t3CE5D0E50DA56D0A6561757039E6F1F292996B84, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};


// System.Object


// System.Object


// System.ValueType


// System.ValueType


// System.Enum

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};


// System.Enum


// System.IntPtr

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.IntPtr


// System.Void


// System.Void


// Mono.Unity.UnityTls/unitytls_x509list_ref


// Mono.Unity.UnityTls/unitytls_x509list_ref


// Firebase.Firestore.FirestoreError


// Firebase.Firestore.FirestoreError


// Firebase.LogLevel


// Firebase.LogLevel


// Vuforia.VuCloudImageTargetQueryError


// Vuforia.VuCloudImageTargetQueryError


// Vuforia.VuEngineError


// Vuforia.VuEngineError


// Mono.Unity.UnityTls/unitytls_x509verify_result


// Mono.Unity.UnityTls/unitytls_x509verify_result

#ifdef __clang__
#pragma clang diagnostic pop
#endif

extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_ARFoundationAnchorManager_OnCreateAnchor_mCADA6AFE997AB1F39A83D9F763E4E1F38A0F0A47(intptr_t ___posePtr0);
extern "C" int32_t DEFAULT_CALL ReversePInvokeWrapper_ARFoundationAnchorManager_OnRemoveAnchor_m14335F88D4013B1EB4168A2B34AB7A0B688515C7(intptr_t ___uuidPtr0);
extern "C" int32_t CDECL ReversePInvokeWrapper_DeflateStreamNative_UnmanagedRead_m0F9E6DBD04FD70318A33801F8FCC02CF06EA11B0(intptr_t ___buffer0, int32_t ___length1, intptr_t ___data2);
extern "C" int32_t CDECL ReversePInvokeWrapper_DeflateStreamNative_UnmanagedWrite_m203E0AF8CE69E96F78BDD65BD95264F3D108C9EB(intptr_t ___buffer0, int32_t ___length1, intptr_t ___data2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_DocumentReference_DocumentSnapshotsHandler_mE75173506F68CF4107731634BF380B9BBB7372F1(int32_t ___callbackId0, intptr_t ___snapshotPtr1, int32_t ___errorCode2, char* ___errorMessage3);
extern "C" void CDECL ReversePInvokeWrapper_Engine_onEngineErrorHandler_m8C0EA380BC4B93C57728A4E2B21FB877516C67FB(int32_t ___errorCode0, intptr_t ___clientData1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FirebaseFirestore_LoadBundleTaskProgressHandler_mDEEE792A4653B34026BC13985E88600A9D3F1393(int32_t ___callbackId0, intptr_t ___progressPtr1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FirebaseFirestore_SnapshotsInSyncHandler_m9820BBCB59CB6A40F2CF44F87BD7ABD483137FDE(int32_t ___callbackId0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Future_QuerySnapshot_SWIG_CompletionDispatcher_m15F1003D3DED0E6AC8CBBD6E50317019574A3596(int32_t ___key0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_LogUtil_LogMessageFromCallback_m04F3C7908BCA7A583B183A9551387E5F04B21994(int32_t ___logLevel0, char* ___message1);
extern "C" void CDECL ReversePInvokeWrapper_NativeUnityDriver_GetDriverConfig_m3967A5457FE2194B627791B3983DB6EAFE7A83DF(intptr_t ___buffer0);
extern "C" void CDECL ReversePInvokeWrapper_NativeUnityDriver_OnDriverCreated_mD2459977E24A616834256BB60D181A8715D13517(intptr_t ___driverPtr0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_m4EF49088C9F8BEC14A9AD73F1C0F992FE808B3E9(intptr_t ___arg0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Query_QuerySnapshotsHandler_m063CC24B00233FBA6CC4C2E3D39E731BD8800821(int32_t ___callbackId0, intptr_t ___snapshotPtr1, int32_t ___errorCode2, char* ___errorMessage3);
extern "C" void CDECL ReversePInvokeWrapper_UnityTlsContext_CertificateCallback_mCF0B5C35CA9C47C2FA5438BA3BA603911642333B(void* ___userData0, unitytls_tlsctx_tA5DB674E2A83ADDD03624096501FCDD29E9DB7FA * ___ctx1, uint8_t* ___cn2, intptr_t ___cnLen3, unitytls_x509name_tC19C2F27FF70AD438A79A5F66E4C5FFA2613EDA6 * ___caList4, intptr_t ___caListLen5, unitytls_x509list_ref_tE4376B9592E1AF7E02BB0BB2CE110D8219832D4D * ___chain6, unitytls_key_ref_t7EFBA70561D0E9FD8517038EBC0CC9FCF9AE6B61 * ___key7, unitytls_errorstate_t0015D496F47B84E1D98D31D5132B27FADB38F499 * ___errorState8);
extern "C" intptr_t CDECL ReversePInvokeWrapper_UnityTlsContext_ReadCallback_m761A4E7B983E40C8D727C2251158F22F762CD419(void* ___userData0, uint8_t* ___buffer1, intptr_t ___bufferLen2, unitytls_errorstate_t0015D496F47B84E1D98D31D5132B27FADB38F499 * ___errorState3);
extern "C" uint32_t CDECL ReversePInvokeWrapper_UnityTlsContext_VerifyCallback_m8367D8479B3FD1AA41D886446279FEE8246F04B2(void* ___userData0, unitytls_x509list_ref_tE4376B9592E1AF7E02BB0BB2CE110D8219832D4D  ___chain1, unitytls_errorstate_t0015D496F47B84E1D98D31D5132B27FADB38F499 * ___errorState2);
extern "C" intptr_t CDECL ReversePInvokeWrapper_UnityTlsContext_WriteCallback_mE11815EF2287F4F9D27C94D0E5414DC1E2BCEFD8(void* ___userData0, uint8_t* ___data1, intptr_t ___bufferLen2, unitytls_errorstate_t0015D496F47B84E1D98D31D5132B27FADB38F499 * ___errorState3);
extern "C" void CDECL ReversePInvokeWrapper_VuCloudImageTargetObserver_HandleObservations_mCE83E3ECFCDC60292F3DBA713B3D22CB5038F31D(intptr_t ___observationsHandle0, intptr_t ___clientData1);
extern "C" void CDECL ReversePInvokeWrapper_VuCloudImageTargetObserver_HandleQueryError_m5D7D9BB06295D01951222C52C62E9B7293046078(int32_t ___error0, intptr_t ___clientData1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingApplicationException_m0829F0837975040087642D6A0EF77DE0F908FCEE(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentException_mB89C3689EC192CAD485B74904289BC70D393B9B3(char* ___message0, char* ___paramName1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentNullException_mE67B9DA5FB8088FD758246C75CACB5A5521B5D26(char* ___message0, char* ___paramName1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3B25880532D77834352308A7841325D00B402D4C(char* ___message0, char* ___paramName1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArithmeticException_m43E86638D478FE1109B49BB4F8844EF4EDD34414(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingDivideByZeroException_m2B34635F25839BAFD46740D0964E237A1582578B(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIOException_m024FB3DF66F596B41BAD3EB701F4DED5057FE49C(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m7BC82194E760DFF75E2F56936BC89321263F7253(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidCastException_m71BAB12378AF0F55B0C013BB0146B9A0AFD0B3DF(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidOperationException_m782571C94E167C84E015BA1F883EB335A1719E45(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingNullReferenceException_mEA2CE40C44CD6C5FE044BAB6FFA434AC5CCF4E45(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOutOfMemoryException_m72E3606F191DCDDEF234BAAF117CC916E1CBD5BC(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOverflowException_m8CD04C3E8A76FACEA1FFC6A074090902A335BD85(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingSystemException_m7BB7BA403702277AEB0FD8DEB51798845CCBD063(char* ___message0);
extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_SWIGStringHelper_CreateString_mFB66507B0E5F5D74116BC8CADAC859CD8614D642(char* ___cString0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FirestoreExceptionHelper_SetPendingFirestoreException_m0CBBB27537ABEEC0933F1407AFE213BCE003DD6C(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingApplicationException_m7FFFCE61B08FE17B5B6C8DD589E8D2BA7E514700(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentException_m03827C4D6C68D5769B0ECF89FBA487FE09CACA03(char* ___message0, char* ___paramName1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentNullException_m47A1AC704DEE8080F51803DC7FED941B2C51D7DE(char* ___message0, char* ___paramName1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mF2C7DEC5C75D945A3FF2025EFD9E51FD4961ED8D(char* ___message0, char* ___paramName1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArithmeticException_m4A492793829E24826915E01F0C17FE0B8499A7B9(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingDivideByZeroException_mD7DFEB207289EFD4D916BAE54302503294B3A469(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIOException_m8CB7AE7D58F9693F5250EEB08B367E50AF724CDB(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m20AF827963A7C8B753AF68164B7D776C15A67DE9(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidCastException_m0581C3F4931A20BD5ED4EA66093DDDD6A906C6D6(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidOperationException_m40B9057C541E1372563BB9E1002E823E38096D62(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingNullReferenceException_m07F507856FF6AE9838F5CA00AA64F38CBFA2DA84(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOutOfMemoryException_m8A1648D61A97D46629D84CB4BF64BE758E3B0987(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOverflowException_mB46C4E0EECEBB64698810B5EEBFF2A10B3869D0D(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingSystemException_m4504C1471D8E13CC34E2DA16ED245C39DBC5125F(char* ___message0);
extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_SWIGStringHelper_CreateString_m3D08489776631E77C6252B713D92331E5D1D685E(char* ___cString0);


IL2CPP_EXTERN_C const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[];
const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[51] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_ARFoundationAnchorManager_OnCreateAnchor_mCADA6AFE997AB1F39A83D9F763E4E1F38A0F0A47),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_ARFoundationAnchorManager_OnRemoveAnchor_m14335F88D4013B1EB4168A2B34AB7A0B688515C7),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStreamNative_UnmanagedRead_m0F9E6DBD04FD70318A33801F8FCC02CF06EA11B0),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DeflateStreamNative_UnmanagedWrite_m203E0AF8CE69E96F78BDD65BD95264F3D108C9EB),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_DocumentReference_DocumentSnapshotsHandler_mE75173506F68CF4107731634BF380B9BBB7372F1),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Engine_onEngineErrorHandler_m8C0EA380BC4B93C57728A4E2B21FB877516C67FB),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_FirebaseFirestore_LoadBundleTaskProgressHandler_mDEEE792A4653B34026BC13985E88600A9D3F1393),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_FirebaseFirestore_SnapshotsInSyncHandler_m9820BBCB59CB6A40F2CF44F87BD7ABD483137FDE),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Future_QuerySnapshot_SWIG_CompletionDispatcher_m15F1003D3DED0E6AC8CBBD6E50317019574A3596),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_LogUtil_LogMessageFromCallback_m04F3C7908BCA7A583B183A9551387E5F04B21994),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeUnityDriver_GetDriverConfig_m3967A5457FE2194B627791B3983DB6EAFE7A83DF),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeUnityDriver_OnDriverCreated_mD2459977E24A616834256BB60D181A8715D13517),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_m4EF49088C9F8BEC14A9AD73F1C0F992FE808B3E9),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Query_QuerySnapshotsHandler_m063CC24B00233FBA6CC4C2E3D39E731BD8800821),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityTlsContext_CertificateCallback_mCF0B5C35CA9C47C2FA5438BA3BA603911642333B),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityTlsContext_ReadCallback_m761A4E7B983E40C8D727C2251158F22F762CD419),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityTlsContext_VerifyCallback_m8367D8479B3FD1AA41D886446279FEE8246F04B2),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_UnityTlsContext_WriteCallback_mE11815EF2287F4F9D27C94D0E5414DC1E2BCEFD8),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_VuCloudImageTargetObserver_HandleObservations_mCE83E3ECFCDC60292F3DBA713B3D22CB5038F31D),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_VuCloudImageTargetObserver_HandleQueryError_m5D7D9BB06295D01951222C52C62E9B7293046078),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingApplicationException_m0829F0837975040087642D6A0EF77DE0F908FCEE),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentException_mB89C3689EC192CAD485B74904289BC70D393B9B3),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentNullException_mE67B9DA5FB8088FD758246C75CACB5A5521B5D26),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3B25880532D77834352308A7841325D00B402D4C),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArithmeticException_m43E86638D478FE1109B49BB4F8844EF4EDD34414),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingDivideByZeroException_m2B34635F25839BAFD46740D0964E237A1582578B),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIOException_m024FB3DF66F596B41BAD3EB701F4DED5057FE49C),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m7BC82194E760DFF75E2F56936BC89321263F7253),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidCastException_m71BAB12378AF0F55B0C013BB0146B9A0AFD0B3DF),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidOperationException_m782571C94E167C84E015BA1F883EB335A1719E45),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingNullReferenceException_mEA2CE40C44CD6C5FE044BAB6FFA434AC5CCF4E45),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOutOfMemoryException_m72E3606F191DCDDEF234BAAF117CC916E1CBD5BC),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOverflowException_m8CD04C3E8A76FACEA1FFC6A074090902A335BD85),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingSystemException_m7BB7BA403702277AEB0FD8DEB51798845CCBD063),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGStringHelper_CreateString_mFB66507B0E5F5D74116BC8CADAC859CD8614D642),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_FirestoreExceptionHelper_SetPendingFirestoreException_m0CBBB27537ABEEC0933F1407AFE213BCE003DD6C),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingApplicationException_m7FFFCE61B08FE17B5B6C8DD589E8D2BA7E514700),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentException_m03827C4D6C68D5769B0ECF89FBA487FE09CACA03),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentNullException_m47A1AC704DEE8080F51803DC7FED941B2C51D7DE),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mF2C7DEC5C75D945A3FF2025EFD9E51FD4961ED8D),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArithmeticException_m4A492793829E24826915E01F0C17FE0B8499A7B9),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingDivideByZeroException_mD7DFEB207289EFD4D916BAE54302503294B3A469),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIOException_m8CB7AE7D58F9693F5250EEB08B367E50AF724CDB),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m20AF827963A7C8B753AF68164B7D776C15A67DE9),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidCastException_m0581C3F4931A20BD5ED4EA66093DDDD6A906C6D6),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidOperationException_m40B9057C541E1372563BB9E1002E823E38096D62),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingNullReferenceException_m07F507856FF6AE9838F5CA00AA64F38CBFA2DA84),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOutOfMemoryException_m8A1648D61A97D46629D84CB4BF64BE758E3B0987),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOverflowException_mB46C4E0EECEBB64698810B5EEBFF2A10B3869D0D),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingSystemException_m4504C1471D8E13CC34E2DA16ED245C39DBC5125F),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_SWIGStringHelper_CreateString_m3D08489776631E77C6252B713D92331E5D1D685E),
};

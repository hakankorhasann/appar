﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericVirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericInterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Comparison`1<SimpleFileBrowser.FileSystemEntry>
struct Comparison_1_t1A5DDD4BB3952FB12382582EA3C6A1DCD3A25210;
// System.Collections.Generic.Dictionary`2<System.Int32,SimpleFileBrowser.ListItem>
struct Dictionary_2_t650F8D518346F558165A2425C6C9BD7DDF51C721;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Sprite>
struct Dictionary_2_t25CEE683B20ACF6398C3718DF163CF651693326D;
// System.Collections.Generic.HashSet`1<System.Char>
struct HashSet_1_t64694F1841F026350E09C7F835AC8D3B148B2488;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t680119C7ED8D82AED56CDB83DF6F0E9149852A9B;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_tBD60400523D840591A17E4CBBACC79397F68FAA2;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tE6A65C5E45E33FD7D9849FD0914DE3AD32B68050;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<SimpleFileBrowser.FileBrowserItem>
struct List_1_tC177D10798BB0E73A3C6EF511075D40316BD5CB7;
// System.Collections.Generic.List`1<SimpleFileBrowser.FileBrowserQuickLink>
struct List_1_t612A2A8CBA335BE2987BF3A35E0D00D10BC40A2F;
// System.Collections.Generic.List`1<SimpleFileBrowser.FileSystemEntry>
struct List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// System.Collections.Generic.List`1<SimpleFileBrowser.FileBrowser/Filter>
struct List_1_tCEE47E7A624FD9E913773D93A05E7DE44BC7D35A;
// System.Collections.Generic.Stack`1<SimpleFileBrowser.ListItem>
struct Stack_1_t6AF1F6A4BF10324432F934511D9F81D8A0F9C68A;
// System.Collections.Generic.HashSet`1/Slot<System.String>[]
struct SlotU5BU5D_t0AE906AEB021E70A8C465C39ADD28C2B4884604D;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// SimpleFileBrowser.FileSystemEntry[]
struct FileSystemEntryU5BU5D_t457BF0158EF6A2EEFB557C493617F0B87ED6B7A3;
// SimpleFileBrowser.FiletypeIcon[]
struct FiletypeIconU5BU5D_t426C3221F7B6F0C08FB4C56DF6B5D6284A109758;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// SimpleFileBrowser.FileBrowser/QuickLink[]
struct QuickLinkU5BU5D_t204AB6BB1B0F422BCD2C3DC7A1F93FDDAFB416BC;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// System.Globalization.CompareInfo
struct CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.UI.Dropdown
struct Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96;
// UnityEngine.Event
struct Event_tED49F8EC5A2514F6E877E301B1AB7ABE4647253E;
// SimpleFileBrowser.FBCallbackHelper
struct FBCallbackHelper_t75E5C6C770413EE5603E2C8C7AA07D21B28CAC4F;
// SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid
struct FBDirectoryReceiveCallbackAndroid_t826BC254F0340D34CA548141A2130BBA31A5C154;
// SimpleFileBrowser.FileBrowser
struct FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8;
// SimpleFileBrowser.FileBrowserAccessRestrictedPanel
struct FileBrowserAccessRestrictedPanel_t96A484C5166B6EF23402D06FF2107AC3ED6ECB30;
// SimpleFileBrowser.FileBrowserContextMenu
struct FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC;
// SimpleFileBrowser.FileBrowserCursorHandler
struct FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C;
// SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel
struct FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229;
// SimpleFileBrowser.FileBrowserItem
struct FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025;
// SimpleFileBrowser.FileBrowserMovement
struct FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19;
// SimpleFileBrowser.FileBrowserQuickLink
struct FileBrowserQuickLink_t3DDB6D8FB4E5B8A77D0401284C0071037161A08C;
// SimpleFileBrowser.FileBrowserRenamedItem
struct FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4;
// SimpleFileBrowser.FileSystemEntry
struct FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9;
// UnityEngine.Font
struct Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.GlobalJavaObjectRef
struct GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// SimpleFileBrowser.IListViewAdapter
struct IListViewAdapter_t043B79411048E065CEDC01867299EFF50F726D00;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.UI.InputField
struct InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// SimpleFileBrowser.RecycledListView
struct RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA;
// UnityEngine.UI.Scrollbar
struct Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.UI.Toggle
struct Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t7964B2E9E52C4E095B14F01C32774B98CA11711E;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// SimpleFileBrowser.UISkin
struct UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForSecondsRealtime
struct WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40;
// SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_tE608D07C839B5A154A5B6D2877B1E03CC482E9A4;
// SimpleFileBrowser.FileBrowser/<>c
struct U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D;
// SimpleFileBrowser.FileBrowser/<>c__DisplayClass220_0
struct U3CU3Ec__DisplayClass220_0_t3D20A2B3DD8845AD34093E57F5B4DE45DBB95600;
// SimpleFileBrowser.FileBrowser/<>c__DisplayClass242_0
struct U3CU3Ec__DisplayClass242_0_t5A54DD142E562A9BF717F627B31C8F71C0E0FFDE;
// SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241
struct U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025;
// SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266
struct U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD;
// SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265
struct U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602;
// SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback
struct AndroidSAFDirectoryPickCallback_tCB2537E0F2C9160F1890DE73E86301251C6F5504;
// SimpleFileBrowser.FileBrowser/FileSystemEntryFilter
struct FileSystemEntryFilter_t570DC98294020ED04118FDDBAA83AF79C201E4E1;
// SimpleFileBrowser.FileBrowser/Filter
struct Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8;
// SimpleFileBrowser.FileBrowser/OnCancel
struct OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E;
// SimpleFileBrowser.FileBrowser/OnSuccess
struct OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C;
// SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed
struct OnOperationConfirmed_tA108D4E3303410B9B615F6D94A787ABC68C137EB;
// SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted
struct OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74;
// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_t2E59014A56EA94168140F0585834954B40D716F7;
// UnityEngine.UI.InputField/OnValidateInput
struct OnValidateInput_t721D2C2A7710D113E4909B36D9893CC6B1C69B9F;
// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t3FD30F627DF2ADEC87C0BE69EE632AAB99F3B8A9;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_tA2F08EF8BB0B0B0F72DB8242DC5AB17BB0D1731E;

IL2CPP_EXTERN_C RuntimeClass* FileBrowserHelpers_t2E079075C38746CC9599F4635A704970DF3AA4E1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral758733BDBED83CBFF4F635AC26CA92AAE477F75D;
IL2CPP_EXTERN_C String_t* _stringLiteralB3F14BF976EFD974E34846B742502C802FABAE9D;
IL2CPP_EXTERN_C String_t* _stringLiteralD99605E29810F93D7DAE4EFBB764C41AF4E80D32;
IL2CPP_EXTERN_C String_t* _stringLiteralF3E84B722399601AD7E281754E917478AA9AD48D;
IL2CPP_EXTERN_C const RuntimeMethod* FileBrowser_U3CCreateNewFolderCoroutineU3Eb__241_0_mE4B278D5E88BC29E2F4F88DDA2F471F57ED0376D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HashSet_1_Add_m990F3F2EEC5E767A82AF639CD2307F4E7575B370_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HashSet_1_Contains_m9B35CC2F57089F860E18D73F6B603F6F845010FF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HashSet_1__ctor_mCC4A4964EEA7915C5CABFACB64E6A9AD82700818_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HashSet_1__ctor_mDA3E71DBB75FDDB89C8D4A28C9CBE739F1EAEEB8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_IEnumerator_Reset_m5F77E4A7CA436A621AC00B2073FE17DB3EFF2850_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CWaitForLoadDialogU3Ed__266_System_Collections_IEnumerator_Reset_m783AE76EC116144B63DC8BE626E95732D3C0BF27_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CWaitForSaveDialogU3Ed__265_System_Collections_IEnumerator_Reset_m38718E5D5CE7AA13F1F4B684775CED69286E1390_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9;;
struct FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshaled_pinvoke;
struct FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshaled_pinvoke;;

struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.HashSet`1::_buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____buckets_7;
	// System.Collections.Generic.HashSet`1/Slot<T>[] System.Collections.Generic.HashSet`1::_slots
	SlotU5BU5D_t0AE906AEB021E70A8C465C39ADD28C2B4884604D* ____slots_8;
	// System.Int32 System.Collections.Generic.HashSet`1::_count
	int32_t ____count_9;
	// System.Int32 System.Collections.Generic.HashSet`1::_lastIndex
	int32_t ____lastIndex_10;
	// System.Int32 System.Collections.Generic.HashSet`1::_freeList
	int32_t ____freeList_11;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::_comparer
	RuntimeObject* ____comparer_12;
	// System.Int32 System.Collections.Generic.HashSet`1::_version
	int32_t ____version_13;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1::_siInfo
	SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ____siInfo_14;

public:
	inline static int32_t get_offset_of__buckets_7() { return static_cast<int32_t>(offsetof(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229, ____buckets_7)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__buckets_7() const { return ____buckets_7; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__buckets_7() { return &____buckets_7; }
	inline void set__buckets_7(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____buckets_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buckets_7), (void*)value);
	}

	inline static int32_t get_offset_of__slots_8() { return static_cast<int32_t>(offsetof(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229, ____slots_8)); }
	inline SlotU5BU5D_t0AE906AEB021E70A8C465C39ADD28C2B4884604D* get__slots_8() const { return ____slots_8; }
	inline SlotU5BU5D_t0AE906AEB021E70A8C465C39ADD28C2B4884604D** get_address_of__slots_8() { return &____slots_8; }
	inline void set__slots_8(SlotU5BU5D_t0AE906AEB021E70A8C465C39ADD28C2B4884604D* value)
	{
		____slots_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____slots_8), (void*)value);
	}

	inline static int32_t get_offset_of__count_9() { return static_cast<int32_t>(offsetof(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229, ____count_9)); }
	inline int32_t get__count_9() const { return ____count_9; }
	inline int32_t* get_address_of__count_9() { return &____count_9; }
	inline void set__count_9(int32_t value)
	{
		____count_9 = value;
	}

	inline static int32_t get_offset_of__lastIndex_10() { return static_cast<int32_t>(offsetof(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229, ____lastIndex_10)); }
	inline int32_t get__lastIndex_10() const { return ____lastIndex_10; }
	inline int32_t* get_address_of__lastIndex_10() { return &____lastIndex_10; }
	inline void set__lastIndex_10(int32_t value)
	{
		____lastIndex_10 = value;
	}

	inline static int32_t get_offset_of__freeList_11() { return static_cast<int32_t>(offsetof(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229, ____freeList_11)); }
	inline int32_t get__freeList_11() const { return ____freeList_11; }
	inline int32_t* get_address_of__freeList_11() { return &____freeList_11; }
	inline void set__freeList_11(int32_t value)
	{
		____freeList_11 = value;
	}

	inline static int32_t get_offset_of__comparer_12() { return static_cast<int32_t>(offsetof(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229, ____comparer_12)); }
	inline RuntimeObject* get__comparer_12() const { return ____comparer_12; }
	inline RuntimeObject** get_address_of__comparer_12() { return &____comparer_12; }
	inline void set__comparer_12(RuntimeObject* value)
	{
		____comparer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____comparer_12), (void*)value);
	}

	inline static int32_t get_offset_of__version_13() { return static_cast<int32_t>(offsetof(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229, ____version_13)); }
	inline int32_t get__version_13() const { return ____version_13; }
	inline int32_t* get_address_of__version_13() { return &____version_13; }
	inline void set__version_13(int32_t value)
	{
		____version_13 = value;
	}

	inline static int32_t get_offset_of__siInfo_14() { return static_cast<int32_t>(offsetof(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229, ____siInfo_14)); }
	inline SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * get__siInfo_14() const { return ____siInfo_14; }
	inline SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 ** get_address_of__siInfo_14() { return &____siInfo_14; }
	inline void set__siInfo_14(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * value)
	{
		____siInfo_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____siInfo_14), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____items_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____items_1)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_tE608D07C839B5A154A5B6D2877B1E03CC482E9A4  : public RuntimeObject
{
public:
	// SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid/<>c__DisplayClass3_0::<>4__this
	FBDirectoryReceiveCallbackAndroid_t826BC254F0340D34CA548141A2130BBA31A5C154 * ___U3CU3E4__this_0;
	// System.String SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid/<>c__DisplayClass3_0::rawUri
	String_t* ___rawUri_1;
	// System.String SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid/<>c__DisplayClass3_0::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tE608D07C839B5A154A5B6D2877B1E03CC482E9A4, ___U3CU3E4__this_0)); }
	inline FBDirectoryReceiveCallbackAndroid_t826BC254F0340D34CA548141A2130BBA31A5C154 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline FBDirectoryReceiveCallbackAndroid_t826BC254F0340D34CA548141A2130BBA31A5C154 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(FBDirectoryReceiveCallbackAndroid_t826BC254F0340D34CA548141A2130BBA31A5C154 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_rawUri_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tE608D07C839B5A154A5B6D2877B1E03CC482E9A4, ___rawUri_1)); }
	inline String_t* get_rawUri_1() const { return ___rawUri_1; }
	inline String_t** get_address_of_rawUri_1() { return &___rawUri_1; }
	inline void set_rawUri_1(String_t* value)
	{
		___rawUri_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rawUri_1), (void*)value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tE608D07C839B5A154A5B6D2877B1E03CC482E9A4, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_2), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/<>c
struct U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D  : public RuntimeObject
{
public:

public:
};


// SimpleFileBrowser.FileBrowser/<>c__DisplayClass220_0
struct U3CU3Ec__DisplayClass220_0_t3D20A2B3DD8845AD34093E57F5B4DE45DBB95600  : public RuntimeObject
{
public:
	// SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowser/<>c__DisplayClass220_0::<>4__this
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * ___U3CU3E4__this_0;
	// System.String[] SimpleFileBrowser.FileBrowser/<>c__DisplayClass220_0::result
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___result_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass220_0_t3D20A2B3DD8845AD34093E57F5B4DE45DBB95600, ___U3CU3E4__this_0)); }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass220_0_t3D20A2B3DD8845AD34093E57F5B4DE45DBB95600, ___result_1)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_result_1() const { return ___result_1; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___result_1), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241
struct U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025  : public RuntimeObject
{
public:
	// System.Int32 SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::<>4__this
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025, ___U3CU3E4__this_2)); }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/Filter
struct Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8  : public RuntimeObject
{
public:
	// System.String SimpleFileBrowser.FileBrowser/Filter::name
	String_t* ___name_0;
	// System.String[] SimpleFileBrowser.FileBrowser/Filter::extensions
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___extensions_1;
	// System.Collections.Generic.HashSet`1<System.String> SimpleFileBrowser.FileBrowser/Filter::extensionsSet
	HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * ___extensionsSet_2;
	// System.String SimpleFileBrowser.FileBrowser/Filter::defaultExtension
	String_t* ___defaultExtension_3;
	// System.Boolean SimpleFileBrowser.FileBrowser/Filter::allExtensionsHaveSingleSuffix
	bool ___allExtensionsHaveSingleSuffix_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_extensions_1() { return static_cast<int32_t>(offsetof(Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8, ___extensions_1)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_extensions_1() const { return ___extensions_1; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_extensions_1() { return &___extensions_1; }
	inline void set_extensions_1(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___extensions_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_1), (void*)value);
	}

	inline static int32_t get_offset_of_extensionsSet_2() { return static_cast<int32_t>(offsetof(Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8, ___extensionsSet_2)); }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * get_extensionsSet_2() const { return ___extensionsSet_2; }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 ** get_address_of_extensionsSet_2() { return &___extensionsSet_2; }
	inline void set_extensionsSet_2(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * value)
	{
		___extensionsSet_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensionsSet_2), (void*)value);
	}

	inline static int32_t get_offset_of_defaultExtension_3() { return static_cast<int32_t>(offsetof(Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8, ___defaultExtension_3)); }
	inline String_t* get_defaultExtension_3() const { return ___defaultExtension_3; }
	inline String_t** get_address_of_defaultExtension_3() { return &___defaultExtension_3; }
	inline void set_defaultExtension_3(String_t* value)
	{
		___defaultExtension_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultExtension_3), (void*)value);
	}

	inline static int32_t get_offset_of_allExtensionsHaveSingleSuffix_4() { return static_cast<int32_t>(offsetof(Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8, ___allExtensionsHaveSingleSuffix_4)); }
	inline bool get_allExtensionsHaveSingleSuffix_4() const { return ___allExtensionsHaveSingleSuffix_4; }
	inline bool* get_address_of_allExtensionsHaveSingleSuffix_4() { return &___allExtensionsHaveSingleSuffix_4; }
	inline void set_allExtensionsHaveSingleSuffix_4(bool value)
	{
		___allExtensionsHaveSingleSuffix_4 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};


// System.Char
struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2__padding[1];
	};

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// SimpleFileBrowser.FileBrowser/FiletypeIcon
struct FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A 
{
public:
	// System.String SimpleFileBrowser.FileBrowser/FiletypeIcon::extension
	String_t* ___extension_0;
	// UnityEngine.Sprite SimpleFileBrowser.FileBrowser/FiletypeIcon::icon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___icon_1;

public:
	inline static int32_t get_offset_of_extension_0() { return static_cast<int32_t>(offsetof(FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A, ___extension_0)); }
	inline String_t* get_extension_0() const { return ___extension_0; }
	inline String_t** get_address_of_extension_0() { return &___extension_0; }
	inline void set_extension_0(String_t* value)
	{
		___extension_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extension_0), (void*)value);
	}

	inline static int32_t get_offset_of_icon_1() { return static_cast<int32_t>(offsetof(FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A, ___icon_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_icon_1() const { return ___icon_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_icon_1() { return &___icon_1; }
	inline void set_icon_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___icon_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___icon_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of SimpleFileBrowser.FileBrowser/FiletypeIcon
struct FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshaled_pinvoke
{
	char* ___extension_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___icon_1;
};
// Native definition for COM marshalling of SimpleFileBrowser.FileBrowser/FiletypeIcon
struct FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshaled_com
{
	Il2CppChar* ___extension_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___icon_1;
};

// UnityEngine.AndroidJavaProxy
struct AndroidJavaProxy_tA8C86826A74CB7CC5511CB353DBA595C9270D9AF  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaClass UnityEngine.AndroidJavaProxy::javaInterface
	AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * ___javaInterface_0;
	// System.IntPtr UnityEngine.AndroidJavaProxy::proxyObject
	intptr_t ___proxyObject_1;

public:
	inline static int32_t get_offset_of_javaInterface_0() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_tA8C86826A74CB7CC5511CB353DBA595C9270D9AF, ___javaInterface_0)); }
	inline AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * get_javaInterface_0() const { return ___javaInterface_0; }
	inline AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 ** get_address_of_javaInterface_0() { return &___javaInterface_0; }
	inline void set_javaInterface_0(AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * value)
	{
		___javaInterface_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___javaInterface_0), (void*)value);
	}

	inline static int32_t get_offset_of_proxyObject_1() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_tA8C86826A74CB7CC5511CB353DBA595C9270D9AF, ___proxyObject_1)); }
	inline intptr_t get_proxyObject_1() const { return ___proxyObject_1; }
	inline intptr_t* get_address_of_proxyObject_1() { return &___proxyObject_1; }
	inline void set_proxyObject_1(intptr_t value)
	{
		___proxyObject_1 = value;
	}
};


// UnityEngine.Bounds
struct Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Center_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Extents_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Extents_1 = value;
	}
};


// UnityEngine.UI.ColorBlock
struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};


// System.Globalization.CompareOptions
struct CompareOptions_tD3D7F165240DC4D784A11B1E2F21DC0D6D18E725 
{
public:
	// System.Int32 System.Globalization.CompareOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompareOptions_tD3D7F165240DC4D784A11B1E2F21DC0D6D18E725, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.IO.FileAttributes
struct FileAttributes_t47DBB9A73CF80C7CA21C9AAB8D5336C92D32C1AE 
{
public:
	// System.Int32 System.IO.FileAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAttributes_t47DBB9A73CF80C7CA21C9AAB8D5336C92D32C1AE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.StringComparison
struct StringComparison_tCC9F72B9B1E2C3C6D2566DD0D3A61E1621048998 
{
public:
	// System.Int32 System.StringComparison::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringComparison_tCC9F72B9B1E2C3C6D2566DD0D3A61E1621048998, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TouchScreenKeyboardType
struct TouchScreenKeyboardType_tBD90DFB07923EC19E5EA59FAF26292AC2799A932 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_tBD90DFB07923EC19E5EA59FAF26292AC2799A932, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Environment/SpecialFolder
struct SpecialFolder_t6103ABF21BDF31D4FF825E2761E4616153810B76 
{
public:
	// System.Int32 System.Environment/SpecialFolder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialFolder_t6103ABF21BDF31D4FF825E2761E4616153810B76, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// SimpleFileBrowser.FileBrowser/Permission
struct Permission_t54F789471D32013B167C19F2CD6132E7E49298A2 
{
public:
	// System.Int32 SimpleFileBrowser.FileBrowser/Permission::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Permission_t54F789471D32013B167C19F2CD6132E7E49298A2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// SimpleFileBrowser.FileBrowser/PickMode
struct PickMode_tE2DF59B643089F6FE81412D1C152B52222B0414B 
{
public:
	// System.Int32 SimpleFileBrowser.FileBrowser/PickMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PickMode_tE2DF59B643089F6FE81412D1C152B52222B0414B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OperationType
struct OperationType_t64496608474BBC82B353B35ED92151B4E254A463 
{
public:
	// System.Int32 SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OperationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OperationType_t64496608474BBC82B353B35ED92151B4E254A463, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.InputField/CharacterValidation
struct CharacterValidation_t03AFB752BBD6215579765978CE67D7159431FC41 
{
public:
	// System.Int32 UnityEngine.UI.InputField/CharacterValidation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharacterValidation_t03AFB752BBD6215579765978CE67D7159431FC41, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.InputField/ContentType
struct ContentType_t15FD47A38F32CADD417E3A07C787F1B3997B9AC1 
{
public:
	// System.Int32 UnityEngine.UI.InputField/ContentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ContentType_t15FD47A38F32CADD417E3A07C787F1B3997B9AC1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.InputField/InputType
struct InputType_t43FE97C0C3EE1F7DB81E2F34420780D1DFBF03D2 
{
public:
	// System.Int32 UnityEngine.UI.InputField/InputType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputType_t43FE97C0C3EE1F7DB81E2F34420780D1DFBF03D2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.InputField/LineType
struct LineType_t3249F1C248D9D12DE265C49F371F2C3618AFEFCE 
{
public:
	// System.Int32 UnityEngine.UI.InputField/LineType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineType_t3249F1C248D9D12DE265C49F371F2C3618AFEFCE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.ScrollRect/MovementType
struct MovementType_tAC9293D74600C5C0F8769961576D21C7107BB258 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/MovementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MovementType_tAC9293D74600C5C0F8769961576D21C7107BB258, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.ScrollRect/ScrollbarVisibility
struct ScrollbarVisibility_t8223EB8BD4F3CB01D1A246265D1563AAB5F89F2E 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/ScrollbarVisibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScrollbarVisibility_t8223EB8BD4F3CB01D1A246265D1563AAB5F89F2E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid
struct FBDirectoryReceiveCallbackAndroid_t826BC254F0340D34CA548141A2130BBA31A5C154  : public AndroidJavaProxy_tA8C86826A74CB7CC5511CB353DBA595C9270D9AF
{
public:
	// SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid::callback
	AndroidSAFDirectoryPickCallback_tCB2537E0F2C9160F1890DE73E86301251C6F5504 * ___callback_4;
	// SimpleFileBrowser.FBCallbackHelper SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid::callbackHelper
	FBCallbackHelper_t75E5C6C770413EE5603E2C8C7AA07D21B28CAC4F * ___callbackHelper_5;

public:
	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(FBDirectoryReceiveCallbackAndroid_t826BC254F0340D34CA548141A2130BBA31A5C154, ___callback_4)); }
	inline AndroidSAFDirectoryPickCallback_tCB2537E0F2C9160F1890DE73E86301251C6F5504 * get_callback_4() const { return ___callback_4; }
	inline AndroidSAFDirectoryPickCallback_tCB2537E0F2C9160F1890DE73E86301251C6F5504 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(AndroidSAFDirectoryPickCallback_tCB2537E0F2C9160F1890DE73E86301251C6F5504 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_4), (void*)value);
	}

	inline static int32_t get_offset_of_callbackHelper_5() { return static_cast<int32_t>(offsetof(FBDirectoryReceiveCallbackAndroid_t826BC254F0340D34CA548141A2130BBA31A5C154, ___callbackHelper_5)); }
	inline FBCallbackHelper_t75E5C6C770413EE5603E2C8C7AA07D21B28CAC4F * get_callbackHelper_5() const { return ___callbackHelper_5; }
	inline FBCallbackHelper_t75E5C6C770413EE5603E2C8C7AA07D21B28CAC4F ** get_address_of_callbackHelper_5() { return &___callbackHelper_5; }
	inline void set_callbackHelper_5(FBCallbackHelper_t75E5C6C770413EE5603E2C8C7AA07D21B28CAC4F * value)
	{
		___callbackHelper_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callbackHelper_5), (void*)value);
	}
};


// SimpleFileBrowser.FileSystemEntry
struct FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 
{
public:
	// System.String SimpleFileBrowser.FileSystemEntry::Path
	String_t* ___Path_0;
	// System.String SimpleFileBrowser.FileSystemEntry::Name
	String_t* ___Name_1;
	// System.String SimpleFileBrowser.FileSystemEntry::Extension
	String_t* ___Extension_2;
	// System.IO.FileAttributes SimpleFileBrowser.FileSystemEntry::Attributes
	int32_t ___Attributes_3;

public:
	inline static int32_t get_offset_of_Path_0() { return static_cast<int32_t>(offsetof(FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9, ___Path_0)); }
	inline String_t* get_Path_0() const { return ___Path_0; }
	inline String_t** get_address_of_Path_0() { return &___Path_0; }
	inline void set_Path_0(String_t* value)
	{
		___Path_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Path_0), (void*)value);
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_1), (void*)value);
	}

	inline static int32_t get_offset_of_Extension_2() { return static_cast<int32_t>(offsetof(FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9, ___Extension_2)); }
	inline String_t* get_Extension_2() const { return ___Extension_2; }
	inline String_t** get_address_of_Extension_2() { return &___Extension_2; }
	inline void set_Extension_2(String_t* value)
	{
		___Extension_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Extension_2), (void*)value);
	}

	inline static int32_t get_offset_of_Attributes_3() { return static_cast<int32_t>(offsetof(FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9, ___Attributes_3)); }
	inline int32_t get_Attributes_3() const { return ___Attributes_3; }
	inline int32_t* get_address_of_Attributes_3() { return &___Attributes_3; }
	inline void set_Attributes_3(int32_t value)
	{
		___Attributes_3 = value;
	}
};

// Native definition for P/Invoke marshalling of SimpleFileBrowser.FileSystemEntry
struct FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshaled_pinvoke
{
	char* ___Path_0;
	char* ___Name_1;
	char* ___Extension_2;
	int32_t ___Attributes_3;
};
// Native definition for COM marshalling of SimpleFileBrowser.FileSystemEntry
struct FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshaled_com
{
	Il2CppChar* ___Path_0;
	Il2CppChar* ___Name_1;
	Il2CppChar* ___Extension_2;
	int32_t ___Attributes_3;
};

// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266
struct U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD  : public RuntimeObject
{
public:
	// System.Int32 SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleFileBrowser.FileBrowser/PickMode SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::pickMode
	int32_t ___pickMode_2;
	// System.Boolean SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::allowMultiSelection
	bool ___allowMultiSelection_3;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::initialPath
	String_t* ___initialPath_4;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::initialFilename
	String_t* ___initialFilename_5;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::title
	String_t* ___title_6;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::loadButtonText
	String_t* ___loadButtonText_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_pickMode_2() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD, ___pickMode_2)); }
	inline int32_t get_pickMode_2() const { return ___pickMode_2; }
	inline int32_t* get_address_of_pickMode_2() { return &___pickMode_2; }
	inline void set_pickMode_2(int32_t value)
	{
		___pickMode_2 = value;
	}

	inline static int32_t get_offset_of_allowMultiSelection_3() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD, ___allowMultiSelection_3)); }
	inline bool get_allowMultiSelection_3() const { return ___allowMultiSelection_3; }
	inline bool* get_address_of_allowMultiSelection_3() { return &___allowMultiSelection_3; }
	inline void set_allowMultiSelection_3(bool value)
	{
		___allowMultiSelection_3 = value;
	}

	inline static int32_t get_offset_of_initialPath_4() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD, ___initialPath_4)); }
	inline String_t* get_initialPath_4() const { return ___initialPath_4; }
	inline String_t** get_address_of_initialPath_4() { return &___initialPath_4; }
	inline void set_initialPath_4(String_t* value)
	{
		___initialPath_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___initialPath_4), (void*)value);
	}

	inline static int32_t get_offset_of_initialFilename_5() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD, ___initialFilename_5)); }
	inline String_t* get_initialFilename_5() const { return ___initialFilename_5; }
	inline String_t** get_address_of_initialFilename_5() { return &___initialFilename_5; }
	inline void set_initialFilename_5(String_t* value)
	{
		___initialFilename_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___initialFilename_5), (void*)value);
	}

	inline static int32_t get_offset_of_title_6() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD, ___title_6)); }
	inline String_t* get_title_6() const { return ___title_6; }
	inline String_t** get_address_of_title_6() { return &___title_6; }
	inline void set_title_6(String_t* value)
	{
		___title_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___title_6), (void*)value);
	}

	inline static int32_t get_offset_of_loadButtonText_7() { return static_cast<int32_t>(offsetof(U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD, ___loadButtonText_7)); }
	inline String_t* get_loadButtonText_7() const { return ___loadButtonText_7; }
	inline String_t** get_address_of_loadButtonText_7() { return &___loadButtonText_7; }
	inline void set_loadButtonText_7(String_t* value)
	{
		___loadButtonText_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loadButtonText_7), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265
struct U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602  : public RuntimeObject
{
public:
	// System.Int32 SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SimpleFileBrowser.FileBrowser/PickMode SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::pickMode
	int32_t ___pickMode_2;
	// System.Boolean SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::allowMultiSelection
	bool ___allowMultiSelection_3;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::initialPath
	String_t* ___initialPath_4;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::initialFilename
	String_t* ___initialFilename_5;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::title
	String_t* ___title_6;
	// System.String SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::saveButtonText
	String_t* ___saveButtonText_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_pickMode_2() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602, ___pickMode_2)); }
	inline int32_t get_pickMode_2() const { return ___pickMode_2; }
	inline int32_t* get_address_of_pickMode_2() { return &___pickMode_2; }
	inline void set_pickMode_2(int32_t value)
	{
		___pickMode_2 = value;
	}

	inline static int32_t get_offset_of_allowMultiSelection_3() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602, ___allowMultiSelection_3)); }
	inline bool get_allowMultiSelection_3() const { return ___allowMultiSelection_3; }
	inline bool* get_address_of_allowMultiSelection_3() { return &___allowMultiSelection_3; }
	inline void set_allowMultiSelection_3(bool value)
	{
		___allowMultiSelection_3 = value;
	}

	inline static int32_t get_offset_of_initialPath_4() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602, ___initialPath_4)); }
	inline String_t* get_initialPath_4() const { return ___initialPath_4; }
	inline String_t** get_address_of_initialPath_4() { return &___initialPath_4; }
	inline void set_initialPath_4(String_t* value)
	{
		___initialPath_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___initialPath_4), (void*)value);
	}

	inline static int32_t get_offset_of_initialFilename_5() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602, ___initialFilename_5)); }
	inline String_t* get_initialFilename_5() const { return ___initialFilename_5; }
	inline String_t** get_address_of_initialFilename_5() { return &___initialFilename_5; }
	inline void set_initialFilename_5(String_t* value)
	{
		___initialFilename_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___initialFilename_5), (void*)value);
	}

	inline static int32_t get_offset_of_title_6() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602, ___title_6)); }
	inline String_t* get_title_6() const { return ___title_6; }
	inline String_t** get_address_of_title_6() { return &___title_6; }
	inline void set_title_6(String_t* value)
	{
		___title_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___title_6), (void*)value);
	}

	inline static int32_t get_offset_of_saveButtonText_7() { return static_cast<int32_t>(offsetof(U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602, ___saveButtonText_7)); }
	inline String_t* get_saveButtonText_7() const { return ___saveButtonText_7; }
	inline String_t** get_address_of_saveButtonText_7() { return &___saveButtonText_7; }
	inline void set_saveButtonText_7(String_t* value)
	{
		___saveButtonText_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___saveButtonText_7), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/QuickLink
struct QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7 
{
public:
	// System.Environment/SpecialFolder SimpleFileBrowser.FileBrowser/QuickLink::target
	int32_t ___target_0;
	// System.String SimpleFileBrowser.FileBrowser/QuickLink::name
	String_t* ___name_1;
	// UnityEngine.Sprite SimpleFileBrowser.FileBrowser/QuickLink::icon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___icon_2;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7, ___target_0)); }
	inline int32_t get_target_0() const { return ___target_0; }
	inline int32_t* get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(int32_t value)
	{
		___target_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_1), (void*)value);
	}

	inline static int32_t get_offset_of_icon_2() { return static_cast<int32_t>(offsetof(QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7, ___icon_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_icon_2() const { return ___icon_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_icon_2() { return &___icon_2; }
	inline void set_icon_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___icon_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___icon_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of SimpleFileBrowser.FileBrowser/QuickLink
struct QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshaled_pinvoke
{
	int32_t ___target_0;
	char* ___name_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___icon_2;
};
// Native definition for COM marshalling of SimpleFileBrowser.FileBrowser/QuickLink
struct QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshaled_com
{
	int32_t ___target_0;
	Il2CppChar* ___name_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___icon_2;
};

// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// SimpleFileBrowser.UISkin
struct UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Int32 SimpleFileBrowser.UISkin::m_version
	int32_t ___m_version_4;
	// UnityEngine.Font SimpleFileBrowser.UISkin::m_font
	Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * ___m_font_5;
	// System.Int32 SimpleFileBrowser.UISkin::m_fontSize
	int32_t ___m_fontSize_6;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_windowColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_windowColor_7;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_filesListColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_filesListColor_8;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_filesVerticalSeparatorColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_filesVerticalSeparatorColor_9;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_titleBackgroundColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_titleBackgroundColor_10;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_titleTextColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_titleTextColor_11;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_windowResizeGizmoColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_windowResizeGizmoColor_12;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_headerButtonsColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_headerButtonsColor_13;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_windowResizeGizmo
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_windowResizeGizmo_14;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_headerBackButton
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_headerBackButton_15;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_headerForwardButton
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_headerForwardButton_16;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_headerUpButton
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_headerUpButton_17;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_headerContextMenuButton
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_headerContextMenuButton_18;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_inputFieldNormalBackgroundColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_inputFieldNormalBackgroundColor_19;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_inputFieldInvalidBackgroundColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_inputFieldInvalidBackgroundColor_20;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_inputFieldTextColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_inputFieldTextColor_21;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_inputFieldPlaceholderTextColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_inputFieldPlaceholderTextColor_22;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_inputFieldSelectedTextColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_inputFieldSelectedTextColor_23;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_inputFieldCaretColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_inputFieldCaretColor_24;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_inputFieldBackground
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_inputFieldBackground_25;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_buttonColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_buttonColor_26;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_buttonTextColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_buttonTextColor_27;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_buttonBackground
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_buttonBackground_28;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_dropdownColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_dropdownColor_29;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_dropdownTextColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_dropdownTextColor_30;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_dropdownArrowColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_dropdownArrowColor_31;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_dropdownCheckmarkColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_dropdownCheckmarkColor_32;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_dropdownBackground
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_dropdownBackground_33;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_dropdownArrow
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_dropdownArrow_34;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_dropdownCheckmark
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_dropdownCheckmark_35;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_toggleColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_toggleColor_36;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_toggleTextColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_toggleTextColor_37;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_toggleCheckmarkColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_toggleCheckmarkColor_38;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_toggleBackground
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_toggleBackground_39;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_toggleCheckmark
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_toggleCheckmark_40;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_scrollbarBackgroundColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_scrollbarBackgroundColor_41;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_scrollbarColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_scrollbarColor_42;
	// System.Single SimpleFileBrowser.UISkin::m_fileHeight
	float ___m_fileHeight_43;
	// System.Single SimpleFileBrowser.UISkin::m_fileIconsPadding
	float ___m_fileIconsPadding_44;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_fileNormalBackgroundColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_fileNormalBackgroundColor_45;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_fileAlternatingBackgroundColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_fileAlternatingBackgroundColor_46;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_fileHoveredBackgroundColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_fileHoveredBackgroundColor_47;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_fileSelectedBackgroundColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_fileSelectedBackgroundColor_48;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_fileNormalTextColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_fileNormalTextColor_49;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_fileSelectedTextColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_fileSelectedTextColor_50;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_folderIcon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_folderIcon_51;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_driveIcon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_driveIcon_52;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_defaultFileIcon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_defaultFileIcon_53;
	// SimpleFileBrowser.FiletypeIcon[] SimpleFileBrowser.UISkin::m_filetypeIcons
	FiletypeIconU5BU5D_t426C3221F7B6F0C08FB4C56DF6B5D6284A109758* ___m_filetypeIcons_54;
	// System.Boolean SimpleFileBrowser.UISkin::initializedFiletypeIcons
	bool ___initializedFiletypeIcons_55;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Sprite> SimpleFileBrowser.UISkin::filetypeToIcon
	Dictionary_2_t25CEE683B20ACF6398C3718DF163CF651693326D * ___filetypeToIcon_56;
	// System.Boolean SimpleFileBrowser.UISkin::m_allIconExtensionsHaveSingleSuffix
	bool ___m_allIconExtensionsHaveSingleSuffix_57;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_fileMultiSelectionToggleOffIcon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_fileMultiSelectionToggleOffIcon_58;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_fileMultiSelectionToggleOnIcon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_fileMultiSelectionToggleOnIcon_59;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_contextMenuBackgroundColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_contextMenuBackgroundColor_60;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_contextMenuTextColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_contextMenuTextColor_61;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_contextMenuSeparatorColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_contextMenuSeparatorColor_62;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_popupPanelsBackgroundColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_popupPanelsBackgroundColor_63;
	// UnityEngine.Color SimpleFileBrowser.UISkin::m_popupPanelsTextColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_popupPanelsTextColor_64;
	// UnityEngine.Sprite SimpleFileBrowser.UISkin::m_popupPanelsBackground
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_popupPanelsBackground_65;

public:
	inline static int32_t get_offset_of_m_version_4() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_version_4)); }
	inline int32_t get_m_version_4() const { return ___m_version_4; }
	inline int32_t* get_address_of_m_version_4() { return &___m_version_4; }
	inline void set_m_version_4(int32_t value)
	{
		___m_version_4 = value;
	}

	inline static int32_t get_offset_of_m_font_5() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_font_5)); }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * get_m_font_5() const { return ___m_font_5; }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 ** get_address_of_m_font_5() { return &___m_font_5; }
	inline void set_m_font_5(Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * value)
	{
		___m_font_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_font_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_fontSize_6() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_fontSize_6)); }
	inline int32_t get_m_fontSize_6() const { return ___m_fontSize_6; }
	inline int32_t* get_address_of_m_fontSize_6() { return &___m_fontSize_6; }
	inline void set_m_fontSize_6(int32_t value)
	{
		___m_fontSize_6 = value;
	}

	inline static int32_t get_offset_of_m_windowColor_7() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_windowColor_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_windowColor_7() const { return ___m_windowColor_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_windowColor_7() { return &___m_windowColor_7; }
	inline void set_m_windowColor_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_windowColor_7 = value;
	}

	inline static int32_t get_offset_of_m_filesListColor_8() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_filesListColor_8)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_filesListColor_8() const { return ___m_filesListColor_8; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_filesListColor_8() { return &___m_filesListColor_8; }
	inline void set_m_filesListColor_8(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_filesListColor_8 = value;
	}

	inline static int32_t get_offset_of_m_filesVerticalSeparatorColor_9() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_filesVerticalSeparatorColor_9)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_filesVerticalSeparatorColor_9() const { return ___m_filesVerticalSeparatorColor_9; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_filesVerticalSeparatorColor_9() { return &___m_filesVerticalSeparatorColor_9; }
	inline void set_m_filesVerticalSeparatorColor_9(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_filesVerticalSeparatorColor_9 = value;
	}

	inline static int32_t get_offset_of_m_titleBackgroundColor_10() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_titleBackgroundColor_10)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_titleBackgroundColor_10() const { return ___m_titleBackgroundColor_10; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_titleBackgroundColor_10() { return &___m_titleBackgroundColor_10; }
	inline void set_m_titleBackgroundColor_10(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_titleBackgroundColor_10 = value;
	}

	inline static int32_t get_offset_of_m_titleTextColor_11() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_titleTextColor_11)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_titleTextColor_11() const { return ___m_titleTextColor_11; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_titleTextColor_11() { return &___m_titleTextColor_11; }
	inline void set_m_titleTextColor_11(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_titleTextColor_11 = value;
	}

	inline static int32_t get_offset_of_m_windowResizeGizmoColor_12() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_windowResizeGizmoColor_12)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_windowResizeGizmoColor_12() const { return ___m_windowResizeGizmoColor_12; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_windowResizeGizmoColor_12() { return &___m_windowResizeGizmoColor_12; }
	inline void set_m_windowResizeGizmoColor_12(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_windowResizeGizmoColor_12 = value;
	}

	inline static int32_t get_offset_of_m_headerButtonsColor_13() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_headerButtonsColor_13)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_headerButtonsColor_13() const { return ___m_headerButtonsColor_13; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_headerButtonsColor_13() { return &___m_headerButtonsColor_13; }
	inline void set_m_headerButtonsColor_13(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_headerButtonsColor_13 = value;
	}

	inline static int32_t get_offset_of_m_windowResizeGizmo_14() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_windowResizeGizmo_14)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_windowResizeGizmo_14() const { return ___m_windowResizeGizmo_14; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_windowResizeGizmo_14() { return &___m_windowResizeGizmo_14; }
	inline void set_m_windowResizeGizmo_14(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_windowResizeGizmo_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_windowResizeGizmo_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_headerBackButton_15() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_headerBackButton_15)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_headerBackButton_15() const { return ___m_headerBackButton_15; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_headerBackButton_15() { return &___m_headerBackButton_15; }
	inline void set_m_headerBackButton_15(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_headerBackButton_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_headerBackButton_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_headerForwardButton_16() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_headerForwardButton_16)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_headerForwardButton_16() const { return ___m_headerForwardButton_16; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_headerForwardButton_16() { return &___m_headerForwardButton_16; }
	inline void set_m_headerForwardButton_16(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_headerForwardButton_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_headerForwardButton_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_headerUpButton_17() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_headerUpButton_17)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_headerUpButton_17() const { return ___m_headerUpButton_17; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_headerUpButton_17() { return &___m_headerUpButton_17; }
	inline void set_m_headerUpButton_17(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_headerUpButton_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_headerUpButton_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_headerContextMenuButton_18() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_headerContextMenuButton_18)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_headerContextMenuButton_18() const { return ___m_headerContextMenuButton_18; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_headerContextMenuButton_18() { return &___m_headerContextMenuButton_18; }
	inline void set_m_headerContextMenuButton_18(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_headerContextMenuButton_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_headerContextMenuButton_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_inputFieldNormalBackgroundColor_19() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_inputFieldNormalBackgroundColor_19)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_inputFieldNormalBackgroundColor_19() const { return ___m_inputFieldNormalBackgroundColor_19; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_inputFieldNormalBackgroundColor_19() { return &___m_inputFieldNormalBackgroundColor_19; }
	inline void set_m_inputFieldNormalBackgroundColor_19(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_inputFieldNormalBackgroundColor_19 = value;
	}

	inline static int32_t get_offset_of_m_inputFieldInvalidBackgroundColor_20() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_inputFieldInvalidBackgroundColor_20)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_inputFieldInvalidBackgroundColor_20() const { return ___m_inputFieldInvalidBackgroundColor_20; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_inputFieldInvalidBackgroundColor_20() { return &___m_inputFieldInvalidBackgroundColor_20; }
	inline void set_m_inputFieldInvalidBackgroundColor_20(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_inputFieldInvalidBackgroundColor_20 = value;
	}

	inline static int32_t get_offset_of_m_inputFieldTextColor_21() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_inputFieldTextColor_21)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_inputFieldTextColor_21() const { return ___m_inputFieldTextColor_21; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_inputFieldTextColor_21() { return &___m_inputFieldTextColor_21; }
	inline void set_m_inputFieldTextColor_21(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_inputFieldTextColor_21 = value;
	}

	inline static int32_t get_offset_of_m_inputFieldPlaceholderTextColor_22() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_inputFieldPlaceholderTextColor_22)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_inputFieldPlaceholderTextColor_22() const { return ___m_inputFieldPlaceholderTextColor_22; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_inputFieldPlaceholderTextColor_22() { return &___m_inputFieldPlaceholderTextColor_22; }
	inline void set_m_inputFieldPlaceholderTextColor_22(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_inputFieldPlaceholderTextColor_22 = value;
	}

	inline static int32_t get_offset_of_m_inputFieldSelectedTextColor_23() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_inputFieldSelectedTextColor_23)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_inputFieldSelectedTextColor_23() const { return ___m_inputFieldSelectedTextColor_23; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_inputFieldSelectedTextColor_23() { return &___m_inputFieldSelectedTextColor_23; }
	inline void set_m_inputFieldSelectedTextColor_23(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_inputFieldSelectedTextColor_23 = value;
	}

	inline static int32_t get_offset_of_m_inputFieldCaretColor_24() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_inputFieldCaretColor_24)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_inputFieldCaretColor_24() const { return ___m_inputFieldCaretColor_24; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_inputFieldCaretColor_24() { return &___m_inputFieldCaretColor_24; }
	inline void set_m_inputFieldCaretColor_24(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_inputFieldCaretColor_24 = value;
	}

	inline static int32_t get_offset_of_m_inputFieldBackground_25() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_inputFieldBackground_25)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_inputFieldBackground_25() const { return ___m_inputFieldBackground_25; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_inputFieldBackground_25() { return &___m_inputFieldBackground_25; }
	inline void set_m_inputFieldBackground_25(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_inputFieldBackground_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_inputFieldBackground_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_buttonColor_26() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_buttonColor_26)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_buttonColor_26() const { return ___m_buttonColor_26; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_buttonColor_26() { return &___m_buttonColor_26; }
	inline void set_m_buttonColor_26(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_buttonColor_26 = value;
	}

	inline static int32_t get_offset_of_m_buttonTextColor_27() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_buttonTextColor_27)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_buttonTextColor_27() const { return ___m_buttonTextColor_27; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_buttonTextColor_27() { return &___m_buttonTextColor_27; }
	inline void set_m_buttonTextColor_27(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_buttonTextColor_27 = value;
	}

	inline static int32_t get_offset_of_m_buttonBackground_28() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_buttonBackground_28)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_buttonBackground_28() const { return ___m_buttonBackground_28; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_buttonBackground_28() { return &___m_buttonBackground_28; }
	inline void set_m_buttonBackground_28(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_buttonBackground_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_buttonBackground_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_dropdownColor_29() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_dropdownColor_29)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_dropdownColor_29() const { return ___m_dropdownColor_29; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_dropdownColor_29() { return &___m_dropdownColor_29; }
	inline void set_m_dropdownColor_29(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_dropdownColor_29 = value;
	}

	inline static int32_t get_offset_of_m_dropdownTextColor_30() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_dropdownTextColor_30)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_dropdownTextColor_30() const { return ___m_dropdownTextColor_30; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_dropdownTextColor_30() { return &___m_dropdownTextColor_30; }
	inline void set_m_dropdownTextColor_30(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_dropdownTextColor_30 = value;
	}

	inline static int32_t get_offset_of_m_dropdownArrowColor_31() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_dropdownArrowColor_31)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_dropdownArrowColor_31() const { return ___m_dropdownArrowColor_31; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_dropdownArrowColor_31() { return &___m_dropdownArrowColor_31; }
	inline void set_m_dropdownArrowColor_31(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_dropdownArrowColor_31 = value;
	}

	inline static int32_t get_offset_of_m_dropdownCheckmarkColor_32() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_dropdownCheckmarkColor_32)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_dropdownCheckmarkColor_32() const { return ___m_dropdownCheckmarkColor_32; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_dropdownCheckmarkColor_32() { return &___m_dropdownCheckmarkColor_32; }
	inline void set_m_dropdownCheckmarkColor_32(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_dropdownCheckmarkColor_32 = value;
	}

	inline static int32_t get_offset_of_m_dropdownBackground_33() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_dropdownBackground_33)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_dropdownBackground_33() const { return ___m_dropdownBackground_33; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_dropdownBackground_33() { return &___m_dropdownBackground_33; }
	inline void set_m_dropdownBackground_33(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_dropdownBackground_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_dropdownBackground_33), (void*)value);
	}

	inline static int32_t get_offset_of_m_dropdownArrow_34() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_dropdownArrow_34)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_dropdownArrow_34() const { return ___m_dropdownArrow_34; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_dropdownArrow_34() { return &___m_dropdownArrow_34; }
	inline void set_m_dropdownArrow_34(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_dropdownArrow_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_dropdownArrow_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_dropdownCheckmark_35() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_dropdownCheckmark_35)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_dropdownCheckmark_35() const { return ___m_dropdownCheckmark_35; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_dropdownCheckmark_35() { return &___m_dropdownCheckmark_35; }
	inline void set_m_dropdownCheckmark_35(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_dropdownCheckmark_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_dropdownCheckmark_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_toggleColor_36() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_toggleColor_36)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_toggleColor_36() const { return ___m_toggleColor_36; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_toggleColor_36() { return &___m_toggleColor_36; }
	inline void set_m_toggleColor_36(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_toggleColor_36 = value;
	}

	inline static int32_t get_offset_of_m_toggleTextColor_37() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_toggleTextColor_37)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_toggleTextColor_37() const { return ___m_toggleTextColor_37; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_toggleTextColor_37() { return &___m_toggleTextColor_37; }
	inline void set_m_toggleTextColor_37(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_toggleTextColor_37 = value;
	}

	inline static int32_t get_offset_of_m_toggleCheckmarkColor_38() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_toggleCheckmarkColor_38)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_toggleCheckmarkColor_38() const { return ___m_toggleCheckmarkColor_38; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_toggleCheckmarkColor_38() { return &___m_toggleCheckmarkColor_38; }
	inline void set_m_toggleCheckmarkColor_38(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_toggleCheckmarkColor_38 = value;
	}

	inline static int32_t get_offset_of_m_toggleBackground_39() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_toggleBackground_39)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_toggleBackground_39() const { return ___m_toggleBackground_39; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_toggleBackground_39() { return &___m_toggleBackground_39; }
	inline void set_m_toggleBackground_39(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_toggleBackground_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_toggleBackground_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_toggleCheckmark_40() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_toggleCheckmark_40)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_toggleCheckmark_40() const { return ___m_toggleCheckmark_40; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_toggleCheckmark_40() { return &___m_toggleCheckmark_40; }
	inline void set_m_toggleCheckmark_40(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_toggleCheckmark_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_toggleCheckmark_40), (void*)value);
	}

	inline static int32_t get_offset_of_m_scrollbarBackgroundColor_41() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_scrollbarBackgroundColor_41)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_scrollbarBackgroundColor_41() const { return ___m_scrollbarBackgroundColor_41; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_scrollbarBackgroundColor_41() { return &___m_scrollbarBackgroundColor_41; }
	inline void set_m_scrollbarBackgroundColor_41(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_scrollbarBackgroundColor_41 = value;
	}

	inline static int32_t get_offset_of_m_scrollbarColor_42() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_scrollbarColor_42)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_scrollbarColor_42() const { return ___m_scrollbarColor_42; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_scrollbarColor_42() { return &___m_scrollbarColor_42; }
	inline void set_m_scrollbarColor_42(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_scrollbarColor_42 = value;
	}

	inline static int32_t get_offset_of_m_fileHeight_43() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_fileHeight_43)); }
	inline float get_m_fileHeight_43() const { return ___m_fileHeight_43; }
	inline float* get_address_of_m_fileHeight_43() { return &___m_fileHeight_43; }
	inline void set_m_fileHeight_43(float value)
	{
		___m_fileHeight_43 = value;
	}

	inline static int32_t get_offset_of_m_fileIconsPadding_44() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_fileIconsPadding_44)); }
	inline float get_m_fileIconsPadding_44() const { return ___m_fileIconsPadding_44; }
	inline float* get_address_of_m_fileIconsPadding_44() { return &___m_fileIconsPadding_44; }
	inline void set_m_fileIconsPadding_44(float value)
	{
		___m_fileIconsPadding_44 = value;
	}

	inline static int32_t get_offset_of_m_fileNormalBackgroundColor_45() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_fileNormalBackgroundColor_45)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_fileNormalBackgroundColor_45() const { return ___m_fileNormalBackgroundColor_45; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_fileNormalBackgroundColor_45() { return &___m_fileNormalBackgroundColor_45; }
	inline void set_m_fileNormalBackgroundColor_45(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_fileNormalBackgroundColor_45 = value;
	}

	inline static int32_t get_offset_of_m_fileAlternatingBackgroundColor_46() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_fileAlternatingBackgroundColor_46)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_fileAlternatingBackgroundColor_46() const { return ___m_fileAlternatingBackgroundColor_46; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_fileAlternatingBackgroundColor_46() { return &___m_fileAlternatingBackgroundColor_46; }
	inline void set_m_fileAlternatingBackgroundColor_46(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_fileAlternatingBackgroundColor_46 = value;
	}

	inline static int32_t get_offset_of_m_fileHoveredBackgroundColor_47() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_fileHoveredBackgroundColor_47)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_fileHoveredBackgroundColor_47() const { return ___m_fileHoveredBackgroundColor_47; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_fileHoveredBackgroundColor_47() { return &___m_fileHoveredBackgroundColor_47; }
	inline void set_m_fileHoveredBackgroundColor_47(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_fileHoveredBackgroundColor_47 = value;
	}

	inline static int32_t get_offset_of_m_fileSelectedBackgroundColor_48() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_fileSelectedBackgroundColor_48)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_fileSelectedBackgroundColor_48() const { return ___m_fileSelectedBackgroundColor_48; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_fileSelectedBackgroundColor_48() { return &___m_fileSelectedBackgroundColor_48; }
	inline void set_m_fileSelectedBackgroundColor_48(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_fileSelectedBackgroundColor_48 = value;
	}

	inline static int32_t get_offset_of_m_fileNormalTextColor_49() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_fileNormalTextColor_49)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_fileNormalTextColor_49() const { return ___m_fileNormalTextColor_49; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_fileNormalTextColor_49() { return &___m_fileNormalTextColor_49; }
	inline void set_m_fileNormalTextColor_49(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_fileNormalTextColor_49 = value;
	}

	inline static int32_t get_offset_of_m_fileSelectedTextColor_50() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_fileSelectedTextColor_50)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_fileSelectedTextColor_50() const { return ___m_fileSelectedTextColor_50; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_fileSelectedTextColor_50() { return &___m_fileSelectedTextColor_50; }
	inline void set_m_fileSelectedTextColor_50(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_fileSelectedTextColor_50 = value;
	}

	inline static int32_t get_offset_of_m_folderIcon_51() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_folderIcon_51)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_folderIcon_51() const { return ___m_folderIcon_51; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_folderIcon_51() { return &___m_folderIcon_51; }
	inline void set_m_folderIcon_51(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_folderIcon_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_folderIcon_51), (void*)value);
	}

	inline static int32_t get_offset_of_m_driveIcon_52() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_driveIcon_52)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_driveIcon_52() const { return ___m_driveIcon_52; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_driveIcon_52() { return &___m_driveIcon_52; }
	inline void set_m_driveIcon_52(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_driveIcon_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_driveIcon_52), (void*)value);
	}

	inline static int32_t get_offset_of_m_defaultFileIcon_53() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_defaultFileIcon_53)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_defaultFileIcon_53() const { return ___m_defaultFileIcon_53; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_defaultFileIcon_53() { return &___m_defaultFileIcon_53; }
	inline void set_m_defaultFileIcon_53(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_defaultFileIcon_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_defaultFileIcon_53), (void*)value);
	}

	inline static int32_t get_offset_of_m_filetypeIcons_54() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_filetypeIcons_54)); }
	inline FiletypeIconU5BU5D_t426C3221F7B6F0C08FB4C56DF6B5D6284A109758* get_m_filetypeIcons_54() const { return ___m_filetypeIcons_54; }
	inline FiletypeIconU5BU5D_t426C3221F7B6F0C08FB4C56DF6B5D6284A109758** get_address_of_m_filetypeIcons_54() { return &___m_filetypeIcons_54; }
	inline void set_m_filetypeIcons_54(FiletypeIconU5BU5D_t426C3221F7B6F0C08FB4C56DF6B5D6284A109758* value)
	{
		___m_filetypeIcons_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_filetypeIcons_54), (void*)value);
	}

	inline static int32_t get_offset_of_initializedFiletypeIcons_55() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___initializedFiletypeIcons_55)); }
	inline bool get_initializedFiletypeIcons_55() const { return ___initializedFiletypeIcons_55; }
	inline bool* get_address_of_initializedFiletypeIcons_55() { return &___initializedFiletypeIcons_55; }
	inline void set_initializedFiletypeIcons_55(bool value)
	{
		___initializedFiletypeIcons_55 = value;
	}

	inline static int32_t get_offset_of_filetypeToIcon_56() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___filetypeToIcon_56)); }
	inline Dictionary_2_t25CEE683B20ACF6398C3718DF163CF651693326D * get_filetypeToIcon_56() const { return ___filetypeToIcon_56; }
	inline Dictionary_2_t25CEE683B20ACF6398C3718DF163CF651693326D ** get_address_of_filetypeToIcon_56() { return &___filetypeToIcon_56; }
	inline void set_filetypeToIcon_56(Dictionary_2_t25CEE683B20ACF6398C3718DF163CF651693326D * value)
	{
		___filetypeToIcon_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filetypeToIcon_56), (void*)value);
	}

	inline static int32_t get_offset_of_m_allIconExtensionsHaveSingleSuffix_57() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_allIconExtensionsHaveSingleSuffix_57)); }
	inline bool get_m_allIconExtensionsHaveSingleSuffix_57() const { return ___m_allIconExtensionsHaveSingleSuffix_57; }
	inline bool* get_address_of_m_allIconExtensionsHaveSingleSuffix_57() { return &___m_allIconExtensionsHaveSingleSuffix_57; }
	inline void set_m_allIconExtensionsHaveSingleSuffix_57(bool value)
	{
		___m_allIconExtensionsHaveSingleSuffix_57 = value;
	}

	inline static int32_t get_offset_of_m_fileMultiSelectionToggleOffIcon_58() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_fileMultiSelectionToggleOffIcon_58)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_fileMultiSelectionToggleOffIcon_58() const { return ___m_fileMultiSelectionToggleOffIcon_58; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_fileMultiSelectionToggleOffIcon_58() { return &___m_fileMultiSelectionToggleOffIcon_58; }
	inline void set_m_fileMultiSelectionToggleOffIcon_58(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_fileMultiSelectionToggleOffIcon_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_fileMultiSelectionToggleOffIcon_58), (void*)value);
	}

	inline static int32_t get_offset_of_m_fileMultiSelectionToggleOnIcon_59() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_fileMultiSelectionToggleOnIcon_59)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_fileMultiSelectionToggleOnIcon_59() const { return ___m_fileMultiSelectionToggleOnIcon_59; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_fileMultiSelectionToggleOnIcon_59() { return &___m_fileMultiSelectionToggleOnIcon_59; }
	inline void set_m_fileMultiSelectionToggleOnIcon_59(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_fileMultiSelectionToggleOnIcon_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_fileMultiSelectionToggleOnIcon_59), (void*)value);
	}

	inline static int32_t get_offset_of_m_contextMenuBackgroundColor_60() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_contextMenuBackgroundColor_60)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_contextMenuBackgroundColor_60() const { return ___m_contextMenuBackgroundColor_60; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_contextMenuBackgroundColor_60() { return &___m_contextMenuBackgroundColor_60; }
	inline void set_m_contextMenuBackgroundColor_60(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_contextMenuBackgroundColor_60 = value;
	}

	inline static int32_t get_offset_of_m_contextMenuTextColor_61() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_contextMenuTextColor_61)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_contextMenuTextColor_61() const { return ___m_contextMenuTextColor_61; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_contextMenuTextColor_61() { return &___m_contextMenuTextColor_61; }
	inline void set_m_contextMenuTextColor_61(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_contextMenuTextColor_61 = value;
	}

	inline static int32_t get_offset_of_m_contextMenuSeparatorColor_62() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_contextMenuSeparatorColor_62)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_contextMenuSeparatorColor_62() const { return ___m_contextMenuSeparatorColor_62; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_contextMenuSeparatorColor_62() { return &___m_contextMenuSeparatorColor_62; }
	inline void set_m_contextMenuSeparatorColor_62(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_contextMenuSeparatorColor_62 = value;
	}

	inline static int32_t get_offset_of_m_popupPanelsBackgroundColor_63() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_popupPanelsBackgroundColor_63)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_popupPanelsBackgroundColor_63() const { return ___m_popupPanelsBackgroundColor_63; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_popupPanelsBackgroundColor_63() { return &___m_popupPanelsBackgroundColor_63; }
	inline void set_m_popupPanelsBackgroundColor_63(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_popupPanelsBackgroundColor_63 = value;
	}

	inline static int32_t get_offset_of_m_popupPanelsTextColor_64() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_popupPanelsTextColor_64)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_popupPanelsTextColor_64() const { return ___m_popupPanelsTextColor_64; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_popupPanelsTextColor_64() { return &___m_popupPanelsTextColor_64; }
	inline void set_m_popupPanelsTextColor_64(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_popupPanelsTextColor_64 = value;
	}

	inline static int32_t get_offset_of_m_popupPanelsBackground_65() { return static_cast<int32_t>(offsetof(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58, ___m_popupPanelsBackground_65)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_popupPanelsBackground_65() const { return ___m_popupPanelsBackground_65; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_popupPanelsBackground_65() { return &___m_popupPanelsBackground_65; }
	inline void set_m_popupPanelsBackground_65(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_popupPanelsBackground_65 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_popupPanelsBackground_65), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/<>c__DisplayClass242_0
struct U3CU3Ec__DisplayClass242_0_t5A54DD142E562A9BF717F627B31C8F71C0E0FFDE  : public RuntimeObject
{
public:
	// SimpleFileBrowser.FileSystemEntry SimpleFileBrowser.FileBrowser/<>c__DisplayClass242_0::fileInfo
	FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  ___fileInfo_0;
	// SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowser/<>c__DisplayClass242_0::<>4__this
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_fileInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass242_0_t5A54DD142E562A9BF717F627B31C8F71C0E0FFDE, ___fileInfo_0)); }
	inline FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  get_fileInfo_0() const { return ___fileInfo_0; }
	inline FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 * get_address_of_fileInfo_0() { return &___fileInfo_0; }
	inline void set_fileInfo_0(FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  value)
	{
		___fileInfo_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___fileInfo_0))->___Path_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___fileInfo_0))->___Name_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___fileInfo_0))->___Extension_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass242_0_t5A54DD142E562A9BF717F627B31C8F71C0E0FFDE, ___U3CU3E4__this_1)); }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback
struct AndroidSAFDirectoryPickCallback_tCB2537E0F2C9160F1890DE73E86301251C6F5504  : public MulticastDelegate_t
{
public:

public:
};


// SimpleFileBrowser.FileBrowser/FileSystemEntryFilter
struct FileSystemEntryFilter_t570DC98294020ED04118FDDBAA83AF79C201E4E1  : public MulticastDelegate_t
{
public:

public:
};


// SimpleFileBrowser.FileBrowser/OnCancel
struct OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E  : public MulticastDelegate_t
{
public:

public:
};


// SimpleFileBrowser.FileBrowser/OnSuccess
struct OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C  : public MulticastDelegate_t
{
public:

public:
};


// SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed
struct OnOperationConfirmed_tA108D4E3303410B9B615F6D94A787ABC68C137EB  : public MulticastDelegate_t
{
public:

public:
};


// SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted
struct OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};


// SimpleFileBrowser.FileBrowser
struct FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// SimpleFileBrowser.UISkin SimpleFileBrowser.FileBrowser::m_skin
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * ___m_skin_9;
	// System.Int32 SimpleFileBrowser.FileBrowser::m_skinVersion
	int32_t ___m_skinVersion_10;
	// UnityEngine.Sprite SimpleFileBrowser.FileBrowser::m_skinPrevDriveIcon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_skinPrevDriveIcon_11;
	// UnityEngine.Sprite SimpleFileBrowser.FileBrowser::m_skinPrevFolderIcon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_skinPrevFolderIcon_12;
	// System.Int32 SimpleFileBrowser.FileBrowser::minWidth
	int32_t ___minWidth_24;
	// System.Int32 SimpleFileBrowser.FileBrowser::minHeight
	int32_t ___minHeight_25;
	// System.Single SimpleFileBrowser.FileBrowser::narrowScreenWidth
	float ___narrowScreenWidth_26;
	// System.Single SimpleFileBrowser.FileBrowser::quickLinksMaxWidthPercentage
	float ___quickLinksMaxWidthPercentage_27;
	// System.Boolean SimpleFileBrowser.FileBrowser::sortFilesByName
	bool ___sortFilesByName_28;
	// System.String[] SimpleFileBrowser.FileBrowser::excludedExtensions
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___excludedExtensions_29;
	// SimpleFileBrowser.FileBrowser/QuickLink[] SimpleFileBrowser.FileBrowser::quickLinks
	QuickLinkU5BU5D_t204AB6BB1B0F422BCD2C3DC7A1F93FDDAFB416BC* ___quickLinks_30;
	// System.Collections.Generic.HashSet`1<System.String> SimpleFileBrowser.FileBrowser::excludedExtensionsSet
	HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * ___excludedExtensionsSet_32;
	// System.Boolean SimpleFileBrowser.FileBrowser::generateQuickLinksForDrives
	bool ___generateQuickLinksForDrives_33;
	// System.Boolean SimpleFileBrowser.FileBrowser::contextMenuShowDeleteButton
	bool ___contextMenuShowDeleteButton_34;
	// System.Boolean SimpleFileBrowser.FileBrowser::contextMenuShowRenameButton
	bool ___contextMenuShowRenameButton_35;
	// System.Boolean SimpleFileBrowser.FileBrowser::showResizeCursor
	bool ___showResizeCursor_36;
	// SimpleFileBrowser.FileBrowserMovement SimpleFileBrowser.FileBrowser::window
	FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19 * ___window_37;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::windowTR
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___windowTR_38;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::topViewNarrowScreen
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___topViewNarrowScreen_39;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::middleView
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___middleView_40;
	// UnityEngine.Vector2 SimpleFileBrowser.FileBrowser::middleViewOriginalPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___middleViewOriginalPosition_41;
	// UnityEngine.Vector2 SimpleFileBrowser.FileBrowser::middleViewOriginalSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___middleViewOriginalSize_42;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::middleViewQuickLinks
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___middleViewQuickLinks_43;
	// UnityEngine.Vector2 SimpleFileBrowser.FileBrowser::middleViewQuickLinksOriginalSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___middleViewQuickLinksOriginalSize_44;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::middleViewFiles
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___middleViewFiles_45;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::middleViewSeparator
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___middleViewSeparator_46;
	// SimpleFileBrowser.FileBrowserItem SimpleFileBrowser.FileBrowser::itemPrefab
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025 * ___itemPrefab_47;
	// System.Collections.Generic.List`1<SimpleFileBrowser.FileBrowserItem> SimpleFileBrowser.FileBrowser::allItems
	List_1_tC177D10798BB0E73A3C6EF511075D40316BD5CB7 * ___allItems_48;
	// SimpleFileBrowser.FileBrowserQuickLink SimpleFileBrowser.FileBrowser::quickLinkPrefab
	FileBrowserQuickLink_t3DDB6D8FB4E5B8A77D0401284C0071037161A08C * ___quickLinkPrefab_49;
	// System.Collections.Generic.List`1<SimpleFileBrowser.FileBrowserQuickLink> SimpleFileBrowser.FileBrowser::allQuickLinks
	List_1_t612A2A8CBA335BE2987BF3A35E0D00D10BC40A2F * ___allQuickLinks_50;
	// UnityEngine.UI.Text SimpleFileBrowser.FileBrowser::titleText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___titleText_51;
	// UnityEngine.UI.Button SimpleFileBrowser.FileBrowser::backButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___backButton_52;
	// UnityEngine.UI.Button SimpleFileBrowser.FileBrowser::forwardButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___forwardButton_53;
	// UnityEngine.UI.Button SimpleFileBrowser.FileBrowser::upButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___upButton_54;
	// UnityEngine.UI.Button SimpleFileBrowser.FileBrowser::moreOptionsButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___moreOptionsButton_55;
	// UnityEngine.UI.InputField SimpleFileBrowser.FileBrowser::pathInputField
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___pathInputField_56;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::pathInputFieldSlotTop
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___pathInputFieldSlotTop_57;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::pathInputFieldSlotBottom
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___pathInputFieldSlotBottom_58;
	// UnityEngine.UI.InputField SimpleFileBrowser.FileBrowser::searchInputField
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___searchInputField_59;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::quickLinksContainer
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___quickLinksContainer_60;
	// UnityEngine.UI.ScrollRect SimpleFileBrowser.FileBrowser::quickLinksScrollRect
	ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * ___quickLinksScrollRect_61;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::filesContainer
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___filesContainer_62;
	// UnityEngine.UI.ScrollRect SimpleFileBrowser.FileBrowser::filesScrollRect
	ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * ___filesScrollRect_63;
	// SimpleFileBrowser.RecycledListView SimpleFileBrowser.FileBrowser::listView
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 * ___listView_64;
	// UnityEngine.UI.InputField SimpleFileBrowser.FileBrowser::filenameInputField
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___filenameInputField_65;
	// UnityEngine.UI.Text SimpleFileBrowser.FileBrowser::filenameInputFieldOverlayText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___filenameInputFieldOverlayText_66;
	// UnityEngine.UI.Image SimpleFileBrowser.FileBrowser::filenameImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___filenameImage_67;
	// UnityEngine.UI.Dropdown SimpleFileBrowser.FileBrowser::filtersDropdown
	Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * ___filtersDropdown_68;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::filtersDropdownContainer
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___filtersDropdownContainer_69;
	// UnityEngine.UI.Text SimpleFileBrowser.FileBrowser::filterItemTemplate
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___filterItemTemplate_70;
	// UnityEngine.UI.Toggle SimpleFileBrowser.FileBrowser::showHiddenFilesToggle
	Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * ___showHiddenFilesToggle_71;
	// UnityEngine.UI.Text SimpleFileBrowser.FileBrowser::submitButtonText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___submitButtonText_72;
	// UnityEngine.UI.Button[] SimpleFileBrowser.FileBrowser::allButtons
	ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* ___allButtons_73;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::moreOptionsContextMenuPosition
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___moreOptionsContextMenuPosition_74;
	// SimpleFileBrowser.FileBrowserRenamedItem SimpleFileBrowser.FileBrowser::renameItem
	FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4 * ___renameItem_75;
	// SimpleFileBrowser.FileBrowserContextMenu SimpleFileBrowser.FileBrowser::contextMenu
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC * ___contextMenu_76;
	// SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel SimpleFileBrowser.FileBrowser::fileOperationConfirmationPanel
	FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229 * ___fileOperationConfirmationPanel_77;
	// SimpleFileBrowser.FileBrowserAccessRestrictedPanel SimpleFileBrowser.FileBrowser::accessRestrictedPanel
	FileBrowserAccessRestrictedPanel_t96A484C5166B6EF23402D06FF2107AC3ED6ECB30 * ___accessRestrictedPanel_78;
	// SimpleFileBrowser.FileBrowserCursorHandler SimpleFileBrowser.FileBrowser::resizeCursorHandler
	FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C * ___resizeCursorHandler_79;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowser::rectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___rectTransform_80;
	// UnityEngine.Canvas SimpleFileBrowser.FileBrowser::canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___canvas_81;
	// System.IO.FileAttributes SimpleFileBrowser.FileBrowser::ignoredFileAttributes
	int32_t ___ignoredFileAttributes_82;
	// SimpleFileBrowser.FileSystemEntry[] SimpleFileBrowser.FileBrowser::allFileEntries
	FileSystemEntryU5BU5D_t457BF0158EF6A2EEFB557C493617F0B87ED6B7A3* ___allFileEntries_83;
	// System.Collections.Generic.List`1<SimpleFileBrowser.FileSystemEntry> SimpleFileBrowser.FileBrowser::validFileEntries
	List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB * ___validFileEntries_84;
	// System.Collections.Generic.List`1<System.Int32> SimpleFileBrowser.FileBrowser::selectedFileEntries
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___selectedFileEntries_85;
	// System.Collections.Generic.List`1<System.String> SimpleFileBrowser.FileBrowser::pendingFileEntrySelection
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___pendingFileEntrySelection_86;
	// System.Collections.Generic.List`1<System.String> SimpleFileBrowser.FileBrowser::submittedFileEntryPaths
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___submittedFileEntryPaths_87;
	// System.Collections.Generic.List`1<System.String> SimpleFileBrowser.FileBrowser::submittedFolderPaths
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___submittedFolderPaths_88;
	// System.Collections.Generic.List`1<SimpleFileBrowser.FileSystemEntry> SimpleFileBrowser.FileBrowser::submittedFileEntriesToOverwrite
	List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB * ___submittedFileEntriesToOverwrite_89;
	// System.Int32 SimpleFileBrowser.FileBrowser::multiSelectionPivotFileEntry
	int32_t ___multiSelectionPivotFileEntry_90;
	// System.Text.StringBuilder SimpleFileBrowser.FileBrowser::multiSelectionFilenameBuilder
	StringBuilder_t * ___multiSelectionFilenameBuilder_91;
	// System.Collections.Generic.List`1<SimpleFileBrowser.FileBrowser/Filter> SimpleFileBrowser.FileBrowser::filters
	List_1_tCEE47E7A624FD9E913773D93A05E7DE44BC7D35A * ___filters_92;
	// SimpleFileBrowser.FileBrowser/Filter SimpleFileBrowser.FileBrowser::allFilesFilter
	Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8 * ___allFilesFilter_93;
	// System.Boolean SimpleFileBrowser.FileBrowser::showAllFilesFilter
	bool ___showAllFilesFilter_94;
	// System.Boolean SimpleFileBrowser.FileBrowser::allFiltersHaveSingleSuffix
	bool ___allFiltersHaveSingleSuffix_95;
	// System.Boolean SimpleFileBrowser.FileBrowser::allExcludedExtensionsHaveSingleSuffix
	bool ___allExcludedExtensionsHaveSingleSuffix_96;
	// System.String SimpleFileBrowser.FileBrowser::defaultInitialPath
	String_t* ___defaultInitialPath_97;
	// System.Int32 SimpleFileBrowser.FileBrowser::currentPathIndex
	int32_t ___currentPathIndex_98;
	// System.Collections.Generic.List`1<System.String> SimpleFileBrowser.FileBrowser::pathsFollowed
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___pathsFollowed_99;
	// System.Collections.Generic.HashSet`1<System.Char> SimpleFileBrowser.FileBrowser::invalidFilenameChars
	HashSet_1_t64694F1841F026350E09C7F835AC8D3B148B2488 * ___invalidFilenameChars_100;
	// System.Single SimpleFileBrowser.FileBrowser::drivesNextRefreshTime
	float ___drivesNextRefreshTime_101;
	// System.String SimpleFileBrowser.FileBrowser::driveQuickLinks
	String_t* ___driveQuickLinks_102;
	// System.Int32 SimpleFileBrowser.FileBrowser::numberOfDriveQuickLinks
	int32_t ___numberOfDriveQuickLinks_103;
	// System.Boolean SimpleFileBrowser.FileBrowser::canvasDimensionsChanged
	bool ___canvasDimensionsChanged_104;
	// System.Globalization.CompareInfo SimpleFileBrowser.FileBrowser::textComparer
	CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * ___textComparer_105;
	// System.Globalization.CompareOptions SimpleFileBrowser.FileBrowser::textCompareOptions
	int32_t ___textCompareOptions_106;
	// UnityEngine.EventSystems.PointerEventData SimpleFileBrowser.FileBrowser::nullPointerEventData
	PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * ___nullPointerEventData_107;
	// System.String SimpleFileBrowser.FileBrowser::m_currentPath
	String_t* ___m_currentPath_108;
	// System.String SimpleFileBrowser.FileBrowser::m_searchString
	String_t* ___m_searchString_109;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_acceptNonExistingFilename
	bool ___m_acceptNonExistingFilename_110;
	// SimpleFileBrowser.FileBrowser/PickMode SimpleFileBrowser.FileBrowser::m_pickerMode
	int32_t ___m_pickerMode_111;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_allowMultiSelection
	bool ___m_allowMultiSelection_112;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_multiSelectionToggleSelectionMode
	bool ___m_multiSelectionToggleSelectionMode_113;
	// SimpleFileBrowser.FileBrowser/OnSuccess SimpleFileBrowser.FileBrowser::onSuccess
	OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C * ___onSuccess_114;
	// SimpleFileBrowser.FileBrowser/OnCancel SimpleFileBrowser.FileBrowser::onCancel
	OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E * ___onCancel_115;

public:
	inline static int32_t get_offset_of_m_skin_9() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_skin_9)); }
	inline UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * get_m_skin_9() const { return ___m_skin_9; }
	inline UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 ** get_address_of_m_skin_9() { return &___m_skin_9; }
	inline void set_m_skin_9(UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * value)
	{
		___m_skin_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_skin_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_skinVersion_10() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_skinVersion_10)); }
	inline int32_t get_m_skinVersion_10() const { return ___m_skinVersion_10; }
	inline int32_t* get_address_of_m_skinVersion_10() { return &___m_skinVersion_10; }
	inline void set_m_skinVersion_10(int32_t value)
	{
		___m_skinVersion_10 = value;
	}

	inline static int32_t get_offset_of_m_skinPrevDriveIcon_11() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_skinPrevDriveIcon_11)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_skinPrevDriveIcon_11() const { return ___m_skinPrevDriveIcon_11; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_skinPrevDriveIcon_11() { return &___m_skinPrevDriveIcon_11; }
	inline void set_m_skinPrevDriveIcon_11(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_skinPrevDriveIcon_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_skinPrevDriveIcon_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_skinPrevFolderIcon_12() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_skinPrevFolderIcon_12)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_skinPrevFolderIcon_12() const { return ___m_skinPrevFolderIcon_12; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_skinPrevFolderIcon_12() { return &___m_skinPrevFolderIcon_12; }
	inline void set_m_skinPrevFolderIcon_12(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_skinPrevFolderIcon_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_skinPrevFolderIcon_12), (void*)value);
	}

	inline static int32_t get_offset_of_minWidth_24() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___minWidth_24)); }
	inline int32_t get_minWidth_24() const { return ___minWidth_24; }
	inline int32_t* get_address_of_minWidth_24() { return &___minWidth_24; }
	inline void set_minWidth_24(int32_t value)
	{
		___minWidth_24 = value;
	}

	inline static int32_t get_offset_of_minHeight_25() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___minHeight_25)); }
	inline int32_t get_minHeight_25() const { return ___minHeight_25; }
	inline int32_t* get_address_of_minHeight_25() { return &___minHeight_25; }
	inline void set_minHeight_25(int32_t value)
	{
		___minHeight_25 = value;
	}

	inline static int32_t get_offset_of_narrowScreenWidth_26() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___narrowScreenWidth_26)); }
	inline float get_narrowScreenWidth_26() const { return ___narrowScreenWidth_26; }
	inline float* get_address_of_narrowScreenWidth_26() { return &___narrowScreenWidth_26; }
	inline void set_narrowScreenWidth_26(float value)
	{
		___narrowScreenWidth_26 = value;
	}

	inline static int32_t get_offset_of_quickLinksMaxWidthPercentage_27() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___quickLinksMaxWidthPercentage_27)); }
	inline float get_quickLinksMaxWidthPercentage_27() const { return ___quickLinksMaxWidthPercentage_27; }
	inline float* get_address_of_quickLinksMaxWidthPercentage_27() { return &___quickLinksMaxWidthPercentage_27; }
	inline void set_quickLinksMaxWidthPercentage_27(float value)
	{
		___quickLinksMaxWidthPercentage_27 = value;
	}

	inline static int32_t get_offset_of_sortFilesByName_28() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___sortFilesByName_28)); }
	inline bool get_sortFilesByName_28() const { return ___sortFilesByName_28; }
	inline bool* get_address_of_sortFilesByName_28() { return &___sortFilesByName_28; }
	inline void set_sortFilesByName_28(bool value)
	{
		___sortFilesByName_28 = value;
	}

	inline static int32_t get_offset_of_excludedExtensions_29() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___excludedExtensions_29)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_excludedExtensions_29() const { return ___excludedExtensions_29; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_excludedExtensions_29() { return &___excludedExtensions_29; }
	inline void set_excludedExtensions_29(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___excludedExtensions_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___excludedExtensions_29), (void*)value);
	}

	inline static int32_t get_offset_of_quickLinks_30() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___quickLinks_30)); }
	inline QuickLinkU5BU5D_t204AB6BB1B0F422BCD2C3DC7A1F93FDDAFB416BC* get_quickLinks_30() const { return ___quickLinks_30; }
	inline QuickLinkU5BU5D_t204AB6BB1B0F422BCD2C3DC7A1F93FDDAFB416BC** get_address_of_quickLinks_30() { return &___quickLinks_30; }
	inline void set_quickLinks_30(QuickLinkU5BU5D_t204AB6BB1B0F422BCD2C3DC7A1F93FDDAFB416BC* value)
	{
		___quickLinks_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___quickLinks_30), (void*)value);
	}

	inline static int32_t get_offset_of_excludedExtensionsSet_32() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___excludedExtensionsSet_32)); }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * get_excludedExtensionsSet_32() const { return ___excludedExtensionsSet_32; }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 ** get_address_of_excludedExtensionsSet_32() { return &___excludedExtensionsSet_32; }
	inline void set_excludedExtensionsSet_32(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * value)
	{
		___excludedExtensionsSet_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___excludedExtensionsSet_32), (void*)value);
	}

	inline static int32_t get_offset_of_generateQuickLinksForDrives_33() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___generateQuickLinksForDrives_33)); }
	inline bool get_generateQuickLinksForDrives_33() const { return ___generateQuickLinksForDrives_33; }
	inline bool* get_address_of_generateQuickLinksForDrives_33() { return &___generateQuickLinksForDrives_33; }
	inline void set_generateQuickLinksForDrives_33(bool value)
	{
		___generateQuickLinksForDrives_33 = value;
	}

	inline static int32_t get_offset_of_contextMenuShowDeleteButton_34() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___contextMenuShowDeleteButton_34)); }
	inline bool get_contextMenuShowDeleteButton_34() const { return ___contextMenuShowDeleteButton_34; }
	inline bool* get_address_of_contextMenuShowDeleteButton_34() { return &___contextMenuShowDeleteButton_34; }
	inline void set_contextMenuShowDeleteButton_34(bool value)
	{
		___contextMenuShowDeleteButton_34 = value;
	}

	inline static int32_t get_offset_of_contextMenuShowRenameButton_35() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___contextMenuShowRenameButton_35)); }
	inline bool get_contextMenuShowRenameButton_35() const { return ___contextMenuShowRenameButton_35; }
	inline bool* get_address_of_contextMenuShowRenameButton_35() { return &___contextMenuShowRenameButton_35; }
	inline void set_contextMenuShowRenameButton_35(bool value)
	{
		___contextMenuShowRenameButton_35 = value;
	}

	inline static int32_t get_offset_of_showResizeCursor_36() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___showResizeCursor_36)); }
	inline bool get_showResizeCursor_36() const { return ___showResizeCursor_36; }
	inline bool* get_address_of_showResizeCursor_36() { return &___showResizeCursor_36; }
	inline void set_showResizeCursor_36(bool value)
	{
		___showResizeCursor_36 = value;
	}

	inline static int32_t get_offset_of_window_37() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___window_37)); }
	inline FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19 * get_window_37() const { return ___window_37; }
	inline FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19 ** get_address_of_window_37() { return &___window_37; }
	inline void set_window_37(FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19 * value)
	{
		___window_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___window_37), (void*)value);
	}

	inline static int32_t get_offset_of_windowTR_38() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___windowTR_38)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_windowTR_38() const { return ___windowTR_38; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_windowTR_38() { return &___windowTR_38; }
	inline void set_windowTR_38(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___windowTR_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___windowTR_38), (void*)value);
	}

	inline static int32_t get_offset_of_topViewNarrowScreen_39() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___topViewNarrowScreen_39)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_topViewNarrowScreen_39() const { return ___topViewNarrowScreen_39; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_topViewNarrowScreen_39() { return &___topViewNarrowScreen_39; }
	inline void set_topViewNarrowScreen_39(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___topViewNarrowScreen_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___topViewNarrowScreen_39), (void*)value);
	}

	inline static int32_t get_offset_of_middleView_40() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleView_40)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_middleView_40() const { return ___middleView_40; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_middleView_40() { return &___middleView_40; }
	inline void set_middleView_40(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___middleView_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___middleView_40), (void*)value);
	}

	inline static int32_t get_offset_of_middleViewOriginalPosition_41() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleViewOriginalPosition_41)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_middleViewOriginalPosition_41() const { return ___middleViewOriginalPosition_41; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_middleViewOriginalPosition_41() { return &___middleViewOriginalPosition_41; }
	inline void set_middleViewOriginalPosition_41(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___middleViewOriginalPosition_41 = value;
	}

	inline static int32_t get_offset_of_middleViewOriginalSize_42() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleViewOriginalSize_42)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_middleViewOriginalSize_42() const { return ___middleViewOriginalSize_42; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_middleViewOriginalSize_42() { return &___middleViewOriginalSize_42; }
	inline void set_middleViewOriginalSize_42(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___middleViewOriginalSize_42 = value;
	}

	inline static int32_t get_offset_of_middleViewQuickLinks_43() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleViewQuickLinks_43)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_middleViewQuickLinks_43() const { return ___middleViewQuickLinks_43; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_middleViewQuickLinks_43() { return &___middleViewQuickLinks_43; }
	inline void set_middleViewQuickLinks_43(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___middleViewQuickLinks_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___middleViewQuickLinks_43), (void*)value);
	}

	inline static int32_t get_offset_of_middleViewQuickLinksOriginalSize_44() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleViewQuickLinksOriginalSize_44)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_middleViewQuickLinksOriginalSize_44() const { return ___middleViewQuickLinksOriginalSize_44; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_middleViewQuickLinksOriginalSize_44() { return &___middleViewQuickLinksOriginalSize_44; }
	inline void set_middleViewQuickLinksOriginalSize_44(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___middleViewQuickLinksOriginalSize_44 = value;
	}

	inline static int32_t get_offset_of_middleViewFiles_45() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleViewFiles_45)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_middleViewFiles_45() const { return ___middleViewFiles_45; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_middleViewFiles_45() { return &___middleViewFiles_45; }
	inline void set_middleViewFiles_45(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___middleViewFiles_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___middleViewFiles_45), (void*)value);
	}

	inline static int32_t get_offset_of_middleViewSeparator_46() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___middleViewSeparator_46)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_middleViewSeparator_46() const { return ___middleViewSeparator_46; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_middleViewSeparator_46() { return &___middleViewSeparator_46; }
	inline void set_middleViewSeparator_46(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___middleViewSeparator_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___middleViewSeparator_46), (void*)value);
	}

	inline static int32_t get_offset_of_itemPrefab_47() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___itemPrefab_47)); }
	inline FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025 * get_itemPrefab_47() const { return ___itemPrefab_47; }
	inline FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025 ** get_address_of_itemPrefab_47() { return &___itemPrefab_47; }
	inline void set_itemPrefab_47(FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025 * value)
	{
		___itemPrefab_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemPrefab_47), (void*)value);
	}

	inline static int32_t get_offset_of_allItems_48() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___allItems_48)); }
	inline List_1_tC177D10798BB0E73A3C6EF511075D40316BD5CB7 * get_allItems_48() const { return ___allItems_48; }
	inline List_1_tC177D10798BB0E73A3C6EF511075D40316BD5CB7 ** get_address_of_allItems_48() { return &___allItems_48; }
	inline void set_allItems_48(List_1_tC177D10798BB0E73A3C6EF511075D40316BD5CB7 * value)
	{
		___allItems_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allItems_48), (void*)value);
	}

	inline static int32_t get_offset_of_quickLinkPrefab_49() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___quickLinkPrefab_49)); }
	inline FileBrowserQuickLink_t3DDB6D8FB4E5B8A77D0401284C0071037161A08C * get_quickLinkPrefab_49() const { return ___quickLinkPrefab_49; }
	inline FileBrowserQuickLink_t3DDB6D8FB4E5B8A77D0401284C0071037161A08C ** get_address_of_quickLinkPrefab_49() { return &___quickLinkPrefab_49; }
	inline void set_quickLinkPrefab_49(FileBrowserQuickLink_t3DDB6D8FB4E5B8A77D0401284C0071037161A08C * value)
	{
		___quickLinkPrefab_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___quickLinkPrefab_49), (void*)value);
	}

	inline static int32_t get_offset_of_allQuickLinks_50() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___allQuickLinks_50)); }
	inline List_1_t612A2A8CBA335BE2987BF3A35E0D00D10BC40A2F * get_allQuickLinks_50() const { return ___allQuickLinks_50; }
	inline List_1_t612A2A8CBA335BE2987BF3A35E0D00D10BC40A2F ** get_address_of_allQuickLinks_50() { return &___allQuickLinks_50; }
	inline void set_allQuickLinks_50(List_1_t612A2A8CBA335BE2987BF3A35E0D00D10BC40A2F * value)
	{
		___allQuickLinks_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allQuickLinks_50), (void*)value);
	}

	inline static int32_t get_offset_of_titleText_51() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___titleText_51)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_titleText_51() const { return ___titleText_51; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_titleText_51() { return &___titleText_51; }
	inline void set_titleText_51(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___titleText_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___titleText_51), (void*)value);
	}

	inline static int32_t get_offset_of_backButton_52() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___backButton_52)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_backButton_52() const { return ___backButton_52; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_backButton_52() { return &___backButton_52; }
	inline void set_backButton_52(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___backButton_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___backButton_52), (void*)value);
	}

	inline static int32_t get_offset_of_forwardButton_53() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___forwardButton_53)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_forwardButton_53() const { return ___forwardButton_53; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_forwardButton_53() { return &___forwardButton_53; }
	inline void set_forwardButton_53(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___forwardButton_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___forwardButton_53), (void*)value);
	}

	inline static int32_t get_offset_of_upButton_54() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___upButton_54)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_upButton_54() const { return ___upButton_54; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_upButton_54() { return &___upButton_54; }
	inline void set_upButton_54(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___upButton_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___upButton_54), (void*)value);
	}

	inline static int32_t get_offset_of_moreOptionsButton_55() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___moreOptionsButton_55)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_moreOptionsButton_55() const { return ___moreOptionsButton_55; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_moreOptionsButton_55() { return &___moreOptionsButton_55; }
	inline void set_moreOptionsButton_55(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___moreOptionsButton_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___moreOptionsButton_55), (void*)value);
	}

	inline static int32_t get_offset_of_pathInputField_56() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___pathInputField_56)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_pathInputField_56() const { return ___pathInputField_56; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_pathInputField_56() { return &___pathInputField_56; }
	inline void set_pathInputField_56(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___pathInputField_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pathInputField_56), (void*)value);
	}

	inline static int32_t get_offset_of_pathInputFieldSlotTop_57() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___pathInputFieldSlotTop_57)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_pathInputFieldSlotTop_57() const { return ___pathInputFieldSlotTop_57; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_pathInputFieldSlotTop_57() { return &___pathInputFieldSlotTop_57; }
	inline void set_pathInputFieldSlotTop_57(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___pathInputFieldSlotTop_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pathInputFieldSlotTop_57), (void*)value);
	}

	inline static int32_t get_offset_of_pathInputFieldSlotBottom_58() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___pathInputFieldSlotBottom_58)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_pathInputFieldSlotBottom_58() const { return ___pathInputFieldSlotBottom_58; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_pathInputFieldSlotBottom_58() { return &___pathInputFieldSlotBottom_58; }
	inline void set_pathInputFieldSlotBottom_58(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___pathInputFieldSlotBottom_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pathInputFieldSlotBottom_58), (void*)value);
	}

	inline static int32_t get_offset_of_searchInputField_59() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___searchInputField_59)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_searchInputField_59() const { return ___searchInputField_59; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_searchInputField_59() { return &___searchInputField_59; }
	inline void set_searchInputField_59(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___searchInputField_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___searchInputField_59), (void*)value);
	}

	inline static int32_t get_offset_of_quickLinksContainer_60() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___quickLinksContainer_60)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_quickLinksContainer_60() const { return ___quickLinksContainer_60; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_quickLinksContainer_60() { return &___quickLinksContainer_60; }
	inline void set_quickLinksContainer_60(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___quickLinksContainer_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___quickLinksContainer_60), (void*)value);
	}

	inline static int32_t get_offset_of_quickLinksScrollRect_61() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___quickLinksScrollRect_61)); }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * get_quickLinksScrollRect_61() const { return ___quickLinksScrollRect_61; }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA ** get_address_of_quickLinksScrollRect_61() { return &___quickLinksScrollRect_61; }
	inline void set_quickLinksScrollRect_61(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * value)
	{
		___quickLinksScrollRect_61 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___quickLinksScrollRect_61), (void*)value);
	}

	inline static int32_t get_offset_of_filesContainer_62() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filesContainer_62)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_filesContainer_62() const { return ___filesContainer_62; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_filesContainer_62() { return &___filesContainer_62; }
	inline void set_filesContainer_62(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___filesContainer_62 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filesContainer_62), (void*)value);
	}

	inline static int32_t get_offset_of_filesScrollRect_63() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filesScrollRect_63)); }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * get_filesScrollRect_63() const { return ___filesScrollRect_63; }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA ** get_address_of_filesScrollRect_63() { return &___filesScrollRect_63; }
	inline void set_filesScrollRect_63(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * value)
	{
		___filesScrollRect_63 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filesScrollRect_63), (void*)value);
	}

	inline static int32_t get_offset_of_listView_64() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___listView_64)); }
	inline RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 * get_listView_64() const { return ___listView_64; }
	inline RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 ** get_address_of_listView_64() { return &___listView_64; }
	inline void set_listView_64(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 * value)
	{
		___listView_64 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___listView_64), (void*)value);
	}

	inline static int32_t get_offset_of_filenameInputField_65() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filenameInputField_65)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_filenameInputField_65() const { return ___filenameInputField_65; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_filenameInputField_65() { return &___filenameInputField_65; }
	inline void set_filenameInputField_65(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___filenameInputField_65 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filenameInputField_65), (void*)value);
	}

	inline static int32_t get_offset_of_filenameInputFieldOverlayText_66() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filenameInputFieldOverlayText_66)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_filenameInputFieldOverlayText_66() const { return ___filenameInputFieldOverlayText_66; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_filenameInputFieldOverlayText_66() { return &___filenameInputFieldOverlayText_66; }
	inline void set_filenameInputFieldOverlayText_66(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___filenameInputFieldOverlayText_66 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filenameInputFieldOverlayText_66), (void*)value);
	}

	inline static int32_t get_offset_of_filenameImage_67() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filenameImage_67)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_filenameImage_67() const { return ___filenameImage_67; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_filenameImage_67() { return &___filenameImage_67; }
	inline void set_filenameImage_67(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___filenameImage_67 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filenameImage_67), (void*)value);
	}

	inline static int32_t get_offset_of_filtersDropdown_68() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filtersDropdown_68)); }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * get_filtersDropdown_68() const { return ___filtersDropdown_68; }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 ** get_address_of_filtersDropdown_68() { return &___filtersDropdown_68; }
	inline void set_filtersDropdown_68(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * value)
	{
		___filtersDropdown_68 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filtersDropdown_68), (void*)value);
	}

	inline static int32_t get_offset_of_filtersDropdownContainer_69() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filtersDropdownContainer_69)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_filtersDropdownContainer_69() const { return ___filtersDropdownContainer_69; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_filtersDropdownContainer_69() { return &___filtersDropdownContainer_69; }
	inline void set_filtersDropdownContainer_69(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___filtersDropdownContainer_69 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filtersDropdownContainer_69), (void*)value);
	}

	inline static int32_t get_offset_of_filterItemTemplate_70() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filterItemTemplate_70)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_filterItemTemplate_70() const { return ___filterItemTemplate_70; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_filterItemTemplate_70() { return &___filterItemTemplate_70; }
	inline void set_filterItemTemplate_70(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___filterItemTemplate_70 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filterItemTemplate_70), (void*)value);
	}

	inline static int32_t get_offset_of_showHiddenFilesToggle_71() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___showHiddenFilesToggle_71)); }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * get_showHiddenFilesToggle_71() const { return ___showHiddenFilesToggle_71; }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E ** get_address_of_showHiddenFilesToggle_71() { return &___showHiddenFilesToggle_71; }
	inline void set_showHiddenFilesToggle_71(Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * value)
	{
		___showHiddenFilesToggle_71 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___showHiddenFilesToggle_71), (void*)value);
	}

	inline static int32_t get_offset_of_submitButtonText_72() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___submitButtonText_72)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_submitButtonText_72() const { return ___submitButtonText_72; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_submitButtonText_72() { return &___submitButtonText_72; }
	inline void set_submitButtonText_72(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___submitButtonText_72 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___submitButtonText_72), (void*)value);
	}

	inline static int32_t get_offset_of_allButtons_73() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___allButtons_73)); }
	inline ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* get_allButtons_73() const { return ___allButtons_73; }
	inline ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B** get_address_of_allButtons_73() { return &___allButtons_73; }
	inline void set_allButtons_73(ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* value)
	{
		___allButtons_73 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allButtons_73), (void*)value);
	}

	inline static int32_t get_offset_of_moreOptionsContextMenuPosition_74() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___moreOptionsContextMenuPosition_74)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_moreOptionsContextMenuPosition_74() const { return ___moreOptionsContextMenuPosition_74; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_moreOptionsContextMenuPosition_74() { return &___moreOptionsContextMenuPosition_74; }
	inline void set_moreOptionsContextMenuPosition_74(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___moreOptionsContextMenuPosition_74 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___moreOptionsContextMenuPosition_74), (void*)value);
	}

	inline static int32_t get_offset_of_renameItem_75() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___renameItem_75)); }
	inline FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4 * get_renameItem_75() const { return ___renameItem_75; }
	inline FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4 ** get_address_of_renameItem_75() { return &___renameItem_75; }
	inline void set_renameItem_75(FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4 * value)
	{
		___renameItem_75 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___renameItem_75), (void*)value);
	}

	inline static int32_t get_offset_of_contextMenu_76() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___contextMenu_76)); }
	inline FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC * get_contextMenu_76() const { return ___contextMenu_76; }
	inline FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC ** get_address_of_contextMenu_76() { return &___contextMenu_76; }
	inline void set_contextMenu_76(FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC * value)
	{
		___contextMenu_76 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contextMenu_76), (void*)value);
	}

	inline static int32_t get_offset_of_fileOperationConfirmationPanel_77() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___fileOperationConfirmationPanel_77)); }
	inline FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229 * get_fileOperationConfirmationPanel_77() const { return ___fileOperationConfirmationPanel_77; }
	inline FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229 ** get_address_of_fileOperationConfirmationPanel_77() { return &___fileOperationConfirmationPanel_77; }
	inline void set_fileOperationConfirmationPanel_77(FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229 * value)
	{
		___fileOperationConfirmationPanel_77 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fileOperationConfirmationPanel_77), (void*)value);
	}

	inline static int32_t get_offset_of_accessRestrictedPanel_78() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___accessRestrictedPanel_78)); }
	inline FileBrowserAccessRestrictedPanel_t96A484C5166B6EF23402D06FF2107AC3ED6ECB30 * get_accessRestrictedPanel_78() const { return ___accessRestrictedPanel_78; }
	inline FileBrowserAccessRestrictedPanel_t96A484C5166B6EF23402D06FF2107AC3ED6ECB30 ** get_address_of_accessRestrictedPanel_78() { return &___accessRestrictedPanel_78; }
	inline void set_accessRestrictedPanel_78(FileBrowserAccessRestrictedPanel_t96A484C5166B6EF23402D06FF2107AC3ED6ECB30 * value)
	{
		___accessRestrictedPanel_78 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___accessRestrictedPanel_78), (void*)value);
	}

	inline static int32_t get_offset_of_resizeCursorHandler_79() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___resizeCursorHandler_79)); }
	inline FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C * get_resizeCursorHandler_79() const { return ___resizeCursorHandler_79; }
	inline FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C ** get_address_of_resizeCursorHandler_79() { return &___resizeCursorHandler_79; }
	inline void set_resizeCursorHandler_79(FileBrowserCursorHandler_tE870FA7F18FFAF0FBC48721A283BF7A45BB7069C * value)
	{
		___resizeCursorHandler_79 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resizeCursorHandler_79), (void*)value);
	}

	inline static int32_t get_offset_of_rectTransform_80() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___rectTransform_80)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_rectTransform_80() const { return ___rectTransform_80; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_rectTransform_80() { return &___rectTransform_80; }
	inline void set_rectTransform_80(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___rectTransform_80 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rectTransform_80), (void*)value);
	}

	inline static int32_t get_offset_of_canvas_81() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___canvas_81)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_canvas_81() const { return ___canvas_81; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_canvas_81() { return &___canvas_81; }
	inline void set_canvas_81(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___canvas_81 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvas_81), (void*)value);
	}

	inline static int32_t get_offset_of_ignoredFileAttributes_82() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___ignoredFileAttributes_82)); }
	inline int32_t get_ignoredFileAttributes_82() const { return ___ignoredFileAttributes_82; }
	inline int32_t* get_address_of_ignoredFileAttributes_82() { return &___ignoredFileAttributes_82; }
	inline void set_ignoredFileAttributes_82(int32_t value)
	{
		___ignoredFileAttributes_82 = value;
	}

	inline static int32_t get_offset_of_allFileEntries_83() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___allFileEntries_83)); }
	inline FileSystemEntryU5BU5D_t457BF0158EF6A2EEFB557C493617F0B87ED6B7A3* get_allFileEntries_83() const { return ___allFileEntries_83; }
	inline FileSystemEntryU5BU5D_t457BF0158EF6A2EEFB557C493617F0B87ED6B7A3** get_address_of_allFileEntries_83() { return &___allFileEntries_83; }
	inline void set_allFileEntries_83(FileSystemEntryU5BU5D_t457BF0158EF6A2EEFB557C493617F0B87ED6B7A3* value)
	{
		___allFileEntries_83 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allFileEntries_83), (void*)value);
	}

	inline static int32_t get_offset_of_validFileEntries_84() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___validFileEntries_84)); }
	inline List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB * get_validFileEntries_84() const { return ___validFileEntries_84; }
	inline List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB ** get_address_of_validFileEntries_84() { return &___validFileEntries_84; }
	inline void set_validFileEntries_84(List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB * value)
	{
		___validFileEntries_84 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___validFileEntries_84), (void*)value);
	}

	inline static int32_t get_offset_of_selectedFileEntries_85() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___selectedFileEntries_85)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_selectedFileEntries_85() const { return ___selectedFileEntries_85; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_selectedFileEntries_85() { return &___selectedFileEntries_85; }
	inline void set_selectedFileEntries_85(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___selectedFileEntries_85 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selectedFileEntries_85), (void*)value);
	}

	inline static int32_t get_offset_of_pendingFileEntrySelection_86() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___pendingFileEntrySelection_86)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_pendingFileEntrySelection_86() const { return ___pendingFileEntrySelection_86; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_pendingFileEntrySelection_86() { return &___pendingFileEntrySelection_86; }
	inline void set_pendingFileEntrySelection_86(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___pendingFileEntrySelection_86 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pendingFileEntrySelection_86), (void*)value);
	}

	inline static int32_t get_offset_of_submittedFileEntryPaths_87() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___submittedFileEntryPaths_87)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_submittedFileEntryPaths_87() const { return ___submittedFileEntryPaths_87; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_submittedFileEntryPaths_87() { return &___submittedFileEntryPaths_87; }
	inline void set_submittedFileEntryPaths_87(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___submittedFileEntryPaths_87 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___submittedFileEntryPaths_87), (void*)value);
	}

	inline static int32_t get_offset_of_submittedFolderPaths_88() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___submittedFolderPaths_88)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_submittedFolderPaths_88() const { return ___submittedFolderPaths_88; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_submittedFolderPaths_88() { return &___submittedFolderPaths_88; }
	inline void set_submittedFolderPaths_88(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___submittedFolderPaths_88 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___submittedFolderPaths_88), (void*)value);
	}

	inline static int32_t get_offset_of_submittedFileEntriesToOverwrite_89() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___submittedFileEntriesToOverwrite_89)); }
	inline List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB * get_submittedFileEntriesToOverwrite_89() const { return ___submittedFileEntriesToOverwrite_89; }
	inline List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB ** get_address_of_submittedFileEntriesToOverwrite_89() { return &___submittedFileEntriesToOverwrite_89; }
	inline void set_submittedFileEntriesToOverwrite_89(List_1_t1F0C96076F92B1DAA8904FC41DEA48888A58E9FB * value)
	{
		___submittedFileEntriesToOverwrite_89 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___submittedFileEntriesToOverwrite_89), (void*)value);
	}

	inline static int32_t get_offset_of_multiSelectionPivotFileEntry_90() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___multiSelectionPivotFileEntry_90)); }
	inline int32_t get_multiSelectionPivotFileEntry_90() const { return ___multiSelectionPivotFileEntry_90; }
	inline int32_t* get_address_of_multiSelectionPivotFileEntry_90() { return &___multiSelectionPivotFileEntry_90; }
	inline void set_multiSelectionPivotFileEntry_90(int32_t value)
	{
		___multiSelectionPivotFileEntry_90 = value;
	}

	inline static int32_t get_offset_of_multiSelectionFilenameBuilder_91() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___multiSelectionFilenameBuilder_91)); }
	inline StringBuilder_t * get_multiSelectionFilenameBuilder_91() const { return ___multiSelectionFilenameBuilder_91; }
	inline StringBuilder_t ** get_address_of_multiSelectionFilenameBuilder_91() { return &___multiSelectionFilenameBuilder_91; }
	inline void set_multiSelectionFilenameBuilder_91(StringBuilder_t * value)
	{
		___multiSelectionFilenameBuilder_91 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___multiSelectionFilenameBuilder_91), (void*)value);
	}

	inline static int32_t get_offset_of_filters_92() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___filters_92)); }
	inline List_1_tCEE47E7A624FD9E913773D93A05E7DE44BC7D35A * get_filters_92() const { return ___filters_92; }
	inline List_1_tCEE47E7A624FD9E913773D93A05E7DE44BC7D35A ** get_address_of_filters_92() { return &___filters_92; }
	inline void set_filters_92(List_1_tCEE47E7A624FD9E913773D93A05E7DE44BC7D35A * value)
	{
		___filters_92 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filters_92), (void*)value);
	}

	inline static int32_t get_offset_of_allFilesFilter_93() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___allFilesFilter_93)); }
	inline Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8 * get_allFilesFilter_93() const { return ___allFilesFilter_93; }
	inline Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8 ** get_address_of_allFilesFilter_93() { return &___allFilesFilter_93; }
	inline void set_allFilesFilter_93(Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8 * value)
	{
		___allFilesFilter_93 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allFilesFilter_93), (void*)value);
	}

	inline static int32_t get_offset_of_showAllFilesFilter_94() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___showAllFilesFilter_94)); }
	inline bool get_showAllFilesFilter_94() const { return ___showAllFilesFilter_94; }
	inline bool* get_address_of_showAllFilesFilter_94() { return &___showAllFilesFilter_94; }
	inline void set_showAllFilesFilter_94(bool value)
	{
		___showAllFilesFilter_94 = value;
	}

	inline static int32_t get_offset_of_allFiltersHaveSingleSuffix_95() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___allFiltersHaveSingleSuffix_95)); }
	inline bool get_allFiltersHaveSingleSuffix_95() const { return ___allFiltersHaveSingleSuffix_95; }
	inline bool* get_address_of_allFiltersHaveSingleSuffix_95() { return &___allFiltersHaveSingleSuffix_95; }
	inline void set_allFiltersHaveSingleSuffix_95(bool value)
	{
		___allFiltersHaveSingleSuffix_95 = value;
	}

	inline static int32_t get_offset_of_allExcludedExtensionsHaveSingleSuffix_96() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___allExcludedExtensionsHaveSingleSuffix_96)); }
	inline bool get_allExcludedExtensionsHaveSingleSuffix_96() const { return ___allExcludedExtensionsHaveSingleSuffix_96; }
	inline bool* get_address_of_allExcludedExtensionsHaveSingleSuffix_96() { return &___allExcludedExtensionsHaveSingleSuffix_96; }
	inline void set_allExcludedExtensionsHaveSingleSuffix_96(bool value)
	{
		___allExcludedExtensionsHaveSingleSuffix_96 = value;
	}

	inline static int32_t get_offset_of_defaultInitialPath_97() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___defaultInitialPath_97)); }
	inline String_t* get_defaultInitialPath_97() const { return ___defaultInitialPath_97; }
	inline String_t** get_address_of_defaultInitialPath_97() { return &___defaultInitialPath_97; }
	inline void set_defaultInitialPath_97(String_t* value)
	{
		___defaultInitialPath_97 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultInitialPath_97), (void*)value);
	}

	inline static int32_t get_offset_of_currentPathIndex_98() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___currentPathIndex_98)); }
	inline int32_t get_currentPathIndex_98() const { return ___currentPathIndex_98; }
	inline int32_t* get_address_of_currentPathIndex_98() { return &___currentPathIndex_98; }
	inline void set_currentPathIndex_98(int32_t value)
	{
		___currentPathIndex_98 = value;
	}

	inline static int32_t get_offset_of_pathsFollowed_99() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___pathsFollowed_99)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_pathsFollowed_99() const { return ___pathsFollowed_99; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_pathsFollowed_99() { return &___pathsFollowed_99; }
	inline void set_pathsFollowed_99(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___pathsFollowed_99 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pathsFollowed_99), (void*)value);
	}

	inline static int32_t get_offset_of_invalidFilenameChars_100() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___invalidFilenameChars_100)); }
	inline HashSet_1_t64694F1841F026350E09C7F835AC8D3B148B2488 * get_invalidFilenameChars_100() const { return ___invalidFilenameChars_100; }
	inline HashSet_1_t64694F1841F026350E09C7F835AC8D3B148B2488 ** get_address_of_invalidFilenameChars_100() { return &___invalidFilenameChars_100; }
	inline void set_invalidFilenameChars_100(HashSet_1_t64694F1841F026350E09C7F835AC8D3B148B2488 * value)
	{
		___invalidFilenameChars_100 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invalidFilenameChars_100), (void*)value);
	}

	inline static int32_t get_offset_of_drivesNextRefreshTime_101() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___drivesNextRefreshTime_101)); }
	inline float get_drivesNextRefreshTime_101() const { return ___drivesNextRefreshTime_101; }
	inline float* get_address_of_drivesNextRefreshTime_101() { return &___drivesNextRefreshTime_101; }
	inline void set_drivesNextRefreshTime_101(float value)
	{
		___drivesNextRefreshTime_101 = value;
	}

	inline static int32_t get_offset_of_driveQuickLinks_102() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___driveQuickLinks_102)); }
	inline String_t* get_driveQuickLinks_102() const { return ___driveQuickLinks_102; }
	inline String_t** get_address_of_driveQuickLinks_102() { return &___driveQuickLinks_102; }
	inline void set_driveQuickLinks_102(String_t* value)
	{
		___driveQuickLinks_102 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___driveQuickLinks_102), (void*)value);
	}

	inline static int32_t get_offset_of_numberOfDriveQuickLinks_103() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___numberOfDriveQuickLinks_103)); }
	inline int32_t get_numberOfDriveQuickLinks_103() const { return ___numberOfDriveQuickLinks_103; }
	inline int32_t* get_address_of_numberOfDriveQuickLinks_103() { return &___numberOfDriveQuickLinks_103; }
	inline void set_numberOfDriveQuickLinks_103(int32_t value)
	{
		___numberOfDriveQuickLinks_103 = value;
	}

	inline static int32_t get_offset_of_canvasDimensionsChanged_104() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___canvasDimensionsChanged_104)); }
	inline bool get_canvasDimensionsChanged_104() const { return ___canvasDimensionsChanged_104; }
	inline bool* get_address_of_canvasDimensionsChanged_104() { return &___canvasDimensionsChanged_104; }
	inline void set_canvasDimensionsChanged_104(bool value)
	{
		___canvasDimensionsChanged_104 = value;
	}

	inline static int32_t get_offset_of_textComparer_105() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___textComparer_105)); }
	inline CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * get_textComparer_105() const { return ___textComparer_105; }
	inline CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 ** get_address_of_textComparer_105() { return &___textComparer_105; }
	inline void set_textComparer_105(CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * value)
	{
		___textComparer_105 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textComparer_105), (void*)value);
	}

	inline static int32_t get_offset_of_textCompareOptions_106() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___textCompareOptions_106)); }
	inline int32_t get_textCompareOptions_106() const { return ___textCompareOptions_106; }
	inline int32_t* get_address_of_textCompareOptions_106() { return &___textCompareOptions_106; }
	inline void set_textCompareOptions_106(int32_t value)
	{
		___textCompareOptions_106 = value;
	}

	inline static int32_t get_offset_of_nullPointerEventData_107() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___nullPointerEventData_107)); }
	inline PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * get_nullPointerEventData_107() const { return ___nullPointerEventData_107; }
	inline PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 ** get_address_of_nullPointerEventData_107() { return &___nullPointerEventData_107; }
	inline void set_nullPointerEventData_107(PointerEventData_tC6C1BEE9D4C8755A31DA7FC0C9A1F28A36456954 * value)
	{
		___nullPointerEventData_107 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nullPointerEventData_107), (void*)value);
	}

	inline static int32_t get_offset_of_m_currentPath_108() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_currentPath_108)); }
	inline String_t* get_m_currentPath_108() const { return ___m_currentPath_108; }
	inline String_t** get_address_of_m_currentPath_108() { return &___m_currentPath_108; }
	inline void set_m_currentPath_108(String_t* value)
	{
		___m_currentPath_108 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_currentPath_108), (void*)value);
	}

	inline static int32_t get_offset_of_m_searchString_109() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_searchString_109)); }
	inline String_t* get_m_searchString_109() const { return ___m_searchString_109; }
	inline String_t** get_address_of_m_searchString_109() { return &___m_searchString_109; }
	inline void set_m_searchString_109(String_t* value)
	{
		___m_searchString_109 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_searchString_109), (void*)value);
	}

	inline static int32_t get_offset_of_m_acceptNonExistingFilename_110() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_acceptNonExistingFilename_110)); }
	inline bool get_m_acceptNonExistingFilename_110() const { return ___m_acceptNonExistingFilename_110; }
	inline bool* get_address_of_m_acceptNonExistingFilename_110() { return &___m_acceptNonExistingFilename_110; }
	inline void set_m_acceptNonExistingFilename_110(bool value)
	{
		___m_acceptNonExistingFilename_110 = value;
	}

	inline static int32_t get_offset_of_m_pickerMode_111() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_pickerMode_111)); }
	inline int32_t get_m_pickerMode_111() const { return ___m_pickerMode_111; }
	inline int32_t* get_address_of_m_pickerMode_111() { return &___m_pickerMode_111; }
	inline void set_m_pickerMode_111(int32_t value)
	{
		___m_pickerMode_111 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiSelection_112() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_allowMultiSelection_112)); }
	inline bool get_m_allowMultiSelection_112() const { return ___m_allowMultiSelection_112; }
	inline bool* get_address_of_m_allowMultiSelection_112() { return &___m_allowMultiSelection_112; }
	inline void set_m_allowMultiSelection_112(bool value)
	{
		___m_allowMultiSelection_112 = value;
	}

	inline static int32_t get_offset_of_m_multiSelectionToggleSelectionMode_113() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___m_multiSelectionToggleSelectionMode_113)); }
	inline bool get_m_multiSelectionToggleSelectionMode_113() const { return ___m_multiSelectionToggleSelectionMode_113; }
	inline bool* get_address_of_m_multiSelectionToggleSelectionMode_113() { return &___m_multiSelectionToggleSelectionMode_113; }
	inline void set_m_multiSelectionToggleSelectionMode_113(bool value)
	{
		___m_multiSelectionToggleSelectionMode_113 = value;
	}

	inline static int32_t get_offset_of_onSuccess_114() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___onSuccess_114)); }
	inline OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C * get_onSuccess_114() const { return ___onSuccess_114; }
	inline OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C ** get_address_of_onSuccess_114() { return &___onSuccess_114; }
	inline void set_onSuccess_114(OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C * value)
	{
		___onSuccess_114 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onSuccess_114), (void*)value);
	}

	inline static int32_t get_offset_of_onCancel_115() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8, ___onCancel_115)); }
	inline OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E * get_onCancel_115() const { return ___onCancel_115; }
	inline OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E ** get_address_of_onCancel_115() { return &___onCancel_115; }
	inline void set_onCancel_115(OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E * value)
	{
		___onCancel_115 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onCancel_115), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowserRenamedItem
struct FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Image SimpleFileBrowser.FileBrowserRenamedItem::background
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___background_4;
	// UnityEngine.UI.Image SimpleFileBrowser.FileBrowserRenamedItem::icon
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___icon_5;
	// UnityEngine.UI.InputField SimpleFileBrowser.FileBrowserRenamedItem::nameInputField
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___nameInputField_6;
	// SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted SimpleFileBrowser.FileBrowserRenamedItem::onRenameCompleted
	OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * ___onRenameCompleted_7;
	// UnityEngine.RectTransform SimpleFileBrowser.FileBrowserRenamedItem::m_transform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_transform_8;

public:
	inline static int32_t get_offset_of_background_4() { return static_cast<int32_t>(offsetof(FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4, ___background_4)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_background_4() const { return ___background_4; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_background_4() { return &___background_4; }
	inline void set_background_4(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___background_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___background_4), (void*)value);
	}

	inline static int32_t get_offset_of_icon_5() { return static_cast<int32_t>(offsetof(FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4, ___icon_5)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_icon_5() const { return ___icon_5; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_icon_5() { return &___icon_5; }
	inline void set_icon_5(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___icon_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___icon_5), (void*)value);
	}

	inline static int32_t get_offset_of_nameInputField_6() { return static_cast<int32_t>(offsetof(FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4, ___nameInputField_6)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_nameInputField_6() const { return ___nameInputField_6; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_nameInputField_6() { return &___nameInputField_6; }
	inline void set_nameInputField_6(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___nameInputField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameInputField_6), (void*)value);
	}

	inline static int32_t get_offset_of_onRenameCompleted_7() { return static_cast<int32_t>(offsetof(FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4, ___onRenameCompleted_7)); }
	inline OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * get_onRenameCompleted_7() const { return ___onRenameCompleted_7; }
	inline OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 ** get_address_of_onRenameCompleted_7() { return &___onRenameCompleted_7; }
	inline void set_onRenameCompleted_7(OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * value)
	{
		___onRenameCompleted_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onRenameCompleted_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_transform_8() { return static_cast<int32_t>(offsetof(FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4, ___m_transform_8)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_transform_8() const { return ___m_transform_8; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_transform_8() { return &___m_transform_8; }
	inline void set_m_transform_8(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_transform_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_transform_8), (void*)value);
	}
};


// SimpleFileBrowser.RecycledListView
struct RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.RectTransform SimpleFileBrowser.RecycledListView::viewportTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___viewportTransform_4;
	// UnityEngine.RectTransform SimpleFileBrowser.RecycledListView::contentTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___contentTransform_5;
	// System.Single SimpleFileBrowser.RecycledListView::itemHeight
	float ___itemHeight_6;
	// System.Single SimpleFileBrowser.RecycledListView::_1OverItemHeight
	float ____1OverItemHeight_7;
	// System.Single SimpleFileBrowser.RecycledListView::viewportHeight
	float ___viewportHeight_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,SimpleFileBrowser.ListItem> SimpleFileBrowser.RecycledListView::items
	Dictionary_2_t650F8D518346F558165A2425C6C9BD7DDF51C721 * ___items_9;
	// System.Collections.Generic.Stack`1<SimpleFileBrowser.ListItem> SimpleFileBrowser.RecycledListView::pooledItems
	Stack_1_t6AF1F6A4BF10324432F934511D9F81D8A0F9C68A * ___pooledItems_10;
	// SimpleFileBrowser.IListViewAdapter SimpleFileBrowser.RecycledListView::adapter
	RuntimeObject* ___adapter_11;
	// System.Int32 SimpleFileBrowser.RecycledListView::currentTopIndex
	int32_t ___currentTopIndex_12;
	// System.Int32 SimpleFileBrowser.RecycledListView::currentBottomIndex
	int32_t ___currentBottomIndex_13;

public:
	inline static int32_t get_offset_of_viewportTransform_4() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___viewportTransform_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_viewportTransform_4() const { return ___viewportTransform_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_viewportTransform_4() { return &___viewportTransform_4; }
	inline void set_viewportTransform_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___viewportTransform_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___viewportTransform_4), (void*)value);
	}

	inline static int32_t get_offset_of_contentTransform_5() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___contentTransform_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_contentTransform_5() const { return ___contentTransform_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_contentTransform_5() { return &___contentTransform_5; }
	inline void set_contentTransform_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___contentTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contentTransform_5), (void*)value);
	}

	inline static int32_t get_offset_of_itemHeight_6() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___itemHeight_6)); }
	inline float get_itemHeight_6() const { return ___itemHeight_6; }
	inline float* get_address_of_itemHeight_6() { return &___itemHeight_6; }
	inline void set_itemHeight_6(float value)
	{
		___itemHeight_6 = value;
	}

	inline static int32_t get_offset_of__1OverItemHeight_7() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ____1OverItemHeight_7)); }
	inline float get__1OverItemHeight_7() const { return ____1OverItemHeight_7; }
	inline float* get_address_of__1OverItemHeight_7() { return &____1OverItemHeight_7; }
	inline void set__1OverItemHeight_7(float value)
	{
		____1OverItemHeight_7 = value;
	}

	inline static int32_t get_offset_of_viewportHeight_8() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___viewportHeight_8)); }
	inline float get_viewportHeight_8() const { return ___viewportHeight_8; }
	inline float* get_address_of_viewportHeight_8() { return &___viewportHeight_8; }
	inline void set_viewportHeight_8(float value)
	{
		___viewportHeight_8 = value;
	}

	inline static int32_t get_offset_of_items_9() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___items_9)); }
	inline Dictionary_2_t650F8D518346F558165A2425C6C9BD7DDF51C721 * get_items_9() const { return ___items_9; }
	inline Dictionary_2_t650F8D518346F558165A2425C6C9BD7DDF51C721 ** get_address_of_items_9() { return &___items_9; }
	inline void set_items_9(Dictionary_2_t650F8D518346F558165A2425C6C9BD7DDF51C721 * value)
	{
		___items_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___items_9), (void*)value);
	}

	inline static int32_t get_offset_of_pooledItems_10() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___pooledItems_10)); }
	inline Stack_1_t6AF1F6A4BF10324432F934511D9F81D8A0F9C68A * get_pooledItems_10() const { return ___pooledItems_10; }
	inline Stack_1_t6AF1F6A4BF10324432F934511D9F81D8A0F9C68A ** get_address_of_pooledItems_10() { return &___pooledItems_10; }
	inline void set_pooledItems_10(Stack_1_t6AF1F6A4BF10324432F934511D9F81D8A0F9C68A * value)
	{
		___pooledItems_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pooledItems_10), (void*)value);
	}

	inline static int32_t get_offset_of_adapter_11() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___adapter_11)); }
	inline RuntimeObject* get_adapter_11() const { return ___adapter_11; }
	inline RuntimeObject** get_address_of_adapter_11() { return &___adapter_11; }
	inline void set_adapter_11(RuntimeObject* value)
	{
		___adapter_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___adapter_11), (void*)value);
	}

	inline static int32_t get_offset_of_currentTopIndex_12() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___currentTopIndex_12)); }
	inline int32_t get_currentTopIndex_12() const { return ___currentTopIndex_12; }
	inline int32_t* get_address_of_currentTopIndex_12() { return &___currentTopIndex_12; }
	inline void set_currentTopIndex_12(int32_t value)
	{
		___currentTopIndex_12 = value;
	}

	inline static int32_t get_offset_of_currentBottomIndex_13() { return static_cast<int32_t>(offsetof(RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767, ___currentBottomIndex_13)); }
	inline int32_t get_currentBottomIndex_13() const { return ___currentBottomIndex_13; }
	inline int32_t* get_address_of_currentBottomIndex_13() { return &___currentBottomIndex_13; }
	inline void set_currentBottomIndex_13(int32_t value)
	{
		___currentBottomIndex_13 = value;
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.UI.ScrollRect
struct ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Content
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_Content_4;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Horizontal
	bool ___m_Horizontal_5;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Vertical
	bool ___m_Vertical_6;
	// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::m_MovementType
	int32_t ___m_MovementType_7;
	// System.Single UnityEngine.UI.ScrollRect::m_Elasticity
	float ___m_Elasticity_8;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Inertia
	bool ___m_Inertia_9;
	// System.Single UnityEngine.UI.ScrollRect::m_DecelerationRate
	float ___m_DecelerationRate_10;
	// System.Single UnityEngine.UI.ScrollRect::m_ScrollSensitivity
	float ___m_ScrollSensitivity_11;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Viewport
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_Viewport_12;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_HorizontalScrollbar
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * ___m_HorizontalScrollbar_13;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_VerticalScrollbar
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * ___m_VerticalScrollbar_14;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_HorizontalScrollbarVisibility
	int32_t ___m_HorizontalScrollbarVisibility_15;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_VerticalScrollbarVisibility
	int32_t ___m_VerticalScrollbarVisibility_16;
	// System.Single UnityEngine.UI.ScrollRect::m_HorizontalScrollbarSpacing
	float ___m_HorizontalScrollbarSpacing_17;
	// System.Single UnityEngine.UI.ScrollRect::m_VerticalScrollbarSpacing
	float ___m_VerticalScrollbarSpacing_18;
	// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::m_OnValueChanged
	ScrollRectEvent_tA2F08EF8BB0B0B0F72DB8242DC5AB17BB0D1731E * ___m_OnValueChanged_19;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PointerStartLocalCursor
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_PointerStartLocalCursor_20;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_ContentStartPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_ContentStartPosition_21;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_ViewRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_ViewRect_22;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ContentBounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___m_ContentBounds_23;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ViewBounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___m_ViewBounds_24;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_Velocity
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Velocity_25;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Dragging
	bool ___m_Dragging_26;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Scrolling
	bool ___m_Scrolling_27;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PrevPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_PrevPosition_28;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevContentBounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___m_PrevContentBounds_29;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevViewBounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___m_PrevViewBounds_30;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HasRebuiltLayout
	bool ___m_HasRebuiltLayout_31;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HSliderExpand
	bool ___m_HSliderExpand_32;
	// System.Boolean UnityEngine.UI.ScrollRect::m_VSliderExpand
	bool ___m_VSliderExpand_33;
	// System.Single UnityEngine.UI.ScrollRect::m_HSliderHeight
	float ___m_HSliderHeight_34;
	// System.Single UnityEngine.UI.ScrollRect::m_VSliderWidth
	float ___m_VSliderWidth_35;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Rect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_Rect_36;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_HorizontalScrollbarRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HorizontalScrollbarRect_37;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_VerticalScrollbarRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_VerticalScrollbarRect_38;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ScrollRect::m_Tracker
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  ___m_Tracker_39;
	// UnityEngine.Vector3[] UnityEngine.UI.ScrollRect::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_40;

public:
	inline static int32_t get_offset_of_m_Content_4() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Content_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_Content_4() const { return ___m_Content_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_Content_4() { return &___m_Content_4; }
	inline void set_m_Content_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_Content_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Content_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Horizontal_5() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Horizontal_5)); }
	inline bool get_m_Horizontal_5() const { return ___m_Horizontal_5; }
	inline bool* get_address_of_m_Horizontal_5() { return &___m_Horizontal_5; }
	inline void set_m_Horizontal_5(bool value)
	{
		___m_Horizontal_5 = value;
	}

	inline static int32_t get_offset_of_m_Vertical_6() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Vertical_6)); }
	inline bool get_m_Vertical_6() const { return ___m_Vertical_6; }
	inline bool* get_address_of_m_Vertical_6() { return &___m_Vertical_6; }
	inline void set_m_Vertical_6(bool value)
	{
		___m_Vertical_6 = value;
	}

	inline static int32_t get_offset_of_m_MovementType_7() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_MovementType_7)); }
	inline int32_t get_m_MovementType_7() const { return ___m_MovementType_7; }
	inline int32_t* get_address_of_m_MovementType_7() { return &___m_MovementType_7; }
	inline void set_m_MovementType_7(int32_t value)
	{
		___m_MovementType_7 = value;
	}

	inline static int32_t get_offset_of_m_Elasticity_8() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Elasticity_8)); }
	inline float get_m_Elasticity_8() const { return ___m_Elasticity_8; }
	inline float* get_address_of_m_Elasticity_8() { return &___m_Elasticity_8; }
	inline void set_m_Elasticity_8(float value)
	{
		___m_Elasticity_8 = value;
	}

	inline static int32_t get_offset_of_m_Inertia_9() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Inertia_9)); }
	inline bool get_m_Inertia_9() const { return ___m_Inertia_9; }
	inline bool* get_address_of_m_Inertia_9() { return &___m_Inertia_9; }
	inline void set_m_Inertia_9(bool value)
	{
		___m_Inertia_9 = value;
	}

	inline static int32_t get_offset_of_m_DecelerationRate_10() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_DecelerationRate_10)); }
	inline float get_m_DecelerationRate_10() const { return ___m_DecelerationRate_10; }
	inline float* get_address_of_m_DecelerationRate_10() { return &___m_DecelerationRate_10; }
	inline void set_m_DecelerationRate_10(float value)
	{
		___m_DecelerationRate_10 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_11() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_ScrollSensitivity_11)); }
	inline float get_m_ScrollSensitivity_11() const { return ___m_ScrollSensitivity_11; }
	inline float* get_address_of_m_ScrollSensitivity_11() { return &___m_ScrollSensitivity_11; }
	inline void set_m_ScrollSensitivity_11(float value)
	{
		___m_ScrollSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_m_Viewport_12() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Viewport_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_Viewport_12() const { return ___m_Viewport_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_Viewport_12() { return &___m_Viewport_12; }
	inline void set_m_Viewport_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_Viewport_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Viewport_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbar_13() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HorizontalScrollbar_13)); }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * get_m_HorizontalScrollbar_13() const { return ___m_HorizontalScrollbar_13; }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 ** get_address_of_m_HorizontalScrollbar_13() { return &___m_HorizontalScrollbar_13; }
	inline void set_m_HorizontalScrollbar_13(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * value)
	{
		___m_HorizontalScrollbar_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HorizontalScrollbar_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_14() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_VerticalScrollbar_14)); }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * get_m_VerticalScrollbar_14() const { return ___m_VerticalScrollbar_14; }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 ** get_address_of_m_VerticalScrollbar_14() { return &___m_VerticalScrollbar_14; }
	inline void set_m_VerticalScrollbar_14(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * value)
	{
		___m_VerticalScrollbar_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_VerticalScrollbar_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarVisibility_15() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HorizontalScrollbarVisibility_15)); }
	inline int32_t get_m_HorizontalScrollbarVisibility_15() const { return ___m_HorizontalScrollbarVisibility_15; }
	inline int32_t* get_address_of_m_HorizontalScrollbarVisibility_15() { return &___m_HorizontalScrollbarVisibility_15; }
	inline void set_m_HorizontalScrollbarVisibility_15(int32_t value)
	{
		___m_HorizontalScrollbarVisibility_15 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarVisibility_16() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_VerticalScrollbarVisibility_16)); }
	inline int32_t get_m_VerticalScrollbarVisibility_16() const { return ___m_VerticalScrollbarVisibility_16; }
	inline int32_t* get_address_of_m_VerticalScrollbarVisibility_16() { return &___m_VerticalScrollbarVisibility_16; }
	inline void set_m_VerticalScrollbarVisibility_16(int32_t value)
	{
		___m_VerticalScrollbarVisibility_16 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarSpacing_17() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HorizontalScrollbarSpacing_17)); }
	inline float get_m_HorizontalScrollbarSpacing_17() const { return ___m_HorizontalScrollbarSpacing_17; }
	inline float* get_address_of_m_HorizontalScrollbarSpacing_17() { return &___m_HorizontalScrollbarSpacing_17; }
	inline void set_m_HorizontalScrollbarSpacing_17(float value)
	{
		___m_HorizontalScrollbarSpacing_17 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarSpacing_18() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_VerticalScrollbarSpacing_18)); }
	inline float get_m_VerticalScrollbarSpacing_18() const { return ___m_VerticalScrollbarSpacing_18; }
	inline float* get_address_of_m_VerticalScrollbarSpacing_18() { return &___m_VerticalScrollbarSpacing_18; }
	inline void set_m_VerticalScrollbarSpacing_18(float value)
	{
		___m_VerticalScrollbarSpacing_18 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_19() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_OnValueChanged_19)); }
	inline ScrollRectEvent_tA2F08EF8BB0B0B0F72DB8242DC5AB17BB0D1731E * get_m_OnValueChanged_19() const { return ___m_OnValueChanged_19; }
	inline ScrollRectEvent_tA2F08EF8BB0B0B0F72DB8242DC5AB17BB0D1731E ** get_address_of_m_OnValueChanged_19() { return &___m_OnValueChanged_19; }
	inline void set_m_OnValueChanged_19(ScrollRectEvent_tA2F08EF8BB0B0B0F72DB8242DC5AB17BB0D1731E * value)
	{
		___m_OnValueChanged_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_PointerStartLocalCursor_20() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_PointerStartLocalCursor_20)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_PointerStartLocalCursor_20() const { return ___m_PointerStartLocalCursor_20; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_PointerStartLocalCursor_20() { return &___m_PointerStartLocalCursor_20; }
	inline void set_m_PointerStartLocalCursor_20(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_PointerStartLocalCursor_20 = value;
	}

	inline static int32_t get_offset_of_m_ContentStartPosition_21() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_ContentStartPosition_21)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_ContentStartPosition_21() const { return ___m_ContentStartPosition_21; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_ContentStartPosition_21() { return &___m_ContentStartPosition_21; }
	inline void set_m_ContentStartPosition_21(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_ContentStartPosition_21 = value;
	}

	inline static int32_t get_offset_of_m_ViewRect_22() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_ViewRect_22)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_ViewRect_22() const { return ___m_ViewRect_22; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_ViewRect_22() { return &___m_ViewRect_22; }
	inline void set_m_ViewRect_22(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_ViewRect_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ViewRect_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ContentBounds_23() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_ContentBounds_23)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_m_ContentBounds_23() const { return ___m_ContentBounds_23; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_m_ContentBounds_23() { return &___m_ContentBounds_23; }
	inline void set_m_ContentBounds_23(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___m_ContentBounds_23 = value;
	}

	inline static int32_t get_offset_of_m_ViewBounds_24() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_ViewBounds_24)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_m_ViewBounds_24() const { return ___m_ViewBounds_24; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_m_ViewBounds_24() { return &___m_ViewBounds_24; }
	inline void set_m_ViewBounds_24(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___m_ViewBounds_24 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_25() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Velocity_25)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Velocity_25() const { return ___m_Velocity_25; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Velocity_25() { return &___m_Velocity_25; }
	inline void set_m_Velocity_25(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Velocity_25 = value;
	}

	inline static int32_t get_offset_of_m_Dragging_26() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Dragging_26)); }
	inline bool get_m_Dragging_26() const { return ___m_Dragging_26; }
	inline bool* get_address_of_m_Dragging_26() { return &___m_Dragging_26; }
	inline void set_m_Dragging_26(bool value)
	{
		___m_Dragging_26 = value;
	}

	inline static int32_t get_offset_of_m_Scrolling_27() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Scrolling_27)); }
	inline bool get_m_Scrolling_27() const { return ___m_Scrolling_27; }
	inline bool* get_address_of_m_Scrolling_27() { return &___m_Scrolling_27; }
	inline void set_m_Scrolling_27(bool value)
	{
		___m_Scrolling_27 = value;
	}

	inline static int32_t get_offset_of_m_PrevPosition_28() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_PrevPosition_28)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_PrevPosition_28() const { return ___m_PrevPosition_28; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_PrevPosition_28() { return &___m_PrevPosition_28; }
	inline void set_m_PrevPosition_28(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_PrevPosition_28 = value;
	}

	inline static int32_t get_offset_of_m_PrevContentBounds_29() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_PrevContentBounds_29)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_m_PrevContentBounds_29() const { return ___m_PrevContentBounds_29; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_m_PrevContentBounds_29() { return &___m_PrevContentBounds_29; }
	inline void set_m_PrevContentBounds_29(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___m_PrevContentBounds_29 = value;
	}

	inline static int32_t get_offset_of_m_PrevViewBounds_30() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_PrevViewBounds_30)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_m_PrevViewBounds_30() const { return ___m_PrevViewBounds_30; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_m_PrevViewBounds_30() { return &___m_PrevViewBounds_30; }
	inline void set_m_PrevViewBounds_30(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___m_PrevViewBounds_30 = value;
	}

	inline static int32_t get_offset_of_m_HasRebuiltLayout_31() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HasRebuiltLayout_31)); }
	inline bool get_m_HasRebuiltLayout_31() const { return ___m_HasRebuiltLayout_31; }
	inline bool* get_address_of_m_HasRebuiltLayout_31() { return &___m_HasRebuiltLayout_31; }
	inline void set_m_HasRebuiltLayout_31(bool value)
	{
		___m_HasRebuiltLayout_31 = value;
	}

	inline static int32_t get_offset_of_m_HSliderExpand_32() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HSliderExpand_32)); }
	inline bool get_m_HSliderExpand_32() const { return ___m_HSliderExpand_32; }
	inline bool* get_address_of_m_HSliderExpand_32() { return &___m_HSliderExpand_32; }
	inline void set_m_HSliderExpand_32(bool value)
	{
		___m_HSliderExpand_32 = value;
	}

	inline static int32_t get_offset_of_m_VSliderExpand_33() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_VSliderExpand_33)); }
	inline bool get_m_VSliderExpand_33() const { return ___m_VSliderExpand_33; }
	inline bool* get_address_of_m_VSliderExpand_33() { return &___m_VSliderExpand_33; }
	inline void set_m_VSliderExpand_33(bool value)
	{
		___m_VSliderExpand_33 = value;
	}

	inline static int32_t get_offset_of_m_HSliderHeight_34() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HSliderHeight_34)); }
	inline float get_m_HSliderHeight_34() const { return ___m_HSliderHeight_34; }
	inline float* get_address_of_m_HSliderHeight_34() { return &___m_HSliderHeight_34; }
	inline void set_m_HSliderHeight_34(float value)
	{
		___m_HSliderHeight_34 = value;
	}

	inline static int32_t get_offset_of_m_VSliderWidth_35() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_VSliderWidth_35)); }
	inline float get_m_VSliderWidth_35() const { return ___m_VSliderWidth_35; }
	inline float* get_address_of_m_VSliderWidth_35() { return &___m_VSliderWidth_35; }
	inline void set_m_VSliderWidth_35(float value)
	{
		___m_VSliderWidth_35 = value;
	}

	inline static int32_t get_offset_of_m_Rect_36() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Rect_36)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_Rect_36() const { return ___m_Rect_36; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_Rect_36() { return &___m_Rect_36; }
	inline void set_m_Rect_36(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_Rect_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Rect_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarRect_37() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_HorizontalScrollbarRect_37)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HorizontalScrollbarRect_37() const { return ___m_HorizontalScrollbarRect_37; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HorizontalScrollbarRect_37() { return &___m_HorizontalScrollbarRect_37; }
	inline void set_m_HorizontalScrollbarRect_37(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HorizontalScrollbarRect_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HorizontalScrollbarRect_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarRect_38() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_VerticalScrollbarRect_38)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_VerticalScrollbarRect_38() const { return ___m_VerticalScrollbarRect_38; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_VerticalScrollbarRect_38() { return &___m_VerticalScrollbarRect_38; }
	inline void set_m_VerticalScrollbarRect_38(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_VerticalScrollbarRect_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_VerticalScrollbarRect_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tracker_39() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Tracker_39)); }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  get_m_Tracker_39() const { return ___m_Tracker_39; }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * get_address_of_m_Tracker_39() { return &___m_Tracker_39; }
	inline void set_m_Tracker_39(DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  value)
	{
		___m_Tracker_39 = value;
	}

	inline static int32_t get_offset_of_m_Corners_40() { return static_cast<int32_t>(offsetof(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA, ___m_Corners_40)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_40() const { return ___m_Corners_40; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_40() { return &___m_Corners_40; }
	inline void set_m_Corners_40(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_40), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};


// UnityEngine.UI.InputField
struct InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.TouchScreenKeyboard UnityEngine.UI.InputField::m_Keyboard
	TouchScreenKeyboard_t7964B2E9E52C4E095B14F01C32774B98CA11711E * ___m_Keyboard_20;
	// UnityEngine.UI.Text UnityEngine.UI.InputField::m_TextComponent
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_TextComponent_24;
	// UnityEngine.UI.Graphic UnityEngine.UI.InputField::m_Placeholder
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_Placeholder_25;
	// UnityEngine.UI.InputField/ContentType UnityEngine.UI.InputField::m_ContentType
	int32_t ___m_ContentType_26;
	// UnityEngine.UI.InputField/InputType UnityEngine.UI.InputField::m_InputType
	int32_t ___m_InputType_27;
	// System.Char UnityEngine.UI.InputField::m_AsteriskChar
	Il2CppChar ___m_AsteriskChar_28;
	// UnityEngine.TouchScreenKeyboardType UnityEngine.UI.InputField::m_KeyboardType
	int32_t ___m_KeyboardType_29;
	// UnityEngine.UI.InputField/LineType UnityEngine.UI.InputField::m_LineType
	int32_t ___m_LineType_30;
	// System.Boolean UnityEngine.UI.InputField::m_HideMobileInput
	bool ___m_HideMobileInput_31;
	// UnityEngine.UI.InputField/CharacterValidation UnityEngine.UI.InputField::m_CharacterValidation
	int32_t ___m_CharacterValidation_32;
	// System.Int32 UnityEngine.UI.InputField::m_CharacterLimit
	int32_t ___m_CharacterLimit_33;
	// UnityEngine.UI.InputField/SubmitEvent UnityEngine.UI.InputField::m_OnEndEdit
	SubmitEvent_t3FD30F627DF2ADEC87C0BE69EE632AAB99F3B8A9 * ___m_OnEndEdit_34;
	// UnityEngine.UI.InputField/OnChangeEvent UnityEngine.UI.InputField::m_OnValueChanged
	OnChangeEvent_t2E59014A56EA94168140F0585834954B40D716F7 * ___m_OnValueChanged_35;
	// UnityEngine.UI.InputField/OnValidateInput UnityEngine.UI.InputField::m_OnValidateInput
	OnValidateInput_t721D2C2A7710D113E4909B36D9893CC6B1C69B9F * ___m_OnValidateInput_36;
	// UnityEngine.Color UnityEngine.UI.InputField::m_CaretColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_CaretColor_37;
	// System.Boolean UnityEngine.UI.InputField::m_CustomCaretColor
	bool ___m_CustomCaretColor_38;
	// UnityEngine.Color UnityEngine.UI.InputField::m_SelectionColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectionColor_39;
	// System.String UnityEngine.UI.InputField::m_Text
	String_t* ___m_Text_40;
	// System.Single UnityEngine.UI.InputField::m_CaretBlinkRate
	float ___m_CaretBlinkRate_41;
	// System.Int32 UnityEngine.UI.InputField::m_CaretWidth
	int32_t ___m_CaretWidth_42;
	// System.Boolean UnityEngine.UI.InputField::m_ReadOnly
	bool ___m_ReadOnly_43;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateOnSelect
	bool ___m_ShouldActivateOnSelect_44;
	// System.Int32 UnityEngine.UI.InputField::m_CaretPosition
	int32_t ___m_CaretPosition_45;
	// System.Int32 UnityEngine.UI.InputField::m_CaretSelectPosition
	int32_t ___m_CaretSelectPosition_46;
	// UnityEngine.RectTransform UnityEngine.UI.InputField::caretRectTrans
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___caretRectTrans_47;
	// UnityEngine.UIVertex[] UnityEngine.UI.InputField::m_CursorVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_CursorVerts_48;
	// UnityEngine.TextGenerator UnityEngine.UI.InputField::m_InputTextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_InputTextCache_49;
	// UnityEngine.CanvasRenderer UnityEngine.UI.InputField::m_CachedInputRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CachedInputRenderer_50;
	// System.Boolean UnityEngine.UI.InputField::m_PreventFontCallback
	bool ___m_PreventFontCallback_51;
	// UnityEngine.Mesh UnityEngine.UI.InputField::m_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_Mesh_52;
	// System.Boolean UnityEngine.UI.InputField::m_AllowInput
	bool ___m_AllowInput_53;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateNextUpdate
	bool ___m_ShouldActivateNextUpdate_54;
	// System.Boolean UnityEngine.UI.InputField::m_UpdateDrag
	bool ___m_UpdateDrag_55;
	// System.Boolean UnityEngine.UI.InputField::m_DragPositionOutOfBounds
	bool ___m_DragPositionOutOfBounds_56;
	// System.Boolean UnityEngine.UI.InputField::m_CaretVisible
	bool ___m_CaretVisible_59;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_BlinkCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ___m_BlinkCoroutine_60;
	// System.Single UnityEngine.UI.InputField::m_BlinkStartTime
	float ___m_BlinkStartTime_61;
	// System.Int32 UnityEngine.UI.InputField::m_DrawStart
	int32_t ___m_DrawStart_62;
	// System.Int32 UnityEngine.UI.InputField::m_DrawEnd
	int32_t ___m_DrawEnd_63;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_DragCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ___m_DragCoroutine_64;
	// System.String UnityEngine.UI.InputField::m_OriginalText
	String_t* ___m_OriginalText_65;
	// System.Boolean UnityEngine.UI.InputField::m_WasCanceled
	bool ___m_WasCanceled_66;
	// System.Boolean UnityEngine.UI.InputField::m_HasDoneFocusTransition
	bool ___m_HasDoneFocusTransition_67;
	// UnityEngine.WaitForSecondsRealtime UnityEngine.UI.InputField::m_WaitForSecondsRealtime
	WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * ___m_WaitForSecondsRealtime_68;
	// System.Boolean UnityEngine.UI.InputField::m_TouchKeyboardAllowsInPlaceEditing
	bool ___m_TouchKeyboardAllowsInPlaceEditing_69;
	// System.Boolean UnityEngine.UI.InputField::m_IsCompositionActive
	bool ___m_IsCompositionActive_70;
	// UnityEngine.Event UnityEngine.UI.InputField::m_ProcessingEvent
	Event_tED49F8EC5A2514F6E877E301B1AB7ABE4647253E * ___m_ProcessingEvent_73;

public:
	inline static int32_t get_offset_of_m_Keyboard_20() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_Keyboard_20)); }
	inline TouchScreenKeyboard_t7964B2E9E52C4E095B14F01C32774B98CA11711E * get_m_Keyboard_20() const { return ___m_Keyboard_20; }
	inline TouchScreenKeyboard_t7964B2E9E52C4E095B14F01C32774B98CA11711E ** get_address_of_m_Keyboard_20() { return &___m_Keyboard_20; }
	inline void set_m_Keyboard_20(TouchScreenKeyboard_t7964B2E9E52C4E095B14F01C32774B98CA11711E * value)
	{
		___m_Keyboard_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Keyboard_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextComponent_24() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_TextComponent_24)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_TextComponent_24() const { return ___m_TextComponent_24; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_TextComponent_24() { return &___m_TextComponent_24; }
	inline void set_m_TextComponent_24(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_TextComponent_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_Placeholder_25() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_Placeholder_25)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_Placeholder_25() const { return ___m_Placeholder_25; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_Placeholder_25() { return &___m_Placeholder_25; }
	inline void set_m_Placeholder_25(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_Placeholder_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Placeholder_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_ContentType_26() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_ContentType_26)); }
	inline int32_t get_m_ContentType_26() const { return ___m_ContentType_26; }
	inline int32_t* get_address_of_m_ContentType_26() { return &___m_ContentType_26; }
	inline void set_m_ContentType_26(int32_t value)
	{
		___m_ContentType_26 = value;
	}

	inline static int32_t get_offset_of_m_InputType_27() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_InputType_27)); }
	inline int32_t get_m_InputType_27() const { return ___m_InputType_27; }
	inline int32_t* get_address_of_m_InputType_27() { return &___m_InputType_27; }
	inline void set_m_InputType_27(int32_t value)
	{
		___m_InputType_27 = value;
	}

	inline static int32_t get_offset_of_m_AsteriskChar_28() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_AsteriskChar_28)); }
	inline Il2CppChar get_m_AsteriskChar_28() const { return ___m_AsteriskChar_28; }
	inline Il2CppChar* get_address_of_m_AsteriskChar_28() { return &___m_AsteriskChar_28; }
	inline void set_m_AsteriskChar_28(Il2CppChar value)
	{
		___m_AsteriskChar_28 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardType_29() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_KeyboardType_29)); }
	inline int32_t get_m_KeyboardType_29() const { return ___m_KeyboardType_29; }
	inline int32_t* get_address_of_m_KeyboardType_29() { return &___m_KeyboardType_29; }
	inline void set_m_KeyboardType_29(int32_t value)
	{
		___m_KeyboardType_29 = value;
	}

	inline static int32_t get_offset_of_m_LineType_30() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_LineType_30)); }
	inline int32_t get_m_LineType_30() const { return ___m_LineType_30; }
	inline int32_t* get_address_of_m_LineType_30() { return &___m_LineType_30; }
	inline void set_m_LineType_30(int32_t value)
	{
		___m_LineType_30 = value;
	}

	inline static int32_t get_offset_of_m_HideMobileInput_31() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_HideMobileInput_31)); }
	inline bool get_m_HideMobileInput_31() const { return ___m_HideMobileInput_31; }
	inline bool* get_address_of_m_HideMobileInput_31() { return &___m_HideMobileInput_31; }
	inline void set_m_HideMobileInput_31(bool value)
	{
		___m_HideMobileInput_31 = value;
	}

	inline static int32_t get_offset_of_m_CharacterValidation_32() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CharacterValidation_32)); }
	inline int32_t get_m_CharacterValidation_32() const { return ___m_CharacterValidation_32; }
	inline int32_t* get_address_of_m_CharacterValidation_32() { return &___m_CharacterValidation_32; }
	inline void set_m_CharacterValidation_32(int32_t value)
	{
		___m_CharacterValidation_32 = value;
	}

	inline static int32_t get_offset_of_m_CharacterLimit_33() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CharacterLimit_33)); }
	inline int32_t get_m_CharacterLimit_33() const { return ___m_CharacterLimit_33; }
	inline int32_t* get_address_of_m_CharacterLimit_33() { return &___m_CharacterLimit_33; }
	inline void set_m_CharacterLimit_33(int32_t value)
	{
		___m_CharacterLimit_33 = value;
	}

	inline static int32_t get_offset_of_m_OnEndEdit_34() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_OnEndEdit_34)); }
	inline SubmitEvent_t3FD30F627DF2ADEC87C0BE69EE632AAB99F3B8A9 * get_m_OnEndEdit_34() const { return ___m_OnEndEdit_34; }
	inline SubmitEvent_t3FD30F627DF2ADEC87C0BE69EE632AAB99F3B8A9 ** get_address_of_m_OnEndEdit_34() { return &___m_OnEndEdit_34; }
	inline void set_m_OnEndEdit_34(SubmitEvent_t3FD30F627DF2ADEC87C0BE69EE632AAB99F3B8A9 * value)
	{
		___m_OnEndEdit_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnEndEdit_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_35() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_OnValueChanged_35)); }
	inline OnChangeEvent_t2E59014A56EA94168140F0585834954B40D716F7 * get_m_OnValueChanged_35() const { return ___m_OnValueChanged_35; }
	inline OnChangeEvent_t2E59014A56EA94168140F0585834954B40D716F7 ** get_address_of_m_OnValueChanged_35() { return &___m_OnValueChanged_35; }
	inline void set_m_OnValueChanged_35(OnChangeEvent_t2E59014A56EA94168140F0585834954B40D716F7 * value)
	{
		___m_OnValueChanged_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnValidateInput_36() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_OnValidateInput_36)); }
	inline OnValidateInput_t721D2C2A7710D113E4909B36D9893CC6B1C69B9F * get_m_OnValidateInput_36() const { return ___m_OnValidateInput_36; }
	inline OnValidateInput_t721D2C2A7710D113E4909B36D9893CC6B1C69B9F ** get_address_of_m_OnValidateInput_36() { return &___m_OnValidateInput_36; }
	inline void set_m_OnValidateInput_36(OnValidateInput_t721D2C2A7710D113E4909B36D9893CC6B1C69B9F * value)
	{
		___m_OnValidateInput_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValidateInput_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_CaretColor_37() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CaretColor_37)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_CaretColor_37() const { return ___m_CaretColor_37; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_CaretColor_37() { return &___m_CaretColor_37; }
	inline void set_m_CaretColor_37(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_CaretColor_37 = value;
	}

	inline static int32_t get_offset_of_m_CustomCaretColor_38() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CustomCaretColor_38)); }
	inline bool get_m_CustomCaretColor_38() const { return ___m_CustomCaretColor_38; }
	inline bool* get_address_of_m_CustomCaretColor_38() { return &___m_CustomCaretColor_38; }
	inline void set_m_CustomCaretColor_38(bool value)
	{
		___m_CustomCaretColor_38 = value;
	}

	inline static int32_t get_offset_of_m_SelectionColor_39() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_SelectionColor_39)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectionColor_39() const { return ___m_SelectionColor_39; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectionColor_39() { return &___m_SelectionColor_39; }
	inline void set_m_SelectionColor_39(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectionColor_39 = value;
	}

	inline static int32_t get_offset_of_m_Text_40() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_Text_40)); }
	inline String_t* get_m_Text_40() const { return ___m_Text_40; }
	inline String_t** get_address_of_m_Text_40() { return &___m_Text_40; }
	inline void set_m_Text_40(String_t* value)
	{
		___m_Text_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_40), (void*)value);
	}

	inline static int32_t get_offset_of_m_CaretBlinkRate_41() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CaretBlinkRate_41)); }
	inline float get_m_CaretBlinkRate_41() const { return ___m_CaretBlinkRate_41; }
	inline float* get_address_of_m_CaretBlinkRate_41() { return &___m_CaretBlinkRate_41; }
	inline void set_m_CaretBlinkRate_41(float value)
	{
		___m_CaretBlinkRate_41 = value;
	}

	inline static int32_t get_offset_of_m_CaretWidth_42() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CaretWidth_42)); }
	inline int32_t get_m_CaretWidth_42() const { return ___m_CaretWidth_42; }
	inline int32_t* get_address_of_m_CaretWidth_42() { return &___m_CaretWidth_42; }
	inline void set_m_CaretWidth_42(int32_t value)
	{
		___m_CaretWidth_42 = value;
	}

	inline static int32_t get_offset_of_m_ReadOnly_43() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_ReadOnly_43)); }
	inline bool get_m_ReadOnly_43() const { return ___m_ReadOnly_43; }
	inline bool* get_address_of_m_ReadOnly_43() { return &___m_ReadOnly_43; }
	inline void set_m_ReadOnly_43(bool value)
	{
		___m_ReadOnly_43 = value;
	}

	inline static int32_t get_offset_of_m_ShouldActivateOnSelect_44() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_ShouldActivateOnSelect_44)); }
	inline bool get_m_ShouldActivateOnSelect_44() const { return ___m_ShouldActivateOnSelect_44; }
	inline bool* get_address_of_m_ShouldActivateOnSelect_44() { return &___m_ShouldActivateOnSelect_44; }
	inline void set_m_ShouldActivateOnSelect_44(bool value)
	{
		___m_ShouldActivateOnSelect_44 = value;
	}

	inline static int32_t get_offset_of_m_CaretPosition_45() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CaretPosition_45)); }
	inline int32_t get_m_CaretPosition_45() const { return ___m_CaretPosition_45; }
	inline int32_t* get_address_of_m_CaretPosition_45() { return &___m_CaretPosition_45; }
	inline void set_m_CaretPosition_45(int32_t value)
	{
		___m_CaretPosition_45 = value;
	}

	inline static int32_t get_offset_of_m_CaretSelectPosition_46() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CaretSelectPosition_46)); }
	inline int32_t get_m_CaretSelectPosition_46() const { return ___m_CaretSelectPosition_46; }
	inline int32_t* get_address_of_m_CaretSelectPosition_46() { return &___m_CaretSelectPosition_46; }
	inline void set_m_CaretSelectPosition_46(int32_t value)
	{
		___m_CaretSelectPosition_46 = value;
	}

	inline static int32_t get_offset_of_caretRectTrans_47() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___caretRectTrans_47)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_caretRectTrans_47() const { return ___caretRectTrans_47; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_caretRectTrans_47() { return &___caretRectTrans_47; }
	inline void set_caretRectTrans_47(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___caretRectTrans_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___caretRectTrans_47), (void*)value);
	}

	inline static int32_t get_offset_of_m_CursorVerts_48() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CursorVerts_48)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_CursorVerts_48() const { return ___m_CursorVerts_48; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_CursorVerts_48() { return &___m_CursorVerts_48; }
	inline void set_m_CursorVerts_48(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_CursorVerts_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CursorVerts_48), (void*)value);
	}

	inline static int32_t get_offset_of_m_InputTextCache_49() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_InputTextCache_49)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_InputTextCache_49() const { return ___m_InputTextCache_49; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_InputTextCache_49() { return &___m_InputTextCache_49; }
	inline void set_m_InputTextCache_49(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_InputTextCache_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InputTextCache_49), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedInputRenderer_50() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CachedInputRenderer_50)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CachedInputRenderer_50() const { return ___m_CachedInputRenderer_50; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CachedInputRenderer_50() { return &___m_CachedInputRenderer_50; }
	inline void set_m_CachedInputRenderer_50(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CachedInputRenderer_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedInputRenderer_50), (void*)value);
	}

	inline static int32_t get_offset_of_m_PreventFontCallback_51() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_PreventFontCallback_51)); }
	inline bool get_m_PreventFontCallback_51() const { return ___m_PreventFontCallback_51; }
	inline bool* get_address_of_m_PreventFontCallback_51() { return &___m_PreventFontCallback_51; }
	inline void set_m_PreventFontCallback_51(bool value)
	{
		___m_PreventFontCallback_51 = value;
	}

	inline static int32_t get_offset_of_m_Mesh_52() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_Mesh_52)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_Mesh_52() const { return ___m_Mesh_52; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_Mesh_52() { return &___m_Mesh_52; }
	inline void set_m_Mesh_52(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_Mesh_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Mesh_52), (void*)value);
	}

	inline static int32_t get_offset_of_m_AllowInput_53() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_AllowInput_53)); }
	inline bool get_m_AllowInput_53() const { return ___m_AllowInput_53; }
	inline bool* get_address_of_m_AllowInput_53() { return &___m_AllowInput_53; }
	inline void set_m_AllowInput_53(bool value)
	{
		___m_AllowInput_53 = value;
	}

	inline static int32_t get_offset_of_m_ShouldActivateNextUpdate_54() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_ShouldActivateNextUpdate_54)); }
	inline bool get_m_ShouldActivateNextUpdate_54() const { return ___m_ShouldActivateNextUpdate_54; }
	inline bool* get_address_of_m_ShouldActivateNextUpdate_54() { return &___m_ShouldActivateNextUpdate_54; }
	inline void set_m_ShouldActivateNextUpdate_54(bool value)
	{
		___m_ShouldActivateNextUpdate_54 = value;
	}

	inline static int32_t get_offset_of_m_UpdateDrag_55() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_UpdateDrag_55)); }
	inline bool get_m_UpdateDrag_55() const { return ___m_UpdateDrag_55; }
	inline bool* get_address_of_m_UpdateDrag_55() { return &___m_UpdateDrag_55; }
	inline void set_m_UpdateDrag_55(bool value)
	{
		___m_UpdateDrag_55 = value;
	}

	inline static int32_t get_offset_of_m_DragPositionOutOfBounds_56() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_DragPositionOutOfBounds_56)); }
	inline bool get_m_DragPositionOutOfBounds_56() const { return ___m_DragPositionOutOfBounds_56; }
	inline bool* get_address_of_m_DragPositionOutOfBounds_56() { return &___m_DragPositionOutOfBounds_56; }
	inline void set_m_DragPositionOutOfBounds_56(bool value)
	{
		___m_DragPositionOutOfBounds_56 = value;
	}

	inline static int32_t get_offset_of_m_CaretVisible_59() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_CaretVisible_59)); }
	inline bool get_m_CaretVisible_59() const { return ___m_CaretVisible_59; }
	inline bool* get_address_of_m_CaretVisible_59() { return &___m_CaretVisible_59; }
	inline void set_m_CaretVisible_59(bool value)
	{
		___m_CaretVisible_59 = value;
	}

	inline static int32_t get_offset_of_m_BlinkCoroutine_60() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_BlinkCoroutine_60)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get_m_BlinkCoroutine_60() const { return ___m_BlinkCoroutine_60; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of_m_BlinkCoroutine_60() { return &___m_BlinkCoroutine_60; }
	inline void set_m_BlinkCoroutine_60(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		___m_BlinkCoroutine_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BlinkCoroutine_60), (void*)value);
	}

	inline static int32_t get_offset_of_m_BlinkStartTime_61() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_BlinkStartTime_61)); }
	inline float get_m_BlinkStartTime_61() const { return ___m_BlinkStartTime_61; }
	inline float* get_address_of_m_BlinkStartTime_61() { return &___m_BlinkStartTime_61; }
	inline void set_m_BlinkStartTime_61(float value)
	{
		___m_BlinkStartTime_61 = value;
	}

	inline static int32_t get_offset_of_m_DrawStart_62() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_DrawStart_62)); }
	inline int32_t get_m_DrawStart_62() const { return ___m_DrawStart_62; }
	inline int32_t* get_address_of_m_DrawStart_62() { return &___m_DrawStart_62; }
	inline void set_m_DrawStart_62(int32_t value)
	{
		___m_DrawStart_62 = value;
	}

	inline static int32_t get_offset_of_m_DrawEnd_63() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_DrawEnd_63)); }
	inline int32_t get_m_DrawEnd_63() const { return ___m_DrawEnd_63; }
	inline int32_t* get_address_of_m_DrawEnd_63() { return &___m_DrawEnd_63; }
	inline void set_m_DrawEnd_63(int32_t value)
	{
		___m_DrawEnd_63 = value;
	}

	inline static int32_t get_offset_of_m_DragCoroutine_64() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_DragCoroutine_64)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get_m_DragCoroutine_64() const { return ___m_DragCoroutine_64; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of_m_DragCoroutine_64() { return &___m_DragCoroutine_64; }
	inline void set_m_DragCoroutine_64(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		___m_DragCoroutine_64 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DragCoroutine_64), (void*)value);
	}

	inline static int32_t get_offset_of_m_OriginalText_65() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_OriginalText_65)); }
	inline String_t* get_m_OriginalText_65() const { return ___m_OriginalText_65; }
	inline String_t** get_address_of_m_OriginalText_65() { return &___m_OriginalText_65; }
	inline void set_m_OriginalText_65(String_t* value)
	{
		___m_OriginalText_65 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OriginalText_65), (void*)value);
	}

	inline static int32_t get_offset_of_m_WasCanceled_66() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_WasCanceled_66)); }
	inline bool get_m_WasCanceled_66() const { return ___m_WasCanceled_66; }
	inline bool* get_address_of_m_WasCanceled_66() { return &___m_WasCanceled_66; }
	inline void set_m_WasCanceled_66(bool value)
	{
		___m_WasCanceled_66 = value;
	}

	inline static int32_t get_offset_of_m_HasDoneFocusTransition_67() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_HasDoneFocusTransition_67)); }
	inline bool get_m_HasDoneFocusTransition_67() const { return ___m_HasDoneFocusTransition_67; }
	inline bool* get_address_of_m_HasDoneFocusTransition_67() { return &___m_HasDoneFocusTransition_67; }
	inline void set_m_HasDoneFocusTransition_67(bool value)
	{
		___m_HasDoneFocusTransition_67 = value;
	}

	inline static int32_t get_offset_of_m_WaitForSecondsRealtime_68() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_WaitForSecondsRealtime_68)); }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * get_m_WaitForSecondsRealtime_68() const { return ___m_WaitForSecondsRealtime_68; }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 ** get_address_of_m_WaitForSecondsRealtime_68() { return &___m_WaitForSecondsRealtime_68; }
	inline void set_m_WaitForSecondsRealtime_68(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * value)
	{
		___m_WaitForSecondsRealtime_68 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_WaitForSecondsRealtime_68), (void*)value);
	}

	inline static int32_t get_offset_of_m_TouchKeyboardAllowsInPlaceEditing_69() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_TouchKeyboardAllowsInPlaceEditing_69)); }
	inline bool get_m_TouchKeyboardAllowsInPlaceEditing_69() const { return ___m_TouchKeyboardAllowsInPlaceEditing_69; }
	inline bool* get_address_of_m_TouchKeyboardAllowsInPlaceEditing_69() { return &___m_TouchKeyboardAllowsInPlaceEditing_69; }
	inline void set_m_TouchKeyboardAllowsInPlaceEditing_69(bool value)
	{
		___m_TouchKeyboardAllowsInPlaceEditing_69 = value;
	}

	inline static int32_t get_offset_of_m_IsCompositionActive_70() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_IsCompositionActive_70)); }
	inline bool get_m_IsCompositionActive_70() const { return ___m_IsCompositionActive_70; }
	inline bool* get_address_of_m_IsCompositionActive_70() { return &___m_IsCompositionActive_70; }
	inline void set_m_IsCompositionActive_70(bool value)
	{
		___m_IsCompositionActive_70 = value;
	}

	inline static int32_t get_offset_of_m_ProcessingEvent_73() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0, ___m_ProcessingEvent_73)); }
	inline Event_tED49F8EC5A2514F6E877E301B1AB7ABE4647253E * get_m_ProcessingEvent_73() const { return ___m_ProcessingEvent_73; }
	inline Event_tED49F8EC5A2514F6E877E301B1AB7ABE4647253E ** get_address_of_m_ProcessingEvent_73() { return &___m_ProcessingEvent_73; }
	inline void set_m_ProcessingEvent_73(Event_tED49F8EC5A2514F6E877E301B1AB7ABE4647253E * value)
	{
		___m_ProcessingEvent_73 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ProcessingEvent_73), (void*)value);
	}
};


// System.Object


// System.Object


// System.Collections.Generic.HashSet`1<System.String>


// System.Collections.Generic.HashSet`1<System.String>


// System.Collections.Generic.List`1<System.Int32>

struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32>


// System.Collections.Generic.List`1<System.String>

struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.String>

struct Il2CppArrayBounds;

// System.Array

struct Il2CppArrayBounds;

// System.Array


// System.String

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.String


// System.ValueType


// System.ValueType


// SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid/<>c__DisplayClass3_0


// SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid/<>c__DisplayClass3_0


// SimpleFileBrowser.FileBrowser/<>c

struct U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_StaticFields
{
public:
	// SimpleFileBrowser.FileBrowser/<>c SimpleFileBrowser.FileBrowser/<>c::<>9
	U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D * ___U3CU3E9_0;
	// System.Comparison`1<SimpleFileBrowser.FileSystemEntry> SimpleFileBrowser.FileBrowser/<>c::<>9__236_0
	Comparison_1_t1A5DDD4BB3952FB12382582EA3C6A1DCD3A25210 * ___U3CU3E9__236_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__236_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_StaticFields, ___U3CU3E9__236_0_1)); }
	inline Comparison_1_t1A5DDD4BB3952FB12382582EA3C6A1DCD3A25210 * get_U3CU3E9__236_0_1() const { return ___U3CU3E9__236_0_1; }
	inline Comparison_1_t1A5DDD4BB3952FB12382582EA3C6A1DCD3A25210 ** get_address_of_U3CU3E9__236_0_1() { return &___U3CU3E9__236_0_1; }
	inline void set_U3CU3E9__236_0_1(Comparison_1_t1A5DDD4BB3952FB12382582EA3C6A1DCD3A25210 * value)
	{
		___U3CU3E9__236_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__236_0_1), (void*)value);
	}
};


// SimpleFileBrowser.FileBrowser/<>c


// SimpleFileBrowser.FileBrowser/<>c__DisplayClass220_0


// SimpleFileBrowser.FileBrowser/<>c__DisplayClass220_0


// SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241


// SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241


// SimpleFileBrowser.FileBrowser/Filter


// SimpleFileBrowser.FileBrowser/Filter


// System.Boolean

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Boolean


// System.Char

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Char


// UnityEngine.Color


// UnityEngine.Color


// UnityEngine.DrivenRectTransformTracker


// UnityEngine.DrivenRectTransformTracker


// System.Enum

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};


// System.Enum


// System.Int32


// System.Int32


// System.IntPtr

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.IntPtr


// System.Single


// System.Single


// UnityEngine.UI.SpriteState


// UnityEngine.UI.SpriteState


// UnityEngine.Vector2

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector2


// UnityEngine.Vector3

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector3


// System.Void


// System.Void


// SimpleFileBrowser.FileBrowser/FiletypeIcon


// SimpleFileBrowser.FileBrowser/FiletypeIcon


// UnityEngine.AndroidJavaProxy

struct AndroidJavaProxy_tA8C86826A74CB7CC5511CB353DBA595C9270D9AF_StaticFields
{
public:
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaProxy::s_JavaLangSystemClass
	GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * ___s_JavaLangSystemClass_2;
	// System.IntPtr UnityEngine.AndroidJavaProxy::s_HashCodeMethodID
	intptr_t ___s_HashCodeMethodID_3;

public:
	inline static int32_t get_offset_of_s_JavaLangSystemClass_2() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_tA8C86826A74CB7CC5511CB353DBA595C9270D9AF_StaticFields, ___s_JavaLangSystemClass_2)); }
	inline GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * get_s_JavaLangSystemClass_2() const { return ___s_JavaLangSystemClass_2; }
	inline GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 ** get_address_of_s_JavaLangSystemClass_2() { return &___s_JavaLangSystemClass_2; }
	inline void set_s_JavaLangSystemClass_2(GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * value)
	{
		___s_JavaLangSystemClass_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_JavaLangSystemClass_2), (void*)value);
	}

	inline static int32_t get_offset_of_s_HashCodeMethodID_3() { return static_cast<int32_t>(offsetof(AndroidJavaProxy_tA8C86826A74CB7CC5511CB353DBA595C9270D9AF_StaticFields, ___s_HashCodeMethodID_3)); }
	inline intptr_t get_s_HashCodeMethodID_3() const { return ___s_HashCodeMethodID_3; }
	inline intptr_t* get_address_of_s_HashCodeMethodID_3() { return &___s_HashCodeMethodID_3; }
	inline void set_s_HashCodeMethodID_3(intptr_t value)
	{
		___s_HashCodeMethodID_3 = value;
	}
};


// UnityEngine.AndroidJavaProxy


// UnityEngine.Bounds


// UnityEngine.Bounds


// UnityEngine.UI.ColorBlock

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// UnityEngine.UI.ColorBlock


// System.Globalization.CompareOptions


// System.Globalization.CompareOptions


// System.Delegate


// System.Delegate


// System.Exception

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};


// System.Exception


// System.IO.FileAttributes


// System.IO.FileAttributes


// UnityEngine.Object

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};


// UnityEngine.Object


// System.StringComparison


// System.StringComparison


// UnityEngine.TouchScreenKeyboardType


// UnityEngine.TouchScreenKeyboardType


// System.Environment/SpecialFolder


// System.Environment/SpecialFolder


// SimpleFileBrowser.FileBrowser/Permission


// SimpleFileBrowser.FileBrowser/Permission


// SimpleFileBrowser.FileBrowser/PickMode


// SimpleFileBrowser.FileBrowser/PickMode


// SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OperationType


// SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OperationType


// UnityEngine.UI.InputField/CharacterValidation


// UnityEngine.UI.InputField/CharacterValidation


// UnityEngine.UI.InputField/ContentType


// UnityEngine.UI.InputField/ContentType


// UnityEngine.UI.InputField/InputType


// UnityEngine.UI.InputField/InputType


// UnityEngine.UI.InputField/LineType


// UnityEngine.UI.InputField/LineType


// UnityEngine.UI.Navigation/Mode


// UnityEngine.UI.Navigation/Mode


// UnityEngine.UI.ScrollRect/MovementType


// UnityEngine.UI.ScrollRect/MovementType


// UnityEngine.UI.ScrollRect/ScrollbarVisibility


// UnityEngine.UI.ScrollRect/ScrollbarVisibility


// UnityEngine.UI.Selectable/Transition


// UnityEngine.UI.Selectable/Transition


// UnityEngine.Component


// UnityEngine.Component


// SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid


// SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid


// SimpleFileBrowser.FileSystemEntry


// SimpleFileBrowser.FileSystemEntry


// UnityEngine.GameObject


// UnityEngine.GameObject


// System.MulticastDelegate


// System.MulticastDelegate


// UnityEngine.UI.Navigation


// UnityEngine.UI.Navigation


// UnityEngine.ScriptableObject


// UnityEngine.ScriptableObject


// UnityEngine.Sprite


// UnityEngine.Sprite


// System.SystemException


// System.SystemException


// SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266


// SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266


// SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265


// SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265


// SimpleFileBrowser.FileBrowser/QuickLink


// SimpleFileBrowser.FileBrowser/QuickLink


// System.AsyncCallback


// System.AsyncCallback


// UnityEngine.Behaviour


// UnityEngine.Behaviour


// System.NotSupportedException


// System.NotSupportedException


// UnityEngine.Transform


// UnityEngine.Transform


// SimpleFileBrowser.UISkin


// SimpleFileBrowser.UISkin


// SimpleFileBrowser.FileBrowser/<>c__DisplayClass242_0


// SimpleFileBrowser.FileBrowser/<>c__DisplayClass242_0


// SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback


// SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback


// SimpleFileBrowser.FileBrowser/FileSystemEntryFilter


// SimpleFileBrowser.FileBrowser/FileSystemEntryFilter


// SimpleFileBrowser.FileBrowser/OnCancel


// SimpleFileBrowser.FileBrowser/OnCancel


// SimpleFileBrowser.FileBrowser/OnSuccess


// SimpleFileBrowser.FileBrowser/OnSuccess


// SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed


// SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed


// SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted


// SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted


// UnityEngine.MonoBehaviour


// UnityEngine.MonoBehaviour


// UnityEngine.RectTransform

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// UnityEngine.RectTransform


// SimpleFileBrowser.FileBrowser

struct FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields
{
public:
	// System.Boolean SimpleFileBrowser.FileBrowser::<IsOpen>k__BackingField
	bool ___U3CIsOpenU3Ek__BackingField_6;
	// System.Boolean SimpleFileBrowser.FileBrowser::<Success>k__BackingField
	bool ___U3CSuccessU3Ek__BackingField_7;
	// System.String[] SimpleFileBrowser.FileBrowser::<Result>k__BackingField
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___U3CResultU3Ek__BackingField_8;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_askPermissions
	bool ___m_askPermissions_13;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_singleClickMode
	bool ___m_singleClickMode_14;
	// SimpleFileBrowser.FileBrowser/FileSystemEntryFilter SimpleFileBrowser.FileBrowser::m_displayedEntriesFilter
	FileSystemEntryFilter_t570DC98294020ED04118FDDBAA83AF79C201E4E1 * ___m_displayedEntriesFilter_15;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_showFileOverwriteDialog
	bool ___m_showFileOverwriteDialog_16;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_checkWriteAccessToDestinationDirectory
	bool ___m_checkWriteAccessToDestinationDirectory_17;
	// System.Single SimpleFileBrowser.FileBrowser::m_drivesRefreshInterval
	float ___m_drivesRefreshInterval_18;
	// System.Boolean SimpleFileBrowser.FileBrowser::m_displayHiddenFilesToggle
	bool ___m_displayHiddenFilesToggle_19;
	// System.String SimpleFileBrowser.FileBrowser::m_allFilesFilterText
	String_t* ___m_allFilesFilterText_20;
	// System.String SimpleFileBrowser.FileBrowser::m_foldersFilterText
	String_t* ___m_foldersFilterText_21;
	// System.String SimpleFileBrowser.FileBrowser::m_pickFolderQuickLinkText
	String_t* ___m_pickFolderQuickLinkText_22;
	// SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowser::m_instance
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * ___m_instance_23;
	// System.Boolean SimpleFileBrowser.FileBrowser::quickLinksInitialized
	bool ___quickLinksInitialized_31;

public:
	inline static int32_t get_offset_of_U3CIsOpenU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___U3CIsOpenU3Ek__BackingField_6)); }
	inline bool get_U3CIsOpenU3Ek__BackingField_6() const { return ___U3CIsOpenU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsOpenU3Ek__BackingField_6() { return &___U3CIsOpenU3Ek__BackingField_6; }
	inline void set_U3CIsOpenU3Ek__BackingField_6(bool value)
	{
		___U3CIsOpenU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CSuccessU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___U3CSuccessU3Ek__BackingField_7)); }
	inline bool get_U3CSuccessU3Ek__BackingField_7() const { return ___U3CSuccessU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CSuccessU3Ek__BackingField_7() { return &___U3CSuccessU3Ek__BackingField_7; }
	inline void set_U3CSuccessU3Ek__BackingField_7(bool value)
	{
		___U3CSuccessU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CResultU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___U3CResultU3Ek__BackingField_8)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_U3CResultU3Ek__BackingField_8() const { return ___U3CResultU3Ek__BackingField_8; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_U3CResultU3Ek__BackingField_8() { return &___U3CResultU3Ek__BackingField_8; }
	inline void set_U3CResultU3Ek__BackingField_8(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___U3CResultU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CResultU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_askPermissions_13() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_askPermissions_13)); }
	inline bool get_m_askPermissions_13() const { return ___m_askPermissions_13; }
	inline bool* get_address_of_m_askPermissions_13() { return &___m_askPermissions_13; }
	inline void set_m_askPermissions_13(bool value)
	{
		___m_askPermissions_13 = value;
	}

	inline static int32_t get_offset_of_m_singleClickMode_14() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_singleClickMode_14)); }
	inline bool get_m_singleClickMode_14() const { return ___m_singleClickMode_14; }
	inline bool* get_address_of_m_singleClickMode_14() { return &___m_singleClickMode_14; }
	inline void set_m_singleClickMode_14(bool value)
	{
		___m_singleClickMode_14 = value;
	}

	inline static int32_t get_offset_of_m_displayedEntriesFilter_15() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_displayedEntriesFilter_15)); }
	inline FileSystemEntryFilter_t570DC98294020ED04118FDDBAA83AF79C201E4E1 * get_m_displayedEntriesFilter_15() const { return ___m_displayedEntriesFilter_15; }
	inline FileSystemEntryFilter_t570DC98294020ED04118FDDBAA83AF79C201E4E1 ** get_address_of_m_displayedEntriesFilter_15() { return &___m_displayedEntriesFilter_15; }
	inline void set_m_displayedEntriesFilter_15(FileSystemEntryFilter_t570DC98294020ED04118FDDBAA83AF79C201E4E1 * value)
	{
		___m_displayedEntriesFilter_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_displayedEntriesFilter_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_showFileOverwriteDialog_16() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_showFileOverwriteDialog_16)); }
	inline bool get_m_showFileOverwriteDialog_16() const { return ___m_showFileOverwriteDialog_16; }
	inline bool* get_address_of_m_showFileOverwriteDialog_16() { return &___m_showFileOverwriteDialog_16; }
	inline void set_m_showFileOverwriteDialog_16(bool value)
	{
		___m_showFileOverwriteDialog_16 = value;
	}

	inline static int32_t get_offset_of_m_checkWriteAccessToDestinationDirectory_17() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_checkWriteAccessToDestinationDirectory_17)); }
	inline bool get_m_checkWriteAccessToDestinationDirectory_17() const { return ___m_checkWriteAccessToDestinationDirectory_17; }
	inline bool* get_address_of_m_checkWriteAccessToDestinationDirectory_17() { return &___m_checkWriteAccessToDestinationDirectory_17; }
	inline void set_m_checkWriteAccessToDestinationDirectory_17(bool value)
	{
		___m_checkWriteAccessToDestinationDirectory_17 = value;
	}

	inline static int32_t get_offset_of_m_drivesRefreshInterval_18() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_drivesRefreshInterval_18)); }
	inline float get_m_drivesRefreshInterval_18() const { return ___m_drivesRefreshInterval_18; }
	inline float* get_address_of_m_drivesRefreshInterval_18() { return &___m_drivesRefreshInterval_18; }
	inline void set_m_drivesRefreshInterval_18(float value)
	{
		___m_drivesRefreshInterval_18 = value;
	}

	inline static int32_t get_offset_of_m_displayHiddenFilesToggle_19() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_displayHiddenFilesToggle_19)); }
	inline bool get_m_displayHiddenFilesToggle_19() const { return ___m_displayHiddenFilesToggle_19; }
	inline bool* get_address_of_m_displayHiddenFilesToggle_19() { return &___m_displayHiddenFilesToggle_19; }
	inline void set_m_displayHiddenFilesToggle_19(bool value)
	{
		___m_displayHiddenFilesToggle_19 = value;
	}

	inline static int32_t get_offset_of_m_allFilesFilterText_20() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_allFilesFilterText_20)); }
	inline String_t* get_m_allFilesFilterText_20() const { return ___m_allFilesFilterText_20; }
	inline String_t** get_address_of_m_allFilesFilterText_20() { return &___m_allFilesFilterText_20; }
	inline void set_m_allFilesFilterText_20(String_t* value)
	{
		___m_allFilesFilterText_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_allFilesFilterText_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_foldersFilterText_21() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_foldersFilterText_21)); }
	inline String_t* get_m_foldersFilterText_21() const { return ___m_foldersFilterText_21; }
	inline String_t** get_address_of_m_foldersFilterText_21() { return &___m_foldersFilterText_21; }
	inline void set_m_foldersFilterText_21(String_t* value)
	{
		___m_foldersFilterText_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_foldersFilterText_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_pickFolderQuickLinkText_22() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_pickFolderQuickLinkText_22)); }
	inline String_t* get_m_pickFolderQuickLinkText_22() const { return ___m_pickFolderQuickLinkText_22; }
	inline String_t** get_address_of_m_pickFolderQuickLinkText_22() { return &___m_pickFolderQuickLinkText_22; }
	inline void set_m_pickFolderQuickLinkText_22(String_t* value)
	{
		___m_pickFolderQuickLinkText_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_pickFolderQuickLinkText_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_instance_23() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___m_instance_23)); }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * get_m_instance_23() const { return ___m_instance_23; }
	inline FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 ** get_address_of_m_instance_23() { return &___m_instance_23; }
	inline void set_m_instance_23(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * value)
	{
		___m_instance_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_instance_23), (void*)value);
	}

	inline static int32_t get_offset_of_quickLinksInitialized_31() { return static_cast<int32_t>(offsetof(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_StaticFields, ___quickLinksInitialized_31)); }
	inline bool get_quickLinksInitialized_31() const { return ___quickLinksInitialized_31; }
	inline bool* get_address_of_quickLinksInitialized_31() { return &___quickLinksInitialized_31; }
	inline void set_quickLinksInitialized_31(bool value)
	{
		___quickLinksInitialized_31 = value;
	}
};


// SimpleFileBrowser.FileBrowser


// SimpleFileBrowser.FileBrowserRenamedItem


// SimpleFileBrowser.FileBrowserRenamedItem


// SimpleFileBrowser.RecycledListView


// SimpleFileBrowser.RecycledListView


// UnityEngine.EventSystems.UIBehaviour


// UnityEngine.EventSystems.UIBehaviour


// UnityEngine.UI.ScrollRect


// UnityEngine.UI.ScrollRect


// UnityEngine.UI.Selectable

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.Selectable


// UnityEngine.UI.InputField

struct InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0_StaticFields
{
public:
	// System.Char[] UnityEngine.UI.InputField::kSeparators
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___kSeparators_21;
	// System.Boolean UnityEngine.UI.InputField::s_IsQuestDeviceEvaluated
	bool ___s_IsQuestDeviceEvaluated_22;
	// System.Boolean UnityEngine.UI.InputField::s_IsQuestDevice
	bool ___s_IsQuestDevice_23;

public:
	inline static int32_t get_offset_of_kSeparators_21() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0_StaticFields, ___kSeparators_21)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_kSeparators_21() const { return ___kSeparators_21; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_kSeparators_21() { return &___kSeparators_21; }
	inline void set_kSeparators_21(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___kSeparators_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___kSeparators_21), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsQuestDeviceEvaluated_22() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0_StaticFields, ___s_IsQuestDeviceEvaluated_22)); }
	inline bool get_s_IsQuestDeviceEvaluated_22() const { return ___s_IsQuestDeviceEvaluated_22; }
	inline bool* get_address_of_s_IsQuestDeviceEvaluated_22() { return &___s_IsQuestDeviceEvaluated_22; }
	inline void set_s_IsQuestDeviceEvaluated_22(bool value)
	{
		___s_IsQuestDeviceEvaluated_22 = value;
	}

	inline static int32_t get_offset_of_s_IsQuestDevice_23() { return static_cast<int32_t>(offsetof(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0_StaticFields, ___s_IsQuestDevice_23)); }
	inline bool get_s_IsQuestDevice_23() const { return ___s_IsQuestDevice_23; }
	inline bool* get_address_of_s_IsQuestDevice_23() { return &___s_IsQuestDevice_23; }
	inline void set_s_IsQuestDevice_23(bool value)
	{
		___s_IsQuestDevice_23 = value;
	}
};


// UnityEngine.UI.InputField

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};

IL2CPP_EXTERN_C void FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshal_pinvoke(const FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9& unmarshaled, FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshaled_pinvoke& marshaled);
IL2CPP_EXTERN_C void FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshal_pinvoke_back(const FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshaled_pinvoke& marshaled, FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9& unmarshaled);
IL2CPP_EXTERN_C void FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshal_pinvoke_cleanup(FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshaled_pinvoke& marshaled);

// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HashSet_1__ctor_m2CDA40DEC2900A9CB00F8348FF386DF44ABD0EC7_gshared (HashSet_1_t680119C7ED8D82AED56CDB83DF6F0E9149852A9B * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HashSet_1_Add_mF670AD4C3F2685F0797E05C5491BC1841CEA9DBA_gshared (HashSet_1_t680119C7ED8D82AED56CDB83DF6F0E9149852A9B * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HashSet_1__ctor_m9D936778F28043838186FC2037F47460DA7925B9_gshared (HashSet_1_t680119C7ED8D82AED56CDB83DF6F0E9149852A9B * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HashSet_1_Contains_m91084DCF8BFB10517C15219307054B7B971AC8A5_gshared (HashSet_1_t680119C7ED8D82AED56CDB83DF6F0E9149852A9B * __this, RuntimeObject * ___item0, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid::DirectoryPickedCallback(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FBDirectoryReceiveCallbackAndroid_DirectoryPickedCallback_m7039234E4669DC9103BE940172E633E59D435800 (FBDirectoryReceiveCallbackAndroid_t826BC254F0340D34CA548141A2130BBA31A5C154 * __this, String_t* ___rawUri0, String_t* ___name1, const RuntimeMethod* method);
// System.Void SimpleFileBrowser.FileBrowser/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m0C7FD05A63A799955E2C175B16DD6E2FA642261E (U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D * __this, const RuntimeMethod* method);
// System.Boolean SimpleFileBrowser.FileSystemEntry::get_IsDirectory()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F (FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 * __this, const RuntimeMethod* method);
// System.Int32 System.String::CompareTo(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t String_CompareTo_m01AF97548BE97133E2DC648B2E63BB16708354CF (String_t* __this, String_t* ___strB0, const RuntimeMethod* method);
// System.Void SimpleFileBrowser.FileBrowser::OnOperationSuccessful(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FileBrowser_OnOperationSuccessful_m5DC93791ECD6CBA227C3FA4436DB0EBE91A27A6C (FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___paths0, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m57429705D977ACD5EE7E210A858EED6F348C36B3 (String_t* ___value0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m50B3548E4AC232558190B0052877B290AA1D436A (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.String SimpleFileBrowser.FileBrowserHelpers::RenameDirectory(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FileBrowserHelpers_RenameDirectory_m070EEAEDFAD3768F3900854F66EE12D126C92D4D (String_t* ___path0, String_t* ___newName1, const RuntimeMethod* method);
// System.String SimpleFileBrowser.FileBrowserHelpers::RenameFile(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FileBrowserHelpers_RenameFile_mA204A30D5E284916B22AA30419A0412094C3AFB7 (String_t* ___path0, String_t* ___newName1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::Clear()
inline void List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640 (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, const RuntimeMethod*))List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
inline void List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * __this, String_t* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 *, String_t*, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void SimpleFileBrowser.FileBrowser::RefreshFiles(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FileBrowser_RefreshFiles_m78E8F197E37E572167B58ECF89907B0F937711D3 (FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * __this, bool ___pathChanged0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.InputField::set_text(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputField_set_text_m30EE95E064474DFA2842CA2DD6E831FF3F3500BF (InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.ScrollRect::set_verticalNormalizedPosition(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScrollRect_set_verticalNormalizedPosition_m18393A7B91359B013466A984325F233535A6941D (ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * __this, float ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_zero_mD28A0EC932762710AFA0BBB9CCC63716C3BD064D (const RuntimeMethod* method);
// System.Void UnityEngine.UI.ScrollRect::set_velocity(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ScrollRect_set_velocity_m6CBDADD5F61163917B37C922FA6F42170D68E1D9_inline (ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
inline int32_t List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
inline void List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_gshared)(__this, method);
}
// System.Void SimpleFileBrowser.FileBrowser::set_MultiSelectionToggleSelectionMode(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FileBrowser_set_MultiSelectionToggleSelectionMode_m176FE9DC46BDD2EB48C108D21831D1B08EB85934 (FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40 (Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * __this, bool ___value0, const RuntimeMethod* method);
// System.Void SimpleFileBrowser.RecycledListView::UpdateList()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RecycledListView_UpdateList_m0637428860547148FA94CA0267BBC6E06911F29F (RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.ScrollRect::set_movementType(UnityEngine.UI.ScrollRect/MovementType)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ScrollRect_set_movementType_m63E3C04ECBDA708AF2341564DB987227D32075CC_inline (ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Single SimpleFileBrowser.UISkin::get_FileHeight()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float UISkin_get_FileHeight_m784A41AB4290882A6FB5FF8F1D9A63B235F35A49_inline (UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Color SimpleFileBrowser.UISkin::get_FileSelectedBackgroundColor()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  UISkin_get_FileSelectedBackgroundColor_mECCF9DED2D0EA5CD1F72560F2DB1086043F87DCD_inline (UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * __this, const RuntimeMethod* method);
// UnityEngine.Sprite SimpleFileBrowser.UISkin::get_FolderIcon()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * UISkin_get_FolderIcon_m10703DA30265BC01B71DCE95B3899ACC1BFA3979_inline (UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * __this, const RuntimeMethod* method);
// System.Void SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnRenameCompleted__ctor_m019658E790544491DDBD4F208F8B422DC77764EB (OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void SimpleFileBrowser.FileBrowserRenamedItem::Show(System.String,UnityEngine.Color,UnityEngine.Sprite,SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FileBrowserRenamedItem_Show_m67CF833D01BEDC56743DF4EAC46AF60E8FFFB5DB (FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4 * __this, String_t* ___initialFilename0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___backgroundColor1, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___icon2, OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * ___onRenameCompleted3, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Boolean SimpleFileBrowser.FileBrowser::ShowLoadDialog(SimpleFileBrowser.FileBrowser/OnSuccess,SimpleFileBrowser.FileBrowser/OnCancel,SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FileBrowser_ShowLoadDialog_m73918E026EA31133F15BB56A903D4C1191FED21F (OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C * ___onSuccess0, OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E * ___onCancel1, int32_t ___pickMode2, bool ___allowMultiSelection3, String_t* ___initialPath4, String_t* ___initialFilename5, String_t* ___title6, String_t* ___loadButtonText7, const RuntimeMethod* method);
// SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowser::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * FileBrowser_get_Instance_m2D1CA7BB2DEE29A481F02D5DDC6B15727AE6457F (const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeSelf()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Boolean SimpleFileBrowser.FileBrowser::ShowSaveDialog(SimpleFileBrowser.FileBrowser/OnSuccess,SimpleFileBrowser.FileBrowser/OnCancel,SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FileBrowser_ShowSaveDialog_mC27C4233D80B769B4DD41FF34476296B811225C9 (OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C * ___onSuccess0, OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E * ___onCancel1, int32_t ___pickMode2, bool ___allowMultiSelection3, String_t* ___initialPath4, String_t* ___initialFilename5, String_t* ___title6, String_t* ___saveButtonText7, const RuntimeMethod* method);
// System.String System.String::ToLowerInvariant()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_ToLowerInvariant_m070E99F11A6005755BD6579A6CC835694395F79F (String_t* __this, const RuntimeMethod* method);
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70 (String_t* __this, int32_t ___index0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m10758B01687A2181C8727AD9FD9CCF5325C61C2A (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.String>::.ctor()
inline void HashSet_1__ctor_mCC4A4964EEA7915C5CABFACB64E6A9AD82700818 (HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * __this, const RuntimeMethod* method)
{
	((  void (*) (HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 *, const RuntimeMethod*))HashSet_1__ctor_m2CDA40DEC2900A9CB00F8348FF386DF44ABD0EC7_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.HashSet`1<System.String>::Add(!0)
inline bool HashSet_1_Add_m990F3F2EEC5E767A82AF639CD2307F4E7575B370 (HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * __this, String_t* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 *, String_t*, const RuntimeMethod*))HashSet_1_Add_mF670AD4C3F2685F0797E05C5491BC1841CEA9DBA_gshared)(__this, ___item0, method);
}
// System.Int32 System.String::LastIndexOf(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t String_LastIndexOf_m29D788F388576F13C5D522AD008A86859E5BA826 (String_t* __this, Il2CppChar ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.String>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void HashSet_1__ctor_mDA3E71DBB75FDDB89C8D4A28C9CBE739F1EAEEB8 (HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 *, RuntimeObject*, const RuntimeMethod*))HashSet_1__ctor_m9D936778F28043838186FC2037F47460DA7925B9_gshared)(__this, ___collection0, method);
}
// System.Boolean System.Collections.Generic.HashSet`1<System.String>::Contains(!0)
inline bool HashSet_1_Contains_m9B35CC2F57089F860E18D73F6B603F6F845010FF (HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * __this, String_t* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 *, String_t*, const RuntimeMethod*))HashSet_1_Contains_m91084DCF8BFB10517C15219307054B7B971AC8A5_gshared)(__this, ___item0, method);
}
// System.Boolean System.String::EndsWith(System.String,System.StringComparison)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_EndsWith_mB6E4F554EB12AF5BB822050E738AB867AF5C9864 (String_t* __this, String_t* ___value0, int32_t ___comparisonType1, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mC41740F000A5A48F8D313FB4CEE0E35E207A2632 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid/<>c__DisplayClass3_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0__ctor_mB3AB85D2B5C46D597CF0E576067F99885DECAF0A (U3CU3Ec__DisplayClass3_0_tE608D07C839B5A154A5B6D2877B1E03CC482E9A4 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid/<>c__DisplayClass3_0::<OnDirectoryPicked>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0_U3COnDirectoryPickedU3Eb__0_m1603B9C63F5074A0440B02DA109258F2C746A2E1 (U3CU3Ec__DisplayClass3_0_tE608D07C839B5A154A5B6D2877B1E03CC482E9A4 * __this, const RuntimeMethod* method)
{
	{
		// callbackHelper.CallOnMainThread( () => DirectoryPickedCallback( rawUri, name ) );
		FBDirectoryReceiveCallbackAndroid_t826BC254F0340D34CA548141A2130BBA31A5C154 * L_0 = __this->get_U3CU3E4__this_0();
		String_t* L_1 = __this->get_rawUri_1();
		String_t* L_2 = __this->get_name_2();
		NullCheck(L_0);
		FBDirectoryReceiveCallbackAndroid_DirectoryPickedCallback_m7039234E4669DC9103BE940172E633E59D435800(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleFileBrowser.FileBrowser/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m68751808F9A0583FD6BDFD4FB074F482E85FB494 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D * L_0 = (U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D *)il2cpp_codegen_object_new(U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m0C7FD05A63A799955E2C175B16DD6E2FA642261E(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void SimpleFileBrowser.FileBrowser/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m0C7FD05A63A799955E2C175B16DD6E2FA642261E (U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 SimpleFileBrowser.FileBrowser/<>c::<RefreshFiles>b__236_0(SimpleFileBrowser.FileSystemEntry,SimpleFileBrowser.FileSystemEntry)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3CRefreshFilesU3Eb__236_0_m4874DD1849D832573640A6C95CE59ECCE03F2C43 (U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D * __this, FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  ___entry10, FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  ___entry21, const RuntimeMethod* method)
{
	{
		// if( entry1.IsDirectory != entry2.IsDirectory )
		bool L_0;
		L_0 = FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F((FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 *)(&___entry10), /*hidden argument*/NULL);
		bool L_1;
		L_1 = FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F((FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 *)(&___entry21), /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001d;
		}
	}
	{
		// return entry1.IsDirectory ? -1 : 1;
		bool L_2;
		L_2 = FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F((FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 *)(&___entry10), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001b;
		}
	}
	{
		return 1;
	}

IL_001b:
	{
		return (-1);
	}

IL_001d:
	{
		// return entry1.Name.CompareTo( entry2.Name );
		FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  L_3 = ___entry10;
		String_t* L_4 = L_3.get_Name_1();
		FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  L_5 = ___entry21;
		String_t* L_6 = L_5.get_Name_1();
		NullCheck(L_4);
		int32_t L_7;
		L_7 = String_CompareTo_m01AF97548BE97133E2DC648B2E63BB16708354CF(L_4, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleFileBrowser.FileBrowser/<>c__DisplayClass220_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass220_0__ctor_mF9972CFE3E9D7C704E4FF4EEDB232DDC1E7F0FDF (U3CU3Ec__DisplayClass220_0_t3D20A2B3DD8845AD34093E57F5B4DE45DBB95600 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleFileBrowser.FileBrowser/<>c__DisplayClass220_0::<OnSubmitButtonClicked>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass220_0_U3COnSubmitButtonClickedU3Eb__0_m8DBA90F0845F09706FF10BEA45D6452ED641384C (U3CU3Ec__DisplayClass220_0_t3D20A2B3DD8845AD34093E57F5B4DE45DBB95600 * __this, const RuntimeMethod* method)
{
	{
		// fileOperationConfirmationPanel.Show( this, submittedFileEntriesToOverwrite, FileBrowserFileOperationConfirmationPanel.OperationType.Overwrite, () => OnOperationSuccessful( result ) );
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_0 = __this->get_U3CU3E4__this_0();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = __this->get_result_1();
		NullCheck(L_0);
		FileBrowser_OnOperationSuccessful_m5DC93791ECD6CBA227C3FA4436DB0EBE91A27A6C(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleFileBrowser.FileBrowser/<>c__DisplayClass242_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass242_0__ctor_m7C7C48E12917367748721E92BE27F9DF7B0228B2 (U3CU3Ec__DisplayClass242_0_t5A54DD142E562A9BF717F627B31C8F71C0E0FFDE * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleFileBrowser.FileBrowser/<>c__DisplayClass242_0::<RenameSelectedFile>b__0(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass242_0_U3CRenameSelectedFileU3Eb__0_m073741ADCE7FDC0ACCB58C46732E2AAB60CB890F (U3CU3Ec__DisplayClass242_0_t5A54DD142E562A9BF717F627B31C8F71C0E0FFDE * __this, String_t* ___newName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FileBrowserHelpers_t2E079075C38746CC9599F4635A704970DF3AA4E1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if( string.IsNullOrEmpty( newName ) || newName == fileInfo.Name )
		String_t* L_0 = ___newName0;
		bool L_1;
		L_1 = String_IsNullOrEmpty_m57429705D977ACD5EE7E210A858EED6F348C36B3(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_2 = ___newName0;
		FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 * L_3 = __this->get_address_of_fileInfo_0();
		String_t* L_4 = L_3->get_Name_1();
		bool L_5;
		L_5 = String_op_Equality_m50B3548E4AC232558190B0052877B290AA1D436A(L_2, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_001c;
		}
	}

IL_001b:
	{
		// return;
		return;
	}

IL_001c:
	{
		// if( fileInfo.IsDirectory )
		FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 * L_6 = __this->get_address_of_fileInfo_0();
		bool L_7;
		L_7 = FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F((FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 *)L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003d;
		}
	}
	{
		// FileBrowserHelpers.RenameDirectory( fileInfo.Path, newName );
		FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 * L_8 = __this->get_address_of_fileInfo_0();
		String_t* L_9 = L_8->get_Path_0();
		String_t* L_10 = ___newName0;
		IL2CPP_RUNTIME_CLASS_INIT(FileBrowserHelpers_t2E079075C38746CC9599F4635A704970DF3AA4E1_il2cpp_TypeInfo_var);
		String_t* L_11;
		L_11 = FileBrowserHelpers_RenameDirectory_m070EEAEDFAD3768F3900854F66EE12D126C92D4D(L_9, L_10, /*hidden argument*/NULL);
		goto IL_004f;
	}

IL_003d:
	{
		// FileBrowserHelpers.RenameFile( fileInfo.Path, newName );
		FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 * L_12 = __this->get_address_of_fileInfo_0();
		String_t* L_13 = L_12->get_Path_0();
		String_t* L_14 = ___newName0;
		IL2CPP_RUNTIME_CLASS_INIT(FileBrowserHelpers_t2E079075C38746CC9599F4635A704970DF3AA4E1_il2cpp_TypeInfo_var);
		String_t* L_15;
		L_15 = FileBrowserHelpers_RenameFile_mA204A30D5E284916B22AA30419A0412094C3AFB7(L_13, L_14, /*hidden argument*/NULL);
	}

IL_004f:
	{
		// pendingFileEntrySelection.Clear();
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_16 = __this->get_U3CU3E4__this_1();
		NullCheck(L_16);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_17 = L_16->get_pendingFileEntrySelection_86();
		NullCheck(L_17);
		List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640(L_17, /*hidden argument*/List_1_Clear_m1E4AF39A1050CD8394AA202B04F2B07267435640_RuntimeMethod_var);
		// pendingFileEntrySelection.Add( newName );
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_18 = __this->get_U3CU3E4__this_1();
		NullCheck(L_18);
		List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * L_19 = L_18->get_pendingFileEntrySelection_86();
		String_t* L_20 = ___newName0;
		NullCheck(L_19);
		List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE(L_19, L_20, /*hidden argument*/List_1_Add_m627ED3F7C50096BB8934F778CB980E79483BD2AE_RuntimeMethod_var);
		// RefreshFiles( true );
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_21 = __this->get_U3CU3E4__this_1();
		NullCheck(L_21);
		FileBrowser_RefreshFiles_m78E8F197E37E572167B58ECF89907B0F937711D3(L_21, (bool)1, /*hidden argument*/NULL);
		// if( ( fileInfo.IsDirectory && m_pickerMode != PickMode.Files ) || ( !fileInfo.IsDirectory && m_pickerMode != PickMode.Folders ) )
		FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 * L_22 = __this->get_address_of_fileInfo_0();
		bool L_23;
		L_23 = FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F((FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 *)L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0096;
		}
	}
	{
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_24 = __this->get_U3CU3E4__this_1();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_m_pickerMode_111();
		if (L_25)
		{
			goto IL_00b1;
		}
	}

IL_0096:
	{
		FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 * L_26 = __this->get_address_of_fileInfo_0();
		bool L_27;
		L_27 = FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F((FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 *)L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00c2;
		}
	}
	{
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_28 = __this->get_U3CU3E4__this_1();
		NullCheck(L_28);
		int32_t L_29 = L_28->get_m_pickerMode_111();
		if ((((int32_t)L_29) == ((int32_t)1)))
		{
			goto IL_00c2;
		}
	}

IL_00b1:
	{
		// filenameInputField.text = newName;
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_30 = __this->get_U3CU3E4__this_1();
		NullCheck(L_30);
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_31 = L_30->get_filenameInputField_65();
		String_t* L_32 = ___newName0;
		NullCheck(L_31);
		InputField_set_text_m30EE95E064474DFA2842CA2DD6E831FF3F3500BF(L_31, L_32, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		// } );
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateNewFolderCoroutineU3Ed__241__ctor_mDFD38C6B4A6455AEDEE398C93FA8C252AE88184B (U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateNewFolderCoroutineU3Ed__241_System_IDisposable_Dispose_mB74B115992FE4B68A55E768310A997AC20A20926 (U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCreateNewFolderCoroutineU3Ed__241_MoveNext_mDC6CFEFA6CBF228341CDD23A2D4EBDBCB7B82798 (U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FileBrowser_U3CCreateNewFolderCoroutineU3Eb__241_0_mE4B278D5E88BC29E2F4F88DDA2F471F57ED0376D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_00ac;
			}
			case 2:
			{
				goto IL_00e4;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->set_U3CU3E1__state_0((-1));
		// filesScrollRect.verticalNormalizedPosition = 1f;
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_3 = V_1;
		NullCheck(L_3);
		ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * L_4 = L_3->get_filesScrollRect_63();
		NullCheck(L_4);
		ScrollRect_set_verticalNormalizedPosition_m18393A7B91359B013466A984325F233535A6941D(L_4, (1.0f), /*hidden argument*/NULL);
		// filesScrollRect.velocity = Vector2.zero;
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_5 = V_1;
		NullCheck(L_5);
		ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * L_6 = L_5->get_filesScrollRect_63();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7;
		L_7 = Vector2_get_zero_mD28A0EC932762710AFA0BBB9CCC63716C3BD064D(/*hidden argument*/NULL);
		NullCheck(L_6);
		ScrollRect_set_velocity_m6CBDADD5F61163917B37C922FA6F42170D68E1D9_inline(L_6, L_7, /*hidden argument*/NULL);
		// if( selectedFileEntries.Count > 0 )
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_8 = V_1;
		NullCheck(L_8);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_9 = L_8->get_selectedFileEntries_85();
		NullCheck(L_9);
		int32_t L_10;
		L_10 = List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_inline(L_9, /*hidden argument*/List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_RuntimeMethod_var);
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0090;
		}
	}
	{
		// selectedFileEntries.Clear();
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_11 = V_1;
		NullCheck(L_11);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_12 = L_11->get_selectedFileEntries_85();
		NullCheck(L_12);
		List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A(L_12, /*hidden argument*/List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_RuntimeMethod_var);
		// MultiSelectionToggleSelectionMode = false;
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_13 = V_1;
		NullCheck(L_13);
		FileBrowser_set_MultiSelectionToggleSelectionMode_m176FE9DC46BDD2EB48C108D21831D1B08EB85934(L_13, (bool)0, /*hidden argument*/NULL);
		// filenameInputField.text = string.Empty;
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_14 = V_1;
		NullCheck(L_14);
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_15 = L_14->get_filenameInputField_65();
		String_t* L_16 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		NullCheck(L_15);
		InputField_set_text_m30EE95E064474DFA2842CA2DD6E831FF3F3500BF(L_15, L_16, /*hidden argument*/NULL);
		// filenameInputField.interactable = true;
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_17 = V_1;
		NullCheck(L_17);
		InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * L_18 = L_17->get_filenameInputField_65();
		NullCheck(L_18);
		Selectable_set_interactable_mE6F57D33A9E0484377174D0F490C4372BF7F0D40(L_18, (bool)1, /*hidden argument*/NULL);
		// listView.UpdateList();
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_19 = V_1;
		NullCheck(L_19);
		RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767 * L_20 = L_19->get_listView_64();
		NullCheck(L_20);
		RecycledListView_UpdateList_m0637428860547148FA94CA0267BBC6E06911F29F(L_20, /*hidden argument*/NULL);
	}

IL_0090:
	{
		// filesScrollRect.movementType = ScrollRect.MovementType.Unrestricted;
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_21 = V_1;
		NullCheck(L_21);
		ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * L_22 = L_21->get_filesScrollRect_63();
		NullCheck(L_22);
		ScrollRect_set_movementType_m63E3C04ECBDA708AF2341564DB987227D32075CC_inline(L_22, 0, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00ac:
	{
		__this->set_U3CU3E1__state_0((-1));
		// filesContainer.anchoredPosition = new Vector2( 0f, -m_skin.FileHeight );
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_23 = V_1;
		NullCheck(L_23);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_24 = L_23->get_filesContainer_62();
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_25 = V_1;
		NullCheck(L_25);
		UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * L_26 = L_25->get_m_skin_9();
		NullCheck(L_26);
		float L_27;
		L_27 = UISkin_get_FileHeight_m784A41AB4290882A6FB5FF8F1D9A63B235F35A49_inline(L_26, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_28;
		memset((&L_28), 0, sizeof(L_28));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_28), (0.0f), ((-L_27)), /*hidden argument*/NULL);
		NullCheck(L_24);
		RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905(L_24, L_28, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_00e4:
	{
		__this->set_U3CU3E1__state_0((-1));
		// filesContainer.anchoredPosition = new Vector2( 0f, -m_skin.FileHeight );
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_29 = V_1;
		NullCheck(L_29);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_30 = L_29->get_filesContainer_62();
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_31 = V_1;
		NullCheck(L_31);
		UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * L_32 = L_31->get_m_skin_9();
		NullCheck(L_32);
		float L_33;
		L_33 = UISkin_get_FileHeight_m784A41AB4290882A6FB5FF8F1D9A63B235F35A49_inline(L_32, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_34;
		memset((&L_34), 0, sizeof(L_34));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_34), (0.0f), ((-L_33)), /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905(L_30, L_34, /*hidden argument*/NULL);
		// ( (RectTransform) renameItem.transform ).anchoredPosition = new Vector2( 1f, m_skin.FileHeight );
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_35 = V_1;
		NullCheck(L_35);
		FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4 * L_36 = L_35->get_renameItem_75();
		NullCheck(L_36);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_37;
		L_37 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_36, /*hidden argument*/NULL);
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_38 = V_1;
		NullCheck(L_38);
		UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * L_39 = L_38->get_m_skin_9();
		NullCheck(L_39);
		float L_40;
		L_40 = UISkin_get_FileHeight_m784A41AB4290882A6FB5FF8F1D9A63B235F35A49_inline(L_39, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_41;
		memset((&L_41), 0, sizeof(L_41));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_41), (1.0f), L_40, /*hidden argument*/NULL);
		NullCheck(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_37, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)));
		RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_37, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)), L_41, /*hidden argument*/NULL);
		// renameItem.Show( string.Empty, m_skin.FileSelectedBackgroundColor, m_skin.FolderIcon, ( folderName ) =>
		// {
		//     filesScrollRect.movementType = ScrollRect.MovementType.Clamped;
		//     filesContainer.anchoredPosition = Vector2.zero;
		// 
		//     if( string.IsNullOrEmpty( folderName ) )
		//         return;
		// 
		//     FileBrowserHelpers.CreateFolderInDirectory( CurrentPath, folderName );
		// 
		//     pendingFileEntrySelection.Clear();
		//     pendingFileEntrySelection.Add( folderName );
		// 
		//     RefreshFiles( true );
		// 
		//     if( m_pickerMode != PickMode.Files )
		//         filenameInputField.text = folderName;
		// 
		//     // Focus on the newly created folder
		//     int fileEntryIndex = Mathf.Max( 0, FilenameToFileEntryIndex( folderName ) );
		//     filesScrollRect.verticalNormalizedPosition = validFileEntries.Count > 1 ? ( 1f - (float) fileEntryIndex / ( validFileEntries.Count - 1 ) ) : 1f;
		// } );
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_42 = V_1;
		NullCheck(L_42);
		FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4 * L_43 = L_42->get_renameItem_75();
		String_t* L_44 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_45 = V_1;
		NullCheck(L_45);
		UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * L_46 = L_45->get_m_skin_9();
		NullCheck(L_46);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_47;
		L_47 = UISkin_get_FileSelectedBackgroundColor_mECCF9DED2D0EA5CD1F72560F2DB1086043F87DCD_inline(L_46, /*hidden argument*/NULL);
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_48 = V_1;
		NullCheck(L_48);
		UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * L_49 = L_48->get_m_skin_9();
		NullCheck(L_49);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_50;
		L_50 = UISkin_get_FolderIcon_m10703DA30265BC01B71DCE95B3899ACC1BFA3979_inline(L_49, /*hidden argument*/NULL);
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_51 = V_1;
		OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * L_52 = (OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 *)il2cpp_codegen_object_new(OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74_il2cpp_TypeInfo_var);
		OnRenameCompleted__ctor_m019658E790544491DDBD4F208F8B422DC77764EB(L_52, L_51, (intptr_t)((intptr_t)FileBrowser_U3CCreateNewFolderCoroutineU3Eb__241_0_mE4B278D5E88BC29E2F4F88DDA2F471F57ED0376D_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_43);
		FileBrowserRenamedItem_Show_m67CF833D01BEDC56743DF4EAC46AF60E8FFFB5DB(L_43, L_44, L_47, L_50, L_52, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0545F6447CE56AA4E999A8B6450E7F30A310430D (U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_IEnumerator_Reset_m5F77E4A7CA436A621AC00B2073FE17DB3EFF2850 (U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_IEnumerator_Reset_m5F77E4A7CA436A621AC00B2073FE17DB3EFF2850_RuntimeMethod_var)));
	}
}
// System.Object SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_IEnumerator_get_Current_m8E8B43EAB0828F57B8691DBEADCF694AA88101DC (U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitForLoadDialogU3Ed__266__ctor_m3F8B1CF0E9CA9644D9E02111A683BD10962D1F5D (U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitForLoadDialogU3Ed__266_System_IDisposable_Dispose_mEF304AF3D0BAE96043ECA7DAD60C537FBB38A35C (U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CWaitForLoadDialogU3Ed__266_MoveNext_m1603ACA4294B686207F02C7542E1D0A6DB24E557 (U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0056;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if( !ShowLoadDialog( null, null, pickMode, allowMultiSelection, initialPath, initialFilename, title, loadButtonText ) )
		int32_t L_3 = __this->get_pickMode_2();
		bool L_4 = __this->get_allowMultiSelection_3();
		String_t* L_5 = __this->get_initialPath_4();
		String_t* L_6 = __this->get_initialFilename_5();
		String_t* L_7 = __this->get_title_6();
		String_t* L_8 = __this->get_loadButtonText_7();
		IL2CPP_RUNTIME_CLASS_INIT(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = FileBrowser_ShowLoadDialog_m73918E026EA31133F15BB56A903D4C1191FED21F((OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C *)NULL, (OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E *)NULL, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_005d;
		}
	}
	{
		// yield break;
		return (bool)0;
	}

IL_0046:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0056:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_005d:
	{
		// while( Instance.gameObject.activeSelf )
		IL2CPP_RUNTIME_CLASS_INIT(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_il2cpp_TypeInfo_var);
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_10;
		L_10 = FileBrowser_get_Instance_m2D1CA7BB2DEE29A481F02D5DDC6B15727AE6457F(/*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11;
		L_11 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		bool L_12;
		L_12 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0046;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitForLoadDialogU3Ed__266_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE96087E920FB48FA18B5996C934D369C33B9B75 (U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitForLoadDialogU3Ed__266_System_Collections_IEnumerator_Reset_m783AE76EC116144B63DC8BE626E95732D3C0BF27 (U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CWaitForLoadDialogU3Ed__266_System_Collections_IEnumerator_Reset_m783AE76EC116144B63DC8BE626E95732D3C0BF27_RuntimeMethod_var)));
	}
}
// System.Object SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitForLoadDialogU3Ed__266_System_Collections_IEnumerator_get_Current_mCF552CCACF7B0B0ED71A0EE750D029528C487043 (U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitForSaveDialogU3Ed__265__ctor_m12F637ABD0A5EBB97C466D0F553D5B8D56033BD1 (U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitForSaveDialogU3Ed__265_System_IDisposable_Dispose_m7BBF2D25C1C1AC74A18CF70C32FF1FDEC15F9459 (U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CWaitForSaveDialogU3Ed__265_MoveNext_mFABB79CE55C954CD101AAC64895FED46AA318176 (U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0056;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if( !ShowSaveDialog( null, null, pickMode, allowMultiSelection, initialPath, initialFilename, title, saveButtonText ) )
		int32_t L_3 = __this->get_pickMode_2();
		bool L_4 = __this->get_allowMultiSelection_3();
		String_t* L_5 = __this->get_initialPath_4();
		String_t* L_6 = __this->get_initialFilename_5();
		String_t* L_7 = __this->get_title_6();
		String_t* L_8 = __this->get_saveButtonText_7();
		IL2CPP_RUNTIME_CLASS_INIT(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = FileBrowser_ShowSaveDialog_mC27C4233D80B769B4DD41FF34476296B811225C9((OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C *)NULL, (OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E *)NULL, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_005d;
		}
	}
	{
		// yield break;
		return (bool)0;
	}

IL_0046:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0056:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_005d:
	{
		// while( Instance.gameObject.activeSelf )
		IL2CPP_RUNTIME_CLASS_INIT(FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_il2cpp_TypeInfo_var);
		FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8 * L_10;
		L_10 = FileBrowser_get_Instance_m2D1CA7BB2DEE29A481F02D5DDC6B15727AE6457F(/*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11;
		L_11 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		bool L_12;
		L_12 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0046;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitForSaveDialogU3Ed__265_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9186D456077EC88F0B778027AD680C149E3D74CE (U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CWaitForSaveDialogU3Ed__265_System_Collections_IEnumerator_Reset_m38718E5D5CE7AA13F1F4B684775CED69286E1390 (U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CWaitForSaveDialogU3Ed__265_System_Collections_IEnumerator_Reset_m38718E5D5CE7AA13F1F4B684775CED69286E1390_RuntimeMethod_var)));
	}
}
// System.Object SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitForSaveDialogU3Ed__265_System_Collections_IEnumerator_get_Current_mE3601AB26ED57A39433E48BB8716C87BD6F45B0F (U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_AndroidSAFDirectoryPickCallback_tCB2537E0F2C9160F1890DE73E86301251C6F5504 (AndroidSAFDirectoryPickCallback_tCB2537E0F2C9160F1890DE73E86301251C6F5504 * __this, String_t* ___rawUri0, String_t* ___name1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((RuntimeDelegate*)__this)->method->nativeFunction);

	// Marshaling of parameter '___rawUri0' to native representation
	char* ____rawUri0_marshaled = NULL;
	____rawUri0_marshaled = il2cpp_codegen_marshal_string(___rawUri0);

	// Marshaling of parameter '___name1' to native representation
	char* ____name1_marshaled = NULL;
	____name1_marshaled = il2cpp_codegen_marshal_string(___name1);

	// Native function invocation
	il2cppPInvokeFunc(____rawUri0_marshaled, ____name1_marshaled);

	// Marshaling cleanup of parameter '___rawUri0' native representation
	il2cpp_codegen_marshal_free(____rawUri0_marshaled);
	____rawUri0_marshaled = NULL;

	// Marshaling cleanup of parameter '___name1' native representation
	il2cpp_codegen_marshal_free(____name1_marshaled);
	____name1_marshaled = NULL;

}
// System.Void SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidSAFDirectoryPickCallback__ctor_mA74461FFAA226B63E9E44A9C221072ED68F45DE8 (AndroidSAFDirectoryPickCallback_tCB2537E0F2C9160F1890DE73E86301251C6F5504 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	if (___object0 == NULL && !MethodIsStatic((RuntimeMethod*)___method1))
	{
		il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
	}
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback::Invoke(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidSAFDirectoryPickCallback_Invoke_m57B8746EC1134F2FD4F3782D7F3468E1F7E33076 (AndroidSAFDirectoryPickCallback_tCB2537E0F2C9160F1890DE73E86301251C6F5504 * __this, String_t* ___rawUri0, String_t* ___name1, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___rawUri0, ___name1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, String_t*, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___rawUri0, ___name1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< String_t* >::Invoke(targetMethod, ___rawUri0, ___name1);
					else
						GenericVirtActionInvoker1< String_t* >::Invoke(targetMethod, ___rawUri0, ___name1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___rawUri0, ___name1);
					else
						VirtActionInvoker1< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___rawUri0, ___name1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___rawUri0, ___name1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< String_t*, String_t* >::Invoke(targetMethod, targetThis, ___rawUri0, ___name1);
					else
						GenericVirtActionInvoker2< String_t*, String_t* >::Invoke(targetMethod, targetThis, ___rawUri0, ___name1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< String_t*, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___rawUri0, ___name1);
					else
						VirtActionInvoker2< String_t*, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___rawUri0, ___name1);
				}
			}
			else
			{
				if (___parameterCount == 1)
				{
					typedef void (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___rawUri0, ___name1, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, String_t*, String_t*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___rawUri0, ___name1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AndroidSAFDirectoryPickCallback_BeginInvoke_mD5991AD25935F5FD2E404BF6245BBE018D425749 (AndroidSAFDirectoryPickCallback_tCB2537E0F2C9160F1890DE73E86301251C6F5504 * __this, String_t* ___rawUri0, String_t* ___name1, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___rawUri0;
	__d_args[1] = ___name1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);;
}
// System.Void SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidSAFDirectoryPickCallback_EndInvoke_mA9F40E882DDF96E33E27DD315C8526A6B508A69C (AndroidSAFDirectoryPickCallback_tCB2537E0F2C9160F1890DE73E86301251C6F5504 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  bool DelegatePInvokeWrapper_FileSystemEntryFilter_t570DC98294020ED04118FDDBAA83AF79C201E4E1 (FileSystemEntryFilter_t570DC98294020ED04118FDDBAA83AF79C201E4E1 * __this, FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  ___entry0, const RuntimeMethod* method)
{


	typedef int32_t (DEFAULT_CALL *PInvokeFunc)(FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((RuntimeDelegate*)__this)->method->nativeFunction);

	// Marshaling of parameter '___entry0' to native representation
	FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshaled_pinvoke ____entry0_marshaled = {};
	FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshal_pinvoke(___entry0, ____entry0_marshaled);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____entry0_marshaled);

	// Marshaling cleanup of parameter '___entry0' native representation
	FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_marshal_pinvoke_cleanup(____entry0_marshaled);

	return static_cast<bool>(returnValue);
}
// System.Void SimpleFileBrowser.FileBrowser/FileSystemEntryFilter::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FileSystemEntryFilter__ctor_mF7DA3A52405E454E79B105125606D6E8BAFF8F74 (FileSystemEntryFilter_t570DC98294020ED04118FDDBAA83AF79C201E4E1 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	if (___object0 == NULL && !MethodIsStatic((RuntimeMethod*)___method1))
	{
		il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
	}
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean SimpleFileBrowser.FileBrowser/FileSystemEntryFilter::Invoke(SimpleFileBrowser.FileSystemEntry)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FileSystemEntryFilter_Invoke_mF9EE2CB6AD810BF7873EF479A6F57CE9A81969DF (FileSystemEntryFilter_t570DC98294020ED04118FDDBAA83AF79C201E4E1 * __this, FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  ___entry0, const RuntimeMethod* method)
{
	bool result = false;
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef bool (*FunctionPointerType) (FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 , const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___entry0, targetMethod);
			}
			else
			{
				// closed
				typedef bool (*FunctionPointerType) (void*, FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 , const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___entry0, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker1< bool, FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  >::Invoke(targetMethod, targetThis, ___entry0);
					else
						result = GenericVirtFuncInvoker1< bool, FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  >::Invoke(targetMethod, targetThis, ___entry0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker1< bool, FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___entry0);
					else
						result = VirtFuncInvoker1< bool, FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___entry0);
				}
			}
			else
			{
				if (___parameterCount == 0)
				{
					typedef bool (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((RuntimeObject*)(reinterpret_cast<RuntimeObject*>(&___entry0) - 1), targetMethod);
				}
				else
				{
					typedef bool (*FunctionPointerType) (void*, FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9 , const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___entry0, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult SimpleFileBrowser.FileBrowser/FileSystemEntryFilter::BeginInvoke(SimpleFileBrowser.FileSystemEntry,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FileSystemEntryFilter_BeginInvoke_mA07C1032C03172D6F020207D97E77631FA1D2055 (FileSystemEntryFilter_t570DC98294020ED04118FDDBAA83AF79C201E4E1 * __this, FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9  ___entry0, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(FileSystemEntry_t39568A64926C3510590419EA1941D00EC864D8F9_il2cpp_TypeInfo_var, &___entry0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);;
}
// System.Boolean SimpleFileBrowser.FileBrowser/FileSystemEntryFilter::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FileSystemEntryFilter_EndInvoke_m18A618059F7D663A424111D2372986129FA0220E (FileSystemEntryFilter_t570DC98294020ED04118FDDBAA83AF79C201E4E1 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: SimpleFileBrowser.FileBrowser/FiletypeIcon
IL2CPP_EXTERN_C void FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshal_pinvoke(const FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A& unmarshaled, FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshaled_pinvoke& marshaled)
{
	Exception_t* ___icon_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'icon' of type 'FiletypeIcon': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___icon_1Exception, NULL);
}
IL2CPP_EXTERN_C void FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshal_pinvoke_back(const FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshaled_pinvoke& marshaled, FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A& unmarshaled)
{
	Exception_t* ___icon_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'icon' of type 'FiletypeIcon': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___icon_1Exception, NULL);
}
// Conversion method for clean up from marshalling of: SimpleFileBrowser.FileBrowser/FiletypeIcon
IL2CPP_EXTERN_C void FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshal_pinvoke_cleanup(FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: SimpleFileBrowser.FileBrowser/FiletypeIcon
IL2CPP_EXTERN_C void FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshal_com(const FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A& unmarshaled, FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshaled_com& marshaled)
{
	Exception_t* ___icon_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'icon' of type 'FiletypeIcon': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___icon_1Exception, NULL);
}
IL2CPP_EXTERN_C void FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshal_com_back(const FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshaled_com& marshaled, FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A& unmarshaled)
{
	Exception_t* ___icon_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'icon' of type 'FiletypeIcon': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___icon_1Exception, NULL);
}
// Conversion method for clean up from marshalling of: SimpleFileBrowser.FileBrowser/FiletypeIcon
IL2CPP_EXTERN_C void FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshal_com_cleanup(FiletypeIcon_tAC71772CF30DA24520D1AE7A63BBE05E9888928A_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleFileBrowser.FileBrowser/Filter::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Filter__ctor_m1074FC06CD79E457F25F52026D634FEBF9ACCF10 (Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		// internal Filter( string name )
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// this.name = name;
		String_t* L_0 = ___name0;
		__this->set_name_0(L_0);
		// extensions = null;
		__this->set_extensions_1((StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)NULL);
		// extensionsSet = null;
		__this->set_extensionsSet_2((HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 *)NULL);
		// defaultExtension = null;
		__this->set_defaultExtension_3((String_t*)NULL);
		// allExtensionsHaveSingleSuffix = true;
		__this->set_allExtensionsHaveSingleSuffix_4((bool)1);
		// }
		return;
	}
}
// System.Void SimpleFileBrowser.FileBrowser/Filter::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Filter__ctor_m38D854E9B7172B814710C305CA0857452A2DE121 (Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8 * __this, String_t* ___name0, String_t* ___extension1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1_Add_m990F3F2EEC5E767A82AF639CD2307F4E7575B370_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1__ctor_mCC4A4964EEA7915C5CABFACB64E6A9AD82700818_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF3E84B722399601AD7E281754E917478AA9AD48D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public Filter( string name, string extension )
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// this.name = name;
		String_t* L_0 = ___name0;
		__this->set_name_0(L_0);
		// extension = extension.ToLowerInvariant();
		String_t* L_1 = ___extension1;
		NullCheck(L_1);
		String_t* L_2;
		L_2 = String_ToLowerInvariant_m070E99F11A6005755BD6579A6CC835694395F79F(L_1, /*hidden argument*/NULL);
		___extension1 = L_2;
		// if( extension[0] != '.' )
		String_t* L_3 = ___extension1;
		NullCheck(L_3);
		Il2CppChar L_4;
		L_4 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_3, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)46))))
		{
			goto IL_002d;
		}
	}
	{
		// extension = "." + extension;
		String_t* L_5 = ___extension1;
		String_t* L_6;
		L_6 = String_Concat_m10758B01687A2181C8727AD9FD9CCF5325C61C2A(_stringLiteralF3E84B722399601AD7E281754E917478AA9AD48D, L_5, /*hidden argument*/NULL);
		___extension1 = L_6;
	}

IL_002d:
	{
		// extensions = new string[1] { extension };
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_7 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = L_7;
		String_t* L_9 = ___extension1;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_9);
		__this->set_extensions_1(L_8);
		// extensionsSet = new HashSet<string>() { extension };
		HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * L_10 = (HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 *)il2cpp_codegen_object_new(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229_il2cpp_TypeInfo_var);
		HashSet_1__ctor_mCC4A4964EEA7915C5CABFACB64E6A9AD82700818(L_10, /*hidden argument*/HashSet_1__ctor_mCC4A4964EEA7915C5CABFACB64E6A9AD82700818_RuntimeMethod_var);
		HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * L_11 = L_10;
		String_t* L_12 = ___extension1;
		NullCheck(L_11);
		bool L_13;
		L_13 = HashSet_1_Add_m990F3F2EEC5E767A82AF639CD2307F4E7575B370(L_11, L_12, /*hidden argument*/HashSet_1_Add_m990F3F2EEC5E767A82AF639CD2307F4E7575B370_RuntimeMethod_var);
		__this->set_extensionsSet_2(L_11);
		// defaultExtension = extension;
		String_t* L_14 = ___extension1;
		__this->set_defaultExtension_3(L_14);
		// allExtensionsHaveSingleSuffix = ( extension.LastIndexOf( '.' ) == 0 );
		String_t* L_15 = ___extension1;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = String_LastIndexOf_m29D788F388576F13C5D522AD008A86859E5BA826(L_15, ((int32_t)46), /*hidden argument*/NULL);
		__this->set_allExtensionsHaveSingleSuffix_4((bool)((((int32_t)L_16) == ((int32_t)0))? 1 : 0));
		// }
		return;
	}
}
// System.Void SimpleFileBrowser.FileBrowser/Filter::.ctor(System.String,System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Filter__ctor_m125246E9D958C713FF3D86B5E02C9D6540B58185 (Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8 * __this, String_t* ___name0, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___extensions1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1__ctor_mDA3E71DBB75FDDB89C8D4A28C9CBE739F1EAEEB8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF3E84B722399601AD7E281754E917478AA9AD48D);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// public Filter( string name, params string[] extensions )
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// this.name = name;
		String_t* L_0 = ___name0;
		__this->set_name_0(L_0);
		// allExtensionsHaveSingleSuffix = true;
		__this->set_allExtensionsHaveSingleSuffix_4((bool)1);
		// for( int i = 0; i < extensions.Length; i++ )
		V_0 = 0;
		goto IL_005e;
	}

IL_0018:
	{
		// extensions[i] = extensions[i].ToLowerInvariant();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = ___extensions1;
		int32_t L_2 = V_0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_3 = ___extensions1;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		String_t* L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		String_t* L_7;
		L_7 = String_ToLowerInvariant_m070E99F11A6005755BD6579A6CC835694395F79F(L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_7);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (String_t*)L_7);
		// if( extensions[i][0] != '.' )
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_8 = ___extensions1;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		String_t* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Il2CppChar L_12;
		L_12 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_11, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_12) == ((int32_t)((int32_t)46))))
		{
			goto IL_0040;
		}
	}
	{
		// extensions[i] = "." + extensions[i];
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = ___extensions1;
		int32_t L_14 = V_0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = ___extensions1;
		int32_t L_16 = V_0;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		String_t* L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		String_t* L_19;
		L_19 = String_Concat_m10758B01687A2181C8727AD9FD9CCF5325C61C2A(_stringLiteralF3E84B722399601AD7E281754E917478AA9AD48D, L_18, /*hidden argument*/NULL);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_19);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (String_t*)L_19);
	}

IL_0040:
	{
		// allExtensionsHaveSingleSuffix &= ( extensions[i].LastIndexOf( '.' ) == 0 );
		bool L_20 = __this->get_allExtensionsHaveSingleSuffix_4();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_21 = ___extensions1;
		int32_t L_22 = V_0;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		String_t* L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_24);
		int32_t L_25;
		L_25 = String_LastIndexOf_m29D788F388576F13C5D522AD008A86859E5BA826(L_24, ((int32_t)46), /*hidden argument*/NULL);
		__this->set_allExtensionsHaveSingleSuffix_4((bool)((int32_t)((int32_t)L_20&(int32_t)((((int32_t)L_25) == ((int32_t)0))? 1 : 0))));
		// for( int i = 0; i < extensions.Length; i++ )
		int32_t L_26 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1));
	}

IL_005e:
	{
		// for( int i = 0; i < extensions.Length; i++ )
		int32_t L_27 = V_0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_28 = ___extensions1;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_28)->max_length))))))
		{
			goto IL_0018;
		}
	}
	{
		// this.extensions = extensions;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_29 = ___extensions1;
		__this->set_extensions_1(L_29);
		// extensionsSet = new HashSet<string>( extensions );
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_30 = ___extensions1;
		HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * L_31 = (HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 *)il2cpp_codegen_object_new(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229_il2cpp_TypeInfo_var);
		HashSet_1__ctor_mDA3E71DBB75FDDB89C8D4A28C9CBE739F1EAEEB8(L_31, (RuntimeObject*)(RuntimeObject*)L_30, /*hidden argument*/HashSet_1__ctor_mDA3E71DBB75FDDB89C8D4A28C9CBE739F1EAEEB8_RuntimeMethod_var);
		__this->set_extensionsSet_2(L_31);
		// defaultExtension = extensions[0];
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_32 = ___extensions1;
		NullCheck(L_32);
		int32_t L_33 = 0;
		String_t* L_34 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		__this->set_defaultExtension_3(L_34);
		// }
		return;
	}
}
// System.Boolean SimpleFileBrowser.FileBrowser/Filter::MatchesExtension(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Filter_MatchesExtension_mB6BA2D275B2A7B7A2B7863D23591832CE7D0B7DF (Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8 * __this, String_t* ___extension0, bool ___extensionMayHaveMultipleSuffixes1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1_Add_m990F3F2EEC5E767A82AF639CD2307F4E7575B370_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1_Contains_m9B35CC2F57089F860E18D73F6B603F6F845010FF_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if( extensionsSet == null || extensionsSet.Contains( extension ) )
		HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * L_0 = __this->get_extensionsSet_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * L_1 = __this->get_extensionsSet_2();
		String_t* L_2 = ___extension0;
		NullCheck(L_1);
		bool L_3;
		L_3 = HashSet_1_Contains_m9B35CC2F57089F860E18D73F6B603F6F845010FF(L_1, L_2, /*hidden argument*/HashSet_1_Contains_m9B35CC2F57089F860E18D73F6B603F6F845010FF_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0018;
		}
	}

IL_0016:
	{
		// return true;
		return (bool)1;
	}

IL_0018:
	{
		// if( extensionMayHaveMultipleSuffixes )
		bool L_4 = ___extensionMayHaveMultipleSuffixes1;
		if (!L_4)
		{
			goto IL_004e;
		}
	}
	{
		// for( int i = 0; i < extensions.Length; i++ )
		V_0 = 0;
		goto IL_0043;
	}

IL_001f:
	{
		// if( extension.EndsWith( extensions[i], StringComparison.Ordinal ) )
		String_t* L_5 = ___extension0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = __this->get_extensions_1();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		String_t* L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_5);
		bool L_10;
		L_10 = String_EndsWith_mB6E4F554EB12AF5BB822050E738AB867AF5C9864(L_5, L_9, 4, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_003f;
		}
	}
	{
		// extensionsSet.Add( extension );
		HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * L_11 = __this->get_extensionsSet_2();
		String_t* L_12 = ___extension0;
		NullCheck(L_11);
		bool L_13;
		L_13 = HashSet_1_Add_m990F3F2EEC5E767A82AF639CD2307F4E7575B370(L_11, L_12, /*hidden argument*/HashSet_1_Add_m990F3F2EEC5E767A82AF639CD2307F4E7575B370_RuntimeMethod_var);
		// return true;
		return (bool)1;
	}

IL_003f:
	{
		// for( int i = 0; i < extensions.Length; i++ )
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0043:
	{
		// for( int i = 0; i < extensions.Length; i++ )
		int32_t L_15 = V_0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_16 = __this->get_extensions_1();
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_16)->max_length))))))
		{
			goto IL_001f;
		}
	}

IL_004e:
	{
		// return false;
		return (bool)0;
	}
}
// System.String SimpleFileBrowser.FileBrowser/Filter::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Filter_ToString_m9DCF19E38E806A743CA49A330EBEA08814B62753 (Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral758733BDBED83CBFF4F635AC26CA92AAE477F75D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB3F14BF976EFD974E34846B742502C802FABAE9D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD99605E29810F93D7DAE4EFBB764C41AF4E80D32);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// string result = string.Empty;
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		V_0 = L_0;
		// if( name != null )
		String_t* L_1 = __this->get_name_0();
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		// result += name;
		String_t* L_2 = V_0;
		String_t* L_3 = __this->get_name_0();
		String_t* L_4;
		L_4 = String_Concat_m10758B01687A2181C8727AD9FD9CCF5325C61C2A(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_001b:
	{
		// if( extensions != null )
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = __this->get_extensions_1();
		if (!L_5)
		{
			goto IL_0087;
		}
	}
	{
		// if( name != null )
		String_t* L_6 = __this->get_name_0();
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		// result += " (";
		String_t* L_7 = V_0;
		String_t* L_8;
		L_8 = String_Concat_m10758B01687A2181C8727AD9FD9CCF5325C61C2A(L_7, _stringLiteralD99605E29810F93D7DAE4EFBB764C41AF4E80D32, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0037:
	{
		// for( int i = 0; i < extensions.Length; i++ )
		V_1 = 0;
		goto IL_0068;
	}

IL_003b:
	{
		// if( i > 0 )
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		// result += ", " + extensions[i];
		String_t* L_10 = V_0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_11 = __this->get_extensions_1();
		int32_t L_12 = V_1;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		String_t* L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		String_t* L_15;
		L_15 = String_Concat_mC41740F000A5A48F8D313FB4CEE0E35E207A2632(L_10, _stringLiteral758733BDBED83CBFF4F635AC26CA92AAE477F75D, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		goto IL_0064;
	}

IL_0055:
	{
		// result += extensions[i];
		String_t* L_16 = V_0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = __this->get_extensions_1();
		int32_t L_18 = V_1;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		String_t* L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		String_t* L_21;
		L_21 = String_Concat_m10758B01687A2181C8727AD9FD9CCF5325C61C2A(L_16, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
	}

IL_0064:
	{
		// for( int i = 0; i < extensions.Length; i++ )
		int32_t L_22 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0068:
	{
		// for( int i = 0; i < extensions.Length; i++ )
		int32_t L_23 = V_1;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_24 = __this->get_extensions_1();
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length))))))
		{
			goto IL_003b;
		}
	}
	{
		// if( name != null )
		String_t* L_25 = __this->get_name_0();
		if (!L_25)
		{
			goto IL_0087;
		}
	}
	{
		// result += ")";
		String_t* L_26 = V_0;
		String_t* L_27;
		L_27 = String_Concat_m10758B01687A2181C8727AD9FD9CCF5325C61C2A(L_26, _stringLiteralB3F14BF976EFD974E34846B742502C802FABAE9D, /*hidden argument*/NULL);
		V_0 = L_27;
	}

IL_0087:
	{
		// return result;
		String_t* L_28 = V_0;
		return L_28;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E (OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E * __this, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((RuntimeDelegate*)__this)->method->nativeFunction);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void SimpleFileBrowser.FileBrowser/OnCancel::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnCancel__ctor_m1B12099B6EC65CE377D0A854F92BC24145CCF42C (OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	if (___object0 == NULL && !MethodIsStatic((RuntimeMethod*)___method1))
	{
		il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
	}
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void SimpleFileBrowser.FileBrowser/OnCancel::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnCancel_Invoke_m91F1A69EF5776DCA7D9A5C5EC9A03C925CDEBD49 (OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E * __this, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 0)
			{
				// open
				typedef void (*FunctionPointerType) (const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
	}
}
// System.IAsyncResult SimpleFileBrowser.FileBrowser/OnCancel::BeginInvoke(System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* OnCancel_BeginInvoke_m21D3022399AFFCF3CFC9926B0597AE814B2E380C (OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E * __this, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);;
}
// System.Void SimpleFileBrowser.FileBrowser/OnCancel::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnCancel_EndInvoke_m11BD247D9B4669B70D8D3116C309BB8EE08023F4 (OnCancel_tA07B3554E3DD6379324287013EB8D4493E3F024E * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C (OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___paths0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char**);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((RuntimeDelegate*)__this)->method->nativeFunction);

	// Marshaling of parameter '___paths0' to native representation
	char** ____paths0_marshaled = NULL;
	if (___paths0 != NULL)
	{
		il2cpp_array_size_t ____paths0_Length = (___paths0)->max_length;
		____paths0_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paths0_Length + 1);
		(____paths0_marshaled)[____paths0_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paths0_Length); i++)
		{
			(____paths0_marshaled)[i] = il2cpp_codegen_marshal_string((___paths0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paths0_marshaled = NULL;
	}

	// Native function invocation
	il2cppPInvokeFunc(____paths0_marshaled);

	// Marshaling cleanup of parameter '___paths0' native representation
	if (____paths0_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paths0_marshaled_CleanupLoopCount = (___paths0 != NULL) ? (___paths0)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paths0_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paths0_marshaled)[i]);
			(____paths0_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paths0_marshaled);
		____paths0_marshaled = NULL;
	}

}
// System.Void SimpleFileBrowser.FileBrowser/OnSuccess::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnSuccess__ctor_m71AD74DD97BD2E317E6CCD78F0B631AC4D3104D7 (OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	if (___object0 == NULL && !MethodIsStatic((RuntimeMethod*)___method1))
	{
		il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
	}
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void SimpleFileBrowser.FileBrowser/OnSuccess::Invoke(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnSuccess_Invoke_m7AC28204C5A1715F07274D5F73B66E39CD7D1317 (OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___paths0, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___paths0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___paths0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___paths0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___paths0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___paths0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___paths0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___paths0, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* >::Invoke(targetMethod, targetThis, ___paths0);
					else
						GenericVirtActionInvoker1< StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* >::Invoke(targetMethod, targetThis, ___paths0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___paths0);
					else
						VirtActionInvoker1< StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___paths0);
				}
			}
			else
			{
				if (___parameterCount == 0)
				{
					typedef void (*FunctionPointerType) (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___paths0, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___paths0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult SimpleFileBrowser.FileBrowser/OnSuccess::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* OnSuccess_BeginInvoke_m99BB5894F15FCDD0F04BD6321CB1E7F19EF28FFE (OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___paths0, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___paths0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);;
}
// System.Void SimpleFileBrowser.FileBrowser/OnSuccess::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnSuccess_EndInvoke_mB36C87C496674F8F56D681D6E25384CAB0F18CBC (OnSuccess_t7A0AC630FF98B02A03403D58144CA7F0BDAD357C * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: SimpleFileBrowser.FileBrowser/QuickLink
IL2CPP_EXTERN_C void QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshal_pinvoke(const QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7& unmarshaled, QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshaled_pinvoke& marshaled)
{
	Exception_t* ___icon_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'icon' of type 'QuickLink': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___icon_2Exception, NULL);
}
IL2CPP_EXTERN_C void QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshal_pinvoke_back(const QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshaled_pinvoke& marshaled, QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7& unmarshaled)
{
	Exception_t* ___icon_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'icon' of type 'QuickLink': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___icon_2Exception, NULL);
}
// Conversion method for clean up from marshalling of: SimpleFileBrowser.FileBrowser/QuickLink
IL2CPP_EXTERN_C void QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshal_pinvoke_cleanup(QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: SimpleFileBrowser.FileBrowser/QuickLink
IL2CPP_EXTERN_C void QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshal_com(const QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7& unmarshaled, QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshaled_com& marshaled)
{
	Exception_t* ___icon_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'icon' of type 'QuickLink': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___icon_2Exception, NULL);
}
IL2CPP_EXTERN_C void QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshal_com_back(const QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshaled_com& marshaled, QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7& unmarshaled)
{
	Exception_t* ___icon_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'icon' of type 'QuickLink': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___icon_2Exception, NULL);
}
// Conversion method for clean up from marshalling of: SimpleFileBrowser.FileBrowser/QuickLink
IL2CPP_EXTERN_C void QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshal_com_cleanup(QuickLink_tD5E1FAD51B844AD9B4010E2340217364760ECEB7_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_OnOperationConfirmed_tA108D4E3303410B9B615F6D94A787ABC68C137EB (OnOperationConfirmed_tA108D4E3303410B9B615F6D94A787ABC68C137EB * __this, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((RuntimeDelegate*)__this)->method->nativeFunction);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnOperationConfirmed__ctor_m6D50A4AEC640684E8F1017EC7E4A709A27DA37CF (OnOperationConfirmed_tA108D4E3303410B9B615F6D94A787ABC68C137EB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	if (___object0 == NULL && !MethodIsStatic((RuntimeMethod*)___method1))
	{
		il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
	}
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnOperationConfirmed_Invoke_mBBE7933011170BC9A4DDBB037931A3E10469FBBE (OnOperationConfirmed_tA108D4E3303410B9B615F6D94A787ABC68C137EB * __this, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 0)
			{
				// open
				typedef void (*FunctionPointerType) (const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
	}
}
// System.IAsyncResult SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed::BeginInvoke(System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* OnOperationConfirmed_BeginInvoke_m3489C6887612AA70AF313950FCE0D4FC339647E7 (OnOperationConfirmed_tA108D4E3303410B9B615F6D94A787ABC68C137EB * __this, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);;
}
// System.Void SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnOperationConfirmed_EndInvoke_m1E48D6AA660F27A97CFE5791EB604AC864A1082B (OnOperationConfirmed_tA108D4E3303410B9B615F6D94A787ABC68C137EB * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 (OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * __this, String_t* ___filename0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((RuntimeDelegate*)__this)->method->nativeFunction);

	// Marshaling of parameter '___filename0' to native representation
	char* ____filename0_marshaled = NULL;
	____filename0_marshaled = il2cpp_codegen_marshal_string(___filename0);

	// Native function invocation
	il2cppPInvokeFunc(____filename0_marshaled);

	// Marshaling cleanup of parameter '___filename0' native representation
	il2cpp_codegen_marshal_free(____filename0_marshaled);
	____filename0_marshaled = NULL;

}
// System.Void SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnRenameCompleted__ctor_m019658E790544491DDBD4F208F8B422DC77764EB (OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	if (___object0 == NULL && !MethodIsStatic((RuntimeMethod*)___method1))
	{
		il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
	}
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::Invoke(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnRenameCompleted_Invoke_m8A1B85475DE7D5362CB711D63543CDD6A68A7CDB (OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * __this, String_t* ___filename0, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___filename0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___filename0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___filename0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___filename0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___filename0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___filename0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___filename0, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< String_t* >::Invoke(targetMethod, targetThis, ___filename0);
					else
						GenericVirtActionInvoker1< String_t* >::Invoke(targetMethod, targetThis, ___filename0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___filename0);
					else
						VirtActionInvoker1< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___filename0);
				}
			}
			else
			{
				if (___parameterCount == 0)
				{
					typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___filename0, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___filename0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::BeginInvoke(System.String,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* OnRenameCompleted_BeginInvoke_m090E447625A7D2DCE9AE24887390FB68BAD85E31 (OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * __this, String_t* ___filename0, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___filename0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);;
}
// System.Void SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnRenameCompleted_EndInvoke_mEC5511E093D8247D914A8802352DC8DA57CFB1B2 (OnRenameCompleted_t20D859F088E0645E7D41807C6AF748880C2B1E74 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ScrollRect_set_velocity_m6CBDADD5F61163917B37C922FA6F42170D68E1D9_inline (ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method)
{
	{
		// public Vector2 velocity { get { return m_Velocity; } set { m_Velocity = value; } }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___value0;
		__this->set_m_Velocity_25(L_0);
		// public Vector2 velocity { get { return m_Velocity; } set { m_Velocity = value; } }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ScrollRect_set_movementType_m63E3C04ECBDA708AF2341564DB987227D32075CC_inline (ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public MovementType movementType { get { return m_MovementType; } set { m_MovementType = value; } }
		int32_t L_0 = ___value0;
		__this->set_m_MovementType_7(L_0);
		// public MovementType movementType { get { return m_MovementType; } set { m_MovementType = value; } }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float UISkin_get_FileHeight_m784A41AB4290882A6FB5FF8F1D9A63B235F35A49_inline (UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_fileHeight; }
		float L_0 = __this->get_m_fileHeight_43();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  UISkin_get_FileSelectedBackgroundColor_mECCF9DED2D0EA5CD1F72560F2DB1086043F87DCD_inline (UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_fileSelectedBackgroundColor; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_fileSelectedBackgroundColor_48();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * UISkin_get_FolderIcon_m10703DA30265BC01B71DCE95B3899ACC1BFA3979_inline (UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_folderIcon; }
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_0 = __this->get_m_folderIcon_51();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}

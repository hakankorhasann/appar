﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* DocumentReference_DocumentSnapshotsHandler_mE75173506F68CF4107731634BF380B9BBB7372F1_RuntimeMethod_var;
extern const RuntimeMethod* FirebaseFirestore_LoadBundleTaskProgressHandler_mDEEE792A4653B34026BC13985E88600A9D3F1393_RuntimeMethod_var;
extern const RuntimeMethod* FirebaseFirestore_SnapshotsInSyncHandler_m9820BBCB59CB6A40F2CF44F87BD7ABD483137FDE_RuntimeMethod_var;
extern const RuntimeMethod* FirestoreExceptionHelper_SetPendingFirestoreException_m0CBBB27537ABEEC0933F1407AFE213BCE003DD6C_RuntimeMethod_var;
extern const RuntimeMethod* Future_QuerySnapshot_SWIG_CompletionDispatcher_m15F1003D3DED0E6AC8CBBD6E50317019574A3596_RuntimeMethod_var;
extern const RuntimeMethod* Query_QuerySnapshotsHandler_m063CC24B00233FBA6CC4C2E3D39E731BD8800821_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingApplicationException_m7FFFCE61B08FE17B5B6C8DD589E8D2BA7E514700_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentException_m03827C4D6C68D5769B0ECF89FBA487FE09CACA03_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentNullException_m47A1AC704DEE8080F51803DC7FED941B2C51D7DE_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mF2C7DEC5C75D945A3FF2025EFD9E51FD4961ED8D_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArithmeticException_m4A492793829E24826915E01F0C17FE0B8499A7B9_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingDivideByZeroException_mD7DFEB207289EFD4D916BAE54302503294B3A469_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIOException_m8CB7AE7D58F9693F5250EEB08B367E50AF724CDB_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m20AF827963A7C8B753AF68164B7D776C15A67DE9_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidCastException_m0581C3F4931A20BD5ED4EA66093DDDD6A906C6D6_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidOperationException_m40B9057C541E1372563BB9E1002E823E38096D62_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingNullReferenceException_m07F507856FF6AE9838F5CA00AA64F38CBFA2DA84_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOutOfMemoryException_m8A1648D61A97D46629D84CB4BF64BE758E3B0987_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOverflowException_mB46C4E0EECEBB64698810B5EEBFF2A10B3869D0D_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingSystemException_m4504C1471D8E13CC34E2DA16ED245C39DBC5125F_RuntimeMethod_var;
extern const RuntimeMethod* SWIGStringHelper_CreateString_m3D08489776631E77C6252B713D92331E5D1D685E_RuntimeMethod_var;



// 0x00000001 System.Int32 Firebase.Firestore.Blob::get_Length()
extern void Blob_get_Length_mCB3B8BACCFDD08FC7B032CE7A19863A877DE1135 (void);
// 0x00000002 Firebase.Firestore.Blob Firebase.Firestore.Blob::CopyFrom(System.Byte[])
extern void Blob_CopyFrom_m4F0E073E8BFEC65E2B8AE05910DCE02FE360E9C6 (void);
// 0x00000003 System.Boolean Firebase.Firestore.Blob::Equals(System.Object)
extern void Blob_Equals_m40803BA16F983221C401AEABB5B9AFB1A1620147 (void);
// 0x00000004 System.Int32 Firebase.Firestore.Blob::GetHashCode()
extern void Blob_GetHashCode_mBF1065AEF1A335C0ED4700A81CC9FDB07CBFFFA9 (void);
// 0x00000005 System.Boolean Firebase.Firestore.Blob::Equals(Firebase.Firestore.Blob)
extern void Blob_Equals_mF653C3D01DFC846FF67142360E6136E161FF0249 (void);
// 0x00000006 System.String Firebase.Firestore.Blob::ToString()
extern void Blob_ToString_mAE6F40B4D0AABB2E52D19C6193DE711A82A5AFC9 (void);
// 0x00000007 System.Void Firebase.Firestore.Blob::.ctor(System.Byte[])
extern void Blob__ctor_mA04D8C7EC8B272C542AC71FE121655E43029C4F6 (void);
// 0x00000008 System.Void Firebase.Firestore.CollectionReference::.ctor(Firebase.Firestore.CollectionReferenceProxy,Firebase.Firestore.FirebaseFirestore)
extern void CollectionReference__ctor_mC360AA34295DB9235C94AE6D0495AEFD4A5E957E (void);
// 0x00000009 Firebase.Firestore.CollectionReferenceProxy Firebase.Firestore.CollectionReference::get_Proxy()
extern void CollectionReference_get_Proxy_mA7FA26913CC407E3F1CF549731FFE4598DFF881B (void);
// 0x0000000A System.String Firebase.Firestore.CollectionReference::get_Path()
extern void CollectionReference_get_Path_m09C9FF769AB3C184F2ADDD1A9F65F2629AD358BD (void);
// 0x0000000B System.Int32 Firebase.Firestore.CollectionReference::GetHashCode()
extern void CollectionReference_GetHashCode_m4FD3BD88088AA273C6BB6D4AC5E6124A6FFD01C2 (void);
// 0x0000000C System.Boolean Firebase.Firestore.CollectionReference::Equals(System.Object)
extern void CollectionReference_Equals_mAFCA2406C867C5AC3D6867C97164AF117BA95DB3 (void);
// 0x0000000D System.Boolean Firebase.Firestore.CollectionReference::Equals(Firebase.Firestore.CollectionReference)
extern void CollectionReference_Equals_m06C2249E16BE904A2A8DA08820535F65049ED071 (void);
// 0x0000000E System.String Firebase.Firestore.CollectionReference::ToString()
extern void CollectionReference_ToString_mB252F8C2EC7E763FB556D4D2E80FCA66DADA51BA (void);
// 0x0000000F System.Void Firebase.Firestore.DocumentReference::.ctor(Firebase.Firestore.DocumentReferenceProxy,Firebase.Firestore.FirebaseFirestore)
extern void DocumentReference__ctor_mDAD350FC1D81AED0B372171FB47A98BE681CA80A (void);
// 0x00000010 System.Void Firebase.Firestore.DocumentReference::ClearCallbacksForOwner(Firebase.Firestore.FirebaseFirestore)
extern void DocumentReference_ClearCallbacksForOwner_m9A023552DD1669B590E3BF8806A4E7FB78CA19B2 (void);
// 0x00000011 Firebase.Firestore.FirebaseFirestore Firebase.Firestore.DocumentReference::get_Firestore()
extern void DocumentReference_get_Firestore_mE365F177DC04FB9E9E7D7F918FC2C84AFB1D1DB4 (void);
// 0x00000012 System.String Firebase.Firestore.DocumentReference::get_Id()
extern void DocumentReference_get_Id_m2A0E1E7E784C3E2232049FAD4CBD6D4E7A0A6785 (void);
// 0x00000013 System.String Firebase.Firestore.DocumentReference::get_Path()
extern void DocumentReference_get_Path_m67AB7E0EEFBE6267BBAEE3D477CADD1ED1BF3DD6 (void);
// 0x00000014 Firebase.Firestore.CollectionReference Firebase.Firestore.DocumentReference::Collection(System.String)
extern void DocumentReference_Collection_mB02DADDBA697050741B80B7B0776AEA3DC8FBD30 (void);
// 0x00000015 System.Int32 Firebase.Firestore.DocumentReference::GetHashCode()
extern void DocumentReference_GetHashCode_m462DFAEFB7FB9A8FCB7DCC73604BBE714BAB40C1 (void);
// 0x00000016 System.Boolean Firebase.Firestore.DocumentReference::Equals(System.Object)
extern void DocumentReference_Equals_mD3BD6E976B96E1D513EEFCF7A399ADE585A0527F (void);
// 0x00000017 System.Boolean Firebase.Firestore.DocumentReference::Equals(Firebase.Firestore.DocumentReference)
extern void DocumentReference_Equals_m0B6FBE10301F76C704D56759D66AE30F668E3828 (void);
// 0x00000018 System.String Firebase.Firestore.DocumentReference::ToString()
extern void DocumentReference_ToString_mED5159C8071CA0CC3B2515A6CA738ABBD0478C28 (void);
// 0x00000019 System.Void Firebase.Firestore.DocumentReference::DocumentSnapshotsHandler(System.Int32,System.IntPtr,Firebase.Firestore.FirestoreError,System.String)
extern void DocumentReference_DocumentSnapshotsHandler_mE75173506F68CF4107731634BF380B9BBB7372F1 (void);
// 0x0000001A System.Void Firebase.Firestore.DocumentReference::.cctor()
extern void DocumentReference__cctor_mFC588BC5B07E2DE31BF30F50E536B9703A7646DB (void);
// 0x0000001B System.Void Firebase.Firestore.DocumentReference/ListenerDelegate::.ctor(System.Object,System.IntPtr)
extern void ListenerDelegate__ctor_m516972B8B90DD780E4E6D8F31CF0CAA8ECC5F921 (void);
// 0x0000001C System.Void Firebase.Firestore.DocumentReference/ListenerDelegate::Invoke(System.Int32,System.IntPtr,Firebase.Firestore.FirestoreError,System.String)
extern void ListenerDelegate_Invoke_mB35599DE6448E57DC4524FF9E88A76DE6AF8CB0A (void);
// 0x0000001D System.IAsyncResult Firebase.Firestore.DocumentReference/ListenerDelegate::BeginInvoke(System.Int32,System.IntPtr,Firebase.Firestore.FirestoreError,System.String,System.AsyncCallback,System.Object)
extern void ListenerDelegate_BeginInvoke_mBD5C9943EE77B8020C874D9049BA5CF4C3A016DF (void);
// 0x0000001E System.Void Firebase.Firestore.DocumentReference/ListenerDelegate::EndInvoke(System.IAsyncResult)
extern void ListenerDelegate_EndInvoke_mC33BABED612FD3DEFE57029E45F20CE9D557C65A (void);
// 0x0000001F System.Void Firebase.Firestore.DocumentSnapshot::.ctor(Firebase.Firestore.DocumentSnapshotProxy,Firebase.Firestore.FirebaseFirestore)
extern void DocumentSnapshot__ctor_m440123FAE3996CA47C8B55F965511720CE3165A9 (void);
// 0x00000020 Firebase.Firestore.DocumentReference Firebase.Firestore.DocumentSnapshot::get_Reference()
extern void DocumentSnapshot_get_Reference_m37D37D9FA6C4E8A7E04206BBF02B3528D470D4CF (void);
// 0x00000021 System.String Firebase.Firestore.DocumentSnapshot::get_Id()
extern void DocumentSnapshot_get_Id_mAF81BEE4FDEF960B4206E37A9C82CBC7FD877912 (void);
// 0x00000022 System.Boolean Firebase.Firestore.DocumentSnapshot::get_Exists()
extern void DocumentSnapshot_get_Exists_m54FDAE92E3B7310C4D957DFA23305D492FD976F7 (void);
// 0x00000023 System.Collections.Generic.Dictionary`2<System.String,System.Object> Firebase.Firestore.DocumentSnapshot::ToDictionary(Firebase.Firestore.ServerTimestampBehavior)
extern void DocumentSnapshot_ToDictionary_mC07187063BC9177FDCD693E5558269A00550C0BC (void);
// 0x00000024 T Firebase.Firestore.DocumentSnapshot::ConvertTo(Firebase.Firestore.ServerTimestampBehavior)
// 0x00000025 T Firebase.Firestore.DocumentSnapshot::GetValue(System.String,Firebase.Firestore.ServerTimestampBehavior)
// 0x00000026 T Firebase.Firestore.DocumentSnapshot::GetValue(Firebase.Firestore.FieldPath,Firebase.Firestore.ServerTimestampBehavior)
// 0x00000027 System.Boolean Firebase.Firestore.DocumentSnapshot::TryGetValue(Firebase.Firestore.FieldPath,T&,Firebase.Firestore.ServerTimestampBehavior)
// 0x00000028 System.Boolean Firebase.Firestore.DocumentSnapshot::ContainsField(System.String)
extern void DocumentSnapshot_ContainsField_mFC09531510FCF98AC73E5763CF0F65F5473E0BC3 (void);
// 0x00000029 System.Boolean Firebase.Firestore.DocumentSnapshot::ContainsField(Firebase.Firestore.FieldPath)
extern void DocumentSnapshot_ContainsField_m09735D2ED7DC7D46FE1C22F1E5065BE0FC601204 (void);
// 0x0000002A System.Boolean Firebase.Firestore.DocumentSnapshot::Equals(System.Object)
extern void DocumentSnapshot_Equals_m81B787A16028821C55F67343411141EAA9F0BCAF (void);
// 0x0000002B System.Boolean Firebase.Firestore.DocumentSnapshot::Equals(Firebase.Firestore.DocumentSnapshot)
extern void DocumentSnapshot_Equals_m73201CDDDA3D2B89CF99552C522BC6187FC7DC5F (void);
// 0x0000002C System.Int32 Firebase.Firestore.DocumentSnapshot::GetHashCode()
extern void DocumentSnapshot_GetHashCode_m6CBCD9427A55E2AE847115AE66F8A85AB18D3B02 (void);
// 0x0000002D System.String Firebase.Firestore.FieldPath::get_EncodedPath()
extern void FieldPath_get_EncodedPath_mEAEF5A0643EDAD66E3A2BAD9D038F877848B767A (void);
// 0x0000002E System.Void Firebase.Firestore.FieldPath::.ctor(System.String[],System.Boolean)
extern void FieldPath__ctor_mAB9DA3A2F79BC83B7532E07CFBB905D8073754F6 (void);
// 0x0000002F Firebase.Firestore.FieldPath Firebase.Firestore.FieldPath::FromDotSeparatedString(System.String)
extern void FieldPath_FromDotSeparatedString_m9F51824509C27E843A48903BBDA89D530566B702 (void);
// 0x00000030 System.String Firebase.Firestore.FieldPath::GetCanonicalPath(System.String[])
extern void FieldPath_GetCanonicalPath_m12697423A394FBE01E253FD086A12FF6071D09F2 (void);
// 0x00000031 System.Boolean Firebase.Firestore.FieldPath::IsValidIdentifier(System.String)
extern void FieldPath_IsValidIdentifier_m89736B9F1E3BDF65CCEC9CC1E0F3C645B4CC65BE (void);
// 0x00000032 System.String Firebase.Firestore.FieldPath::ToString()
extern void FieldPath_ToString_mA86C92B511EB8DC55FCCB875B34BA7062FD55064 (void);
// 0x00000033 System.Boolean Firebase.Firestore.FieldPath::Equals(System.Object)
extern void FieldPath_Equals_mB2615B89703A687210B2BF23DD983831F7E8CF78 (void);
// 0x00000034 System.Int32 Firebase.Firestore.FieldPath::GetHashCode()
extern void FieldPath_GetHashCode_m9464AE3EAE03CEE1C713440B4283E55E71D9B3EE (void);
// 0x00000035 System.Boolean Firebase.Firestore.FieldPath::Equals(Firebase.Firestore.FieldPath)
extern void FieldPath_Equals_mC4441ADA1B09270DF8507E2F8F394DD327F124A6 (void);
// 0x00000036 Firebase.Firestore.FieldPathProxy Firebase.Firestore.FieldPath::ConvertToProxy()
extern void FieldPath_ConvertToProxy_m1CE0CBF5574041CABBD2B20C63BF488D1C1E9C9D (void);
// 0x00000037 System.Void Firebase.Firestore.FieldPath::.cctor()
extern void FieldPath__cctor_mDD87F2472A6111700D835D969C9C1C74A15820C8 (void);
// 0x00000038 System.Void Firebase.Firestore.FieldPath/<>c::.cctor()
extern void U3CU3Ec__cctor_m99B01518EEC1E1754F7131678F70C3559273FAF5 (void);
// 0x00000039 System.Void Firebase.Firestore.FieldPath/<>c::.ctor()
extern void U3CU3Ec__ctor_m6F5075DAD3A72A7FB76631F5BF3C9116513BC681 (void);
// 0x0000003A System.Boolean Firebase.Firestore.FieldPath/<>c::<.ctor>b__9_0(System.String)
extern void U3CU3Ec_U3C_ctorU3Eb__9_0_m2B669814E472D31E1525F0884009508E133A59FF (void);
// 0x0000003B System.Void Firebase.Firestore.FirebaseFirestore::.ctor(Firebase.Firestore.FirestoreProxy,Firebase.FirebaseApp)
extern void FirebaseFirestore__ctor_m9C62CA4F8015680D272664055C99AAE316C67405 (void);
// 0x0000003C System.Void Firebase.Firestore.FirebaseFirestore::Finalize()
extern void FirebaseFirestore_Finalize_m3BE16FC86F39D69102D07B161DB77851A2FD998C (void);
// 0x0000003D System.Void Firebase.Firestore.FirebaseFirestore::OnAppDisposed(System.Object,System.EventArgs)
extern void FirebaseFirestore_OnAppDisposed_mF2054621ACB198D9DAC900DC85B8C167A0AFA00F (void);
// 0x0000003E System.Void Firebase.Firestore.FirebaseFirestore::Dispose()
extern void FirebaseFirestore_Dispose_mF7B3ADC42322E6C60E0DC75D70E2A53F7CEF5DC4 (void);
// 0x0000003F Firebase.FirebaseApp Firebase.Firestore.FirebaseFirestore::get_App()
extern void FirebaseFirestore_get_App_mFE20ECBF10BEA00E179B6FD61C12726D333E5FAF (void);
// 0x00000040 System.Void Firebase.Firestore.FirebaseFirestore::set_App(Firebase.FirebaseApp)
extern void FirebaseFirestore_set_App_mE0BD4DEC2C83B9AEA5EDD64A8A03864838A15583 (void);
// 0x00000041 Firebase.Firestore.FirebaseFirestore Firebase.Firestore.FirebaseFirestore::get_DefaultInstance()
extern void FirebaseFirestore_get_DefaultInstance_mCA4E13FD1BEE2DBC48A333C44A79E9B3EFEF42F8 (void);
// 0x00000042 Firebase.Firestore.FirebaseFirestore Firebase.Firestore.FirebaseFirestore::GetInstance(Firebase.FirebaseApp)
extern void FirebaseFirestore_GetInstance_m296C14279E63A0A7E3ADBD75DC9FCB28EA191441 (void);
// 0x00000043 Firebase.Firestore.Query Firebase.Firestore.FirebaseFirestore::CollectionGroup(System.String)
extern void FirebaseFirestore_CollectionGroup_mFFDB715CA9460D01E8F14BAD7AC468D010B3F933 (void);
// 0x00000044 System.Void Firebase.Firestore.FirebaseFirestore::SnapshotsInSyncHandler(System.Int32)
extern void FirebaseFirestore_SnapshotsInSyncHandler_m9820BBCB59CB6A40F2CF44F87BD7ABD483137FDE (void);
// 0x00000045 System.Void Firebase.Firestore.FirebaseFirestore::LoadBundleTaskProgressHandler(System.Int32,System.IntPtr)
extern void FirebaseFirestore_LoadBundleTaskProgressHandler_mDEEE792A4653B34026BC13985E88600A9D3F1393 (void);
// 0x00000046 T Firebase.Firestore.FirebaseFirestore::WithFirestoreProxy(System.Func`2<Firebase.Firestore.FirestoreProxy,T>)
// 0x00000047 System.Void Firebase.Firestore.FirebaseFirestore::RemoveSelfFromInstanceCache()
extern void FirebaseFirestore_RemoveSelfFromInstanceCache_mC97952393CD6D6F91D377E9C24B0B0300CE5536D (void);
// 0x00000048 System.Void Firebase.Firestore.FirebaseFirestore::.cctor()
extern void FirebaseFirestore__cctor_mFFADFC6F3390299CE0B1BB9ECDDC6CD48EDF8195 (void);
// 0x00000049 System.Void Firebase.Firestore.FirebaseFirestore/SnapshotsInSyncDelegate::.ctor(System.Object,System.IntPtr)
extern void SnapshotsInSyncDelegate__ctor_m7241EBE6FFAF08E441FC622EC4516DE80A82398C (void);
// 0x0000004A System.Void Firebase.Firestore.FirebaseFirestore/SnapshotsInSyncDelegate::Invoke(System.Int32)
extern void SnapshotsInSyncDelegate_Invoke_mCF79216E0F2CD8985B9EC68B1423E2CC93DCFCC9 (void);
// 0x0000004B System.IAsyncResult Firebase.Firestore.FirebaseFirestore/SnapshotsInSyncDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void SnapshotsInSyncDelegate_BeginInvoke_m7E5437770EEA32E77296AE6F07EEBFF8A4704DBF (void);
// 0x0000004C System.Void Firebase.Firestore.FirebaseFirestore/SnapshotsInSyncDelegate::EndInvoke(System.IAsyncResult)
extern void SnapshotsInSyncDelegate_EndInvoke_m830D027CAF37F86D382317AB2C5476535889E76F (void);
// 0x0000004D System.Void Firebase.Firestore.FirebaseFirestore/LoadBundleTaskProgressDelegate::.ctor(System.Object,System.IntPtr)
extern void LoadBundleTaskProgressDelegate__ctor_m3EE2F66C634BB0A62D27BBE7F5F09CE672161097 (void);
// 0x0000004E System.Void Firebase.Firestore.FirebaseFirestore/LoadBundleTaskProgressDelegate::Invoke(System.Int32,System.IntPtr)
extern void LoadBundleTaskProgressDelegate_Invoke_m07314483EE7FC580263807C0346D57C793517C33 (void);
// 0x0000004F System.IAsyncResult Firebase.Firestore.FirebaseFirestore/LoadBundleTaskProgressDelegate::BeginInvoke(System.Int32,System.IntPtr,System.AsyncCallback,System.Object)
extern void LoadBundleTaskProgressDelegate_BeginInvoke_m46358250B6BC223BE2C18662FA1D1A9F7C00A443 (void);
// 0x00000050 System.Void Firebase.Firestore.FirebaseFirestore/LoadBundleTaskProgressDelegate::EndInvoke(System.IAsyncResult)
extern void LoadBundleTaskProgressDelegate_EndInvoke_mE47B48347054A8B7A26FF25872FFF4CA6E4C9A0D (void);
// 0x00000051 System.Void Firebase.Firestore.FirebaseFirestore/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m682691CF5F63B885B0B7194ABA2216CCA6F0DEB3 (void);
// 0x00000052 Firebase.Firestore.Query Firebase.Firestore.FirebaseFirestore/<>c__DisplayClass22_0::<CollectionGroup>b__0(Firebase.Firestore.FirestoreProxy)
extern void U3CU3Ec__DisplayClass22_0_U3CCollectionGroupU3Eb__0_m5C582A53DB94741FD61353B34BDBA5AD457B544D (void);
// 0x00000053 System.Void Firebase.Firestore.FirebaseFirestore/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_mD56CA4506A6DB27D504809E954CB850653F46812 (void);
// 0x00000054 System.Object Firebase.Firestore.FirebaseFirestore/<>c__DisplayClass31_0::<SnapshotsInSyncHandler>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CSnapshotsInSyncHandlerU3Eb__0_mD3D31300A41E79DCC2F99CCA9975683523D5E56E (void);
// 0x00000055 System.Void Firebase.Firestore.FirebaseFirestoreSettings::.ctor(Firebase.Firestore.FirestoreProxy)
extern void FirebaseFirestoreSettings__ctor_m87D06EE2D1AD8DA54D55600F498D84993AF55335 (void);
// 0x00000056 System.Void Firebase.Firestore.FirebaseFirestoreSettings::Dispose()
extern void FirebaseFirestoreSettings_Dispose_m05D04CF8500D42CDBC9843B2A9803B72E8DD5794 (void);
// 0x00000057 System.String Firebase.Firestore.FirebaseFirestoreSettings::get_Host()
extern void FirebaseFirestoreSettings_get_Host_m355D5395BF19006F2B84EDB3B31E751DB3A81864 (void);
// 0x00000058 System.Boolean Firebase.Firestore.FirebaseFirestoreSettings::get_SslEnabled()
extern void FirebaseFirestoreSettings_get_SslEnabled_m4CF8E28C1A6F636ACE348F4AF3F1FA0F6FE8577D (void);
// 0x00000059 System.Boolean Firebase.Firestore.FirebaseFirestoreSettings::get_PersistenceEnabled()
extern void FirebaseFirestoreSettings_get_PersistenceEnabled_m69741B6B8804C0473E3CFE769DCCB6313904BFB6 (void);
// 0x0000005A System.Int64 Firebase.Firestore.FirebaseFirestoreSettings::get_CacheSizeBytes()
extern void FirebaseFirestoreSettings_get_CacheSizeBytes_m076680BCF68AEE8204FCEEC892DB398566B9E0CA (void);
// 0x0000005B T Firebase.Firestore.FirebaseFirestoreSettings::WithReadLock(System.Func`1<T>)
// 0x0000005C System.Void Firebase.Firestore.FirebaseFirestoreSettings::EnsureAppliedToFirestoreProxy()
extern void FirebaseFirestoreSettings_EnsureAppliedToFirestoreProxy_mCB18D5B0C23EC325A370A190C31626C539E2453B (void);
// 0x0000005D System.String Firebase.Firestore.FirebaseFirestoreSettings::ToString()
extern void FirebaseFirestoreSettings_ToString_m29AE336D5FCF1157DF91B8E396B9CFE8302FFEF6 (void);
// 0x0000005E System.Void Firebase.Firestore.FirebaseFirestoreSettings::.cctor()
extern void FirebaseFirestoreSettings__cctor_m519703A7E6C4AD1A5922A7E6FEA0C3DF9108C3BF (void);
// 0x0000005F System.String Firebase.Firestore.FirebaseFirestoreSettings::<get_Host>b__10_0()
extern void FirebaseFirestoreSettings_U3Cget_HostU3Eb__10_0_m32F265210FDA8E14193F1BD78432B9F213B35A31 (void);
// 0x00000060 System.Boolean Firebase.Firestore.FirebaseFirestoreSettings::<get_SslEnabled>b__13_0()
extern void FirebaseFirestoreSettings_U3Cget_SslEnabledU3Eb__13_0_m6F3213B4B51AF8CB72A4A009D83D385CBFA6DF93 (void);
// 0x00000061 System.Boolean Firebase.Firestore.FirebaseFirestoreSettings::<get_PersistenceEnabled>b__16_0()
extern void FirebaseFirestoreSettings_U3Cget_PersistenceEnabledU3Eb__16_0_m215919B05D4739DA6725DBA7355A8B59F00D7203 (void);
// 0x00000062 System.Int64 Firebase.Firestore.FirebaseFirestoreSettings::<get_CacheSizeBytes>b__19_0()
extern void FirebaseFirestoreSettings_U3Cget_CacheSizeBytesU3Eb__19_0_mB5530EB247CF9DC7DB85982AD8F4372208C8911C (void);
// 0x00000063 Firebase.Firestore.UnknownPropertyHandling Firebase.Firestore.FirestoreDataAttribute::get_UnknownPropertyHandling()
extern void FirestoreDataAttribute_get_UnknownPropertyHandling_m6F41C10164ECDE6EB41A4AA100A0E5F420D9E9A2 (void);
// 0x00000064 System.Type Firebase.Firestore.FirestoreDataAttribute::get_ConverterType()
extern void FirestoreDataAttribute_get_ConverterType_m2A28EAEB06F3AABC3A9972DEAE91C40E5AD55207 (void);
// 0x00000065 System.Void Firebase.Firestore.FirestoreException::.ctor(Firebase.Firestore.FirestoreError,System.String)
extern void FirestoreException__ctor_m054F3489BFADDC539275DE1B03ACF8BA7EAF3E3D (void);
// 0x00000066 System.Void Firebase.Firestore.FirestoreException::.ctor(System.Int32,System.String)
extern void FirestoreException__ctor_m5C94F368827F7FA0624CA0CFAAFFBBCEEF862944 (void);
// 0x00000067 System.Void Firebase.Firestore.FirestoreException::set_ErrorCode(Firebase.Firestore.FirestoreError)
extern void FirestoreException_set_ErrorCode_mB0AB11FA7E7D8F8A9C60419B9BEB3A6F18634DD6 (void);
// 0x00000068 System.String Firebase.Firestore.FirestorePropertyAttribute::get_Name()
extern void FirestorePropertyAttribute_get_Name_m79E0733F6DCF9A582A3DDAE8DBC2B05B22FE80C1 (void);
// 0x00000069 System.Type Firebase.Firestore.FirestorePropertyAttribute::get_ConverterType()
extern void FirestorePropertyAttribute_get_ConverterType_m01F243160236E57CB17ADC2EE8318597FB6511C3 (void);
// 0x0000006A System.Double Firebase.Firestore.GeoPoint::get_Latitude()
extern void GeoPoint_get_Latitude_m8579A0A251B83D4EA9440C8F5DC8903F66A90C64 (void);
// 0x0000006B System.Double Firebase.Firestore.GeoPoint::get_Longitude()
extern void GeoPoint_get_Longitude_m3F0E30D4821DFD38D45FE9F07AE8A6E77CC6FAFD (void);
// 0x0000006C System.Void Firebase.Firestore.GeoPoint::.ctor(System.Double,System.Double)
extern void GeoPoint__ctor_m317FEC5570FD35C090273B4370D17B4EE7CEB207 (void);
// 0x0000006D System.Boolean Firebase.Firestore.GeoPoint::Equals(System.Object)
extern void GeoPoint_Equals_mCC5F79AFC2260998069ECA5A2E37E8F1DF9F8E67 (void);
// 0x0000006E System.Int32 Firebase.Firestore.GeoPoint::GetHashCode()
extern void GeoPoint_GetHashCode_mAF353C5026FB2581BAECEB9331D6ECD63915356D (void);
// 0x0000006F System.Boolean Firebase.Firestore.GeoPoint::Equals(Firebase.Firestore.GeoPoint)
extern void GeoPoint_Equals_m3CD83E438DB040259EA1391F4F1ED0742F68A317 (void);
// 0x00000070 System.String Firebase.Firestore.GeoPoint::ToString()
extern void GeoPoint_ToString_mA5A4F4B5C2236F6D5A26D257379BF36673A06B21 (void);
// 0x00000071 Firebase.Firestore.GeoPoint Firebase.Firestore.GeoPoint::ConvertFromProxy(Firebase.Firestore.GeoPointProxy)
extern void GeoPoint_ConvertFromProxy_mE6D8EDE2CAFF46E03B64156326578953F9640FA1 (void);
// 0x00000072 System.Void Firebase.Firestore.LoadBundleTaskProgress::.ctor(Firebase.Firestore.LoadBundleTaskProgressProxy)
extern void LoadBundleTaskProgress__ctor_mAFB44DB1ACB58ED62DD7EA2424468A7C8A8BD664 (void);
// 0x00000073 System.Int32 Firebase.Firestore.LoadBundleTaskProgress::get_DocumentsLoaded()
extern void LoadBundleTaskProgress_get_DocumentsLoaded_m6BF970EB086FFA5D68F9B9520C48D60D9234DA1A (void);
// 0x00000074 System.Void Firebase.Firestore.LoadBundleTaskProgress::set_DocumentsLoaded(System.Int32)
extern void LoadBundleTaskProgress_set_DocumentsLoaded_m130F01C0AB7E075B49DBDF630670F3FC610CAD0C (void);
// 0x00000075 System.Int32 Firebase.Firestore.LoadBundleTaskProgress::get_TotalDocuments()
extern void LoadBundleTaskProgress_get_TotalDocuments_m513E22C6A8D8A79AD1D1B89EACC48EC1310AA654 (void);
// 0x00000076 System.Void Firebase.Firestore.LoadBundleTaskProgress::set_TotalDocuments(System.Int32)
extern void LoadBundleTaskProgress_set_TotalDocuments_m1D9478DCD1B5ACB953F4B4D034779B14486C621D (void);
// 0x00000077 System.Int64 Firebase.Firestore.LoadBundleTaskProgress::get_BytesLoaded()
extern void LoadBundleTaskProgress_get_BytesLoaded_m2DFB33B9A86BAF2924709D1E4A2C3F60B37CFD19 (void);
// 0x00000078 System.Void Firebase.Firestore.LoadBundleTaskProgress::set_BytesLoaded(System.Int64)
extern void LoadBundleTaskProgress_set_BytesLoaded_m1CCE09C44FB57F3378F1A689771A3C28A25AD07A (void);
// 0x00000079 System.Int64 Firebase.Firestore.LoadBundleTaskProgress::get_TotalBytes()
extern void LoadBundleTaskProgress_get_TotalBytes_m79CEE08DCB3EAD39E76A0DB5B96089216657C4EE (void);
// 0x0000007A System.Void Firebase.Firestore.LoadBundleTaskProgress::set_TotalBytes(System.Int64)
extern void LoadBundleTaskProgress_set_TotalBytes_m93FD8AB0DF78A19EB459193DF297091E554ED7AC (void);
// 0x0000007B Firebase.Firestore.LoadBundleTaskProgress/LoadBundleTaskState Firebase.Firestore.LoadBundleTaskProgress::get_State()
extern void LoadBundleTaskProgress_get_State_m8F769764F28E3783E72E51996EB7E87BC19C3C69 (void);
// 0x0000007C System.Void Firebase.Firestore.LoadBundleTaskProgress::set_State(Firebase.Firestore.LoadBundleTaskProgress/LoadBundleTaskState)
extern void LoadBundleTaskProgress_set_State_m2786077A3A514CC38685844F0A18D8F5448F0F26 (void);
// 0x0000007D System.Boolean Firebase.Firestore.LoadBundleTaskProgress::Equals(System.Object)
extern void LoadBundleTaskProgress_Equals_m433BEA923EEDB72F084E1CFD4220304774B98275 (void);
// 0x0000007E System.Int32 Firebase.Firestore.LoadBundleTaskProgress::GetHashCode()
extern void LoadBundleTaskProgress_GetHashCode_mFC367643EA25F701314E361FF6137E175B1242CF (void);
// 0x0000007F System.Void Firebase.Firestore.Query::.ctor(Firebase.Firestore.QueryProxy,Firebase.Firestore.FirebaseFirestore)
extern void Query__ctor_mEB2ADC4A6E9AFFE447C9D1F31F36CCFD818852FD (void);
// 0x00000080 System.Void Firebase.Firestore.Query::ClearCallbacksForOwner(Firebase.Firestore.FirebaseFirestore)
extern void Query_ClearCallbacksForOwner_m97EDA1CD84FD1BE2B658CCC2CA251EDF2640DAF2 (void);
// 0x00000081 Firebase.Firestore.FirebaseFirestore Firebase.Firestore.Query::get_Firestore()
extern void Query_get_Firestore_mC814C09FC2283E8507285FCF53B6B449D84132EB (void);
// 0x00000082 System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot> Firebase.Firestore.Query::GetSnapshotAsync(Firebase.Firestore.Source)
extern void Query_GetSnapshotAsync_m4DCA7247582F0851CB06A9395A52F4E2B5CEE82F (void);
// 0x00000083 System.Boolean Firebase.Firestore.Query::Equals(System.Object)
extern void Query_Equals_m5284C5936D7613C32BE6220A12E3F664954F7163 (void);
// 0x00000084 System.Boolean Firebase.Firestore.Query::Equals(Firebase.Firestore.Query)
extern void Query_Equals_mEEBBE0C8E44726E31CFE9E84D0B2C01175EF0151 (void);
// 0x00000085 System.Int32 Firebase.Firestore.Query::GetHashCode()
extern void Query_GetHashCode_m0563E67FA2AE859A43D3343C662553257C6ABC87 (void);
// 0x00000086 System.Void Firebase.Firestore.Query::QuerySnapshotsHandler(System.Int32,System.IntPtr,Firebase.Firestore.FirestoreError,System.String)
extern void Query_QuerySnapshotsHandler_m063CC24B00233FBA6CC4C2E3D39E731BD8800821 (void);
// 0x00000087 System.Void Firebase.Firestore.Query::.cctor()
extern void Query__cctor_m7476F6E50B304E1A0FE5D8E49B11A84BBDF8A115 (void);
// 0x00000088 Firebase.Firestore.QuerySnapshot Firebase.Firestore.Query::<GetSnapshotAsync>b__42_0(Firebase.Firestore.QuerySnapshotProxy)
extern void Query_U3CGetSnapshotAsyncU3Eb__42_0_m1DE8A930153268D1A35C97196F0531E516F24C19 (void);
// 0x00000089 System.Void Firebase.Firestore.Query/ListenerDelegate::.ctor(System.Object,System.IntPtr)
extern void ListenerDelegate__ctor_m43866ED56E7128C8146C12F9362EEA538C1EA003 (void);
// 0x0000008A System.Void Firebase.Firestore.Query/ListenerDelegate::Invoke(System.Int32,System.IntPtr,Firebase.Firestore.FirestoreError,System.String)
extern void ListenerDelegate_Invoke_m25CBB5929C885D49D34F22E8F5BE7D91990998B3 (void);
// 0x0000008B System.IAsyncResult Firebase.Firestore.Query/ListenerDelegate::BeginInvoke(System.Int32,System.IntPtr,Firebase.Firestore.FirestoreError,System.String,System.AsyncCallback,System.Object)
extern void ListenerDelegate_BeginInvoke_m083FDD25D5A449FC98CA54551A88A6900D293A00 (void);
// 0x0000008C System.Void Firebase.Firestore.Query/ListenerDelegate::EndInvoke(System.IAsyncResult)
extern void ListenerDelegate_EndInvoke_mF13563B5B2A5FC6D1DC416215CEC13FF5DFF0D74 (void);
// 0x0000008D System.Void Firebase.Firestore.QuerySnapshot::.ctor(Firebase.Firestore.QuerySnapshotProxy,Firebase.Firestore.FirebaseFirestore)
extern void QuerySnapshot__ctor_m7B944833CBA25BC9221D2312F14879EFCB7AF44A (void);
// 0x0000008E System.Collections.Generic.IEnumerable`1<Firebase.Firestore.DocumentSnapshot> Firebase.Firestore.QuerySnapshot::get_Documents()
extern void QuerySnapshot_get_Documents_m37B48B52758015A41B5A6A234E1AADDA2EF1C931 (void);
// 0x0000008F System.Int32 Firebase.Firestore.QuerySnapshot::get_Count()
extern void QuerySnapshot_get_Count_m26905C4585CB9FD9B1258C743491DB646CC84978 (void);
// 0x00000090 System.Boolean Firebase.Firestore.QuerySnapshot::Equals(System.Object)
extern void QuerySnapshot_Equals_m79F99EB5F0DF4B410DFF2924C03547F32BE2B0FA (void);
// 0x00000091 System.Boolean Firebase.Firestore.QuerySnapshot::Equals(Firebase.Firestore.QuerySnapshot)
extern void QuerySnapshot_Equals_m7209D3F353832DA27C2E144B6CE2615B8DA1CE5F (void);
// 0x00000092 System.Int32 Firebase.Firestore.QuerySnapshot::GetHashCode()
extern void QuerySnapshot_GetHashCode_m7EC56D7A28BBA74F1D6308F0B68BD63EA1AB85B7 (void);
// 0x00000093 System.Collections.Generic.IEnumerator`1<Firebase.Firestore.DocumentSnapshot> Firebase.Firestore.QuerySnapshot::GetEnumerator()
extern void QuerySnapshot_GetEnumerator_m73650855D40A51E5D3512E90AAA61C06B9982410 (void);
// 0x00000094 System.Collections.IEnumerator Firebase.Firestore.QuerySnapshot::System.Collections.IEnumerable.GetEnumerator()
extern void QuerySnapshot_System_Collections_IEnumerable_GetEnumerator_mA169630FC6BD68F1FE3E70D751923604A3CABF9E (void);
// 0x00000095 System.Void Firebase.Firestore.QuerySnapshot::LoadDocumentsCached()
extern void QuerySnapshot_LoadDocumentsCached_m0009CC8648134C02B7878605C488DF9E40F6E7CE (void);
// 0x00000096 Firebase.Firestore.DocumentSnapshotProxy/ServerTimestampBehavior Firebase.Firestore.ServerTimestampBehaviorConverter::ConvertToProxy(Firebase.Firestore.ServerTimestampBehavior)
extern void ServerTimestampBehaviorConverter_ConvertToProxy_mEEA510E99EA91987C2497A71A418C75A4D234F30 (void);
// 0x00000097 System.DateTime Firebase.Firestore.Timestamp::ToDateTime()
extern void Timestamp_ToDateTime_mCE43F7D6B36597BD9EB814027BC58604EB50463C (void);
// 0x00000098 System.DateTimeOffset Firebase.Firestore.Timestamp::ToDateTimeOffset()
extern void Timestamp_ToDateTimeOffset_mE057569E9579185AB1C03D7DC59E0E3B0EB7986E (void);
// 0x00000099 System.Boolean Firebase.Firestore.Timestamp::Equals(System.Object)
extern void Timestamp_Equals_mC5FB2C7784C4154CD68A065B9C3B2979954D8AA7 (void);
// 0x0000009A System.Int32 Firebase.Firestore.Timestamp::GetHashCode()
extern void Timestamp_GetHashCode_m77644F39C766B1A3E3C3C8A8DE9898DAE0DDE115 (void);
// 0x0000009B System.Boolean Firebase.Firestore.Timestamp::Equals(Firebase.Firestore.Timestamp)
extern void Timestamp_Equals_mE6E5EF21BA260D4719381F9C9171CD1C80A9C350 (void);
// 0x0000009C System.Int32 Firebase.Firestore.Timestamp::CompareTo(Firebase.Firestore.Timestamp)
extern void Timestamp_CompareTo_m5E0F72C5F36DC0E3E81C88313619B79D213F920F (void);
// 0x0000009D System.Int32 Firebase.Firestore.Timestamp::CompareTo(System.Object)
extern void Timestamp_CompareTo_mAC46B7F62D530BDEB81CAB57230CA93B72B5A14D (void);
// 0x0000009E System.String Firebase.Firestore.Timestamp::ToString()
extern void Timestamp_ToString_m52D6B302274294F4092FE717A7897B06FB88BD22 (void);
// 0x0000009F System.Void Firebase.Firestore.Timestamp::.ctor(System.Int64,System.Int32)
extern void Timestamp__ctor_mE2833EDD86D95B4925817BEE9D4EA8C7A3960F0C (void);
// 0x000000A0 Firebase.Firestore.Timestamp Firebase.Firestore.Timestamp::ConvertFromProxy(Firebase.Firestore.TimestampProxy)
extern void Timestamp_ConvertFromProxy_mE19C85E7998F189FF868EF6A5C194E9D0DB8BD81 (void);
// 0x000000A1 System.Void Firebase.Firestore.Timestamp::.cctor()
extern void Timestamp__cctor_m5F7DD6186799067260D981E0542699AC3B741724 (void);
// 0x000000A2 System.Collections.Generic.IDictionary`2<System.Type,Firebase.Firestore.Converters.IFirestoreInternalConverter> Firebase.Firestore.ConverterRegistry::ToConverterDictionary()
extern void ConverterRegistry_ToConverterDictionary_m4808DE699DEBC029E7429A5245FBE78B37671B48 (void);
// 0x000000A3 Firebase.Firestore.FirebaseFirestore Firebase.Firestore.DeserializationContext::get_Firestore()
extern void DeserializationContext_get_Firestore_m6B0FAF5AA5BE201010D7C68F4569B91485E766A3 (void);
// 0x000000A4 Firebase.Firestore.DocumentReference Firebase.Firestore.DeserializationContext::get_DocumentReference()
extern void DeserializationContext_get_DocumentReference_m4DFEC290691ADFC8BDD5E24DB21149ED65280C6F (void);
// 0x000000A5 System.Void Firebase.Firestore.DeserializationContext::.ctor(Firebase.Firestore.DocumentSnapshot)
extern void DeserializationContext__ctor_mEFCDB84903C674450970C8B17E712E2E6BDC73F2 (void);
// 0x000000A6 Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.DeserializationContext::GetConverter(System.Type)
extern void DeserializationContext_GetConverter_m47CEDFD4868171BFEF47DE4D7976FF879E592DBC (void);
// 0x000000A7 T Firebase.Firestore.FirestoreConverter`1::FromFirestore(System.Object)
// 0x000000A8 System.Void Firebase.Firestore.ListenerRegistrationMap`1::.ctor()
// 0x000000A9 System.Void Firebase.Firestore.ListenerRegistrationMap`1::AssertGenericArgumentIsDelegate()
// 0x000000AA System.Boolean Firebase.Firestore.ListenerRegistrationMap`1::TryGetCallback(System.Int32,T&)
// 0x000000AB System.Void Firebase.Firestore.ListenerRegistrationMap`1::Unregister(System.Int32)
// 0x000000AC System.Void Firebase.Firestore.ListenerRegistrationMap`1::ClearCallbacksForOwner(System.Object)
// 0x000000AD System.Void Firebase.Firestore.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern void MonoPInvokeCallbackAttribute__ctor_m252D1BA1415C301DA86545F886ABEF89C3455CFE (void);
// 0x000000AE Firebase.Firestore.SerializationContext Firebase.Firestore.SerializationContext::get_Default()
extern void SerializationContext_get_Default_m0956FECF356BC8E246C6E42D5C8A4ED85B6E55A3 (void);
// 0x000000AF System.Void Firebase.Firestore.SerializationContext::.ctor(Firebase.Firestore.ConverterRegistry)
extern void SerializationContext__ctor_m25A92660D9E2E40E0AE5A92251AD0C0EC1618874 (void);
// 0x000000B0 Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.SerializationContext::GetConverter(System.Type)
extern void SerializationContext_GetConverter_mB984F5EE0455BF4CFA117386329B8B08FA58C0F0 (void);
// 0x000000B1 System.Void Firebase.Firestore.SerializationContext::.cctor()
extern void SerializationContext__cctor_m9F632160D25C8C0FA3B739442FAB6F04C5501A01 (void);
// 0x000000B2 System.Void Firebase.Firestore.TransactionManager::.ctor(Firebase.Firestore.FirebaseFirestore,Firebase.Firestore.FirestoreProxy)
extern void TransactionManager__ctor_m035B33F98C36F472E7FF88AEAE5FCA8EB9F61655 (void);
// 0x000000B3 System.Void Firebase.Firestore.TransactionManager::Finalize()
extern void TransactionManager_Finalize_mF2F91FB6C47AC4B6454A1E9C6001EE2D866C5954 (void);
// 0x000000B4 System.Void Firebase.Firestore.TransactionManager::Dispose()
extern void TransactionManager_Dispose_m7C5D2B38A91122D1320508B02C8675DEF087DE4C (void);
// 0x000000B5 System.Void Firebase.Firestore.TransactionManager::.cctor()
extern void TransactionManager__cctor_mBEF867B58B1D8075E64D3AE7258CF35E3B1B8C6D (void);
// 0x000000B6 System.Object Firebase.Firestore.ValueDeserializer::Deserialize(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy,System.Type)
extern void ValueDeserializer_Deserialize_m79C973D65BE151BFBFBCD37F1A6BEDB9BE00C475 (void);
// 0x000000B7 System.Type Firebase.Firestore.ValueDeserializer::GetTargetType(Firebase.Firestore.FieldValueProxy)
extern void ValueDeserializer_GetTargetType_mAA329A104F405BB2F863F4E04330AD63D5A6FC10 (void);
// 0x000000B8 System.Void Firebase.Firestore.GeoPointProxy::.ctor(System.IntPtr,System.Boolean)
extern void GeoPointProxy__ctor_m503FE59503EAEBDAE684E8363F7CFCFFDE3688F5 (void);
// 0x000000B9 System.Void Firebase.Firestore.GeoPointProxy::Finalize()
extern void GeoPointProxy_Finalize_m5D1810A18A4FAC7E1B142C2145C88DA93800A27A (void);
// 0x000000BA System.Void Firebase.Firestore.GeoPointProxy::Dispose()
extern void GeoPointProxy_Dispose_mB4652360A9B30A9A42E3DD9EA94CD369C83B8A21 (void);
// 0x000000BB System.Void Firebase.Firestore.GeoPointProxy::Dispose(System.Boolean)
extern void GeoPointProxy_Dispose_mED62560CA8FE528B3F066CFA0917687140DB0F60 (void);
// 0x000000BC System.Double Firebase.Firestore.GeoPointProxy::latitude()
extern void GeoPointProxy_latitude_mD77BEC5F0193678C898C89416F1662A1A49BE354 (void);
// 0x000000BD System.Double Firebase.Firestore.GeoPointProxy::longitude()
extern void GeoPointProxy_longitude_m49B75B72326AFB9A846FD018B59208CD34DA2031 (void);
// 0x000000BE System.Void Firebase.Firestore.TimestampProxy::.ctor(System.IntPtr,System.Boolean)
extern void TimestampProxy__ctor_mA4660CC22801025A8965BA1A993215FBB6E85BD5 (void);
// 0x000000BF System.Void Firebase.Firestore.TimestampProxy::Finalize()
extern void TimestampProxy_Finalize_mCF761B35246D4CCD0BB2ED4E855DDD96D3A77E86 (void);
// 0x000000C0 System.Void Firebase.Firestore.TimestampProxy::Dispose()
extern void TimestampProxy_Dispose_m8A46682120EDD8C21F249EE707EAD6F7848EF606 (void);
// 0x000000C1 System.Void Firebase.Firestore.TimestampProxy::Dispose(System.Boolean)
extern void TimestampProxy_Dispose_mFFD18273ED5195FD49634955E6C1D8F5222437BE (void);
// 0x000000C2 System.Int64 Firebase.Firestore.TimestampProxy::seconds()
extern void TimestampProxy_seconds_m0F2EE61F32833A1BA1675ECAB05FD7CC20B3CBA7 (void);
// 0x000000C3 System.Int32 Firebase.Firestore.TimestampProxy::nanoseconds()
extern void TimestampProxy_nanoseconds_m08AEE21A7F9CFA8AA09FA0EAA57D9A7F923A2C82 (void);
// 0x000000C4 System.String Firebase.Firestore.TimestampProxy::ToString()
extern void TimestampProxy_ToString_m4952B4CD40DC4C860E71E09A89FC14045B160D18 (void);
// 0x000000C5 System.Void Firebase.Firestore.Future_QuerySnapshot::.ctor(System.IntPtr,System.Boolean)
extern void Future_QuerySnapshot__ctor_mDEA54B3B64E61DFF472724C181F3F9455336608E (void);
// 0x000000C6 System.Void Firebase.Firestore.Future_QuerySnapshot::Dispose(System.Boolean)
extern void Future_QuerySnapshot_Dispose_m5FCF1C1E08921D680B67F245E27F2AB60E3EB27C (void);
// 0x000000C7 System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshotProxy> Firebase.Firestore.Future_QuerySnapshot::GetTask(Firebase.Firestore.Future_QuerySnapshot)
extern void Future_QuerySnapshot_GetTask_m66DDC683EF82E646BDB538ABD56B37DC90387EC1 (void);
// 0x000000C8 System.Void Firebase.Firestore.Future_QuerySnapshot::ThrowIfDisposed()
extern void Future_QuerySnapshot_ThrowIfDisposed_m2705F53407E210AB76966B4E95BFFC73C7DCAE7A (void);
// 0x000000C9 System.Void Firebase.Firestore.Future_QuerySnapshot::SetOnCompletionCallback(Firebase.Firestore.Future_QuerySnapshot/Action)
extern void Future_QuerySnapshot_SetOnCompletionCallback_m75637E2782B5A3D3CF3D5451174685B22F788A6A (void);
// 0x000000CA System.Void Firebase.Firestore.Future_QuerySnapshot::SetCompletionData(System.IntPtr)
extern void Future_QuerySnapshot_SetCompletionData_mEF8805DF1E200ABA2BFF78A7EDDCE63E0B3BE610 (void);
// 0x000000CB System.Void Firebase.Firestore.Future_QuerySnapshot::SWIG_CompletionDispatcher(System.Int32)
extern void Future_QuerySnapshot_SWIG_CompletionDispatcher_m15F1003D3DED0E6AC8CBBD6E50317019574A3596 (void);
// 0x000000CC System.IntPtr Firebase.Firestore.Future_QuerySnapshot::SWIG_OnCompletion(Firebase.Firestore.Future_QuerySnapshot/SWIG_CompletionDelegate,System.Int32)
extern void Future_QuerySnapshot_SWIG_OnCompletion_m28863D996259870ECF3CA4F99B4F4E3C68F6DE27 (void);
// 0x000000CD System.Void Firebase.Firestore.Future_QuerySnapshot::SWIG_FreeCompletionData(System.IntPtr)
extern void Future_QuerySnapshot_SWIG_FreeCompletionData_mB8BB4B764EF8AB036121B46ED916EF50AF947B7A (void);
// 0x000000CE Firebase.Firestore.QuerySnapshotProxy Firebase.Firestore.Future_QuerySnapshot::GetResult()
extern void Future_QuerySnapshot_GetResult_mB5451A3D0E569C8B118F719D4829D8BE64F67600 (void);
// 0x000000CF System.Void Firebase.Firestore.Future_QuerySnapshot::.cctor()
extern void Future_QuerySnapshot__cctor_mBDEA7E704BDB9B7EBEC1C5964A061AC2AB6D4E57 (void);
// 0x000000D0 System.Void Firebase.Firestore.Future_QuerySnapshot/Action::.ctor(System.Object,System.IntPtr)
extern void Action__ctor_m7EF534DDB33940EFCB27C62F9CC0099EEC5C449A (void);
// 0x000000D1 System.Void Firebase.Firestore.Future_QuerySnapshot/Action::Invoke()
extern void Action_Invoke_m89DF1B06F56CAA5C63411739D5737CC7B14899A3 (void);
// 0x000000D2 System.IAsyncResult Firebase.Firestore.Future_QuerySnapshot/Action::BeginInvoke(System.AsyncCallback,System.Object)
extern void Action_BeginInvoke_m7DE528B79101FD5A35532ED4A92E19217F252F11 (void);
// 0x000000D3 System.Void Firebase.Firestore.Future_QuerySnapshot/Action::EndInvoke(System.IAsyncResult)
extern void Action_EndInvoke_mDF49D090DD28E94B16D603662716A61178460981 (void);
// 0x000000D4 System.Void Firebase.Firestore.Future_QuerySnapshot/SWIG_CompletionDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIG_CompletionDelegate__ctor_m99D201F61D68828B480F1010B48318E4053D3D78 (void);
// 0x000000D5 System.Void Firebase.Firestore.Future_QuerySnapshot/SWIG_CompletionDelegate::Invoke(System.Int32)
extern void SWIG_CompletionDelegate_Invoke_m304CB24BCA0217EFCD7B8430157301A563520954 (void);
// 0x000000D6 System.IAsyncResult Firebase.Firestore.Future_QuerySnapshot/SWIG_CompletionDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void SWIG_CompletionDelegate_BeginInvoke_m8906D7078C669A40F525B3C56C2908F5834816AE (void);
// 0x000000D7 System.Void Firebase.Firestore.Future_QuerySnapshot/SWIG_CompletionDelegate::EndInvoke(System.IAsyncResult)
extern void SWIG_CompletionDelegate_EndInvoke_m19114113506A682DF91CFC186000628BD046C9CF (void);
// 0x000000D8 System.Void Firebase.Firestore.Future_QuerySnapshot/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m4774D1F84E0BD959630F97D96156A9A5705B5595 (void);
// 0x000000D9 System.Void Firebase.Firestore.Future_QuerySnapshot/<>c__DisplayClass5_0::<GetTask>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CGetTaskU3Eb__0_mE383F7A3496FB0EACA7E95E3E9C43F7797A8C4D8 (void);
// 0x000000DA System.Void Firebase.Firestore.FieldPathProxy::.ctor(System.IntPtr,System.Boolean)
extern void FieldPathProxy__ctor_m551E15BC25F35D5CAD46B4423A894C2C4007D5EB (void);
// 0x000000DB System.Runtime.InteropServices.HandleRef Firebase.Firestore.FieldPathProxy::getCPtr(Firebase.Firestore.FieldPathProxy)
extern void FieldPathProxy_getCPtr_m704AFA24E03C02A34164F62CB898E97066563342 (void);
// 0x000000DC System.Void Firebase.Firestore.FieldPathProxy::Finalize()
extern void FieldPathProxy_Finalize_m4D19B0ABC6DC43C3786D64F025AE7B41C5874171 (void);
// 0x000000DD System.Void Firebase.Firestore.FieldPathProxy::Dispose()
extern void FieldPathProxy_Dispose_m88895AB7067D85F6D41DF9011A5479AC856C960F (void);
// 0x000000DE System.Void Firebase.Firestore.FieldPathProxy::Dispose(System.Boolean)
extern void FieldPathProxy_Dispose_m4989360F4FBE2985E885C9FB0018AD9BC277B10B (void);
// 0x000000DF System.Void Firebase.Firestore.FieldPathProxy::.ctor(Firebase.StringList)
extern void FieldPathProxy__ctor_m990B19F76EDCD541343F65C2502E1D928881AD7C (void);
// 0x000000E0 System.Void Firebase.Firestore.DocumentReferenceProxy::.ctor(System.IntPtr,System.Boolean)
extern void DocumentReferenceProxy__ctor_mC16BDEB7842F70661F8CA8F1250935A1177AF031 (void);
// 0x000000E1 System.Void Firebase.Firestore.DocumentReferenceProxy::Finalize()
extern void DocumentReferenceProxy_Finalize_m4726B63A0533838834D6982ABCEAF32EC8C81D15 (void);
// 0x000000E2 System.Void Firebase.Firestore.DocumentReferenceProxy::Dispose()
extern void DocumentReferenceProxy_Dispose_m51F96BFD6E3F7B7A6893AF1BF74114131D14558A (void);
// 0x000000E3 System.Void Firebase.Firestore.DocumentReferenceProxy::Dispose(System.Boolean)
extern void DocumentReferenceProxy_Dispose_m25459CB8B570DDB4DD040C924087D2BF3D38BAAC (void);
// 0x000000E4 System.String Firebase.Firestore.DocumentReferenceProxy::id()
extern void DocumentReferenceProxy_id_m7E403109672DA3100AE2FD3B3C50A777AD94FD2E (void);
// 0x000000E5 System.String Firebase.Firestore.DocumentReferenceProxy::path()
extern void DocumentReferenceProxy_path_m029F85D7203D96AC23B4F4F5876641445C141A8C (void);
// 0x000000E6 Firebase.Firestore.CollectionReferenceProxy Firebase.Firestore.DocumentReferenceProxy::Collection(System.String)
extern void DocumentReferenceProxy_Collection_mEB94ED01A8596BAE1E7A25DADC82064165AB36EC (void);
// 0x000000E7 System.Boolean Firebase.Firestore.DocumentReferenceProxy::is_valid()
extern void DocumentReferenceProxy_is_valid_m35E2AF9EA37631F1FB48F5404E720C1F38CAC66B (void);
// 0x000000E8 System.Void Firebase.Firestore.DocumentSnapshotProxy::.ctor(System.IntPtr,System.Boolean)
extern void DocumentSnapshotProxy__ctor_mBAD0F8FE3E90D3DE870F6F5AB349361ED8514D54 (void);
// 0x000000E9 System.Runtime.InteropServices.HandleRef Firebase.Firestore.DocumentSnapshotProxy::getCPtr(Firebase.Firestore.DocumentSnapshotProxy)
extern void DocumentSnapshotProxy_getCPtr_mC596F629731827F5601A4CCE082D6BB46EB7CFE7 (void);
// 0x000000EA System.Void Firebase.Firestore.DocumentSnapshotProxy::Finalize()
extern void DocumentSnapshotProxy_Finalize_m62E5BFBA5FF7E2497050D719B7C4CF77C73B0C18 (void);
// 0x000000EB System.Void Firebase.Firestore.DocumentSnapshotProxy::Dispose()
extern void DocumentSnapshotProxy_Dispose_m4EC7C87930238519CC17E343ADDACE585DD5213D (void);
// 0x000000EC System.Void Firebase.Firestore.DocumentSnapshotProxy::Dispose(System.Boolean)
extern void DocumentSnapshotProxy_Dispose_mDBF0D1C3D0BB693D3BDAFF6C729753A51A23DCF7 (void);
// 0x000000ED System.String Firebase.Firestore.DocumentSnapshotProxy::id()
extern void DocumentSnapshotProxy_id_m33FD7F6797324295E546D9FBDF520F4BBC06EBB3 (void);
// 0x000000EE Firebase.Firestore.DocumentReferenceProxy Firebase.Firestore.DocumentSnapshotProxy::reference()
extern void DocumentSnapshotProxy_reference_m439DE34728C7DB587F55C2A5FB95DCCB549968B5 (void);
// 0x000000EF System.Boolean Firebase.Firestore.DocumentSnapshotProxy::exists()
extern void DocumentSnapshotProxy_exists_m0D38B8E8DEE0DBFDE41CDE8D2D0F640BB35EF970 (void);
// 0x000000F0 Firebase.Firestore.FieldValueProxy Firebase.Firestore.DocumentSnapshotProxy::Get(Firebase.Firestore.FieldPathProxy,Firebase.Firestore.DocumentSnapshotProxy/ServerTimestampBehavior)
extern void DocumentSnapshotProxy_Get_mD7F98EDDAF252126061424402D5342F819DAA57F (void);
// 0x000000F1 Firebase.Firestore.FieldValueProxy Firebase.Firestore.DocumentSnapshotProxy::Get(Firebase.Firestore.FieldPathProxy)
extern void DocumentSnapshotProxy_Get_m6A5C488C926D8AA1177A69D703E0DA6D58F133CA (void);
// 0x000000F2 System.Void Firebase.Firestore.FieldValueProxy::.ctor(System.IntPtr,System.Boolean)
extern void FieldValueProxy__ctor_mE828DA30BD0C7E6EE1DA953D0476A4A9E77661F4 (void);
// 0x000000F3 System.Runtime.InteropServices.HandleRef Firebase.Firestore.FieldValueProxy::getCPtr(Firebase.Firestore.FieldValueProxy)
extern void FieldValueProxy_getCPtr_m3947C9F651BD757FC8AED85AB38AB437DC361548 (void);
// 0x000000F4 System.Void Firebase.Firestore.FieldValueProxy::Finalize()
extern void FieldValueProxy_Finalize_m3E90D05962E503EB598B4BEBC99F34FE88498B93 (void);
// 0x000000F5 System.Void Firebase.Firestore.FieldValueProxy::Dispose()
extern void FieldValueProxy_Dispose_m95750EE003249AF5D8A18F63EEE8AB4E61740A66 (void);
// 0x000000F6 System.Void Firebase.Firestore.FieldValueProxy::Dispose(System.Boolean)
extern void FieldValueProxy_Dispose_m37EC6DDC55EE3DA29AB636A1C95DD94FDA78A8CF (void);
// 0x000000F7 Firebase.Firestore.FieldValueProxy/Type Firebase.Firestore.FieldValueProxy::type()
extern void FieldValueProxy_type_m55A3F37538E22B530641299E3B5505A9A059B59A (void);
// 0x000000F8 System.Boolean Firebase.Firestore.FieldValueProxy::is_null()
extern void FieldValueProxy_is_null_m4AA92FD9666242AA45DA660AC2BB8C7A1C3DF1A5 (void);
// 0x000000F9 System.Boolean Firebase.Firestore.FieldValueProxy::boolean_value()
extern void FieldValueProxy_boolean_value_mE70FE6A25101476564ED10874B6264D0450ADD85 (void);
// 0x000000FA System.Int64 Firebase.Firestore.FieldValueProxy::integer_value()
extern void FieldValueProxy_integer_value_mC6B9A8ED23560D552E5D86975F5E34D3D8188C53 (void);
// 0x000000FB System.Double Firebase.Firestore.FieldValueProxy::double_value()
extern void FieldValueProxy_double_value_mEC74286D7C5EFD0E2F03CFE3483B8719104CAA0C (void);
// 0x000000FC Firebase.Firestore.TimestampProxy Firebase.Firestore.FieldValueProxy::timestamp_value()
extern void FieldValueProxy_timestamp_value_mAFB838B5DAEB67F815401BF41F15F95C2A60407B (void);
// 0x000000FD System.String Firebase.Firestore.FieldValueProxy::string_value()
extern void FieldValueProxy_string_value_m23A143E31F5C198FB4E9348ED0AC113FB299E9FC (void);
// 0x000000FE Firebase.Firestore.SWIGTYPE_p_unsigned_char Firebase.Firestore.FieldValueProxy::blob_value()
extern void FieldValueProxy_blob_value_m3D58D62958553095D100FEF987DCF15E05C467B1 (void);
// 0x000000FF System.UInt32 Firebase.Firestore.FieldValueProxy::blob_size()
extern void FieldValueProxy_blob_size_m0D51A7AD5010F00F3541879E9F617C1D4E8C774E (void);
// 0x00000100 Firebase.Firestore.DocumentReferenceProxy Firebase.Firestore.FieldValueProxy::reference_value()
extern void FieldValueProxy_reference_value_mE3570A66A31EB4DD26AAD0CEC6536B30D7587F3A (void);
// 0x00000101 Firebase.Firestore.GeoPointProxy Firebase.Firestore.FieldValueProxy::geo_point_value()
extern void FieldValueProxy_geo_point_value_mE1E1FFFF2D3C200742F9C04369D63767EF1A27B4 (void);
// 0x00000102 System.Boolean Firebase.Firestore.FieldValueProxy::is_valid()
extern void FieldValueProxy_is_valid_m7FF64B82B8E0D43C9604956E4E3EB04F892E410C (void);
// 0x00000103 Firebase.Firestore.FieldValueProxy Firebase.Firestore.FieldValueProxy::ServerTimestamp()
extern void FieldValueProxy_ServerTimestamp_m5B07EAB411791549FEF1EF76D55400C916FD9764 (void);
// 0x00000104 System.Void Firebase.Firestore.QueryProxy::.ctor(System.IntPtr,System.Boolean)
extern void QueryProxy__ctor_mC23671BBD1432C4BAA366E797F5DE246FBD506F0 (void);
// 0x00000105 System.Runtime.InteropServices.HandleRef Firebase.Firestore.QueryProxy::getCPtr(Firebase.Firestore.QueryProxy)
extern void QueryProxy_getCPtr_m72857AE629E20043696A3A817E9C100DFE47E10C (void);
// 0x00000106 System.Void Firebase.Firestore.QueryProxy::Finalize()
extern void QueryProxy_Finalize_mB9CFA288DD6C60B2AEA691A200080E38BC933CFD (void);
// 0x00000107 System.Void Firebase.Firestore.QueryProxy::Dispose()
extern void QueryProxy_Dispose_m960DD3B08BEDCC048DE5D16F9D6BE5985AD41C6E (void);
// 0x00000108 System.Void Firebase.Firestore.QueryProxy::Dispose(System.Boolean)
extern void QueryProxy_Dispose_m35E4DB2E72A0D4993286E5A0920164D0B8A210CE (void);
// 0x00000109 System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshotProxy> Firebase.Firestore.QueryProxy::GetAsync(Firebase.Firestore.SourceProxy)
extern void QueryProxy_GetAsync_m605193A42AA193CD332BC4CEB3FD408896FCC46F (void);
// 0x0000010A System.Void Firebase.Firestore.CollectionReferenceProxy::.ctor(System.IntPtr,System.Boolean)
extern void CollectionReferenceProxy__ctor_mBA68C64A100E06FB7DDB45ECEFF570FC6BA62BD5 (void);
// 0x0000010B System.Void Firebase.Firestore.CollectionReferenceProxy::Dispose(System.Boolean)
extern void CollectionReferenceProxy_Dispose_m3CF3C81545003B9E05F7D2D6A5D5FA41389E8EE4 (void);
// 0x0000010C System.String Firebase.Firestore.CollectionReferenceProxy::path()
extern void CollectionReferenceProxy_path_mDB709F31636C6061CF16E128B033BC653206AEEF (void);
// 0x0000010D System.Void Firebase.Firestore.QuerySnapshotProxy::.ctor(System.IntPtr,System.Boolean)
extern void QuerySnapshotProxy__ctor_m79F372CBDBD224830BA43ED48C63FBA27870BFAD (void);
// 0x0000010E System.Runtime.InteropServices.HandleRef Firebase.Firestore.QuerySnapshotProxy::getCPtr(Firebase.Firestore.QuerySnapshotProxy)
extern void QuerySnapshotProxy_getCPtr_m86260290881CFD039F214598F2D6C8C89B7D6FF6 (void);
// 0x0000010F System.Void Firebase.Firestore.QuerySnapshotProxy::Finalize()
extern void QuerySnapshotProxy_Finalize_m9884C8D991BE36287C0FCE54FF9EDBE420A182A9 (void);
// 0x00000110 System.Void Firebase.Firestore.QuerySnapshotProxy::Dispose()
extern void QuerySnapshotProxy_Dispose_mE01AC7029ECB65708C684DE1B9CF86DE95E91854 (void);
// 0x00000111 System.Void Firebase.Firestore.QuerySnapshotProxy::Dispose(System.Boolean)
extern void QuerySnapshotProxy_Dispose_m6EC845FBC2F8A7E652F63E24F26066FB0EF2B040 (void);
// 0x00000112 System.UInt32 Firebase.Firestore.QuerySnapshotProxy::size()
extern void QuerySnapshotProxy_size_m5F85F760E58C896DEB38068B1EA3C1E0F061272E (void);
// 0x00000113 System.Void Firebase.Firestore.SettingsProxy::.ctor(System.IntPtr,System.Boolean)
extern void SettingsProxy__ctor_m57F7B30B9FC12FC9AD8CEB2FF644366ACE08FCD0 (void);
// 0x00000114 System.Runtime.InteropServices.HandleRef Firebase.Firestore.SettingsProxy::getCPtr(Firebase.Firestore.SettingsProxy)
extern void SettingsProxy_getCPtr_m3F02F7C4D67FB3CA6516EFD760DC730C22C255D2 (void);
// 0x00000115 System.Void Firebase.Firestore.SettingsProxy::Finalize()
extern void SettingsProxy_Finalize_m8A4DCB9AB82CD5828C13FE34F26FE3C083DE7226 (void);
// 0x00000116 System.Void Firebase.Firestore.SettingsProxy::Dispose()
extern void SettingsProxy_Dispose_m020A1134948209EE62549E8CCCE9BBEB8C0B8C9E (void);
// 0x00000117 System.Void Firebase.Firestore.SettingsProxy::Dispose(System.Boolean)
extern void SettingsProxy_Dispose_mFA2E769C190403E9ECB4C81B35827270B784D8D6 (void);
// 0x00000118 System.Void Firebase.Firestore.SettingsProxy::.ctor()
extern void SettingsProxy__ctor_m1E66E049659C1D5848BA5598F2CD221DCA012C0F (void);
// 0x00000119 System.String Firebase.Firestore.SettingsProxy::host()
extern void SettingsProxy_host_mEA699725ABF182715DCC2B4BBABB6085C43BB5D6 (void);
// 0x0000011A System.Boolean Firebase.Firestore.SettingsProxy::is_ssl_enabled()
extern void SettingsProxy_is_ssl_enabled_mA385B9EC7BE54B2D119A01BFA46F44C6BEE04BDA (void);
// 0x0000011B System.Boolean Firebase.Firestore.SettingsProxy::is_persistence_enabled()
extern void SettingsProxy_is_persistence_enabled_mD08842A00B90262157A80B8418024D5D73A6BAE6 (void);
// 0x0000011C System.Int64 Firebase.Firestore.SettingsProxy::cache_size_bytes()
extern void SettingsProxy_cache_size_bytes_mDE0491A0EA8D2DA09281519C3BDA6BAAB761503C (void);
// 0x0000011D System.Void Firebase.Firestore.SettingsProxy::set_host(System.String)
extern void SettingsProxy_set_host_m61CA41356D9E2D4E32EFD01B7F989FB06D96646D (void);
// 0x0000011E System.Void Firebase.Firestore.SettingsProxy::set_ssl_enabled(System.Boolean)
extern void SettingsProxy_set_ssl_enabled_mF2ACBD4CEDFDBEDB56E9189A083B031725A906B6 (void);
// 0x0000011F System.Void Firebase.Firestore.SettingsProxy::set_persistence_enabled(System.Boolean)
extern void SettingsProxy_set_persistence_enabled_mAD8328FE1F2707E4508218D1A3FF0769CAFD0D84 (void);
// 0x00000120 System.Void Firebase.Firestore.SettingsProxy::set_cache_size_bytes(System.Int64)
extern void SettingsProxy_set_cache_size_bytes_mE974897951FB753357D9DFD49B6C1F8F546DBB90 (void);
// 0x00000121 System.Void Firebase.Firestore.SettingsProxy::.cctor()
extern void SettingsProxy__cctor_mF50600439845272C13761BC7D243D4D650414217 (void);
// 0x00000122 System.Void Firebase.Firestore.LoadBundleTaskProgressProxy::.ctor(System.IntPtr,System.Boolean)
extern void LoadBundleTaskProgressProxy__ctor_mBF5E431FF5E258BBF0CD71351DC80E6FF1B8DCD8 (void);
// 0x00000123 System.Void Firebase.Firestore.LoadBundleTaskProgressProxy::Finalize()
extern void LoadBundleTaskProgressProxy_Finalize_m5F9E9B3C32964253E34B941FDC9E08037D7CACE5 (void);
// 0x00000124 System.Void Firebase.Firestore.LoadBundleTaskProgressProxy::Dispose()
extern void LoadBundleTaskProgressProxy_Dispose_mEC65FDAC5B40DD08897FB7CF1E9EE1A9F45A90FB (void);
// 0x00000125 System.Void Firebase.Firestore.LoadBundleTaskProgressProxy::Dispose(System.Boolean)
extern void LoadBundleTaskProgressProxy_Dispose_mD473194165A831BB56B772A654E7C068E3901160 (void);
// 0x00000126 System.Int32 Firebase.Firestore.LoadBundleTaskProgressProxy::documents_loaded()
extern void LoadBundleTaskProgressProxy_documents_loaded_m552BF9D8F53588F185F6B8809098814DBA65DE2A (void);
// 0x00000127 System.Int32 Firebase.Firestore.LoadBundleTaskProgressProxy::total_documents()
extern void LoadBundleTaskProgressProxy_total_documents_mD46FF5F4D699E3220B48E3837532C9AA2C610144 (void);
// 0x00000128 System.Int64 Firebase.Firestore.LoadBundleTaskProgressProxy::bytes_loaded()
extern void LoadBundleTaskProgressProxy_bytes_loaded_mD0AE2512A5A34FA2077462A1B86162F1B8E535D6 (void);
// 0x00000129 System.Int64 Firebase.Firestore.LoadBundleTaskProgressProxy::total_bytes()
extern void LoadBundleTaskProgressProxy_total_bytes_mE03751426599353CC31437ECE1FCCEE171A60097 (void);
// 0x0000012A Firebase.Firestore.LoadBundleTaskProgressProxy/State Firebase.Firestore.LoadBundleTaskProgressProxy::state()
extern void LoadBundleTaskProgressProxy_state_m95106E703DACA2FD5CD5B303D8BD6EB95C6BAEF3 (void);
// 0x0000012B System.Void Firebase.Firestore.FirestoreProxy::.ctor(System.IntPtr,System.Boolean)
extern void FirestoreProxy__ctor_m4C4FFB2CDFF1E08173F83A7CE70138F7A3D69A7D (void);
// 0x0000012C System.Runtime.InteropServices.HandleRef Firebase.Firestore.FirestoreProxy::getCPtr(Firebase.Firestore.FirestoreProxy)
extern void FirestoreProxy_getCPtr_mDB561C2FF706494DC5B52DABFA4A78A9CA79894D (void);
// 0x0000012D System.Void Firebase.Firestore.FirestoreProxy::Finalize()
extern void FirestoreProxy_Finalize_mE88DF3B641523CC454028A6B71318A7067FBDB67 (void);
// 0x0000012E System.Void Firebase.Firestore.FirestoreProxy::Dispose()
extern void FirestoreProxy_Dispose_m6678747251CDFEE7CCA821D4A9D7269C62501F46 (void);
// 0x0000012F System.Void Firebase.Firestore.FirestoreProxy::Dispose(System.Boolean)
extern void FirestoreProxy_Dispose_m7313A037D25DEF004CA8CFBA522F4318492EAE23 (void);
// 0x00000130 Firebase.Firestore.FirestoreProxy Firebase.Firestore.FirestoreProxy::GetInstance(Firebase.FirebaseApp)
extern void FirestoreProxy_GetInstance_m7ABF5D2143A9EBF56328B183FF677791D9C758C8 (void);
// 0x00000131 Firebase.Firestore.QueryProxy Firebase.Firestore.FirestoreProxy::CollectionGroup(System.String)
extern void FirestoreProxy_CollectionGroup_m1EC6B3F459B061CB3FA60DBB938C114898033F6A (void);
// 0x00000132 Firebase.Firestore.SettingsProxy Firebase.Firestore.FirestoreProxy::settings()
extern void FirestoreProxy_settings_m685CFC1A9E739B2060CCE7178F2F6B9EAA7D8ABA (void);
// 0x00000133 System.Void Firebase.Firestore.FirestoreProxy::set_settings(Firebase.Firestore.SettingsProxy)
extern void FirestoreProxy_set_settings_m51B39F1D76707F90748B4F42029D368242FC1EFF (void);
// 0x00000134 System.Void Firebase.Firestore.ApiHeaders::SetClientLanguage(System.String)
extern void ApiHeaders_SetClientLanguage_m3AF676E9FF8F6604CAFE0D94C721EBA688CF3488 (void);
// 0x00000135 System.Void Firebase.Firestore.TransactionCallbackProxy::Dispose()
extern void TransactionCallbackProxy_Dispose_m1CA0BFFBDDA685A1DE31BAA7CA252913EC891607 (void);
// 0x00000136 System.Void Firebase.Firestore.TransactionCallbackProxy::Dispose(System.Boolean)
extern void TransactionCallbackProxy_Dispose_mBC87D7102F42BC74C2E2CCABDFA42EBAC3525651 (void);
// 0x00000137 System.Void Firebase.Firestore.TransactionManagerProxy::.ctor(System.IntPtr,System.Boolean)
extern void TransactionManagerProxy__ctor_m53CEB44B8C3BC25F31C48776CA8B408D22F214CD (void);
// 0x00000138 System.Void Firebase.Firestore.TransactionManagerProxy::Finalize()
extern void TransactionManagerProxy_Finalize_mFD74B4034FFAA833657FD94DD84F7DDEC820B506 (void);
// 0x00000139 System.Void Firebase.Firestore.TransactionManagerProxy::Dispose()
extern void TransactionManagerProxy_Dispose_m34EE3E31286A32DE4FD0C85740DFF827B4BE58AA (void);
// 0x0000013A System.Void Firebase.Firestore.TransactionManagerProxy::Dispose(System.Boolean)
extern void TransactionManagerProxy_Dispose_m52B4D40D02D22B2B87CCC21811810861C91C84CC (void);
// 0x0000013B System.Void Firebase.Firestore.TransactionManagerProxy::.ctor(Firebase.Firestore.FirestoreProxy)
extern void TransactionManagerProxy__ctor_m1D026BDB5EA6D37C2C7D9B7F7813B4B23353F694 (void);
// 0x0000013C System.Void Firebase.Firestore.TransactionManagerProxy::CppDispose()
extern void TransactionManagerProxy_CppDispose_m806AB05DC32D3CAE9FF86414EC067D5C70204611 (void);
// 0x0000013D System.Void Firebase.Firestore.FieldToValueMap::.ctor(System.IntPtr,System.Boolean)
extern void FieldToValueMap__ctor_m5339A881427D4DA7E3A7F34F24290E189A758919 (void);
// 0x0000013E System.Void Firebase.Firestore.FieldToValueMap::Finalize()
extern void FieldToValueMap_Finalize_mBF86F5DAEBC922A79D819E480CF8AE1A8F959DEC (void);
// 0x0000013F System.Void Firebase.Firestore.FieldToValueMap::Dispose()
extern void FieldToValueMap_Dispose_mFF45079007FA669B86FD0968E22870EF2E07E225 (void);
// 0x00000140 System.Void Firebase.Firestore.FieldToValueMap::Dispose(System.Boolean)
extern void FieldToValueMap_Dispose_m277CCC95581DAB0047DA74E28F2135F93DC2EE5E (void);
// 0x00000141 Firebase.Firestore.FieldToValueMapIterator Firebase.Firestore.FieldToValueMap::Iterator()
extern void FieldToValueMap_Iterator_mFEBB720DD557118C1D8623555D7B73EDEDBCB998 (void);
// 0x00000142 System.Void Firebase.Firestore.FieldToValueMapIterator::.ctor(System.IntPtr,System.Boolean)
extern void FieldToValueMapIterator__ctor_mBFE6E50889C6F726BC0933FED3570B26B63A9D2A (void);
// 0x00000143 System.Void Firebase.Firestore.FieldToValueMapIterator::Finalize()
extern void FieldToValueMapIterator_Finalize_mBCB231C8B6B184D2840E68DE39A468F084BCCF8F (void);
// 0x00000144 System.Void Firebase.Firestore.FieldToValueMapIterator::Dispose()
extern void FieldToValueMapIterator_Dispose_mBF4E5BA72F53EE1FC039F7477B0EC8562733AF3A (void);
// 0x00000145 System.Void Firebase.Firestore.FieldToValueMapIterator::Dispose(System.Boolean)
extern void FieldToValueMapIterator_Dispose_mCE8AE325CD1A523EE1BD044A85CF691F435EE581 (void);
// 0x00000146 System.Boolean Firebase.Firestore.FieldToValueMapIterator::HasMore()
extern void FieldToValueMapIterator_HasMore_m189096D902425FDA478A1379E402026304B311EA (void);
// 0x00000147 System.Void Firebase.Firestore.FieldToValueMapIterator::Advance()
extern void FieldToValueMapIterator_Advance_mE1DD7B73FB55A3E59C7F4CCB4913559A7FF61216 (void);
// 0x00000148 System.String Firebase.Firestore.FieldToValueMapIterator::UnsafeKeyView()
extern void FieldToValueMapIterator_UnsafeKeyView_mA1499884B3EFE247AD50162B0CE7EA24EE244484 (void);
// 0x00000149 Firebase.Firestore.FieldValueProxy Firebase.Firestore.FieldToValueMapIterator::UnsafeValueView()
extern void FieldToValueMapIterator_UnsafeValueView_mCD65EE0367E18C0D55CE4F168CE2BFA6BFB651DE (void);
// 0x0000014A System.Void Firebase.Firestore.DocumentSnapshotVector::.ctor(System.IntPtr,System.Boolean)
extern void DocumentSnapshotVector__ctor_mF5426DCC89074F309B8CE675E6E99F0B9CF537BA (void);
// 0x0000014B System.Void Firebase.Firestore.DocumentSnapshotVector::Finalize()
extern void DocumentSnapshotVector_Finalize_m7428D02E1769AD720BE3721B24A42010713D21E8 (void);
// 0x0000014C System.Void Firebase.Firestore.DocumentSnapshotVector::Dispose()
extern void DocumentSnapshotVector_Dispose_mA3AF9504F817D8125D0D9B58E96325DF4315E434 (void);
// 0x0000014D System.Void Firebase.Firestore.DocumentSnapshotVector::Dispose(System.Boolean)
extern void DocumentSnapshotVector_Dispose_mD3134B43B36CBEC1BEC989557AF8ED8A05494141 (void);
// 0x0000014E Firebase.Firestore.DocumentSnapshotProxy Firebase.Firestore.DocumentSnapshotVector::GetCopy(System.UInt32)
extern void DocumentSnapshotVector_GetCopy_m8DDDF5D444CD64042E931C761E6984498FA5D757 (void);
// 0x0000014F System.Void Firebase.Firestore.FieldValueVector::.ctor(System.IntPtr,System.Boolean)
extern void FieldValueVector__ctor_mE18FA41B6B529487E8688077F80A74189FC62DAE (void);
// 0x00000150 System.Void Firebase.Firestore.FieldValueVector::Finalize()
extern void FieldValueVector_Finalize_mAA757ABBCF56EEAA67AA1CFC454A19547BD5A6A7 (void);
// 0x00000151 System.Void Firebase.Firestore.FieldValueVector::Dispose()
extern void FieldValueVector_Dispose_m6C8D5DA5C3370F4A15EDEF8B7BE26404F906657D (void);
// 0x00000152 System.Void Firebase.Firestore.FieldValueVector::Dispose(System.Boolean)
extern void FieldValueVector_Dispose_m9A096A9F13355EAE94CCCC5D4066F68EF8857758 (void);
// 0x00000153 System.UInt32 Firebase.Firestore.FieldValueVector::Size()
extern void FieldValueVector_Size_mA9B2314BB8BF28735078B6CFC52B1DFCC423A16F (void);
// 0x00000154 Firebase.Firestore.FieldValueProxy Firebase.Firestore.FieldValueVector::GetUnsafeView(System.UInt32)
extern void FieldValueVector_GetUnsafeView_m36367447BDCEC4FFF4EC60D99CF124018527AE9E (void);
// 0x00000155 System.Void Firebase.Firestore.FirestoreCppPINVOKE::.cctor()
extern void FirestoreCppPINVOKE__cctor_m6A9FB3CEB620709BFDEC4D2D2AF869F3A737B388 (void);
// 0x00000156 System.Double Firebase.Firestore.FirestoreCppPINVOKE::GeoPointProxy_latitude(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_GeoPointProxy_latitude_m537FE7FED53D1454296BD0B9D8547AFD0688E60E (void);
// 0x00000157 System.Double Firebase.Firestore.FirestoreCppPINVOKE::GeoPointProxy_longitude(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_GeoPointProxy_longitude_m51EF668A6B0BAD251E1FA48CBCF4A6664E38381A (void);
// 0x00000158 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_GeoPointProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_GeoPointProxy_mED14F013285E6857B0A34B88166A2C4415F76F56 (void);
// 0x00000159 System.Int64 Firebase.Firestore.FirestoreCppPINVOKE::TimestampProxy_seconds(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_TimestampProxy_seconds_m4430E089356F673DD4349FCA11F566B1D9A916B9 (void);
// 0x0000015A System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::TimestampProxy_nanoseconds(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_TimestampProxy_nanoseconds_m8047B0FF2505A98EFAD4536FD97410C754F85043 (void);
// 0x0000015B System.String Firebase.Firestore.FirestoreCppPINVOKE::TimestampProxy_ToString(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_TimestampProxy_ToString_mA91DDA5AB5BF27A022B63D8E914470060FA545FB (void);
// 0x0000015C System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_TimestampProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_TimestampProxy_mE2426617F833198994E71D4484DAA6C003B3B3BE (void);
// 0x0000015D System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::Future_QuerySnapshot_SWIG_OnCompletion(System.Runtime.InteropServices.HandleRef,Firebase.Firestore.Future_QuerySnapshot/SWIG_CompletionDelegate,System.Int32)
extern void FirestoreCppPINVOKE_Future_QuerySnapshot_SWIG_OnCompletion_mA33D4D03BEDD8C344FF3FEAD7C67A6FEAC9CDF8F (void);
// 0x0000015E System.Void Firebase.Firestore.FirestoreCppPINVOKE::Future_QuerySnapshot_SWIG_FreeCompletionData(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void FirestoreCppPINVOKE_Future_QuerySnapshot_SWIG_FreeCompletionData_m99EC9E6390243CD7E9055A74BEB26A55AF0C1BA0 (void);
// 0x0000015F System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::Future_QuerySnapshot_GetResult(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_Future_QuerySnapshot_GetResult_mB282775D2D5EC7A8F3A87A8C18FD43B5C5AE643E (void);
// 0x00000160 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_Future_QuerySnapshot(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_Future_QuerySnapshot_m15662DAA837429601EB9C89E73C2B2C74417DCE8 (void);
// 0x00000161 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::new_FieldPathProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_new_FieldPathProxy_m87ABD8A7DD51859E038105AA7C00077B7C0867B8 (void);
// 0x00000162 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_FieldPathProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_FieldPathProxy_mF4084D48638E1119FE695AF732B4F9FE60E1E035 (void);
// 0x00000163 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_DocumentReferenceProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_DocumentReferenceProxy_mA7D5F0F56719E34AE70F4A7A77653AB3E7665797 (void);
// 0x00000164 System.String Firebase.Firestore.FirestoreCppPINVOKE::DocumentReferenceProxy_id(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentReferenceProxy_id_m4DACDC8AA63C76713D38205D5D8BE7DFB1748650 (void);
// 0x00000165 System.String Firebase.Firestore.FirestoreCppPINVOKE::DocumentReferenceProxy_path(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentReferenceProxy_path_mBD7874B87080484A379890CA896E4C5886B079FD (void);
// 0x00000166 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::DocumentReferenceProxy_Collection__SWIG_0(System.Runtime.InteropServices.HandleRef,System.String)
extern void FirestoreCppPINVOKE_DocumentReferenceProxy_Collection__SWIG_0_mC53C05F2259F229233F521BEEB3A82AD17BB1E50 (void);
// 0x00000167 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::DocumentReferenceProxy_is_valid(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentReferenceProxy_is_valid_m4B4AD8FCD88216AC47B32A48BC285F31CCD3DF3C (void);
// 0x00000168 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_DocumentSnapshotProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_DocumentSnapshotProxy_m2708B2FE67CB08FFED03FAB0192AD79DA00BB4E5 (void);
// 0x00000169 System.String Firebase.Firestore.FirestoreCppPINVOKE::DocumentSnapshotProxy_id(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentSnapshotProxy_id_m09A7FBCF9B82F712B84723CAE641D80A5758F166 (void);
// 0x0000016A System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::DocumentSnapshotProxy_reference(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentSnapshotProxy_reference_mC7156E7AA6DC01B96EDBD6A9F7BB079DD7ECA26A (void);
// 0x0000016B System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::DocumentSnapshotProxy_exists(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentSnapshotProxy_exists_mC95BD56939C1A4FC06FC89B91D6B3FCCEC6FC9FC (void);
// 0x0000016C System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::DocumentSnapshotProxy_Get__SWIG_4(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef,System.Int32)
extern void FirestoreCppPINVOKE_DocumentSnapshotProxy_Get__SWIG_4_mFBDD0CD92F85722AA7AB5F0605AD6901DC771459 (void);
// 0x0000016D System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::DocumentSnapshotProxy_Get__SWIG_5(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentSnapshotProxy_Get__SWIG_5_mD460A5D0549A33BFBE4A7FE09B32C13988B6291E (void);
// 0x0000016E System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_FieldValueProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_FieldValueProxy_mD92AF82E60AB6F6A4C2D49E2096E9F1E88A90F00 (void);
// 0x0000016F System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_type(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_type_mE04FEBAB491DA62FDFE1D60B40A63D4CB40866C1 (void);
// 0x00000170 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_is_null(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_is_null_m04A661A9AD919338D30C756D48269E3E78849AE5 (void);
// 0x00000171 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_boolean_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_boolean_value_m3233D408B22705664486498D57048754D81C4A10 (void);
// 0x00000172 System.Int64 Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_integer_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_integer_value_m2A1E2B41577017985C361238AFA93505A2F7675B (void);
// 0x00000173 System.Double Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_double_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_double_value_mA8E38B05E0B4946364AFA010B9BC3A5BC7004CFD (void);
// 0x00000174 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_timestamp_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_timestamp_value_m5FC7860EF2A41F6D3282EFF20C3D2CE5F3E1646D (void);
// 0x00000175 System.String Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_string_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_string_value_m79353C0E5B6E8AD460F06C67DE4AF938942D7AF8 (void);
// 0x00000176 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_blob_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_blob_value_mE1E3AF0ECC594FFA3A4984041C37DB67126F0E15 (void);
// 0x00000177 System.UInt32 Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_blob_size(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_blob_size_mFE868920BEF027EDC2E679F15AA91F0EC71E6C98 (void);
// 0x00000178 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_reference_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_reference_value_m952902083B7DDBE69D17538DA03BC2F4874654B3 (void);
// 0x00000179 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_geo_point_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_geo_point_value_m03094FD58C2D5372153DCB7972FBF468BD408B06 (void);
// 0x0000017A System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_is_valid(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_is_valid_mC9092415AE893E7AC1EDEF7B821E04BD7FFEE320 (void);
// 0x0000017B System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_ServerTimestamp()
extern void FirestoreCppPINVOKE_FieldValueProxy_ServerTimestamp_mE018EA2B6AFE62FB55A27778AC5BC47DEBE75A86 (void);
// 0x0000017C System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_QueryProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_QueryProxy_m228C9F5E88FC4664FD78CF6EA74D4058E5102007 (void);
// 0x0000017D System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::QueryProxy_Get__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void FirestoreCppPINVOKE_QueryProxy_Get__SWIG_0_m3115B7AAF5848F8FA8E2BA6806FA57874E1C1326 (void);
// 0x0000017E System.String Firebase.Firestore.FirestoreCppPINVOKE::CollectionReferenceProxy_path(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_CollectionReferenceProxy_path_m4521D143BDF4FCE6E3375633F1FDED4271F17747 (void);
// 0x0000017F System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_CollectionReferenceProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_CollectionReferenceProxy_mBD43D3610EEAC3B43B0EEE284B3C928AD4C9918F (void);
// 0x00000180 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_QuerySnapshotProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_QuerySnapshotProxy_m68A5283AE62C854AA4E9ADB6922ABCE81B56063F (void);
// 0x00000181 System.UInt32 Firebase.Firestore.FirestoreCppPINVOKE::QuerySnapshotProxy_size(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_QuerySnapshotProxy_size_m9BFE60EDC71DD4966C1E1914CE64A3CE906AE383 (void);
// 0x00000182 System.Int64 Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_kCacheSizeUnlimited_get()
extern void FirestoreCppPINVOKE_SettingsProxy_kCacheSizeUnlimited_get_m2F7BBE5038D7E9CAD9980A90B4A6280EC8097D8B (void);
// 0x00000183 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::new_SettingsProxy__SWIG_0()
extern void FirestoreCppPINVOKE_new_SettingsProxy__SWIG_0_m688BFD940F82F9327983447043CB806EE31C2CCC (void);
// 0x00000184 System.String Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_host(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_SettingsProxy_host_m6DC84E2B2121422BC6CDADC536354F725D3308A4 (void);
// 0x00000185 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_is_ssl_enabled(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_SettingsProxy_is_ssl_enabled_mEC26AA47A593DB3F83D87FAEE765EDDD3D8A55B2 (void);
// 0x00000186 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_is_persistence_enabled(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_SettingsProxy_is_persistence_enabled_m4E5291EB27D3090B8304E18D8A56D7F027FF2BBA (void);
// 0x00000187 System.Int64 Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_cache_size_bytes(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_SettingsProxy_cache_size_bytes_m1A8AD60E0A48F636DAE036A3512D18643DBE0AC7 (void);
// 0x00000188 System.Void Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_set_host(System.Runtime.InteropServices.HandleRef,System.String)
extern void FirestoreCppPINVOKE_SettingsProxy_set_host_mC1C913CECCF32040492CE49A5AB2E28A1130341E (void);
// 0x00000189 System.Void Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_set_ssl_enabled(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void FirestoreCppPINVOKE_SettingsProxy_set_ssl_enabled_m66282A99FA880FA5451F4EFD713E63710DA0A6C1 (void);
// 0x0000018A System.Void Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_set_persistence_enabled(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void FirestoreCppPINVOKE_SettingsProxy_set_persistence_enabled_mE19F04D4289F0B3F3CE981E97A5C3D4D0D0AAC86 (void);
// 0x0000018B System.Void Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_set_cache_size_bytes(System.Runtime.InteropServices.HandleRef,System.Int64)
extern void FirestoreCppPINVOKE_SettingsProxy_set_cache_size_bytes_m79399836EC0972A5A0F13C7DD3177083F16970B9 (void);
// 0x0000018C System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_SettingsProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_SettingsProxy_m114100B64B935697B05A2C4C99AF00F809B16C90 (void);
// 0x0000018D System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::LoadBundleTaskProgressProxy_documents_loaded(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_documents_loaded_m62117737D1CFF5B4654B41F0D386DBF1519E80BB (void);
// 0x0000018E System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::LoadBundleTaskProgressProxy_total_documents(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_total_documents_m41245AD15C13BE9748C6AAA011A6CF7C6BE793EC (void);
// 0x0000018F System.Int64 Firebase.Firestore.FirestoreCppPINVOKE::LoadBundleTaskProgressProxy_bytes_loaded(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_bytes_loaded_m587322A1529B00FB674425010392448871F5DB6A (void);
// 0x00000190 System.Int64 Firebase.Firestore.FirestoreCppPINVOKE::LoadBundleTaskProgressProxy_total_bytes(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_total_bytes_mE0A3EA40F724A27030786FCA168F1D94B2BF4220 (void);
// 0x00000191 System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::LoadBundleTaskProgressProxy_state(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_state_mE3E6DA981B84C777411400B9E39395A674ADD9EA (void);
// 0x00000192 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_LoadBundleTaskProgressProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_LoadBundleTaskProgressProxy_mFC3046514D537A92ED7AE9F78E9632DA6F0A6FDF (void);
// 0x00000193 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FirestoreProxy_GetInstance__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FirestoreProxy_GetInstance__SWIG_1_mBD594EF31E9E6E52CF3A5E2A78041A6C8FC7CEB3 (void);
// 0x00000194 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_FirestoreProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_FirestoreProxy_mEF5ACB794EC595C7432D0D59BB365BCA21CAD386 (void);
// 0x00000195 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FirestoreProxy_CollectionGroup__SWIG_0(System.Runtime.InteropServices.HandleRef,System.String)
extern void FirestoreCppPINVOKE_FirestoreProxy_CollectionGroup__SWIG_0_mCA3399619A837C66D338A08A67FD53EECDF19F48 (void);
// 0x00000196 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FirestoreProxy_settings(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FirestoreProxy_settings_m48EBFBF46A828E40260B30E3AA1C597BB1AE2421 (void);
// 0x00000197 System.Void Firebase.Firestore.FirestoreCppPINVOKE::FirestoreProxy_set_settings(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FirestoreProxy_set_settings_mCD052E2C597DE0B3D7841C05A4450241FDD8D4D1 (void);
// 0x00000198 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::QueryEquals(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_QueryEquals_m0426DF2ED4809404E969E63310B1BFC0D9B12DFD (void);
// 0x00000199 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::QuerySnapshotEquals(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_QuerySnapshotEquals_m07F3AC1EC83D3F3D06C1470F7AB5D96F2BE2E89C (void);
// 0x0000019A System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::DocumentSnapshotEquals(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentSnapshotEquals_m460B1847DD65DEFD258374F5B0B175FFA1EFEA08 (void);
// 0x0000019B System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::QueryHashCode(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_QueryHashCode_m908CC15F2024AF939A762C5778B592416E7F1BD1 (void);
// 0x0000019C System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::QuerySnapshotHashCode(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_QuerySnapshotHashCode_m38F7F4FCDCAA53681C7C87BCA56A94856E70F29C (void);
// 0x0000019D System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::DocumentSnapshotHashCode(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentSnapshotHashCode_m4F40AB3D10F3377C37F95974513CD8457C5B34C2 (void);
// 0x0000019E System.Void Firebase.Firestore.FirestoreCppPINVOKE::ApiHeaders_SetClientLanguage(System.String)
extern void FirestoreCppPINVOKE_ApiHeaders_SetClientLanguage_mFE2F038BDCE725BEABEEC65CA0B74472525DC655 (void);
// 0x0000019F System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::ConvertFieldValueToMap(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_ConvertFieldValueToMap_mBDCA4247A42753FE6E9DBFA711A998092AAB11FE (void);
// 0x000001A0 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::ConvertSnapshotToFieldValue(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void FirestoreCppPINVOKE_ConvertSnapshotToFieldValue_m02CC9A869A6F5693F15497C727F881DD5C1FEFCC (void);
// 0x000001A1 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_TransactionCallbackProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_TransactionCallbackProxy_mE1B8873E8EE0910D456872A34EFC7F15C0E0A6CB (void);
// 0x000001A2 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::new_TransactionManagerProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_new_TransactionManagerProxy_mDC63D2A4A583124DA6C703B3BA9C2E3F29CE2083 (void);
// 0x000001A3 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_TransactionManagerProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_TransactionManagerProxy_m1D0BC77C0DFF0CDF09923AA6A9DAF8D1E51459D5 (void);
// 0x000001A4 System.Void Firebase.Firestore.FirestoreCppPINVOKE::TransactionManagerProxy_CppDispose(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_TransactionManagerProxy_CppDispose_mF2FB0567B4AC38A9A10FFECDA60BE2EE1506CA43 (void);
// 0x000001A5 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldToValueMap_Iterator(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldToValueMap_Iterator_mCFEA49BEA05008313EFFFD915D46C0BA04A37E71 (void);
// 0x000001A6 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_FieldToValueMap(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_FieldToValueMap_m5A4456978BA774A8724FEAE557C3998A08074131 (void);
// 0x000001A7 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::FieldToValueMapIterator_HasMore(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldToValueMapIterator_HasMore_mB05288423F66BFF297E56B5EC546CFF9769D4476 (void);
// 0x000001A8 System.Void Firebase.Firestore.FirestoreCppPINVOKE::FieldToValueMapIterator_Advance(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldToValueMapIterator_Advance_m2EB53F62915671CAEBA81916F8750DC9EBB52166 (void);
// 0x000001A9 System.String Firebase.Firestore.FirestoreCppPINVOKE::FieldToValueMapIterator_UnsafeKeyView(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldToValueMapIterator_UnsafeKeyView_m5A2D5372B95546681215C51C74BF445AB3873280 (void);
// 0x000001AA System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldToValueMapIterator_UnsafeValueView(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldToValueMapIterator_UnsafeValueView_mA0D3E465E2A30789331CC4624505E9B17AF41818 (void);
// 0x000001AB System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_FieldToValueMapIterator(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_FieldToValueMapIterator_m1714A53BB7800AE2CA78B443DDF33419BBAE739D (void);
// 0x000001AC System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::ConvertFieldValueToVector(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_ConvertFieldValueToVector_mFB518CC2861633D35A31799688BFF008E61C757E (void);
// 0x000001AD System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::QuerySnapshotDocuments(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_QuerySnapshotDocuments_mA35B2F5FAE7B778E372CC46A64E1FCA8F237BFB7 (void);
// 0x000001AE System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::DocumentSnapshotVector_GetCopy(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern void FirestoreCppPINVOKE_DocumentSnapshotVector_GetCopy_m5445F4BDC462687FB7D1091C0E947EBA3EDA1428 (void);
// 0x000001AF System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_DocumentSnapshotVector(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_DocumentSnapshotVector_m670DD105A4E40A1B4285FA2FC6ED4318BA0F22B0 (void);
// 0x000001B0 System.UInt32 Firebase.Firestore.FirestoreCppPINVOKE::FieldValueVector_Size(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueVector_Size_m3D05B8294C969ADF4D4B85D930CCF94E5065E140 (void);
// 0x000001B1 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldValueVector_GetUnsafeView(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern void FirestoreCppPINVOKE_FieldValueVector_GetUnsafeView_m34B063798E9746541015D0C11C06E2341F1A8A74 (void);
// 0x000001B2 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_FieldValueVector(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_FieldValueVector_m0BEEC7CFB160B40F88F72888DF6AFCF67857D3E7 (void);
// 0x000001B3 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::Future_QuerySnapshot_SWIGUpcast(System.IntPtr)
extern void FirestoreCppPINVOKE_Future_QuerySnapshot_SWIGUpcast_mB3CDB804C07D71576B8362A3F3DB5E22B1E8FEE6 (void);
// 0x000001B4 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::CollectionReferenceProxy_SWIGUpcast(System.IntPtr)
extern void FirestoreCppPINVOKE_CollectionReferenceProxy_SWIGUpcast_m0A97EF773998F7E859BBD4CF19031BC01665F06E (void);
// 0x000001B5 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_FirestoreCpp(Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_FirestoreCpp_m6C188A50EE56FC2EAC9F599E69D9B992CD843FD3 (void);
// 0x000001B6 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_FirestoreCpp(Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_FirestoreCpp_m3A63854EE2F267084BB10ECF72812606A7C8A02B (void);
// 0x000001B7 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern void SWIGExceptionHelper_SetPendingApplicationException_m7FFFCE61B08FE17B5B6C8DD589E8D2BA7E514700 (void);
// 0x000001B8 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern void SWIGExceptionHelper_SetPendingArithmeticException_m4A492793829E24826915E01F0C17FE0B8499A7B9 (void);
// 0x000001B9 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern void SWIGExceptionHelper_SetPendingDivideByZeroException_mD7DFEB207289EFD4D916BAE54302503294B3A469 (void);
// 0x000001BA System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m20AF827963A7C8B753AF68164B7D776C15A67DE9 (void);
// 0x000001BB System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidCastException_m0581C3F4931A20BD5ED4EA66093DDDD6A906C6D6 (void);
// 0x000001BC System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidOperationException_m40B9057C541E1372563BB9E1002E823E38096D62 (void);
// 0x000001BD System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
extern void SWIGExceptionHelper_SetPendingIOException_m8CB7AE7D58F9693F5250EEB08B367E50AF724CDB (void);
// 0x000001BE System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern void SWIGExceptionHelper_SetPendingNullReferenceException_m07F507856FF6AE9838F5CA00AA64F38CBFA2DA84 (void);
// 0x000001BF System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern void SWIGExceptionHelper_SetPendingOutOfMemoryException_m8A1648D61A97D46629D84CB4BF64BE758E3B0987 (void);
// 0x000001C0 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern void SWIGExceptionHelper_SetPendingOverflowException_mB46C4E0EECEBB64698810B5EEBFF2A10B3869D0D (void);
// 0x000001C1 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
extern void SWIGExceptionHelper_SetPendingSystemException_m4504C1471D8E13CC34E2DA16ED245C39DBC5125F (void);
// 0x000001C2 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentException_m03827C4D6C68D5769B0ECF89FBA487FE09CACA03 (void);
// 0x000001C3 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentNullException_m47A1AC704DEE8080F51803DC7FED941B2C51D7DE (void);
// 0x000001C4 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mF2C7DEC5C75D945A3FF2025EFD9E51FD4961ED8D (void);
// 0x000001C5 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::.cctor()
extern void SWIGExceptionHelper__cctor_m189D14C73558E34D3719096B0D4335B10CEE1C4E (void);
// 0x000001C6 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::.ctor()
extern void SWIGExceptionHelper__ctor_mCF6B0992CAA9023285904906FDC292208787FC9B (void);
// 0x000001C7 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionDelegate__ctor_mC595F515BC47D7AC79D539D68D4E366B1420200B (void);
// 0x000001C8 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
extern void ExceptionDelegate_Invoke_m1B61EF0B01C5CF7A2D07F258577754EF3B1B2955 (void);
// 0x000001C9 System.IAsyncResult Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ExceptionDelegate_BeginInvoke_m8E08D4A6A75414295FEFC0F4265928E3E482FF5B (void);
// 0x000001CA System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate::EndInvoke(System.IAsyncResult)
extern void ExceptionDelegate_EndInvoke_m8EDAF282C57579DE0CAF3A515DEB876CB2E74BFD (void);
// 0x000001CB System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionArgumentDelegate__ctor_m9F01D2ABFC0C00CE898B0B221743087D8F4B8752 (void);
// 0x000001CC System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern void ExceptionArgumentDelegate_Invoke_mBC598720C145A95896A58C22713E2B9F2ED6050A (void);
// 0x000001CD System.IAsyncResult Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void ExceptionArgumentDelegate_BeginInvoke_m45175AFBC511D24A5015409670372324C42A4F98 (void);
// 0x000001CE System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::EndInvoke(System.IAsyncResult)
extern void ExceptionArgumentDelegate_EndInvoke_m0F9C89D47A8F7BB1E8A9B04B2406042727917EFF (void);
// 0x000001CF System.Boolean Firebase.Firestore.FirestoreCppPINVOKE/SWIGPendingException::get_Pending()
extern void SWIGPendingException_get_Pending_mF84F3FBA5605BA67D765478A98C671E8FB4C6E87 (void);
// 0x000001D0 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGPendingException::Set(System.Exception)
extern void SWIGPendingException_Set_m30BF2B418CABEA624325DEB0DF73C3023E52A25E (void);
// 0x000001D1 System.Exception Firebase.Firestore.FirestoreCppPINVOKE/SWIGPendingException::Retrieve()
extern void SWIGPendingException_Retrieve_mDA2A1495F86E94474F6E963157D3F20448706633 (void);
// 0x000001D2 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGPendingException::.cctor()
extern void SWIGPendingException__cctor_mE9E4DFDB9BC44EBE23148572E74A8B7C78965F10 (void);
// 0x000001D3 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_FirestoreCpp(Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern void SWIGStringHelper_SWIGRegisterStringCallback_FirestoreCpp_mEA29157BC87D49CF0FB829D4A2736FCBB22F5046 (void);
// 0x000001D4 System.String Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper::CreateString(System.String)
extern void SWIGStringHelper_CreateString_m3D08489776631E77C6252B713D92331E5D1D685E (void);
// 0x000001D5 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper::.cctor()
extern void SWIGStringHelper__cctor_m6BE5D3DCEA342224000DE1EE7230BFEFE264DCDE (void);
// 0x000001D6 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper::.ctor()
extern void SWIGStringHelper__ctor_mF0099E228DAD2091EAA8FBAE553F3DD5E3BFB641 (void);
// 0x000001D7 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIGStringDelegate__ctor_m7CC2D5A8488224BFE6019D12B2ED1F6197DB4800 (void);
// 0x000001D8 System.String Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
extern void SWIGStringDelegate_Invoke_mAA5463CA6488C5FE2EFCE914340A1DDA4669C86F (void);
// 0x000001D9 System.IAsyncResult Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper/SWIGStringDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void SWIGStringDelegate_BeginInvoke_m0352E605D4C0F4C9A4894DEF06A1A944A9263BFB (void);
// 0x000001DA System.String Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper/SWIGStringDelegate::EndInvoke(System.IAsyncResult)
extern void SWIGStringDelegate_EndInvoke_m6B7E1EF3C9F52C8B03B36D44A41AED488D8DDF0D (void);
// 0x000001DB System.Void Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper::FirestoreExceptionRegisterCallback(Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper/FirestoreExceptionDelegate)
extern void FirestoreExceptionHelper_FirestoreExceptionRegisterCallback_m025EEE3989AC1913A21D0100039CF8A94E291F79 (void);
// 0x000001DC System.Void Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper::SetPendingFirestoreException(System.String)
extern void FirestoreExceptionHelper_SetPendingFirestoreException_m0CBBB27537ABEEC0933F1407AFE213BCE003DD6C (void);
// 0x000001DD System.Void Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper::.cctor()
extern void FirestoreExceptionHelper__cctor_m32635013ED9B8A36A1F107FAFDFA5D1FAC246FE8 (void);
// 0x000001DE System.Void Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper::.ctor()
extern void FirestoreExceptionHelper__ctor_m2EE7241825B5CE1470EB6F1D28B4011DEFD41E54 (void);
// 0x000001DF System.Void Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper/FirestoreExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern void FirestoreExceptionDelegate__ctor_m168141C217760A5660EFCFCF4D8A8ED4BB544D69 (void);
// 0x000001E0 System.Void Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper/FirestoreExceptionDelegate::Invoke(System.String)
extern void FirestoreExceptionDelegate_Invoke_m063633C86AC571797B674240A8E0762D57FC2CCD (void);
// 0x000001E1 System.IAsyncResult Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper/FirestoreExceptionDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void FirestoreExceptionDelegate_BeginInvoke_m7DAB8A094C45A2ACAB4A2F5BF8A96752647070EA (void);
// 0x000001E2 System.Void Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper/FirestoreExceptionDelegate::EndInvoke(System.IAsyncResult)
extern void FirestoreExceptionDelegate_EndInvoke_m39C54A86CB450941E5E7D4111B22340DC2F0DA79 (void);
// 0x000001E3 System.Boolean Firebase.Firestore.FirestoreCpp::QueryEquals(Firebase.Firestore.QueryProxy,Firebase.Firestore.QueryProxy)
extern void FirestoreCpp_QueryEquals_m1A1D102131F563EC9CC4700EDD0838D757178C23 (void);
// 0x000001E4 System.Boolean Firebase.Firestore.FirestoreCpp::QuerySnapshotEquals(Firebase.Firestore.QuerySnapshotProxy,Firebase.Firestore.QuerySnapshotProxy)
extern void FirestoreCpp_QuerySnapshotEquals_m9F0C3443E8D422704ED11DE305F5B1C4E6427C10 (void);
// 0x000001E5 System.Boolean Firebase.Firestore.FirestoreCpp::DocumentSnapshotEquals(Firebase.Firestore.DocumentSnapshotProxy,Firebase.Firestore.DocumentSnapshotProxy)
extern void FirestoreCpp_DocumentSnapshotEquals_mBB53A41FEBFB0142EE997A066D93D7A4707FDFF8 (void);
// 0x000001E6 System.Int32 Firebase.Firestore.FirestoreCpp::QueryHashCode(Firebase.Firestore.QueryProxy)
extern void FirestoreCpp_QueryHashCode_m3F7607E088B1E8B9E04549EAE32083BECD3B743B (void);
// 0x000001E7 System.Int32 Firebase.Firestore.FirestoreCpp::QuerySnapshotHashCode(Firebase.Firestore.QuerySnapshotProxy)
extern void FirestoreCpp_QuerySnapshotHashCode_m39EE1BCCCFCC91EAB232133B61545C5D96BEBBA8 (void);
// 0x000001E8 System.Int32 Firebase.Firestore.FirestoreCpp::DocumentSnapshotHashCode(Firebase.Firestore.DocumentSnapshotProxy)
extern void FirestoreCpp_DocumentSnapshotHashCode_mABD0453A1F70705840C56492C9E95E238BF7CC0F (void);
// 0x000001E9 Firebase.Firestore.FieldToValueMap Firebase.Firestore.FirestoreCpp::ConvertFieldValueToMap(Firebase.Firestore.FieldValueProxy)
extern void FirestoreCpp_ConvertFieldValueToMap_m87B16DA1743A3E70FAE879815B5C667BC99ED6A3 (void);
// 0x000001EA Firebase.Firestore.FieldValueProxy Firebase.Firestore.FirestoreCpp::ConvertSnapshotToFieldValue(Firebase.Firestore.DocumentSnapshotProxy,Firebase.Firestore.DocumentSnapshotProxy/ServerTimestampBehavior)
extern void FirestoreCpp_ConvertSnapshotToFieldValue_m00FA1F0927CA04092C0C62690A238E7AFFB56BAF (void);
// 0x000001EB Firebase.Firestore.FieldValueVector Firebase.Firestore.FirestoreCpp::ConvertFieldValueToVector(Firebase.Firestore.FieldValueProxy)
extern void FirestoreCpp_ConvertFieldValueToVector_m4514465B7F9155D3B51C6F3D3453B55157FA4854 (void);
// 0x000001EC Firebase.Firestore.DocumentSnapshotVector Firebase.Firestore.FirestoreCpp::QuerySnapshotDocuments(Firebase.Firestore.QuerySnapshotProxy)
extern void FirestoreCpp_QuerySnapshotDocuments_m928FF10F9321799B36879878D733E3F9D4961616 (void);
// 0x000001ED System.Void Firebase.Firestore.SWIGTYPE_p_unsigned_char::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_unsigned_char__ctor_mC25543DA29742BBE978FE9EE814672ED040E614D (void);
// 0x000001EE System.Runtime.InteropServices.HandleRef Firebase.Firestore.SWIGTYPE_p_unsigned_char::getCPtr(Firebase.Firestore.SWIGTYPE_p_unsigned_char)
extern void SWIGTYPE_p_unsigned_char_getCPtr_m262767A369063D7DC1BFE0D3D778F5B9AADF63F7 (void);
// 0x000001EF System.Void Firebase.Firestore.Internal.AssertFailedException::.ctor(System.String)
extern void AssertFailedException__ctor_m05D002A26B7FE659617A48D630A420E4CB3B8665 (void);
// 0x000001F0 Firebase.Firestore.SourceProxy Firebase.Firestore.Internal.Enums::Convert(Firebase.Firestore.Source)
extern void Enums_Convert_mBDD60DF763EE06A7DB84D60E8E7A0DE2BC92C462 (void);
// 0x000001F1 System.String Firebase.Firestore.Internal.EnvironmentVersion::GetEnvironmentVersion()
extern void EnvironmentVersion_GetEnvironmentVersion_m485B071412F808C11BC687906C4AF4CF285B7ABC (void);
// 0x000001F2 System.String Firebase.Firestore.Internal.EnvironmentVersion::FormatVersion(System.Version)
extern void EnvironmentVersion_FormatVersion_mBE8DB88F6A5AA04F8967BE4E1E50B4654F74A39D (void);
// 0x000001F3 System.Int32 Firebase.Firestore.Internal.Hash::LongHash(System.Int64)
extern void Hash_LongHash_m46C175F7C083D3A28EDCF4B5A072177DA00BC164 (void);
// 0x000001F4 System.Int32 Firebase.Firestore.Internal.Hash::DoubleBitwiseHash(System.Double)
extern void Hash_DoubleBitwiseHash_mC1E113DBAA724DABCB8DACBC4648B1A26741F09D (void);
// 0x000001F5 T Firebase.Firestore.Internal.Preconditions::CheckNotNull(T,System.String)
// 0x000001F6 System.String Firebase.Firestore.Internal.Preconditions::CheckNotNullOrEmpty(System.String,System.String)
extern void Preconditions_CheckNotNullOrEmpty_m8DDC1D772B15915D545E6B76827F48392480D64F (void);
// 0x000001F7 System.Void Firebase.Firestore.Internal.Preconditions::CheckState(System.Boolean,System.String)
extern void Preconditions_CheckState_m2C52BED7317286BF6049FF703D71F9EC93EEC919 (void);
// 0x000001F8 System.Void Firebase.Firestore.Internal.Preconditions::CheckState(System.Boolean,System.String,T)
// 0x000001F9 System.Void Firebase.Firestore.Internal.Preconditions::CheckState(System.Boolean,System.String,T1,T2)
// 0x000001FA System.Void Firebase.Firestore.Internal.Preconditions::CheckState(System.Boolean,System.String,T1,T2,T3)
// 0x000001FB System.Void Firebase.Firestore.Internal.Preconditions::CheckArgument(System.Boolean,System.String,System.String)
extern void Preconditions_CheckArgument_m18F7F58C411622F2A035052AB6CD56EFA519C455 (void);
// 0x000001FC System.Void Firebase.Firestore.Internal.Util::Unreachable()
extern void Util_Unreachable_m0AC7DAE43F9C84B867459337D2CE9326B4C8885B (void);
// 0x000001FD System.Void Firebase.Firestore.Internal.Util::HardAssert(System.Boolean,System.String)
extern void Util_HardAssert_mA8CD1B90D807DF3A26D045E90CB8268A38C95CEE (void);
// 0x000001FE T Firebase.Firestore.Internal.Util::NotNull(T,System.String)
// 0x000001FF System.Threading.Tasks.Task`1<U> Firebase.Firestore.Internal.Util::MapResult(System.Threading.Tasks.Task`1<T>,System.Func`2<T,U>)
// 0x00000200 System.Void Firebase.Firestore.Internal.Util::FlattenAndThrowException(System.Threading.Tasks.Task)
extern void Util_FlattenAndThrowException_mB884FECD5C9962C88EC525348837E234E639CFBF (void);
// 0x00000201 System.Exception Firebase.Firestore.Internal.Util::FlattenException(System.AggregateException)
extern void Util_FlattenException_mE558EB9316C1B1B919C7C5C94097CA8CBD15579C (void);
// 0x00000202 System.Void Firebase.Firestore.Internal.Util::OnPInvokeManagedException(System.Exception,System.String)
extern void Util_OnPInvokeManagedException_m304890FF768577EF1024FC0C35033BFDE62BFE4E (void);
// 0x00000203 System.Void Firebase.Firestore.Internal.Util/<>c__DisplayClass5_0`2::.ctor()
// 0x00000204 U Firebase.Firestore.Internal.Util/<>c__DisplayClass5_0`2::<MapResult>b__0(System.Threading.Tasks.Task`1<T>)
// 0x00000205 System.Void Firebase.Firestore.Converters.AnonymousTypeConverter::.ctor(System.Type)
extern void AnonymousTypeConverter__ctor_m7DCAC02FE85E120F046D3E0CBFD087604F980005 (void);
// 0x00000206 System.Void Firebase.Firestore.Converters.ArrayConverter::.ctor(System.Type)
extern void ArrayConverter__ctor_mA1313A1815E38AD2C3529C5A4BC32E3D4636DE49 (void);
// 0x00000207 System.Object Firebase.Firestore.Converters.ArrayConverter::DeserializeArray(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
extern void ArrayConverter_DeserializeArray_m49DFAEF6D2EF4866AF17897554F59DF09AA29A90 (void);
// 0x00000208 System.Void Firebase.Firestore.Converters.AttributedIdAssigner::.ctor(System.Collections.Generic.List`1<System.Reflection.PropertyInfo>,System.Collections.Generic.List`1<System.Reflection.PropertyInfo>)
extern void AttributedIdAssigner__ctor_m845A3F17677D200F097676153BF6C4803FD855D5 (void);
// 0x00000209 System.Void Firebase.Firestore.Converters.AttributedIdAssigner::AssignId(System.Object,Firebase.Firestore.DocumentReference)
extern void AttributedIdAssigner_AssignId_mB7E049C4FEB669301E3148923A83392D2860CD78 (void);
// 0x0000020A System.Void Firebase.Firestore.Converters.AttributedIdAssigner::MaybeAssignId(System.Object,Firebase.Firestore.DocumentReference)
extern void AttributedIdAssigner_MaybeAssignId_mB3118783949435F16240AE6AAB5C19DB1BEF3E08 (void);
// 0x0000020B Firebase.Firestore.Converters.AttributedIdAssigner Firebase.Firestore.Converters.AttributedIdAssigner::MaybeCreateAssigner(System.Type)
extern void AttributedIdAssigner_MaybeCreateAssigner_mEE7AE4DEDAB6DCB7CB78F73AE8E408BFBA0B789D (void);
// 0x0000020C System.Void Firebase.Firestore.Converters.AttributedIdAssigner::.cctor()
extern void AttributedIdAssigner__cctor_mF4AEB637CAF60D08C64E703DB4680A7630195089 (void);
// 0x0000020D System.Void Firebase.Firestore.Converters.AttributedTypeConverter::.ctor(System.Type,Firebase.Firestore.FirestoreDataAttribute)
extern void AttributedTypeConverter__ctor_mBF292B7ED4199739AEC6E40FF32121C951B8253D (void);
// 0x0000020E System.Func`1<System.Object> Firebase.Firestore.Converters.AttributedTypeConverter::CreateObjectCreator(System.Type)
extern void AttributedTypeConverter_CreateObjectCreator_mED2F455222F2096D3A565C71D39825A7A7DFB124 (void);
// 0x0000020F Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.Converters.AttributedTypeConverter::ForType(System.Type)
extern void AttributedTypeConverter_ForType_m09327601549BD65EEAB5D2B9441EDE3768963269 (void);
// 0x00000210 System.Object Firebase.Firestore.Converters.AttributedTypeConverter::DeserializeMap(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
extern void AttributedTypeConverter_DeserializeMap_m97C053D1B161037B03F803CAE08D5F5FAC51F1D8 (void);
// 0x00000211 System.Boolean Firebase.Firestore.Converters.AttributedTypeConverter/AttributedProperty::get_CanRead()
extern void AttributedProperty_get_CanRead_m9069D0364513C6A5A39B7D17E777EF0EECFBC179 (void);
// 0x00000212 System.Boolean Firebase.Firestore.Converters.AttributedTypeConverter/AttributedProperty::get_CanWrite()
extern void AttributedProperty_get_CanWrite_mAAE46AA315D53C7E28EC64E1F95D9369E1296637 (void);
// 0x00000213 System.Boolean Firebase.Firestore.Converters.AttributedTypeConverter/AttributedProperty::get_IsNullableValue()
extern void AttributedProperty_get_IsNullableValue_m9B85984E806381E63B62F75336CA45D5A3414EF2 (void);
// 0x00000214 System.Void Firebase.Firestore.Converters.AttributedTypeConverter/AttributedProperty::.ctor(System.Reflection.PropertyInfo,Firebase.Firestore.FirestorePropertyAttribute)
extern void AttributedProperty__ctor_mA6AEC934683566B0D0805CBC7F5BEEECE4B4CE2C (void);
// 0x00000215 System.Void Firebase.Firestore.Converters.AttributedTypeConverter/AttributedProperty::SetValue(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy,System.Object)
extern void AttributedProperty_SetValue_m0F2898647DB8E1DACB8230C160A60ED8DFF0466D (void);
// 0x00000216 System.Void Firebase.Firestore.Converters.AttributedTypeConverter/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mFD9F7A1AD5FCF206816968696E1A5D7394846E22 (void);
// 0x00000217 System.Boolean Firebase.Firestore.Converters.AttributedTypeConverter/<>c__DisplayClass4_0::<.ctor>b__0(Firebase.Firestore.Converters.AttributedTypeConverter/AttributedProperty)
extern void U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__0_mEB7907452D39AA14013247B77E3FF81E124D5BAA (void);
// 0x00000218 System.Void Firebase.Firestore.Converters.AttributedTypeConverter/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m8D43B2649D32E27E0EF257F443F2AE7550AD6578 (void);
// 0x00000219 System.Object Firebase.Firestore.Converters.AttributedTypeConverter/<>c__DisplayClass5_0::<CreateObjectCreator>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CCreateObjectCreatorU3Eb__0_m3B94C156F95E8379DBB7803C74F0C581D960FF0C (void);
// 0x0000021A System.Void Firebase.Firestore.Converters.AttributedTypeConverter/<>c__DisplayClass5_1::.ctor()
extern void U3CU3Ec__DisplayClass5_1__ctor_m79176D35335E95E4E2E4BB4F617FE2169111DF96 (void);
// 0x0000021B System.Object Firebase.Firestore.Converters.AttributedTypeConverter/<>c__DisplayClass5_1::<CreateObjectCreator>b__2()
extern void U3CU3Ec__DisplayClass5_1_U3CCreateObjectCreatorU3Eb__2_mC3D03145E21BD648150E70B7FE9133CC2AEC0885 (void);
// 0x0000021C System.Void Firebase.Firestore.Converters.AttributedTypeConverter/<>c::.cctor()
extern void U3CU3Ec__cctor_m8DBF73C83323708E2FDBBD70A4987B27C01A92BD (void);
// 0x0000021D System.Void Firebase.Firestore.Converters.AttributedTypeConverter/<>c::.ctor()
extern void U3CU3Ec__ctor_mF8365A90F41A2A920A70CE536A7CA8C6FBCF27F9 (void);
// 0x0000021E System.Boolean Firebase.Firestore.Converters.AttributedTypeConverter/<>c::<CreateObjectCreator>b__5_1(System.Reflection.ConstructorInfo)
extern void U3CU3Ec_U3CCreateObjectCreatorU3Eb__5_1_m5A927C00BB3A2852B858C99CFF04267DEE31313B (void);
// 0x0000021F System.Void Firebase.Firestore.Converters.ConverterBase::.ctor(System.Type)
extern void ConverterBase__ctor_m97C0396E9FFFE6250D5AFB45CFA362D834AF2272 (void);
// 0x00000220 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeMap(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
extern void ConverterBase_DeserializeMap_m66826A0AD40B18EC9415EC1FC64B575191225916 (void);
// 0x00000221 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeValue(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
extern void ConverterBase_DeserializeValue_m00C88DE91D5DCF03E2EBEB2A09B2B3CFE5021EE4 (void);
// 0x00000222 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeArray(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
extern void ConverterBase_DeserializeArray_m6BC3F3E7C9301E5AB7C52FEE5C4920B8608B64E6 (void);
// 0x00000223 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeBoolean(Firebase.Firestore.DeserializationContext,System.Boolean)
extern void ConverterBase_DeserializeBoolean_m10CF93FCE6FB9ECF4BFB211AC1B302BF804DD435 (void);
// 0x00000224 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeBytes(Firebase.Firestore.DeserializationContext,System.Byte[])
extern void ConverterBase_DeserializeBytes_m6167EDD3E924B01672B5EE7F551CC90C470BC534 (void);
// 0x00000225 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeDouble(Firebase.Firestore.DeserializationContext,System.Double)
extern void ConverterBase_DeserializeDouble_m9962EB90FBEAA78B4AA5CF9F61151915DC31E989 (void);
// 0x00000226 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeGeoPoint(Firebase.Firestore.DeserializationContext,Firebase.Firestore.GeoPoint)
extern void ConverterBase_DeserializeGeoPoint_mD198047E195817DE2D87916F6FF7918085C7B26A (void);
// 0x00000227 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void ConverterBase_DeserializeInteger_mD309DFC20C8C98EA26213330E0DB8B4D5C1626B0 (void);
// 0x00000228 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeReference(Firebase.Firestore.DeserializationContext,Firebase.Firestore.DocumentReference)
extern void ConverterBase_DeserializeReference_mBB585F5386F19A2338FD5FEADE9A26F7D284FEAE (void);
// 0x00000229 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeString(Firebase.Firestore.DeserializationContext,System.String)
extern void ConverterBase_DeserializeString_m18B989E649017A4A8B04FCAF6B08EF35BC86B554 (void);
// 0x0000022A System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeTimestamp(Firebase.Firestore.DeserializationContext,Firebase.Firestore.Timestamp)
extern void ConverterBase_DeserializeTimestamp_m9CAB74E5E801EF4A678020083FEC4C6A59B4C169 (void);
// 0x0000022B System.Byte[] Firebase.Firestore.Converters.ConverterBase::ConvertFromProxyBlob(Firebase.Firestore.FieldValueProxy)
extern void ConverterBase_ConvertFromProxyBlob_m85813B2A63FB4C5DD17701C0D9A8EC67C18131CA (void);
// 0x0000022C Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.Converters.ConverterCache::GetConverter(System.Type)
extern void ConverterCache_GetConverter_mFA8FD20B24A60E770780BF20FAB0C14501A1DD04 (void);
// 0x0000022D System.Void Firebase.Firestore.Converters.ConverterCache::InitializeConverterCache()
extern void ConverterCache_InitializeConverterCache_m42AB202740B9CD255E00701B0E66E08FCC934419 (void);
// 0x0000022E Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.Converters.ConverterCache::CreateDictionaryConverter(System.Type)
// 0x0000022F Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.Converters.ConverterCache::CreateConverter(System.Type)
extern void ConverterCache_CreateConverter_m702436996235BC0AA4AD27B270A814CE861B9215 (void);
// 0x00000230 System.Boolean Firebase.Firestore.Converters.ConverterCache::TryGetStringDictionaryValueType(System.Type,System.Type&)
extern void ConverterCache_TryGetStringDictionaryValueType_m59C6555BF1324FD266E6314EDF82F4E5E4F78ADD (void);
// 0x00000231 System.Type Firebase.Firestore.Converters.ConverterCache::MapInterfaceToDictionaryValueTypeArgument(System.Type)
extern void ConverterCache_MapInterfaceToDictionaryValueTypeArgument_m43AC3ADC961F50F3693674AFB270586E92260A44 (void);
// 0x00000232 System.Void Firebase.Firestore.Converters.ConverterCache::.cctor()
extern void ConverterCache__cctor_m9040033FF723399492820D23E96AFD108939D54D (void);
// 0x00000233 System.Void Firebase.Firestore.Converters.ConverterCache/<>c::.cctor()
extern void U3CU3Ec__cctor_m23FC8EC2ADC406520637A02A65D453FCAF53C3E0 (void);
// 0x00000234 System.Void Firebase.Firestore.Converters.ConverterCache/<>c::.ctor()
extern void U3CU3Ec__ctor_mD53255F31383902CB5E33E6BD59EAA8A966A3BEB (void);
// 0x00000235 System.Boolean Firebase.Firestore.Converters.ConverterCache/<>c::<TryGetStringDictionaryValueType>b__6_0(System.Type)
extern void U3CU3Ec_U3CTryGetStringDictionaryValueTypeU3Eb__6_0_mFE81D3AA4C3BA8314ABE02D7AB3D1D6671987156 (void);
// 0x00000236 Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.Converters.CustomConverter::ForConverterType(System.Type,System.Type)
extern void CustomConverter_ForConverterType_m7EB6F5B02D57772148A990A4339C5369CDF95775 (void);
// 0x00000237 Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.Converters.CustomConverter::CreateInstance(Firebase.Firestore.FirestoreConverter`1<T>)
// 0x00000238 System.Void Firebase.Firestore.Converters.CustomConverter::.cctor()
extern void CustomConverter__cctor_mDB392684F60F75FF209FFCE45D55BE36AE02DDB5 (void);
// 0x00000239 System.Void Firebase.Firestore.Converters.CustomConverter`1::.ctor(Firebase.Firestore.FirestoreConverter`1<T>)
// 0x0000023A System.Object Firebase.Firestore.Converters.CustomConverter`1::DeserializeValue(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
// 0x0000023B System.Void Firebase.Firestore.Converters.DictionaryConverter`1::.ctor(System.Type)
// 0x0000023C System.Object Firebase.Firestore.Converters.DictionaryConverter`1::DeserializeMap(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
// 0x0000023D System.Void Firebase.Firestore.Converters.EnumConverter::.ctor(System.Type)
extern void EnumConverter__ctor_mD37CFE82084160EF7F8B28F33CD0B06647CE3E7C (void);
// 0x0000023E System.Object Firebase.Firestore.Converters.EnumConverter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void EnumConverter_DeserializeInteger_m3239C09E0A481DE24FF76D8C77A03054010A9368 (void);
// 0x0000023F System.Object Firebase.Firestore.Converters.EnumConverter::Int64ToEnumBaseType(System.Int64)
extern void EnumConverter_Int64ToEnumBaseType_mCD6A4FF021CC4EF06083E49C0AACD4309C588696 (void);
// 0x00000240 System.Void Firebase.Firestore.Converters.EnumerableConverter::.ctor(System.Type)
extern void EnumerableConverter__ctor_mB48815398EF3D3A11F8BBC678428AFC5455B7952 (void);
// 0x00000241 System.Object Firebase.Firestore.Converters.EnumerableConverter::DeserializeArray(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
extern void EnumerableConverter_DeserializeArray_m2FA58C3F2795DE6947A612F01CF2E92ECA033C2E (void);
// 0x00000242 System.Void Firebase.Firestore.Converters.EnumerableConverter/<>c::.cctor()
extern void U3CU3Ec__cctor_mE940390EF3A6ACE9BA732FA6699C02B471095DFD (void);
// 0x00000243 System.Void Firebase.Firestore.Converters.EnumerableConverter/<>c::.ctor()
extern void U3CU3Ec__ctor_m288763E12379E266FE23B1326FA3ED95CB431C85 (void);
// 0x00000244 System.Boolean Firebase.Firestore.Converters.EnumerableConverter/<>c::<.ctor>b__1_0(System.Type)
extern void U3CU3Ec_U3C_ctorU3Eb__1_0_m0A28F878818DE956B76EF57911FA756D87DC8209 (void);
// 0x00000245 System.Void Firebase.Firestore.Converters.EnumerableConverterBase::.ctor(System.Type)
extern void EnumerableConverterBase__ctor_mACF692E41E653590FAA7229986A8007D74BA093D (void);
// 0x00000246 System.Object Firebase.Firestore.Converters.IFirestoreInternalConverter::DeserializeValue(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
// 0x00000247 System.Void Firebase.Firestore.Converters.MapConverterBase::.ctor(System.Type)
extern void MapConverterBase__ctor_mC049B3C208A807A2D36B156346EEDF7023D20318 (void);
// 0x00000248 System.Void Firebase.Firestore.Converters.StringConverter::.ctor()
extern void StringConverter__ctor_m98FE6424F0B19EBBA2C3D4A56395848BFBA0FB8D (void);
// 0x00000249 System.Object Firebase.Firestore.Converters.StringConverter::DeserializeString(Firebase.Firestore.DeserializationContext,System.String)
extern void StringConverter_DeserializeString_m2138230D08D0B337A248C18871EA72DFBF72EAA9 (void);
// 0x0000024A System.Void Firebase.Firestore.Converters.IntegerConverterBase::.ctor(System.Type)
extern void IntegerConverterBase__ctor_mC6D2C4315041A669DC89E2A608585368F03051FE (void);
// 0x0000024B System.Object Firebase.Firestore.Converters.IntegerConverterBase::DeserializeDouble(Firebase.Firestore.DeserializationContext,System.Double)
extern void IntegerConverterBase_DeserializeDouble_mA924A2E16AB9F0B92555D02A77B2DEB54A2771FA (void);
// 0x0000024C System.Void Firebase.Firestore.Converters.ByteConverter::.ctor()
extern void ByteConverter__ctor_mF9792E6D174D89395EE27DDF6043776927EB029F (void);
// 0x0000024D System.Object Firebase.Firestore.Converters.ByteConverter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void ByteConverter_DeserializeInteger_m589094EE35BA48B5092502F5D1F098D7F40C4A80 (void);
// 0x0000024E System.Void Firebase.Firestore.Converters.SByteConverter::.ctor()
extern void SByteConverter__ctor_mB4A9C6F21E4B989A68CF6364A36790D874661359 (void);
// 0x0000024F System.Object Firebase.Firestore.Converters.SByteConverter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void SByteConverter_DeserializeInteger_mAD0CD165E2513E083923CA286F97F975523B9384 (void);
// 0x00000250 System.Void Firebase.Firestore.Converters.Int16Converter::.ctor()
extern void Int16Converter__ctor_m5CCA12D045ADA1DF683861482721BD4355A00923 (void);
// 0x00000251 System.Object Firebase.Firestore.Converters.Int16Converter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void Int16Converter_DeserializeInteger_m6532BE8B668ACD0CBA3BD1043148755FB176A922 (void);
// 0x00000252 System.Void Firebase.Firestore.Converters.UInt16Converter::.ctor()
extern void UInt16Converter__ctor_m04FFBB5129B7794DA366B913ADC966F606729DC3 (void);
// 0x00000253 System.Object Firebase.Firestore.Converters.UInt16Converter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void UInt16Converter_DeserializeInteger_m560F57300825FD654D76C0A37102F296BCB62B76 (void);
// 0x00000254 System.Void Firebase.Firestore.Converters.Int32Converter::.ctor()
extern void Int32Converter__ctor_m04DBC86216C93FB26610A0246031D883DEFB1EDC (void);
// 0x00000255 System.Object Firebase.Firestore.Converters.Int32Converter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void Int32Converter_DeserializeInteger_m1D6467D336A5C6D998A617A69A8F02436161587B (void);
// 0x00000256 System.Void Firebase.Firestore.Converters.UInt32Converter::.ctor()
extern void UInt32Converter__ctor_m28B1715D1D72F564B7F953E4FA109EDE0E050A6D (void);
// 0x00000257 System.Object Firebase.Firestore.Converters.UInt32Converter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void UInt32Converter_DeserializeInteger_m390C8F493F4C82B4101D3EA6A3EAA55719A74561 (void);
// 0x00000258 System.Void Firebase.Firestore.Converters.Int64Converter::.ctor()
extern void Int64Converter__ctor_m83C5877D10DE63CFC459CC095D7A3220412AC141 (void);
// 0x00000259 System.Object Firebase.Firestore.Converters.Int64Converter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void Int64Converter_DeserializeInteger_m29D7C3D54F53506CE4463713B61497D6072DA7B8 (void);
// 0x0000025A System.Void Firebase.Firestore.Converters.UInt64Converter::.ctor()
extern void UInt64Converter__ctor_mA34FFF9980DF663B5A29F3E2A4DCB1F06D8592E8 (void);
// 0x0000025B System.Object Firebase.Firestore.Converters.UInt64Converter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void UInt64Converter_DeserializeInteger_mAE1CFB00A64A65EB6BFA468BDAC8198BB0F4483A (void);
// 0x0000025C System.Void Firebase.Firestore.Converters.SingleConverter::.ctor()
extern void SingleConverter__ctor_m1BEE3FF96828B431A845D1EF4CB0BD80940729C2 (void);
// 0x0000025D System.Object Firebase.Firestore.Converters.SingleConverter::DeserializeDouble(Firebase.Firestore.DeserializationContext,System.Double)
extern void SingleConverter_DeserializeDouble_m4FFDBCF934B47AFE1E2288B63E5AA0FB6C0E38CC (void);
// 0x0000025E System.Object Firebase.Firestore.Converters.SingleConverter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void SingleConverter_DeserializeInteger_m040F04554DDCDEFCE291974441526556AAFAB9B5 (void);
// 0x0000025F System.Void Firebase.Firestore.Converters.DoubleConverter::.ctor()
extern void DoubleConverter__ctor_m79DB1F23037EDAEED1C5481821D5EB179320665A (void);
// 0x00000260 System.Object Firebase.Firestore.Converters.DoubleConverter::DeserializeDouble(Firebase.Firestore.DeserializationContext,System.Double)
extern void DoubleConverter_DeserializeDouble_m86F7A05229B9752892200D1AC7A215B55AD6BDEA (void);
// 0x00000261 System.Object Firebase.Firestore.Converters.DoubleConverter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void DoubleConverter_DeserializeInteger_mE3E4CD79C1DD77FF945D3A71912987F0B0CF5787 (void);
// 0x00000262 System.Void Firebase.Firestore.Converters.BooleanConverter::.ctor()
extern void BooleanConverter__ctor_mA7183CE40E3C6B690175361759D1503EC2E402E9 (void);
// 0x00000263 System.Object Firebase.Firestore.Converters.BooleanConverter::DeserializeBoolean(Firebase.Firestore.DeserializationContext,System.Boolean)
extern void BooleanConverter_DeserializeBoolean_mE5B87DCC3A69F46397DAF6570328F1BD52DDAA89 (void);
// 0x00000264 System.Void Firebase.Firestore.Converters.TimestampConverter::.ctor()
extern void TimestampConverter__ctor_m03864D5954A693D167B5D50FD36BE26EDD6001B2 (void);
// 0x00000265 System.Object Firebase.Firestore.Converters.TimestampConverter::DeserializeTimestamp(Firebase.Firestore.DeserializationContext,Firebase.Firestore.Timestamp)
extern void TimestampConverter_DeserializeTimestamp_m2C44736ECAD84B1588129151AA38CB9D37469291 (void);
// 0x00000266 System.Void Firebase.Firestore.Converters.GeoPointConverter::.ctor()
extern void GeoPointConverter__ctor_m88FED2B6D5AD779C604B4AFB4E87695C10299FE9 (void);
// 0x00000267 System.Object Firebase.Firestore.Converters.GeoPointConverter::DeserializeGeoPoint(Firebase.Firestore.DeserializationContext,Firebase.Firestore.GeoPoint)
extern void GeoPointConverter_DeserializeGeoPoint_mFA52D60151AEE029B1A99162669B815D6BEA4D07 (void);
// 0x00000268 System.Void Firebase.Firestore.Converters.ByteArrayConverter::.ctor()
extern void ByteArrayConverter__ctor_m6AF637922A82D6B3D9183B5259D6789DB2B03D5D (void);
// 0x00000269 System.Object Firebase.Firestore.Converters.ByteArrayConverter::DeserializeBytes(Firebase.Firestore.DeserializationContext,System.Byte[])
extern void ByteArrayConverter_DeserializeBytes_m17BD90A3CB8CAE08F2E683A04D7391A7C005A150 (void);
// 0x0000026A System.Void Firebase.Firestore.Converters.BlobConverter::.ctor()
extern void BlobConverter__ctor_m1C338FCD5CA26ABF85D4CDE0E83D5205EB36FE89 (void);
// 0x0000026B System.Object Firebase.Firestore.Converters.BlobConverter::DeserializeBytes(Firebase.Firestore.DeserializationContext,System.Byte[])
extern void BlobConverter_DeserializeBytes_mDCF82DCA3DEB1643A670D0CE58871F33B6420171 (void);
// 0x0000026C System.Void Firebase.Firestore.Converters.FieldValueProxyConverter::.ctor()
extern void FieldValueProxyConverter__ctor_m36EB0DA5C8D905FB06603CF9B0DAD598A3DF67B4 (void);
// 0x0000026D System.Void Firebase.Firestore.Converters.DateTimeConverter::.ctor()
extern void DateTimeConverter__ctor_m6BB0654F1E1A7B9FE3C6B9CC642486369624B599 (void);
// 0x0000026E System.Object Firebase.Firestore.Converters.DateTimeConverter::DeserializeTimestamp(Firebase.Firestore.DeserializationContext,Firebase.Firestore.Timestamp)
extern void DateTimeConverter_DeserializeTimestamp_m947D35CC004229796E006D09057144E21EBFAE6A (void);
// 0x0000026F System.Void Firebase.Firestore.Converters.DateTimeOffsetConverter::.ctor()
extern void DateTimeOffsetConverter__ctor_m5DB81C2ED157E462FEC06726BF9BF570DA75C109 (void);
// 0x00000270 System.Object Firebase.Firestore.Converters.DateTimeOffsetConverter::DeserializeTimestamp(Firebase.Firestore.DeserializationContext,Firebase.Firestore.Timestamp)
extern void DateTimeOffsetConverter_DeserializeTimestamp_m6252B475710A397DD610DD9B096EE5720033B902 (void);
// 0x00000271 System.Void Firebase.Firestore.Converters.DocumentReferenceConverter::.ctor()
extern void DocumentReferenceConverter__ctor_mA0F99E49DF3314D112A8305CEE3A1C44B45DFA0C (void);
// 0x00000272 System.Object Firebase.Firestore.Converters.DocumentReferenceConverter::DeserializeReference(Firebase.Firestore.DeserializationContext,Firebase.Firestore.DocumentReference)
extern void DocumentReferenceConverter_DeserializeReference_mC9CF79070287A37D7FE257A4CBDA18A02622DF59 (void);
static Il2CppMethodPointer s_methodPointers[626] = 
{
	Blob_get_Length_mCB3B8BACCFDD08FC7B032CE7A19863A877DE1135,
	Blob_CopyFrom_m4F0E073E8BFEC65E2B8AE05910DCE02FE360E9C6,
	Blob_Equals_m40803BA16F983221C401AEABB5B9AFB1A1620147,
	Blob_GetHashCode_mBF1065AEF1A335C0ED4700A81CC9FDB07CBFFFA9,
	Blob_Equals_mF653C3D01DFC846FF67142360E6136E161FF0249,
	Blob_ToString_mAE6F40B4D0AABB2E52D19C6193DE711A82A5AFC9,
	Blob__ctor_mA04D8C7EC8B272C542AC71FE121655E43029C4F6,
	CollectionReference__ctor_mC360AA34295DB9235C94AE6D0495AEFD4A5E957E,
	CollectionReference_get_Proxy_mA7FA26913CC407E3F1CF549731FFE4598DFF881B,
	CollectionReference_get_Path_m09C9FF769AB3C184F2ADDD1A9F65F2629AD358BD,
	CollectionReference_GetHashCode_m4FD3BD88088AA273C6BB6D4AC5E6124A6FFD01C2,
	CollectionReference_Equals_mAFCA2406C867C5AC3D6867C97164AF117BA95DB3,
	CollectionReference_Equals_m06C2249E16BE904A2A8DA08820535F65049ED071,
	CollectionReference_ToString_mB252F8C2EC7E763FB556D4D2E80FCA66DADA51BA,
	DocumentReference__ctor_mDAD350FC1D81AED0B372171FB47A98BE681CA80A,
	DocumentReference_ClearCallbacksForOwner_m9A023552DD1669B590E3BF8806A4E7FB78CA19B2,
	DocumentReference_get_Firestore_mE365F177DC04FB9E9E7D7F918FC2C84AFB1D1DB4,
	DocumentReference_get_Id_m2A0E1E7E784C3E2232049FAD4CBD6D4E7A0A6785,
	DocumentReference_get_Path_m67AB7E0EEFBE6267BBAEE3D477CADD1ED1BF3DD6,
	DocumentReference_Collection_mB02DADDBA697050741B80B7B0776AEA3DC8FBD30,
	DocumentReference_GetHashCode_m462DFAEFB7FB9A8FCB7DCC73604BBE714BAB40C1,
	DocumentReference_Equals_mD3BD6E976B96E1D513EEFCF7A399ADE585A0527F,
	DocumentReference_Equals_m0B6FBE10301F76C704D56759D66AE30F668E3828,
	DocumentReference_ToString_mED5159C8071CA0CC3B2515A6CA738ABBD0478C28,
	DocumentReference_DocumentSnapshotsHandler_mE75173506F68CF4107731634BF380B9BBB7372F1,
	DocumentReference__cctor_mFC588BC5B07E2DE31BF30F50E536B9703A7646DB,
	ListenerDelegate__ctor_m516972B8B90DD780E4E6D8F31CF0CAA8ECC5F921,
	ListenerDelegate_Invoke_mB35599DE6448E57DC4524FF9E88A76DE6AF8CB0A,
	ListenerDelegate_BeginInvoke_mBD5C9943EE77B8020C874D9049BA5CF4C3A016DF,
	ListenerDelegate_EndInvoke_mC33BABED612FD3DEFE57029E45F20CE9D557C65A,
	DocumentSnapshot__ctor_m440123FAE3996CA47C8B55F965511720CE3165A9,
	DocumentSnapshot_get_Reference_m37D37D9FA6C4E8A7E04206BBF02B3528D470D4CF,
	DocumentSnapshot_get_Id_mAF81BEE4FDEF960B4206E37A9C82CBC7FD877912,
	DocumentSnapshot_get_Exists_m54FDAE92E3B7310C4D957DFA23305D492FD976F7,
	DocumentSnapshot_ToDictionary_mC07187063BC9177FDCD693E5558269A00550C0BC,
	NULL,
	NULL,
	NULL,
	NULL,
	DocumentSnapshot_ContainsField_mFC09531510FCF98AC73E5763CF0F65F5473E0BC3,
	DocumentSnapshot_ContainsField_m09735D2ED7DC7D46FE1C22F1E5065BE0FC601204,
	DocumentSnapshot_Equals_m81B787A16028821C55F67343411141EAA9F0BCAF,
	DocumentSnapshot_Equals_m73201CDDDA3D2B89CF99552C522BC6187FC7DC5F,
	DocumentSnapshot_GetHashCode_m6CBCD9427A55E2AE847115AE66F8A85AB18D3B02,
	FieldPath_get_EncodedPath_mEAEF5A0643EDAD66E3A2BAD9D038F877848B767A,
	FieldPath__ctor_mAB9DA3A2F79BC83B7532E07CFBB905D8073754F6,
	FieldPath_FromDotSeparatedString_m9F51824509C27E843A48903BBDA89D530566B702,
	FieldPath_GetCanonicalPath_m12697423A394FBE01E253FD086A12FF6071D09F2,
	FieldPath_IsValidIdentifier_m89736B9F1E3BDF65CCEC9CC1E0F3C645B4CC65BE,
	FieldPath_ToString_mA86C92B511EB8DC55FCCB875B34BA7062FD55064,
	FieldPath_Equals_mB2615B89703A687210B2BF23DD983831F7E8CF78,
	FieldPath_GetHashCode_m9464AE3EAE03CEE1C713440B4283E55E71D9B3EE,
	FieldPath_Equals_mC4441ADA1B09270DF8507E2F8F394DD327F124A6,
	FieldPath_ConvertToProxy_m1CE0CBF5574041CABBD2B20C63BF488D1C1E9C9D,
	FieldPath__cctor_mDD87F2472A6111700D835D969C9C1C74A15820C8,
	U3CU3Ec__cctor_m99B01518EEC1E1754F7131678F70C3559273FAF5,
	U3CU3Ec__ctor_m6F5075DAD3A72A7FB76631F5BF3C9116513BC681,
	U3CU3Ec_U3C_ctorU3Eb__9_0_m2B669814E472D31E1525F0884009508E133A59FF,
	FirebaseFirestore__ctor_m9C62CA4F8015680D272664055C99AAE316C67405,
	FirebaseFirestore_Finalize_m3BE16FC86F39D69102D07B161DB77851A2FD998C,
	FirebaseFirestore_OnAppDisposed_mF2054621ACB198D9DAC900DC85B8C167A0AFA00F,
	FirebaseFirestore_Dispose_mF7B3ADC42322E6C60E0DC75D70E2A53F7CEF5DC4,
	FirebaseFirestore_get_App_mFE20ECBF10BEA00E179B6FD61C12726D333E5FAF,
	FirebaseFirestore_set_App_mE0BD4DEC2C83B9AEA5EDD64A8A03864838A15583,
	FirebaseFirestore_get_DefaultInstance_mCA4E13FD1BEE2DBC48A333C44A79E9B3EFEF42F8,
	FirebaseFirestore_GetInstance_m296C14279E63A0A7E3ADBD75DC9FCB28EA191441,
	FirebaseFirestore_CollectionGroup_mFFDB715CA9460D01E8F14BAD7AC468D010B3F933,
	FirebaseFirestore_SnapshotsInSyncHandler_m9820BBCB59CB6A40F2CF44F87BD7ABD483137FDE,
	FirebaseFirestore_LoadBundleTaskProgressHandler_mDEEE792A4653B34026BC13985E88600A9D3F1393,
	NULL,
	FirebaseFirestore_RemoveSelfFromInstanceCache_mC97952393CD6D6F91D377E9C24B0B0300CE5536D,
	FirebaseFirestore__cctor_mFFADFC6F3390299CE0B1BB9ECDDC6CD48EDF8195,
	SnapshotsInSyncDelegate__ctor_m7241EBE6FFAF08E441FC622EC4516DE80A82398C,
	SnapshotsInSyncDelegate_Invoke_mCF79216E0F2CD8985B9EC68B1423E2CC93DCFCC9,
	SnapshotsInSyncDelegate_BeginInvoke_m7E5437770EEA32E77296AE6F07EEBFF8A4704DBF,
	SnapshotsInSyncDelegate_EndInvoke_m830D027CAF37F86D382317AB2C5476535889E76F,
	LoadBundleTaskProgressDelegate__ctor_m3EE2F66C634BB0A62D27BBE7F5F09CE672161097,
	LoadBundleTaskProgressDelegate_Invoke_m07314483EE7FC580263807C0346D57C793517C33,
	LoadBundleTaskProgressDelegate_BeginInvoke_m46358250B6BC223BE2C18662FA1D1A9F7C00A443,
	LoadBundleTaskProgressDelegate_EndInvoke_mE47B48347054A8B7A26FF25872FFF4CA6E4C9A0D,
	U3CU3Ec__DisplayClass22_0__ctor_m682691CF5F63B885B0B7194ABA2216CCA6F0DEB3,
	U3CU3Ec__DisplayClass22_0_U3CCollectionGroupU3Eb__0_m5C582A53DB94741FD61353B34BDBA5AD457B544D,
	U3CU3Ec__DisplayClass31_0__ctor_mD56CA4506A6DB27D504809E954CB850653F46812,
	U3CU3Ec__DisplayClass31_0_U3CSnapshotsInSyncHandlerU3Eb__0_mD3D31300A41E79DCC2F99CCA9975683523D5E56E,
	FirebaseFirestoreSettings__ctor_m87D06EE2D1AD8DA54D55600F498D84993AF55335,
	FirebaseFirestoreSettings_Dispose_m05D04CF8500D42CDBC9843B2A9803B72E8DD5794,
	FirebaseFirestoreSettings_get_Host_m355D5395BF19006F2B84EDB3B31E751DB3A81864,
	FirebaseFirestoreSettings_get_SslEnabled_m4CF8E28C1A6F636ACE348F4AF3F1FA0F6FE8577D,
	FirebaseFirestoreSettings_get_PersistenceEnabled_m69741B6B8804C0473E3CFE769DCCB6313904BFB6,
	FirebaseFirestoreSettings_get_CacheSizeBytes_m076680BCF68AEE8204FCEEC892DB398566B9E0CA,
	NULL,
	FirebaseFirestoreSettings_EnsureAppliedToFirestoreProxy_mCB18D5B0C23EC325A370A190C31626C539E2453B,
	FirebaseFirestoreSettings_ToString_m29AE336D5FCF1157DF91B8E396B9CFE8302FFEF6,
	FirebaseFirestoreSettings__cctor_m519703A7E6C4AD1A5922A7E6FEA0C3DF9108C3BF,
	FirebaseFirestoreSettings_U3Cget_HostU3Eb__10_0_m32F265210FDA8E14193F1BD78432B9F213B35A31,
	FirebaseFirestoreSettings_U3Cget_SslEnabledU3Eb__13_0_m6F3213B4B51AF8CB72A4A009D83D385CBFA6DF93,
	FirebaseFirestoreSettings_U3Cget_PersistenceEnabledU3Eb__16_0_m215919B05D4739DA6725DBA7355A8B59F00D7203,
	FirebaseFirestoreSettings_U3Cget_CacheSizeBytesU3Eb__19_0_mB5530EB247CF9DC7DB85982AD8F4372208C8911C,
	FirestoreDataAttribute_get_UnknownPropertyHandling_m6F41C10164ECDE6EB41A4AA100A0E5F420D9E9A2,
	FirestoreDataAttribute_get_ConverterType_m2A28EAEB06F3AABC3A9972DEAE91C40E5AD55207,
	FirestoreException__ctor_m054F3489BFADDC539275DE1B03ACF8BA7EAF3E3D,
	FirestoreException__ctor_m5C94F368827F7FA0624CA0CFAAFFBBCEEF862944,
	FirestoreException_set_ErrorCode_mB0AB11FA7E7D8F8A9C60419B9BEB3A6F18634DD6,
	FirestorePropertyAttribute_get_Name_m79E0733F6DCF9A582A3DDAE8DBC2B05B22FE80C1,
	FirestorePropertyAttribute_get_ConverterType_m01F243160236E57CB17ADC2EE8318597FB6511C3,
	GeoPoint_get_Latitude_m8579A0A251B83D4EA9440C8F5DC8903F66A90C64,
	GeoPoint_get_Longitude_m3F0E30D4821DFD38D45FE9F07AE8A6E77CC6FAFD,
	GeoPoint__ctor_m317FEC5570FD35C090273B4370D17B4EE7CEB207,
	GeoPoint_Equals_mCC5F79AFC2260998069ECA5A2E37E8F1DF9F8E67,
	GeoPoint_GetHashCode_mAF353C5026FB2581BAECEB9331D6ECD63915356D,
	GeoPoint_Equals_m3CD83E438DB040259EA1391F4F1ED0742F68A317,
	GeoPoint_ToString_mA5A4F4B5C2236F6D5A26D257379BF36673A06B21,
	GeoPoint_ConvertFromProxy_mE6D8EDE2CAFF46E03B64156326578953F9640FA1,
	LoadBundleTaskProgress__ctor_mAFB44DB1ACB58ED62DD7EA2424468A7C8A8BD664,
	LoadBundleTaskProgress_get_DocumentsLoaded_m6BF970EB086FFA5D68F9B9520C48D60D9234DA1A,
	LoadBundleTaskProgress_set_DocumentsLoaded_m130F01C0AB7E075B49DBDF630670F3FC610CAD0C,
	LoadBundleTaskProgress_get_TotalDocuments_m513E22C6A8D8A79AD1D1B89EACC48EC1310AA654,
	LoadBundleTaskProgress_set_TotalDocuments_m1D9478DCD1B5ACB953F4B4D034779B14486C621D,
	LoadBundleTaskProgress_get_BytesLoaded_m2DFB33B9A86BAF2924709D1E4A2C3F60B37CFD19,
	LoadBundleTaskProgress_set_BytesLoaded_m1CCE09C44FB57F3378F1A689771A3C28A25AD07A,
	LoadBundleTaskProgress_get_TotalBytes_m79CEE08DCB3EAD39E76A0DB5B96089216657C4EE,
	LoadBundleTaskProgress_set_TotalBytes_m93FD8AB0DF78A19EB459193DF297091E554ED7AC,
	LoadBundleTaskProgress_get_State_m8F769764F28E3783E72E51996EB7E87BC19C3C69,
	LoadBundleTaskProgress_set_State_m2786077A3A514CC38685844F0A18D8F5448F0F26,
	LoadBundleTaskProgress_Equals_m433BEA923EEDB72F084E1CFD4220304774B98275,
	LoadBundleTaskProgress_GetHashCode_mFC367643EA25F701314E361FF6137E175B1242CF,
	Query__ctor_mEB2ADC4A6E9AFFE447C9D1F31F36CCFD818852FD,
	Query_ClearCallbacksForOwner_m97EDA1CD84FD1BE2B658CCC2CA251EDF2640DAF2,
	Query_get_Firestore_mC814C09FC2283E8507285FCF53B6B449D84132EB,
	Query_GetSnapshotAsync_m4DCA7247582F0851CB06A9395A52F4E2B5CEE82F,
	Query_Equals_m5284C5936D7613C32BE6220A12E3F664954F7163,
	Query_Equals_mEEBBE0C8E44726E31CFE9E84D0B2C01175EF0151,
	Query_GetHashCode_m0563E67FA2AE859A43D3343C662553257C6ABC87,
	Query_QuerySnapshotsHandler_m063CC24B00233FBA6CC4C2E3D39E731BD8800821,
	Query__cctor_m7476F6E50B304E1A0FE5D8E49B11A84BBDF8A115,
	Query_U3CGetSnapshotAsyncU3Eb__42_0_m1DE8A930153268D1A35C97196F0531E516F24C19,
	ListenerDelegate__ctor_m43866ED56E7128C8146C12F9362EEA538C1EA003,
	ListenerDelegate_Invoke_m25CBB5929C885D49D34F22E8F5BE7D91990998B3,
	ListenerDelegate_BeginInvoke_m083FDD25D5A449FC98CA54551A88A6900D293A00,
	ListenerDelegate_EndInvoke_mF13563B5B2A5FC6D1DC416215CEC13FF5DFF0D74,
	QuerySnapshot__ctor_m7B944833CBA25BC9221D2312F14879EFCB7AF44A,
	QuerySnapshot_get_Documents_m37B48B52758015A41B5A6A234E1AADDA2EF1C931,
	QuerySnapshot_get_Count_m26905C4585CB9FD9B1258C743491DB646CC84978,
	QuerySnapshot_Equals_m79F99EB5F0DF4B410DFF2924C03547F32BE2B0FA,
	QuerySnapshot_Equals_m7209D3F353832DA27C2E144B6CE2615B8DA1CE5F,
	QuerySnapshot_GetHashCode_m7EC56D7A28BBA74F1D6308F0B68BD63EA1AB85B7,
	QuerySnapshot_GetEnumerator_m73650855D40A51E5D3512E90AAA61C06B9982410,
	QuerySnapshot_System_Collections_IEnumerable_GetEnumerator_mA169630FC6BD68F1FE3E70D751923604A3CABF9E,
	QuerySnapshot_LoadDocumentsCached_m0009CC8648134C02B7878605C488DF9E40F6E7CE,
	ServerTimestampBehaviorConverter_ConvertToProxy_mEEA510E99EA91987C2497A71A418C75A4D234F30,
	Timestamp_ToDateTime_mCE43F7D6B36597BD9EB814027BC58604EB50463C,
	Timestamp_ToDateTimeOffset_mE057569E9579185AB1C03D7DC59E0E3B0EB7986E,
	Timestamp_Equals_mC5FB2C7784C4154CD68A065B9C3B2979954D8AA7,
	Timestamp_GetHashCode_m77644F39C766B1A3E3C3C8A8DE9898DAE0DDE115,
	Timestamp_Equals_mE6E5EF21BA260D4719381F9C9171CD1C80A9C350,
	Timestamp_CompareTo_m5E0F72C5F36DC0E3E81C88313619B79D213F920F,
	Timestamp_CompareTo_mAC46B7F62D530BDEB81CAB57230CA93B72B5A14D,
	Timestamp_ToString_m52D6B302274294F4092FE717A7897B06FB88BD22,
	Timestamp__ctor_mE2833EDD86D95B4925817BEE9D4EA8C7A3960F0C,
	Timestamp_ConvertFromProxy_mE19C85E7998F189FF868EF6A5C194E9D0DB8BD81,
	Timestamp__cctor_m5F7DD6186799067260D981E0542699AC3B741724,
	ConverterRegistry_ToConverterDictionary_m4808DE699DEBC029E7429A5245FBE78B37671B48,
	DeserializationContext_get_Firestore_m6B0FAF5AA5BE201010D7C68F4569B91485E766A3,
	DeserializationContext_get_DocumentReference_m4DFEC290691ADFC8BDD5E24DB21149ED65280C6F,
	DeserializationContext__ctor_mEFCDB84903C674450970C8B17E712E2E6BDC73F2,
	DeserializationContext_GetConverter_m47CEDFD4868171BFEF47DE4D7976FF879E592DBC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MonoPInvokeCallbackAttribute__ctor_m252D1BA1415C301DA86545F886ABEF89C3455CFE,
	SerializationContext_get_Default_m0956FECF356BC8E246C6E42D5C8A4ED85B6E55A3,
	SerializationContext__ctor_m25A92660D9E2E40E0AE5A92251AD0C0EC1618874,
	SerializationContext_GetConverter_mB984F5EE0455BF4CFA117386329B8B08FA58C0F0,
	SerializationContext__cctor_m9F632160D25C8C0FA3B739442FAB6F04C5501A01,
	TransactionManager__ctor_m035B33F98C36F472E7FF88AEAE5FCA8EB9F61655,
	TransactionManager_Finalize_mF2F91FB6C47AC4B6454A1E9C6001EE2D866C5954,
	TransactionManager_Dispose_m7C5D2B38A91122D1320508B02C8675DEF087DE4C,
	TransactionManager__cctor_mBEF867B58B1D8075E64D3AE7258CF35E3B1B8C6D,
	ValueDeserializer_Deserialize_m79C973D65BE151BFBFBCD37F1A6BEDB9BE00C475,
	ValueDeserializer_GetTargetType_mAA329A104F405BB2F863F4E04330AD63D5A6FC10,
	GeoPointProxy__ctor_m503FE59503EAEBDAE684E8363F7CFCFFDE3688F5,
	GeoPointProxy_Finalize_m5D1810A18A4FAC7E1B142C2145C88DA93800A27A,
	GeoPointProxy_Dispose_mB4652360A9B30A9A42E3DD9EA94CD369C83B8A21,
	GeoPointProxy_Dispose_mED62560CA8FE528B3F066CFA0917687140DB0F60,
	GeoPointProxy_latitude_mD77BEC5F0193678C898C89416F1662A1A49BE354,
	GeoPointProxy_longitude_m49B75B72326AFB9A846FD018B59208CD34DA2031,
	TimestampProxy__ctor_mA4660CC22801025A8965BA1A993215FBB6E85BD5,
	TimestampProxy_Finalize_mCF761B35246D4CCD0BB2ED4E855DDD96D3A77E86,
	TimestampProxy_Dispose_m8A46682120EDD8C21F249EE707EAD6F7848EF606,
	TimestampProxy_Dispose_mFFD18273ED5195FD49634955E6C1D8F5222437BE,
	TimestampProxy_seconds_m0F2EE61F32833A1BA1675ECAB05FD7CC20B3CBA7,
	TimestampProxy_nanoseconds_m08AEE21A7F9CFA8AA09FA0EAA57D9A7F923A2C82,
	TimestampProxy_ToString_m4952B4CD40DC4C860E71E09A89FC14045B160D18,
	Future_QuerySnapshot__ctor_mDEA54B3B64E61DFF472724C181F3F9455336608E,
	Future_QuerySnapshot_Dispose_m5FCF1C1E08921D680B67F245E27F2AB60E3EB27C,
	Future_QuerySnapshot_GetTask_m66DDC683EF82E646BDB538ABD56B37DC90387EC1,
	Future_QuerySnapshot_ThrowIfDisposed_m2705F53407E210AB76966B4E95BFFC73C7DCAE7A,
	Future_QuerySnapshot_SetOnCompletionCallback_m75637E2782B5A3D3CF3D5451174685B22F788A6A,
	Future_QuerySnapshot_SetCompletionData_mEF8805DF1E200ABA2BFF78A7EDDCE63E0B3BE610,
	Future_QuerySnapshot_SWIG_CompletionDispatcher_m15F1003D3DED0E6AC8CBBD6E50317019574A3596,
	Future_QuerySnapshot_SWIG_OnCompletion_m28863D996259870ECF3CA4F99B4F4E3C68F6DE27,
	Future_QuerySnapshot_SWIG_FreeCompletionData_mB8BB4B764EF8AB036121B46ED916EF50AF947B7A,
	Future_QuerySnapshot_GetResult_mB5451A3D0E569C8B118F719D4829D8BE64F67600,
	Future_QuerySnapshot__cctor_mBDEA7E704BDB9B7EBEC1C5964A061AC2AB6D4E57,
	Action__ctor_m7EF534DDB33940EFCB27C62F9CC0099EEC5C449A,
	Action_Invoke_m89DF1B06F56CAA5C63411739D5737CC7B14899A3,
	Action_BeginInvoke_m7DE528B79101FD5A35532ED4A92E19217F252F11,
	Action_EndInvoke_mDF49D090DD28E94B16D603662716A61178460981,
	SWIG_CompletionDelegate__ctor_m99D201F61D68828B480F1010B48318E4053D3D78,
	SWIG_CompletionDelegate_Invoke_m304CB24BCA0217EFCD7B8430157301A563520954,
	SWIG_CompletionDelegate_BeginInvoke_m8906D7078C669A40F525B3C56C2908F5834816AE,
	SWIG_CompletionDelegate_EndInvoke_m19114113506A682DF91CFC186000628BD046C9CF,
	U3CU3Ec__DisplayClass5_0__ctor_m4774D1F84E0BD959630F97D96156A9A5705B5595,
	U3CU3Ec__DisplayClass5_0_U3CGetTaskU3Eb__0_mE383F7A3496FB0EACA7E95E3E9C43F7797A8C4D8,
	FieldPathProxy__ctor_m551E15BC25F35D5CAD46B4423A894C2C4007D5EB,
	FieldPathProxy_getCPtr_m704AFA24E03C02A34164F62CB898E97066563342,
	FieldPathProxy_Finalize_m4D19B0ABC6DC43C3786D64F025AE7B41C5874171,
	FieldPathProxy_Dispose_m88895AB7067D85F6D41DF9011A5479AC856C960F,
	FieldPathProxy_Dispose_m4989360F4FBE2985E885C9FB0018AD9BC277B10B,
	FieldPathProxy__ctor_m990B19F76EDCD541343F65C2502E1D928881AD7C,
	DocumentReferenceProxy__ctor_mC16BDEB7842F70661F8CA8F1250935A1177AF031,
	DocumentReferenceProxy_Finalize_m4726B63A0533838834D6982ABCEAF32EC8C81D15,
	DocumentReferenceProxy_Dispose_m51F96BFD6E3F7B7A6893AF1BF74114131D14558A,
	DocumentReferenceProxy_Dispose_m25459CB8B570DDB4DD040C924087D2BF3D38BAAC,
	DocumentReferenceProxy_id_m7E403109672DA3100AE2FD3B3C50A777AD94FD2E,
	DocumentReferenceProxy_path_m029F85D7203D96AC23B4F4F5876641445C141A8C,
	DocumentReferenceProxy_Collection_mEB94ED01A8596BAE1E7A25DADC82064165AB36EC,
	DocumentReferenceProxy_is_valid_m35E2AF9EA37631F1FB48F5404E720C1F38CAC66B,
	DocumentSnapshotProxy__ctor_mBAD0F8FE3E90D3DE870F6F5AB349361ED8514D54,
	DocumentSnapshotProxy_getCPtr_mC596F629731827F5601A4CCE082D6BB46EB7CFE7,
	DocumentSnapshotProxy_Finalize_m62E5BFBA5FF7E2497050D719B7C4CF77C73B0C18,
	DocumentSnapshotProxy_Dispose_m4EC7C87930238519CC17E343ADDACE585DD5213D,
	DocumentSnapshotProxy_Dispose_mDBF0D1C3D0BB693D3BDAFF6C729753A51A23DCF7,
	DocumentSnapshotProxy_id_m33FD7F6797324295E546D9FBDF520F4BBC06EBB3,
	DocumentSnapshotProxy_reference_m439DE34728C7DB587F55C2A5FB95DCCB549968B5,
	DocumentSnapshotProxy_exists_m0D38B8E8DEE0DBFDE41CDE8D2D0F640BB35EF970,
	DocumentSnapshotProxy_Get_mD7F98EDDAF252126061424402D5342F819DAA57F,
	DocumentSnapshotProxy_Get_m6A5C488C926D8AA1177A69D703E0DA6D58F133CA,
	FieldValueProxy__ctor_mE828DA30BD0C7E6EE1DA953D0476A4A9E77661F4,
	FieldValueProxy_getCPtr_m3947C9F651BD757FC8AED85AB38AB437DC361548,
	FieldValueProxy_Finalize_m3E90D05962E503EB598B4BEBC99F34FE88498B93,
	FieldValueProxy_Dispose_m95750EE003249AF5D8A18F63EEE8AB4E61740A66,
	FieldValueProxy_Dispose_m37EC6DDC55EE3DA29AB636A1C95DD94FDA78A8CF,
	FieldValueProxy_type_m55A3F37538E22B530641299E3B5505A9A059B59A,
	FieldValueProxy_is_null_m4AA92FD9666242AA45DA660AC2BB8C7A1C3DF1A5,
	FieldValueProxy_boolean_value_mE70FE6A25101476564ED10874B6264D0450ADD85,
	FieldValueProxy_integer_value_mC6B9A8ED23560D552E5D86975F5E34D3D8188C53,
	FieldValueProxy_double_value_mEC74286D7C5EFD0E2F03CFE3483B8719104CAA0C,
	FieldValueProxy_timestamp_value_mAFB838B5DAEB67F815401BF41F15F95C2A60407B,
	FieldValueProxy_string_value_m23A143E31F5C198FB4E9348ED0AC113FB299E9FC,
	FieldValueProxy_blob_value_m3D58D62958553095D100FEF987DCF15E05C467B1,
	FieldValueProxy_blob_size_m0D51A7AD5010F00F3541879E9F617C1D4E8C774E,
	FieldValueProxy_reference_value_mE3570A66A31EB4DD26AAD0CEC6536B30D7587F3A,
	FieldValueProxy_geo_point_value_mE1E1FFFF2D3C200742F9C04369D63767EF1A27B4,
	FieldValueProxy_is_valid_m7FF64B82B8E0D43C9604956E4E3EB04F892E410C,
	FieldValueProxy_ServerTimestamp_m5B07EAB411791549FEF1EF76D55400C916FD9764,
	QueryProxy__ctor_mC23671BBD1432C4BAA366E797F5DE246FBD506F0,
	QueryProxy_getCPtr_m72857AE629E20043696A3A817E9C100DFE47E10C,
	QueryProxy_Finalize_mB9CFA288DD6C60B2AEA691A200080E38BC933CFD,
	QueryProxy_Dispose_m960DD3B08BEDCC048DE5D16F9D6BE5985AD41C6E,
	QueryProxy_Dispose_m35E4DB2E72A0D4993286E5A0920164D0B8A210CE,
	QueryProxy_GetAsync_m605193A42AA193CD332BC4CEB3FD408896FCC46F,
	CollectionReferenceProxy__ctor_mBA68C64A100E06FB7DDB45ECEFF570FC6BA62BD5,
	CollectionReferenceProxy_Dispose_m3CF3C81545003B9E05F7D2D6A5D5FA41389E8EE4,
	CollectionReferenceProxy_path_mDB709F31636C6061CF16E128B033BC653206AEEF,
	QuerySnapshotProxy__ctor_m79F372CBDBD224830BA43ED48C63FBA27870BFAD,
	QuerySnapshotProxy_getCPtr_m86260290881CFD039F214598F2D6C8C89B7D6FF6,
	QuerySnapshotProxy_Finalize_m9884C8D991BE36287C0FCE54FF9EDBE420A182A9,
	QuerySnapshotProxy_Dispose_mE01AC7029ECB65708C684DE1B9CF86DE95E91854,
	QuerySnapshotProxy_Dispose_m6EC845FBC2F8A7E652F63E24F26066FB0EF2B040,
	QuerySnapshotProxy_size_m5F85F760E58C896DEB38068B1EA3C1E0F061272E,
	SettingsProxy__ctor_m57F7B30B9FC12FC9AD8CEB2FF644366ACE08FCD0,
	SettingsProxy_getCPtr_m3F02F7C4D67FB3CA6516EFD760DC730C22C255D2,
	SettingsProxy_Finalize_m8A4DCB9AB82CD5828C13FE34F26FE3C083DE7226,
	SettingsProxy_Dispose_m020A1134948209EE62549E8CCCE9BBEB8C0B8C9E,
	SettingsProxy_Dispose_mFA2E769C190403E9ECB4C81B35827270B784D8D6,
	SettingsProxy__ctor_m1E66E049659C1D5848BA5598F2CD221DCA012C0F,
	SettingsProxy_host_mEA699725ABF182715DCC2B4BBABB6085C43BB5D6,
	SettingsProxy_is_ssl_enabled_mA385B9EC7BE54B2D119A01BFA46F44C6BEE04BDA,
	SettingsProxy_is_persistence_enabled_mD08842A00B90262157A80B8418024D5D73A6BAE6,
	SettingsProxy_cache_size_bytes_mDE0491A0EA8D2DA09281519C3BDA6BAAB761503C,
	SettingsProxy_set_host_m61CA41356D9E2D4E32EFD01B7F989FB06D96646D,
	SettingsProxy_set_ssl_enabled_mF2ACBD4CEDFDBEDB56E9189A083B031725A906B6,
	SettingsProxy_set_persistence_enabled_mAD8328FE1F2707E4508218D1A3FF0769CAFD0D84,
	SettingsProxy_set_cache_size_bytes_mE974897951FB753357D9DFD49B6C1F8F546DBB90,
	SettingsProxy__cctor_mF50600439845272C13761BC7D243D4D650414217,
	LoadBundleTaskProgressProxy__ctor_mBF5E431FF5E258BBF0CD71351DC80E6FF1B8DCD8,
	LoadBundleTaskProgressProxy_Finalize_m5F9E9B3C32964253E34B941FDC9E08037D7CACE5,
	LoadBundleTaskProgressProxy_Dispose_mEC65FDAC5B40DD08897FB7CF1E9EE1A9F45A90FB,
	LoadBundleTaskProgressProxy_Dispose_mD473194165A831BB56B772A654E7C068E3901160,
	LoadBundleTaskProgressProxy_documents_loaded_m552BF9D8F53588F185F6B8809098814DBA65DE2A,
	LoadBundleTaskProgressProxy_total_documents_mD46FF5F4D699E3220B48E3837532C9AA2C610144,
	LoadBundleTaskProgressProxy_bytes_loaded_mD0AE2512A5A34FA2077462A1B86162F1B8E535D6,
	LoadBundleTaskProgressProxy_total_bytes_mE03751426599353CC31437ECE1FCCEE171A60097,
	LoadBundleTaskProgressProxy_state_m95106E703DACA2FD5CD5B303D8BD6EB95C6BAEF3,
	FirestoreProxy__ctor_m4C4FFB2CDFF1E08173F83A7CE70138F7A3D69A7D,
	FirestoreProxy_getCPtr_mDB561C2FF706494DC5B52DABFA4A78A9CA79894D,
	FirestoreProxy_Finalize_mE88DF3B641523CC454028A6B71318A7067FBDB67,
	FirestoreProxy_Dispose_m6678747251CDFEE7CCA821D4A9D7269C62501F46,
	FirestoreProxy_Dispose_m7313A037D25DEF004CA8CFBA522F4318492EAE23,
	FirestoreProxy_GetInstance_m7ABF5D2143A9EBF56328B183FF677791D9C758C8,
	FirestoreProxy_CollectionGroup_m1EC6B3F459B061CB3FA60DBB938C114898033F6A,
	FirestoreProxy_settings_m685CFC1A9E739B2060CCE7178F2F6B9EAA7D8ABA,
	FirestoreProxy_set_settings_m51B39F1D76707F90748B4F42029D368242FC1EFF,
	ApiHeaders_SetClientLanguage_m3AF676E9FF8F6604CAFE0D94C721EBA688CF3488,
	TransactionCallbackProxy_Dispose_m1CA0BFFBDDA685A1DE31BAA7CA252913EC891607,
	TransactionCallbackProxy_Dispose_mBC87D7102F42BC74C2E2CCABDFA42EBAC3525651,
	TransactionManagerProxy__ctor_m53CEB44B8C3BC25F31C48776CA8B408D22F214CD,
	TransactionManagerProxy_Finalize_mFD74B4034FFAA833657FD94DD84F7DDEC820B506,
	TransactionManagerProxy_Dispose_m34EE3E31286A32DE4FD0C85740DFF827B4BE58AA,
	TransactionManagerProxy_Dispose_m52B4D40D02D22B2B87CCC21811810861C91C84CC,
	TransactionManagerProxy__ctor_m1D026BDB5EA6D37C2C7D9B7F7813B4B23353F694,
	TransactionManagerProxy_CppDispose_m806AB05DC32D3CAE9FF86414EC067D5C70204611,
	FieldToValueMap__ctor_m5339A881427D4DA7E3A7F34F24290E189A758919,
	FieldToValueMap_Finalize_mBF86F5DAEBC922A79D819E480CF8AE1A8F959DEC,
	FieldToValueMap_Dispose_mFF45079007FA669B86FD0968E22870EF2E07E225,
	FieldToValueMap_Dispose_m277CCC95581DAB0047DA74E28F2135F93DC2EE5E,
	FieldToValueMap_Iterator_mFEBB720DD557118C1D8623555D7B73EDEDBCB998,
	FieldToValueMapIterator__ctor_mBFE6E50889C6F726BC0933FED3570B26B63A9D2A,
	FieldToValueMapIterator_Finalize_mBCB231C8B6B184D2840E68DE39A468F084BCCF8F,
	FieldToValueMapIterator_Dispose_mBF4E5BA72F53EE1FC039F7477B0EC8562733AF3A,
	FieldToValueMapIterator_Dispose_mCE8AE325CD1A523EE1BD044A85CF691F435EE581,
	FieldToValueMapIterator_HasMore_m189096D902425FDA478A1379E402026304B311EA,
	FieldToValueMapIterator_Advance_mE1DD7B73FB55A3E59C7F4CCB4913559A7FF61216,
	FieldToValueMapIterator_UnsafeKeyView_mA1499884B3EFE247AD50162B0CE7EA24EE244484,
	FieldToValueMapIterator_UnsafeValueView_mCD65EE0367E18C0D55CE4F168CE2BFA6BFB651DE,
	DocumentSnapshotVector__ctor_mF5426DCC89074F309B8CE675E6E99F0B9CF537BA,
	DocumentSnapshotVector_Finalize_m7428D02E1769AD720BE3721B24A42010713D21E8,
	DocumentSnapshotVector_Dispose_mA3AF9504F817D8125D0D9B58E96325DF4315E434,
	DocumentSnapshotVector_Dispose_mD3134B43B36CBEC1BEC989557AF8ED8A05494141,
	DocumentSnapshotVector_GetCopy_m8DDDF5D444CD64042E931C761E6984498FA5D757,
	FieldValueVector__ctor_mE18FA41B6B529487E8688077F80A74189FC62DAE,
	FieldValueVector_Finalize_mAA757ABBCF56EEAA67AA1CFC454A19547BD5A6A7,
	FieldValueVector_Dispose_m6C8D5DA5C3370F4A15EDEF8B7BE26404F906657D,
	FieldValueVector_Dispose_m9A096A9F13355EAE94CCCC5D4066F68EF8857758,
	FieldValueVector_Size_mA9B2314BB8BF28735078B6CFC52B1DFCC423A16F,
	FieldValueVector_GetUnsafeView_m36367447BDCEC4FFF4EC60D99CF124018527AE9E,
	FirestoreCppPINVOKE__cctor_m6A9FB3CEB620709BFDEC4D2D2AF869F3A737B388,
	FirestoreCppPINVOKE_GeoPointProxy_latitude_m537FE7FED53D1454296BD0B9D8547AFD0688E60E,
	FirestoreCppPINVOKE_GeoPointProxy_longitude_m51EF668A6B0BAD251E1FA48CBCF4A6664E38381A,
	FirestoreCppPINVOKE_delete_GeoPointProxy_mED14F013285E6857B0A34B88166A2C4415F76F56,
	FirestoreCppPINVOKE_TimestampProxy_seconds_m4430E089356F673DD4349FCA11F566B1D9A916B9,
	FirestoreCppPINVOKE_TimestampProxy_nanoseconds_m8047B0FF2505A98EFAD4536FD97410C754F85043,
	FirestoreCppPINVOKE_TimestampProxy_ToString_mA91DDA5AB5BF27A022B63D8E914470060FA545FB,
	FirestoreCppPINVOKE_delete_TimestampProxy_mE2426617F833198994E71D4484DAA6C003B3B3BE,
	FirestoreCppPINVOKE_Future_QuerySnapshot_SWIG_OnCompletion_mA33D4D03BEDD8C344FF3FEAD7C67A6FEAC9CDF8F,
	FirestoreCppPINVOKE_Future_QuerySnapshot_SWIG_FreeCompletionData_m99EC9E6390243CD7E9055A74BEB26A55AF0C1BA0,
	FirestoreCppPINVOKE_Future_QuerySnapshot_GetResult_mB282775D2D5EC7A8F3A87A8C18FD43B5C5AE643E,
	FirestoreCppPINVOKE_delete_Future_QuerySnapshot_m15662DAA837429601EB9C89E73C2B2C74417DCE8,
	FirestoreCppPINVOKE_new_FieldPathProxy_m87ABD8A7DD51859E038105AA7C00077B7C0867B8,
	FirestoreCppPINVOKE_delete_FieldPathProxy_mF4084D48638E1119FE695AF732B4F9FE60E1E035,
	FirestoreCppPINVOKE_delete_DocumentReferenceProxy_mA7D5F0F56719E34AE70F4A7A77653AB3E7665797,
	FirestoreCppPINVOKE_DocumentReferenceProxy_id_m4DACDC8AA63C76713D38205D5D8BE7DFB1748650,
	FirestoreCppPINVOKE_DocumentReferenceProxy_path_mBD7874B87080484A379890CA896E4C5886B079FD,
	FirestoreCppPINVOKE_DocumentReferenceProxy_Collection__SWIG_0_mC53C05F2259F229233F521BEEB3A82AD17BB1E50,
	FirestoreCppPINVOKE_DocumentReferenceProxy_is_valid_m4B4AD8FCD88216AC47B32A48BC285F31CCD3DF3C,
	FirestoreCppPINVOKE_delete_DocumentSnapshotProxy_m2708B2FE67CB08FFED03FAB0192AD79DA00BB4E5,
	FirestoreCppPINVOKE_DocumentSnapshotProxy_id_m09A7FBCF9B82F712B84723CAE641D80A5758F166,
	FirestoreCppPINVOKE_DocumentSnapshotProxy_reference_mC7156E7AA6DC01B96EDBD6A9F7BB079DD7ECA26A,
	FirestoreCppPINVOKE_DocumentSnapshotProxy_exists_mC95BD56939C1A4FC06FC89B91D6B3FCCEC6FC9FC,
	FirestoreCppPINVOKE_DocumentSnapshotProxy_Get__SWIG_4_mFBDD0CD92F85722AA7AB5F0605AD6901DC771459,
	FirestoreCppPINVOKE_DocumentSnapshotProxy_Get__SWIG_5_mD460A5D0549A33BFBE4A7FE09B32C13988B6291E,
	FirestoreCppPINVOKE_delete_FieldValueProxy_mD92AF82E60AB6F6A4C2D49E2096E9F1E88A90F00,
	FirestoreCppPINVOKE_FieldValueProxy_type_mE04FEBAB491DA62FDFE1D60B40A63D4CB40866C1,
	FirestoreCppPINVOKE_FieldValueProxy_is_null_m04A661A9AD919338D30C756D48269E3E78849AE5,
	FirestoreCppPINVOKE_FieldValueProxy_boolean_value_m3233D408B22705664486498D57048754D81C4A10,
	FirestoreCppPINVOKE_FieldValueProxy_integer_value_m2A1E2B41577017985C361238AFA93505A2F7675B,
	FirestoreCppPINVOKE_FieldValueProxy_double_value_mA8E38B05E0B4946364AFA010B9BC3A5BC7004CFD,
	FirestoreCppPINVOKE_FieldValueProxy_timestamp_value_m5FC7860EF2A41F6D3282EFF20C3D2CE5F3E1646D,
	FirestoreCppPINVOKE_FieldValueProxy_string_value_m79353C0E5B6E8AD460F06C67DE4AF938942D7AF8,
	FirestoreCppPINVOKE_FieldValueProxy_blob_value_mE1E3AF0ECC594FFA3A4984041C37DB67126F0E15,
	FirestoreCppPINVOKE_FieldValueProxy_blob_size_mFE868920BEF027EDC2E679F15AA91F0EC71E6C98,
	FirestoreCppPINVOKE_FieldValueProxy_reference_value_m952902083B7DDBE69D17538DA03BC2F4874654B3,
	FirestoreCppPINVOKE_FieldValueProxy_geo_point_value_m03094FD58C2D5372153DCB7972FBF468BD408B06,
	FirestoreCppPINVOKE_FieldValueProxy_is_valid_mC9092415AE893E7AC1EDEF7B821E04BD7FFEE320,
	FirestoreCppPINVOKE_FieldValueProxy_ServerTimestamp_mE018EA2B6AFE62FB55A27778AC5BC47DEBE75A86,
	FirestoreCppPINVOKE_delete_QueryProxy_m228C9F5E88FC4664FD78CF6EA74D4058E5102007,
	FirestoreCppPINVOKE_QueryProxy_Get__SWIG_0_m3115B7AAF5848F8FA8E2BA6806FA57874E1C1326,
	FirestoreCppPINVOKE_CollectionReferenceProxy_path_m4521D143BDF4FCE6E3375633F1FDED4271F17747,
	FirestoreCppPINVOKE_delete_CollectionReferenceProxy_mBD43D3610EEAC3B43B0EEE284B3C928AD4C9918F,
	FirestoreCppPINVOKE_delete_QuerySnapshotProxy_m68A5283AE62C854AA4E9ADB6922ABCE81B56063F,
	FirestoreCppPINVOKE_QuerySnapshotProxy_size_m9BFE60EDC71DD4966C1E1914CE64A3CE906AE383,
	FirestoreCppPINVOKE_SettingsProxy_kCacheSizeUnlimited_get_m2F7BBE5038D7E9CAD9980A90B4A6280EC8097D8B,
	FirestoreCppPINVOKE_new_SettingsProxy__SWIG_0_m688BFD940F82F9327983447043CB806EE31C2CCC,
	FirestoreCppPINVOKE_SettingsProxy_host_m6DC84E2B2121422BC6CDADC536354F725D3308A4,
	FirestoreCppPINVOKE_SettingsProxy_is_ssl_enabled_mEC26AA47A593DB3F83D87FAEE765EDDD3D8A55B2,
	FirestoreCppPINVOKE_SettingsProxy_is_persistence_enabled_m4E5291EB27D3090B8304E18D8A56D7F027FF2BBA,
	FirestoreCppPINVOKE_SettingsProxy_cache_size_bytes_m1A8AD60E0A48F636DAE036A3512D18643DBE0AC7,
	FirestoreCppPINVOKE_SettingsProxy_set_host_mC1C913CECCF32040492CE49A5AB2E28A1130341E,
	FirestoreCppPINVOKE_SettingsProxy_set_ssl_enabled_m66282A99FA880FA5451F4EFD713E63710DA0A6C1,
	FirestoreCppPINVOKE_SettingsProxy_set_persistence_enabled_mE19F04D4289F0B3F3CE981E97A5C3D4D0D0AAC86,
	FirestoreCppPINVOKE_SettingsProxy_set_cache_size_bytes_m79399836EC0972A5A0F13C7DD3177083F16970B9,
	FirestoreCppPINVOKE_delete_SettingsProxy_m114100B64B935697B05A2C4C99AF00F809B16C90,
	FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_documents_loaded_m62117737D1CFF5B4654B41F0D386DBF1519E80BB,
	FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_total_documents_m41245AD15C13BE9748C6AAA011A6CF7C6BE793EC,
	FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_bytes_loaded_m587322A1529B00FB674425010392448871F5DB6A,
	FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_total_bytes_mE0A3EA40F724A27030786FCA168F1D94B2BF4220,
	FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_state_mE3E6DA981B84C777411400B9E39395A674ADD9EA,
	FirestoreCppPINVOKE_delete_LoadBundleTaskProgressProxy_mFC3046514D537A92ED7AE9F78E9632DA6F0A6FDF,
	FirestoreCppPINVOKE_FirestoreProxy_GetInstance__SWIG_1_mBD594EF31E9E6E52CF3A5E2A78041A6C8FC7CEB3,
	FirestoreCppPINVOKE_delete_FirestoreProxy_mEF5ACB794EC595C7432D0D59BB365BCA21CAD386,
	FirestoreCppPINVOKE_FirestoreProxy_CollectionGroup__SWIG_0_mCA3399619A837C66D338A08A67FD53EECDF19F48,
	FirestoreCppPINVOKE_FirestoreProxy_settings_m48EBFBF46A828E40260B30E3AA1C597BB1AE2421,
	FirestoreCppPINVOKE_FirestoreProxy_set_settings_mCD052E2C597DE0B3D7841C05A4450241FDD8D4D1,
	FirestoreCppPINVOKE_QueryEquals_m0426DF2ED4809404E969E63310B1BFC0D9B12DFD,
	FirestoreCppPINVOKE_QuerySnapshotEquals_m07F3AC1EC83D3F3D06C1470F7AB5D96F2BE2E89C,
	FirestoreCppPINVOKE_DocumentSnapshotEquals_m460B1847DD65DEFD258374F5B0B175FFA1EFEA08,
	FirestoreCppPINVOKE_QueryHashCode_m908CC15F2024AF939A762C5778B592416E7F1BD1,
	FirestoreCppPINVOKE_QuerySnapshotHashCode_m38F7F4FCDCAA53681C7C87BCA56A94856E70F29C,
	FirestoreCppPINVOKE_DocumentSnapshotHashCode_m4F40AB3D10F3377C37F95974513CD8457C5B34C2,
	FirestoreCppPINVOKE_ApiHeaders_SetClientLanguage_mFE2F038BDCE725BEABEEC65CA0B74472525DC655,
	FirestoreCppPINVOKE_ConvertFieldValueToMap_mBDCA4247A42753FE6E9DBFA711A998092AAB11FE,
	FirestoreCppPINVOKE_ConvertSnapshotToFieldValue_m02CC9A869A6F5693F15497C727F881DD5C1FEFCC,
	FirestoreCppPINVOKE_delete_TransactionCallbackProxy_mE1B8873E8EE0910D456872A34EFC7F15C0E0A6CB,
	FirestoreCppPINVOKE_new_TransactionManagerProxy_mDC63D2A4A583124DA6C703B3BA9C2E3F29CE2083,
	FirestoreCppPINVOKE_delete_TransactionManagerProxy_m1D0BC77C0DFF0CDF09923AA6A9DAF8D1E51459D5,
	FirestoreCppPINVOKE_TransactionManagerProxy_CppDispose_mF2FB0567B4AC38A9A10FFECDA60BE2EE1506CA43,
	FirestoreCppPINVOKE_FieldToValueMap_Iterator_mCFEA49BEA05008313EFFFD915D46C0BA04A37E71,
	FirestoreCppPINVOKE_delete_FieldToValueMap_m5A4456978BA774A8724FEAE557C3998A08074131,
	FirestoreCppPINVOKE_FieldToValueMapIterator_HasMore_mB05288423F66BFF297E56B5EC546CFF9769D4476,
	FirestoreCppPINVOKE_FieldToValueMapIterator_Advance_m2EB53F62915671CAEBA81916F8750DC9EBB52166,
	FirestoreCppPINVOKE_FieldToValueMapIterator_UnsafeKeyView_m5A2D5372B95546681215C51C74BF445AB3873280,
	FirestoreCppPINVOKE_FieldToValueMapIterator_UnsafeValueView_mA0D3E465E2A30789331CC4624505E9B17AF41818,
	FirestoreCppPINVOKE_delete_FieldToValueMapIterator_m1714A53BB7800AE2CA78B443DDF33419BBAE739D,
	FirestoreCppPINVOKE_ConvertFieldValueToVector_mFB518CC2861633D35A31799688BFF008E61C757E,
	FirestoreCppPINVOKE_QuerySnapshotDocuments_mA35B2F5FAE7B778E372CC46A64E1FCA8F237BFB7,
	FirestoreCppPINVOKE_DocumentSnapshotVector_GetCopy_m5445F4BDC462687FB7D1091C0E947EBA3EDA1428,
	FirestoreCppPINVOKE_delete_DocumentSnapshotVector_m670DD105A4E40A1B4285FA2FC6ED4318BA0F22B0,
	FirestoreCppPINVOKE_FieldValueVector_Size_m3D05B8294C969ADF4D4B85D930CCF94E5065E140,
	FirestoreCppPINVOKE_FieldValueVector_GetUnsafeView_m34B063798E9746541015D0C11C06E2341F1A8A74,
	FirestoreCppPINVOKE_delete_FieldValueVector_m0BEEC7CFB160B40F88F72888DF6AFCF67857D3E7,
	FirestoreCppPINVOKE_Future_QuerySnapshot_SWIGUpcast_mB3CDB804C07D71576B8362A3F3DB5E22B1E8FEE6,
	FirestoreCppPINVOKE_CollectionReferenceProxy_SWIGUpcast_m0A97EF773998F7E859BBD4CF19031BC01665F06E,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_FirestoreCpp_m6C188A50EE56FC2EAC9F599E69D9B992CD843FD3,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_FirestoreCpp_m3A63854EE2F267084BB10ECF72812606A7C8A02B,
	SWIGExceptionHelper_SetPendingApplicationException_m7FFFCE61B08FE17B5B6C8DD589E8D2BA7E514700,
	SWIGExceptionHelper_SetPendingArithmeticException_m4A492793829E24826915E01F0C17FE0B8499A7B9,
	SWIGExceptionHelper_SetPendingDivideByZeroException_mD7DFEB207289EFD4D916BAE54302503294B3A469,
	SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m20AF827963A7C8B753AF68164B7D776C15A67DE9,
	SWIGExceptionHelper_SetPendingInvalidCastException_m0581C3F4931A20BD5ED4EA66093DDDD6A906C6D6,
	SWIGExceptionHelper_SetPendingInvalidOperationException_m40B9057C541E1372563BB9E1002E823E38096D62,
	SWIGExceptionHelper_SetPendingIOException_m8CB7AE7D58F9693F5250EEB08B367E50AF724CDB,
	SWIGExceptionHelper_SetPendingNullReferenceException_m07F507856FF6AE9838F5CA00AA64F38CBFA2DA84,
	SWIGExceptionHelper_SetPendingOutOfMemoryException_m8A1648D61A97D46629D84CB4BF64BE758E3B0987,
	SWIGExceptionHelper_SetPendingOverflowException_mB46C4E0EECEBB64698810B5EEBFF2A10B3869D0D,
	SWIGExceptionHelper_SetPendingSystemException_m4504C1471D8E13CC34E2DA16ED245C39DBC5125F,
	SWIGExceptionHelper_SetPendingArgumentException_m03827C4D6C68D5769B0ECF89FBA487FE09CACA03,
	SWIGExceptionHelper_SetPendingArgumentNullException_m47A1AC704DEE8080F51803DC7FED941B2C51D7DE,
	SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mF2C7DEC5C75D945A3FF2025EFD9E51FD4961ED8D,
	SWIGExceptionHelper__cctor_m189D14C73558E34D3719096B0D4335B10CEE1C4E,
	SWIGExceptionHelper__ctor_mCF6B0992CAA9023285904906FDC292208787FC9B,
	ExceptionDelegate__ctor_mC595F515BC47D7AC79D539D68D4E366B1420200B,
	ExceptionDelegate_Invoke_m1B61EF0B01C5CF7A2D07F258577754EF3B1B2955,
	ExceptionDelegate_BeginInvoke_m8E08D4A6A75414295FEFC0F4265928E3E482FF5B,
	ExceptionDelegate_EndInvoke_m8EDAF282C57579DE0CAF3A515DEB876CB2E74BFD,
	ExceptionArgumentDelegate__ctor_m9F01D2ABFC0C00CE898B0B221743087D8F4B8752,
	ExceptionArgumentDelegate_Invoke_mBC598720C145A95896A58C22713E2B9F2ED6050A,
	ExceptionArgumentDelegate_BeginInvoke_m45175AFBC511D24A5015409670372324C42A4F98,
	ExceptionArgumentDelegate_EndInvoke_m0F9C89D47A8F7BB1E8A9B04B2406042727917EFF,
	SWIGPendingException_get_Pending_mF84F3FBA5605BA67D765478A98C671E8FB4C6E87,
	SWIGPendingException_Set_m30BF2B418CABEA624325DEB0DF73C3023E52A25E,
	SWIGPendingException_Retrieve_mDA2A1495F86E94474F6E963157D3F20448706633,
	SWIGPendingException__cctor_mE9E4DFDB9BC44EBE23148572E74A8B7C78965F10,
	SWIGStringHelper_SWIGRegisterStringCallback_FirestoreCpp_mEA29157BC87D49CF0FB829D4A2736FCBB22F5046,
	SWIGStringHelper_CreateString_m3D08489776631E77C6252B713D92331E5D1D685E,
	SWIGStringHelper__cctor_m6BE5D3DCEA342224000DE1EE7230BFEFE264DCDE,
	SWIGStringHelper__ctor_mF0099E228DAD2091EAA8FBAE553F3DD5E3BFB641,
	SWIGStringDelegate__ctor_m7CC2D5A8488224BFE6019D12B2ED1F6197DB4800,
	SWIGStringDelegate_Invoke_mAA5463CA6488C5FE2EFCE914340A1DDA4669C86F,
	SWIGStringDelegate_BeginInvoke_m0352E605D4C0F4C9A4894DEF06A1A944A9263BFB,
	SWIGStringDelegate_EndInvoke_m6B7E1EF3C9F52C8B03B36D44A41AED488D8DDF0D,
	FirestoreExceptionHelper_FirestoreExceptionRegisterCallback_m025EEE3989AC1913A21D0100039CF8A94E291F79,
	FirestoreExceptionHelper_SetPendingFirestoreException_m0CBBB27537ABEEC0933F1407AFE213BCE003DD6C,
	FirestoreExceptionHelper__cctor_m32635013ED9B8A36A1F107FAFDFA5D1FAC246FE8,
	FirestoreExceptionHelper__ctor_m2EE7241825B5CE1470EB6F1D28B4011DEFD41E54,
	FirestoreExceptionDelegate__ctor_m168141C217760A5660EFCFCF4D8A8ED4BB544D69,
	FirestoreExceptionDelegate_Invoke_m063633C86AC571797B674240A8E0762D57FC2CCD,
	FirestoreExceptionDelegate_BeginInvoke_m7DAB8A094C45A2ACAB4A2F5BF8A96752647070EA,
	FirestoreExceptionDelegate_EndInvoke_m39C54A86CB450941E5E7D4111B22340DC2F0DA79,
	FirestoreCpp_QueryEquals_m1A1D102131F563EC9CC4700EDD0838D757178C23,
	FirestoreCpp_QuerySnapshotEquals_m9F0C3443E8D422704ED11DE305F5B1C4E6427C10,
	FirestoreCpp_DocumentSnapshotEquals_mBB53A41FEBFB0142EE997A066D93D7A4707FDFF8,
	FirestoreCpp_QueryHashCode_m3F7607E088B1E8B9E04549EAE32083BECD3B743B,
	FirestoreCpp_QuerySnapshotHashCode_m39EE1BCCCFCC91EAB232133B61545C5D96BEBBA8,
	FirestoreCpp_DocumentSnapshotHashCode_mABD0453A1F70705840C56492C9E95E238BF7CC0F,
	FirestoreCpp_ConvertFieldValueToMap_m87B16DA1743A3E70FAE879815B5C667BC99ED6A3,
	FirestoreCpp_ConvertSnapshotToFieldValue_m00FA1F0927CA04092C0C62690A238E7AFFB56BAF,
	FirestoreCpp_ConvertFieldValueToVector_m4514465B7F9155D3B51C6F3D3453B55157FA4854,
	FirestoreCpp_QuerySnapshotDocuments_m928FF10F9321799B36879878D733E3F9D4961616,
	SWIGTYPE_p_unsigned_char__ctor_mC25543DA29742BBE978FE9EE814672ED040E614D,
	SWIGTYPE_p_unsigned_char_getCPtr_m262767A369063D7DC1BFE0D3D778F5B9AADF63F7,
	AssertFailedException__ctor_m05D002A26B7FE659617A48D630A420E4CB3B8665,
	Enums_Convert_mBDD60DF763EE06A7DB84D60E8E7A0DE2BC92C462,
	EnvironmentVersion_GetEnvironmentVersion_m485B071412F808C11BC687906C4AF4CF285B7ABC,
	EnvironmentVersion_FormatVersion_mBE8DB88F6A5AA04F8967BE4E1E50B4654F74A39D,
	Hash_LongHash_m46C175F7C083D3A28EDCF4B5A072177DA00BC164,
	Hash_DoubleBitwiseHash_mC1E113DBAA724DABCB8DACBC4648B1A26741F09D,
	NULL,
	Preconditions_CheckNotNullOrEmpty_m8DDC1D772B15915D545E6B76827F48392480D64F,
	Preconditions_CheckState_m2C52BED7317286BF6049FF703D71F9EC93EEC919,
	NULL,
	NULL,
	NULL,
	Preconditions_CheckArgument_m18F7F58C411622F2A035052AB6CD56EFA519C455,
	Util_Unreachable_m0AC7DAE43F9C84B867459337D2CE9326B4C8885B,
	Util_HardAssert_mA8CD1B90D807DF3A26D045E90CB8268A38C95CEE,
	NULL,
	NULL,
	Util_FlattenAndThrowException_mB884FECD5C9962C88EC525348837E234E639CFBF,
	Util_FlattenException_mE558EB9316C1B1B919C7C5C94097CA8CBD15579C,
	Util_OnPInvokeManagedException_m304890FF768577EF1024FC0C35033BFDE62BFE4E,
	NULL,
	NULL,
	AnonymousTypeConverter__ctor_m7DCAC02FE85E120F046D3E0CBFD087604F980005,
	ArrayConverter__ctor_mA1313A1815E38AD2C3529C5A4BC32E3D4636DE49,
	ArrayConverter_DeserializeArray_m49DFAEF6D2EF4866AF17897554F59DF09AA29A90,
	AttributedIdAssigner__ctor_m845A3F17677D200F097676153BF6C4803FD855D5,
	AttributedIdAssigner_AssignId_mB7E049C4FEB669301E3148923A83392D2860CD78,
	AttributedIdAssigner_MaybeAssignId_mB3118783949435F16240AE6AAB5C19DB1BEF3E08,
	AttributedIdAssigner_MaybeCreateAssigner_mEE7AE4DEDAB6DCB7CB78F73AE8E408BFBA0B789D,
	AttributedIdAssigner__cctor_mF4AEB637CAF60D08C64E703DB4680A7630195089,
	AttributedTypeConverter__ctor_mBF292B7ED4199739AEC6E40FF32121C951B8253D,
	AttributedTypeConverter_CreateObjectCreator_mED2F455222F2096D3A565C71D39825A7A7DFB124,
	AttributedTypeConverter_ForType_m09327601549BD65EEAB5D2B9441EDE3768963269,
	AttributedTypeConverter_DeserializeMap_m97C053D1B161037B03F803CAE08D5F5FAC51F1D8,
	AttributedProperty_get_CanRead_m9069D0364513C6A5A39B7D17E777EF0EECFBC179,
	AttributedProperty_get_CanWrite_mAAE46AA315D53C7E28EC64E1F95D9369E1296637,
	AttributedProperty_get_IsNullableValue_m9B85984E806381E63B62F75336CA45D5A3414EF2,
	AttributedProperty__ctor_mA6AEC934683566B0D0805CBC7F5BEEECE4B4CE2C,
	AttributedProperty_SetValue_m0F2898647DB8E1DACB8230C160A60ED8DFF0466D,
	U3CU3Ec__DisplayClass4_0__ctor_mFD9F7A1AD5FCF206816968696E1A5D7394846E22,
	U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__0_mEB7907452D39AA14013247B77E3FF81E124D5BAA,
	U3CU3Ec__DisplayClass5_0__ctor_m8D43B2649D32E27E0EF257F443F2AE7550AD6578,
	U3CU3Ec__DisplayClass5_0_U3CCreateObjectCreatorU3Eb__0_m3B94C156F95E8379DBB7803C74F0C581D960FF0C,
	U3CU3Ec__DisplayClass5_1__ctor_m79176D35335E95E4E2E4BB4F617FE2169111DF96,
	U3CU3Ec__DisplayClass5_1_U3CCreateObjectCreatorU3Eb__2_mC3D03145E21BD648150E70B7FE9133CC2AEC0885,
	U3CU3Ec__cctor_m8DBF73C83323708E2FDBBD70A4987B27C01A92BD,
	U3CU3Ec__ctor_mF8365A90F41A2A920A70CE536A7CA8C6FBCF27F9,
	U3CU3Ec_U3CCreateObjectCreatorU3Eb__5_1_m5A927C00BB3A2852B858C99CFF04267DEE31313B,
	ConverterBase__ctor_m97C0396E9FFFE6250D5AFB45CFA362D834AF2272,
	ConverterBase_DeserializeMap_m66826A0AD40B18EC9415EC1FC64B575191225916,
	ConverterBase_DeserializeValue_m00C88DE91D5DCF03E2EBEB2A09B2B3CFE5021EE4,
	ConverterBase_DeserializeArray_m6BC3F3E7C9301E5AB7C52FEE5C4920B8608B64E6,
	ConverterBase_DeserializeBoolean_m10CF93FCE6FB9ECF4BFB211AC1B302BF804DD435,
	ConverterBase_DeserializeBytes_m6167EDD3E924B01672B5EE7F551CC90C470BC534,
	ConverterBase_DeserializeDouble_m9962EB90FBEAA78B4AA5CF9F61151915DC31E989,
	ConverterBase_DeserializeGeoPoint_mD198047E195817DE2D87916F6FF7918085C7B26A,
	ConverterBase_DeserializeInteger_mD309DFC20C8C98EA26213330E0DB8B4D5C1626B0,
	ConverterBase_DeserializeReference_mBB585F5386F19A2338FD5FEADE9A26F7D284FEAE,
	ConverterBase_DeserializeString_m18B989E649017A4A8B04FCAF6B08EF35BC86B554,
	ConverterBase_DeserializeTimestamp_m9CAB74E5E801EF4A678020083FEC4C6A59B4C169,
	ConverterBase_ConvertFromProxyBlob_m85813B2A63FB4C5DD17701C0D9A8EC67C18131CA,
	ConverterCache_GetConverter_mFA8FD20B24A60E770780BF20FAB0C14501A1DD04,
	ConverterCache_InitializeConverterCache_m42AB202740B9CD255E00701B0E66E08FCC934419,
	NULL,
	ConverterCache_CreateConverter_m702436996235BC0AA4AD27B270A814CE861B9215,
	ConverterCache_TryGetStringDictionaryValueType_m59C6555BF1324FD266E6314EDF82F4E5E4F78ADD,
	ConverterCache_MapInterfaceToDictionaryValueTypeArgument_m43AC3ADC961F50F3693674AFB270586E92260A44,
	ConverterCache__cctor_m9040033FF723399492820D23E96AFD108939D54D,
	U3CU3Ec__cctor_m23FC8EC2ADC406520637A02A65D453FCAF53C3E0,
	U3CU3Ec__ctor_mD53255F31383902CB5E33E6BD59EAA8A966A3BEB,
	U3CU3Ec_U3CTryGetStringDictionaryValueTypeU3Eb__6_0_mFE81D3AA4C3BA8314ABE02D7AB3D1D6671987156,
	CustomConverter_ForConverterType_m7EB6F5B02D57772148A990A4339C5369CDF95775,
	NULL,
	CustomConverter__cctor_mDB392684F60F75FF209FFCE45D55BE36AE02DDB5,
	NULL,
	NULL,
	NULL,
	NULL,
	EnumConverter__ctor_mD37CFE82084160EF7F8B28F33CD0B06647CE3E7C,
	EnumConverter_DeserializeInteger_m3239C09E0A481DE24FF76D8C77A03054010A9368,
	EnumConverter_Int64ToEnumBaseType_mCD6A4FF021CC4EF06083E49C0AACD4309C588696,
	EnumerableConverter__ctor_mB48815398EF3D3A11F8BBC678428AFC5455B7952,
	EnumerableConverter_DeserializeArray_m2FA58C3F2795DE6947A612F01CF2E92ECA033C2E,
	U3CU3Ec__cctor_mE940390EF3A6ACE9BA732FA6699C02B471095DFD,
	U3CU3Ec__ctor_m288763E12379E266FE23B1326FA3ED95CB431C85,
	U3CU3Ec_U3C_ctorU3Eb__1_0_m0A28F878818DE956B76EF57911FA756D87DC8209,
	EnumerableConverterBase__ctor_mACF692E41E653590FAA7229986A8007D74BA093D,
	NULL,
	MapConverterBase__ctor_mC049B3C208A807A2D36B156346EEDF7023D20318,
	StringConverter__ctor_m98FE6424F0B19EBBA2C3D4A56395848BFBA0FB8D,
	StringConverter_DeserializeString_m2138230D08D0B337A248C18871EA72DFBF72EAA9,
	IntegerConverterBase__ctor_mC6D2C4315041A669DC89E2A608585368F03051FE,
	IntegerConverterBase_DeserializeDouble_mA924A2E16AB9F0B92555D02A77B2DEB54A2771FA,
	ByteConverter__ctor_mF9792E6D174D89395EE27DDF6043776927EB029F,
	ByteConverter_DeserializeInteger_m589094EE35BA48B5092502F5D1F098D7F40C4A80,
	SByteConverter__ctor_mB4A9C6F21E4B989A68CF6364A36790D874661359,
	SByteConverter_DeserializeInteger_mAD0CD165E2513E083923CA286F97F975523B9384,
	Int16Converter__ctor_m5CCA12D045ADA1DF683861482721BD4355A00923,
	Int16Converter_DeserializeInteger_m6532BE8B668ACD0CBA3BD1043148755FB176A922,
	UInt16Converter__ctor_m04FFBB5129B7794DA366B913ADC966F606729DC3,
	UInt16Converter_DeserializeInteger_m560F57300825FD654D76C0A37102F296BCB62B76,
	Int32Converter__ctor_m04DBC86216C93FB26610A0246031D883DEFB1EDC,
	Int32Converter_DeserializeInteger_m1D6467D336A5C6D998A617A69A8F02436161587B,
	UInt32Converter__ctor_m28B1715D1D72F564B7F953E4FA109EDE0E050A6D,
	UInt32Converter_DeserializeInteger_m390C8F493F4C82B4101D3EA6A3EAA55719A74561,
	Int64Converter__ctor_m83C5877D10DE63CFC459CC095D7A3220412AC141,
	Int64Converter_DeserializeInteger_m29D7C3D54F53506CE4463713B61497D6072DA7B8,
	UInt64Converter__ctor_mA34FFF9980DF663B5A29F3E2A4DCB1F06D8592E8,
	UInt64Converter_DeserializeInteger_mAE1CFB00A64A65EB6BFA468BDAC8198BB0F4483A,
	SingleConverter__ctor_m1BEE3FF96828B431A845D1EF4CB0BD80940729C2,
	SingleConverter_DeserializeDouble_m4FFDBCF934B47AFE1E2288B63E5AA0FB6C0E38CC,
	SingleConverter_DeserializeInteger_m040F04554DDCDEFCE291974441526556AAFAB9B5,
	DoubleConverter__ctor_m79DB1F23037EDAEED1C5481821D5EB179320665A,
	DoubleConverter_DeserializeDouble_m86F7A05229B9752892200D1AC7A215B55AD6BDEA,
	DoubleConverter_DeserializeInteger_mE3E4CD79C1DD77FF945D3A71912987F0B0CF5787,
	BooleanConverter__ctor_mA7183CE40E3C6B690175361759D1503EC2E402E9,
	BooleanConverter_DeserializeBoolean_mE5B87DCC3A69F46397DAF6570328F1BD52DDAA89,
	TimestampConverter__ctor_m03864D5954A693D167B5D50FD36BE26EDD6001B2,
	TimestampConverter_DeserializeTimestamp_m2C44736ECAD84B1588129151AA38CB9D37469291,
	GeoPointConverter__ctor_m88FED2B6D5AD779C604B4AFB4E87695C10299FE9,
	GeoPointConverter_DeserializeGeoPoint_mFA52D60151AEE029B1A99162669B815D6BEA4D07,
	ByteArrayConverter__ctor_m6AF637922A82D6B3D9183B5259D6789DB2B03D5D,
	ByteArrayConverter_DeserializeBytes_m17BD90A3CB8CAE08F2E683A04D7391A7C005A150,
	BlobConverter__ctor_m1C338FCD5CA26ABF85D4CDE0E83D5205EB36FE89,
	BlobConverter_DeserializeBytes_mDCF82DCA3DEB1643A670D0CE58871F33B6420171,
	FieldValueProxyConverter__ctor_m36EB0DA5C8D905FB06603CF9B0DAD598A3DF67B4,
	DateTimeConverter__ctor_m6BB0654F1E1A7B9FE3C6B9CC642486369624B599,
	DateTimeConverter_DeserializeTimestamp_m947D35CC004229796E006D09057144E21EBFAE6A,
	DateTimeOffsetConverter__ctor_m5DB81C2ED157E462FEC06726BF9BF570DA75C109,
	DateTimeOffsetConverter_DeserializeTimestamp_m6252B475710A397DD610DD9B096EE5720033B902,
	DocumentReferenceConverter__ctor_mA0F99E49DF3314D112A8305CEE3A1C44B45DFA0C,
	DocumentReferenceConverter_DeserializeReference_mC9CF79070287A37D7FE257A4CBDA18A02622DF59,
};
extern void Blob_get_Length_mCB3B8BACCFDD08FC7B032CE7A19863A877DE1135_AdjustorThunk (void);
extern void Blob_Equals_m40803BA16F983221C401AEABB5B9AFB1A1620147_AdjustorThunk (void);
extern void Blob_GetHashCode_mBF1065AEF1A335C0ED4700A81CC9FDB07CBFFFA9_AdjustorThunk (void);
extern void Blob_Equals_mF653C3D01DFC846FF67142360E6136E161FF0249_AdjustorThunk (void);
extern void Blob_ToString_mAE6F40B4D0AABB2E52D19C6193DE711A82A5AFC9_AdjustorThunk (void);
extern void Blob__ctor_mA04D8C7EC8B272C542AC71FE121655E43029C4F6_AdjustorThunk (void);
extern void GeoPoint_get_Latitude_m8579A0A251B83D4EA9440C8F5DC8903F66A90C64_AdjustorThunk (void);
extern void GeoPoint_get_Longitude_m3F0E30D4821DFD38D45FE9F07AE8A6E77CC6FAFD_AdjustorThunk (void);
extern void GeoPoint__ctor_m317FEC5570FD35C090273B4370D17B4EE7CEB207_AdjustorThunk (void);
extern void GeoPoint_Equals_mCC5F79AFC2260998069ECA5A2E37E8F1DF9F8E67_AdjustorThunk (void);
extern void GeoPoint_GetHashCode_mAF353C5026FB2581BAECEB9331D6ECD63915356D_AdjustorThunk (void);
extern void GeoPoint_Equals_m3CD83E438DB040259EA1391F4F1ED0742F68A317_AdjustorThunk (void);
extern void GeoPoint_ToString_mA5A4F4B5C2236F6D5A26D257379BF36673A06B21_AdjustorThunk (void);
extern void Timestamp_ToDateTime_mCE43F7D6B36597BD9EB814027BC58604EB50463C_AdjustorThunk (void);
extern void Timestamp_ToDateTimeOffset_mE057569E9579185AB1C03D7DC59E0E3B0EB7986E_AdjustorThunk (void);
extern void Timestamp_Equals_mC5FB2C7784C4154CD68A065B9C3B2979954D8AA7_AdjustorThunk (void);
extern void Timestamp_GetHashCode_m77644F39C766B1A3E3C3C8A8DE9898DAE0DDE115_AdjustorThunk (void);
extern void Timestamp_Equals_mE6E5EF21BA260D4719381F9C9171CD1C80A9C350_AdjustorThunk (void);
extern void Timestamp_CompareTo_m5E0F72C5F36DC0E3E81C88313619B79D213F920F_AdjustorThunk (void);
extern void Timestamp_CompareTo_mAC46B7F62D530BDEB81CAB57230CA93B72B5A14D_AdjustorThunk (void);
extern void Timestamp_ToString_m52D6B302274294F4092FE717A7897B06FB88BD22_AdjustorThunk (void);
extern void Timestamp__ctor_mE2833EDD86D95B4925817BEE9D4EA8C7A3960F0C_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[22] = 
{
	{ 0x06000001, Blob_get_Length_mCB3B8BACCFDD08FC7B032CE7A19863A877DE1135_AdjustorThunk },
	{ 0x06000003, Blob_Equals_m40803BA16F983221C401AEABB5B9AFB1A1620147_AdjustorThunk },
	{ 0x06000004, Blob_GetHashCode_mBF1065AEF1A335C0ED4700A81CC9FDB07CBFFFA9_AdjustorThunk },
	{ 0x06000005, Blob_Equals_mF653C3D01DFC846FF67142360E6136E161FF0249_AdjustorThunk },
	{ 0x06000006, Blob_ToString_mAE6F40B4D0AABB2E52D19C6193DE711A82A5AFC9_AdjustorThunk },
	{ 0x06000007, Blob__ctor_mA04D8C7EC8B272C542AC71FE121655E43029C4F6_AdjustorThunk },
	{ 0x0600006A, GeoPoint_get_Latitude_m8579A0A251B83D4EA9440C8F5DC8903F66A90C64_AdjustorThunk },
	{ 0x0600006B, GeoPoint_get_Longitude_m3F0E30D4821DFD38D45FE9F07AE8A6E77CC6FAFD_AdjustorThunk },
	{ 0x0600006C, GeoPoint__ctor_m317FEC5570FD35C090273B4370D17B4EE7CEB207_AdjustorThunk },
	{ 0x0600006D, GeoPoint_Equals_mCC5F79AFC2260998069ECA5A2E37E8F1DF9F8E67_AdjustorThunk },
	{ 0x0600006E, GeoPoint_GetHashCode_mAF353C5026FB2581BAECEB9331D6ECD63915356D_AdjustorThunk },
	{ 0x0600006F, GeoPoint_Equals_m3CD83E438DB040259EA1391F4F1ED0742F68A317_AdjustorThunk },
	{ 0x06000070, GeoPoint_ToString_mA5A4F4B5C2236F6D5A26D257379BF36673A06B21_AdjustorThunk },
	{ 0x06000097, Timestamp_ToDateTime_mCE43F7D6B36597BD9EB814027BC58604EB50463C_AdjustorThunk },
	{ 0x06000098, Timestamp_ToDateTimeOffset_mE057569E9579185AB1C03D7DC59E0E3B0EB7986E_AdjustorThunk },
	{ 0x06000099, Timestamp_Equals_mC5FB2C7784C4154CD68A065B9C3B2979954D8AA7_AdjustorThunk },
	{ 0x0600009A, Timestamp_GetHashCode_m77644F39C766B1A3E3C3C8A8DE9898DAE0DDE115_AdjustorThunk },
	{ 0x0600009B, Timestamp_Equals_mE6E5EF21BA260D4719381F9C9171CD1C80A9C350_AdjustorThunk },
	{ 0x0600009C, Timestamp_CompareTo_m5E0F72C5F36DC0E3E81C88313619B79D213F920F_AdjustorThunk },
	{ 0x0600009D, Timestamp_CompareTo_mAC46B7F62D530BDEB81CAB57230CA93B72B5A14D_AdjustorThunk },
	{ 0x0600009E, Timestamp_ToString_m52D6B302274294F4092FE717A7897B06FB88BD22_AdjustorThunk },
	{ 0x0600009F, Timestamp__ctor_mE2833EDD86D95B4925817BEE9D4EA8C7A3960F0C_AdjustorThunk },
};
static const int32_t s_InvokerIndices[626] = 
{
	3518,
	4957,
	2080,
	3518,
	2027,
	3533,
	2856,
	1582,
	3533,
	3533,
	3518,
	2080,
	2080,
	3533,
	1582,
	5298,
	3533,
	3533,
	3533,
	2550,
	3518,
	2080,
	2080,
	3533,
	4181,
	5396,
	1581,
	590,
	143,
	2856,
	1582,
	3533,
	3533,
	3478,
	2547,
	-1,
	-1,
	-1,
	-1,
	2080,
	2080,
	2080,
	2080,
	3518,
	3533,
	1571,
	5160,
	5160,
	4969,
	3533,
	2080,
	3518,
	2080,
	3533,
	5396,
	5396,
	3595,
	2080,
	1582,
	3595,
	1582,
	3595,
	3533,
	2856,
	5377,
	5160,
	2550,
	5294,
	4846,
	-1,
	3595,
	5396,
	1581,
	2842,
	808,
	2856,
	1581,
	1459,
	463,
	2856,
	3595,
	2550,
	3595,
	3533,
	2856,
	3595,
	3533,
	3478,
	3478,
	3519,
	-1,
	3595,
	3533,
	5396,
	3533,
	3478,
	3478,
	3519,
	3518,
	3533,
	1470,
	1470,
	2842,
	3533,
	3533,
	3494,
	3494,
	1317,
	2080,
	3518,
	2054,
	3533,
	5024,
	2856,
	3518,
	2842,
	3518,
	2842,
	3519,
	2843,
	3519,
	2843,
	3518,
	2842,
	2080,
	3518,
	1582,
	5298,
	3533,
	2547,
	2080,
	2080,
	3518,
	4181,
	5396,
	2550,
	1581,
	590,
	143,
	2856,
	1582,
	3533,
	3518,
	2080,
	2080,
	3518,
	3533,
	3533,
	3595,
	5048,
	3489,
	3490,
	2080,
	3518,
	2123,
	2442,
	2414,
	3533,
	1556,
	5227,
	5396,
	3533,
	3533,
	3533,
	2856,
	2550,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2856,
	5377,
	2856,
	2550,
	5396,
	1582,
	3595,
	3595,
	5396,
	4384,
	5160,
	1560,
	3595,
	3595,
	2797,
	3494,
	3494,
	1560,
	3595,
	3595,
	2797,
	3519,
	3518,
	3533,
	1560,
	2797,
	5160,
	3595,
	2856,
	2844,
	5294,
	1231,
	2844,
	3533,
	5396,
	1581,
	3595,
	1264,
	2856,
	1581,
	2842,
	808,
	2856,
	3595,
	3595,
	1560,
	5026,
	3595,
	3595,
	2797,
	2856,
	1560,
	3595,
	3595,
	2797,
	3533,
	3533,
	2550,
	3478,
	1560,
	5026,
	3595,
	3595,
	2797,
	3533,
	3533,
	3478,
	1261,
	2550,
	1560,
	5026,
	3595,
	3595,
	2797,
	3518,
	3478,
	3478,
	3519,
	3494,
	3533,
	3533,
	3533,
	3586,
	3533,
	3533,
	3478,
	5377,
	1560,
	5026,
	3595,
	3595,
	2797,
	2547,
	1560,
	2797,
	3533,
	1560,
	5026,
	3595,
	3595,
	2797,
	3586,
	1560,
	5026,
	3595,
	3595,
	2797,
	3595,
	3533,
	3478,
	3478,
	3519,
	2856,
	2797,
	2797,
	2843,
	5396,
	1560,
	3595,
	3595,
	2797,
	3518,
	3518,
	3519,
	3519,
	3518,
	1560,
	5026,
	3595,
	3595,
	2797,
	5160,
	2550,
	3533,
	2856,
	5298,
	3595,
	2797,
	1560,
	3595,
	3595,
	2797,
	2856,
	3595,
	1560,
	3595,
	3595,
	2797,
	3533,
	1560,
	3595,
	3595,
	2797,
	3478,
	3595,
	3533,
	3533,
	1560,
	3595,
	3595,
	2797,
	2556,
	1560,
	3595,
	3595,
	2797,
	3586,
	2556,
	5396,
	5010,
	5010,
	5293,
	5062,
	5046,
	5154,
	5293,
	4334,
	4841,
	5075,
	5293,
	5075,
	5293,
	5293,
	5154,
	5154,
	4685,
	4964,
	5293,
	5154,
	5075,
	4964,
	4333,
	4683,
	5293,
	5046,
	4964,
	4964,
	5062,
	5010,
	5075,
	5154,
	5075,
	5246,
	5075,
	5075,
	4964,
	5373,
	5293,
	4684,
	5154,
	5293,
	5293,
	5246,
	5372,
	5373,
	5154,
	4964,
	4964,
	5062,
	4842,
	4837,
	4837,
	4840,
	5293,
	5046,
	5046,
	5062,
	5062,
	5046,
	5293,
	5075,
	5293,
	4685,
	5075,
	4838,
	4557,
	4557,
	4557,
	5046,
	5046,
	5046,
	5298,
	5075,
	4684,
	5293,
	5075,
	5293,
	5293,
	5075,
	5293,
	4964,
	5293,
	5154,
	5075,
	5293,
	5075,
	5075,
	4686,
	5293,
	5246,
	4686,
	5293,
	5078,
	5078,
	3686,
	4502,
	5298,
	5298,
	5298,
	5298,
	5298,
	5298,
	5298,
	5298,
	5298,
	5298,
	5298,
	4866,
	4866,
	4866,
	5396,
	3595,
	1581,
	2856,
	830,
	2856,
	1581,
	1582,
	498,
	2856,
	5363,
	5298,
	5377,
	5396,
	5298,
	5160,
	5396,
	3595,
	1581,
	2550,
	830,
	2550,
	5298,
	5298,
	5396,
	3595,
	1581,
	2856,
	830,
	2856,
	4574,
	4574,
	4574,
	5052,
	5052,
	5052,
	5160,
	4726,
	5160,
	5160,
	1560,
	5026,
	2856,
	5048,
	5377,
	5160,
	5049,
	5045,
	-1,
	4731,
	4835,
	-1,
	-1,
	-1,
	4446,
	5396,
	4835,
	-1,
	-1,
	5298,
	5160,
	4866,
	-1,
	-1,
	2856,
	2856,
	1264,
	1582,
	1582,
	4866,
	5160,
	5396,
	1582,
	5160,
	5160,
	1264,
	3478,
	3478,
	3478,
	1582,
	964,
	3595,
	2080,
	3595,
	3533,
	3595,
	3533,
	5396,
	3595,
	2080,
	2856,
	1264,
	1264,
	1264,
	1257,
	1264,
	1259,
	1260,
	1262,
	1264,
	1264,
	1265,
	5160,
	5160,
	5396,
	-1,
	5160,
	4570,
	5160,
	5396,
	5396,
	3595,
	2080,
	4731,
	-1,
	5396,
	-1,
	-1,
	-1,
	-1,
	2856,
	1262,
	2548,
	2856,
	1264,
	5396,
	3595,
	2080,
	2856,
	1264,
	2856,
	3595,
	1264,
	2856,
	1259,
	3595,
	1262,
	3595,
	1262,
	3595,
	1262,
	3595,
	1262,
	3595,
	1262,
	3595,
	1262,
	3595,
	1262,
	3595,
	1262,
	3595,
	1259,
	1262,
	3595,
	1259,
	1262,
	3595,
	1257,
	3595,
	1265,
	3595,
	1260,
	3595,
	1264,
	3595,
	1264,
	3595,
	3595,
	1265,
	3595,
	1265,
	3595,
	1264,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[21] = 
{
	{ 0x06000019, 4,  (void**)&DocumentReference_DocumentSnapshotsHandler_mE75173506F68CF4107731634BF380B9BBB7372F1_RuntimeMethod_var, 0 },
	{ 0x06000044, 7,  (void**)&FirebaseFirestore_SnapshotsInSyncHandler_m9820BBCB59CB6A40F2CF44F87BD7ABD483137FDE_RuntimeMethod_var, 0 },
	{ 0x06000045, 6,  (void**)&FirebaseFirestore_LoadBundleTaskProgressHandler_mDEEE792A4653B34026BC13985E88600A9D3F1393_RuntimeMethod_var, 0 },
	{ 0x06000086, 13,  (void**)&Query_QuerySnapshotsHandler_m063CC24B00233FBA6CC4C2E3D39E731BD8800821_RuntimeMethod_var, 0 },
	{ 0x060000CB, 8,  (void**)&Future_QuerySnapshot_SWIG_CompletionDispatcher_m15F1003D3DED0E6AC8CBBD6E50317019574A3596_RuntimeMethod_var, 0 },
	{ 0x060001B7, 36,  (void**)&SWIGExceptionHelper_SetPendingApplicationException_m7FFFCE61B08FE17B5B6C8DD589E8D2BA7E514700_RuntimeMethod_var, 0 },
	{ 0x060001B8, 40,  (void**)&SWIGExceptionHelper_SetPendingArithmeticException_m4A492793829E24826915E01F0C17FE0B8499A7B9_RuntimeMethod_var, 0 },
	{ 0x060001B9, 41,  (void**)&SWIGExceptionHelper_SetPendingDivideByZeroException_mD7DFEB207289EFD4D916BAE54302503294B3A469_RuntimeMethod_var, 0 },
	{ 0x060001BA, 43,  (void**)&SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m20AF827963A7C8B753AF68164B7D776C15A67DE9_RuntimeMethod_var, 0 },
	{ 0x060001BB, 44,  (void**)&SWIGExceptionHelper_SetPendingInvalidCastException_m0581C3F4931A20BD5ED4EA66093DDDD6A906C6D6_RuntimeMethod_var, 0 },
	{ 0x060001BC, 45,  (void**)&SWIGExceptionHelper_SetPendingInvalidOperationException_m40B9057C541E1372563BB9E1002E823E38096D62_RuntimeMethod_var, 0 },
	{ 0x060001BD, 42,  (void**)&SWIGExceptionHelper_SetPendingIOException_m8CB7AE7D58F9693F5250EEB08B367E50AF724CDB_RuntimeMethod_var, 0 },
	{ 0x060001BE, 46,  (void**)&SWIGExceptionHelper_SetPendingNullReferenceException_m07F507856FF6AE9838F5CA00AA64F38CBFA2DA84_RuntimeMethod_var, 0 },
	{ 0x060001BF, 47,  (void**)&SWIGExceptionHelper_SetPendingOutOfMemoryException_m8A1648D61A97D46629D84CB4BF64BE758E3B0987_RuntimeMethod_var, 0 },
	{ 0x060001C0, 48,  (void**)&SWIGExceptionHelper_SetPendingOverflowException_mB46C4E0EECEBB64698810B5EEBFF2A10B3869D0D_RuntimeMethod_var, 0 },
	{ 0x060001C1, 49,  (void**)&SWIGExceptionHelper_SetPendingSystemException_m4504C1471D8E13CC34E2DA16ED245C39DBC5125F_RuntimeMethod_var, 0 },
	{ 0x060001C2, 37,  (void**)&SWIGExceptionHelper_SetPendingArgumentException_m03827C4D6C68D5769B0ECF89FBA487FE09CACA03_RuntimeMethod_var, 0 },
	{ 0x060001C3, 38,  (void**)&SWIGExceptionHelper_SetPendingArgumentNullException_m47A1AC704DEE8080F51803DC7FED941B2C51D7DE_RuntimeMethod_var, 0 },
	{ 0x060001C4, 39,  (void**)&SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mF2C7DEC5C75D945A3FF2025EFD9E51FD4961ED8D_RuntimeMethod_var, 0 },
	{ 0x060001D4, 50,  (void**)&SWIGStringHelper_CreateString_m3D08489776631E77C6252B713D92331E5D1D685E_RuntimeMethod_var, 0 },
	{ 0x060001DC, 35,  (void**)&FirestoreExceptionHelper_SetPendingFirestoreException_m0CBBB27537ABEEC0933F1407AFE213BCE003DD6C_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[17] = 
{
	{ 0x02000023, { 8, 15 } },
	{ 0x02000054, { 36, 2 } },
	{ 0x02000062, { 42, 2 } },
	{ 0x02000063, { 44, 4 } },
	{ 0x06000024, { 0, 2 } },
	{ 0x06000025, { 2, 1 } },
	{ 0x06000026, { 3, 1 } },
	{ 0x06000027, { 4, 2 } },
	{ 0x06000046, { 6, 1 } },
	{ 0x0600005B, { 7, 1 } },
	{ 0x060001F5, { 23, 1 } },
	{ 0x060001F8, { 24, 1 } },
	{ 0x060001F9, { 25, 2 } },
	{ 0x060001FA, { 27, 3 } },
	{ 0x060001FF, { 30, 6 } },
	{ 0x0600022E, { 38, 2 } },
	{ 0x06000237, { 40, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[48] = 
{
	{ (Il2CppRGCTXDataType)1, 124 },
	{ (Il2CppRGCTXDataType)2, 124 },
	{ (Il2CppRGCTXDataType)3, 26263 },
	{ (Il2CppRGCTXDataType)3, 26267 },
	{ (Il2CppRGCTXDataType)1, 123 },
	{ (Il2CppRGCTXDataType)2, 123 },
	{ (Il2CppRGCTXDataType)3, 10823 },
	{ (Il2CppRGCTXDataType)3, 10573 },
	{ (Il2CppRGCTXDataType)2, 1926 },
	{ (Il2CppRGCTXDataType)3, 3767 },
	{ (Il2CppRGCTXDataType)3, 18263 },
	{ (Il2CppRGCTXDataType)1, 586 },
	{ (Il2CppRGCTXDataType)3, 3770 },
	{ (Il2CppRGCTXDataType)3, 21410 },
	{ (Il2CppRGCTXDataType)3, 3769 },
	{ (Il2CppRGCTXDataType)3, 3768 },
	{ (Il2CppRGCTXDataType)3, 8828 },
	{ (Il2CppRGCTXDataType)3, 14333 },
	{ (Il2CppRGCTXDataType)3, 21409 },
	{ (Il2CppRGCTXDataType)3, 14332 },
	{ (Il2CppRGCTXDataType)3, 8827 },
	{ (Il2CppRGCTXDataType)2, 2251 },
	{ (Il2CppRGCTXDataType)3, 18264 },
	{ (Il2CppRGCTXDataType)2, 319 },
	{ (Il2CppRGCTXDataType)2, 318 },
	{ (Il2CppRGCTXDataType)2, 441 },
	{ (Il2CppRGCTXDataType)2, 855 },
	{ (Il2CppRGCTXDataType)2, 453 },
	{ (Il2CppRGCTXDataType)2, 867 },
	{ (Il2CppRGCTXDataType)2, 998 },
	{ (Il2CppRGCTXDataType)2, 1334 },
	{ (Il2CppRGCTXDataType)3, 236 },
	{ (Il2CppRGCTXDataType)3, 237 },
	{ (Il2CppRGCTXDataType)2, 2584 },
	{ (Il2CppRGCTXDataType)3, 10773 },
	{ (Il2CppRGCTXDataType)3, 21024 },
	{ (Il2CppRGCTXDataType)3, 21067 },
	{ (Il2CppRGCTXDataType)3, 10730 },
	{ (Il2CppRGCTXDataType)2, 1847 },
	{ (Il2CppRGCTXDataType)3, 3513 },
	{ (Il2CppRGCTXDataType)2, 1835 },
	{ (Il2CppRGCTXDataType)3, 3463 },
	{ (Il2CppRGCTXDataType)3, 10548 },
	{ (Il2CppRGCTXDataType)2, 498 },
	{ (Il2CppRGCTXDataType)1, 2012 },
	{ (Il2CppRGCTXDataType)2, 3116 },
	{ (Il2CppRGCTXDataType)1, 500 },
	{ (Il2CppRGCTXDataType)2, 500 },
};
extern const CustomAttributesCacheGenerator g_Firebase_Firestore_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Firebase_Firestore_CodeGenModule;
const Il2CppCodeGenModule g_Firebase_Firestore_CodeGenModule = 
{
	"Firebase.Firestore.dll",
	626,
	s_methodPointers,
	22,
	s_adjustorThunks,
	s_InvokerIndices,
	21,
	s_reversePInvokeIndices,
	17,
	s_rgctxIndices,
	48,
	s_rgctxValues,
	NULL,
	g_Firebase_Firestore_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

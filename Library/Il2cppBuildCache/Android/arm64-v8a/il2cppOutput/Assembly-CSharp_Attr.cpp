﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};


// System.Object


// System.Object


// System.Attribute


// System.Attribute


// System.Reflection.MemberInfo


// System.Reflection.MemberInfo


// System.String

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.String


// System.ValueType


// System.ValueType


// System.Boolean

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Boolean


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute


// System.Runtime.CompilerServices.CompilerGeneratedAttribute


// System.Runtime.CompilerServices.CompilerGeneratedAttribute


// System.Diagnostics.DebuggerHiddenAttribute


// System.Diagnostics.DebuggerHiddenAttribute


// System.Enum

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};


// System.Enum


// System.IntPtr

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.IntPtr


// UnityEngine.PropertyAttribute


// UnityEngine.PropertyAttribute


// UnityEngine.RequireComponent


// UnityEngine.RequireComponent


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute


// UnityEngine.SerializeField


// UnityEngine.SerializeField


// System.Runtime.CompilerServices.StateMachineAttribute


// System.Runtime.CompilerServices.StateMachineAttribute


// System.Void


// System.Void


// System.Reflection.BindingFlags


// System.Reflection.BindingFlags


// UnityEngine.HeaderAttribute


// UnityEngine.HeaderAttribute


// System.Runtime.CompilerServices.IteratorStateMachineAttribute


// System.Runtime.CompilerServices.IteratorStateMachineAttribute


// System.RuntimeTypeHandle


// System.RuntimeTypeHandle


// System.Diagnostics.DebuggableAttribute/DebuggingModes


// System.Diagnostics.DebuggableAttribute/DebuggingModes


// System.Diagnostics.DebuggableAttribute


// System.Diagnostics.DebuggableAttribute


// System.Type

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Type

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void BtnPrefabScript_tD918A72763276317AD81FA3E9C72D5014A92F3E3_CustomAttributesCacheGenerator_BtnPrefabScript_U3CDownloadVerticalButtonsPhotosU3Eb__12_0_mC76C117FC1139565C8B47F663D4B1FC4B7445A9C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_tF82578CDE88026909D98AA0584DD60EDBAA86616_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Create3DModel_t911E541B22429F7C1EE53AC4DFCAF58A2329A073_CustomAttributesCacheGenerator_targetTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Create3DModel_t911E541B22429F7C1EE53AC4DFCAF58A2329A073_CustomAttributesCacheGenerator_Create3DModel_DownloadAssetBundle_mCA1A34D5EE51EC4AC314555016F106BCA535C08E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_0_0_0_var), NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__11__ctor_mD5B96E18A4598751D9D323F9DE32E1256E85CD52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__11_System_IDisposable_Dispose_m3247B77C318D87D59E280129CC1EBDE8DC812930(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9989BD403603E7630A3FC51367E31B1E2117DBBD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__11_System_Collections_IEnumerator_Reset_mE49F30B824501D89E1C8CA52A2127FA44CEF97BA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__11_System_Collections_IEnumerator_get_Current_m280482882ED846B0B6EF97FE3E980C350FE1055A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DownloadAssetBundleExtension_tD735DD43C905CF1C39EE2BFE13BE870A1DE208D2_CustomAttributesCacheGenerator_DownloadAssetBundleExtension_DownloadAssetBundle_m402969E102F8C322D4BFBB7282CC99E1C04A14EC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_0_0_0_var), NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__0__ctor_m7B2BF0DF356348556FC7042A3AE4A685755D5DE0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__0_System_IDisposable_Dispose_m7AFBF52E380519A03B91A866EE543235CCE30139(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6BA65CE24E036AEB241CC688912C5C0BBF77F321(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__0_System_Collections_IEnumerator_Reset_m95EEC0E276163815A0621D083A5245BA80D0E806(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__0_System_Collections_IEnumerator_get_Current_m2376DACBEBE824FDE4BC450E4AB22942DB8AAE7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DownloadImageExtensions_t013A125B9853322BD6718CB1DC2315C88C2759A9_CustomAttributesCacheGenerator_DownloadImageExtensions_DownloadImage_m425EFA7E5A50937CE8EEE15F9D0067CF80BFE8A6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_0_0_0_var), NULL);
	}
}
static void U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__0__ctor_m24345C0A4F0AF3E8FE9AE84E784DBB6034C9E009(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__0_System_IDisposable_Dispose_m565B46EF4ABF21CEF4DC0689E0FC9FA6BB94F7FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDF12CBB291BAB27BBF99AF1DC0B7EBC3F89DC9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__0_System_Collections_IEnumerator_Reset_mDA3A2F9B4C6CC57A732C0CE780504A4C486E0245(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__0_System_Collections_IEnumerator_get_Current_mD8EAA09FDD726A953D8984E16355CA7FA8A6FB4C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AssetBundleAugmenter_t2F3C36AFE2FD9BFE87451A343806D3381DECDC9C_CustomAttributesCacheGenerator_AssetBundleAugmenter_DownloadAndCache_m827A785F9BA8DF539DDF5F123704DF6676456951(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_0_0_0_var), NULL);
	}
}
static void U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__6__ctor_mC207A786AC388526B65793D9B7A3CD21637E7FE7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__6_System_IDisposable_Dispose_m1232F60243AEDD6A732706374CA4B4291EA423EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m45D9FC362C3EF9B098B0D929053584864B3D3801(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__6_System_Collections_IEnumerator_Reset_m1C26776B3F6459BAC1EEBCB840B1CEB3A0F0B5CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__6_System_Collections_IEnumerator_get_Current_mD133B8114A8DF8E11DD7D96976E3AF9257A12C56(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_tE8121A843C65E2DA1DDBDC18D287157848DA26CE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MerkezeGoreBoyutlandir_t607A9B26417F72BD75EAE26E5E5F32115F87CA52_CustomAttributesCacheGenerator_scrollParent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MerkezeGoreBoyutlandir_t607A9B26417F72BD75EAE26E5E5F32115F87CA52_CustomAttributesCacheGenerator_boyutlandirmaOrani(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MerkezeGoreBoyutlandir_t607A9B26417F72BD75EAE26E5E5F32115F87CA52_CustomAttributesCacheGenerator_yon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CachingLoadExample_t834CDE8453BA379D396A43E309AEEBD64279D1DC_CustomAttributesCacheGenerator_CachingLoadExample_DownloadAndCache_mF4487DC82E54CF46DD031998ACFF4DC7370E882B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_0_0_0_var), NULL);
	}
}
static void U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__4__ctor_m4F21C539BBFCA4BA9FB7EC3A671F6138DEE5E249(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__4_System_IDisposable_Dispose_mD02B8042829A3C35444BC73F7665F90E5D91D0DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1EBBB1958D5DD1DA2D998CA242D213D6A60A8D76(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__4_System_Collections_IEnumerator_Reset_m6E4ED7FA6DD400F4E6B4ABB2556E685C2A4132C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__4_System_Collections_IEnumerator_get_Current_mA1313E5FECCB8B642D0B69ADA6546EA3CEE028BA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void script_t9023BF623D4A6A9D9C11357DC6489ED305B06088_CustomAttributesCacheGenerator_script_Start_mBDC0E6710997FCBF56EB900A4B5F22EB9B94A1AE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_CustomAttributesCacheGenerator_U3CStartU3Ed__2__ctor_mF5705C0C398CD8684E58523E9DA12AA6C0B51E2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_IDisposable_Dispose_m0D240E2FDBED87E1E915A2689C41CF809807E00A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0B3B79FF648B8964506DD8D39E66DEC99CD268ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m1453297B53E3241F93D9F46A973072628FD12E5C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mCF672EEEF34AAC75D10821A8CC8FFE11F2589942(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LineRendererPatternShifter_t403FB1A6D6B70FB2EAED67C04F378A95BD559797_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_0_0_0_var), NULL);
	}
}
static void ModularBillBoard_tECE1BE125DDD1BAA622B28A514FA9D7DA1ECAD43_CustomAttributesCacheGenerator_ArCamera(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x20\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void ModularBillBoard_tECE1BE125DDD1BAA622B28A514FA9D7DA1ECAD43_CustomAttributesCacheGenerator_LandmarkIcon(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6E\x76\x61\x73\x20\x43\x6F\x6E\x74\x65\x6E\x74"), NULL);
	}
}
static void ModularBillBoard_tECE1BE125DDD1BAA622B28A514FA9D7DA1ECAD43_CustomAttributesCacheGenerator_Content(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x74\x68\x65\x72\x20\x43\x6F\x6E\x74\x65\x6E\x74"), NULL);
	}
}
static void ModularBillBoard_tECE1BE125DDD1BAA622B28A514FA9D7DA1ECAD43_CustomAttributesCacheGenerator_OpeningStarted(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x63\x6F\x6E\x20\x4F\x70\x65\x6E\x69\x6E\x67\x20\x52\x61\x6E\x67\x65"), NULL);
	}
}
static void NavMeshManager_t993A22B85C827B8726B8C1F0929322DB1F97FDD1_CustomAttributesCacheGenerator_AreaTargetTransform(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x72\x65\x61\x20\x54\x61\x72\x67\x65\x74\x20\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void NavMeshManager_t993A22B85C827B8726B8C1F0929322DB1F97FDD1_CustomAttributesCacheGenerator_ArCameraTransform(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x76\x69\x67\x61\x74\x69\x6F\x6E\x20\x41\x67\x65\x6E\x74\x20\x61\x6E\x64\x20\x69\x74\x73\x20\x41\x52\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void NavMeshManager_t993A22B85C827B8726B8C1F0929322DB1F97FDD1_CustomAttributesCacheGenerator_NavigationLine(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x76\x69\x67\x61\x74\x69\x6F\x6E\x20\x4C\x69\x6E\x65"), NULL);
	}
}
static void Augmentation_t22FF38113B92AADB13A86CE785B5C6DF02273A90_CustomAttributesCacheGenerator_Augmentation_WaitForThen_mA4589585D567DF3C14D9A5A0552310FC18D8CE7B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_0_0_0_var), NULL);
	}
}
static void U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_CustomAttributesCacheGenerator_U3CWaitForThenU3Ed__11__ctor_mFFFBEE7F2D8F86205B3CBEAD655B12E45CD95F1D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_CustomAttributesCacheGenerator_U3CWaitForThenU3Ed__11_System_IDisposable_Dispose_m7F60964BF9A4B184C86603C54221EE480F58660B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_CustomAttributesCacheGenerator_U3CWaitForThenU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1626491025BBF0AB6C9162787B3EA2E7B1DB49FD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_CustomAttributesCacheGenerator_U3CWaitForThenU3Ed__11_System_Collections_IEnumerator_Reset_m2B4FEFC0BC9F8B009BD0D522ECBABDAF9400ECBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_CustomAttributesCacheGenerator_U3CWaitForThenU3Ed__11_System_Collections_IEnumerator_get_Current_mE570029CC6814163A3D05CC00A50545D1455B2C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_tE954A04C4C8CEDE15F0534E6CE98FDE97C6D47AC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void indirmedeneme_t8FE3BD0A902BD9751E770FD2FD327B4413943EA5_CustomAttributesCacheGenerator_indirmedeneme_DownloadAssetBundle_m5BBEF88A094AB650B20E49253205735D38EC8A58(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_0_0_0_var), NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__3__ctor_mFF9F5075D624368E8FC900F023EB07A1078D442D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__3_System_IDisposable_Dispose_m7C1D676C06CF11FCF991A7ACD7EEC1BAB2247A1B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m08AA6ED27E147482E5FE7BBD37F785AEB95FB2FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__3_System_Collections_IEnumerator_Reset_m302FCF7F3EBEB51FAA89277A789E5E3BC301D403(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__3_System_Collections_IEnumerator_get_Current_m4DC90CC5A391D8FE9697C8BECA79B7F3EFF7B193(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[73] = 
{
	U3CU3Ec__DisplayClass12_0_tF82578CDE88026909D98AA0584DD60EDBAA86616_CustomAttributesCacheGenerator,
	U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_CustomAttributesCacheGenerator,
	U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_CustomAttributesCacheGenerator,
	U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_CustomAttributesCacheGenerator,
	U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_CustomAttributesCacheGenerator,
	U3CU3Ec_tE8121A843C65E2DA1DDBDC18D287157848DA26CE_CustomAttributesCacheGenerator,
	U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_CustomAttributesCacheGenerator,
	U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_CustomAttributesCacheGenerator,
	LineRendererPatternShifter_t403FB1A6D6B70FB2EAED67C04F378A95BD559797_CustomAttributesCacheGenerator,
	U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_CustomAttributesCacheGenerator,
	U3CU3Ec_tE954A04C4C8CEDE15F0534E6CE98FDE97C6D47AC_CustomAttributesCacheGenerator,
	U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_CustomAttributesCacheGenerator,
	Create3DModel_t911E541B22429F7C1EE53AC4DFCAF58A2329A073_CustomAttributesCacheGenerator_targetTransform,
	MerkezeGoreBoyutlandir_t607A9B26417F72BD75EAE26E5E5F32115F87CA52_CustomAttributesCacheGenerator_scrollParent,
	MerkezeGoreBoyutlandir_t607A9B26417F72BD75EAE26E5E5F32115F87CA52_CustomAttributesCacheGenerator_boyutlandirmaOrani,
	MerkezeGoreBoyutlandir_t607A9B26417F72BD75EAE26E5E5F32115F87CA52_CustomAttributesCacheGenerator_yon,
	ModularBillBoard_tECE1BE125DDD1BAA622B28A514FA9D7DA1ECAD43_CustomAttributesCacheGenerator_ArCamera,
	ModularBillBoard_tECE1BE125DDD1BAA622B28A514FA9D7DA1ECAD43_CustomAttributesCacheGenerator_LandmarkIcon,
	ModularBillBoard_tECE1BE125DDD1BAA622B28A514FA9D7DA1ECAD43_CustomAttributesCacheGenerator_Content,
	ModularBillBoard_tECE1BE125DDD1BAA622B28A514FA9D7DA1ECAD43_CustomAttributesCacheGenerator_OpeningStarted,
	NavMeshManager_t993A22B85C827B8726B8C1F0929322DB1F97FDD1_CustomAttributesCacheGenerator_AreaTargetTransform,
	NavMeshManager_t993A22B85C827B8726B8C1F0929322DB1F97FDD1_CustomAttributesCacheGenerator_ArCameraTransform,
	NavMeshManager_t993A22B85C827B8726B8C1F0929322DB1F97FDD1_CustomAttributesCacheGenerator_NavigationLine,
	BtnPrefabScript_tD918A72763276317AD81FA3E9C72D5014A92F3E3_CustomAttributesCacheGenerator_BtnPrefabScript_U3CDownloadVerticalButtonsPhotosU3Eb__12_0_mC76C117FC1139565C8B47F663D4B1FC4B7445A9C,
	Create3DModel_t911E541B22429F7C1EE53AC4DFCAF58A2329A073_CustomAttributesCacheGenerator_Create3DModel_DownloadAssetBundle_mCA1A34D5EE51EC4AC314555016F106BCA535C08E,
	U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__11__ctor_mD5B96E18A4598751D9D323F9DE32E1256E85CD52,
	U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__11_System_IDisposable_Dispose_m3247B77C318D87D59E280129CC1EBDE8DC812930,
	U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9989BD403603E7630A3FC51367E31B1E2117DBBD,
	U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__11_System_Collections_IEnumerator_Reset_mE49F30B824501D89E1C8CA52A2127FA44CEF97BA,
	U3CDownloadAssetBundleU3Ed__11_t69F9439512C401B5986D7C688CA535FBD5307B8F_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__11_System_Collections_IEnumerator_get_Current_m280482882ED846B0B6EF97FE3E980C350FE1055A,
	DownloadAssetBundleExtension_tD735DD43C905CF1C39EE2BFE13BE870A1DE208D2_CustomAttributesCacheGenerator_DownloadAssetBundleExtension_DownloadAssetBundle_m402969E102F8C322D4BFBB7282CC99E1C04A14EC,
	U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__0__ctor_m7B2BF0DF356348556FC7042A3AE4A685755D5DE0,
	U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__0_System_IDisposable_Dispose_m7AFBF52E380519A03B91A866EE543235CCE30139,
	U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6BA65CE24E036AEB241CC688912C5C0BBF77F321,
	U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__0_System_Collections_IEnumerator_Reset_m95EEC0E276163815A0621D083A5245BA80D0E806,
	U3CDownloadAssetBundleU3Ed__0_tD6D0277F6DD4547270EDE336FE3C36C23A6DB69A_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__0_System_Collections_IEnumerator_get_Current_m2376DACBEBE824FDE4BC450E4AB22942DB8AAE7A,
	DownloadImageExtensions_t013A125B9853322BD6718CB1DC2315C88C2759A9_CustomAttributesCacheGenerator_DownloadImageExtensions_DownloadImage_m425EFA7E5A50937CE8EEE15F9D0067CF80BFE8A6,
	U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__0__ctor_m24345C0A4F0AF3E8FE9AE84E784DBB6034C9E009,
	U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__0_System_IDisposable_Dispose_m565B46EF4ABF21CEF4DC0689E0FC9FA6BB94F7FF,
	U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDF12CBB291BAB27BBF99AF1DC0B7EBC3F89DC9D,
	U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__0_System_Collections_IEnumerator_Reset_mDA3A2F9B4C6CC57A732C0CE780504A4C486E0245,
	U3CDownloadImageU3Ed__0_t928D5F7A6B79E5B8960A7B320190689D5BECEFFB_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__0_System_Collections_IEnumerator_get_Current_mD8EAA09FDD726A953D8984E16355CA7FA8A6FB4C,
	AssetBundleAugmenter_t2F3C36AFE2FD9BFE87451A343806D3381DECDC9C_CustomAttributesCacheGenerator_AssetBundleAugmenter_DownloadAndCache_m827A785F9BA8DF539DDF5F123704DF6676456951,
	U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__6__ctor_mC207A786AC388526B65793D9B7A3CD21637E7FE7,
	U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__6_System_IDisposable_Dispose_m1232F60243AEDD6A732706374CA4B4291EA423EC,
	U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m45D9FC362C3EF9B098B0D929053584864B3D3801,
	U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__6_System_Collections_IEnumerator_Reset_m1C26776B3F6459BAC1EEBCB840B1CEB3A0F0B5CB,
	U3CDownloadAndCacheU3Ed__6_t778629A3F3C7C3AE3BC869998831F0A37A4AF557_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__6_System_Collections_IEnumerator_get_Current_mD133B8114A8DF8E11DD7D96976E3AF9257A12C56,
	CachingLoadExample_t834CDE8453BA379D396A43E309AEEBD64279D1DC_CustomAttributesCacheGenerator_CachingLoadExample_DownloadAndCache_mF4487DC82E54CF46DD031998ACFF4DC7370E882B,
	U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__4__ctor_m4F21C539BBFCA4BA9FB7EC3A671F6138DEE5E249,
	U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__4_System_IDisposable_Dispose_mD02B8042829A3C35444BC73F7665F90E5D91D0DD,
	U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1EBBB1958D5DD1DA2D998CA242D213D6A60A8D76,
	U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__4_System_Collections_IEnumerator_Reset_m6E4ED7FA6DD400F4E6B4ABB2556E685C2A4132C8,
	U3CDownloadAndCacheU3Ed__4_t5CE580FF9381F090BBB8A0ACF26FCD047BFEB74C_CustomAttributesCacheGenerator_U3CDownloadAndCacheU3Ed__4_System_Collections_IEnumerator_get_Current_mA1313E5FECCB8B642D0B69ADA6546EA3CEE028BA,
	script_t9023BF623D4A6A9D9C11357DC6489ED305B06088_CustomAttributesCacheGenerator_script_Start_mBDC0E6710997FCBF56EB900A4B5F22EB9B94A1AE,
	U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_CustomAttributesCacheGenerator_U3CStartU3Ed__2__ctor_mF5705C0C398CD8684E58523E9DA12AA6C0B51E2A,
	U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_IDisposable_Dispose_m0D240E2FDBED87E1E915A2689C41CF809807E00A,
	U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0B3B79FF648B8964506DD8D39E66DEC99CD268ED,
	U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m1453297B53E3241F93D9F46A973072628FD12E5C,
	U3CStartU3Ed__2_t6DD95E4C4AFB795A298F7379E431A28561757F8E_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mCF672EEEF34AAC75D10821A8CC8FFE11F2589942,
	Augmentation_t22FF38113B92AADB13A86CE785B5C6DF02273A90_CustomAttributesCacheGenerator_Augmentation_WaitForThen_mA4589585D567DF3C14D9A5A0552310FC18D8CE7B,
	U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_CustomAttributesCacheGenerator_U3CWaitForThenU3Ed__11__ctor_mFFFBEE7F2D8F86205B3CBEAD655B12E45CD95F1D,
	U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_CustomAttributesCacheGenerator_U3CWaitForThenU3Ed__11_System_IDisposable_Dispose_m7F60964BF9A4B184C86603C54221EE480F58660B,
	U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_CustomAttributesCacheGenerator_U3CWaitForThenU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1626491025BBF0AB6C9162787B3EA2E7B1DB49FD,
	U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_CustomAttributesCacheGenerator_U3CWaitForThenU3Ed__11_System_Collections_IEnumerator_Reset_m2B4FEFC0BC9F8B009BD0D522ECBABDAF9400ECBB,
	U3CWaitForThenU3Ed__11_t33F348C47748D4F87363F6974B97D71640E79B78_CustomAttributesCacheGenerator_U3CWaitForThenU3Ed__11_System_Collections_IEnumerator_get_Current_mE570029CC6814163A3D05CC00A50545D1455B2C6,
	indirmedeneme_t8FE3BD0A902BD9751E770FD2FD327B4413943EA5_CustomAttributesCacheGenerator_indirmedeneme_DownloadAssetBundle_m5BBEF88A094AB650B20E49253205735D38EC8A58,
	U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__3__ctor_mFF9F5075D624368E8FC900F023EB07A1078D442D,
	U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__3_System_IDisposable_Dispose_m7C1D676C06CF11FCF991A7ACD7EEC1BAB2247A1B,
	U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m08AA6ED27E147482E5FE7BBD37F785AEB95FB2FE,
	U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__3_System_Collections_IEnumerator_Reset_m302FCF7F3EBEB51FAA89277A789E5E3BC301D403,
	U3CDownloadAssetBundleU3Ed__3_t06F3B7D46560B515C858928207454305028134B4_CustomAttributesCacheGenerator_U3CDownloadAssetBundleU3Ed__3_System_Collections_IEnumerator_get_Current_m4DC90CC5A391D8FE9697C8BECA79B7F3EFF7B193,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}

﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.ContextMenu::menuItem
	String_t* ___menuItem_0;
	// System.Boolean UnityEngine.ContextMenu::validate
	bool ___validate_1;
	// System.Int32 UnityEngine.ContextMenu::priority
	int32_t ___priority_2;

public:
	inline static int32_t get_offset_of_menuItem_0() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___menuItem_0)); }
	inline String_t* get_menuItem_0() const { return ___menuItem_0; }
	inline String_t** get_address_of_menuItem_0() { return &___menuItem_0; }
	inline void set_menuItem_0(String_t* value)
	{
		___menuItem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menuItem_0), (void*)value);
	}

	inline static int32_t get_offset_of_validate_1() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___validate_1)); }
	inline bool get_validate_1() const { return ___validate_1; }
	inline bool* get_address_of_validate_1() { return &___validate_1; }
	inline void set_validate_1(bool value)
	{
		___validate_1 = value;
	}

	inline static int32_t get_offset_of_priority_2() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___priority_2)); }
	inline int32_t get_priority_2() const { return ___priority_2; }
	inline int32_t* get_address_of_priority_2() { return &___priority_2; }
	inline void set_priority_2(int32_t value)
	{
		___priority_2 = value;
	}
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 UnityEngine.DefaultExecutionOrder::m_Order
	int32_t ___m_Order_0;

public:
	inline static int32_t get_offset_of_m_Order_0() { return static_cast<int32_t>(offsetof(DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F, ___m_Order_0)); }
	inline int32_t get_m_Order_0() const { return ___m_Order_0; }
	inline int32_t* get_address_of_m_Order_0() { return &___m_Order_0; }
	inline void set_m_Order_0(int32_t value)
	{
		___m_Order_0 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};


// System.Object


// System.Object


// System.Attribute


// System.Attribute


// System.Reflection.MemberInfo


// System.Reflection.MemberInfo


// System.String

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.String


// System.ValueType


// System.ValueType


// System.Boolean

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Boolean


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute


// System.Runtime.CompilerServices.CompilerGeneratedAttribute


// System.Runtime.CompilerServices.CompilerGeneratedAttribute


// UnityEngine.ContextMenu


// UnityEngine.ContextMenu


// UnityEngine.CreateAssetMenuAttribute


// UnityEngine.CreateAssetMenuAttribute


// System.Diagnostics.DebuggerHiddenAttribute


// System.Diagnostics.DebuggerHiddenAttribute


// UnityEngine.DefaultExecutionOrder


// UnityEngine.DefaultExecutionOrder


// System.Enum

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};


// System.Enum


// UnityEngine.Serialization.FormerlySerializedAsAttribute


// UnityEngine.Serialization.FormerlySerializedAsAttribute


// System.Int32


// System.Int32


// System.IntPtr

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.IntPtr


// System.ParamArrayAttribute


// System.ParamArrayAttribute


// UnityEngine.PropertyAttribute


// UnityEngine.PropertyAttribute


// UnityEngine.RequireComponent


// UnityEngine.RequireComponent


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute


// UnityEngine.SerializeField


// UnityEngine.SerializeField


// System.Runtime.CompilerServices.StateMachineAttribute


// System.Runtime.CompilerServices.StateMachineAttribute


// System.Void


// System.Void


// System.Reflection.BindingFlags


// System.Reflection.BindingFlags


// UnityEngine.HeaderAttribute


// UnityEngine.HeaderAttribute


// System.Runtime.CompilerServices.IteratorStateMachineAttribute


// System.Runtime.CompilerServices.IteratorStateMachineAttribute


// System.RuntimeTypeHandle


// System.RuntimeTypeHandle


// System.Diagnostics.DebuggableAttribute/DebuggingModes


// System.Diagnostics.DebuggableAttribute/DebuggingModes


// System.Diagnostics.DebuggableAttribute


// System.Diagnostics.DebuggableAttribute


// System.Type

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Type

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.DefaultExecutionOrder::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9 (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * __this, int32_t ___order0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ContextMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * __this, String_t* ___itemName0, const RuntimeMethod* method);
static void SimpleFileBrowser_Runtime_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tE608D07C839B5A154A5B6D2877B1E03CC482E9A4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBPermissionCallbackAndroid_t590C05274E5ABAE813EBCC8ADB5B194CA11E7A09_CustomAttributesCacheGenerator_U3CResultU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBPermissionCallbackAndroid_t590C05274E5ABAE813EBCC8ADB5B194CA11E7A09_CustomAttributesCacheGenerator_FBPermissionCallbackAndroid_get_Result_m131BEEFCA19EC6152CEC8DB8C88EA1D8A56E3FA8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FBPermissionCallbackAndroid_t590C05274E5ABAE813EBCC8ADB5B194CA11E7A09_CustomAttributesCacheGenerator_FBPermissionCallbackAndroid_set_Result_m213D385F82F35521EDEEFCAE6FAF01DEF27F3458(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventSystemHandler_t3FA67703E6F09AA291CC6AC5D2CEE120FE9A2429_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * tmp = (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F *)cache->attributes[0];
		DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9(tmp, 1000LL, NULL);
	}
}
static void EventSystemHandler_t3FA67703E6F09AA291CC6AC5D2CEE120FE9A2429_CustomAttributesCacheGenerator_embeddedEventSystem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_U3CIsOpenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_U3CSuccessU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_U3CResultU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_m_skin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_minWidth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_minHeight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_narrowScreenWidth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_quickLinksMaxWidthPercentage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_sortFilesByName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_excludedExtensions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x65\x78\x63\x6C\x75\x64\x65\x45\x78\x74\x65\x6E\x73\x69\x6F\x6E\x73"), NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_quickLinks(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_generateQuickLinksForDrives(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_contextMenuShowDeleteButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_contextMenuShowRenameButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_showResizeCursor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_window(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x72\x6E\x61\x6C\x20\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x73"), NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_topViewNarrowScreen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_middleView(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_middleViewQuickLinks(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_middleViewFiles(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_middleViewSeparator(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_itemPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_quickLinkPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_titleText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_backButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_forwardButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_upButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_moreOptionsButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_pathInputField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_pathInputFieldSlotTop(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_pathInputFieldSlotBottom(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_searchInputField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_quickLinksContainer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_quickLinksScrollRect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filesContainer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filesScrollRect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_listView(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filenameInputField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filenameInputFieldOverlayText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filenameImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filtersDropdown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filtersDropdownContainer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filterItemTemplate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_showHiddenFilesToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_submitButtonText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_allButtons(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_moreOptionsContextMenuPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_renameItem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_contextMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_fileOperationConfirmationPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_accessRestrictedPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_resizeCursorHandler(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_get_IsOpen_m76F153181B2F972690A98BB60FA0B3E6E72ED7C6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_set_IsOpen_m132B34485185EB72B9F840606547745EACF08B6E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_get_Success_mB13435856C4E9A5FC6B87057E8800CD4473D00C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_set_Success_mF82FCE01FA9934B22AB6AAE52484801AD5521320(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_get_Result_mA521CA35A57DFB25B29C5CAEB5BCF94F3B5D5D44(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_set_Result_mAC115C70FD40098215B44FB277BFCC4D49D56CF3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_CreateNewFolderCoroutine_mCC4E6964B0EEDC56E8C937CEBBFEAEA262C8581E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_0_0_0_var), NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_WaitForSaveDialog_m5454CF697CF1BE9FBD027F7020E98E095BEAA7FD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_0_0_0_var), NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_WaitForLoadDialog_mE110ACB9DAAEC9C617822A5A2001382F345B58C9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_0_0_0_var), NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_SetExcludedExtensions_mA9BC5DC0482E7055BF2DD270B3874BB0A31FD709____excludedExtensions0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_SetFilters_mAFC93DB0463A70423C79B058BD36CE01AFE9D84D____filters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_SetFilters_mB60CC9235A36041CF175CE8BC913E3B961D58B77____filters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_U3CCreateNewFolderCoroutineU3Eb__241_0_mE4B278D5E88BC29E2F4F88DDA2F471F57ED0376D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_U3CDeleteSelectedFilesU3Eb__243_0_mC8B8270D31AF46160629E570D96CFC769CA83308(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8_CustomAttributesCacheGenerator_Filter__ctor_m125246E9D958C713FF3D86B5E02C9D6540B58185____extensions1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass220_0_t3D20A2B3DD8845AD34093E57F5B4DE45DBB95600_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_CustomAttributesCacheGenerator_U3CCreateNewFolderCoroutineU3Ed__241__ctor_mDFD38C6B4A6455AEDEE398C93FA8C252AE88184B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_CustomAttributesCacheGenerator_U3CCreateNewFolderCoroutineU3Ed__241_System_IDisposable_Dispose_mB74B115992FE4B68A55E768310A997AC20A20926(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_CustomAttributesCacheGenerator_U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0545F6447CE56AA4E999A8B6450E7F30A310430D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_CustomAttributesCacheGenerator_U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_IEnumerator_Reset_m5F77E4A7CA436A621AC00B2073FE17DB3EFF2850(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_CustomAttributesCacheGenerator_U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_IEnumerator_get_Current_m8E8B43EAB0828F57B8691DBEADCF694AA88101DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass242_0_t5A54DD142E562A9BF717F627B31C8F71C0E0FFDE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_CustomAttributesCacheGenerator_U3CWaitForSaveDialogU3Ed__265__ctor_m12F637ABD0A5EBB97C466D0F553D5B8D56033BD1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_CustomAttributesCacheGenerator_U3CWaitForSaveDialogU3Ed__265_System_IDisposable_Dispose_m7BBF2D25C1C1AC74A18CF70C32FF1FDEC15F9459(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_CustomAttributesCacheGenerator_U3CWaitForSaveDialogU3Ed__265_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9186D456077EC88F0B778027AD680C149E3D74CE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_CustomAttributesCacheGenerator_U3CWaitForSaveDialogU3Ed__265_System_Collections_IEnumerator_Reset_m38718E5D5CE7AA13F1F4B684775CED69286E1390(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_CustomAttributesCacheGenerator_U3CWaitForSaveDialogU3Ed__265_System_Collections_IEnumerator_get_Current_mE3601AB26ED57A39433E48BB8716C87BD6F45B0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_CustomAttributesCacheGenerator_U3CWaitForLoadDialogU3Ed__266__ctor_m3F8B1CF0E9CA9644D9E02111A683BD10962D1F5D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_CustomAttributesCacheGenerator_U3CWaitForLoadDialogU3Ed__266_System_IDisposable_Dispose_mEF304AF3D0BAE96043ECA7DAD60C537FBB38A35C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_CustomAttributesCacheGenerator_U3CWaitForLoadDialogU3Ed__266_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE96087E920FB48FA18B5996C934D369C33B9B75(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_CustomAttributesCacheGenerator_U3CWaitForLoadDialogU3Ed__266_System_Collections_IEnumerator_Reset_m783AE76EC116144B63DC8BE626E95732D3C0BF27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_CustomAttributesCacheGenerator_U3CWaitForLoadDialogU3Ed__266_System_Collections_IEnumerator_get_Current_mCF552CCACF7B0B0ED71A0EE750D029528C487043(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void FileBrowserAccessRestrictedPanel_t96A484C5166B6EF23402D06FF2107AC3ED6ECB30_CustomAttributesCacheGenerator_messageLabel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserAccessRestrictedPanel_t96A484C5166B6EF23402D06FF2107AC3ED6ECB30_CustomAttributesCacheGenerator_okButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_fileBrowser(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_rectTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_selectAllButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_deselectAllButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_createFolderButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_deleteButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_renameButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_selectAllButtonSeparator(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_allButtonTexts(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_allButtonSeparators(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_minDistanceToEdges(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_titleLabels(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_targetItems(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_targetItemIcons(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_targetItemNames(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_targetItemsRest(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_targetItemsRestLabel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_yesButtonTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_noButtonTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_narrowScreenWidth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_background(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_icon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_multiSelectionToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_nameText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_U3CIsDirectoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_FileBrowserItem_get_IsDirectory_mC40A8B51825BC34EB34D45C72D35A1CA6E1A89BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_FileBrowserItem_set_IsDirectory_mDB0D5D005A9C6F689AF30D6A63E8AE555FFF6DF6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19_CustomAttributesCacheGenerator_window(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19_CustomAttributesCacheGenerator_listView(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4_CustomAttributesCacheGenerator_background(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4_CustomAttributesCacheGenerator_icon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4_CustomAttributesCacheGenerator_nameInputField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NonDrawingGraphic_t14C8D2E57B2CC0F4160CBB2D73DA16E0D0966345_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E_0_0_0_var), NULL);
	}
}
static void ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var), NULL);
	}
}
static void ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator_U3CTagU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator_ListItem_get_Tag_mA71AE4254B5B31A3B822DB8F1674D50AB25A3041(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator_ListItem_set_Tag_m74393E301D898520AB53B5AA336ACFBC97EC787E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator_ListItem_get_Position_mB4639770E22F328A1496CDCA7FCB936D65E1FFAE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator_ListItem_set_Position_mD18CBDB9DDB4D5C2E68E5CFA0B2C7D2B94540DFE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA_0_0_0_var), NULL);
	}
}
static void RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767_CustomAttributesCacheGenerator_viewportTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767_CustomAttributesCacheGenerator_contentTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767_CustomAttributesCacheGenerator_RecycledListView_U3CStartU3Eb__10_0_m24E1DAB94C847738A00D58ED91844281DAFC0696(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x53\x6B\x69\x6E"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x79\x61\x73\x69\x72\x6B\x75\x6C\x61\x2F\x53\x69\x6D\x70\x6C\x65\x46\x69\x6C\x65\x42\x72\x6F\x77\x73\x65\x72\x2F\x55\x49\x20\x53\x6B\x69\x6E"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 111LL, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_font(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x6C"), NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fontSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_windowColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6C\x65\x20\x42\x72\x6F\x77\x73\x65\x72\x20\x57\x69\x6E\x64\x6F\x77"), NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_filesListColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_filesVerticalSeparatorColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_titleBackgroundColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_titleTextColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_windowResizeGizmoColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_headerButtonsColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_windowResizeGizmo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_headerBackButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_headerForwardButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_headerUpButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_headerContextMenuButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldNormalBackgroundColor(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x70\x75\x74\x20\x46\x69\x65\x6C\x64\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldInvalidBackgroundColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldTextColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldPlaceholderTextColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldSelectedTextColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldCaretColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldBackground(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_buttonColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x75\x74\x74\x6F\x6E\x73"), NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_buttonTextColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_buttonBackground(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x72\x6F\x70\x64\x6F\x77\x6E\x73"), NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownTextColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownArrowColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownCheckmarkColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownBackground(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownArrow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownCheckmark(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_toggleColor(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x6F\x67\x67\x6C\x65\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_toggleTextColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_toggleCheckmarkColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_toggleBackground(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_toggleCheckmark(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_scrollbarBackgroundColor(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x6F\x6C\x6C\x62\x61\x72\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_scrollbarColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileHeight(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6C\x65\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileIconsPadding(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileNormalBackgroundColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileAlternatingBackgroundColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileHoveredBackgroundColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileSelectedBackgroundColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileNormalTextColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileSelectedTextColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_folderIcon(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6C\x65\x20\x49\x63\x6F\x6E\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_driveIcon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_defaultFileIcon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_filetypeIcons(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileMultiSelectionToggleOffIcon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileMultiSelectionToggleOnIcon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_contextMenuBackgroundColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x74\x65\x78\x74\x20\x4D\x65\x6E\x75"), NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_contextMenuTextColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_contextMenuSeparatorColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_popupPanelsBackgroundColor(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x70\x75\x70\x20\x50\x61\x6E\x65\x6C\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x5F\x64\x65\x6C\x65\x74\x65\x50\x61\x6E\x65\x6C\x42\x61\x63\x6B\x67\x72\x6F\x75\x6E\x64\x43\x6F\x6C\x6F\x72"), NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_popupPanelsTextColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x5F\x64\x65\x6C\x65\x74\x65\x50\x61\x6E\x65\x6C\x54\x65\x78\x74\x43\x6F\x6C\x6F\x72"), NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_popupPanelsBackground(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x5F\x64\x65\x6C\x65\x74\x65\x50\x61\x6E\x65\x6C\x42\x61\x63\x6B\x67\x72\x6F\x75\x6E\x64"), NULL);
	}
}
static void UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_UISkin_Invalidate_mF0FE40247D7E67A8FBC84DEC8EA36BE9E11ED089(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x72\x65\x73\x68\x20\x55\x49"), NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_SimpleFileBrowser_Runtime_AttributeGenerators[];
const CustomAttributesCacheGenerator g_SimpleFileBrowser_Runtime_AttributeGenerators[201] = 
{
	U3CU3Ec__DisplayClass3_0_tE608D07C839B5A154A5B6D2877B1E03CC482E9A4_CustomAttributesCacheGenerator,
	EventSystemHandler_t3FA67703E6F09AA291CC6AC5D2CEE120FE9A2429_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass220_0_t3D20A2B3DD8845AD34093E57F5B4DE45DBB95600_CustomAttributesCacheGenerator,
	U3CU3Ec_t9D0E62F41F741FE144A75483D46B58B8559E463D_CustomAttributesCacheGenerator,
	U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass242_0_t5A54DD142E562A9BF717F627B31C8F71C0E0FFDE_CustomAttributesCacheGenerator,
	U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_CustomAttributesCacheGenerator,
	U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_CustomAttributesCacheGenerator,
	NonDrawingGraphic_t14C8D2E57B2CC0F4160CBB2D73DA16E0D0966345_CustomAttributesCacheGenerator,
	ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator,
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767_CustomAttributesCacheGenerator,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator,
	FBPermissionCallbackAndroid_t590C05274E5ABAE813EBCC8ADB5B194CA11E7A09_CustomAttributesCacheGenerator_U3CResultU3Ek__BackingField,
	EventSystemHandler_t3FA67703E6F09AA291CC6AC5D2CEE120FE9A2429_CustomAttributesCacheGenerator_embeddedEventSystem,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_U3CIsOpenU3Ek__BackingField,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_U3CSuccessU3Ek__BackingField,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_U3CResultU3Ek__BackingField,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_m_skin,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_minWidth,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_minHeight,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_narrowScreenWidth,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_quickLinksMaxWidthPercentage,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_sortFilesByName,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_excludedExtensions,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_quickLinks,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_generateQuickLinksForDrives,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_contextMenuShowDeleteButton,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_contextMenuShowRenameButton,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_showResizeCursor,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_window,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_topViewNarrowScreen,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_middleView,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_middleViewQuickLinks,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_middleViewFiles,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_middleViewSeparator,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_itemPrefab,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_quickLinkPrefab,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_titleText,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_backButton,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_forwardButton,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_upButton,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_moreOptionsButton,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_pathInputField,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_pathInputFieldSlotTop,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_pathInputFieldSlotBottom,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_searchInputField,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_quickLinksContainer,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_quickLinksScrollRect,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filesContainer,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filesScrollRect,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_listView,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filenameInputField,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filenameInputFieldOverlayText,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filenameImage,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filtersDropdown,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filtersDropdownContainer,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_filterItemTemplate,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_showHiddenFilesToggle,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_submitButtonText,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_allButtons,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_moreOptionsContextMenuPosition,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_renameItem,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_contextMenu,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_fileOperationConfirmationPanel,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_accessRestrictedPanel,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_resizeCursorHandler,
	FileBrowserAccessRestrictedPanel_t96A484C5166B6EF23402D06FF2107AC3ED6ECB30_CustomAttributesCacheGenerator_messageLabel,
	FileBrowserAccessRestrictedPanel_t96A484C5166B6EF23402D06FF2107AC3ED6ECB30_CustomAttributesCacheGenerator_okButton,
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_fileBrowser,
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_rectTransform,
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_selectAllButton,
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_deselectAllButton,
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_createFolderButton,
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_deleteButton,
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_renameButton,
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_selectAllButtonSeparator,
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_allButtonTexts,
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_allButtonSeparators,
	FileBrowserContextMenu_tE34C33B86796FBC055E7F6829571820BE778EECC_CustomAttributesCacheGenerator_minDistanceToEdges,
	FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_titleLabels,
	FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_targetItems,
	FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_targetItemIcons,
	FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_targetItemNames,
	FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_targetItemsRest,
	FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_targetItemsRestLabel,
	FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_yesButtonTransform,
	FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_noButtonTransform,
	FileBrowserFileOperationConfirmationPanel_tF69293C66047E83D95593D21390B387C3D7D3229_CustomAttributesCacheGenerator_narrowScreenWidth,
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_background,
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_icon,
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_multiSelectionToggle,
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_nameText,
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_U3CIsDirectoryU3Ek__BackingField,
	FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19_CustomAttributesCacheGenerator_window,
	FileBrowserMovement_t8361BFC09BAC98111B660568AA86FCF699377C19_CustomAttributesCacheGenerator_listView,
	FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4_CustomAttributesCacheGenerator_background,
	FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4_CustomAttributesCacheGenerator_icon,
	FileBrowserRenamedItem_t72AF428EFEAF291CD9E1858EDEDB17A04D99CDA4_CustomAttributesCacheGenerator_nameInputField,
	ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator_U3CTagU3Ek__BackingField,
	ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField,
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767_CustomAttributesCacheGenerator_viewportTransform,
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767_CustomAttributesCacheGenerator_contentTransform,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_font,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fontSize,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_windowColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_filesListColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_filesVerticalSeparatorColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_titleBackgroundColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_titleTextColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_windowResizeGizmoColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_headerButtonsColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_windowResizeGizmo,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_headerBackButton,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_headerForwardButton,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_headerUpButton,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_headerContextMenuButton,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldNormalBackgroundColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldInvalidBackgroundColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldTextColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldPlaceholderTextColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldSelectedTextColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldCaretColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_inputFieldBackground,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_buttonColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_buttonTextColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_buttonBackground,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownTextColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownArrowColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownCheckmarkColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownBackground,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownArrow,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_dropdownCheckmark,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_toggleColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_toggleTextColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_toggleCheckmarkColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_toggleBackground,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_toggleCheckmark,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_scrollbarBackgroundColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_scrollbarColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileHeight,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileIconsPadding,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileNormalBackgroundColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileAlternatingBackgroundColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileHoveredBackgroundColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileSelectedBackgroundColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileNormalTextColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileSelectedTextColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_folderIcon,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_driveIcon,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_defaultFileIcon,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_filetypeIcons,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileMultiSelectionToggleOffIcon,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_fileMultiSelectionToggleOnIcon,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_contextMenuBackgroundColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_contextMenuTextColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_contextMenuSeparatorColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_popupPanelsBackgroundColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_popupPanelsTextColor,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_m_popupPanelsBackground,
	FBPermissionCallbackAndroid_t590C05274E5ABAE813EBCC8ADB5B194CA11E7A09_CustomAttributesCacheGenerator_FBPermissionCallbackAndroid_get_Result_m131BEEFCA19EC6152CEC8DB8C88EA1D8A56E3FA8,
	FBPermissionCallbackAndroid_t590C05274E5ABAE813EBCC8ADB5B194CA11E7A09_CustomAttributesCacheGenerator_FBPermissionCallbackAndroid_set_Result_m213D385F82F35521EDEEFCAE6FAF01DEF27F3458,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_get_IsOpen_m76F153181B2F972690A98BB60FA0B3E6E72ED7C6,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_set_IsOpen_m132B34485185EB72B9F840606547745EACF08B6E,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_get_Success_mB13435856C4E9A5FC6B87057E8800CD4473D00C7,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_set_Success_mF82FCE01FA9934B22AB6AAE52484801AD5521320,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_get_Result_mA521CA35A57DFB25B29C5CAEB5BCF94F3B5D5D44,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_set_Result_mAC115C70FD40098215B44FB277BFCC4D49D56CF3,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_CreateNewFolderCoroutine_mCC4E6964B0EEDC56E8C937CEBBFEAEA262C8581E,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_WaitForSaveDialog_m5454CF697CF1BE9FBD027F7020E98E095BEAA7FD,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_WaitForLoadDialog_mE110ACB9DAAEC9C617822A5A2001382F345B58C9,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_U3CCreateNewFolderCoroutineU3Eb__241_0_mE4B278D5E88BC29E2F4F88DDA2F471F57ED0376D,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_U3CDeleteSelectedFilesU3Eb__243_0_mC8B8270D31AF46160629E570D96CFC769CA83308,
	U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_CustomAttributesCacheGenerator_U3CCreateNewFolderCoroutineU3Ed__241__ctor_mDFD38C6B4A6455AEDEE398C93FA8C252AE88184B,
	U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_CustomAttributesCacheGenerator_U3CCreateNewFolderCoroutineU3Ed__241_System_IDisposable_Dispose_mB74B115992FE4B68A55E768310A997AC20A20926,
	U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_CustomAttributesCacheGenerator_U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0545F6447CE56AA4E999A8B6450E7F30A310430D,
	U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_CustomAttributesCacheGenerator_U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_IEnumerator_Reset_m5F77E4A7CA436A621AC00B2073FE17DB3EFF2850,
	U3CCreateNewFolderCoroutineU3Ed__241_t11E33FBED5353F7A6085CE84873F14A0EE10A025_CustomAttributesCacheGenerator_U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_IEnumerator_get_Current_m8E8B43EAB0828F57B8691DBEADCF694AA88101DC,
	U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_CustomAttributesCacheGenerator_U3CWaitForSaveDialogU3Ed__265__ctor_m12F637ABD0A5EBB97C466D0F553D5B8D56033BD1,
	U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_CustomAttributesCacheGenerator_U3CWaitForSaveDialogU3Ed__265_System_IDisposable_Dispose_m7BBF2D25C1C1AC74A18CF70C32FF1FDEC15F9459,
	U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_CustomAttributesCacheGenerator_U3CWaitForSaveDialogU3Ed__265_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9186D456077EC88F0B778027AD680C149E3D74CE,
	U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_CustomAttributesCacheGenerator_U3CWaitForSaveDialogU3Ed__265_System_Collections_IEnumerator_Reset_m38718E5D5CE7AA13F1F4B684775CED69286E1390,
	U3CWaitForSaveDialogU3Ed__265_t46BA72F7BB4DEBBCE6D331F6EBF3FD56D5B52602_CustomAttributesCacheGenerator_U3CWaitForSaveDialogU3Ed__265_System_Collections_IEnumerator_get_Current_mE3601AB26ED57A39433E48BB8716C87BD6F45B0F,
	U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_CustomAttributesCacheGenerator_U3CWaitForLoadDialogU3Ed__266__ctor_m3F8B1CF0E9CA9644D9E02111A683BD10962D1F5D,
	U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_CustomAttributesCacheGenerator_U3CWaitForLoadDialogU3Ed__266_System_IDisposable_Dispose_mEF304AF3D0BAE96043ECA7DAD60C537FBB38A35C,
	U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_CustomAttributesCacheGenerator_U3CWaitForLoadDialogU3Ed__266_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE96087E920FB48FA18B5996C934D369C33B9B75,
	U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_CustomAttributesCacheGenerator_U3CWaitForLoadDialogU3Ed__266_System_Collections_IEnumerator_Reset_m783AE76EC116144B63DC8BE626E95732D3C0BF27,
	U3CWaitForLoadDialogU3Ed__266_t06AF75E267953541435C08ABFC5FF500FA4C9BFD_CustomAttributesCacheGenerator_U3CWaitForLoadDialogU3Ed__266_System_Collections_IEnumerator_get_Current_mCF552CCACF7B0B0ED71A0EE750D029528C487043,
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_FileBrowserItem_get_IsDirectory_mC40A8B51825BC34EB34D45C72D35A1CA6E1A89BB,
	FileBrowserItem_t840F0FDE6B7ECC6589B9E2E37E689BD819D56025_CustomAttributesCacheGenerator_FileBrowserItem_set_IsDirectory_mDB0D5D005A9C6F689AF30D6A63E8AE555FFF6DF6,
	ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator_ListItem_get_Tag_mA71AE4254B5B31A3B822DB8F1674D50AB25A3041,
	ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator_ListItem_set_Tag_m74393E301D898520AB53B5AA336ACFBC97EC787E,
	ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator_ListItem_get_Position_mB4639770E22F328A1496CDCA7FCB936D65E1FFAE,
	ListItem_tF4DB85736E0A828BBFC1DDFDA9926B86139651ED_CustomAttributesCacheGenerator_ListItem_set_Position_mD18CBDB9DDB4D5C2E68E5CFA0B2C7D2B94540DFE,
	RecycledListView_tDB80C41C939FD2E066CFE06F86C46D2675338767_CustomAttributesCacheGenerator_RecycledListView_U3CStartU3Eb__10_0_m24E1DAB94C847738A00D58ED91844281DAFC0696,
	UISkin_t986B4ABBB5AA0956056C4A32DAFED549C712CA58_CustomAttributesCacheGenerator_UISkin_Invalidate_mF0FE40247D7E67A8FBC84DEC8EA36BE9E11ED089,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_SetExcludedExtensions_mA9BC5DC0482E7055BF2DD270B3874BB0A31FD709____excludedExtensions0,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_SetFilters_mAFC93DB0463A70423C79B058BD36CE01AFE9D84D____filters1,
	FileBrowser_tD89767CC0F93F9CC656A0323682DCB02BF711AF8_CustomAttributesCacheGenerator_FileBrowser_SetFilters_mB60CC9235A36041CF175CE8BC913E3B961D58B77____filters1,
	Filter_tB9C16BF7DCE8B106732124C1722F70A081F6AAD8_CustomAttributesCacheGenerator_Filter__ctor_m125246E9D958C713FF3D86B5E02C9D6540B58185____extensions1,
	SimpleFileBrowser_Runtime_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_2(L_0);
		return;
	}
}

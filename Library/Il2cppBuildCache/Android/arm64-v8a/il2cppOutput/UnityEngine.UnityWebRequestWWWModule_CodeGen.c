﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,System.Int32)
extern void WWW_LoadFromCacheOrDownload_m2772A637BC4DC15E8EDD35B1612ABAD8E3AE5490 (void);
// 0x00000002 UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,System.Int32,System.UInt32)
extern void WWW_LoadFromCacheOrDownload_mCD2F8D4B41C321DC873CBC0C3CBEFC2260A49E15 (void);
// 0x00000003 UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,UnityEngine.Hash128,System.UInt32)
extern void WWW_LoadFromCacheOrDownload_m70A95398FA8D5927C25DEE4A28331AB3A45FE678 (void);
// 0x00000004 System.Void UnityEngine.WWW::.ctor(System.String)
extern void WWW__ctor_mE77AD6C372CC76F48A893C5E2F91A81193A9F8C5 (void);
// 0x00000005 System.Void UnityEngine.WWW::.ctor(System.String,System.String,UnityEngine.Hash128,System.UInt32)
extern void WWW__ctor_m443FACCAB63C95EB81924E1FF3B5ADE2D7875186 (void);
// 0x00000006 UnityEngine.AssetBundle UnityEngine.WWW::get_assetBundle()
extern void WWW_get_assetBundle_mE523FA754E97AAECE72EA84CBC5D1888E445DE9F (void);
// 0x00000007 System.Byte[] UnityEngine.WWW::get_bytes()
extern void WWW_get_bytes_m378FCCD8E91FB7FE7FA22E05BA3FE528CD7EAF1A (void);
// 0x00000008 System.String UnityEngine.WWW::get_error()
extern void WWW_get_error_mB278F5EC90EF99FEF70D80112940CFB49E79C9BC (void);
// 0x00000009 System.String UnityEngine.WWW::get_url()
extern void WWW_get_url_m1D75D492D78A7AA8F607C5D7700497B8FE5E9526 (void);
// 0x0000000A System.Boolean UnityEngine.WWW::get_keepWaiting()
extern void WWW_get_keepWaiting_m231A6A7A835610182D78FC414665CC75195ABD70 (void);
// 0x0000000B System.Void UnityEngine.WWW::Dispose()
extern void WWW_Dispose_mF5A8B944281564903043545BC1E7F1CAD941519F (void);
// 0x0000000C System.Boolean UnityEngine.WWW::WaitUntilDoneIfPossible()
extern void WWW_WaitUntilDoneIfPossible_m8D6B638F661CBD13B442F392BF42F5C9BDF0E84D (void);
static Il2CppMethodPointer s_methodPointers[12] = 
{
	WWW_LoadFromCacheOrDownload_m2772A637BC4DC15E8EDD35B1612ABAD8E3AE5490,
	WWW_LoadFromCacheOrDownload_mCD2F8D4B41C321DC873CBC0C3CBEFC2260A49E15,
	WWW_LoadFromCacheOrDownload_m70A95398FA8D5927C25DEE4A28331AB3A45FE678,
	WWW__ctor_mE77AD6C372CC76F48A893C5E2F91A81193A9F8C5,
	WWW__ctor_m443FACCAB63C95EB81924E1FF3B5ADE2D7875186,
	WWW_get_assetBundle_mE523FA754E97AAECE72EA84CBC5D1888E445DE9F,
	WWW_get_bytes_m378FCCD8E91FB7FE7FA22E05BA3FE528CD7EAF1A,
	WWW_get_error_mB278F5EC90EF99FEF70D80112940CFB49E79C9BC,
	WWW_get_url_m1D75D492D78A7AA8F607C5D7700497B8FE5E9526,
	WWW_get_keepWaiting_m231A6A7A835610182D78FC414665CC75195ABD70,
	WWW_Dispose_mF5A8B944281564903043545BC1E7F1CAD941519F,
	WWW_WaitUntilDoneIfPossible_m8D6B638F661CBD13B442F392BF42F5C9BDF0E84D,
};
static const int32_t s_InvokerIndices[12] = 
{
	4726,
	4379,
	4374,
	2856,
	629,
	3533,
	3533,
	3533,
	3533,
	3478,
	3595,
	3478,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_UnityWebRequestWWWModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModule_CodeGenModule = 
{
	"UnityEngine.UnityWebRequestWWWModule.dll",
	12,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_UnityWebRequestWWWModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void SimpleFileBrowser.FBCallbackHelper::Awake()
extern void FBCallbackHelper_Awake_mB8B69B074C05354A975DD11C5E747C4B73F4DB2E (void);
// 0x00000002 System.Void SimpleFileBrowser.FBCallbackHelper::Update()
extern void FBCallbackHelper_Update_mB811E18A297FE94BA75C049913A38E9249502794 (void);
// 0x00000003 System.Void SimpleFileBrowser.FBCallbackHelper::CallOnMainThread(System.Action)
extern void FBCallbackHelper_CallOnMainThread_m7263C7F17001BA043E6A5DABF76DCCEAE4E4266A (void);
// 0x00000004 System.Void SimpleFileBrowser.FBCallbackHelper::.ctor()
extern void FBCallbackHelper__ctor_mBD9122BDF99C5606EF88E1DDE21839008E975897 (void);
// 0x00000005 System.Void SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid::.ctor(SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback)
extern void FBDirectoryReceiveCallbackAndroid__ctor_m53D215598FBA53D3333D359293759E8452582F48 (void);
// 0x00000006 System.Void SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid::OnDirectoryPicked(System.String,System.String)
extern void FBDirectoryReceiveCallbackAndroid_OnDirectoryPicked_m412BA955127668554C2AF55D6CC28166559B46F1 (void);
// 0x00000007 System.Void SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid::DirectoryPickedCallback(System.String,System.String)
extern void FBDirectoryReceiveCallbackAndroid_DirectoryPickedCallback_m7039234E4669DC9103BE940172E633E59D435800 (void);
// 0x00000008 System.Void SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB3AB85D2B5C46D597CF0E576067F99885DECAF0A (void);
// 0x00000009 System.Void SimpleFileBrowser.FBDirectoryReceiveCallbackAndroid/<>c__DisplayClass3_0::<OnDirectoryPicked>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3COnDirectoryPickedU3Eb__0_m1603B9C63F5074A0440B02DA109258F2C746A2E1 (void);
// 0x0000000A System.Int32 SimpleFileBrowser.FBPermissionCallbackAndroid::get_Result()
extern void FBPermissionCallbackAndroid_get_Result_m131BEEFCA19EC6152CEC8DB8C88EA1D8A56E3FA8 (void);
// 0x0000000B System.Void SimpleFileBrowser.FBPermissionCallbackAndroid::set_Result(System.Int32)
extern void FBPermissionCallbackAndroid_set_Result_m213D385F82F35521EDEEFCAE6FAF01DEF27F3458 (void);
// 0x0000000C System.Void SimpleFileBrowser.FBPermissionCallbackAndroid::.ctor(System.Object)
extern void FBPermissionCallbackAndroid__ctor_mEE2AA74588F9FEA5D4D8F4DE7F18B053EB90DB0A (void);
// 0x0000000D System.Void SimpleFileBrowser.FBPermissionCallbackAndroid::OnPermissionResult(System.Int32)
extern void FBPermissionCallbackAndroid_OnPermissionResult_m3913733BDF92AB0A676888FF75FEDF5E444FAC4D (void);
// 0x0000000E System.Void SimpleFileBrowser.EventSystemHandler::OnEnable()
extern void EventSystemHandler_OnEnable_m9D10F4CAB895730E81BBA104C07658ED4BADCB85 (void);
// 0x0000000F System.Void SimpleFileBrowser.EventSystemHandler::OnDisable()
extern void EventSystemHandler_OnDisable_mF4B18C44DDA3ECC217B112411284470C5C3DC9D4 (void);
// 0x00000010 System.Void SimpleFileBrowser.EventSystemHandler::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void EventSystemHandler_OnSceneLoaded_mCF2EB1E2FBF505556047B5357818AC5668339288 (void);
// 0x00000011 System.Void SimpleFileBrowser.EventSystemHandler::OnSceneUnloaded(UnityEngine.SceneManagement.Scene)
extern void EventSystemHandler_OnSceneUnloaded_m9947DEFB077E5140E1E0667E38A2BF12C04A015C (void);
// 0x00000012 System.Void SimpleFileBrowser.EventSystemHandler::ActivateEventSystemIfNeeded()
extern void EventSystemHandler_ActivateEventSystemIfNeeded_m8960B11D43A64D845502AB43765B7B3DF832C145 (void);
// 0x00000013 System.Void SimpleFileBrowser.EventSystemHandler::DeactivateEventSystem()
extern void EventSystemHandler_DeactivateEventSystem_m587AB2F9F86139969B5B0A6DF83F3985F6D21A0F (void);
// 0x00000014 System.Void SimpleFileBrowser.EventSystemHandler::.ctor()
extern void EventSystemHandler__ctor_m1E08073F95F3C31EBA6D14474480FCE2BE198634 (void);
// 0x00000015 System.Boolean SimpleFileBrowser.FileBrowser::get_IsOpen()
extern void FileBrowser_get_IsOpen_m76F153181B2F972690A98BB60FA0B3E6E72ED7C6 (void);
// 0x00000016 System.Void SimpleFileBrowser.FileBrowser::set_IsOpen(System.Boolean)
extern void FileBrowser_set_IsOpen_m132B34485185EB72B9F840606547745EACF08B6E (void);
// 0x00000017 System.Boolean SimpleFileBrowser.FileBrowser::get_Success()
extern void FileBrowser_get_Success_mB13435856C4E9A5FC6B87057E8800CD4473D00C7 (void);
// 0x00000018 System.Void SimpleFileBrowser.FileBrowser::set_Success(System.Boolean)
extern void FileBrowser_set_Success_mF82FCE01FA9934B22AB6AAE52484801AD5521320 (void);
// 0x00000019 System.String[] SimpleFileBrowser.FileBrowser::get_Result()
extern void FileBrowser_get_Result_mA521CA35A57DFB25B29C5CAEB5BCF94F3B5D5D44 (void);
// 0x0000001A System.Void SimpleFileBrowser.FileBrowser::set_Result(System.String[])
extern void FileBrowser_set_Result_mAC115C70FD40098215B44FB277BFCC4D49D56CF3 (void);
// 0x0000001B SimpleFileBrowser.UISkin SimpleFileBrowser.FileBrowser::get_Skin()
extern void FileBrowser_get_Skin_m01C1027C832CAE1ACD0CB592E7B8686A41DDF37F (void);
// 0x0000001C System.Void SimpleFileBrowser.FileBrowser::set_Skin(SimpleFileBrowser.UISkin)
extern void FileBrowser_set_Skin_mE2032BCA5CEE026DC4B09FAB9B07B6E9DDD3CDC4 (void);
// 0x0000001D System.Boolean SimpleFileBrowser.FileBrowser::get_AskPermissions()
extern void FileBrowser_get_AskPermissions_m4D1AF6F162CDD1421E99F2FEEA470B0485D06696 (void);
// 0x0000001E System.Void SimpleFileBrowser.FileBrowser::set_AskPermissions(System.Boolean)
extern void FileBrowser_set_AskPermissions_m1BB9D378C8EC0378E97AC35673301973D3628392 (void);
// 0x0000001F System.Boolean SimpleFileBrowser.FileBrowser::get_SingleClickMode()
extern void FileBrowser_get_SingleClickMode_mD046AB6FA4994A68B94BB36DF515636CD9C9D6B2 (void);
// 0x00000020 System.Void SimpleFileBrowser.FileBrowser::set_SingleClickMode(System.Boolean)
extern void FileBrowser_set_SingleClickMode_m1A10873DF50E0ED741452543A4D3C609D8C7AAEB (void);
// 0x00000021 System.Void SimpleFileBrowser.FileBrowser::add_DisplayedEntriesFilter(SimpleFileBrowser.FileBrowser/FileSystemEntryFilter)
extern void FileBrowser_add_DisplayedEntriesFilter_mB4B74173E5E6113286DA9931ED5A0102DB12B8DB (void);
// 0x00000022 System.Void SimpleFileBrowser.FileBrowser::remove_DisplayedEntriesFilter(SimpleFileBrowser.FileBrowser/FileSystemEntryFilter)
extern void FileBrowser_remove_DisplayedEntriesFilter_m079E11328A44793B3FF1A68BFB982469094BD680 (void);
// 0x00000023 System.Boolean SimpleFileBrowser.FileBrowser::get_ShowFileOverwriteDialog()
extern void FileBrowser_get_ShowFileOverwriteDialog_m3DDB324AEFC613F81411864AC59E92FF3BFB6459 (void);
// 0x00000024 System.Void SimpleFileBrowser.FileBrowser::set_ShowFileOverwriteDialog(System.Boolean)
extern void FileBrowser_set_ShowFileOverwriteDialog_m9F784F4E9D09535EB58F4B4FADB2C36E1CDCC053 (void);
// 0x00000025 System.Boolean SimpleFileBrowser.FileBrowser::get_CheckWriteAccessToDestinationDirectory()
extern void FileBrowser_get_CheckWriteAccessToDestinationDirectory_m734B3AE9BBE8B0F809891ECE57142B5AA09DEAE8 (void);
// 0x00000026 System.Void SimpleFileBrowser.FileBrowser::set_CheckWriteAccessToDestinationDirectory(System.Boolean)
extern void FileBrowser_set_CheckWriteAccessToDestinationDirectory_m090B0F241B58DF128E8FD890EC1E2CC693C84A2A (void);
// 0x00000027 System.Single SimpleFileBrowser.FileBrowser::get_DrivesRefreshInterval()
extern void FileBrowser_get_DrivesRefreshInterval_mF9A1BA2C1ED51D1AD62D59B702819D8024386768 (void);
// 0x00000028 System.Void SimpleFileBrowser.FileBrowser::set_DrivesRefreshInterval(System.Single)
extern void FileBrowser_set_DrivesRefreshInterval_m76ED7834037AEBD521A9E468F91150F2F25F13EE (void);
// 0x00000029 System.Boolean SimpleFileBrowser.FileBrowser::get_ShowHiddenFiles()
extern void FileBrowser_get_ShowHiddenFiles_m23EEB1ED7622AB57021425618A8FBA9AD7FA1C05 (void);
// 0x0000002A System.Void SimpleFileBrowser.FileBrowser::set_ShowHiddenFiles(System.Boolean)
extern void FileBrowser_set_ShowHiddenFiles_m8608D1A058DC2A12E4B15FBEC6237AE86264DA19 (void);
// 0x0000002B System.Boolean SimpleFileBrowser.FileBrowser::get_DisplayHiddenFilesToggle()
extern void FileBrowser_get_DisplayHiddenFilesToggle_mB72DDBC142BC9F1916F11947E7D1787AC94267A3 (void);
// 0x0000002C System.Void SimpleFileBrowser.FileBrowser::set_DisplayHiddenFilesToggle(System.Boolean)
extern void FileBrowser_set_DisplayHiddenFilesToggle_mACD3467C6D979B49E141C0DDA701D2FEEFD0EA73 (void);
// 0x0000002D System.String SimpleFileBrowser.FileBrowser::get_AllFilesFilterText()
extern void FileBrowser_get_AllFilesFilterText_m1E35E8D8DDFA2183FFA83386533E9D880EA381B9 (void);
// 0x0000002E System.Void SimpleFileBrowser.FileBrowser::set_AllFilesFilterText(System.String)
extern void FileBrowser_set_AllFilesFilterText_m0DF904F97562955FC28115BB3FBEE173BD0BD744 (void);
// 0x0000002F System.String SimpleFileBrowser.FileBrowser::get_FoldersFilterText()
extern void FileBrowser_get_FoldersFilterText_m1A157B4031B3EC98C4F4CA8F60AEF1093794E14E (void);
// 0x00000030 System.Void SimpleFileBrowser.FileBrowser::set_FoldersFilterText(System.String)
extern void FileBrowser_set_FoldersFilterText_m7E4CB3272BB12AC16A47A3295A9A58A736D49E64 (void);
// 0x00000031 System.String SimpleFileBrowser.FileBrowser::get_PickFolderQuickLinkText()
extern void FileBrowser_get_PickFolderQuickLinkText_mAEFCEA9DE74D1B1A5C045A17730BB91D562D111C (void);
// 0x00000032 System.Void SimpleFileBrowser.FileBrowser::set_PickFolderQuickLinkText(System.String)
extern void FileBrowser_set_PickFolderQuickLinkText_m1939B72D8FB9EBD2ED57FAB352EA4E4AD24474D8 (void);
// 0x00000033 SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowser::get_Instance()
extern void FileBrowser_get_Instance_m2D1CA7BB2DEE29A481F02D5DDC6B15727AE6457F (void);
// 0x00000034 System.Boolean SimpleFileBrowser.FileBrowser::get_AllExtensionsHaveSingleSuffix()
extern void FileBrowser_get_AllExtensionsHaveSingleSuffix_m5B98B13935078D47B077A2E5473E44CAF9476278 (void);
// 0x00000035 System.String SimpleFileBrowser.FileBrowser::get_CurrentPath()
extern void FileBrowser_get_CurrentPath_m7AEBF65EEE1AAE67FC79949C9565CA382D5C1991 (void);
// 0x00000036 System.Void SimpleFileBrowser.FileBrowser::set_CurrentPath(System.String)
extern void FileBrowser_set_CurrentPath_m9AE094D4E74E17CC9CF900ED71D6316713E0253E (void);
// 0x00000037 System.String SimpleFileBrowser.FileBrowser::get_SearchString()
extern void FileBrowser_get_SearchString_m60BAB1281D2F408F902C290FEE3656CAC11ED288 (void);
// 0x00000038 System.Void SimpleFileBrowser.FileBrowser::set_SearchString(System.String)
extern void FileBrowser_set_SearchString_m20BFE4C41DA445EFA2C00947BE6D18EE3A4EE337 (void);
// 0x00000039 System.Boolean SimpleFileBrowser.FileBrowser::get_AcceptNonExistingFilename()
extern void FileBrowser_get_AcceptNonExistingFilename_mCC73584A920A441DE4DAF2D85CA7FC40A9815DBD (void);
// 0x0000003A System.Void SimpleFileBrowser.FileBrowser::set_AcceptNonExistingFilename(System.Boolean)
extern void FileBrowser_set_AcceptNonExistingFilename_mA5362B26AB27A67FD0BCFEC0B18839A1A8179F74 (void);
// 0x0000003B SimpleFileBrowser.FileBrowser/PickMode SimpleFileBrowser.FileBrowser::get_PickerMode()
extern void FileBrowser_get_PickerMode_mA44EDEDB67BDD91DC42FA68CDF8A554D6498E9FB (void);
// 0x0000003C System.Void SimpleFileBrowser.FileBrowser::set_PickerMode(SimpleFileBrowser.FileBrowser/PickMode)
extern void FileBrowser_set_PickerMode_m3D0E21378984533DDC35DE55B7B1FCB7634863F8 (void);
// 0x0000003D System.Boolean SimpleFileBrowser.FileBrowser::get_AllowMultiSelection()
extern void FileBrowser_get_AllowMultiSelection_m862C120260F12908D02FCE55A1FB9B87F558BEBB (void);
// 0x0000003E System.Void SimpleFileBrowser.FileBrowser::set_AllowMultiSelection(System.Boolean)
extern void FileBrowser_set_AllowMultiSelection_m6FDF012EE485D450F0B68E5A1D9827DF2E52B986 (void);
// 0x0000003F System.Boolean SimpleFileBrowser.FileBrowser::get_MultiSelectionToggleSelectionMode()
extern void FileBrowser_get_MultiSelectionToggleSelectionMode_m975C5D8A411A2755DF57798D39D2188FED2053F8 (void);
// 0x00000040 System.Void SimpleFileBrowser.FileBrowser::set_MultiSelectionToggleSelectionMode(System.Boolean)
extern void FileBrowser_set_MultiSelectionToggleSelectionMode_m176FE9DC46BDD2EB48C108D21831D1B08EB85934 (void);
// 0x00000041 System.String SimpleFileBrowser.FileBrowser::get_Title()
extern void FileBrowser_get_Title_m27BB4C891CE9E16CB8575D73DE6D4994A14261B4 (void);
// 0x00000042 System.Void SimpleFileBrowser.FileBrowser::set_Title(System.String)
extern void FileBrowser_set_Title_m8F51E153087C97D275799A74D1B9248938BA72C7 (void);
// 0x00000043 System.String SimpleFileBrowser.FileBrowser::get_SubmitButtonText()
extern void FileBrowser_get_SubmitButtonText_m20489A29D7521B14D2EF387064647C74D6BEE7FD (void);
// 0x00000044 System.Void SimpleFileBrowser.FileBrowser::set_SubmitButtonText(System.String)
extern void FileBrowser_set_SubmitButtonText_m0B06037B82E56030B481A207250EB04BF448B1DF (void);
// 0x00000045 System.String SimpleFileBrowser.FileBrowser::get_LastBrowsedFolder()
extern void FileBrowser_get_LastBrowsedFolder_m89508024E6A7AF675E464F160A6DC158FB43AA40 (void);
// 0x00000046 System.Void SimpleFileBrowser.FileBrowser::set_LastBrowsedFolder(System.String)
extern void FileBrowser_set_LastBrowsedFolder_mB7CAA2B1E74906B21752B05ADF01FE0E7B6FF204 (void);
// 0x00000047 System.Void SimpleFileBrowser.FileBrowser::Awake()
extern void FileBrowser_Awake_m7ED8D2EAF2FC13A2995A236FED771977A01E29DE (void);
// 0x00000048 System.Void SimpleFileBrowser.FileBrowser::OnRectTransformDimensionsChange()
extern void FileBrowser_OnRectTransformDimensionsChange_m7656558630069B8EAFE99EAC07037AADA4F4A24B (void);
// 0x00000049 System.Void SimpleFileBrowser.FileBrowser::Update()
extern void FileBrowser_Update_mB3481CC859B7DF12AB0B40D02E9DB302D13E959F (void);
// 0x0000004A System.Void SimpleFileBrowser.FileBrowser::LateUpdate()
extern void FileBrowser_LateUpdate_mB72B0AF4CEE098E1E1617CBC742556A0443A676A (void);
// 0x0000004B System.Void SimpleFileBrowser.FileBrowser::OnApplicationFocus(System.Boolean)
extern void FileBrowser_OnApplicationFocus_m3A55A6E53DF3CB09D533CC71B396CB2C8192007A (void);
// 0x0000004C SimpleFileBrowser.OnItemClickedHandler SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.get_OnItemClicked()
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_get_OnItemClicked_m3571B66C7BCCCF5A952BC3B77ECA2EC8F7C31AE8 (void);
// 0x0000004D System.Void SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.set_OnItemClicked(SimpleFileBrowser.OnItemClickedHandler)
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_set_OnItemClicked_mAD8C5576E8F5DBEE4E9934E1E961646E18D13AD9 (void);
// 0x0000004E System.Int32 SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.get_Count()
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_get_Count_m53920F2D439AB6729AACAAE8B10196C0E780DFEF (void);
// 0x0000004F System.Single SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.get_ItemHeight()
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_get_ItemHeight_m87809A489D1604A41B6264EB197215F5CF1164FB (void);
// 0x00000050 SimpleFileBrowser.ListItem SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.CreateItem()
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_CreateItem_m4B3594285FC355021488EDE0424C6A63CEF75909 (void);
// 0x00000051 System.Void SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.SetItemContent(SimpleFileBrowser.ListItem)
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_SetItemContent_mFABF6CE61B6A8A87B1A810287172E11B56BBD44B (void);
// 0x00000052 System.Void SimpleFileBrowser.FileBrowser::InitializeQuickLinks()
extern void FileBrowser_InitializeQuickLinks_m3E58E20D0485E8685D61E4A5C21A2E81C3FCB1AA (void);
// 0x00000053 System.Void SimpleFileBrowser.FileBrowser::RefreshDriveQuickLinks()
extern void FileBrowser_RefreshDriveQuickLinks_mFA2736938A9141A7AE8D4CCE963BF8599116025F (void);
// 0x00000054 System.Void SimpleFileBrowser.FileBrowser::RefreshSkin()
extern void FileBrowser_RefreshSkin_m655D943EDA19691F340A1EDB9DC504BEF2E31507 (void);
// 0x00000055 System.Void SimpleFileBrowser.FileBrowser::OnBackButtonPressed()
extern void FileBrowser_OnBackButtonPressed_mE07193E07A9F83C1975D4B56C407B27656458CD6 (void);
// 0x00000056 System.Void SimpleFileBrowser.FileBrowser::OnForwardButtonPressed()
extern void FileBrowser_OnForwardButtonPressed_m2386A29CF07AFCB08198E40F6E3C5164F285EEA0 (void);
// 0x00000057 System.Void SimpleFileBrowser.FileBrowser::OnUpButtonPressed()
extern void FileBrowser_OnUpButtonPressed_m0D3B5941FF82CF88975BB820574CC3DBAF6F37B3 (void);
// 0x00000058 System.Void SimpleFileBrowser.FileBrowser::OnMoreOptionsButtonClicked()
extern void FileBrowser_OnMoreOptionsButtonClicked_mE6946C5641CAD3C5ADAACB66B59D184D586BCFFA (void);
// 0x00000059 System.Void SimpleFileBrowser.FileBrowser::OnContextMenuTriggered(UnityEngine.Vector2)
extern void FileBrowser_OnContextMenuTriggered_m24D91C79FC0A88DFAD5548607BD9B8C145C1E761 (void);
// 0x0000005A System.Void SimpleFileBrowser.FileBrowser::ShowContextMenuAt(UnityEngine.Vector2,System.Boolean)
extern void FileBrowser_ShowContextMenuAt_m40500C533570BA07A1126123B3B097C63C374249 (void);
// 0x0000005B System.Void SimpleFileBrowser.FileBrowser::OnSubmitButtonClicked()
extern void FileBrowser_OnSubmitButtonClicked_m7E848A843F3D025488C0EE6FF5E54EA6D57DCBE3 (void);
// 0x0000005C System.Void SimpleFileBrowser.FileBrowser::OnCancelButtonClicked()
extern void FileBrowser_OnCancelButtonClicked_mB5936764FEC583FA4F56C10619D3CF8BA60E96D8 (void);
// 0x0000005D System.Void SimpleFileBrowser.FileBrowser::OnOperationSuccessful(System.String[])
extern void FileBrowser_OnOperationSuccessful_m5DC93791ECD6CBA227C3FA4436DB0EBE91A27A6C (void);
// 0x0000005E System.Void SimpleFileBrowser.FileBrowser::OnOperationCanceled(System.Boolean)
extern void FileBrowser_OnOperationCanceled_m8D25FC9E848E384F3E878FF801A7993C36DF1947 (void);
// 0x0000005F System.Void SimpleFileBrowser.FileBrowser::OnPathChanged(System.String)
extern void FileBrowser_OnPathChanged_m690981D06142B84FA26FF5904150DB0CD3758D72 (void);
// 0x00000060 System.Void SimpleFileBrowser.FileBrowser::OnSearchStringChanged(System.String)
extern void FileBrowser_OnSearchStringChanged_m455D848F1D77E8509209BEEA041E6F0AF1D0796F (void);
// 0x00000061 System.Void SimpleFileBrowser.FileBrowser::OnFilterChanged()
extern void FileBrowser_OnFilterChanged_m54C7B8ABB2A479ECA7994A22D3FA484E279D8B9B (void);
// 0x00000062 System.Void SimpleFileBrowser.FileBrowser::OnShowHiddenFilesToggleChanged()
extern void FileBrowser_OnShowHiddenFilesToggleChanged_mA0BC97B0151F590135CAD8588EACC5AF6399A794 (void);
// 0x00000063 System.Void SimpleFileBrowser.FileBrowser::OnItemSelected(SimpleFileBrowser.FileBrowserItem,System.Boolean)
extern void FileBrowser_OnItemSelected_m169A9AE1DED1C198827C499C3DE7DE1C25D884F9 (void);
// 0x00000064 System.Void SimpleFileBrowser.FileBrowser::OnItemHeld(SimpleFileBrowser.FileBrowserItem)
extern void FileBrowser_OnItemHeld_m12287097633B233E71C322067B79BED7956395DB (void);
// 0x00000065 System.Void SimpleFileBrowser.FileBrowser::OnSAFDirectoryPicked(System.String,System.String)
extern void FileBrowser_OnSAFDirectoryPicked_m869B694845C301A1835784A117AEA7D1E86749C6 (void);
// 0x00000066 System.Void SimpleFileBrowser.FileBrowser::FetchPersistedSAFQuickLinks()
extern void FileBrowser_FetchPersistedSAFQuickLinks_mEC9A78E59C56F58C5644CF9404A53A8ABA75FF4F (void);
// 0x00000067 System.Char SimpleFileBrowser.FileBrowser::OnValidateFilenameInput(System.String,System.Int32,System.Char)
extern void FileBrowser_OnValidateFilenameInput_mF3FEA1E1138C07A818B30E19B5A988BF742C7922 (void);
// 0x00000068 System.Void SimpleFileBrowser.FileBrowser::OnFilenameInputChanged(System.String)
extern void FileBrowser_OnFilenameInputChanged_m8597E8B6648695042D028063BA767891987F90F4 (void);
// 0x00000069 System.Void SimpleFileBrowser.FileBrowser::Show(System.String,System.String)
extern void FileBrowser_Show_mFFC02C0EB9E1F56896F3AA2EE65AA8CB396FEE78 (void);
// 0x0000006A System.Void SimpleFileBrowser.FileBrowser::Hide()
extern void FileBrowser_Hide_mF5BFFE4E660A4C0BD26E4360676AE6C401B84335 (void);
// 0x0000006B System.Void SimpleFileBrowser.FileBrowser::RefreshFiles(System.Boolean)
extern void FileBrowser_RefreshFiles_m78E8F197E37E572167B58ECF89907B0F937711D3 (void);
// 0x0000006C System.Boolean SimpleFileBrowser.FileBrowser::FileSystemEntryMatchesFilters(SimpleFileBrowser.FileSystemEntry,System.Boolean)
extern void FileBrowser_FileSystemEntryMatchesFilters_mF30551D4CB3981102F8434FE12A90CA412DF64EE (void);
// 0x0000006D System.Void SimpleFileBrowser.FileBrowser::SelectAllFiles()
extern void FileBrowser_SelectAllFiles_mE91B7524DDC69BE8825340880B581B63772A070B (void);
// 0x0000006E System.Void SimpleFileBrowser.FileBrowser::DeselectAllFiles()
extern void FileBrowser_DeselectAllFiles_m74394CF1CECC64623176BEDC0E0A85807DE5B526 (void);
// 0x0000006F System.Void SimpleFileBrowser.FileBrowser::CreateNewFolder()
extern void FileBrowser_CreateNewFolder_m93131A2F5C3BBD4AE007C8E0D6AB310F5405E626 (void);
// 0x00000070 System.Collections.IEnumerator SimpleFileBrowser.FileBrowser::CreateNewFolderCoroutine()
extern void FileBrowser_CreateNewFolderCoroutine_mCC4E6964B0EEDC56E8C937CEBBFEAEA262C8581E (void);
// 0x00000071 System.Void SimpleFileBrowser.FileBrowser::RenameSelectedFile()
extern void FileBrowser_RenameSelectedFile_m0BDC939786A45A504F9C74459EC23882ACD3A25B (void);
// 0x00000072 System.Void SimpleFileBrowser.FileBrowser::DeleteSelectedFiles()
extern void FileBrowser_DeleteSelectedFiles_mB3E6B271A957C4519AA8A9AFDBABBB7863520A17 (void);
// 0x00000073 System.Void SimpleFileBrowser.FileBrowser::PersistFileEntrySelection()
extern void FileBrowser_PersistFileEntrySelection_m8A55E481241BD19BA81EC14E6B88F9B1557A9EDA (void);
// 0x00000074 System.Boolean SimpleFileBrowser.FileBrowser::AddQuickLink(UnityEngine.Sprite,System.String,System.String)
extern void FileBrowser_AddQuickLink_mB4F5AF2799B7717685A57C2DDF444E319DE55B81 (void);
// 0x00000075 System.Void SimpleFileBrowser.FileBrowser::ClearQuickLinksInternal()
extern void FileBrowser_ClearQuickLinksInternal_m942EF9858522B6740C416E790EB022984CC59E26 (void);
// 0x00000076 System.Void SimpleFileBrowser.FileBrowser::EnsureScrollViewIsWithinBounds()
extern void FileBrowser_EnsureScrollViewIsWithinBounds_m928C6CC80134DD6859C52BC6A7DB04C4EDB7B845 (void);
// 0x00000077 System.Void SimpleFileBrowser.FileBrowser::EnsureWindowIsWithinBounds()
extern void FileBrowser_EnsureWindowIsWithinBounds_m0C2BE0125CCF95DC08987F4EEC1334949F33560D (void);
// 0x00000078 System.Void SimpleFileBrowser.FileBrowser::OnWindowDimensionsChanged(UnityEngine.Vector2)
extern void FileBrowser_OnWindowDimensionsChanged_mDFABDE888D6BF3701A840871D2A202DAF6279AB3 (void);
// 0x00000079 UnityEngine.Sprite SimpleFileBrowser.FileBrowser::GetIconForFileEntry(SimpleFileBrowser.FileSystemEntry)
extern void FileBrowser_GetIconForFileEntry_m27CDFFF69366A6DD7809BBDA3D49DED0B92DFEC0 (void);
// 0x0000007A System.String SimpleFileBrowser.FileBrowser::GetExtensionFromFilename(System.String,System.Boolean)
extern void FileBrowser_GetExtensionFromFilename_m2495AB77331771B0EB8CBD100BF6D81F779A36E5 (void);
// 0x0000007B System.String SimpleFileBrowser.FileBrowser::GetPathWithoutTrailingDirectorySeparator(System.String)
extern void FileBrowser_GetPathWithoutTrailingDirectorySeparator_m2C5C9B65DF3EDBB6DF9B415815F037C88832C5BE (void);
// 0x0000007C System.Void SimpleFileBrowser.FileBrowser::UpdateFilenameInputFieldWithSelection()
extern void FileBrowser_UpdateFilenameInputFieldWithSelection_mE42C35DBAC32BD4EA372AB25A9F1E8C01BB0A5FF (void);
// 0x0000007D System.Int32 SimpleFileBrowser.FileBrowser::ExtractFilenameFromInput(System.String,System.Int32&,System.Int32&)
extern void FileBrowser_ExtractFilenameFromInput_m839AB82ACA752B225AF295B030B0242D8E0BBF0B (void);
// 0x0000007E System.Int32 SimpleFileBrowser.FileBrowser::FilenameToFileEntryIndex(System.String)
extern void FileBrowser_FilenameToFileEntryIndex_m314FDAD9ED8489A31A6F192083FCE420C1EF2127 (void);
// 0x0000007F System.Boolean SimpleFileBrowser.FileBrowser::VerifyFilename(System.String)
extern void FileBrowser_VerifyFilename_m246B3104BA5AB79EF45296154BAE8F8BEF301EF9 (void);
// 0x00000080 System.Int32 SimpleFileBrowser.FileBrowser::CalculateLengthOfDropdownText(System.String)
extern void FileBrowser_CalculateLengthOfDropdownText_m03EEF8FBFE5FCDE52EC3C58157123E6E4F19D026 (void);
// 0x00000081 System.String SimpleFileBrowser.FileBrowser::GetInitialPath(System.String)
extern void FileBrowser_GetInitialPath_m423E2A4E4AA334B2E2181314CB37C2B614DA256F (void);
// 0x00000082 System.Boolean SimpleFileBrowser.FileBrowser::CheckDirectoryWriteAccess(System.String)
extern void FileBrowser_CheckDirectoryWriteAccess_mDAFD493D3F5BE1DB70FC2F0B215CDE23FAA7BADA (void);
// 0x00000083 System.Boolean SimpleFileBrowser.FileBrowser::IsCtrlKeyHeld()
extern void FileBrowser_IsCtrlKeyHeld_m2A22066F676381F1D26BFD6BD8B9E554C72F68D2 (void);
// 0x00000084 System.Boolean SimpleFileBrowser.FileBrowser::ShowSaveDialog(SimpleFileBrowser.FileBrowser/OnSuccess,SimpleFileBrowser.FileBrowser/OnCancel,SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_ShowSaveDialog_mC27C4233D80B769B4DD41FF34476296B811225C9 (void);
// 0x00000085 System.Boolean SimpleFileBrowser.FileBrowser::ShowLoadDialog(SimpleFileBrowser.FileBrowser/OnSuccess,SimpleFileBrowser.FileBrowser/OnCancel,SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_ShowLoadDialog_m73918E026EA31133F15BB56A903D4C1191FED21F (void);
// 0x00000086 System.Boolean SimpleFileBrowser.FileBrowser::ShowDialogInternal(SimpleFileBrowser.FileBrowser/OnSuccess,SimpleFileBrowser.FileBrowser/OnCancel,SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_ShowDialogInternal_mA32167D762691C56DD9F733B1D0E24C4D7F1CA96 (void);
// 0x00000087 System.Void SimpleFileBrowser.FileBrowser::HideDialog(System.Boolean)
extern void FileBrowser_HideDialog_mDA627E443C7CA60608031520B27488F52B9ABB90 (void);
// 0x00000088 System.Collections.IEnumerator SimpleFileBrowser.FileBrowser::WaitForSaveDialog(SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_WaitForSaveDialog_m5454CF697CF1BE9FBD027F7020E98E095BEAA7FD (void);
// 0x00000089 System.Collections.IEnumerator SimpleFileBrowser.FileBrowser::WaitForLoadDialog(SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_WaitForLoadDialog_mE110ACB9DAAEC9C617822A5A2001382F345B58C9 (void);
// 0x0000008A System.Boolean SimpleFileBrowser.FileBrowser::AddQuickLink(System.String,System.String,UnityEngine.Sprite)
extern void FileBrowser_AddQuickLink_m9F3258A18B6735F8086E39AA94B53447CFF71747 (void);
// 0x0000008B System.Void SimpleFileBrowser.FileBrowser::ClearQuickLinks()
extern void FileBrowser_ClearQuickLinks_m044403DC97DCA1DD98473C7A76FFCED06107817E (void);
// 0x0000008C System.Void SimpleFileBrowser.FileBrowser::SetExcludedExtensions(System.String[])
extern void FileBrowser_SetExcludedExtensions_mA9BC5DC0482E7055BF2DD270B3874BB0A31FD709 (void);
// 0x0000008D System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean)
extern void FileBrowser_SetFilters_mA1C1F9BAA756E48107E1BEDD4C1ED1E5271EECBE (void);
// 0x0000008E System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean,System.Collections.Generic.IEnumerable`1<System.String>)
extern void FileBrowser_SetFilters_m0B63664B2101EC6189E35358DDD16481D5644214 (void);
// 0x0000008F System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean,System.String[])
extern void FileBrowser_SetFilters_mAFC93DB0463A70423C79B058BD36CE01AFE9D84D (void);
// 0x00000090 System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean,System.Collections.Generic.IEnumerable`1<SimpleFileBrowser.FileBrowser/Filter>)
extern void FileBrowser_SetFilters_m970A512FD08E081D6BABADED1B81C3C6D151ADCC (void);
// 0x00000091 System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean,SimpleFileBrowser.FileBrowser/Filter[])
extern void FileBrowser_SetFilters_mB60CC9235A36041CF175CE8BC913E3B961D58B77 (void);
// 0x00000092 System.Void SimpleFileBrowser.FileBrowser::SetFiltersPreProcessing(System.Boolean)
extern void FileBrowser_SetFiltersPreProcessing_mB9B0E5A320AC057252D4A7B93D255AC760D76CC0 (void);
// 0x00000093 System.Void SimpleFileBrowser.FileBrowser::SetFiltersPostProcessing()
extern void FileBrowser_SetFiltersPostProcessing_mDF6CA0D8DE1B928D09DF8421499295881BBB2E85 (void);
// 0x00000094 System.Boolean SimpleFileBrowser.FileBrowser::SetDefaultFilter(System.String)
extern void FileBrowser_SetDefaultFilter_mCBC7A444E73202D821187EC3266D941001250D22 (void);
// 0x00000095 SimpleFileBrowser.FileBrowser/Permission SimpleFileBrowser.FileBrowser::CheckPermission()
extern void FileBrowser_CheckPermission_m5761448828E9FB82E4FBA4C236636855B9375B61 (void);
// 0x00000096 SimpleFileBrowser.FileBrowser/Permission SimpleFileBrowser.FileBrowser::RequestPermission()
extern void FileBrowser_RequestPermission_m51C864DFE488B7306A4ED77E41F322960CDBB4F5 (void);
// 0x00000097 System.Void SimpleFileBrowser.FileBrowser::.ctor()
extern void FileBrowser__ctor_mE67BF18A7571B5FE7D82A7454AAB098D9A1AACD2 (void);
// 0x00000098 System.Void SimpleFileBrowser.FileBrowser::.cctor()
extern void FileBrowser__cctor_m069A4B4A72D44AD588EDD19D064B46F52E7CD2F1 (void);
// 0x00000099 System.Void SimpleFileBrowser.FileBrowser::<CreateNewFolderCoroutine>b__241_0(System.String)
extern void FileBrowser_U3CCreateNewFolderCoroutineU3Eb__241_0_mE4B278D5E88BC29E2F4F88DDA2F471F57ED0376D (void);
// 0x0000009A System.Void SimpleFileBrowser.FileBrowser::<DeleteSelectedFiles>b__243_0()
extern void FileBrowser_U3CDeleteSelectedFilesU3Eb__243_0_mC8B8270D31AF46160629E570D96CFC769CA83308 (void);
// 0x0000009B System.Void SimpleFileBrowser.FileBrowser/Filter::.ctor(System.String)
extern void Filter__ctor_m1074FC06CD79E457F25F52026D634FEBF9ACCF10 (void);
// 0x0000009C System.Void SimpleFileBrowser.FileBrowser/Filter::.ctor(System.String,System.String)
extern void Filter__ctor_m38D854E9B7172B814710C305CA0857452A2DE121 (void);
// 0x0000009D System.Void SimpleFileBrowser.FileBrowser/Filter::.ctor(System.String,System.String[])
extern void Filter__ctor_m125246E9D958C713FF3D86B5E02C9D6540B58185 (void);
// 0x0000009E System.Boolean SimpleFileBrowser.FileBrowser/Filter::MatchesExtension(System.String,System.Boolean)
extern void Filter_MatchesExtension_mB6BA2D275B2A7B7A2B7863D23591832CE7D0B7DF (void);
// 0x0000009F System.String SimpleFileBrowser.FileBrowser/Filter::ToString()
extern void Filter_ToString_m9DCF19E38E806A743CA49A330EBEA08814B62753 (void);
// 0x000000A0 System.Void SimpleFileBrowser.FileBrowser/OnSuccess::.ctor(System.Object,System.IntPtr)
extern void OnSuccess__ctor_m71AD74DD97BD2E317E6CCD78F0B631AC4D3104D7 (void);
// 0x000000A1 System.Void SimpleFileBrowser.FileBrowser/OnSuccess::Invoke(System.String[])
extern void OnSuccess_Invoke_m7AC28204C5A1715F07274D5F73B66E39CD7D1317 (void);
// 0x000000A2 System.IAsyncResult SimpleFileBrowser.FileBrowser/OnSuccess::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
extern void OnSuccess_BeginInvoke_m99BB5894F15FCDD0F04BD6321CB1E7F19EF28FFE (void);
// 0x000000A3 System.Void SimpleFileBrowser.FileBrowser/OnSuccess::EndInvoke(System.IAsyncResult)
extern void OnSuccess_EndInvoke_mB36C87C496674F8F56D681D6E25384CAB0F18CBC (void);
// 0x000000A4 System.Void SimpleFileBrowser.FileBrowser/OnCancel::.ctor(System.Object,System.IntPtr)
extern void OnCancel__ctor_m1B12099B6EC65CE377D0A854F92BC24145CCF42C (void);
// 0x000000A5 System.Void SimpleFileBrowser.FileBrowser/OnCancel::Invoke()
extern void OnCancel_Invoke_m91F1A69EF5776DCA7D9A5C5EC9A03C925CDEBD49 (void);
// 0x000000A6 System.IAsyncResult SimpleFileBrowser.FileBrowser/OnCancel::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnCancel_BeginInvoke_m21D3022399AFFCF3CFC9926B0597AE814B2E380C (void);
// 0x000000A7 System.Void SimpleFileBrowser.FileBrowser/OnCancel::EndInvoke(System.IAsyncResult)
extern void OnCancel_EndInvoke_m11BD247D9B4669B70D8D3116C309BB8EE08023F4 (void);
// 0x000000A8 System.Void SimpleFileBrowser.FileBrowser/FileSystemEntryFilter::.ctor(System.Object,System.IntPtr)
extern void FileSystemEntryFilter__ctor_mF7DA3A52405E454E79B105125606D6E8BAFF8F74 (void);
// 0x000000A9 System.Boolean SimpleFileBrowser.FileBrowser/FileSystemEntryFilter::Invoke(SimpleFileBrowser.FileSystemEntry)
extern void FileSystemEntryFilter_Invoke_mF9EE2CB6AD810BF7873EF479A6F57CE9A81969DF (void);
// 0x000000AA System.IAsyncResult SimpleFileBrowser.FileBrowser/FileSystemEntryFilter::BeginInvoke(SimpleFileBrowser.FileSystemEntry,System.AsyncCallback,System.Object)
extern void FileSystemEntryFilter_BeginInvoke_mA07C1032C03172D6F020207D97E77631FA1D2055 (void);
// 0x000000AB System.Boolean SimpleFileBrowser.FileBrowser/FileSystemEntryFilter::EndInvoke(System.IAsyncResult)
extern void FileSystemEntryFilter_EndInvoke_m18A618059F7D663A424111D2372986129FA0220E (void);
// 0x000000AC System.Void SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback::.ctor(System.Object,System.IntPtr)
extern void AndroidSAFDirectoryPickCallback__ctor_mA74461FFAA226B63E9E44A9C221072ED68F45DE8 (void);
// 0x000000AD System.Void SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback::Invoke(System.String,System.String)
extern void AndroidSAFDirectoryPickCallback_Invoke_m57B8746EC1134F2FD4F3782D7F3468E1F7E33076 (void);
// 0x000000AE System.IAsyncResult SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void AndroidSAFDirectoryPickCallback_BeginInvoke_mD5991AD25935F5FD2E404BF6245BBE018D425749 (void);
// 0x000000AF System.Void SimpleFileBrowser.FileBrowser/AndroidSAFDirectoryPickCallback::EndInvoke(System.IAsyncResult)
extern void AndroidSAFDirectoryPickCallback_EndInvoke_mA9F40E882DDF96E33E27DD315C8526A6B508A69C (void);
// 0x000000B0 System.Void SimpleFileBrowser.FileBrowser/<>c__DisplayClass220_0::.ctor()
extern void U3CU3Ec__DisplayClass220_0__ctor_mF9972CFE3E9D7C704E4FF4EEDB232DDC1E7F0FDF (void);
// 0x000000B1 System.Void SimpleFileBrowser.FileBrowser/<>c__DisplayClass220_0::<OnSubmitButtonClicked>b__0()
extern void U3CU3Ec__DisplayClass220_0_U3COnSubmitButtonClickedU3Eb__0_m8DBA90F0845F09706FF10BEA45D6452ED641384C (void);
// 0x000000B2 System.Void SimpleFileBrowser.FileBrowser/<>c::.cctor()
extern void U3CU3Ec__cctor_m68751808F9A0583FD6BDFD4FB074F482E85FB494 (void);
// 0x000000B3 System.Void SimpleFileBrowser.FileBrowser/<>c::.ctor()
extern void U3CU3Ec__ctor_m0C7FD05A63A799955E2C175B16DD6E2FA642261E (void);
// 0x000000B4 System.Int32 SimpleFileBrowser.FileBrowser/<>c::<RefreshFiles>b__236_0(SimpleFileBrowser.FileSystemEntry,SimpleFileBrowser.FileSystemEntry)
extern void U3CU3Ec_U3CRefreshFilesU3Eb__236_0_m4874DD1849D832573640A6C95CE59ECCE03F2C43 (void);
// 0x000000B5 System.Void SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::.ctor(System.Int32)
extern void U3CCreateNewFolderCoroutineU3Ed__241__ctor_mDFD38C6B4A6455AEDEE398C93FA8C252AE88184B (void);
// 0x000000B6 System.Void SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::System.IDisposable.Dispose()
extern void U3CCreateNewFolderCoroutineU3Ed__241_System_IDisposable_Dispose_mB74B115992FE4B68A55E768310A997AC20A20926 (void);
// 0x000000B7 System.Boolean SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::MoveNext()
extern void U3CCreateNewFolderCoroutineU3Ed__241_MoveNext_mDC6CFEFA6CBF228341CDD23A2D4EBDBCB7B82798 (void);
// 0x000000B8 System.Object SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0545F6447CE56AA4E999A8B6450E7F30A310430D (void);
// 0x000000B9 System.Void SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::System.Collections.IEnumerator.Reset()
extern void U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_IEnumerator_Reset_m5F77E4A7CA436A621AC00B2073FE17DB3EFF2850 (void);
// 0x000000BA System.Object SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__241::System.Collections.IEnumerator.get_Current()
extern void U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_IEnumerator_get_Current_m8E8B43EAB0828F57B8691DBEADCF694AA88101DC (void);
// 0x000000BB System.Void SimpleFileBrowser.FileBrowser/<>c__DisplayClass242_0::.ctor()
extern void U3CU3Ec__DisplayClass242_0__ctor_m7C7C48E12917367748721E92BE27F9DF7B0228B2 (void);
// 0x000000BC System.Void SimpleFileBrowser.FileBrowser/<>c__DisplayClass242_0::<RenameSelectedFile>b__0(System.String)
extern void U3CU3Ec__DisplayClass242_0_U3CRenameSelectedFileU3Eb__0_m073741ADCE7FDC0ACCB58C46732E2AAB60CB890F (void);
// 0x000000BD System.Void SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::.ctor(System.Int32)
extern void U3CWaitForSaveDialogU3Ed__265__ctor_m12F637ABD0A5EBB97C466D0F553D5B8D56033BD1 (void);
// 0x000000BE System.Void SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::System.IDisposable.Dispose()
extern void U3CWaitForSaveDialogU3Ed__265_System_IDisposable_Dispose_m7BBF2D25C1C1AC74A18CF70C32FF1FDEC15F9459 (void);
// 0x000000BF System.Boolean SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::MoveNext()
extern void U3CWaitForSaveDialogU3Ed__265_MoveNext_mFABB79CE55C954CD101AAC64895FED46AA318176 (void);
// 0x000000C0 System.Object SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSaveDialogU3Ed__265_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9186D456077EC88F0B778027AD680C149E3D74CE (void);
// 0x000000C1 System.Void SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSaveDialogU3Ed__265_System_Collections_IEnumerator_Reset_m38718E5D5CE7AA13F1F4B684775CED69286E1390 (void);
// 0x000000C2 System.Object SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__265::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSaveDialogU3Ed__265_System_Collections_IEnumerator_get_Current_mE3601AB26ED57A39433E48BB8716C87BD6F45B0F (void);
// 0x000000C3 System.Void SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::.ctor(System.Int32)
extern void U3CWaitForLoadDialogU3Ed__266__ctor_m3F8B1CF0E9CA9644D9E02111A683BD10962D1F5D (void);
// 0x000000C4 System.Void SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::System.IDisposable.Dispose()
extern void U3CWaitForLoadDialogU3Ed__266_System_IDisposable_Dispose_mEF304AF3D0BAE96043ECA7DAD60C537FBB38A35C (void);
// 0x000000C5 System.Boolean SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::MoveNext()
extern void U3CWaitForLoadDialogU3Ed__266_MoveNext_m1603ACA4294B686207F02C7542E1D0A6DB24E557 (void);
// 0x000000C6 System.Object SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForLoadDialogU3Ed__266_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE96087E920FB48FA18B5996C934D369C33B9B75 (void);
// 0x000000C7 System.Void SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::System.Collections.IEnumerator.Reset()
extern void U3CWaitForLoadDialogU3Ed__266_System_Collections_IEnumerator_Reset_m783AE76EC116144B63DC8BE626E95732D3C0BF27 (void);
// 0x000000C8 System.Object SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__266::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForLoadDialogU3Ed__266_System_Collections_IEnumerator_get_Current_mCF552CCACF7B0B0ED71A0EE750D029528C487043 (void);
// 0x000000C9 System.Void SimpleFileBrowser.FileBrowserAccessRestrictedPanel::Show()
extern void FileBrowserAccessRestrictedPanel_Show_mBCD473301AB85C2BA6C6D2F7972507FDFCDE2D22 (void);
// 0x000000CA System.Void SimpleFileBrowser.FileBrowserAccessRestrictedPanel::RefreshSkin(SimpleFileBrowser.UISkin)
extern void FileBrowserAccessRestrictedPanel_RefreshSkin_mF36763DA5E7DD72F3EBE1CDDD74358BAD0F4C91C (void);
// 0x000000CB System.Void SimpleFileBrowser.FileBrowserAccessRestrictedPanel::OKButtonClicked()
extern void FileBrowserAccessRestrictedPanel_OKButtonClicked_mFD84702894080C9B7FBE5611BA9B70DEE53C1088 (void);
// 0x000000CC System.Void SimpleFileBrowser.FileBrowserAccessRestrictedPanel::.ctor()
extern void FileBrowserAccessRestrictedPanel__ctor_m6F4950CA0E640922744C3EBAA57A2BC73D3F1390 (void);
// 0x000000CD System.Void SimpleFileBrowser.FileBrowserContextMenu::Show(System.Boolean,System.Boolean,System.Boolean,System.Boolean,UnityEngine.Vector2,System.Boolean)
extern void FileBrowserContextMenu_Show_m048F7147CB835F992FF2017662D8557D74937ED1 (void);
// 0x000000CE System.Void SimpleFileBrowser.FileBrowserContextMenu::Hide()
extern void FileBrowserContextMenu_Hide_m74048A668955B61CF3C72AAA635387BD82631F64 (void);
// 0x000000CF System.Void SimpleFileBrowser.FileBrowserContextMenu::RefreshSkin(SimpleFileBrowser.UISkin)
extern void FileBrowserContextMenu_RefreshSkin_mA679EA42D2EA903D444A1672076301D958DAAA52 (void);
// 0x000000D0 System.Void SimpleFileBrowser.FileBrowserContextMenu::OnSelectAllButtonClicked()
extern void FileBrowserContextMenu_OnSelectAllButtonClicked_mA30D36E00D69D1EF1F7083A1D3F9C4E6E6BDA4B0 (void);
// 0x000000D1 System.Void SimpleFileBrowser.FileBrowserContextMenu::OnDeselectAllButtonClicked()
extern void FileBrowserContextMenu_OnDeselectAllButtonClicked_mB45F767EE9CD38BA3F1235D0D4B3CDC74D5562F9 (void);
// 0x000000D2 System.Void SimpleFileBrowser.FileBrowserContextMenu::OnCreateFolderButtonClicked()
extern void FileBrowserContextMenu_OnCreateFolderButtonClicked_mD6CBE83296EB54560F9E3F933AFD521312D07376 (void);
// 0x000000D3 System.Void SimpleFileBrowser.FileBrowserContextMenu::OnDeleteButtonClicked()
extern void FileBrowserContextMenu_OnDeleteButtonClicked_m99ACE4623BD9BFAFC79B94AA484D60CBEE2D91AB (void);
// 0x000000D4 System.Void SimpleFileBrowser.FileBrowserContextMenu::OnRenameButtonClicked()
extern void FileBrowserContextMenu_OnRenameButtonClicked_m37DEA61748E86D2F41D6013F1106CE72AC6D5D00 (void);
// 0x000000D5 System.Void SimpleFileBrowser.FileBrowserContextMenu::.ctor()
extern void FileBrowserContextMenu__ctor_m0CE266A05467D4C59C3BBD3E3D9A65AF477D995F (void);
// 0x000000D6 System.Void SimpleFileBrowser.FileBrowserCursorHandler::.ctor()
extern void FileBrowserCursorHandler__ctor_m700BE779271111CF7F82596D9B059379BF6D9B79 (void);
// 0x000000D7 System.Void SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel::Show(SimpleFileBrowser.FileBrowser,System.Collections.Generic.List`1<SimpleFileBrowser.FileSystemEntry>,SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OperationType,SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed)
extern void FileBrowserFileOperationConfirmationPanel_Show_m15EBC0082F416DA80066E67B9C8784FBD19BA49C (void);
// 0x000000D8 System.Void SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel::Show(SimpleFileBrowser.FileBrowser,System.Collections.Generic.List`1<SimpleFileBrowser.FileSystemEntry>,System.Collections.Generic.List`1<System.Int32>,SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OperationType,SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed)
extern void FileBrowserFileOperationConfirmationPanel_Show_mE5E9BDBA381D3CC63CAA0096120133385FFF557E (void);
// 0x000000D9 System.Void SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel::OnCanvasDimensionsChanged(UnityEngine.Vector2)
extern void FileBrowserFileOperationConfirmationPanel_OnCanvasDimensionsChanged_m38A3C39F813C2A4AC73194A075A3427FF5E896D8 (void);
// 0x000000DA System.Void SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel::RefreshSkin(SimpleFileBrowser.UISkin)
extern void FileBrowserFileOperationConfirmationPanel_RefreshSkin_mC41A8FA560E50E101C5A31A14983D0184AA1F136 (void);
// 0x000000DB System.Void SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel::YesButtonClicked()
extern void FileBrowserFileOperationConfirmationPanel_YesButtonClicked_m6B61A55D8D71E9D1EF9BEBCF17AAFC43AD84FB74 (void);
// 0x000000DC System.Void SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel::NoButtonClicked()
extern void FileBrowserFileOperationConfirmationPanel_NoButtonClicked_m59E628E01A36D1C8D34939CC5D63F726B001D1E2 (void);
// 0x000000DD System.Void SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel::.ctor()
extern void FileBrowserFileOperationConfirmationPanel__ctor_mD9E5D3B05C389BD60E92BC04DD79505248AFFCA7 (void);
// 0x000000DE System.Void SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed::.ctor(System.Object,System.IntPtr)
extern void OnOperationConfirmed__ctor_m6D50A4AEC640684E8F1017EC7E4A709A27DA37CF (void);
// 0x000000DF System.Void SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed::Invoke()
extern void OnOperationConfirmed_Invoke_mBBE7933011170BC9A4DDBB037931A3E10469FBBE (void);
// 0x000000E0 System.IAsyncResult SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnOperationConfirmed_BeginInvoke_m3489C6887612AA70AF313950FCE0D4FC339647E7 (void);
// 0x000000E1 System.Void SimpleFileBrowser.FileBrowserFileOperationConfirmationPanel/OnOperationConfirmed::EndInvoke(System.IAsyncResult)
extern void OnOperationConfirmed_EndInvoke_m1E48D6AA660F27A97CFE5791EB604AC864A1082B (void);
// 0x000000E2 System.Boolean SimpleFileBrowser.FileSystemEntry::get_IsDirectory()
extern void FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F (void);
// 0x000000E3 System.Void SimpleFileBrowser.FileSystemEntry::.ctor(System.String,System.String,System.String,System.Boolean)
extern void FileSystemEntry__ctor_m08BA758AC7BDAC9D19FA4C874BFE7FF6EC349ADA (void);
// 0x000000E4 System.Void SimpleFileBrowser.FileSystemEntry::.ctor(System.IO.FileSystemInfo,System.String)
extern void FileSystemEntry__ctor_mF213C56704D663A552E1FD301772FA84A177828D (void);
// 0x000000E5 UnityEngine.AndroidJavaClass SimpleFileBrowser.FileBrowserHelpers::get_AJC()
extern void FileBrowserHelpers_get_AJC_m7A13844F2E1638F5A37A24E3EEC7122F997A27E5 (void);
// 0x000000E6 UnityEngine.AndroidJavaObject SimpleFileBrowser.FileBrowserHelpers::get_Context()
extern void FileBrowserHelpers_get_Context_m0D55D47A0B3CCD833D775D124A15C55626062052 (void);
// 0x000000E7 System.String SimpleFileBrowser.FileBrowserHelpers::get_TemporaryFilePath()
extern void FileBrowserHelpers_get_TemporaryFilePath_mABA3208150554B9028C2D76ABD44EA8B01776182 (void);
// 0x000000E8 System.Boolean SimpleFileBrowser.FileBrowserHelpers::get_ShouldUseSAF()
extern void FileBrowserHelpers_get_ShouldUseSAF_m294D64E951F424A1922EB38E45D702B65424CFA8 (void);
// 0x000000E9 System.Boolean SimpleFileBrowser.FileBrowserHelpers::ShouldUseSAFForPath(System.String)
extern void FileBrowserHelpers_ShouldUseSAFForPath_m4A2F79FE5240CDCF6452CC9483FC6412E502A41F (void);
// 0x000000EA System.Boolean SimpleFileBrowser.FileBrowserHelpers::FileExists(System.String)
extern void FileBrowserHelpers_FileExists_mD3579F307508B344F0785F92CDF0F5668656D6DD (void);
// 0x000000EB System.Boolean SimpleFileBrowser.FileBrowserHelpers::DirectoryExists(System.String)
extern void FileBrowserHelpers_DirectoryExists_m6F82D57E2F9C7AEA033D02D86C58797D6CF638BC (void);
// 0x000000EC System.Boolean SimpleFileBrowser.FileBrowserHelpers::IsDirectory(System.String)
extern void FileBrowserHelpers_IsDirectory_m58B5A826DB00DA28AA6132B4E59F1B327980212E (void);
// 0x000000ED System.Boolean SimpleFileBrowser.FileBrowserHelpers::IsPathDescendantOfAnother(System.String,System.String)
extern void FileBrowserHelpers_IsPathDescendantOfAnother_m45D10B3F750E1D6F5660F79484142A0DD3703E00 (void);
// 0x000000EE System.String SimpleFileBrowser.FileBrowserHelpers::GetDirectoryName(System.String)
extern void FileBrowserHelpers_GetDirectoryName_m32F9849AA17666FC7491B9DEA3222CC7340B3A85 (void);
// 0x000000EF SimpleFileBrowser.FileSystemEntry[] SimpleFileBrowser.FileBrowserHelpers::GetEntriesInDirectory(System.String,System.Boolean)
extern void FileBrowserHelpers_GetEntriesInDirectory_mF3215D5020D4E60D0CB0FA3DBABDFE1716EE5880 (void);
// 0x000000F0 System.String SimpleFileBrowser.FileBrowserHelpers::CreateFileInDirectory(System.String,System.String)
extern void FileBrowserHelpers_CreateFileInDirectory_m109E3355A5F2A7787577650E5F165CF816B54100 (void);
// 0x000000F1 System.String SimpleFileBrowser.FileBrowserHelpers::CreateFolderInDirectory(System.String,System.String)
extern void FileBrowserHelpers_CreateFolderInDirectory_mA81EF67AA61CA351367CFA7B99067C3840B82CCB (void);
// 0x000000F2 System.Void SimpleFileBrowser.FileBrowserHelpers::WriteBytesToFile(System.String,System.Byte[])
extern void FileBrowserHelpers_WriteBytesToFile_m90771EDE57CC6ACB6F54BBA156DD88E4DAF15D86 (void);
// 0x000000F3 System.Void SimpleFileBrowser.FileBrowserHelpers::WriteTextToFile(System.String,System.String)
extern void FileBrowserHelpers_WriteTextToFile_m63B109F445EE1CD31AD94593C83D51866FBDB83D (void);
// 0x000000F4 System.Void SimpleFileBrowser.FileBrowserHelpers::AppendBytesToFile(System.String,System.Byte[])
extern void FileBrowserHelpers_AppendBytesToFile_mCC55D795345B1399032C519A9CF8222C8DBFD0B7 (void);
// 0x000000F5 System.Void SimpleFileBrowser.FileBrowserHelpers::AppendTextToFile(System.String,System.String)
extern void FileBrowserHelpers_AppendTextToFile_m7F7FD975C7E5E738F53F68686D66DAB025F7AB96 (void);
// 0x000000F6 System.Void SimpleFileBrowser.FileBrowserHelpers::AppendFileToFile(System.String,System.String)
extern void FileBrowserHelpers_AppendFileToFile_m8BD1E3ACA58E0ACEB57836AC994BEBB7A84A243F (void);
// 0x000000F7 System.Byte[] SimpleFileBrowser.FileBrowserHelpers::ReadBytesFromFile(System.String)
extern void FileBrowserHelpers_ReadBytesFromFile_m59B3BBC5F09059FF548A69B3F14D05E4C0F479D3 (void);
// 0x000000F8 System.String SimpleFileBrowser.FileBrowserHelpers::ReadTextFromFile(System.String)
extern void FileBrowserHelpers_ReadTextFromFile_mEB28F5CB4D7682F1E04A908E6BD60967126F338D (void);
// 0x000000F9 System.Void SimpleFileBrowser.FileBrowserHelpers::CopyFile(System.String,System.String)
extern void FileBrowserHelpers_CopyFile_m07B3C712FBCDCB7FF8C32248EB8119BB823A1FC0 (void);
// 0x000000FA System.Void SimpleFileBrowser.FileBrowserHelpers::CopyDirectory(System.String,System.String)
extern void FileBrowserHelpers_CopyDirectory_mBDD288CF2FD8CA11206243BE2D971AB14BD2732C (void);
// 0x000000FB System.Void SimpleFileBrowser.FileBrowserHelpers::CopyDirectoryRecursively(System.IO.DirectoryInfo,System.String)
extern void FileBrowserHelpers_CopyDirectoryRecursively_m00DFF80853B380FF671EC7EC51BFE2C414F0509B (void);
// 0x000000FC System.Void SimpleFileBrowser.FileBrowserHelpers::MoveFile(System.String,System.String)
extern void FileBrowserHelpers_MoveFile_m67DF8A9A06BE2FB651ADE67B2C7D010395B8BB55 (void);
// 0x000000FD System.Void SimpleFileBrowser.FileBrowserHelpers::MoveDirectory(System.String,System.String)
extern void FileBrowserHelpers_MoveDirectory_m5E407890B8916573A09E7335F38EBC75B26C728A (void);
// 0x000000FE System.String SimpleFileBrowser.FileBrowserHelpers::RenameFile(System.String,System.String)
extern void FileBrowserHelpers_RenameFile_mA204A30D5E284916B22AA30419A0412094C3AFB7 (void);
// 0x000000FF System.String SimpleFileBrowser.FileBrowserHelpers::RenameDirectory(System.String,System.String)
extern void FileBrowserHelpers_RenameDirectory_m070EEAEDFAD3768F3900854F66EE12D126C92D4D (void);
// 0x00000100 System.Void SimpleFileBrowser.FileBrowserHelpers::DeleteFile(System.String)
extern void FileBrowserHelpers_DeleteFile_mFED2EA416401BED8C9826D66C61808EA9E4202CC (void);
// 0x00000101 System.Void SimpleFileBrowser.FileBrowserHelpers::DeleteDirectory(System.String)
extern void FileBrowserHelpers_DeleteDirectory_mA531BA313D6566C3430BE47A38C72CCEF1B66606 (void);
// 0x00000102 System.String SimpleFileBrowser.FileBrowserHelpers::GetFilename(System.String)
extern void FileBrowserHelpers_GetFilename_mBE32212132286ECDDF7E722032A9E6CAFEEEF044 (void);
// 0x00000103 System.Int64 SimpleFileBrowser.FileBrowserHelpers::GetFilesize(System.String)
extern void FileBrowserHelpers_GetFilesize_m983FA57D636D262551E458A8689F717F7A36EAAD (void);
// 0x00000104 System.DateTime SimpleFileBrowser.FileBrowserHelpers::GetLastModifiedDate(System.String)
extern void FileBrowserHelpers_GetLastModifiedDate_m91A46C094E1B3475E56ACC38E86A5B1D85FE8755 (void);
// 0x00000105 System.Void SimpleFileBrowser.FileBrowserHelpers::.cctor()
extern void FileBrowserHelpers__cctor_m8E7397ABED927DA005F6A603AC24B15C3DBB4660 (void);
// 0x00000106 UnityEngine.UI.Image SimpleFileBrowser.FileBrowserItem::get_Icon()
extern void FileBrowserItem_get_Icon_m883D60BCF0DF40A8F3435064999B0C9FA2718175 (void);
// 0x00000107 UnityEngine.RectTransform SimpleFileBrowser.FileBrowserItem::get_TransformComponent()
extern void FileBrowserItem_get_TransformComponent_mEDD8CD474DBBF4B58E11D1BE82C255CAE65141CA (void);
// 0x00000108 System.String SimpleFileBrowser.FileBrowserItem::get_Name()
extern void FileBrowserItem_get_Name_m1F09E0A5E84870485231447173A5AF496DC0E01F (void);
// 0x00000109 System.Boolean SimpleFileBrowser.FileBrowserItem::get_IsDirectory()
extern void FileBrowserItem_get_IsDirectory_mC40A8B51825BC34EB34D45C72D35A1CA6E1A89BB (void);
// 0x0000010A System.Void SimpleFileBrowser.FileBrowserItem::set_IsDirectory(System.Boolean)
extern void FileBrowserItem_set_IsDirectory_mDB0D5D005A9C6F689AF30D6A63E8AE555FFF6DF6 (void);
// 0x0000010B System.Void SimpleFileBrowser.FileBrowserItem::SetFileBrowser(SimpleFileBrowser.FileBrowser,SimpleFileBrowser.UISkin)
extern void FileBrowserItem_SetFileBrowser_m9C9ADC77158FD31D95E816DEAA1439DB305E8AA9 (void);
// 0x0000010C System.Void SimpleFileBrowser.FileBrowserItem::SetFile(UnityEngine.Sprite,System.String,System.Boolean)
extern void FileBrowserItem_SetFile_m1F1468944380FAE443BB546ADF30AC56B3C6616D (void);
// 0x0000010D System.Void SimpleFileBrowser.FileBrowserItem::Update()
extern void FileBrowserItem_Update_m3A16C87E26FCD94FB63A74DBD6F10ED09E6D7FC4 (void);
// 0x0000010E System.Void SimpleFileBrowser.FileBrowserItem::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserItem_OnPointerClick_m324DF382072BE76D6FFDBE62E8C2DFABD47A6065 (void);
// 0x0000010F System.Void SimpleFileBrowser.FileBrowserItem::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserItem_OnPointerDown_m4EA10AF080820CE9779BDE00E61CFF69C9BE4BF8 (void);
// 0x00000110 System.Void SimpleFileBrowser.FileBrowserItem::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserItem_OnPointerUp_m7CEDD0C86CCBB748225E07597536AB8FB9D8436A (void);
// 0x00000111 System.Void SimpleFileBrowser.FileBrowserItem::SetSelected(System.Boolean)
extern void FileBrowserItem_SetSelected_mF1A4735B8F234281F0698E5C0DA0B9575C0D4C28 (void);
// 0x00000112 System.Void SimpleFileBrowser.FileBrowserItem::SetHidden(System.Boolean)
extern void FileBrowserItem_SetHidden_m91D66EE00C0EEAF1FFEB6C9220BF363310271F28 (void);
// 0x00000113 System.Void SimpleFileBrowser.FileBrowserItem::OnSkinRefreshed(SimpleFileBrowser.UISkin,System.Boolean)
extern void FileBrowserItem_OnSkinRefreshed_mF1600D4A8CBD447F719FDFE284DFA88E68022626 (void);
// 0x00000114 System.Void SimpleFileBrowser.FileBrowserItem::.ctor()
extern void FileBrowserItem__ctor_m78CFC23F9073522F0BBFB7F9792BAA0A46362454 (void);
// 0x00000115 System.Void SimpleFileBrowser.FileBrowserMovement::Initialize(SimpleFileBrowser.FileBrowser)
extern void FileBrowserMovement_Initialize_m471ABE4C29F9D705461783AB33F2CC921AC51478 (void);
// 0x00000116 System.Void SimpleFileBrowser.FileBrowserMovement::OnDragStarted(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnDragStarted_m3E0201898C42B961B9D8194C95DDB8EEA8555B65 (void);
// 0x00000117 System.Void SimpleFileBrowser.FileBrowserMovement::OnDrag(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnDrag_mD8D2918E9F95EFFDCC104E321E5B957850743AF8 (void);
// 0x00000118 System.Void SimpleFileBrowser.FileBrowserMovement::OnEndDrag(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnEndDrag_m443E3696DBB3571E87FC1B616BDE956C5F3A506E (void);
// 0x00000119 System.Void SimpleFileBrowser.FileBrowserMovement::OnResizeStarted(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnResizeStarted_mB31A17958B7A24A16CBE1B21D9B22451A2C0D486 (void);
// 0x0000011A System.Void SimpleFileBrowser.FileBrowserMovement::OnResize(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnResize_m49E6F628633974DE0F8E1737D127FB03BE774853 (void);
// 0x0000011B System.Void SimpleFileBrowser.FileBrowserMovement::OnEndResize(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnEndResize_mFB53FC3931D90175FE576DA10B16F94CD75BEC27 (void);
// 0x0000011C System.Void SimpleFileBrowser.FileBrowserMovement::.ctor()
extern void FileBrowserMovement__ctor_mB4D1B389EACB5128B7FC728B439AFE488642E95D (void);
// 0x0000011D System.String SimpleFileBrowser.FileBrowserQuickLink::get_TargetPath()
extern void FileBrowserQuickLink_get_TargetPath_mD1CA8CA83A2B19226D00B8E77BC024A57D0C4C91 (void);
// 0x0000011E System.Void SimpleFileBrowser.FileBrowserQuickLink::SetQuickLink(UnityEngine.Sprite,System.String,System.String)
extern void FileBrowserQuickLink_SetQuickLink_mDEDE06D8E3C9A90193FB71E57336A68E92895350 (void);
// 0x0000011F System.Void SimpleFileBrowser.FileBrowserQuickLink::.ctor()
extern void FileBrowserQuickLink__ctor_mCED0CE4E2EDD195AAB3F2BBA553F9EFC9715D8A5 (void);
// 0x00000120 UnityEngine.UI.InputField SimpleFileBrowser.FileBrowserRenamedItem::get_InputField()
extern void FileBrowserRenamedItem_get_InputField_m83330E63A271A5280D8E444601AB15D1C58786D8 (void);
// 0x00000121 UnityEngine.RectTransform SimpleFileBrowser.FileBrowserRenamedItem::get_TransformComponent()
extern void FileBrowserRenamedItem_get_TransformComponent_m45A05A656AB0AAB92E504A39469F821D2CCC7DE4 (void);
// 0x00000122 System.Void SimpleFileBrowser.FileBrowserRenamedItem::Show(System.String,UnityEngine.Color,UnityEngine.Sprite,SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted)
extern void FileBrowserRenamedItem_Show_m67CF833D01BEDC56743DF4EAC46AF60E8FFFB5DB (void);
// 0x00000123 System.Void SimpleFileBrowser.FileBrowserRenamedItem::OnInputFieldEndEdit(System.String)
extern void FileBrowserRenamedItem_OnInputFieldEndEdit_mB5AC8C49EEF73640F34CB94F2D5FA62F1C48C366 (void);
// 0x00000124 System.Void SimpleFileBrowser.FileBrowserRenamedItem::.ctor()
extern void FileBrowserRenamedItem__ctor_m32B6017C97863E0349A0D4B61D355CD9EFECE11B (void);
// 0x00000125 System.Void SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::.ctor(System.Object,System.IntPtr)
extern void OnRenameCompleted__ctor_m019658E790544491DDBD4F208F8B422DC77764EB (void);
// 0x00000126 System.Void SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::Invoke(System.String)
extern void OnRenameCompleted_Invoke_m8A1B85475DE7D5362CB711D63543CDD6A68A7CDB (void);
// 0x00000127 System.IAsyncResult SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void OnRenameCompleted_BeginInvoke_m090E447625A7D2DCE9AE24887390FB68BAD85E31 (void);
// 0x00000128 System.Void SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::EndInvoke(System.IAsyncResult)
extern void OnRenameCompleted_EndInvoke_mEC5511E093D8247D914A8802352DC8DA57CFB1B2 (void);
// 0x00000129 System.Void SimpleFileBrowser.NonDrawingGraphic::SetMaterialDirty()
extern void NonDrawingGraphic_SetMaterialDirty_m329A03BC12E9779085DC0D91E2DE2582127E8395 (void);
// 0x0000012A System.Void SimpleFileBrowser.NonDrawingGraphic::SetVerticesDirty()
extern void NonDrawingGraphic_SetVerticesDirty_mD732D935D4951495843EC5A9EA1DB19665415793 (void);
// 0x0000012B System.Void SimpleFileBrowser.NonDrawingGraphic::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void NonDrawingGraphic_OnPopulateMesh_m1C0A9BA5BE3AE3AAC6CBFE8F742E4AB51823198C (void);
// 0x0000012C System.Void SimpleFileBrowser.NonDrawingGraphic::.ctor()
extern void NonDrawingGraphic__ctor_m995D20F1C7EA51EC17D9E9D5F8F10A226597D6BB (void);
// 0x0000012D System.Void SimpleFileBrowser.OnItemClickedHandler::.ctor(System.Object,System.IntPtr)
extern void OnItemClickedHandler__ctor_m2E672645F74ACB3CB0B7F152205B647BCB399FAC (void);
// 0x0000012E System.Void SimpleFileBrowser.OnItemClickedHandler::Invoke(SimpleFileBrowser.ListItem)
extern void OnItemClickedHandler_Invoke_mE3A8562310E72EC8ADF01FA3693D1B02A010B9D3 (void);
// 0x0000012F System.IAsyncResult SimpleFileBrowser.OnItemClickedHandler::BeginInvoke(SimpleFileBrowser.ListItem,System.AsyncCallback,System.Object)
extern void OnItemClickedHandler_BeginInvoke_mAC5A0CEC312EBB2FEB0BBACA665E2FB8B43226E3 (void);
// 0x00000130 System.Void SimpleFileBrowser.OnItemClickedHandler::EndInvoke(System.IAsyncResult)
extern void OnItemClickedHandler_EndInvoke_m89D2C7968FA251487A47AFC4D2FFAFB65C892E3C (void);
// 0x00000131 SimpleFileBrowser.OnItemClickedHandler SimpleFileBrowser.IListViewAdapter::get_OnItemClicked()
// 0x00000132 System.Void SimpleFileBrowser.IListViewAdapter::set_OnItemClicked(SimpleFileBrowser.OnItemClickedHandler)
// 0x00000133 System.Int32 SimpleFileBrowser.IListViewAdapter::get_Count()
// 0x00000134 System.Single SimpleFileBrowser.IListViewAdapter::get_ItemHeight()
// 0x00000135 SimpleFileBrowser.ListItem SimpleFileBrowser.IListViewAdapter::CreateItem()
// 0x00000136 System.Void SimpleFileBrowser.IListViewAdapter::SetItemContent(SimpleFileBrowser.ListItem)
// 0x00000137 System.Object SimpleFileBrowser.ListItem::get_Tag()
extern void ListItem_get_Tag_mA71AE4254B5B31A3B822DB8F1674D50AB25A3041 (void);
// 0x00000138 System.Void SimpleFileBrowser.ListItem::set_Tag(System.Object)
extern void ListItem_set_Tag_m74393E301D898520AB53B5AA336ACFBC97EC787E (void);
// 0x00000139 System.Int32 SimpleFileBrowser.ListItem::get_Position()
extern void ListItem_get_Position_mB4639770E22F328A1496CDCA7FCB936D65E1FFAE (void);
// 0x0000013A System.Void SimpleFileBrowser.ListItem::set_Position(System.Int32)
extern void ListItem_set_Position_mD18CBDB9DDB4D5C2E68E5CFA0B2C7D2B94540DFE (void);
// 0x0000013B System.Void SimpleFileBrowser.ListItem::SetAdapter(SimpleFileBrowser.IListViewAdapter)
extern void ListItem_SetAdapter_m5A6BE935C41666EC29E6AFA47207C30F2AB2FD0D (void);
// 0x0000013C System.Void SimpleFileBrowser.ListItem::OnClick()
extern void ListItem_OnClick_m950C0BA1BCA2C8B4EE9DE7A8044E822169B83669 (void);
// 0x0000013D System.Void SimpleFileBrowser.ListItem::.ctor()
extern void ListItem__ctor_m108C9F53A120596095F9DDD0A2ABFD0EF0B2084C (void);
// 0x0000013E System.Void SimpleFileBrowser.RecycledListView::Start()
extern void RecycledListView_Start_mBE55ACC0C6D315D3337AD031F6F9CC15421AA23A (void);
// 0x0000013F System.Void SimpleFileBrowser.RecycledListView::SetAdapter(SimpleFileBrowser.IListViewAdapter)
extern void RecycledListView_SetAdapter_m06A0E4AF885C03B6B9F6A3E1933F83A3E90541B7 (void);
// 0x00000140 System.Void SimpleFileBrowser.RecycledListView::OnSkinRefreshed()
extern void RecycledListView_OnSkinRefreshed_mEDFC858497AC069C64149A523C6A41CA77C4CC84 (void);
// 0x00000141 System.Void SimpleFileBrowser.RecycledListView::UpdateList()
extern void RecycledListView_UpdateList_m0637428860547148FA94CA0267BBC6E06911F29F (void);
// 0x00000142 System.Void SimpleFileBrowser.RecycledListView::OnViewportDimensionsChanged()
extern void RecycledListView_OnViewportDimensionsChanged_m62B43716804A2EF7DB7F8E45508B2A780D06A4C2 (void);
// 0x00000143 System.Void SimpleFileBrowser.RecycledListView::UpdateItemsInTheList(System.Boolean)
extern void RecycledListView_UpdateItemsInTheList_mFFE94878442D42C7B7F49A08025E4511FDFCCBC0 (void);
// 0x00000144 System.Void SimpleFileBrowser.RecycledListView::CreateItemsBetweenIndices(System.Int32,System.Int32)
extern void RecycledListView_CreateItemsBetweenIndices_m1EBA714FEB4E87540C618C5C01A2176140CF34B0 (void);
// 0x00000145 System.Void SimpleFileBrowser.RecycledListView::CreateItemAtIndex(System.Int32)
extern void RecycledListView_CreateItemAtIndex_m848A2F8857745DC016A54F1CC80B138A364664BB (void);
// 0x00000146 System.Void SimpleFileBrowser.RecycledListView::DestroyItemsBetweenIndices(System.Int32,System.Int32)
extern void RecycledListView_DestroyItemsBetweenIndices_m9DD81FA19B89E35FCC06583C85EB8299D7FD8137 (void);
// 0x00000147 System.Void SimpleFileBrowser.RecycledListView::UpdateItemContentsBetweenIndices(System.Int32,System.Int32)
extern void RecycledListView_UpdateItemContentsBetweenIndices_m7C71B732BD2850BAC39B8CBE1AEE1A592451A0F4 (void);
// 0x00000148 System.Void SimpleFileBrowser.RecycledListView::.ctor()
extern void RecycledListView__ctor_m360F5EF4D831B9AEAC4D7E023CABA6F2931F1F7E (void);
// 0x00000149 System.Void SimpleFileBrowser.RecycledListView::<Start>b__10_0(UnityEngine.Vector2)
extern void RecycledListView_U3CStartU3Eb__10_0_m24E1DAB94C847738A00D58ED91844281DAFC0696 (void);
// 0x0000014A System.Int32 SimpleFileBrowser.UISkin::get_Version()
extern void UISkin_get_Version_mDEB52FC78056118447FD6E860344A046AA3AF3BC (void);
// 0x0000014B System.Void SimpleFileBrowser.UISkin::Invalidate()
extern void UISkin_Invalidate_mF0FE40247D7E67A8FBC84DEC8EA36BE9E11ED089 (void);
// 0x0000014C UnityEngine.Font SimpleFileBrowser.UISkin::get_Font()
extern void UISkin_get_Font_m1552BEAA0A3BBBFCE0BD560333FB5FA4107E939D (void);
// 0x0000014D System.Void SimpleFileBrowser.UISkin::set_Font(UnityEngine.Font)
extern void UISkin_set_Font_m8141DB785C4C87E748E34BD1CE7C2518937359FB (void);
// 0x0000014E System.Int32 SimpleFileBrowser.UISkin::get_FontSize()
extern void UISkin_get_FontSize_mFEAD4AD2A1616FF6472909374486EEEF86BA9DBA (void);
// 0x0000014F System.Void SimpleFileBrowser.UISkin::set_FontSize(System.Int32)
extern void UISkin_set_FontSize_mA65E49B2321F1A0EDCC455B8B1DECA2EE9859286 (void);
// 0x00000150 UnityEngine.Color SimpleFileBrowser.UISkin::get_WindowColor()
extern void UISkin_get_WindowColor_m367BFEF2404BC9C6D4332A86B385DB53D8D4C6E0 (void);
// 0x00000151 System.Void SimpleFileBrowser.UISkin::set_WindowColor(UnityEngine.Color)
extern void UISkin_set_WindowColor_m1739473F28A992891FE6A3B54FBE8EEE0F5AF873 (void);
// 0x00000152 UnityEngine.Color SimpleFileBrowser.UISkin::get_FilesListColor()
extern void UISkin_get_FilesListColor_mAEC97EF9E2EBF67C06BCEB99577FF8F644FFA46C (void);
// 0x00000153 System.Void SimpleFileBrowser.UISkin::set_FilesListColor(UnityEngine.Color)
extern void UISkin_set_FilesListColor_mAD96AD5C1018EDC0663BDECD91B7D62F3E4DA8D8 (void);
// 0x00000154 UnityEngine.Color SimpleFileBrowser.UISkin::get_FilesVerticalSeparatorColor()
extern void UISkin_get_FilesVerticalSeparatorColor_m54206BF03DED84B48BA6B33D618466340CA4986A (void);
// 0x00000155 System.Void SimpleFileBrowser.UISkin::set_FilesVerticalSeparatorColor(UnityEngine.Color)
extern void UISkin_set_FilesVerticalSeparatorColor_m31EA2F064048AFBC2C6986DA4483BABA2EFA045B (void);
// 0x00000156 UnityEngine.Color SimpleFileBrowser.UISkin::get_TitleBackgroundColor()
extern void UISkin_get_TitleBackgroundColor_m00BCA39F5548EB231D00861F264B84885E13AAAE (void);
// 0x00000157 System.Void SimpleFileBrowser.UISkin::set_TitleBackgroundColor(UnityEngine.Color)
extern void UISkin_set_TitleBackgroundColor_mFFB484E2D39B9B8DF85EA1D1F34424E0863D7093 (void);
// 0x00000158 UnityEngine.Color SimpleFileBrowser.UISkin::get_TitleTextColor()
extern void UISkin_get_TitleTextColor_mD7D42B4EA169A95EB13B2321814DC1C11CCE2060 (void);
// 0x00000159 System.Void SimpleFileBrowser.UISkin::set_TitleTextColor(UnityEngine.Color)
extern void UISkin_set_TitleTextColor_m9904013AABE86133AF0596C9445B56FECBE4FC32 (void);
// 0x0000015A UnityEngine.Color SimpleFileBrowser.UISkin::get_WindowResizeGizmoColor()
extern void UISkin_get_WindowResizeGizmoColor_mAC5C2F897A3EA08375CEBB3E59FC3CE9927AEFB0 (void);
// 0x0000015B System.Void SimpleFileBrowser.UISkin::set_WindowResizeGizmoColor(UnityEngine.Color)
extern void UISkin_set_WindowResizeGizmoColor_m73A3957EE7B045D9A5FE16AE48855F834F1A4474 (void);
// 0x0000015C UnityEngine.Color SimpleFileBrowser.UISkin::get_HeaderButtonsColor()
extern void UISkin_get_HeaderButtonsColor_m31EB94F085067FF0528DBFE0B6F00BFC763792C5 (void);
// 0x0000015D System.Void SimpleFileBrowser.UISkin::set_HeaderButtonsColor(UnityEngine.Color)
extern void UISkin_set_HeaderButtonsColor_m597E212FE241FCA9197391FE02B694278482881B (void);
// 0x0000015E UnityEngine.Sprite SimpleFileBrowser.UISkin::get_WindowResizeGizmo()
extern void UISkin_get_WindowResizeGizmo_m53E6F918D39261E32F7E235BB43C3CE93E87899B (void);
// 0x0000015F System.Void SimpleFileBrowser.UISkin::set_WindowResizeGizmo(UnityEngine.Sprite)
extern void UISkin_set_WindowResizeGizmo_mC373651E93ED96B4BBFE3BD8472FDFFBE93754D0 (void);
// 0x00000160 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_HeaderBackButton()
extern void UISkin_get_HeaderBackButton_mFE7BF99995C4E03A69537BD03C920732CACA0C98 (void);
// 0x00000161 System.Void SimpleFileBrowser.UISkin::set_HeaderBackButton(UnityEngine.Sprite)
extern void UISkin_set_HeaderBackButton_m01FACB6087977D06B815F3F3FF098F30D1889715 (void);
// 0x00000162 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_HeaderForwardButton()
extern void UISkin_get_HeaderForwardButton_m0AB55675E37203A316E70E7AC6962F5D9404A83E (void);
// 0x00000163 System.Void SimpleFileBrowser.UISkin::set_HeaderForwardButton(UnityEngine.Sprite)
extern void UISkin_set_HeaderForwardButton_m97EB75D74E491353B2AB5F22CC60718007FB5B09 (void);
// 0x00000164 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_HeaderUpButton()
extern void UISkin_get_HeaderUpButton_m2F8A0E79E6EF0FC107B4C17F003D2722679528D2 (void);
// 0x00000165 System.Void SimpleFileBrowser.UISkin::set_HeaderUpButton(UnityEngine.Sprite)
extern void UISkin_set_HeaderUpButton_mAB91307BAF755156C876EA718D3FE6C1AA290A56 (void);
// 0x00000166 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_HeaderContextMenuButton()
extern void UISkin_get_HeaderContextMenuButton_m99137E276EF90F2FABA6982F91848A4F3607D4CF (void);
// 0x00000167 System.Void SimpleFileBrowser.UISkin::set_HeaderContextMenuButton(UnityEngine.Sprite)
extern void UISkin_set_HeaderContextMenuButton_mD1B4B578CDEE9AF5880EE0AE5E0DE8E6776DF1A6 (void);
// 0x00000168 UnityEngine.Color SimpleFileBrowser.UISkin::get_InputFieldNormalBackgroundColor()
extern void UISkin_get_InputFieldNormalBackgroundColor_mF870D1EF69BA439EE2020669A25EEF56B3129B00 (void);
// 0x00000169 System.Void SimpleFileBrowser.UISkin::set_InputFieldNormalBackgroundColor(UnityEngine.Color)
extern void UISkin_set_InputFieldNormalBackgroundColor_mD80211096A36637A8692509F8A9182F4BEA2EB7B (void);
// 0x0000016A UnityEngine.Color SimpleFileBrowser.UISkin::get_InputFieldInvalidBackgroundColor()
extern void UISkin_get_InputFieldInvalidBackgroundColor_m9B27CD7446BF26DC1AE8FFB83B4B8BB522E2FC87 (void);
// 0x0000016B System.Void SimpleFileBrowser.UISkin::set_InputFieldInvalidBackgroundColor(UnityEngine.Color)
extern void UISkin_set_InputFieldInvalidBackgroundColor_m5631498B98FE5F8F544517176D0ED7AD996E27A1 (void);
// 0x0000016C UnityEngine.Color SimpleFileBrowser.UISkin::get_InputFieldTextColor()
extern void UISkin_get_InputFieldTextColor_mE7D55005ECF6791606337569B171E38CC6650521 (void);
// 0x0000016D System.Void SimpleFileBrowser.UISkin::set_InputFieldTextColor(UnityEngine.Color)
extern void UISkin_set_InputFieldTextColor_mF3FC41494D35612E81DE87973623B6A8C82EAED1 (void);
// 0x0000016E UnityEngine.Color SimpleFileBrowser.UISkin::get_InputFieldPlaceholderTextColor()
extern void UISkin_get_InputFieldPlaceholderTextColor_mCBE0A7B179B3AF4E012BD40F983F97F05FADBD14 (void);
// 0x0000016F System.Void SimpleFileBrowser.UISkin::set_InputFieldPlaceholderTextColor(UnityEngine.Color)
extern void UISkin_set_InputFieldPlaceholderTextColor_m409358A8B62DEE0EDA1D7E5B07FFE9A0D0B04EFA (void);
// 0x00000170 UnityEngine.Color SimpleFileBrowser.UISkin::get_InputFieldSelectedTextColor()
extern void UISkin_get_InputFieldSelectedTextColor_mFBB0E015FD5218DE4D510D7AA294C18726280694 (void);
// 0x00000171 System.Void SimpleFileBrowser.UISkin::set_InputFieldSelectedTextColor(UnityEngine.Color)
extern void UISkin_set_InputFieldSelectedTextColor_mC52EFF3E4B6B3785BDDADE6259629EE76297B449 (void);
// 0x00000172 UnityEngine.Color SimpleFileBrowser.UISkin::get_InputFieldCaretColor()
extern void UISkin_get_InputFieldCaretColor_m629083D3778AA9C526D8477BF89015AAA061ABAD (void);
// 0x00000173 System.Void SimpleFileBrowser.UISkin::set_InputFieldCaretColor(UnityEngine.Color)
extern void UISkin_set_InputFieldCaretColor_m6472937FA19EF8F995D8F722847D401E9266ECB7 (void);
// 0x00000174 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_InputFieldBackground()
extern void UISkin_get_InputFieldBackground_m00B5F061A1F1ED9C3848BC9D726D9FEC778296F6 (void);
// 0x00000175 System.Void SimpleFileBrowser.UISkin::set_InputFieldBackground(UnityEngine.Sprite)
extern void UISkin_set_InputFieldBackground_m0ADEC56B2C91599776C29EC0FAC2D1E4839A1035 (void);
// 0x00000176 UnityEngine.Color SimpleFileBrowser.UISkin::get_ButtonColor()
extern void UISkin_get_ButtonColor_m25E3D9790C3240828B57E2CFCB16ADC617203F14 (void);
// 0x00000177 System.Void SimpleFileBrowser.UISkin::set_ButtonColor(UnityEngine.Color)
extern void UISkin_set_ButtonColor_m613746EE1E12F0856A4B717D2524321145E1D1E0 (void);
// 0x00000178 UnityEngine.Color SimpleFileBrowser.UISkin::get_ButtonTextColor()
extern void UISkin_get_ButtonTextColor_mC0C9600B8D7A85DF94F003F19E288CCD97E6975D (void);
// 0x00000179 System.Void SimpleFileBrowser.UISkin::set_ButtonTextColor(UnityEngine.Color)
extern void UISkin_set_ButtonTextColor_m6679F1B2204CFA98EE423F15150E324D823144A4 (void);
// 0x0000017A UnityEngine.Sprite SimpleFileBrowser.UISkin::get_ButtonBackground()
extern void UISkin_get_ButtonBackground_mB0580D2AF2C36C1DE1D3DF4A392AC38E1585AC73 (void);
// 0x0000017B System.Void SimpleFileBrowser.UISkin::set_ButtonBackground(UnityEngine.Sprite)
extern void UISkin_set_ButtonBackground_m7739D490658A3F1B79F8D53A97F18ACD9F2826FA (void);
// 0x0000017C UnityEngine.Color SimpleFileBrowser.UISkin::get_DropdownColor()
extern void UISkin_get_DropdownColor_m03D71B8A5D8498BCD2488063FD2FA3E71A6AADDA (void);
// 0x0000017D System.Void SimpleFileBrowser.UISkin::set_DropdownColor(UnityEngine.Color)
extern void UISkin_set_DropdownColor_mB7B05D7DE11A84C08D3A9A7D48CC8F1EE1AE78C5 (void);
// 0x0000017E UnityEngine.Color SimpleFileBrowser.UISkin::get_DropdownTextColor()
extern void UISkin_get_DropdownTextColor_mA22C0C21BFB974BBCEF01C1C839427CDBC4E2D48 (void);
// 0x0000017F System.Void SimpleFileBrowser.UISkin::set_DropdownTextColor(UnityEngine.Color)
extern void UISkin_set_DropdownTextColor_m4153CD8C628861F3A3C87583EF489E721750195C (void);
// 0x00000180 UnityEngine.Color SimpleFileBrowser.UISkin::get_DropdownArrowColor()
extern void UISkin_get_DropdownArrowColor_mD3BBDE2D0DF58F9886AC7F4025C0DB280B4178C9 (void);
// 0x00000181 System.Void SimpleFileBrowser.UISkin::set_DropdownArrowColor(UnityEngine.Color)
extern void UISkin_set_DropdownArrowColor_mF7CDA08A272624DC04C532FFB522A3B10C0EDECA (void);
// 0x00000182 UnityEngine.Color SimpleFileBrowser.UISkin::get_DropdownCheckmarkColor()
extern void UISkin_get_DropdownCheckmarkColor_m86BF3BA77417133802032D78CD3CFEA7F96F5EAA (void);
// 0x00000183 System.Void SimpleFileBrowser.UISkin::set_DropdownCheckmarkColor(UnityEngine.Color)
extern void UISkin_set_DropdownCheckmarkColor_mA6520323EE9413EEAADF91A6BB760B4420A24BD4 (void);
// 0x00000184 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_DropdownBackground()
extern void UISkin_get_DropdownBackground_m03239FBEA1B63109672B48A193ECD9A150ECB312 (void);
// 0x00000185 System.Void SimpleFileBrowser.UISkin::set_DropdownBackground(UnityEngine.Sprite)
extern void UISkin_set_DropdownBackground_mF7EB335ED79AFD53F7344604A1E28B93E602BE62 (void);
// 0x00000186 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_DropdownArrow()
extern void UISkin_get_DropdownArrow_m3E87EE49A0B2CD5557E989106CAA6B4ADB34E995 (void);
// 0x00000187 System.Void SimpleFileBrowser.UISkin::set_DropdownArrow(UnityEngine.Sprite)
extern void UISkin_set_DropdownArrow_mFC4181E1571132535996277DA94F5678A0ABFDA2 (void);
// 0x00000188 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_DropdownCheckmark()
extern void UISkin_get_DropdownCheckmark_m5F74D6FEC069548D9783C03607994960838A989D (void);
// 0x00000189 System.Void SimpleFileBrowser.UISkin::set_DropdownCheckmark(UnityEngine.Sprite)
extern void UISkin_set_DropdownCheckmark_mC8EF60554FF5818BE001EA96C6686431176FFEAA (void);
// 0x0000018A UnityEngine.Color SimpleFileBrowser.UISkin::get_ToggleColor()
extern void UISkin_get_ToggleColor_mA10581F220B24B9E839960099720FCA08676BED2 (void);
// 0x0000018B System.Void SimpleFileBrowser.UISkin::set_ToggleColor(UnityEngine.Color)
extern void UISkin_set_ToggleColor_mFC4C10896061D2CB6EA0F7B73184FF0480E46D89 (void);
// 0x0000018C UnityEngine.Color SimpleFileBrowser.UISkin::get_ToggleTextColor()
extern void UISkin_get_ToggleTextColor_mFDB717AEA72F0EA989D1D96C83C7A1EE9E264564 (void);
// 0x0000018D System.Void SimpleFileBrowser.UISkin::set_ToggleTextColor(UnityEngine.Color)
extern void UISkin_set_ToggleTextColor_mB7B07F8F090F8A76C09C1A20BB48F37949A9E2C1 (void);
// 0x0000018E UnityEngine.Color SimpleFileBrowser.UISkin::get_ToggleCheckmarkColor()
extern void UISkin_get_ToggleCheckmarkColor_m0A663CCFCA3898C5CDA8B872CB59E169A8A4D511 (void);
// 0x0000018F System.Void SimpleFileBrowser.UISkin::set_ToggleCheckmarkColor(UnityEngine.Color)
extern void UISkin_set_ToggleCheckmarkColor_mAA44730401700F54B28FA8524E7D03CA09A56D63 (void);
// 0x00000190 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_ToggleBackground()
extern void UISkin_get_ToggleBackground_mD8E5E9DF237868DFB8A82BDE7D8C85C8DF73CC84 (void);
// 0x00000191 System.Void SimpleFileBrowser.UISkin::set_ToggleBackground(UnityEngine.Sprite)
extern void UISkin_set_ToggleBackground_mDA8524E7B0004881622B06E452587A2F12F97AF7 (void);
// 0x00000192 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_ToggleCheckmark()
extern void UISkin_get_ToggleCheckmark_m2144C261014032236EC73FB9F2CE100FFF988DCB (void);
// 0x00000193 System.Void SimpleFileBrowser.UISkin::set_ToggleCheckmark(UnityEngine.Sprite)
extern void UISkin_set_ToggleCheckmark_m70BC5F254768CD93869659F3DFA38A37156EF1FC (void);
// 0x00000194 UnityEngine.Color SimpleFileBrowser.UISkin::get_ScrollbarBackgroundColor()
extern void UISkin_get_ScrollbarBackgroundColor_m38062AE154858D1A50A2A461B74ED429A41963B2 (void);
// 0x00000195 System.Void SimpleFileBrowser.UISkin::set_ScrollbarBackgroundColor(UnityEngine.Color)
extern void UISkin_set_ScrollbarBackgroundColor_m90CD6B95BA884242F4666183D1CD63122D76903A (void);
// 0x00000196 UnityEngine.Color SimpleFileBrowser.UISkin::get_ScrollbarColor()
extern void UISkin_get_ScrollbarColor_m86AEEEBFFF97B35E53255689A8E90574853D6672 (void);
// 0x00000197 System.Void SimpleFileBrowser.UISkin::set_ScrollbarColor(UnityEngine.Color)
extern void UISkin_set_ScrollbarColor_m44CE8C73FB8641C31754206789CC7B2D935AF08E (void);
// 0x00000198 System.Single SimpleFileBrowser.UISkin::get_FileHeight()
extern void UISkin_get_FileHeight_m784A41AB4290882A6FB5FF8F1D9A63B235F35A49 (void);
// 0x00000199 System.Void SimpleFileBrowser.UISkin::set_FileHeight(System.Single)
extern void UISkin_set_FileHeight_mB2F99F56E4AA30E4FA46E9E92E42DA0728873F6B (void);
// 0x0000019A System.Single SimpleFileBrowser.UISkin::get_FileIconsPadding()
extern void UISkin_get_FileIconsPadding_mA3FA133EEC4FEBC91FAA6FE058FAAA871BE8A868 (void);
// 0x0000019B System.Void SimpleFileBrowser.UISkin::set_FileIconsPadding(System.Single)
extern void UISkin_set_FileIconsPadding_mF08CCFD6786FA0750AB3228937ADCA4072055FEA (void);
// 0x0000019C UnityEngine.Color SimpleFileBrowser.UISkin::get_FileNormalBackgroundColor()
extern void UISkin_get_FileNormalBackgroundColor_mABCFD3A9EF29524E385C3D90EAA68CFE8A9698C3 (void);
// 0x0000019D System.Void SimpleFileBrowser.UISkin::set_FileNormalBackgroundColor(UnityEngine.Color)
extern void UISkin_set_FileNormalBackgroundColor_m7F9F3AF069062B1C2F0EC1565A8BF7AD8D06FF40 (void);
// 0x0000019E UnityEngine.Color SimpleFileBrowser.UISkin::get_FileAlternatingBackgroundColor()
extern void UISkin_get_FileAlternatingBackgroundColor_mCFAA250F27E5DCAD195D7F02B9A1DC7015243A44 (void);
// 0x0000019F System.Void SimpleFileBrowser.UISkin::set_FileAlternatingBackgroundColor(UnityEngine.Color)
extern void UISkin_set_FileAlternatingBackgroundColor_mA9C32C576B114F604F93C3CC5455982AA083E005 (void);
// 0x000001A0 UnityEngine.Color SimpleFileBrowser.UISkin::get_FileHoveredBackgroundColor()
extern void UISkin_get_FileHoveredBackgroundColor_m1ECB52EB8B9DE59A9143DEF75C3A40BD8A6A298D (void);
// 0x000001A1 System.Void SimpleFileBrowser.UISkin::set_FileHoveredBackgroundColor(UnityEngine.Color)
extern void UISkin_set_FileHoveredBackgroundColor_m8105DB5EDB09B73EEED40FB9F7860AD96F002D72 (void);
// 0x000001A2 UnityEngine.Color SimpleFileBrowser.UISkin::get_FileSelectedBackgroundColor()
extern void UISkin_get_FileSelectedBackgroundColor_mECCF9DED2D0EA5CD1F72560F2DB1086043F87DCD (void);
// 0x000001A3 System.Void SimpleFileBrowser.UISkin::set_FileSelectedBackgroundColor(UnityEngine.Color)
extern void UISkin_set_FileSelectedBackgroundColor_m94612EFFD515ACE645213AD8659D5A2DDF35ED0B (void);
// 0x000001A4 UnityEngine.Color SimpleFileBrowser.UISkin::get_FileNormalTextColor()
extern void UISkin_get_FileNormalTextColor_m94137BB83AB5D126D55ED6A541A5ABD11847E5C5 (void);
// 0x000001A5 System.Void SimpleFileBrowser.UISkin::set_FileNormalTextColor(UnityEngine.Color)
extern void UISkin_set_FileNormalTextColor_m815E3DF4ED0F60B5D2C01AFBA44DBCBC03C5F936 (void);
// 0x000001A6 UnityEngine.Color SimpleFileBrowser.UISkin::get_FileSelectedTextColor()
extern void UISkin_get_FileSelectedTextColor_m2C2C426E89BD3A050DE9BB4DA5BC9335BB34DBC3 (void);
// 0x000001A7 System.Void SimpleFileBrowser.UISkin::set_FileSelectedTextColor(UnityEngine.Color)
extern void UISkin_set_FileSelectedTextColor_m0542FACE02C0139E39D822F75B74653A4EDD66FB (void);
// 0x000001A8 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_FolderIcon()
extern void UISkin_get_FolderIcon_m10703DA30265BC01B71DCE95B3899ACC1BFA3979 (void);
// 0x000001A9 System.Void SimpleFileBrowser.UISkin::set_FolderIcon(UnityEngine.Sprite)
extern void UISkin_set_FolderIcon_m5836518F1CDB93540880D50E1B3CE3C3E7694BD5 (void);
// 0x000001AA UnityEngine.Sprite SimpleFileBrowser.UISkin::get_DriveIcon()
extern void UISkin_get_DriveIcon_m30D8DC96362267C3C305E915009D8F45439A2575 (void);
// 0x000001AB System.Void SimpleFileBrowser.UISkin::set_DriveIcon(UnityEngine.Sprite)
extern void UISkin_set_DriveIcon_m09FFC7F28281348F3EE69BE17CA6F2C8D9785BF6 (void);
// 0x000001AC UnityEngine.Sprite SimpleFileBrowser.UISkin::get_DefaultFileIcon()
extern void UISkin_get_DefaultFileIcon_m0EFB3E68E908956388276D9B993B97AED5A990B9 (void);
// 0x000001AD System.Void SimpleFileBrowser.UISkin::set_DefaultFileIcon(UnityEngine.Sprite)
extern void UISkin_set_DefaultFileIcon_mA09256F0754F51D10AFAA9E0B3B162CE02C57C7A (void);
// 0x000001AE SimpleFileBrowser.FiletypeIcon[] SimpleFileBrowser.UISkin::get_FiletypeIcons()
extern void UISkin_get_FiletypeIcons_mC985DADA521A84E41D92929712A839D4CCE8E10C (void);
// 0x000001AF System.Void SimpleFileBrowser.UISkin::set_FiletypeIcons(SimpleFileBrowser.FiletypeIcon[])
extern void UISkin_set_FiletypeIcons_m61D15175F5E4B79158BA7F7239177B6C0702EFE8 (void);
// 0x000001B0 System.Boolean SimpleFileBrowser.UISkin::get_AllIconExtensionsHaveSingleSuffix()
extern void UISkin_get_AllIconExtensionsHaveSingleSuffix_mB9A3FDE933F3B78A962D7A89874FE3A25CE4000E (void);
// 0x000001B1 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_FileMultiSelectionToggleOffIcon()
extern void UISkin_get_FileMultiSelectionToggleOffIcon_mA1589D7C53F9EFDA492585318B7A7933AA152242 (void);
// 0x000001B2 System.Void SimpleFileBrowser.UISkin::set_FileMultiSelectionToggleOffIcon(UnityEngine.Sprite)
extern void UISkin_set_FileMultiSelectionToggleOffIcon_m0BABD1AA434F15970A7C41B69355E01AA4760D75 (void);
// 0x000001B3 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_FileMultiSelectionToggleOnIcon()
extern void UISkin_get_FileMultiSelectionToggleOnIcon_mE08E625A3C9BD9198322B23A0E7AD22D469DB3F0 (void);
// 0x000001B4 System.Void SimpleFileBrowser.UISkin::set_FileMultiSelectionToggleOnIcon(UnityEngine.Sprite)
extern void UISkin_set_FileMultiSelectionToggleOnIcon_mEA070AE489AB06B5538636014CE7DE7F3E49166B (void);
// 0x000001B5 UnityEngine.Color SimpleFileBrowser.UISkin::get_ContextMenuBackgroundColor()
extern void UISkin_get_ContextMenuBackgroundColor_m9B8A8162A1E6A37BDBEEE42B21DDE638F9DF865B (void);
// 0x000001B6 System.Void SimpleFileBrowser.UISkin::set_ContextMenuBackgroundColor(UnityEngine.Color)
extern void UISkin_set_ContextMenuBackgroundColor_m5677743250073FE0C8A3BE5B6BB9DAF836BA774C (void);
// 0x000001B7 UnityEngine.Color SimpleFileBrowser.UISkin::get_ContextMenuTextColor()
extern void UISkin_get_ContextMenuTextColor_m2500FFA9D7A78C9D8FF3810C00D572621241926E (void);
// 0x000001B8 System.Void SimpleFileBrowser.UISkin::set_ContextMenuTextColor(UnityEngine.Color)
extern void UISkin_set_ContextMenuTextColor_m542C730DBDB0DE035A49EF6935CCD8526A7D5316 (void);
// 0x000001B9 UnityEngine.Color SimpleFileBrowser.UISkin::get_ContextMenuSeparatorColor()
extern void UISkin_get_ContextMenuSeparatorColor_m737A812132EB0D8B8CC8E92E47B82C8B19424D46 (void);
// 0x000001BA System.Void SimpleFileBrowser.UISkin::set_ContextMenuSeparatorColor(UnityEngine.Color)
extern void UISkin_set_ContextMenuSeparatorColor_mDF49F5F06612774DBF3674D2CE0F2830939C6058 (void);
// 0x000001BB UnityEngine.Color SimpleFileBrowser.UISkin::get_PopupPanelsBackgroundColor()
extern void UISkin_get_PopupPanelsBackgroundColor_m6FE42D39B8E6334BE7BCDF9CA5D036761BAC3409 (void);
// 0x000001BC System.Void SimpleFileBrowser.UISkin::set_PopupPanelsBackgroundColor(UnityEngine.Color)
extern void UISkin_set_PopupPanelsBackgroundColor_mA7DE119B7C57964ACC7B1DCA6C0DF78230A16272 (void);
// 0x000001BD UnityEngine.Color SimpleFileBrowser.UISkin::get_PopupPanelsTextColor()
extern void UISkin_get_PopupPanelsTextColor_m9D6F1AB55FBCFAE9AE713764075BEF0210AABE13 (void);
// 0x000001BE System.Void SimpleFileBrowser.UISkin::set_PopupPanelsTextColor(UnityEngine.Color)
extern void UISkin_set_PopupPanelsTextColor_m1F1DCCBBFF8A2949C20D52E7A1C9AA546EB3880E (void);
// 0x000001BF UnityEngine.Sprite SimpleFileBrowser.UISkin::get_PopupPanelsBackground()
extern void UISkin_get_PopupPanelsBackground_m62AEEFAE7A479500E9B25D5D629D69DB54AB1C0B (void);
// 0x000001C0 System.Void SimpleFileBrowser.UISkin::set_PopupPanelsBackground(UnityEngine.Sprite)
extern void UISkin_set_PopupPanelsBackground_m16548FF781FF5D14FF8FB26F010A61E5F410E857 (void);
// 0x000001C1 System.Void SimpleFileBrowser.UISkin::ApplyTo(UnityEngine.UI.Text,UnityEngine.Color)
extern void UISkin_ApplyTo_mD75CBB71DA4422BDDD0AA76ADE944F88101A285F (void);
// 0x000001C2 System.Void SimpleFileBrowser.UISkin::ApplyTo(UnityEngine.UI.InputField)
extern void UISkin_ApplyTo_mD9E18F1010A70A5A1EB431BF5DEDAB78EF25B948 (void);
// 0x000001C3 System.Void SimpleFileBrowser.UISkin::ApplyTo(UnityEngine.UI.Button)
extern void UISkin_ApplyTo_m6C2BFBADBF44FEF1BB0E77A63F0C2A6E78C14933 (void);
// 0x000001C4 System.Void SimpleFileBrowser.UISkin::ApplyTo(UnityEngine.UI.Dropdown)
extern void UISkin_ApplyTo_mAEEEBA685A4BFF29C29C7D0221E0F0D9F452F410 (void);
// 0x000001C5 System.Void SimpleFileBrowser.UISkin::ApplyTo(UnityEngine.UI.Toggle)
extern void UISkin_ApplyTo_mC10AA3BB1D71BB4267AA9073B8A9E342A36F271E (void);
// 0x000001C6 System.Void SimpleFileBrowser.UISkin::ApplyTo(UnityEngine.UI.Scrollbar)
extern void UISkin_ApplyTo_m2D73AF4AFF845AC150134E12A20B82207AE13773 (void);
// 0x000001C7 UnityEngine.Sprite SimpleFileBrowser.UISkin::GetIconForFileEntry(SimpleFileBrowser.FileSystemEntry,System.Boolean)
extern void UISkin_GetIconForFileEntry_m9E93DEA9F8837F7CD683060A9C25848138B65B4C (void);
// 0x000001C8 System.Void SimpleFileBrowser.UISkin::InitializeFiletypeIcons()
extern void UISkin_InitializeFiletypeIcons_m625E79C7BD8F0D5359AC39FE3690145FA5A703E5 (void);
// 0x000001C9 System.Void SimpleFileBrowser.UISkin::.ctor()
extern void UISkin__ctor_mF281CDB8B2AF4A68A487ADC8F4575B5A9E07AB71 (void);
static Il2CppMethodPointer s_methodPointers[457] = 
{
	FBCallbackHelper_Awake_mB8B69B074C05354A975DD11C5E747C4B73F4DB2E,
	FBCallbackHelper_Update_mB811E18A297FE94BA75C049913A38E9249502794,
	FBCallbackHelper_CallOnMainThread_m7263C7F17001BA043E6A5DABF76DCCEAE4E4266A,
	FBCallbackHelper__ctor_mBD9122BDF99C5606EF88E1DDE21839008E975897,
	FBDirectoryReceiveCallbackAndroid__ctor_m53D215598FBA53D3333D359293759E8452582F48,
	FBDirectoryReceiveCallbackAndroid_OnDirectoryPicked_m412BA955127668554C2AF55D6CC28166559B46F1,
	FBDirectoryReceiveCallbackAndroid_DirectoryPickedCallback_m7039234E4669DC9103BE940172E633E59D435800,
	U3CU3Ec__DisplayClass3_0__ctor_mB3AB85D2B5C46D597CF0E576067F99885DECAF0A,
	U3CU3Ec__DisplayClass3_0_U3COnDirectoryPickedU3Eb__0_m1603B9C63F5074A0440B02DA109258F2C746A2E1,
	FBPermissionCallbackAndroid_get_Result_m131BEEFCA19EC6152CEC8DB8C88EA1D8A56E3FA8,
	FBPermissionCallbackAndroid_set_Result_m213D385F82F35521EDEEFCAE6FAF01DEF27F3458,
	FBPermissionCallbackAndroid__ctor_mEE2AA74588F9FEA5D4D8F4DE7F18B053EB90DB0A,
	FBPermissionCallbackAndroid_OnPermissionResult_m3913733BDF92AB0A676888FF75FEDF5E444FAC4D,
	EventSystemHandler_OnEnable_m9D10F4CAB895730E81BBA104C07658ED4BADCB85,
	EventSystemHandler_OnDisable_mF4B18C44DDA3ECC217B112411284470C5C3DC9D4,
	EventSystemHandler_OnSceneLoaded_mCF2EB1E2FBF505556047B5357818AC5668339288,
	EventSystemHandler_OnSceneUnloaded_m9947DEFB077E5140E1E0667E38A2BF12C04A015C,
	EventSystemHandler_ActivateEventSystemIfNeeded_m8960B11D43A64D845502AB43765B7B3DF832C145,
	EventSystemHandler_DeactivateEventSystem_m587AB2F9F86139969B5B0A6DF83F3985F6D21A0F,
	EventSystemHandler__ctor_m1E08073F95F3C31EBA6D14474480FCE2BE198634,
	FileBrowser_get_IsOpen_m76F153181B2F972690A98BB60FA0B3E6E72ED7C6,
	FileBrowser_set_IsOpen_m132B34485185EB72B9F840606547745EACF08B6E,
	FileBrowser_get_Success_mB13435856C4E9A5FC6B87057E8800CD4473D00C7,
	FileBrowser_set_Success_mF82FCE01FA9934B22AB6AAE52484801AD5521320,
	FileBrowser_get_Result_mA521CA35A57DFB25B29C5CAEB5BCF94F3B5D5D44,
	FileBrowser_set_Result_mAC115C70FD40098215B44FB277BFCC4D49D56CF3,
	FileBrowser_get_Skin_m01C1027C832CAE1ACD0CB592E7B8686A41DDF37F,
	FileBrowser_set_Skin_mE2032BCA5CEE026DC4B09FAB9B07B6E9DDD3CDC4,
	FileBrowser_get_AskPermissions_m4D1AF6F162CDD1421E99F2FEEA470B0485D06696,
	FileBrowser_set_AskPermissions_m1BB9D378C8EC0378E97AC35673301973D3628392,
	FileBrowser_get_SingleClickMode_mD046AB6FA4994A68B94BB36DF515636CD9C9D6B2,
	FileBrowser_set_SingleClickMode_m1A10873DF50E0ED741452543A4D3C609D8C7AAEB,
	FileBrowser_add_DisplayedEntriesFilter_mB4B74173E5E6113286DA9931ED5A0102DB12B8DB,
	FileBrowser_remove_DisplayedEntriesFilter_m079E11328A44793B3FF1A68BFB982469094BD680,
	FileBrowser_get_ShowFileOverwriteDialog_m3DDB324AEFC613F81411864AC59E92FF3BFB6459,
	FileBrowser_set_ShowFileOverwriteDialog_m9F784F4E9D09535EB58F4B4FADB2C36E1CDCC053,
	FileBrowser_get_CheckWriteAccessToDestinationDirectory_m734B3AE9BBE8B0F809891ECE57142B5AA09DEAE8,
	FileBrowser_set_CheckWriteAccessToDestinationDirectory_m090B0F241B58DF128E8FD890EC1E2CC693C84A2A,
	FileBrowser_get_DrivesRefreshInterval_mF9A1BA2C1ED51D1AD62D59B702819D8024386768,
	FileBrowser_set_DrivesRefreshInterval_m76ED7834037AEBD521A9E468F91150F2F25F13EE,
	FileBrowser_get_ShowHiddenFiles_m23EEB1ED7622AB57021425618A8FBA9AD7FA1C05,
	FileBrowser_set_ShowHiddenFiles_m8608D1A058DC2A12E4B15FBEC6237AE86264DA19,
	FileBrowser_get_DisplayHiddenFilesToggle_mB72DDBC142BC9F1916F11947E7D1787AC94267A3,
	FileBrowser_set_DisplayHiddenFilesToggle_mACD3467C6D979B49E141C0DDA701D2FEEFD0EA73,
	FileBrowser_get_AllFilesFilterText_m1E35E8D8DDFA2183FFA83386533E9D880EA381B9,
	FileBrowser_set_AllFilesFilterText_m0DF904F97562955FC28115BB3FBEE173BD0BD744,
	FileBrowser_get_FoldersFilterText_m1A157B4031B3EC98C4F4CA8F60AEF1093794E14E,
	FileBrowser_set_FoldersFilterText_m7E4CB3272BB12AC16A47A3295A9A58A736D49E64,
	FileBrowser_get_PickFolderQuickLinkText_mAEFCEA9DE74D1B1A5C045A17730BB91D562D111C,
	FileBrowser_set_PickFolderQuickLinkText_m1939B72D8FB9EBD2ED57FAB352EA4E4AD24474D8,
	FileBrowser_get_Instance_m2D1CA7BB2DEE29A481F02D5DDC6B15727AE6457F,
	FileBrowser_get_AllExtensionsHaveSingleSuffix_m5B98B13935078D47B077A2E5473E44CAF9476278,
	FileBrowser_get_CurrentPath_m7AEBF65EEE1AAE67FC79949C9565CA382D5C1991,
	FileBrowser_set_CurrentPath_m9AE094D4E74E17CC9CF900ED71D6316713E0253E,
	FileBrowser_get_SearchString_m60BAB1281D2F408F902C290FEE3656CAC11ED288,
	FileBrowser_set_SearchString_m20BFE4C41DA445EFA2C00947BE6D18EE3A4EE337,
	FileBrowser_get_AcceptNonExistingFilename_mCC73584A920A441DE4DAF2D85CA7FC40A9815DBD,
	FileBrowser_set_AcceptNonExistingFilename_mA5362B26AB27A67FD0BCFEC0B18839A1A8179F74,
	FileBrowser_get_PickerMode_mA44EDEDB67BDD91DC42FA68CDF8A554D6498E9FB,
	FileBrowser_set_PickerMode_m3D0E21378984533DDC35DE55B7B1FCB7634863F8,
	FileBrowser_get_AllowMultiSelection_m862C120260F12908D02FCE55A1FB9B87F558BEBB,
	FileBrowser_set_AllowMultiSelection_m6FDF012EE485D450F0B68E5A1D9827DF2E52B986,
	FileBrowser_get_MultiSelectionToggleSelectionMode_m975C5D8A411A2755DF57798D39D2188FED2053F8,
	FileBrowser_set_MultiSelectionToggleSelectionMode_m176FE9DC46BDD2EB48C108D21831D1B08EB85934,
	FileBrowser_get_Title_m27BB4C891CE9E16CB8575D73DE6D4994A14261B4,
	FileBrowser_set_Title_m8F51E153087C97D275799A74D1B9248938BA72C7,
	FileBrowser_get_SubmitButtonText_m20489A29D7521B14D2EF387064647C74D6BEE7FD,
	FileBrowser_set_SubmitButtonText_m0B06037B82E56030B481A207250EB04BF448B1DF,
	FileBrowser_get_LastBrowsedFolder_m89508024E6A7AF675E464F160A6DC158FB43AA40,
	FileBrowser_set_LastBrowsedFolder_mB7CAA2B1E74906B21752B05ADF01FE0E7B6FF204,
	FileBrowser_Awake_m7ED8D2EAF2FC13A2995A236FED771977A01E29DE,
	FileBrowser_OnRectTransformDimensionsChange_m7656558630069B8EAFE99EAC07037AADA4F4A24B,
	FileBrowser_Update_mB3481CC859B7DF12AB0B40D02E9DB302D13E959F,
	FileBrowser_LateUpdate_mB72B0AF4CEE098E1E1617CBC742556A0443A676A,
	FileBrowser_OnApplicationFocus_m3A55A6E53DF3CB09D533CC71B396CB2C8192007A,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_get_OnItemClicked_m3571B66C7BCCCF5A952BC3B77ECA2EC8F7C31AE8,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_set_OnItemClicked_mAD8C5576E8F5DBEE4E9934E1E961646E18D13AD9,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_get_Count_m53920F2D439AB6729AACAAE8B10196C0E780DFEF,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_get_ItemHeight_m87809A489D1604A41B6264EB197215F5CF1164FB,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_CreateItem_m4B3594285FC355021488EDE0424C6A63CEF75909,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_SetItemContent_mFABF6CE61B6A8A87B1A810287172E11B56BBD44B,
	FileBrowser_InitializeQuickLinks_m3E58E20D0485E8685D61E4A5C21A2E81C3FCB1AA,
	FileBrowser_RefreshDriveQuickLinks_mFA2736938A9141A7AE8D4CCE963BF8599116025F,
	FileBrowser_RefreshSkin_m655D943EDA19691F340A1EDB9DC504BEF2E31507,
	FileBrowser_OnBackButtonPressed_mE07193E07A9F83C1975D4B56C407B27656458CD6,
	FileBrowser_OnForwardButtonPressed_m2386A29CF07AFCB08198E40F6E3C5164F285EEA0,
	FileBrowser_OnUpButtonPressed_m0D3B5941FF82CF88975BB820574CC3DBAF6F37B3,
	FileBrowser_OnMoreOptionsButtonClicked_mE6946C5641CAD3C5ADAACB66B59D184D586BCFFA,
	FileBrowser_OnContextMenuTriggered_m24D91C79FC0A88DFAD5548607BD9B8C145C1E761,
	FileBrowser_ShowContextMenuAt_m40500C533570BA07A1126123B3B097C63C374249,
	FileBrowser_OnSubmitButtonClicked_m7E848A843F3D025488C0EE6FF5E54EA6D57DCBE3,
	FileBrowser_OnCancelButtonClicked_mB5936764FEC583FA4F56C10619D3CF8BA60E96D8,
	FileBrowser_OnOperationSuccessful_m5DC93791ECD6CBA227C3FA4436DB0EBE91A27A6C,
	FileBrowser_OnOperationCanceled_m8D25FC9E848E384F3E878FF801A7993C36DF1947,
	FileBrowser_OnPathChanged_m690981D06142B84FA26FF5904150DB0CD3758D72,
	FileBrowser_OnSearchStringChanged_m455D848F1D77E8509209BEEA041E6F0AF1D0796F,
	FileBrowser_OnFilterChanged_m54C7B8ABB2A479ECA7994A22D3FA484E279D8B9B,
	FileBrowser_OnShowHiddenFilesToggleChanged_mA0BC97B0151F590135CAD8588EACC5AF6399A794,
	FileBrowser_OnItemSelected_m169A9AE1DED1C198827C499C3DE7DE1C25D884F9,
	FileBrowser_OnItemHeld_m12287097633B233E71C322067B79BED7956395DB,
	FileBrowser_OnSAFDirectoryPicked_m869B694845C301A1835784A117AEA7D1E86749C6,
	FileBrowser_FetchPersistedSAFQuickLinks_mEC9A78E59C56F58C5644CF9404A53A8ABA75FF4F,
	FileBrowser_OnValidateFilenameInput_mF3FEA1E1138C07A818B30E19B5A988BF742C7922,
	FileBrowser_OnFilenameInputChanged_m8597E8B6648695042D028063BA767891987F90F4,
	FileBrowser_Show_mFFC02C0EB9E1F56896F3AA2EE65AA8CB396FEE78,
	FileBrowser_Hide_mF5BFFE4E660A4C0BD26E4360676AE6C401B84335,
	FileBrowser_RefreshFiles_m78E8F197E37E572167B58ECF89907B0F937711D3,
	FileBrowser_FileSystemEntryMatchesFilters_mF30551D4CB3981102F8434FE12A90CA412DF64EE,
	FileBrowser_SelectAllFiles_mE91B7524DDC69BE8825340880B581B63772A070B,
	FileBrowser_DeselectAllFiles_m74394CF1CECC64623176BEDC0E0A85807DE5B526,
	FileBrowser_CreateNewFolder_m93131A2F5C3BBD4AE007C8E0D6AB310F5405E626,
	FileBrowser_CreateNewFolderCoroutine_mCC4E6964B0EEDC56E8C937CEBBFEAEA262C8581E,
	FileBrowser_RenameSelectedFile_m0BDC939786A45A504F9C74459EC23882ACD3A25B,
	FileBrowser_DeleteSelectedFiles_mB3E6B271A957C4519AA8A9AFDBABBB7863520A17,
	FileBrowser_PersistFileEntrySelection_m8A55E481241BD19BA81EC14E6B88F9B1557A9EDA,
	FileBrowser_AddQuickLink_mB4F5AF2799B7717685A57C2DDF444E319DE55B81,
	FileBrowser_ClearQuickLinksInternal_m942EF9858522B6740C416E790EB022984CC59E26,
	FileBrowser_EnsureScrollViewIsWithinBounds_m928C6CC80134DD6859C52BC6A7DB04C4EDB7B845,
	FileBrowser_EnsureWindowIsWithinBounds_m0C2BE0125CCF95DC08987F4EEC1334949F33560D,
	FileBrowser_OnWindowDimensionsChanged_mDFABDE888D6BF3701A840871D2A202DAF6279AB3,
	FileBrowser_GetIconForFileEntry_m27CDFFF69366A6DD7809BBDA3D49DED0B92DFEC0,
	FileBrowser_GetExtensionFromFilename_m2495AB77331771B0EB8CBD100BF6D81F779A36E5,
	FileBrowser_GetPathWithoutTrailingDirectorySeparator_m2C5C9B65DF3EDBB6DF9B415815F037C88832C5BE,
	FileBrowser_UpdateFilenameInputFieldWithSelection_mE42C35DBAC32BD4EA372AB25A9F1E8C01BB0A5FF,
	FileBrowser_ExtractFilenameFromInput_m839AB82ACA752B225AF295B030B0242D8E0BBF0B,
	FileBrowser_FilenameToFileEntryIndex_m314FDAD9ED8489A31A6F192083FCE420C1EF2127,
	FileBrowser_VerifyFilename_m246B3104BA5AB79EF45296154BAE8F8BEF301EF9,
	FileBrowser_CalculateLengthOfDropdownText_m03EEF8FBFE5FCDE52EC3C58157123E6E4F19D026,
	FileBrowser_GetInitialPath_m423E2A4E4AA334B2E2181314CB37C2B614DA256F,
	FileBrowser_CheckDirectoryWriteAccess_mDAFD493D3F5BE1DB70FC2F0B215CDE23FAA7BADA,
	FileBrowser_IsCtrlKeyHeld_m2A22066F676381F1D26BFD6BD8B9E554C72F68D2,
	FileBrowser_ShowSaveDialog_mC27C4233D80B769B4DD41FF34476296B811225C9,
	FileBrowser_ShowLoadDialog_m73918E026EA31133F15BB56A903D4C1191FED21F,
	FileBrowser_ShowDialogInternal_mA32167D762691C56DD9F733B1D0E24C4D7F1CA96,
	FileBrowser_HideDialog_mDA627E443C7CA60608031520B27488F52B9ABB90,
	FileBrowser_WaitForSaveDialog_m5454CF697CF1BE9FBD027F7020E98E095BEAA7FD,
	FileBrowser_WaitForLoadDialog_mE110ACB9DAAEC9C617822A5A2001382F345B58C9,
	FileBrowser_AddQuickLink_m9F3258A18B6735F8086E39AA94B53447CFF71747,
	FileBrowser_ClearQuickLinks_m044403DC97DCA1DD98473C7A76FFCED06107817E,
	FileBrowser_SetExcludedExtensions_mA9BC5DC0482E7055BF2DD270B3874BB0A31FD709,
	FileBrowser_SetFilters_mA1C1F9BAA756E48107E1BEDD4C1ED1E5271EECBE,
	FileBrowser_SetFilters_m0B63664B2101EC6189E35358DDD16481D5644214,
	FileBrowser_SetFilters_mAFC93DB0463A70423C79B058BD36CE01AFE9D84D,
	FileBrowser_SetFilters_m970A512FD08E081D6BABADED1B81C3C6D151ADCC,
	FileBrowser_SetFilters_mB60CC9235A36041CF175CE8BC913E3B961D58B77,
	FileBrowser_SetFiltersPreProcessing_mB9B0E5A320AC057252D4A7B93D255AC760D76CC0,
	FileBrowser_SetFiltersPostProcessing_mDF6CA0D8DE1B928D09DF8421499295881BBB2E85,
	FileBrowser_SetDefaultFilter_mCBC7A444E73202D821187EC3266D941001250D22,
	FileBrowser_CheckPermission_m5761448828E9FB82E4FBA4C236636855B9375B61,
	FileBrowser_RequestPermission_m51C864DFE488B7306A4ED77E41F322960CDBB4F5,
	FileBrowser__ctor_mE67BF18A7571B5FE7D82A7454AAB098D9A1AACD2,
	FileBrowser__cctor_m069A4B4A72D44AD588EDD19D064B46F52E7CD2F1,
	FileBrowser_U3CCreateNewFolderCoroutineU3Eb__241_0_mE4B278D5E88BC29E2F4F88DDA2F471F57ED0376D,
	FileBrowser_U3CDeleteSelectedFilesU3Eb__243_0_mC8B8270D31AF46160629E570D96CFC769CA83308,
	Filter__ctor_m1074FC06CD79E457F25F52026D634FEBF9ACCF10,
	Filter__ctor_m38D854E9B7172B814710C305CA0857452A2DE121,
	Filter__ctor_m125246E9D958C713FF3D86B5E02C9D6540B58185,
	Filter_MatchesExtension_mB6BA2D275B2A7B7A2B7863D23591832CE7D0B7DF,
	Filter_ToString_m9DCF19E38E806A743CA49A330EBEA08814B62753,
	OnSuccess__ctor_m71AD74DD97BD2E317E6CCD78F0B631AC4D3104D7,
	OnSuccess_Invoke_m7AC28204C5A1715F07274D5F73B66E39CD7D1317,
	OnSuccess_BeginInvoke_m99BB5894F15FCDD0F04BD6321CB1E7F19EF28FFE,
	OnSuccess_EndInvoke_mB36C87C496674F8F56D681D6E25384CAB0F18CBC,
	OnCancel__ctor_m1B12099B6EC65CE377D0A854F92BC24145CCF42C,
	OnCancel_Invoke_m91F1A69EF5776DCA7D9A5C5EC9A03C925CDEBD49,
	OnCancel_BeginInvoke_m21D3022399AFFCF3CFC9926B0597AE814B2E380C,
	OnCancel_EndInvoke_m11BD247D9B4669B70D8D3116C309BB8EE08023F4,
	FileSystemEntryFilter__ctor_mF7DA3A52405E454E79B105125606D6E8BAFF8F74,
	FileSystemEntryFilter_Invoke_mF9EE2CB6AD810BF7873EF479A6F57CE9A81969DF,
	FileSystemEntryFilter_BeginInvoke_mA07C1032C03172D6F020207D97E77631FA1D2055,
	FileSystemEntryFilter_EndInvoke_m18A618059F7D663A424111D2372986129FA0220E,
	AndroidSAFDirectoryPickCallback__ctor_mA74461FFAA226B63E9E44A9C221072ED68F45DE8,
	AndroidSAFDirectoryPickCallback_Invoke_m57B8746EC1134F2FD4F3782D7F3468E1F7E33076,
	AndroidSAFDirectoryPickCallback_BeginInvoke_mD5991AD25935F5FD2E404BF6245BBE018D425749,
	AndroidSAFDirectoryPickCallback_EndInvoke_mA9F40E882DDF96E33E27DD315C8526A6B508A69C,
	U3CU3Ec__DisplayClass220_0__ctor_mF9972CFE3E9D7C704E4FF4EEDB232DDC1E7F0FDF,
	U3CU3Ec__DisplayClass220_0_U3COnSubmitButtonClickedU3Eb__0_m8DBA90F0845F09706FF10BEA45D6452ED641384C,
	U3CU3Ec__cctor_m68751808F9A0583FD6BDFD4FB074F482E85FB494,
	U3CU3Ec__ctor_m0C7FD05A63A799955E2C175B16DD6E2FA642261E,
	U3CU3Ec_U3CRefreshFilesU3Eb__236_0_m4874DD1849D832573640A6C95CE59ECCE03F2C43,
	U3CCreateNewFolderCoroutineU3Ed__241__ctor_mDFD38C6B4A6455AEDEE398C93FA8C252AE88184B,
	U3CCreateNewFolderCoroutineU3Ed__241_System_IDisposable_Dispose_mB74B115992FE4B68A55E768310A997AC20A20926,
	U3CCreateNewFolderCoroutineU3Ed__241_MoveNext_mDC6CFEFA6CBF228341CDD23A2D4EBDBCB7B82798,
	U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0545F6447CE56AA4E999A8B6450E7F30A310430D,
	U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_IEnumerator_Reset_m5F77E4A7CA436A621AC00B2073FE17DB3EFF2850,
	U3CCreateNewFolderCoroutineU3Ed__241_System_Collections_IEnumerator_get_Current_m8E8B43EAB0828F57B8691DBEADCF694AA88101DC,
	U3CU3Ec__DisplayClass242_0__ctor_m7C7C48E12917367748721E92BE27F9DF7B0228B2,
	U3CU3Ec__DisplayClass242_0_U3CRenameSelectedFileU3Eb__0_m073741ADCE7FDC0ACCB58C46732E2AAB60CB890F,
	U3CWaitForSaveDialogU3Ed__265__ctor_m12F637ABD0A5EBB97C466D0F553D5B8D56033BD1,
	U3CWaitForSaveDialogU3Ed__265_System_IDisposable_Dispose_m7BBF2D25C1C1AC74A18CF70C32FF1FDEC15F9459,
	U3CWaitForSaveDialogU3Ed__265_MoveNext_mFABB79CE55C954CD101AAC64895FED46AA318176,
	U3CWaitForSaveDialogU3Ed__265_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9186D456077EC88F0B778027AD680C149E3D74CE,
	U3CWaitForSaveDialogU3Ed__265_System_Collections_IEnumerator_Reset_m38718E5D5CE7AA13F1F4B684775CED69286E1390,
	U3CWaitForSaveDialogU3Ed__265_System_Collections_IEnumerator_get_Current_mE3601AB26ED57A39433E48BB8716C87BD6F45B0F,
	U3CWaitForLoadDialogU3Ed__266__ctor_m3F8B1CF0E9CA9644D9E02111A683BD10962D1F5D,
	U3CWaitForLoadDialogU3Ed__266_System_IDisposable_Dispose_mEF304AF3D0BAE96043ECA7DAD60C537FBB38A35C,
	U3CWaitForLoadDialogU3Ed__266_MoveNext_m1603ACA4294B686207F02C7542E1D0A6DB24E557,
	U3CWaitForLoadDialogU3Ed__266_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE96087E920FB48FA18B5996C934D369C33B9B75,
	U3CWaitForLoadDialogU3Ed__266_System_Collections_IEnumerator_Reset_m783AE76EC116144B63DC8BE626E95732D3C0BF27,
	U3CWaitForLoadDialogU3Ed__266_System_Collections_IEnumerator_get_Current_mCF552CCACF7B0B0ED71A0EE750D029528C487043,
	FileBrowserAccessRestrictedPanel_Show_mBCD473301AB85C2BA6C6D2F7972507FDFCDE2D22,
	FileBrowserAccessRestrictedPanel_RefreshSkin_mF36763DA5E7DD72F3EBE1CDDD74358BAD0F4C91C,
	FileBrowserAccessRestrictedPanel_OKButtonClicked_mFD84702894080C9B7FBE5611BA9B70DEE53C1088,
	FileBrowserAccessRestrictedPanel__ctor_m6F4950CA0E640922744C3EBAA57A2BC73D3F1390,
	FileBrowserContextMenu_Show_m048F7147CB835F992FF2017662D8557D74937ED1,
	FileBrowserContextMenu_Hide_m74048A668955B61CF3C72AAA635387BD82631F64,
	FileBrowserContextMenu_RefreshSkin_mA679EA42D2EA903D444A1672076301D958DAAA52,
	FileBrowserContextMenu_OnSelectAllButtonClicked_mA30D36E00D69D1EF1F7083A1D3F9C4E6E6BDA4B0,
	FileBrowserContextMenu_OnDeselectAllButtonClicked_mB45F767EE9CD38BA3F1235D0D4B3CDC74D5562F9,
	FileBrowserContextMenu_OnCreateFolderButtonClicked_mD6CBE83296EB54560F9E3F933AFD521312D07376,
	FileBrowserContextMenu_OnDeleteButtonClicked_m99ACE4623BD9BFAFC79B94AA484D60CBEE2D91AB,
	FileBrowserContextMenu_OnRenameButtonClicked_m37DEA61748E86D2F41D6013F1106CE72AC6D5D00,
	FileBrowserContextMenu__ctor_m0CE266A05467D4C59C3BBD3E3D9A65AF477D995F,
	FileBrowserCursorHandler__ctor_m700BE779271111CF7F82596D9B059379BF6D9B79,
	FileBrowserFileOperationConfirmationPanel_Show_m15EBC0082F416DA80066E67B9C8784FBD19BA49C,
	FileBrowserFileOperationConfirmationPanel_Show_mE5E9BDBA381D3CC63CAA0096120133385FFF557E,
	FileBrowserFileOperationConfirmationPanel_OnCanvasDimensionsChanged_m38A3C39F813C2A4AC73194A075A3427FF5E896D8,
	FileBrowserFileOperationConfirmationPanel_RefreshSkin_mC41A8FA560E50E101C5A31A14983D0184AA1F136,
	FileBrowserFileOperationConfirmationPanel_YesButtonClicked_m6B61A55D8D71E9D1EF9BEBCF17AAFC43AD84FB74,
	FileBrowserFileOperationConfirmationPanel_NoButtonClicked_m59E628E01A36D1C8D34939CC5D63F726B001D1E2,
	FileBrowserFileOperationConfirmationPanel__ctor_mD9E5D3B05C389BD60E92BC04DD79505248AFFCA7,
	OnOperationConfirmed__ctor_m6D50A4AEC640684E8F1017EC7E4A709A27DA37CF,
	OnOperationConfirmed_Invoke_mBBE7933011170BC9A4DDBB037931A3E10469FBBE,
	OnOperationConfirmed_BeginInvoke_m3489C6887612AA70AF313950FCE0D4FC339647E7,
	OnOperationConfirmed_EndInvoke_m1E48D6AA660F27A97CFE5791EB604AC864A1082B,
	FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F,
	FileSystemEntry__ctor_m08BA758AC7BDAC9D19FA4C874BFE7FF6EC349ADA,
	FileSystemEntry__ctor_mF213C56704D663A552E1FD301772FA84A177828D,
	FileBrowserHelpers_get_AJC_m7A13844F2E1638F5A37A24E3EEC7122F997A27E5,
	FileBrowserHelpers_get_Context_m0D55D47A0B3CCD833D775D124A15C55626062052,
	FileBrowserHelpers_get_TemporaryFilePath_mABA3208150554B9028C2D76ABD44EA8B01776182,
	FileBrowserHelpers_get_ShouldUseSAF_m294D64E951F424A1922EB38E45D702B65424CFA8,
	FileBrowserHelpers_ShouldUseSAFForPath_m4A2F79FE5240CDCF6452CC9483FC6412E502A41F,
	FileBrowserHelpers_FileExists_mD3579F307508B344F0785F92CDF0F5668656D6DD,
	FileBrowserHelpers_DirectoryExists_m6F82D57E2F9C7AEA033D02D86C58797D6CF638BC,
	FileBrowserHelpers_IsDirectory_m58B5A826DB00DA28AA6132B4E59F1B327980212E,
	FileBrowserHelpers_IsPathDescendantOfAnother_m45D10B3F750E1D6F5660F79484142A0DD3703E00,
	FileBrowserHelpers_GetDirectoryName_m32F9849AA17666FC7491B9DEA3222CC7340B3A85,
	FileBrowserHelpers_GetEntriesInDirectory_mF3215D5020D4E60D0CB0FA3DBABDFE1716EE5880,
	FileBrowserHelpers_CreateFileInDirectory_m109E3355A5F2A7787577650E5F165CF816B54100,
	FileBrowserHelpers_CreateFolderInDirectory_mA81EF67AA61CA351367CFA7B99067C3840B82CCB,
	FileBrowserHelpers_WriteBytesToFile_m90771EDE57CC6ACB6F54BBA156DD88E4DAF15D86,
	FileBrowserHelpers_WriteTextToFile_m63B109F445EE1CD31AD94593C83D51866FBDB83D,
	FileBrowserHelpers_AppendBytesToFile_mCC55D795345B1399032C519A9CF8222C8DBFD0B7,
	FileBrowserHelpers_AppendTextToFile_m7F7FD975C7E5E738F53F68686D66DAB025F7AB96,
	FileBrowserHelpers_AppendFileToFile_m8BD1E3ACA58E0ACEB57836AC994BEBB7A84A243F,
	FileBrowserHelpers_ReadBytesFromFile_m59B3BBC5F09059FF548A69B3F14D05E4C0F479D3,
	FileBrowserHelpers_ReadTextFromFile_mEB28F5CB4D7682F1E04A908E6BD60967126F338D,
	FileBrowserHelpers_CopyFile_m07B3C712FBCDCB7FF8C32248EB8119BB823A1FC0,
	FileBrowserHelpers_CopyDirectory_mBDD288CF2FD8CA11206243BE2D971AB14BD2732C,
	FileBrowserHelpers_CopyDirectoryRecursively_m00DFF80853B380FF671EC7EC51BFE2C414F0509B,
	FileBrowserHelpers_MoveFile_m67DF8A9A06BE2FB651ADE67B2C7D010395B8BB55,
	FileBrowserHelpers_MoveDirectory_m5E407890B8916573A09E7335F38EBC75B26C728A,
	FileBrowserHelpers_RenameFile_mA204A30D5E284916B22AA30419A0412094C3AFB7,
	FileBrowserHelpers_RenameDirectory_m070EEAEDFAD3768F3900854F66EE12D126C92D4D,
	FileBrowserHelpers_DeleteFile_mFED2EA416401BED8C9826D66C61808EA9E4202CC,
	FileBrowserHelpers_DeleteDirectory_mA531BA313D6566C3430BE47A38C72CCEF1B66606,
	FileBrowserHelpers_GetFilename_mBE32212132286ECDDF7E722032A9E6CAFEEEF044,
	FileBrowserHelpers_GetFilesize_m983FA57D636D262551E458A8689F717F7A36EAAD,
	FileBrowserHelpers_GetLastModifiedDate_m91A46C094E1B3475E56ACC38E86A5B1D85FE8755,
	FileBrowserHelpers__cctor_m8E7397ABED927DA005F6A603AC24B15C3DBB4660,
	FileBrowserItem_get_Icon_m883D60BCF0DF40A8F3435064999B0C9FA2718175,
	FileBrowserItem_get_TransformComponent_mEDD8CD474DBBF4B58E11D1BE82C255CAE65141CA,
	FileBrowserItem_get_Name_m1F09E0A5E84870485231447173A5AF496DC0E01F,
	FileBrowserItem_get_IsDirectory_mC40A8B51825BC34EB34D45C72D35A1CA6E1A89BB,
	FileBrowserItem_set_IsDirectory_mDB0D5D005A9C6F689AF30D6A63E8AE555FFF6DF6,
	FileBrowserItem_SetFileBrowser_m9C9ADC77158FD31D95E816DEAA1439DB305E8AA9,
	FileBrowserItem_SetFile_m1F1468944380FAE443BB546ADF30AC56B3C6616D,
	FileBrowserItem_Update_m3A16C87E26FCD94FB63A74DBD6F10ED09E6D7FC4,
	FileBrowserItem_OnPointerClick_m324DF382072BE76D6FFDBE62E8C2DFABD47A6065,
	FileBrowserItem_OnPointerDown_m4EA10AF080820CE9779BDE00E61CFF69C9BE4BF8,
	FileBrowserItem_OnPointerUp_m7CEDD0C86CCBB748225E07597536AB8FB9D8436A,
	FileBrowserItem_SetSelected_mF1A4735B8F234281F0698E5C0DA0B9575C0D4C28,
	FileBrowserItem_SetHidden_m91D66EE00C0EEAF1FFEB6C9220BF363310271F28,
	FileBrowserItem_OnSkinRefreshed_mF1600D4A8CBD447F719FDFE284DFA88E68022626,
	FileBrowserItem__ctor_m78CFC23F9073522F0BBFB7F9792BAA0A46362454,
	FileBrowserMovement_Initialize_m471ABE4C29F9D705461783AB33F2CC921AC51478,
	FileBrowserMovement_OnDragStarted_m3E0201898C42B961B9D8194C95DDB8EEA8555B65,
	FileBrowserMovement_OnDrag_mD8D2918E9F95EFFDCC104E321E5B957850743AF8,
	FileBrowserMovement_OnEndDrag_m443E3696DBB3571E87FC1B616BDE956C5F3A506E,
	FileBrowserMovement_OnResizeStarted_mB31A17958B7A24A16CBE1B21D9B22451A2C0D486,
	FileBrowserMovement_OnResize_m49E6F628633974DE0F8E1737D127FB03BE774853,
	FileBrowserMovement_OnEndResize_mFB53FC3931D90175FE576DA10B16F94CD75BEC27,
	FileBrowserMovement__ctor_mB4D1B389EACB5128B7FC728B439AFE488642E95D,
	FileBrowserQuickLink_get_TargetPath_mD1CA8CA83A2B19226D00B8E77BC024A57D0C4C91,
	FileBrowserQuickLink_SetQuickLink_mDEDE06D8E3C9A90193FB71E57336A68E92895350,
	FileBrowserQuickLink__ctor_mCED0CE4E2EDD195AAB3F2BBA553F9EFC9715D8A5,
	FileBrowserRenamedItem_get_InputField_m83330E63A271A5280D8E444601AB15D1C58786D8,
	FileBrowserRenamedItem_get_TransformComponent_m45A05A656AB0AAB92E504A39469F821D2CCC7DE4,
	FileBrowserRenamedItem_Show_m67CF833D01BEDC56743DF4EAC46AF60E8FFFB5DB,
	FileBrowserRenamedItem_OnInputFieldEndEdit_mB5AC8C49EEF73640F34CB94F2D5FA62F1C48C366,
	FileBrowserRenamedItem__ctor_m32B6017C97863E0349A0D4B61D355CD9EFECE11B,
	OnRenameCompleted__ctor_m019658E790544491DDBD4F208F8B422DC77764EB,
	OnRenameCompleted_Invoke_m8A1B85475DE7D5362CB711D63543CDD6A68A7CDB,
	OnRenameCompleted_BeginInvoke_m090E447625A7D2DCE9AE24887390FB68BAD85E31,
	OnRenameCompleted_EndInvoke_mEC5511E093D8247D914A8802352DC8DA57CFB1B2,
	NonDrawingGraphic_SetMaterialDirty_m329A03BC12E9779085DC0D91E2DE2582127E8395,
	NonDrawingGraphic_SetVerticesDirty_mD732D935D4951495843EC5A9EA1DB19665415793,
	NonDrawingGraphic_OnPopulateMesh_m1C0A9BA5BE3AE3AAC6CBFE8F742E4AB51823198C,
	NonDrawingGraphic__ctor_m995D20F1C7EA51EC17D9E9D5F8F10A226597D6BB,
	OnItemClickedHandler__ctor_m2E672645F74ACB3CB0B7F152205B647BCB399FAC,
	OnItemClickedHandler_Invoke_mE3A8562310E72EC8ADF01FA3693D1B02A010B9D3,
	OnItemClickedHandler_BeginInvoke_mAC5A0CEC312EBB2FEB0BBACA665E2FB8B43226E3,
	OnItemClickedHandler_EndInvoke_m89D2C7968FA251487A47AFC4D2FFAFB65C892E3C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ListItem_get_Tag_mA71AE4254B5B31A3B822DB8F1674D50AB25A3041,
	ListItem_set_Tag_m74393E301D898520AB53B5AA336ACFBC97EC787E,
	ListItem_get_Position_mB4639770E22F328A1496CDCA7FCB936D65E1FFAE,
	ListItem_set_Position_mD18CBDB9DDB4D5C2E68E5CFA0B2C7D2B94540DFE,
	ListItem_SetAdapter_m5A6BE935C41666EC29E6AFA47207C30F2AB2FD0D,
	ListItem_OnClick_m950C0BA1BCA2C8B4EE9DE7A8044E822169B83669,
	ListItem__ctor_m108C9F53A120596095F9DDD0A2ABFD0EF0B2084C,
	RecycledListView_Start_mBE55ACC0C6D315D3337AD031F6F9CC15421AA23A,
	RecycledListView_SetAdapter_m06A0E4AF885C03B6B9F6A3E1933F83A3E90541B7,
	RecycledListView_OnSkinRefreshed_mEDFC858497AC069C64149A523C6A41CA77C4CC84,
	RecycledListView_UpdateList_m0637428860547148FA94CA0267BBC6E06911F29F,
	RecycledListView_OnViewportDimensionsChanged_m62B43716804A2EF7DB7F8E45508B2A780D06A4C2,
	RecycledListView_UpdateItemsInTheList_mFFE94878442D42C7B7F49A08025E4511FDFCCBC0,
	RecycledListView_CreateItemsBetweenIndices_m1EBA714FEB4E87540C618C5C01A2176140CF34B0,
	RecycledListView_CreateItemAtIndex_m848A2F8857745DC016A54F1CC80B138A364664BB,
	RecycledListView_DestroyItemsBetweenIndices_m9DD81FA19B89E35FCC06583C85EB8299D7FD8137,
	RecycledListView_UpdateItemContentsBetweenIndices_m7C71B732BD2850BAC39B8CBE1AEE1A592451A0F4,
	RecycledListView__ctor_m360F5EF4D831B9AEAC4D7E023CABA6F2931F1F7E,
	RecycledListView_U3CStartU3Eb__10_0_m24E1DAB94C847738A00D58ED91844281DAFC0696,
	UISkin_get_Version_mDEB52FC78056118447FD6E860344A046AA3AF3BC,
	UISkin_Invalidate_mF0FE40247D7E67A8FBC84DEC8EA36BE9E11ED089,
	UISkin_get_Font_m1552BEAA0A3BBBFCE0BD560333FB5FA4107E939D,
	UISkin_set_Font_m8141DB785C4C87E748E34BD1CE7C2518937359FB,
	UISkin_get_FontSize_mFEAD4AD2A1616FF6472909374486EEEF86BA9DBA,
	UISkin_set_FontSize_mA65E49B2321F1A0EDCC455B8B1DECA2EE9859286,
	UISkin_get_WindowColor_m367BFEF2404BC9C6D4332A86B385DB53D8D4C6E0,
	UISkin_set_WindowColor_m1739473F28A992891FE6A3B54FBE8EEE0F5AF873,
	UISkin_get_FilesListColor_mAEC97EF9E2EBF67C06BCEB99577FF8F644FFA46C,
	UISkin_set_FilesListColor_mAD96AD5C1018EDC0663BDECD91B7D62F3E4DA8D8,
	UISkin_get_FilesVerticalSeparatorColor_m54206BF03DED84B48BA6B33D618466340CA4986A,
	UISkin_set_FilesVerticalSeparatorColor_m31EA2F064048AFBC2C6986DA4483BABA2EFA045B,
	UISkin_get_TitleBackgroundColor_m00BCA39F5548EB231D00861F264B84885E13AAAE,
	UISkin_set_TitleBackgroundColor_mFFB484E2D39B9B8DF85EA1D1F34424E0863D7093,
	UISkin_get_TitleTextColor_mD7D42B4EA169A95EB13B2321814DC1C11CCE2060,
	UISkin_set_TitleTextColor_m9904013AABE86133AF0596C9445B56FECBE4FC32,
	UISkin_get_WindowResizeGizmoColor_mAC5C2F897A3EA08375CEBB3E59FC3CE9927AEFB0,
	UISkin_set_WindowResizeGizmoColor_m73A3957EE7B045D9A5FE16AE48855F834F1A4474,
	UISkin_get_HeaderButtonsColor_m31EB94F085067FF0528DBFE0B6F00BFC763792C5,
	UISkin_set_HeaderButtonsColor_m597E212FE241FCA9197391FE02B694278482881B,
	UISkin_get_WindowResizeGizmo_m53E6F918D39261E32F7E235BB43C3CE93E87899B,
	UISkin_set_WindowResizeGizmo_mC373651E93ED96B4BBFE3BD8472FDFFBE93754D0,
	UISkin_get_HeaderBackButton_mFE7BF99995C4E03A69537BD03C920732CACA0C98,
	UISkin_set_HeaderBackButton_m01FACB6087977D06B815F3F3FF098F30D1889715,
	UISkin_get_HeaderForwardButton_m0AB55675E37203A316E70E7AC6962F5D9404A83E,
	UISkin_set_HeaderForwardButton_m97EB75D74E491353B2AB5F22CC60718007FB5B09,
	UISkin_get_HeaderUpButton_m2F8A0E79E6EF0FC107B4C17F003D2722679528D2,
	UISkin_set_HeaderUpButton_mAB91307BAF755156C876EA718D3FE6C1AA290A56,
	UISkin_get_HeaderContextMenuButton_m99137E276EF90F2FABA6982F91848A4F3607D4CF,
	UISkin_set_HeaderContextMenuButton_mD1B4B578CDEE9AF5880EE0AE5E0DE8E6776DF1A6,
	UISkin_get_InputFieldNormalBackgroundColor_mF870D1EF69BA439EE2020669A25EEF56B3129B00,
	UISkin_set_InputFieldNormalBackgroundColor_mD80211096A36637A8692509F8A9182F4BEA2EB7B,
	UISkin_get_InputFieldInvalidBackgroundColor_m9B27CD7446BF26DC1AE8FFB83B4B8BB522E2FC87,
	UISkin_set_InputFieldInvalidBackgroundColor_m5631498B98FE5F8F544517176D0ED7AD996E27A1,
	UISkin_get_InputFieldTextColor_mE7D55005ECF6791606337569B171E38CC6650521,
	UISkin_set_InputFieldTextColor_mF3FC41494D35612E81DE87973623B6A8C82EAED1,
	UISkin_get_InputFieldPlaceholderTextColor_mCBE0A7B179B3AF4E012BD40F983F97F05FADBD14,
	UISkin_set_InputFieldPlaceholderTextColor_m409358A8B62DEE0EDA1D7E5B07FFE9A0D0B04EFA,
	UISkin_get_InputFieldSelectedTextColor_mFBB0E015FD5218DE4D510D7AA294C18726280694,
	UISkin_set_InputFieldSelectedTextColor_mC52EFF3E4B6B3785BDDADE6259629EE76297B449,
	UISkin_get_InputFieldCaretColor_m629083D3778AA9C526D8477BF89015AAA061ABAD,
	UISkin_set_InputFieldCaretColor_m6472937FA19EF8F995D8F722847D401E9266ECB7,
	UISkin_get_InputFieldBackground_m00B5F061A1F1ED9C3848BC9D726D9FEC778296F6,
	UISkin_set_InputFieldBackground_m0ADEC56B2C91599776C29EC0FAC2D1E4839A1035,
	UISkin_get_ButtonColor_m25E3D9790C3240828B57E2CFCB16ADC617203F14,
	UISkin_set_ButtonColor_m613746EE1E12F0856A4B717D2524321145E1D1E0,
	UISkin_get_ButtonTextColor_mC0C9600B8D7A85DF94F003F19E288CCD97E6975D,
	UISkin_set_ButtonTextColor_m6679F1B2204CFA98EE423F15150E324D823144A4,
	UISkin_get_ButtonBackground_mB0580D2AF2C36C1DE1D3DF4A392AC38E1585AC73,
	UISkin_set_ButtonBackground_m7739D490658A3F1B79F8D53A97F18ACD9F2826FA,
	UISkin_get_DropdownColor_m03D71B8A5D8498BCD2488063FD2FA3E71A6AADDA,
	UISkin_set_DropdownColor_mB7B05D7DE11A84C08D3A9A7D48CC8F1EE1AE78C5,
	UISkin_get_DropdownTextColor_mA22C0C21BFB974BBCEF01C1C839427CDBC4E2D48,
	UISkin_set_DropdownTextColor_m4153CD8C628861F3A3C87583EF489E721750195C,
	UISkin_get_DropdownArrowColor_mD3BBDE2D0DF58F9886AC7F4025C0DB280B4178C9,
	UISkin_set_DropdownArrowColor_mF7CDA08A272624DC04C532FFB522A3B10C0EDECA,
	UISkin_get_DropdownCheckmarkColor_m86BF3BA77417133802032D78CD3CFEA7F96F5EAA,
	UISkin_set_DropdownCheckmarkColor_mA6520323EE9413EEAADF91A6BB760B4420A24BD4,
	UISkin_get_DropdownBackground_m03239FBEA1B63109672B48A193ECD9A150ECB312,
	UISkin_set_DropdownBackground_mF7EB335ED79AFD53F7344604A1E28B93E602BE62,
	UISkin_get_DropdownArrow_m3E87EE49A0B2CD5557E989106CAA6B4ADB34E995,
	UISkin_set_DropdownArrow_mFC4181E1571132535996277DA94F5678A0ABFDA2,
	UISkin_get_DropdownCheckmark_m5F74D6FEC069548D9783C03607994960838A989D,
	UISkin_set_DropdownCheckmark_mC8EF60554FF5818BE001EA96C6686431176FFEAA,
	UISkin_get_ToggleColor_mA10581F220B24B9E839960099720FCA08676BED2,
	UISkin_set_ToggleColor_mFC4C10896061D2CB6EA0F7B73184FF0480E46D89,
	UISkin_get_ToggleTextColor_mFDB717AEA72F0EA989D1D96C83C7A1EE9E264564,
	UISkin_set_ToggleTextColor_mB7B07F8F090F8A76C09C1A20BB48F37949A9E2C1,
	UISkin_get_ToggleCheckmarkColor_m0A663CCFCA3898C5CDA8B872CB59E169A8A4D511,
	UISkin_set_ToggleCheckmarkColor_mAA44730401700F54B28FA8524E7D03CA09A56D63,
	UISkin_get_ToggleBackground_mD8E5E9DF237868DFB8A82BDE7D8C85C8DF73CC84,
	UISkin_set_ToggleBackground_mDA8524E7B0004881622B06E452587A2F12F97AF7,
	UISkin_get_ToggleCheckmark_m2144C261014032236EC73FB9F2CE100FFF988DCB,
	UISkin_set_ToggleCheckmark_m70BC5F254768CD93869659F3DFA38A37156EF1FC,
	UISkin_get_ScrollbarBackgroundColor_m38062AE154858D1A50A2A461B74ED429A41963B2,
	UISkin_set_ScrollbarBackgroundColor_m90CD6B95BA884242F4666183D1CD63122D76903A,
	UISkin_get_ScrollbarColor_m86AEEEBFFF97B35E53255689A8E90574853D6672,
	UISkin_set_ScrollbarColor_m44CE8C73FB8641C31754206789CC7B2D935AF08E,
	UISkin_get_FileHeight_m784A41AB4290882A6FB5FF8F1D9A63B235F35A49,
	UISkin_set_FileHeight_mB2F99F56E4AA30E4FA46E9E92E42DA0728873F6B,
	UISkin_get_FileIconsPadding_mA3FA133EEC4FEBC91FAA6FE058FAAA871BE8A868,
	UISkin_set_FileIconsPadding_mF08CCFD6786FA0750AB3228937ADCA4072055FEA,
	UISkin_get_FileNormalBackgroundColor_mABCFD3A9EF29524E385C3D90EAA68CFE8A9698C3,
	UISkin_set_FileNormalBackgroundColor_m7F9F3AF069062B1C2F0EC1565A8BF7AD8D06FF40,
	UISkin_get_FileAlternatingBackgroundColor_mCFAA250F27E5DCAD195D7F02B9A1DC7015243A44,
	UISkin_set_FileAlternatingBackgroundColor_mA9C32C576B114F604F93C3CC5455982AA083E005,
	UISkin_get_FileHoveredBackgroundColor_m1ECB52EB8B9DE59A9143DEF75C3A40BD8A6A298D,
	UISkin_set_FileHoveredBackgroundColor_m8105DB5EDB09B73EEED40FB9F7860AD96F002D72,
	UISkin_get_FileSelectedBackgroundColor_mECCF9DED2D0EA5CD1F72560F2DB1086043F87DCD,
	UISkin_set_FileSelectedBackgroundColor_m94612EFFD515ACE645213AD8659D5A2DDF35ED0B,
	UISkin_get_FileNormalTextColor_m94137BB83AB5D126D55ED6A541A5ABD11847E5C5,
	UISkin_set_FileNormalTextColor_m815E3DF4ED0F60B5D2C01AFBA44DBCBC03C5F936,
	UISkin_get_FileSelectedTextColor_m2C2C426E89BD3A050DE9BB4DA5BC9335BB34DBC3,
	UISkin_set_FileSelectedTextColor_m0542FACE02C0139E39D822F75B74653A4EDD66FB,
	UISkin_get_FolderIcon_m10703DA30265BC01B71DCE95B3899ACC1BFA3979,
	UISkin_set_FolderIcon_m5836518F1CDB93540880D50E1B3CE3C3E7694BD5,
	UISkin_get_DriveIcon_m30D8DC96362267C3C305E915009D8F45439A2575,
	UISkin_set_DriveIcon_m09FFC7F28281348F3EE69BE17CA6F2C8D9785BF6,
	UISkin_get_DefaultFileIcon_m0EFB3E68E908956388276D9B993B97AED5A990B9,
	UISkin_set_DefaultFileIcon_mA09256F0754F51D10AFAA9E0B3B162CE02C57C7A,
	UISkin_get_FiletypeIcons_mC985DADA521A84E41D92929712A839D4CCE8E10C,
	UISkin_set_FiletypeIcons_m61D15175F5E4B79158BA7F7239177B6C0702EFE8,
	UISkin_get_AllIconExtensionsHaveSingleSuffix_mB9A3FDE933F3B78A962D7A89874FE3A25CE4000E,
	UISkin_get_FileMultiSelectionToggleOffIcon_mA1589D7C53F9EFDA492585318B7A7933AA152242,
	UISkin_set_FileMultiSelectionToggleOffIcon_m0BABD1AA434F15970A7C41B69355E01AA4760D75,
	UISkin_get_FileMultiSelectionToggleOnIcon_mE08E625A3C9BD9198322B23A0E7AD22D469DB3F0,
	UISkin_set_FileMultiSelectionToggleOnIcon_mEA070AE489AB06B5538636014CE7DE7F3E49166B,
	UISkin_get_ContextMenuBackgroundColor_m9B8A8162A1E6A37BDBEEE42B21DDE638F9DF865B,
	UISkin_set_ContextMenuBackgroundColor_m5677743250073FE0C8A3BE5B6BB9DAF836BA774C,
	UISkin_get_ContextMenuTextColor_m2500FFA9D7A78C9D8FF3810C00D572621241926E,
	UISkin_set_ContextMenuTextColor_m542C730DBDB0DE035A49EF6935CCD8526A7D5316,
	UISkin_get_ContextMenuSeparatorColor_m737A812132EB0D8B8CC8E92E47B82C8B19424D46,
	UISkin_set_ContextMenuSeparatorColor_mDF49F5F06612774DBF3674D2CE0F2830939C6058,
	UISkin_get_PopupPanelsBackgroundColor_m6FE42D39B8E6334BE7BCDF9CA5D036761BAC3409,
	UISkin_set_PopupPanelsBackgroundColor_mA7DE119B7C57964ACC7B1DCA6C0DF78230A16272,
	UISkin_get_PopupPanelsTextColor_m9D6F1AB55FBCFAE9AE713764075BEF0210AABE13,
	UISkin_set_PopupPanelsTextColor_m1F1DCCBBFF8A2949C20D52E7A1C9AA546EB3880E,
	UISkin_get_PopupPanelsBackground_m62AEEFAE7A479500E9B25D5D629D69DB54AB1C0B,
	UISkin_set_PopupPanelsBackground_m16548FF781FF5D14FF8FB26F010A61E5F410E857,
	UISkin_ApplyTo_mD75CBB71DA4422BDDD0AA76ADE944F88101A285F,
	UISkin_ApplyTo_mD9E18F1010A70A5A1EB431BF5DEDAB78EF25B948,
	UISkin_ApplyTo_m6C2BFBADBF44FEF1BB0E77A63F0C2A6E78C14933,
	UISkin_ApplyTo_mAEEEBA685A4BFF29C29C7D0221E0F0D9F452F410,
	UISkin_ApplyTo_mC10AA3BB1D71BB4267AA9073B8A9E342A36F271E,
	UISkin_ApplyTo_m2D73AF4AFF845AC150134E12A20B82207AE13773,
	UISkin_GetIconForFileEntry_m9E93DEA9F8837F7CD683060A9C25848138B65B4C,
	UISkin_InitializeFiletypeIcons_m625E79C7BD8F0D5359AC39FE3690145FA5A703E5,
	UISkin__ctor_mF281CDB8B2AF4A68A487ADC8F4575B5A9E07AB71,
};
extern void FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F_AdjustorThunk (void);
extern void FileSystemEntry__ctor_m08BA758AC7BDAC9D19FA4C874BFE7FF6EC349ADA_AdjustorThunk (void);
extern void FileSystemEntry__ctor_mF213C56704D663A552E1FD301772FA84A177828D_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[3] = 
{
	{ 0x060000E2, FileSystemEntry_get_IsDirectory_mC0A5CA152877707F28A9F9B7C2C326B1739DB74F_AdjustorThunk },
	{ 0x060000E3, FileSystemEntry__ctor_m08BA758AC7BDAC9D19FA4C874BFE7FF6EC349ADA_AdjustorThunk },
	{ 0x060000E4, FileSystemEntry__ctor_mF213C56704D663A552E1FD301772FA84A177828D_AdjustorThunk },
};
static const int32_t s_InvokerIndices[457] = 
{
	3595,
	3595,
	2856,
	3595,
	2856,
	1582,
	1582,
	3595,
	3595,
	3518,
	2842,
	2856,
	2842,
	3595,
	3595,
	1610,
	2882,
	3595,
	3595,
	3595,
	5363,
	5290,
	5363,
	5290,
	5377,
	5298,
	5377,
	5298,
	5363,
	5290,
	5363,
	5290,
	5298,
	5298,
	5363,
	5290,
	5363,
	5290,
	5386,
	5302,
	5363,
	5290,
	5363,
	5290,
	5377,
	5298,
	5377,
	5298,
	5377,
	5298,
	5377,
	3478,
	3533,
	2856,
	3533,
	2856,
	3478,
	2797,
	3518,
	2842,
	3478,
	2797,
	3478,
	2797,
	3533,
	2856,
	3533,
	2856,
	3533,
	2856,
	3595,
	3595,
	3595,
	3595,
	2797,
	3533,
	2856,
	3518,
	3563,
	3533,
	2856,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	2907,
	1632,
	3595,
	3595,
	2856,
	2797,
	2856,
	2856,
	3595,
	3595,
	1571,
	2856,
	1582,
	3595,
	882,
	2856,
	1582,
	3595,
	2797,
	1027,
	3595,
	3595,
	3595,
	3533,
	3595,
	3595,
	3595,
	708,
	3595,
	3595,
	3595,
	2907,
	2545,
	4722,
	2550,
	3595,
	758,
	2414,
	2080,
	2414,
	2550,
	2080,
	3478,
	3703,
	3703,
	3689,
	5290,
	3781,
	3781,
	4252,
	5396,
	5298,
	5290,
	4835,
	4835,
	4835,
	4835,
	5290,
	5396,
	4969,
	5371,
	5371,
	3595,
	5396,
	2856,
	3595,
	2856,
	1582,
	1582,
	1059,
	3533,
	1581,
	2856,
	830,
	2856,
	1581,
	3595,
	1264,
	2856,
	1581,
	2051,
	800,
	2080,
	1581,
	1582,
	498,
	2856,
	3595,
	3595,
	5396,
	3595,
	1177,
	2842,
	3595,
	3478,
	3533,
	3595,
	3533,
	3595,
	2856,
	2842,
	3595,
	3478,
	3533,
	3595,
	3533,
	2842,
	3595,
	3478,
	3533,
	3595,
	3533,
	3595,
	2856,
	3595,
	3595,
	174,
	3595,
	2856,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	3595,
	633,
	320,
	2907,
	2856,
	3595,
	3595,
	3595,
	1581,
	3595,
	1264,
	2856,
	3478,
	636,
	1582,
	5377,
	5377,
	5377,
	5363,
	4969,
	4969,
	4969,
	4969,
	4574,
	5160,
	4722,
	4731,
	4731,
	4866,
	4866,
	4866,
	4866,
	4866,
	5160,
	5160,
	4866,
	4866,
	4866,
	4866,
	4866,
	4731,
	4731,
	5298,
	5298,
	5160,
	5067,
	4989,
	5396,
	3533,
	3533,
	3533,
	3478,
	2797,
	1582,
	961,
	3595,
	2856,
	2856,
	2856,
	2797,
	2797,
	1571,
	3595,
	2856,
	2856,
	2856,
	2856,
	2856,
	2856,
	2856,
	3595,
	3533,
	964,
	3595,
	3533,
	3533,
	608,
	2856,
	3595,
	1581,
	2856,
	830,
	2856,
	3595,
	3595,
	2856,
	3595,
	1581,
	2856,
	830,
	2856,
	3533,
	2856,
	3518,
	3563,
	3533,
	2856,
	3533,
	2856,
	3518,
	2842,
	2856,
	3595,
	3595,
	3595,
	2856,
	3595,
	3595,
	3595,
	2797,
	1457,
	2842,
	1457,
	1457,
	3595,
	2907,
	3518,
	3595,
	3533,
	2856,
	3518,
	2842,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3533,
	2856,
	3533,
	2856,
	3533,
	2856,
	3533,
	2856,
	3533,
	2856,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3533,
	2856,
	3482,
	2801,
	3482,
	2801,
	3533,
	2856,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3533,
	2856,
	3533,
	2856,
	3533,
	2856,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3533,
	2856,
	3533,
	2856,
	3482,
	2801,
	3482,
	2801,
	3563,
	2883,
	3563,
	2883,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3533,
	2856,
	3533,
	2856,
	3533,
	2856,
	3533,
	2856,
	3478,
	3533,
	2856,
	3533,
	2856,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3482,
	2801,
	3533,
	2856,
	1573,
	2856,
	2856,
	2856,
	2856,
	2856,
	1244,
	3595,
	3595,
};
extern const CustomAttributesCacheGenerator g_SimpleFileBrowser_Runtime_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_SimpleFileBrowser_Runtime_CodeGenModule;
const Il2CppCodeGenModule g_SimpleFileBrowser_Runtime_CodeGenModule = 
{
	"SimpleFileBrowser.Runtime.dll",
	457,
	s_methodPointers,
	3,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_SimpleFileBrowser_Runtime_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

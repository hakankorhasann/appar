﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Single VFX.CameraUtil::GetHorizontalFovRadians(UnityEngine.Camera)
extern void CameraUtil_GetHorizontalFovRadians_mBB247BF39E4B829C1001CCD9E23B8CC064ED8051 (void);
// 0x00000002 System.Single VFX.CameraUtil::GetVerticalFovRadians(UnityEngine.Camera)
extern void CameraUtil_GetVerticalFovRadians_m066B328721CDB8817AC0B53882464E6496E5EBC3 (void);
// 0x00000003 System.Void VFX.MaterialUtil::ApplyMaterial(UnityEngine.GameObject,UnityEngine.Material,System.Boolean)
extern void MaterialUtil_ApplyMaterial_m09553DE4758E5FDF7C4CA15CC4B683BB7C91950F (void);
// 0x00000004 System.Void VFX.MaterialUtil::ApplyMaterial(UnityEngine.Renderer,UnityEngine.Material)
extern void MaterialUtil_ApplyMaterial_m7F584F364957D55045AFA76728571AB3505145C8 (void);
// 0x00000005 System.Void VFX.MaterialUtil::ApplyAlpha(UnityEngine.GameObject,System.Single,System.Boolean)
extern void MaterialUtil_ApplyAlpha_m8401B26706FC2BD33CE040E9CAA529EEF07F49A0 (void);
// 0x00000006 System.Void VFX.MaterialUtil::ApplyAlpha(UnityEngine.Renderer,System.Single)
extern void MaterialUtil_ApplyAlpha_mD3ACDC51BFCF87225F3057850F65E0058434C2C7 (void);
// 0x00000007 System.Void VFX.MaterialUtil::TrySetVector4Property(UnityEngine.Material,System.String,UnityEngine.Vector4)
extern void MaterialUtil_TrySetVector4Property_mC732632438DE44228E777EDDCF56A6225BF83E5E (void);
// 0x00000008 System.Void VFX.MaterialUtil::TrySetFloatProperty(UnityEngine.Material,System.String,System.Single)
extern void MaterialUtil_TrySetFloatProperty_m76345DCF5F6A7994993905DB27CC50C2E016F002 (void);
// 0x00000009 System.Void VFX.MaterialUtil::TrySetColorProperty(UnityEngine.Material,System.String,UnityEngine.Color)
extern void MaterialUtil_TrySetColorProperty_mB5C7CF731E5CCB5B0ED9F7843252F219E6CD5987 (void);
// 0x0000000A UnityEngine.Matrix4x4 VFX.MatrixUtil::MatrixFromArray(System.Single[])
extern void MatrixUtil_MatrixFromArray_m1587EE0321B5B85419EAC14BC9E0F3D2392BA3C7 (void);
// 0x0000000B UnityEngine.Matrix4x4 VFX.MatrixUtil::GetFromToMatrix(UnityEngine.Transform,UnityEngine.Transform)
extern void MatrixUtil_GetFromToMatrix_m111F16EEBA3D4674F5FFB018AB5503C5EA6D1CC5 (void);
// 0x0000000C UnityEngine.Quaternion VFX.MatrixUtil::GetRotation(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetRotation_mAE0E1948878FD6D18B093CC9BCBEEAD86E9AA90B (void);
// 0x0000000D UnityEngine.Vector3 VFX.MatrixUtil::GetPosition(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetPosition_mC006816FFC13863556103F7F0B2D6874CD353699 (void);
// 0x0000000E UnityEngine.Vector3 VFX.MatrixUtil::GetScale(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetScale_mC5CD813EB343F0341B45684B2D022F5D69EDC4ED (void);
// 0x0000000F System.Single VFX.MatrixUtil::GetHorizontalFovRadians(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetHorizontalFovRadians_mB94FA785BB5274A0BAC3B83E38D7747978DA7503 (void);
// 0x00000010 System.Single VFX.MatrixUtil::GetHorizontalFovDegrees(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetHorizontalFovDegrees_m21CFA27957BF8B4AE116EEF605E8831213B6A8E6 (void);
// 0x00000011 System.Single VFX.MatrixUtil::GetVerticalFovRadians(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetVerticalFovRadians_mB065E70D310293789514117A8C92991388746FB6 (void);
// 0x00000012 System.Single VFX.MatrixUtil::GetVerticalFovDegrees(UnityEngine.Matrix4x4)
extern void MatrixUtil_GetVerticalFovDegrees_m05A9A792AA97FAB939C2E041A01F2EF92ABF1D46 (void);
// 0x00000013 System.Void VFX.CameraDepthMode::Awake()
extern void CameraDepthMode_Awake_m7B67FA5BC9A1E327E449AB1359F522DAA387C6C9 (void);
// 0x00000014 System.Void VFX.CameraDepthMode::.ctor()
extern void CameraDepthMode__ctor_m5C3CEA20DC855AC4E636F238232B059BED228C0F (void);
// 0x00000015 System.Void VFX.ShaderVFX::Awake()
extern void ShaderVFX_Awake_mD83D6074B8CFFA04541E28C315A5B12AB14C296B (void);
// 0x00000016 System.Void VFX.ShaderVFX::Update()
extern void ShaderVFX_Update_m2BB3D8C9244E0D393D9CCD1881C0A26C91E0C0A3 (void);
// 0x00000017 System.Void VFX.ShaderVFX::Play()
extern void ShaderVFX_Play_m5BD3ED6340C3C64994820D836D87F61934FF4A3F (void);
// 0x00000018 System.Void VFX.ShaderVFX::Pause()
extern void ShaderVFX_Pause_m987A8792AA017C49091CC5E18337B1A5990988B1 (void);
// 0x00000019 System.Void VFX.ShaderVFX::Rewind()
extern void ShaderVFX_Rewind_mFB23F45D8074CE414756F54B8B7D3E1F7F1CB331 (void);
// 0x0000001A System.Void VFX.ShaderVFX::RewindAndPlay()
extern void ShaderVFX_RewindAndPlay_m8C10971BD3BFA38232BAD3E0E780057EF1B65E2A (void);
// 0x0000001B System.Void VFX.ShaderVFX::Stop()
extern void ShaderVFX_Stop_mEFF4CD09413F3716AB686ABD4B58A5E06D3F84F8 (void);
// 0x0000001C System.Void VFX.ShaderVFX::UpdateDynamicColors(System.Single&)
extern void ShaderVFX_UpdateDynamicColors_m81783FCD5ADBAD56E8BDFE7B58535D96847D878B (void);
// 0x0000001D System.Void VFX.ShaderVFX::UpdateDynamicVectorProperties(System.Single&)
extern void ShaderVFX_UpdateDynamicVectorProperties_m2E2AFA5B018D8DB65EFFD62D62BCB5B2E71D5B4D (void);
// 0x0000001E System.Void VFX.ShaderVFX::UpdateDynamicScalarProperties(System.Single&)
extern void ShaderVFX_UpdateDynamicScalarProperties_m0813951738C8764B796FBE0B7B58703542CE6AD7 (void);
// 0x0000001F System.Single VFX.ShaderVFX::SetMaxTimeAndGetLerpValue(System.Single&,System.Single,System.Single)
extern void ShaderVFX_SetMaxTimeAndGetLerpValue_m08880DC506FDE352596973F76EF0C1D6ED6EC8D6 (void);
// 0x00000020 System.Void VFX.ShaderVFX::InitMaterial()
extern void ShaderVFX_InitMaterial_m171D68FD13B4E034564394A02173E755E2E7B559 (void);
// 0x00000021 System.Void VFX.ShaderVFX::InitStaticProperties()
extern void ShaderVFX_InitStaticProperties_m1F784764030AD2A80A1998B080CAADA2C56E462B (void);
// 0x00000022 System.Void VFX.ShaderVFX::InitDynamicProperties()
extern void ShaderVFX_InitDynamicProperties_mD15D937FE9B8C7D016A24105DC5F64861DF799B4 (void);
// 0x00000023 System.Void VFX.ShaderVFX::.ctor()
extern void ShaderVFX__ctor_m73626EBBF876BB3A3B3221510E95C9E0104BDEEA (void);
// 0x00000024 System.Void VFX.TargetVFX::Awake()
extern void TargetVFX_Awake_m580F062B1EADE7DF3E2CFAB756E25956966B5EBD (void);
// 0x00000025 System.Void VFX.TargetVFX::Update()
extern void TargetVFX_Update_m0A3F95D97C081A5377D710571E2C42CC76621489 (void);
// 0x00000026 System.Void VFX.TargetVFX::OnDestroy()
extern void TargetVFX_OnDestroy_mC47B16AB90F747409D3E28C743502D2240E6CC14 (void);
// 0x00000027 System.Void VFX.TargetVFX::UpdateShaderScale()
extern void TargetVFX_UpdateShaderScale_m604A6646A1A0B2AFDC6DAA4084C07E7C357DD3B9 (void);
// 0x00000028 System.Void VFX.TargetVFX::TargetStatusChanged(Vuforia.ObserverBehaviour,Vuforia.TargetStatus)
extern void TargetVFX_TargetStatusChanged_m30E23C81045B1AC3718F2C07E36F10DDE55CA89F (void);
// 0x00000029 System.Boolean VFX.TargetVFX::IsTracked(Vuforia.TargetStatus)
extern void TargetVFX_IsTracked_mC519C14E548F692D34342B9F418E437AE3742E9D (void);
// 0x0000002A System.Boolean VFX.TargetVFX::IsUnTracked(Vuforia.TargetStatus)
extern void TargetVFX_IsUnTracked_mAACE5B46B0917219F53B866DD82210C0C34C2347 (void);
// 0x0000002B System.Void VFX.TargetVFX::OnTargetFound()
extern void TargetVFX_OnTargetFound_m305B4A161DD078595633031B2167A5ABA02157F9 (void);
// 0x0000002C System.Void VFX.TargetVFX::UpdateShaderCenterAndAxis()
extern void TargetVFX_UpdateShaderCenterAndAxis_m6CA6C7B6F0EA1BD3117DFD06ED98217704C1964F (void);
// 0x0000002D System.Void VFX.TargetVFX::GetAxisVectors(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void TargetVFX_GetAxisVectors_mC102A9070CF86F33ECB02701F8137F391562F08B (void);
// 0x0000002E UnityEngine.Vector3 VFX.TargetVFX::GetCenterPointWCS()
extern void TargetVFX_GetCenterPointWCS_m54BB65B9127401B833732036B2E91E8FE5DE897B (void);
// 0x0000002F System.Void VFX.TargetVFX::.ctor()
extern void TargetVFX__ctor_mA31361704A336591D437608CA46129760F47FAE0 (void);
// 0x00000030 UnityEngine.Camera VFX.VuforiaCameraUtil::GetCamera()
extern void VuforiaCameraUtil_GetCamera_mDEE21FA25DA759EF6E3D259BF272139961BD5072 (void);
// 0x00000031 UnityEngine.Vector3 VFX.VuforiaObserverUtil::GetTargetSize(Vuforia.ObserverBehaviour)
extern void VuforiaObserverUtil_GetTargetSize_m5297CE9F787F2ABFE6FCA221F283E198FC11CB05 (void);
static Il2CppMethodPointer s_methodPointers[49] = 
{
	CameraUtil_GetHorizontalFovRadians_mBB247BF39E4B829C1001CCD9E23B8CC064ED8051,
	CameraUtil_GetVerticalFovRadians_m066B328721CDB8817AC0B53882464E6496E5EBC3,
	MaterialUtil_ApplyMaterial_m09553DE4758E5FDF7C4CA15CC4B683BB7C91950F,
	MaterialUtil_ApplyMaterial_m7F584F364957D55045AFA76728571AB3505145C8,
	MaterialUtil_ApplyAlpha_m8401B26706FC2BD33CE040E9CAA529EEF07F49A0,
	MaterialUtil_ApplyAlpha_mD3ACDC51BFCF87225F3057850F65E0058434C2C7,
	MaterialUtil_TrySetVector4Property_mC732632438DE44228E777EDDCF56A6225BF83E5E,
	MaterialUtil_TrySetFloatProperty_m76345DCF5F6A7994993905DB27CC50C2E016F002,
	MaterialUtil_TrySetColorProperty_mB5C7CF731E5CCB5B0ED9F7843252F219E6CD5987,
	MatrixUtil_MatrixFromArray_m1587EE0321B5B85419EAC14BC9E0F3D2392BA3C7,
	MatrixUtil_GetFromToMatrix_m111F16EEBA3D4674F5FFB018AB5503C5EA6D1CC5,
	MatrixUtil_GetRotation_mAE0E1948878FD6D18B093CC9BCBEEAD86E9AA90B,
	MatrixUtil_GetPosition_mC006816FFC13863556103F7F0B2D6874CD353699,
	MatrixUtil_GetScale_mC5CD813EB343F0341B45684B2D022F5D69EDC4ED,
	MatrixUtil_GetHorizontalFovRadians_mB94FA785BB5274A0BAC3B83E38D7747978DA7503,
	MatrixUtil_GetHorizontalFovDegrees_m21CFA27957BF8B4AE116EEF605E8831213B6A8E6,
	MatrixUtil_GetVerticalFovRadians_mB065E70D310293789514117A8C92991388746FB6,
	MatrixUtil_GetVerticalFovDegrees_m05A9A792AA97FAB939C2E041A01F2EF92ABF1D46,
	CameraDepthMode_Awake_m7B67FA5BC9A1E327E449AB1359F522DAA387C6C9,
	CameraDepthMode__ctor_m5C3CEA20DC855AC4E636F238232B059BED228C0F,
	ShaderVFX_Awake_mD83D6074B8CFFA04541E28C315A5B12AB14C296B,
	ShaderVFX_Update_m2BB3D8C9244E0D393D9CCD1881C0A26C91E0C0A3,
	ShaderVFX_Play_m5BD3ED6340C3C64994820D836D87F61934FF4A3F,
	ShaderVFX_Pause_m987A8792AA017C49091CC5E18337B1A5990988B1,
	ShaderVFX_Rewind_mFB23F45D8074CE414756F54B8B7D3E1F7F1CB331,
	ShaderVFX_RewindAndPlay_m8C10971BD3BFA38232BAD3E0E780057EF1B65E2A,
	ShaderVFX_Stop_mEFF4CD09413F3716AB686ABD4B58A5E06D3F84F8,
	ShaderVFX_UpdateDynamicColors_m81783FCD5ADBAD56E8BDFE7B58535D96847D878B,
	ShaderVFX_UpdateDynamicVectorProperties_m2E2AFA5B018D8DB65EFFD62D62BCB5B2E71D5B4D,
	ShaderVFX_UpdateDynamicScalarProperties_m0813951738C8764B796FBE0B7B58703542CE6AD7,
	ShaderVFX_SetMaxTimeAndGetLerpValue_m08880DC506FDE352596973F76EF0C1D6ED6EC8D6,
	ShaderVFX_InitMaterial_m171D68FD13B4E034564394A02173E755E2E7B559,
	ShaderVFX_InitStaticProperties_m1F784764030AD2A80A1998B080CAADA2C56E462B,
	ShaderVFX_InitDynamicProperties_mD15D937FE9B8C7D016A24105DC5F64861DF799B4,
	ShaderVFX__ctor_m73626EBBF876BB3A3B3221510E95C9E0104BDEEA,
	TargetVFX_Awake_m580F062B1EADE7DF3E2CFAB756E25956966B5EBD,
	TargetVFX_Update_m0A3F95D97C081A5377D710571E2C42CC76621489,
	TargetVFX_OnDestroy_mC47B16AB90F747409D3E28C743502D2240E6CC14,
	TargetVFX_UpdateShaderScale_m604A6646A1A0B2AFDC6DAA4084C07E7C357DD3B9,
	TargetVFX_TargetStatusChanged_m30E23C81045B1AC3718F2C07E36F10DDE55CA89F,
	TargetVFX_IsTracked_mC519C14E548F692D34342B9F418E437AE3742E9D,
	TargetVFX_IsUnTracked_mAACE5B46B0917219F53B866DD82210C0C34C2347,
	TargetVFX_OnTargetFound_m305B4A161DD078595633031B2167A5ABA02157F9,
	TargetVFX_UpdateShaderCenterAndAxis_m6CA6C7B6F0EA1BD3117DFD06ED98217704C1964F,
	TargetVFX_GetAxisVectors_mC102A9070CF86F33ECB02701F8137F391562F08B,
	TargetVFX_GetCenterPointWCS_m54BB65B9127401B833732036B2E91E8FE5DE897B,
	TargetVFX__ctor_mA31361704A336591D437608CA46129760F47FAE0,
	VuforiaCameraUtil_GetCamera_mDEE21FA25DA759EF6E3D259BF272139961BD5072,
	VuforiaObserverUtil_GetTargetSize_m5297CE9F787F2ABFE6FCA221F283E198FC11CB05,
};
static const int32_t s_InvokerIndices[49] = 
{
	10553,
	10553,
	9228,
	9979,
	9243,
	9983,
	9240,
	9235,
	9229,
	10383,
	9627,
	10510,
	10653,
	10653,
	10552,
	10552,
	10552,
	10552,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	5638,
	5638,
	5638,
	1504,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	3219,
	4225,
	4225,
	7252,
	7252,
	1548,
	7241,
	7252,
	10847,
	10654,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_VFXCore_CodeGenModule;
const Il2CppCodeGenModule g_VFXCore_CodeGenModule = 
{
	"VFXCore.dll",
	49,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

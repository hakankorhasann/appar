﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Threading.Tasks.Task Firebase.Extensions.TaskExtension::ContinueWithOnMainThread(System.Threading.Tasks.Task`1<T>,System.Action`1<System.Threading.Tasks.Task`1<T>>)
// 0x00000002 System.Void Firebase.Extensions.TaskExtension/<>c__DisplayClass4_0`1::.ctor()
// 0x00000003 System.Threading.Tasks.Task`1<System.Boolean> Firebase.Extensions.TaskExtension/<>c__DisplayClass4_0`1::<ContinueWithOnMainThread>b__0(System.Threading.Tasks.Task`1<T>)
// 0x00000004 System.Void Firebase.Extensions.TaskExtension/<>c__DisplayClass4_1`1::.ctor()
// 0x00000005 System.Boolean Firebase.Extensions.TaskExtension/<>c__DisplayClass4_1`1::<ContinueWithOnMainThread>b__1()
static Il2CppMethodPointer s_methodPointers[5] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000003, { 7, 3 } },
	{ 0x02000004, { 10, 2 } },
	{ 0x06000001, { 0, 7 } },
};
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass4_0_1_tD8E49BAF21FCD27B948E4B9FCE8A28CAAC11F0C0;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass4_0_1__ctor_m41539FB26411045A0024D080A0C1F1F620F76E55;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass4_0_1_U3CContinueWithOnMainThreadU3Eb__0_m063EB1797F86A972EFF4861630FCDFA7F00CDC26;
extern const uint32_t g_rgctx_Func_2_t898DDB2CE8CDE6CCA806823CBF95B39B79503DB6;
extern const uint32_t g_rgctx_Func_2__ctor_m3DA61164A5A0B76DB7670F1E65E35CF502EFFA3E;
extern const uint32_t g_rgctx_Task_1_t69D08064C08DB6B9735597F7CC2B4087BB9A84BB;
extern const uint32_t g_rgctx_Task_1_ContinueWith_TisTask_1_t824317F4B958F7512E8F7300511752937A6C6043_m2120D3A321D229BFC86FE4BC143036C8DD8341DE;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass4_1_1_t66C25011EBC0FE816FCEEA3608CD760DCB971E29;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass4_1_1__ctor_m6490B3D3825DCB0B8918F81DAD575163F284AFFB;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass4_1_1_U3CContinueWithOnMainThreadU3Eb__1_m09666C36BA2B5E9E91E64A4BCBB184D38C8E3CA6;
extern const uint32_t g_rgctx_Action_1_t6376CBD339623DBD6CC40C9F00F365E6E59F80BB;
extern const uint32_t g_rgctx_Action_1_Invoke_m99EAA9D433A4A40937B103115C9BFB9F78EB7488;
static const Il2CppRGCTXDefinition s_rgctxValues[12] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass4_0_1_tD8E49BAF21FCD27B948E4B9FCE8A28CAAC11F0C0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass4_0_1__ctor_m41539FB26411045A0024D080A0C1F1F620F76E55 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass4_0_1_U3CContinueWithOnMainThreadU3Eb__0_m063EB1797F86A972EFF4861630FCDFA7F00CDC26 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t898DDB2CE8CDE6CCA806823CBF95B39B79503DB6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_m3DA61164A5A0B76DB7670F1E65E35CF502EFFA3E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Task_1_t69D08064C08DB6B9735597F7CC2B4087BB9A84BB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Task_1_ContinueWith_TisTask_1_t824317F4B958F7512E8F7300511752937A6C6043_m2120D3A321D229BFC86FE4BC143036C8DD8341DE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass4_1_1_t66C25011EBC0FE816FCEEA3608CD760DCB971E29 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass4_1_1__ctor_m6490B3D3825DCB0B8918F81DAD575163F284AFFB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass4_1_1_U3CContinueWithOnMainThreadU3Eb__1_m09666C36BA2B5E9E91E64A4BCBB184D38C8E3CA6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t6376CBD339623DBD6CC40C9F00F365E6E59F80BB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Action_1_Invoke_m99EAA9D433A4A40937B103115C9BFB9F78EB7488 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Firebase_TaskExtension_CodeGenModule;
const Il2CppCodeGenModule g_Firebase_TaskExtension_CodeGenModule = 
{
	"Firebase.TaskExtension.dll",
	5,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	12,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

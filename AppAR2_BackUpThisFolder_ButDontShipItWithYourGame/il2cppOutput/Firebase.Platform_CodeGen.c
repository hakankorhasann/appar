﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Collections.Generic.List`1<System.Exception> Firebase.ExceptionAggregator::get_Exceptions()
extern void ExceptionAggregator_get_Exceptions_m972559432E5D3893ED3CE759068B981B4F3860CC (void);
// 0x00000002 System.Exception Firebase.ExceptionAggregator::GetAndClearPendingExceptions()
extern void ExceptionAggregator_GetAndClearPendingExceptions_m7E2429EE2556341124AB3D82CB1725543365A28A (void);
// 0x00000003 System.Void Firebase.ExceptionAggregator::ThrowAndClearPendingExceptions()
extern void ExceptionAggregator_ThrowAndClearPendingExceptions_m5678256804A44705C715EC54446D8F973C4029A6 (void);
// 0x00000004 System.Exception Firebase.ExceptionAggregator::LogException(System.Exception)
extern void ExceptionAggregator_LogException_m3058016DA78DA3A3204AEA89C92BB0826C2F11F3 (void);
// 0x00000005 System.Void Firebase.ExceptionAggregator::Wrap(System.Action)
extern void ExceptionAggregator_Wrap_m0B206615C9C9608E5D2CC1738CB2056F121811FB (void);
// 0x00000006 System.Void Firebase.ExceptionAggregator::.cctor()
extern void ExceptionAggregator__cctor_m0A8976163955C4D2D501402578DF21C1171A15F4 (void);
// 0x00000007 System.Void Firebase.Dispatcher::.ctor()
extern void Dispatcher__ctor_m20D7687CB59CF114ACF69131CD3BA289B80A8B2A (void);
// 0x00000008 TResult Firebase.Dispatcher::Run(System.Func`1<TResult>)
// 0x00000009 System.Threading.Tasks.Task`1<TResult> Firebase.Dispatcher::RunAsync(System.Func`1<TResult>)
// 0x0000000A System.Threading.Tasks.Task`1<TResult> Firebase.Dispatcher::RunAsyncNow(System.Func`1<TResult>)
// 0x0000000B System.Boolean Firebase.Dispatcher::ManagesThisThread()
extern void Dispatcher_ManagesThisThread_mF00448368389637B00847E99D9DA66DEF82E118D (void);
// 0x0000000C System.Void Firebase.Dispatcher::PollJobs()
extern void Dispatcher_PollJobs_m90DF848D76273E55DB08A80E9C246E9E92E931CE (void);
// 0x0000000D TResult Firebase.Dispatcher/CallbackStorage`1::get_Result()
// 0x0000000E System.Void Firebase.Dispatcher/CallbackStorage`1::set_Result(TResult)
// 0x0000000F System.Exception Firebase.Dispatcher/CallbackStorage`1::get_Exception()
// 0x00000010 System.Void Firebase.Dispatcher/CallbackStorage`1::set_Exception(System.Exception)
// 0x00000011 System.Void Firebase.Dispatcher/CallbackStorage`1::.ctor()
// 0x00000012 System.Void Firebase.Dispatcher/<>c__DisplayClass4_0`1::.ctor()
// 0x00000013 System.Void Firebase.Dispatcher/<>c__DisplayClass4_0`1::<Run>b__0()
// 0x00000014 System.Void Firebase.Dispatcher/<>c__DisplayClass5_0`1::.ctor()
// 0x00000015 System.Void Firebase.Dispatcher/<>c__DisplayClass5_0`1::<RunAsync>b__0()
// 0x00000016 Firebase.Unity.UnityLoggingService Firebase.Unity.UnityLoggingService::get_Instance()
extern void UnityLoggingService_get_Instance_mD7B53127EFA0E161C976160DACDF8C7E90B20E87 (void);
// 0x00000017 System.Void Firebase.Unity.UnityLoggingService::.ctor()
extern void UnityLoggingService__ctor_mCD777147C85E451F625C68F6D3B5713E7FB00D31 (void);
// 0x00000018 System.Void Firebase.Unity.UnityLoggingService::.cctor()
extern void UnityLoggingService__cctor_m83A5B3C1C5E217321C63C3704CD9920D029A955C (void);
// 0x00000019 System.Void Firebase.Unity.UnityPlatformServices::SetupServices()
extern void UnityPlatformServices_SetupServices_m8BC0FADC50ED15B12820FA8AD44AD7599728DAB4 (void);
// 0x0000001A System.Void Firebase.Unity.UnitySynchronizationContext::.ctor(UnityEngine.GameObject)
extern void UnitySynchronizationContext__ctor_m02C29AFB8681306BC3D3761C68B805F815C169C9 (void);
// 0x0000001B System.Void Firebase.Unity.UnitySynchronizationContext::Create(UnityEngine.GameObject)
extern void UnitySynchronizationContext_Create_m038F4C4C5A084C760A9340693B303F211FB3F3E4 (void);
// 0x0000001C System.Void Firebase.Unity.UnitySynchronizationContext::Destroy()
extern void UnitySynchronizationContext_Destroy_m9D39F827AC31DC811259997B10C13894D716C086 (void);
// 0x0000001D System.Threading.ManualResetEvent Firebase.Unity.UnitySynchronizationContext::GetThreadEvent()
extern void UnitySynchronizationContext_GetThreadEvent_m427C8A60FC426FB828BFE2FD8B3F4099CCDC25D0 (void);
// 0x0000001E System.Void Firebase.Unity.UnitySynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Post_m27CEEE71D49CB394434E47807272ADBB1B00B7A9 (void);
// 0x0000001F System.Void Firebase.Unity.UnitySynchronizationContext::Send(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Send_m6287E48D5488043652D3E93E610AE279CD616D00 (void);
// 0x00000020 System.Void Firebase.Unity.UnitySynchronizationContext::.cctor()
extern void UnitySynchronizationContext__cctor_m34420B5BE964BF2E7B480A23AD56847562DF8282 (void);
// 0x00000021 System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>> Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir::get_CallbackQueue()
extern void SynchronizationContextBehavoir_get_CallbackQueue_m291C810E98C2BF128991603294E306DF9C54836E (void);
// 0x00000022 System.Collections.IEnumerator Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir::Start()
extern void SynchronizationContextBehavoir_Start_m138FD9256FCC72808F0F211B4578B4F1B400A507 (void);
// 0x00000023 System.Void Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir::.ctor()
extern void SynchronizationContextBehavoir__ctor_m535727BFD0F4884E295BF1DD883A4A47DB5CA226 (void);
// 0x00000024 System.Void Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>d__3::.ctor(System.Int32)
extern void U3CStartU3Ed__3__ctor_m35E3CB0C51C8D66808E279EC08986685FB35F0B4 (void);
// 0x00000025 System.Void Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>d__3::System.IDisposable.Dispose()
extern void U3CStartU3Ed__3_System_IDisposable_Dispose_m6BFEE452494CADFC903A5DE0A0391FE66BE0732B (void);
// 0x00000026 System.Boolean Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>d__3::MoveNext()
extern void U3CStartU3Ed__3_MoveNext_mAFA0389B4DED6BFC59975113737EE832A6051ABA (void);
// 0x00000027 System.Object Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7D5524D4D60AEB5589BBE0C16EEB6B4FCE158B5 (void);
// 0x00000028 System.Void Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>d__3::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__3_System_Collections_IEnumerator_Reset_m85F03652CF544E4A9AE0E1CFCB02377AE7354FC5 (void);
// 0x00000029 System.Object Firebase.Unity.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__3_System_Collections_IEnumerator_get_Current_m6A22713E2B13BDEFA00F6E7D7258603D92C9ABAD (void);
// 0x0000002A System.Void Firebase.Unity.UnitySynchronizationContext/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m71F85406259CC9E07BABFC4C9C3534B60D1188B4 (void);
// 0x0000002B System.Void Firebase.Unity.UnitySynchronizationContext/<>c__DisplayClass14_1::.ctor()
extern void U3CU3Ec__DisplayClass14_1__ctor_mA297006BB71E8DC6C8F9544843121E2BB0E191FA (void);
// 0x0000002C System.Void Firebase.Unity.UnitySynchronizationContext/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mE5A94FA226B6D545714950BAC1684E5B99E9BA9B (void);
// 0x0000002D System.Void Firebase.Unity.UnitySynchronizationContext/<>c__DisplayClass16_1::.ctor()
extern void U3CU3Ec__DisplayClass16_1__ctor_m614CECB53A481A4CAE1FF29E4311490C145185DC (void);
// 0x0000002E System.Void Firebase.Unity.UnitySynchronizationContext/<>c__DisplayClass16_1::<Send>b__0(System.Object)
extern void U3CU3Ec__DisplayClass16_1_U3CSendU3Eb__0_mE2861360FB9F457F08CCB9240711D8A785BEAD09 (void);
// 0x0000002F Firebase.Platform.DebugLogger Firebase.Platform.DebugLogger::get_Instance()
extern void DebugLogger_get_Instance_mA20E9232A5C60CAA50C641FAE996FA62A5D82A33 (void);
// 0x00000030 System.Void Firebase.Platform.DebugLogger::.ctor()
extern void DebugLogger__ctor_m975238F7F458467754AB4691AA1175EBB3BE7C9B (void);
// 0x00000031 System.Void Firebase.Platform.DebugLogger::.cctor()
extern void DebugLogger__cctor_mF8D5A3D179D533E6CD6FA37D85BEF242D3B8AFE3 (void);
// 0x00000032 System.Void Firebase.Platform.Services::.cctor()
extern void Services__cctor_m4036DF2DCFD0D7BCDA089BEE0A8B31B6EFB2D00D (void);
// 0x00000033 System.Void Firebase.Platform.Services::set_AppConfig(Firebase.Platform.IAppConfigExtensions)
extern void Services_set_AppConfig_m3A9EF6EEAE013442C30B8CC432ED28B5FD4D9F2C (void);
// 0x00000034 System.Void Firebase.Platform.Services::set_Clock(Firebase.Platform.IClockService)
extern void Services_set_Clock_m6EA7AA445A3CCC399FA5BB9DFD5AF4E6DECACE88 (void);
// 0x00000035 System.Void Firebase.Platform.Services::set_Logging(Firebase.Platform.ILoggingService)
extern void Services_set_Logging_m0406136BE33759DC9EAC480E0FE232690A86A16A (void);
// 0x00000036 System.Void Firebase.Platform.IFirebaseAppUtils::TranslateDllNotFoundException(System.Action)
// 0x00000037 System.Void Firebase.Platform.IFirebaseAppUtils::PollCallbacks()
// 0x00000038 Firebase.Platform.PlatformLogLevel Firebase.Platform.IFirebaseAppUtils::GetLogLevel()
// 0x00000039 Firebase.Platform.FirebaseAppUtilsStub Firebase.Platform.FirebaseAppUtilsStub::get_Instance()
extern void FirebaseAppUtilsStub_get_Instance_m361D43B0066F8C3067E898965964328EC136E74A (void);
// 0x0000003A System.Void Firebase.Platform.FirebaseAppUtilsStub::TranslateDllNotFoundException(System.Action)
extern void FirebaseAppUtilsStub_TranslateDllNotFoundException_mDDDE0AFBE2714E5A360C9C26184C46E0FD6169E7 (void);
// 0x0000003B System.Void Firebase.Platform.FirebaseAppUtilsStub::PollCallbacks()
extern void FirebaseAppUtilsStub_PollCallbacks_mB4F4C4556E7FE81BCAFAB9D06A2AF268E62F9463 (void);
// 0x0000003C Firebase.Platform.PlatformLogLevel Firebase.Platform.FirebaseAppUtilsStub::GetLogLevel()
extern void FirebaseAppUtilsStub_GetLogLevel_mE32FBF80367DE28198974386C8E0F17CFD2B925F (void);
// 0x0000003D System.Void Firebase.Platform.FirebaseAppUtilsStub::.ctor()
extern void FirebaseAppUtilsStub__ctor_m26C823CCA8BC46F64579375E15FA2B1766402132 (void);
// 0x0000003E System.Void Firebase.Platform.FirebaseAppUtilsStub::.cctor()
extern void FirebaseAppUtilsStub__cctor_m308390A5EC41ED3A7FB17E84C7F9A8EA00F8C2FE (void);
// 0x0000003F System.Void Firebase.Platform.MainThreadProperty`1::.ctor(System.Func`1<T>)
// 0x00000040 T Firebase.Platform.MainThreadProperty`1::get_Value()
// 0x00000041 T Firebase.Platform.MainThreadProperty`1::<get_Value>b__5_0()
// 0x00000042 Firebase.Platform.IFirebaseAppUtils Firebase.Platform.FirebaseHandler::get_AppUtils()
extern void FirebaseHandler_get_AppUtils_mD317C324F3E2F6D3260E1CEAA946847BAB91BC2C (void);
// 0x00000043 System.Void Firebase.Platform.FirebaseHandler::set_AppUtils(Firebase.Platform.IFirebaseAppUtils)
extern void FirebaseHandler_set_AppUtils_m3CF4CEF8A89CB92676CDEAC04E59850DAFB651BA (void);
// 0x00000044 System.Int32 Firebase.Platform.FirebaseHandler::get_TickCount()
extern void FirebaseHandler_get_TickCount_mA791188CABE0F2464FAB7E76709CB74631D4BCD1 (void);
// 0x00000045 Firebase.Dispatcher Firebase.Platform.FirebaseHandler::get_ThreadDispatcher()
extern void FirebaseHandler_get_ThreadDispatcher_mF4FACC8630771208FCF0A6E4ADBD9DE2B92623E3 (void);
// 0x00000046 System.Void Firebase.Platform.FirebaseHandler::set_ThreadDispatcher(Firebase.Dispatcher)
extern void FirebaseHandler_set_ThreadDispatcher_m2AE8411CCA401E1683B93B40D4FDCC29DF953F5A (void);
// 0x00000047 System.Boolean Firebase.Platform.FirebaseHandler::get_IsPlayMode()
extern void FirebaseHandler_get_IsPlayMode_m9B9EF275DDA78C0897805EEF2CF096070AA77D96 (void);
// 0x00000048 System.Void Firebase.Platform.FirebaseHandler::set_IsPlayMode(System.Boolean)
extern void FirebaseHandler_set_IsPlayMode_mD40E7856155867FC95E46FA9E1985EA7FA47ED29 (void);
// 0x00000049 System.Void Firebase.Platform.FirebaseHandler::.cctor()
extern void FirebaseHandler__cctor_mBB75468D5FE6DFD867E6E5043D863E7664EAC730 (void);
// 0x0000004A System.Void Firebase.Platform.FirebaseHandler::.ctor()
extern void FirebaseHandler__ctor_m528A373696F4853B62E70CA8C61E74DD582E3442 (void);
// 0x0000004B System.Void Firebase.Platform.FirebaseHandler::StartMonoBehaviour()
extern void FirebaseHandler_StartMonoBehaviour_m27FB410A511845F73EE29218BDEA9CEEAB1D5246 (void);
// 0x0000004C System.Void Firebase.Platform.FirebaseHandler::StopMonoBehaviour()
extern void FirebaseHandler_StopMonoBehaviour_m6D5F532432F18CE6837F27A0FA9D586F55947451 (void);
// 0x0000004D TResult Firebase.Platform.FirebaseHandler::RunOnMainThread(System.Func`1<TResult>)
// 0x0000004E System.Threading.Tasks.Task`1<TResult> Firebase.Platform.FirebaseHandler::RunOnMainThreadAsync(System.Func`1<TResult>)
// 0x0000004F Firebase.Platform.FirebaseHandler Firebase.Platform.FirebaseHandler::get_DefaultInstance()
extern void FirebaseHandler_get_DefaultInstance_mAE0FC8CDB594816EE0DC5A552501A20500655F1E (void);
// 0x00000050 System.Void Firebase.Platform.FirebaseHandler::CreatePartialOnMainThread(Firebase.Platform.IFirebaseAppUtils)
extern void FirebaseHandler_CreatePartialOnMainThread_mCB214362F224CAD869C9563A1AD92CFBC3B4AF6F (void);
// 0x00000051 System.Void Firebase.Platform.FirebaseHandler::Create(Firebase.Platform.IFirebaseAppUtils)
extern void FirebaseHandler_Create_m551C916AE81A5ACBFF2119B36A9BD405FDEC1588 (void);
// 0x00000052 System.Void Firebase.Platform.FirebaseHandler::Update()
extern void FirebaseHandler_Update_mDBFD412D735E6C3F328C9BA3E5B3C5ECFBF49138 (void);
// 0x00000053 System.Void Firebase.Platform.FirebaseHandler::OnApplicationFocus(System.Boolean)
extern void FirebaseHandler_OnApplicationFocus_mEDF45D3543635BFE0D26562C1AF3A7F8005D5054 (void);
// 0x00000054 System.Void Firebase.Platform.FirebaseHandler::OnMonoBehaviourDestroyed(Firebase.Platform.FirebaseMonoBehaviour)
extern void FirebaseHandler_OnMonoBehaviourDestroyed_m5AB2EEE4B6515BFB6B75E7E7305B1519883AB519 (void);
// 0x00000055 System.Void Firebase.Platform.FirebaseHandler::<Update>b__36_0()
extern void FirebaseHandler_U3CUpdateU3Eb__36_0_mF9F0EE2B1EA48002FA4AAF4E90A45F0CD28183BE (void);
// 0x00000056 System.Void Firebase.Platform.FirebaseHandler/ApplicationFocusChangedEventArgs::set_HasFocus(System.Boolean)
extern void ApplicationFocusChangedEventArgs_set_HasFocus_m144DF1C01B04C22AC4C98A06E2FE88E89AA9C107 (void);
// 0x00000057 System.Void Firebase.Platform.FirebaseHandler/ApplicationFocusChangedEventArgs::.ctor()
extern void ApplicationFocusChangedEventArgs__ctor_m839E74DC0C6B72AB8A768A2D23F32ECE4A3B39AE (void);
// 0x00000058 System.Void Firebase.Platform.FirebaseHandler/<>c::.cctor()
extern void U3CU3Ec__cctor_m260427B8C39C75CB97941760EE64E9067346D45F (void);
// 0x00000059 System.Void Firebase.Platform.FirebaseHandler/<>c::.ctor()
extern void U3CU3Ec__ctor_mD106D0E3573CF90E581E70D2894A38124C3755BA (void);
// 0x0000005A System.Boolean Firebase.Platform.FirebaseHandler/<>c::<StopMonoBehaviour>b__19_0()
extern void U3CU3Ec_U3CStopMonoBehaviourU3Eb__19_0_mB990EC27D5AB7A305AAA79490EE3DECA513B2507 (void);
// 0x0000005B System.Void Firebase.Platform.FirebaseHandler/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mF57098ECC5E1BDC40DC0479FFFF8462C4286FF0D (void);
// 0x0000005C System.Void Firebase.Platform.FirebaseHandler/<>c__DisplayClass34_0::<CreatePartialOnMainThread>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CCreatePartialOnMainThreadU3Eb__0_mF30FC5B54AA56E8760A72FCF6F7B4E228921A701 (void);
// 0x0000005D System.Boolean Firebase.Platform.PlatformInformation::get_IsAndroid()
extern void PlatformInformation_get_IsAndroid_m8AB95334299A6FA8FEBFD7B0A6BFE9B45CB34474 (void);
// 0x0000005E System.Boolean Firebase.Platform.PlatformInformation::get_IsIOS()
extern void PlatformInformation_get_IsIOS_mFD5800DAFA3923CEEEFEA6BA8E07FF6691EFB677 (void);
// 0x0000005F System.String Firebase.Platform.PlatformInformation::get_DefaultConfigLocation()
extern void PlatformInformation_get_DefaultConfigLocation_mAEB7514B85C90CB6B9C8DEA4B2B27B0D309A8166 (void);
// 0x00000060 System.Single Firebase.Platform.PlatformInformation::get_RealtimeSinceStartup()
extern void PlatformInformation_get_RealtimeSinceStartup_m954BB1B40808B6F1E4E432FC1A9A930017FD41E1 (void);
// 0x00000061 System.Void Firebase.Platform.PlatformInformation::set_RealtimeSinceStartupSafe(System.Single)
extern void PlatformInformation_set_RealtimeSinceStartupSafe_mBA2F7097A44D7F1DF964ABCC2B23C0A951919941 (void);
// 0x00000062 System.String Firebase.Platform.PlatformInformation::get_RuntimeName()
extern void PlatformInformation_get_RuntimeName_m014CA17680A844F0F26164D3F01C4722182C5A90 (void);
// 0x00000063 System.String Firebase.Platform.PlatformInformation::get_RuntimeVersion()
extern void PlatformInformation_get_RuntimeVersion_mD482340448C87BCF8CC6D361C0309028E808DE39 (void);
// 0x00000064 System.Void Firebase.Platform.PlatformInformation/<>c::.cctor()
extern void U3CU3Ec__cctor_m1576EBA22A4F2E2E581A4A13FF91180980120324 (void);
// 0x00000065 System.Void Firebase.Platform.PlatformInformation/<>c::.ctor()
extern void U3CU3Ec__ctor_m6741C1EC646C313F640C936AFA67DBB23B75A397 (void);
// 0x00000066 System.String Firebase.Platform.PlatformInformation/<>c::<get_DefaultConfigLocation>b__6_0()
extern void U3CU3Ec_U3Cget_DefaultConfigLocationU3Eb__6_0_m3AB345EB876894BBEF4EA889DB7C050149A84ED5 (void);
// 0x00000067 System.String Firebase.Platform.PlatformInformation/<>c::<get_RuntimeVersion>b__18_0()
extern void U3CU3Ec_U3Cget_RuntimeVersionU3Eb__18_0_m534FD38D257EAEFB9FB15D828D2177701A9FC47F (void);
// 0x00000068 System.Boolean Firebase.Platform.FirebaseLogger::IsStackTraceLogTypeIncompatibleWithNativeLogs(UnityEngine.StackTraceLogType)
extern void FirebaseLogger_IsStackTraceLogTypeIncompatibleWithNativeLogs_m017E8BD41D237084924DDCF88CB2C6ABABF237DB (void);
// 0x00000069 System.Boolean Firebase.Platform.FirebaseLogger::CurrentStackTraceLogTypeIsIncompatibleWithNativeLogs()
extern void FirebaseLogger_CurrentStackTraceLogTypeIsIncompatibleWithNativeLogs_mC97D0928657EF657E397290DA97756027989B96E (void);
// 0x0000006A System.Boolean Firebase.Platform.FirebaseLogger::get_CanRedirectNativeLogs()
extern void FirebaseLogger_get_CanRedirectNativeLogs_mEBA68986F4E0627B01B89A8F793B48E2224139D3 (void);
// 0x0000006B System.Void Firebase.Platform.FirebaseLogger::LogMessage(Firebase.Platform.PlatformLogLevel,System.String)
extern void FirebaseLogger_LogMessage_mD75A87D37AD77AC7BF0463C48DDD46E7901B0106 (void);
// 0x0000006C System.Void Firebase.Platform.FirebaseLogger::.cctor()
extern void FirebaseLogger__cctor_m801A17DE81304E6096207E1925500D14421ECE95 (void);
// 0x0000006D Firebase.Platform.FirebaseHandler Firebase.Platform.FirebaseMonoBehaviour::GetFirebaseHandlerOrDestroyGameObject()
extern void FirebaseMonoBehaviour_GetFirebaseHandlerOrDestroyGameObject_m45E0E65DFD7138558207655AC144AE884F578967 (void);
// 0x0000006E System.Void Firebase.Platform.FirebaseMonoBehaviour::OnEnable()
extern void FirebaseMonoBehaviour_OnEnable_mF2CFA86462A454B9C0206AAC3ACEF950FF3E3631 (void);
// 0x0000006F System.Void Firebase.Platform.FirebaseMonoBehaviour::Update()
extern void FirebaseMonoBehaviour_Update_m7DEE72CA7C251D41465F5997BA4455A33B429A6C (void);
// 0x00000070 System.Void Firebase.Platform.FirebaseMonoBehaviour::OnApplicationFocus(System.Boolean)
extern void FirebaseMonoBehaviour_OnApplicationFocus_m47DCA45653CBECEC3CD2ED4AA93CFED775F7508C (void);
// 0x00000071 System.Void Firebase.Platform.FirebaseMonoBehaviour::OnDestroy()
extern void FirebaseMonoBehaviour_OnDestroy_m92D958429C5A513836C9CD93BC6A79F1E3632BB5 (void);
// 0x00000072 System.Void Firebase.Platform.FirebaseMonoBehaviour::.ctor()
extern void FirebaseMonoBehaviour__ctor_mE65E0F0D449146C96EF81AB694A08D627CE61870 (void);
// 0x00000073 System.Type Firebase.Platform.FirebaseEditorDispatcher::get_EditorApplicationType()
extern void FirebaseEditorDispatcher_get_EditorApplicationType_m744C3F7A8A4F0111A7428A471FF1832EE21E9F4F (void);
// 0x00000074 System.Boolean Firebase.Platform.FirebaseEditorDispatcher::get_EditorIsPlaying()
extern void FirebaseEditorDispatcher_get_EditorIsPlaying_m1E0AD5FC35B4CC8CBF3CB9C979888AF2385B167A (void);
// 0x00000075 System.Boolean Firebase.Platform.FirebaseEditorDispatcher::get_EditorIsPlayingOrWillChangePlaymode()
extern void FirebaseEditorDispatcher_get_EditorIsPlayingOrWillChangePlaymode_mDE6D686F4F53DAAD26C63F42F5D75A04F82FF1EA (void);
// 0x00000076 System.Void Firebase.Platform.FirebaseEditorDispatcher::StartEditorUpdate()
extern void FirebaseEditorDispatcher_StartEditorUpdate_m0C8119B5324DE93D01358924686F0E5277AB4A9D (void);
// 0x00000077 System.Void Firebase.Platform.FirebaseEditorDispatcher::StopEditorUpdate()
extern void FirebaseEditorDispatcher_StopEditorUpdate_mEBA51CC256D7E6F74C078B41991EC7E16406FCB2 (void);
// 0x00000078 System.Void Firebase.Platform.FirebaseEditorDispatcher::Update()
extern void FirebaseEditorDispatcher_Update_mF6AC2EC58DD196A30C1D9A95B37546F193E4214A (void);
// 0x00000079 System.Void Firebase.Platform.FirebaseEditorDispatcher::ListenToPlayState(System.Boolean)
extern void FirebaseEditorDispatcher_ListenToPlayState_m26460E4F581DC3A701E81F1031D0B8FFFDFA8975 (void);
// 0x0000007A System.Void Firebase.Platform.FirebaseEditorDispatcher::PlayModeStateChanged()
extern void FirebaseEditorDispatcher_PlayModeStateChanged_mEBABF2E4EC5AFD6F6ED10B2471759E9531DE011C (void);
// 0x0000007B System.Void Firebase.Platform.FirebaseEditorDispatcher::PlayModeStateChangedWithArg(T)
// 0x0000007C System.Void Firebase.Platform.FirebaseEditorDispatcher::AddRemoveCallbackToField(System.Reflection.FieldInfo,System.Action,System.Object,System.Boolean,System.String)
extern void FirebaseEditorDispatcher_AddRemoveCallbackToField_mDA1FE1B11F0931F52F72E71C495E2CB124F0F33B (void);
// 0x0000007D Firebase.Platform.IAppConfigExtensions Firebase.Platform.Default.AppConfigExtensions::get_Instance()
extern void AppConfigExtensions_get_Instance_mE7CD33CD1BA6F7AB1C77ECA12204C46D8CF0FA17 (void);
// 0x0000007E System.Void Firebase.Platform.Default.AppConfigExtensions::.ctor()
extern void AppConfigExtensions__ctor_mAAB3762C447D2387878715D18F41BADFE2DB001F (void);
// 0x0000007F System.Void Firebase.Platform.Default.AppConfigExtensions::.cctor()
extern void AppConfigExtensions__cctor_m29CC574E19B84AAB7D610608DC2CFCDF025AD29B (void);
// 0x00000080 System.Void Firebase.Platform.Default.SystemClock::.ctor()
extern void SystemClock__ctor_m3F79E58AE923842C64A5EFA25E36918C5DB7495A (void);
// 0x00000081 System.Void Firebase.Platform.Default.SystemClock::.cctor()
extern void SystemClock__cctor_m35B31B56AB9A62CD5044A2F5AEDD4AB62734BFE7 (void);
// 0x00000082 Firebase.Platform.IAppConfigExtensions Firebase.Platform.Default.UnityConfigExtensions::get_DefaultInstance()
extern void UnityConfigExtensions_get_DefaultInstance_mB4442653C9662A8DFA4B1313B8440F52712EB5A9 (void);
// 0x00000083 System.Void Firebase.Platform.Default.UnityConfigExtensions::.ctor()
extern void UnityConfigExtensions__ctor_m7B66E6481FB0FF321D962EF1A37B737210D0C854 (void);
// 0x00000084 System.Void Firebase.Platform.Default.UnityConfigExtensions::.cctor()
extern void UnityConfigExtensions__cctor_mD999998F632C47D4F6FB35B8DFDDCE96A8F51A56 (void);
static Il2CppMethodPointer s_methodPointers[132] = 
{
	ExceptionAggregator_get_Exceptions_m972559432E5D3893ED3CE759068B981B4F3860CC,
	ExceptionAggregator_GetAndClearPendingExceptions_m7E2429EE2556341124AB3D82CB1725543365A28A,
	ExceptionAggregator_ThrowAndClearPendingExceptions_m5678256804A44705C715EC54446D8F973C4029A6,
	ExceptionAggregator_LogException_m3058016DA78DA3A3204AEA89C92BB0826C2F11F3,
	ExceptionAggregator_Wrap_m0B206615C9C9608E5D2CC1738CB2056F121811FB,
	ExceptionAggregator__cctor_m0A8976163955C4D2D501402578DF21C1171A15F4,
	Dispatcher__ctor_m20D7687CB59CF114ACF69131CD3BA289B80A8B2A,
	NULL,
	NULL,
	NULL,
	Dispatcher_ManagesThisThread_mF00448368389637B00847E99D9DA66DEF82E118D,
	Dispatcher_PollJobs_m90DF848D76273E55DB08A80E9C246E9E92E931CE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UnityLoggingService_get_Instance_mD7B53127EFA0E161C976160DACDF8C7E90B20E87,
	UnityLoggingService__ctor_mCD777147C85E451F625C68F6D3B5713E7FB00D31,
	UnityLoggingService__cctor_m83A5B3C1C5E217321C63C3704CD9920D029A955C,
	UnityPlatformServices_SetupServices_m8BC0FADC50ED15B12820FA8AD44AD7599728DAB4,
	UnitySynchronizationContext__ctor_m02C29AFB8681306BC3D3761C68B805F815C169C9,
	UnitySynchronizationContext_Create_m038F4C4C5A084C760A9340693B303F211FB3F3E4,
	UnitySynchronizationContext_Destroy_m9D39F827AC31DC811259997B10C13894D716C086,
	UnitySynchronizationContext_GetThreadEvent_m427C8A60FC426FB828BFE2FD8B3F4099CCDC25D0,
	UnitySynchronizationContext_Post_m27CEEE71D49CB394434E47807272ADBB1B00B7A9,
	UnitySynchronizationContext_Send_m6287E48D5488043652D3E93E610AE279CD616D00,
	UnitySynchronizationContext__cctor_m34420B5BE964BF2E7B480A23AD56847562DF8282,
	SynchronizationContextBehavoir_get_CallbackQueue_m291C810E98C2BF128991603294E306DF9C54836E,
	SynchronizationContextBehavoir_Start_m138FD9256FCC72808F0F211B4578B4F1B400A507,
	SynchronizationContextBehavoir__ctor_m535727BFD0F4884E295BF1DD883A4A47DB5CA226,
	U3CStartU3Ed__3__ctor_m35E3CB0C51C8D66808E279EC08986685FB35F0B4,
	U3CStartU3Ed__3_System_IDisposable_Dispose_m6BFEE452494CADFC903A5DE0A0391FE66BE0732B,
	U3CStartU3Ed__3_MoveNext_mAFA0389B4DED6BFC59975113737EE832A6051ABA,
	U3CStartU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7D5524D4D60AEB5589BBE0C16EEB6B4FCE158B5,
	U3CStartU3Ed__3_System_Collections_IEnumerator_Reset_m85F03652CF544E4A9AE0E1CFCB02377AE7354FC5,
	U3CStartU3Ed__3_System_Collections_IEnumerator_get_Current_m6A22713E2B13BDEFA00F6E7D7258603D92C9ABAD,
	U3CU3Ec__DisplayClass14_0__ctor_m71F85406259CC9E07BABFC4C9C3534B60D1188B4,
	U3CU3Ec__DisplayClass14_1__ctor_mA297006BB71E8DC6C8F9544843121E2BB0E191FA,
	U3CU3Ec__DisplayClass16_0__ctor_mE5A94FA226B6D545714950BAC1684E5B99E9BA9B,
	U3CU3Ec__DisplayClass16_1__ctor_m614CECB53A481A4CAE1FF29E4311490C145185DC,
	U3CU3Ec__DisplayClass16_1_U3CSendU3Eb__0_mE2861360FB9F457F08CCB9240711D8A785BEAD09,
	DebugLogger_get_Instance_mA20E9232A5C60CAA50C641FAE996FA62A5D82A33,
	DebugLogger__ctor_m975238F7F458467754AB4691AA1175EBB3BE7C9B,
	DebugLogger__cctor_mF8D5A3D179D533E6CD6FA37D85BEF242D3B8AFE3,
	Services__cctor_m4036DF2DCFD0D7BCDA089BEE0A8B31B6EFB2D00D,
	Services_set_AppConfig_m3A9EF6EEAE013442C30B8CC432ED28B5FD4D9F2C,
	Services_set_Clock_m6EA7AA445A3CCC399FA5BB9DFD5AF4E6DECACE88,
	Services_set_Logging_m0406136BE33759DC9EAC480E0FE232690A86A16A,
	NULL,
	NULL,
	NULL,
	FirebaseAppUtilsStub_get_Instance_m361D43B0066F8C3067E898965964328EC136E74A,
	FirebaseAppUtilsStub_TranslateDllNotFoundException_mDDDE0AFBE2714E5A360C9C26184C46E0FD6169E7,
	FirebaseAppUtilsStub_PollCallbacks_mB4F4C4556E7FE81BCAFAB9D06A2AF268E62F9463,
	FirebaseAppUtilsStub_GetLogLevel_mE32FBF80367DE28198974386C8E0F17CFD2B925F,
	FirebaseAppUtilsStub__ctor_m26C823CCA8BC46F64579375E15FA2B1766402132,
	FirebaseAppUtilsStub__cctor_m308390A5EC41ED3A7FB17E84C7F9A8EA00F8C2FE,
	NULL,
	NULL,
	NULL,
	FirebaseHandler_get_AppUtils_mD317C324F3E2F6D3260E1CEAA946847BAB91BC2C,
	FirebaseHandler_set_AppUtils_m3CF4CEF8A89CB92676CDEAC04E59850DAFB651BA,
	FirebaseHandler_get_TickCount_mA791188CABE0F2464FAB7E76709CB74631D4BCD1,
	FirebaseHandler_get_ThreadDispatcher_mF4FACC8630771208FCF0A6E4ADBD9DE2B92623E3,
	FirebaseHandler_set_ThreadDispatcher_m2AE8411CCA401E1683B93B40D4FDCC29DF953F5A,
	FirebaseHandler_get_IsPlayMode_m9B9EF275DDA78C0897805EEF2CF096070AA77D96,
	FirebaseHandler_set_IsPlayMode_mD40E7856155867FC95E46FA9E1985EA7FA47ED29,
	FirebaseHandler__cctor_mBB75468D5FE6DFD867E6E5043D863E7664EAC730,
	FirebaseHandler__ctor_m528A373696F4853B62E70CA8C61E74DD582E3442,
	FirebaseHandler_StartMonoBehaviour_m27FB410A511845F73EE29218BDEA9CEEAB1D5246,
	FirebaseHandler_StopMonoBehaviour_m6D5F532432F18CE6837F27A0FA9D586F55947451,
	NULL,
	NULL,
	FirebaseHandler_get_DefaultInstance_mAE0FC8CDB594816EE0DC5A552501A20500655F1E,
	FirebaseHandler_CreatePartialOnMainThread_mCB214362F224CAD869C9563A1AD92CFBC3B4AF6F,
	FirebaseHandler_Create_m551C916AE81A5ACBFF2119B36A9BD405FDEC1588,
	FirebaseHandler_Update_mDBFD412D735E6C3F328C9BA3E5B3C5ECFBF49138,
	FirebaseHandler_OnApplicationFocus_mEDF45D3543635BFE0D26562C1AF3A7F8005D5054,
	FirebaseHandler_OnMonoBehaviourDestroyed_m5AB2EEE4B6515BFB6B75E7E7305B1519883AB519,
	FirebaseHandler_U3CUpdateU3Eb__36_0_mF9F0EE2B1EA48002FA4AAF4E90A45F0CD28183BE,
	ApplicationFocusChangedEventArgs_set_HasFocus_m144DF1C01B04C22AC4C98A06E2FE88E89AA9C107,
	ApplicationFocusChangedEventArgs__ctor_m839E74DC0C6B72AB8A768A2D23F32ECE4A3B39AE,
	U3CU3Ec__cctor_m260427B8C39C75CB97941760EE64E9067346D45F,
	U3CU3Ec__ctor_mD106D0E3573CF90E581E70D2894A38124C3755BA,
	U3CU3Ec_U3CStopMonoBehaviourU3Eb__19_0_mB990EC27D5AB7A305AAA79490EE3DECA513B2507,
	U3CU3Ec__DisplayClass34_0__ctor_mF57098ECC5E1BDC40DC0479FFFF8462C4286FF0D,
	U3CU3Ec__DisplayClass34_0_U3CCreatePartialOnMainThreadU3Eb__0_mF30FC5B54AA56E8760A72FCF6F7B4E228921A701,
	PlatformInformation_get_IsAndroid_m8AB95334299A6FA8FEBFD7B0A6BFE9B45CB34474,
	PlatformInformation_get_IsIOS_mFD5800DAFA3923CEEEFEA6BA8E07FF6691EFB677,
	PlatformInformation_get_DefaultConfigLocation_mAEB7514B85C90CB6B9C8DEA4B2B27B0D309A8166,
	PlatformInformation_get_RealtimeSinceStartup_m954BB1B40808B6F1E4E432FC1A9A930017FD41E1,
	PlatformInformation_set_RealtimeSinceStartupSafe_mBA2F7097A44D7F1DF964ABCC2B23C0A951919941,
	PlatformInformation_get_RuntimeName_m014CA17680A844F0F26164D3F01C4722182C5A90,
	PlatformInformation_get_RuntimeVersion_mD482340448C87BCF8CC6D361C0309028E808DE39,
	U3CU3Ec__cctor_m1576EBA22A4F2E2E581A4A13FF91180980120324,
	U3CU3Ec__ctor_m6741C1EC646C313F640C936AFA67DBB23B75A397,
	U3CU3Ec_U3Cget_DefaultConfigLocationU3Eb__6_0_m3AB345EB876894BBEF4EA889DB7C050149A84ED5,
	U3CU3Ec_U3Cget_RuntimeVersionU3Eb__18_0_m534FD38D257EAEFB9FB15D828D2177701A9FC47F,
	FirebaseLogger_IsStackTraceLogTypeIncompatibleWithNativeLogs_m017E8BD41D237084924DDCF88CB2C6ABABF237DB,
	FirebaseLogger_CurrentStackTraceLogTypeIsIncompatibleWithNativeLogs_mC97D0928657EF657E397290DA97756027989B96E,
	FirebaseLogger_get_CanRedirectNativeLogs_mEBA68986F4E0627B01B89A8F793B48E2224139D3,
	FirebaseLogger_LogMessage_mD75A87D37AD77AC7BF0463C48DDD46E7901B0106,
	FirebaseLogger__cctor_m801A17DE81304E6096207E1925500D14421ECE95,
	FirebaseMonoBehaviour_GetFirebaseHandlerOrDestroyGameObject_m45E0E65DFD7138558207655AC144AE884F578967,
	FirebaseMonoBehaviour_OnEnable_mF2CFA86462A454B9C0206AAC3ACEF950FF3E3631,
	FirebaseMonoBehaviour_Update_m7DEE72CA7C251D41465F5997BA4455A33B429A6C,
	FirebaseMonoBehaviour_OnApplicationFocus_m47DCA45653CBECEC3CD2ED4AA93CFED775F7508C,
	FirebaseMonoBehaviour_OnDestroy_m92D958429C5A513836C9CD93BC6A79F1E3632BB5,
	FirebaseMonoBehaviour__ctor_mE65E0F0D449146C96EF81AB694A08D627CE61870,
	FirebaseEditorDispatcher_get_EditorApplicationType_m744C3F7A8A4F0111A7428A471FF1832EE21E9F4F,
	FirebaseEditorDispatcher_get_EditorIsPlaying_m1E0AD5FC35B4CC8CBF3CB9C979888AF2385B167A,
	FirebaseEditorDispatcher_get_EditorIsPlayingOrWillChangePlaymode_mDE6D686F4F53DAAD26C63F42F5D75A04F82FF1EA,
	FirebaseEditorDispatcher_StartEditorUpdate_m0C8119B5324DE93D01358924686F0E5277AB4A9D,
	FirebaseEditorDispatcher_StopEditorUpdate_mEBA51CC256D7E6F74C078B41991EC7E16406FCB2,
	FirebaseEditorDispatcher_Update_mF6AC2EC58DD196A30C1D9A95B37546F193E4214A,
	FirebaseEditorDispatcher_ListenToPlayState_m26460E4F581DC3A701E81F1031D0B8FFFDFA8975,
	FirebaseEditorDispatcher_PlayModeStateChanged_mEBABF2E4EC5AFD6F6ED10B2471759E9531DE011C,
	NULL,
	FirebaseEditorDispatcher_AddRemoveCallbackToField_mDA1FE1B11F0931F52F72E71C495E2CB124F0F33B,
	AppConfigExtensions_get_Instance_mE7CD33CD1BA6F7AB1C77ECA12204C46D8CF0FA17,
	AppConfigExtensions__ctor_mAAB3762C447D2387878715D18F41BADFE2DB001F,
	AppConfigExtensions__cctor_m29CC574E19B84AAB7D610608DC2CFCDF025AD29B,
	SystemClock__ctor_m3F79E58AE923842C64A5EFA25E36918C5DB7495A,
	SystemClock__cctor_m35B31B56AB9A62CD5044A2F5AEDD4AB62734BFE7,
	UnityConfigExtensions_get_DefaultInstance_mB4442653C9662A8DFA4B1313B8440F52712EB5A9,
	UnityConfigExtensions__ctor_m7B66E6481FB0FF321D962EF1A37B737210D0C854,
	UnityConfigExtensions__cctor_mD999998F632C47D4F6FB35B8DFDDCE96A8F51A56,
};
static const int32_t s_InvokerIndices[132] = 
{
	10847,
	10847,
	10889,
	10466,
	10683,
	10889,
	7252,
	0,
	0,
	0,
	7012,
	7252,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	10847,
	7252,
	10889,
	10889,
	5754,
	10683,
	10889,
	7114,
	3206,
	3206,
	10889,
	7114,
	7114,
	7252,
	5725,
	7252,
	7012,
	7114,
	7252,
	7114,
	7252,
	7252,
	7252,
	7252,
	5754,
	10847,
	7252,
	10889,
	10889,
	10683,
	10683,
	10683,
	0,
	0,
	0,
	10847,
	5754,
	7252,
	7083,
	7252,
	10889,
	0,
	0,
	0,
	10847,
	10683,
	10840,
	10847,
	10683,
	7012,
	5650,
	10889,
	7252,
	7252,
	7252,
	0,
	0,
	10847,
	10683,
	10683,
	7252,
	5650,
	10683,
	7252,
	5650,
	7252,
	10889,
	7252,
	7012,
	7252,
	7252,
	10823,
	10823,
	10847,
	10869,
	10690,
	10847,
	10847,
	10889,
	7252,
	7114,
	7114,
	10221,
	10823,
	10823,
	9909,
	10889,
	7114,
	7252,
	7252,
	5650,
	7252,
	7252,
	10847,
	10823,
	10823,
	10889,
	10889,
	10889,
	10675,
	10889,
	0,
	8112,
	10847,
	7252,
	10889,
	7252,
	10889,
	10847,
	7252,
	10889,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x02000005, { 23, 5 } },
	{ 0x02000006, { 28, 5 } },
	{ 0x02000019, { 33, 7 } },
	{ 0x06000008, { 0, 9 } },
	{ 0x06000009, { 9, 7 } },
	{ 0x0600000A, { 16, 7 } },
	{ 0x0600004D, { 40, 3 } },
	{ 0x0600004E, { 43, 2 } },
};
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass4_0_1_tAADB1AEAD638964D1CF2A88AF58C95C9B9EFB24C;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass4_0_1__ctor_m568E4BF965EDBA3756AEFC724BE507821315D695;
extern const uint32_t g_rgctx_Func_1_tE6613584449C3B0738B7F46B414AA1495C77631F;
extern const uint32_t g_rgctx_Func_1_Invoke_mD2D0140301035067FB823577F6D8A47B3F1EDFB3;
extern const uint32_t g_rgctx_CallbackStorage_1_tB18F32E2253122C593C3C978F0DD0F29292E6CAD;
extern const uint32_t g_rgctx_CallbackStorage_1__ctor_m13092E3C3BA958469A48B5683E3AFCC0FC31402C;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass4_0_1_U3CRunU3Eb__0_m20026BA2F3D3FAD37D909960A9865291C3F57C1A;
extern const uint32_t g_rgctx_CallbackStorage_1_get_Exception_m12168012D17F8A3E4D0FBC09E0C07964309F06BB;
extern const uint32_t g_rgctx_CallbackStorage_1_get_Result_mBB7B9B4DB0B9E1EFFCA683DE4A2AF24FEFADB8F9;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass5_0_1_t2A2F2CD99A63C187FDCB11411F42973460DFCEBC;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass5_0_1__ctor_m1A9EFB8D2CDA3614F142241837BF16C5347D3F9C;
extern const uint32_t g_rgctx_Dispatcher_RunAsyncNow_TisTResult_t27D1ABD7117FBC1BF1B700E445ADA6D1F2E94F98_m00ACE59C98D38228ECA10FE4CC835ACAD24EA3BB;
extern const uint32_t g_rgctx_TaskCompletionSource_1_tFCC3BACB288724B2C4094602E97DB81BCF2C9BAE;
extern const uint32_t g_rgctx_TaskCompletionSource_1__ctor_m4DAAFF0DDF174B725FA230ACC38D34BA41FA21EF;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass5_0_1_U3CRunAsyncU3Eb__0_mD97109BFD314C72A83368B4AE5778763E5734B49;
extern const uint32_t g_rgctx_TaskCompletionSource_1_get_Task_m5AD8681B281715F4AB2B7E6B35E4C19124557290;
extern const uint32_t g_rgctx_Func_1_t7BF0840276CE05A75F154B46D925AF4D344E2E12;
extern const uint32_t g_rgctx_Func_1_Invoke_mC428D57AAD4AFA24C1DC6CEBE8AD6248B9F1D5AC;
extern const uint32_t g_rgctx_Task_FromResult_TisTResult_t5AAD3ED26078298B4CA177F4F59742414D8E4670_mC2B2C8E8AE7F80D9AEF9924934642DDBA015E3EE;
extern const uint32_t g_rgctx_TaskCompletionSource_1_t6C6BABD2219E6D567C80A6D857FCC08DCD0DE642;
extern const uint32_t g_rgctx_TaskCompletionSource_1__ctor_m86232D53DD5518CC1A7F07E7AE04CDB01890C163;
extern const uint32_t g_rgctx_TaskCompletionSource_1_TrySetException_m4177FEB4CD9E8540959048ED3007512ADCA7D3E2;
extern const uint32_t g_rgctx_TaskCompletionSource_1_get_Task_mFE8BEC25E0BC3A7D0405CFEC41F20A823D31383F;
extern const uint32_t g_rgctx_Func_1_t2BB9A10EF30A8276876BC638FF6F765E828051B7;
extern const uint32_t g_rgctx_Func_1_Invoke_mF4F44F42306BCB1074C59EB6EEB4BE49A86481AC;
extern const uint32_t g_rgctx_CallbackStorage_1_t2B132D5758BCF811DDA857C86DBC66C659EB4468;
extern const uint32_t g_rgctx_CallbackStorage_1_set_Result_mCBDDA466301919AA366AF37B49667BE362006858;
extern const uint32_t g_rgctx_CallbackStorage_1_set_Exception_m887533D3C7F42085737117EC2DCDA1F3D43E234A;
extern const uint32_t g_rgctx_Func_1_t74BE0538EEE71BC25675C4A3DAB02529318DA1E5;
extern const uint32_t g_rgctx_Func_1_Invoke_m1F91F19887902CFD2D139EAFD8F9CB7A919D7C62;
extern const uint32_t g_rgctx_TaskCompletionSource_1_tB12F8B1F51896F1419CEAB99620E5A1A46ABBE22;
extern const uint32_t g_rgctx_TaskCompletionSource_1_SetResult_mC32B18DCF1F9F4AACD835B61D1594F7E1C14F21D;
extern const uint32_t g_rgctx_TaskCompletionSource_1_SetException_m6FDC1ABC70A783BC3BE3E82F7E5F1B7923A6C6EC;
extern const uint32_t g_rgctx_MainThreadProperty_1_U3Cget_ValueU3Eb__5_0_m394E836045956D464D8683C64DDB1CED638F5B11;
extern const uint32_t g_rgctx_Func_1_tFBB7E866F9EDE09602978CCA0F79DBA96D768323;
extern const uint32_t g_rgctx_Func_1__ctor_m5E021E75650F65DAF2C7536C29D99176FDD6E3AF;
extern const uint32_t g_rgctx_FirebaseHandler_RunOnMainThreadAsync_TisT_t89E6745F1ECDA7D15585B8C398BD885CEDB1AD05_mE6FD422C18B621E8F98C951F311C016C73610952;
extern const uint32_t g_rgctx_Task_1_t9614D936EA6470EC1E1741CC87B3E2B5A5C14A40;
extern const uint32_t g_rgctx_Task_1_get_Result_mFBA7557EFC8509BE49939048D6D30FCB264A8206;
extern const uint32_t g_rgctx_Func_1_Invoke_m9388CA7FCDFCC870506D527BD80D468BD837F0B1;
extern const uint32_t g_rgctx_Dispatcher_Run_TisTResult_t57B1C1429DEDEC9AFB226AF556694A497C18225D_mD737E1B8C1879E873E52AA220243ACF57949C466;
extern const uint32_t g_rgctx_Func_1_t468D86948B43D3FF053E73CD334F50868A885C40;
extern const uint32_t g_rgctx_Func_1_Invoke_mB7442413EDF85A56CA1EEFB7BEF1C6C60E62E6DD;
extern const uint32_t g_rgctx_Dispatcher_RunAsync_TisTResult_tD6DDF2DD8943D7C0C28879A183A800F70135431C_m55DDB5F2D5EB895AF57EF72201981FB6644710DB;
extern const uint32_t g_rgctx_Dispatcher_RunAsyncNow_TisTResult_tD6DDF2DD8943D7C0C28879A183A800F70135431C_mA63CF49102633CE3018ED6676FD1319F426B6766;
static const Il2CppRGCTXDefinition s_rgctxValues[45] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass4_0_1_tAADB1AEAD638964D1CF2A88AF58C95C9B9EFB24C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass4_0_1__ctor_m568E4BF965EDBA3756AEFC724BE507821315D695 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_tE6613584449C3B0738B7F46B414AA1495C77631F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_mD2D0140301035067FB823577F6D8A47B3F1EDFB3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_CallbackStorage_1_tB18F32E2253122C593C3C978F0DD0F29292E6CAD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_CallbackStorage_1__ctor_m13092E3C3BA958469A48B5683E3AFCC0FC31402C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass4_0_1_U3CRunU3Eb__0_m20026BA2F3D3FAD37D909960A9865291C3F57C1A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_CallbackStorage_1_get_Exception_m12168012D17F8A3E4D0FBC09E0C07964309F06BB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_CallbackStorage_1_get_Result_mBB7B9B4DB0B9E1EFFCA683DE4A2AF24FEFADB8F9 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass5_0_1_t2A2F2CD99A63C187FDCB11411F42973460DFCEBC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass5_0_1__ctor_m1A9EFB8D2CDA3614F142241837BF16C5347D3F9C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dispatcher_RunAsyncNow_TisTResult_t27D1ABD7117FBC1BF1B700E445ADA6D1F2E94F98_m00ACE59C98D38228ECA10FE4CC835ACAD24EA3BB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TaskCompletionSource_1_tFCC3BACB288724B2C4094602E97DB81BCF2C9BAE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TaskCompletionSource_1__ctor_m4DAAFF0DDF174B725FA230ACC38D34BA41FA21EF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass5_0_1_U3CRunAsyncU3Eb__0_mD97109BFD314C72A83368B4AE5778763E5734B49 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TaskCompletionSource_1_get_Task_m5AD8681B281715F4AB2B7E6B35E4C19124557290 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t7BF0840276CE05A75F154B46D925AF4D344E2E12 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_mC428D57AAD4AFA24C1DC6CEBE8AD6248B9F1D5AC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Task_FromResult_TisTResult_t5AAD3ED26078298B4CA177F4F59742414D8E4670_mC2B2C8E8AE7F80D9AEF9924934642DDBA015E3EE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TaskCompletionSource_1_t6C6BABD2219E6D567C80A6D857FCC08DCD0DE642 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TaskCompletionSource_1__ctor_m86232D53DD5518CC1A7F07E7AE04CDB01890C163 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TaskCompletionSource_1_TrySetException_m4177FEB4CD9E8540959048ED3007512ADCA7D3E2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TaskCompletionSource_1_get_Task_mFE8BEC25E0BC3A7D0405CFEC41F20A823D31383F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t2BB9A10EF30A8276876BC638FF6F765E828051B7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_mF4F44F42306BCB1074C59EB6EEB4BE49A86481AC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_CallbackStorage_1_t2B132D5758BCF811DDA857C86DBC66C659EB4468 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_CallbackStorage_1_set_Result_mCBDDA466301919AA366AF37B49667BE362006858 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_CallbackStorage_1_set_Exception_m887533D3C7F42085737117EC2DCDA1F3D43E234A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t74BE0538EEE71BC25675C4A3DAB02529318DA1E5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_m1F91F19887902CFD2D139EAFD8F9CB7A919D7C62 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TaskCompletionSource_1_tB12F8B1F51896F1419CEAB99620E5A1A46ABBE22 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TaskCompletionSource_1_SetResult_mC32B18DCF1F9F4AACD835B61D1594F7E1C14F21D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TaskCompletionSource_1_SetException_m6FDC1ABC70A783BC3BE3E82F7E5F1B7923A6C6EC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_MainThreadProperty_1_U3Cget_ValueU3Eb__5_0_m394E836045956D464D8683C64DDB1CED638F5B11 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_tFBB7E866F9EDE09602978CCA0F79DBA96D768323 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1__ctor_m5E021E75650F65DAF2C7536C29D99176FDD6E3AF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_FirebaseHandler_RunOnMainThreadAsync_TisT_t89E6745F1ECDA7D15585B8C398BD885CEDB1AD05_mE6FD422C18B621E8F98C951F311C016C73610952 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Task_1_t9614D936EA6470EC1E1741CC87B3E2B5A5C14A40 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Task_1_get_Result_mFBA7557EFC8509BE49939048D6D30FCB264A8206 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_m9388CA7FCDFCC870506D527BD80D468BD837F0B1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dispatcher_Run_TisTResult_t57B1C1429DEDEC9AFB226AF556694A497C18225D_mD737E1B8C1879E873E52AA220243ACF57949C466 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t468D86948B43D3FF053E73CD334F50868A885C40 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_mB7442413EDF85A56CA1EEFB7BEF1C6C60E62E6DD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dispatcher_RunAsync_TisTResult_tD6DDF2DD8943D7C0C28879A183A800F70135431C_m55DDB5F2D5EB895AF57EF72201981FB6644710DB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dispatcher_RunAsyncNow_TisTResult_tD6DDF2DD8943D7C0C28879A183A800F70135431C_mA63CF49102633CE3018ED6676FD1319F426B6766 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Firebase_Platform_CodeGenModule;
const Il2CppCodeGenModule g_Firebase_Platform_CodeGenModule = 
{
	"Firebase.Platform.dll",
	132,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	45,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Links::Start()
extern void Links_Start_mD4C7680CC2CDF8EAE0F3C3236D14989961362C90 (void);
// 0x00000002 System.Void Links::Update()
extern void Links_Update_m391FF2C1305BC193326E0568C941DB64E9C724D1 (void);
// 0x00000003 System.Void Links::.ctor()
extern void Links__ctor_mC64AE52DDDF32F0469AB2C82DD86822A5C6B8492 (void);
// 0x00000004 System.Void AssetBundleAugmenter::Start()
extern void AssetBundleAugmenter_Start_m1F6B923C0735972FEC73227A85F3F92346B4EDF3 (void);
// 0x00000005 System.Collections.IEnumerator AssetBundleAugmenter::DownloadAndCache()
extern void AssetBundleAugmenter_DownloadAndCache_m3A3DD8DCAB3633C590F5E0F63BC5F7C004B2D569 (void);
// 0x00000006 System.Void AssetBundleAugmenter::LoadAssetToTarget()
extern void AssetBundleAugmenter_LoadAssetToTarget_mCE61E79DDCA954A8A4B452146E5D9156252E9972 (void);
// 0x00000007 System.Void AssetBundleAugmenter::.ctor()
extern void AssetBundleAugmenter__ctor_m5733419DC571A56833902E11DB57E3E2CC11EE5D (void);
// 0x00000008 System.Void AssetBundleAugmenter/<DownloadAndCache>d__6::.ctor(System.Int32)
extern void U3CDownloadAndCacheU3Ed__6__ctor_m6117947E915B1F42CA8DEECBB6264AEC2B859415 (void);
// 0x00000009 System.Void AssetBundleAugmenter/<DownloadAndCache>d__6::System.IDisposable.Dispose()
extern void U3CDownloadAndCacheU3Ed__6_System_IDisposable_Dispose_mC996CA3B88875585B4EAE00D4F3F71042F00EC12 (void);
// 0x0000000A System.Boolean AssetBundleAugmenter/<DownloadAndCache>d__6::MoveNext()
extern void U3CDownloadAndCacheU3Ed__6_MoveNext_m3E7DBD345B248B772991DCAED65E48BF043450A7 (void);
// 0x0000000B System.Void AssetBundleAugmenter/<DownloadAndCache>d__6::<>m__Finally1()
extern void U3CDownloadAndCacheU3Ed__6_U3CU3Em__Finally1_m9A8FE3FA2D93847E735E89861E36476B9C77798E (void);
// 0x0000000C System.Object AssetBundleAugmenter/<DownloadAndCache>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadAndCacheU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m847B435C54A1528CB7F3D8F84E34ED64C3FD3BC1 (void);
// 0x0000000D System.Void AssetBundleAugmenter/<DownloadAndCache>d__6::System.Collections.IEnumerator.Reset()
extern void U3CDownloadAndCacheU3Ed__6_System_Collections_IEnumerator_Reset_m5856F943253ABABA33AEF5EEBDCDBC7B6BC3F183 (void);
// 0x0000000E System.Object AssetBundleAugmenter/<DownloadAndCache>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadAndCacheU3Ed__6_System_Collections_IEnumerator_get_Current_m2311484CD8028150A0EB84996A6CA04AB2B9F842 (void);
// 0x0000000F System.Void ButtonCreator::Start()
extern void ButtonCreator_Start_m5BBDF3A30EF06C7B5FED8CE144882E641D5B3F38 (void);
// 0x00000010 System.Void ButtonCreator::buttonCreate(System.String[])
extern void ButtonCreator_buttonCreate_mD377EE34982441845258D1E28073FFA606AF3651 (void);
// 0x00000011 System.Void ButtonCreator::CreateButton(System.String)
extern void ButtonCreator_CreateButton_m8A8A11C85AAC14E8F581AFC29CA642F1661643F6 (void);
// 0x00000012 System.Collections.IEnumerator ButtonCreator::DownloadImage(System.String,UnityEngine.UI.RawImage)
extern void ButtonCreator_DownloadImage_m2C566381164718D77627D41EE907402D95C3247E (void);
// 0x00000013 System.Void ButtonCreator::.ctor()
extern void ButtonCreator__ctor_mC113A0EAB5109E7FCC6A5AC1BAAD2713C925EFFF (void);
// 0x00000014 System.Void ButtonCreator/<DownloadImage>d__9::.ctor(System.Int32)
extern void U3CDownloadImageU3Ed__9__ctor_m36CE121F54A66D653BBAFE86C8C3F36C3E8A9A70 (void);
// 0x00000015 System.Void ButtonCreator/<DownloadImage>d__9::System.IDisposable.Dispose()
extern void U3CDownloadImageU3Ed__9_System_IDisposable_Dispose_mA7F09EFCD49275A39C8C430DFA64B0587992B1A8 (void);
// 0x00000016 System.Boolean ButtonCreator/<DownloadImage>d__9::MoveNext()
extern void U3CDownloadImageU3Ed__9_MoveNext_mCF8AF6455F99CB2C2028601DCF46BA989B720A2B (void);
// 0x00000017 System.Void ButtonCreator/<DownloadImage>d__9::<>m__Finally1()
extern void U3CDownloadImageU3Ed__9_U3CU3Em__Finally1_mA2E26617BD780516F14B700E3826A4FA9A32714E (void);
// 0x00000018 System.Object ButtonCreator/<DownloadImage>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadImageU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A23B36453D204DE6E28468AC35FE64A1E9FA052 (void);
// 0x00000019 System.Void ButtonCreator/<DownloadImage>d__9::System.Collections.IEnumerator.Reset()
extern void U3CDownloadImageU3Ed__9_System_Collections_IEnumerator_Reset_m9E50E5F39F3DB7D1AF17323089D6BA3547BB4C69 (void);
// 0x0000001A System.Object ButtonCreator/<DownloadImage>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadImageU3Ed__9_System_Collections_IEnumerator_get_Current_m9673B80BF60BB0331A93DE80F2CDB40B6D701811 (void);
// 0x0000001B System.Void Deneme::Start()
extern void Deneme_Start_m4149F1251FF4C18042F5BC5C1979EA949A36431C (void);
// 0x0000001C System.Collections.IEnumerator Deneme::DownloadAssetBundle(System.String)
extern void Deneme_DownloadAssetBundle_m61277F0223991C310B1A91146A816036B1F7989C (void);
// 0x0000001D System.Collections.IEnumerator Deneme::DownloadImage(System.String)
extern void Deneme_DownloadImage_m3C3B6051E8EA44F59084A49F358C69CAEAD9DAD2 (void);
// 0x0000001E System.Void Deneme::.ctor()
extern void Deneme__ctor_m6E4CE9125326146DD6DA01F856D9DBC6454146D8 (void);
// 0x0000001F System.Void Deneme::<Start>b__5_0(System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot>)
extern void Deneme_U3CStartU3Eb__5_0_mE1392A6512F3BBD07348E1959C379627CC2D0977 (void);
// 0x00000020 System.Void Deneme/<DownloadAssetBundle>d__6::.ctor(System.Int32)
extern void U3CDownloadAssetBundleU3Ed__6__ctor_m0AAEBB63E5BF0C9176ED992804997B15008AB41E (void);
// 0x00000021 System.Void Deneme/<DownloadAssetBundle>d__6::System.IDisposable.Dispose()
extern void U3CDownloadAssetBundleU3Ed__6_System_IDisposable_Dispose_m4A6613135BA27F9A2C95943C0D4F777A4A565E71 (void);
// 0x00000022 System.Boolean Deneme/<DownloadAssetBundle>d__6::MoveNext()
extern void U3CDownloadAssetBundleU3Ed__6_MoveNext_m555C5DAAE4653FBA530FC0E12233544E70DF2F4E (void);
// 0x00000023 System.Void Deneme/<DownloadAssetBundle>d__6::<>m__Finally1()
extern void U3CDownloadAssetBundleU3Ed__6_U3CU3Em__Finally1_m54954E626FF2067E9489377938C29BFC5374E0E0 (void);
// 0x00000024 System.Object Deneme/<DownloadAssetBundle>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadAssetBundleU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFBB9345A012BBD02BEA9B3804A3CBA945C01323A (void);
// 0x00000025 System.Void Deneme/<DownloadAssetBundle>d__6::System.Collections.IEnumerator.Reset()
extern void U3CDownloadAssetBundleU3Ed__6_System_Collections_IEnumerator_Reset_m6264A0BDFCCE071C6E0FB498626E69E0AFA7024B (void);
// 0x00000026 System.Object Deneme/<DownloadAssetBundle>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadAssetBundleU3Ed__6_System_Collections_IEnumerator_get_Current_m8C70CD0FA762D9D639AA5E5E255884CA8D569483 (void);
// 0x00000027 System.Void Deneme/<DownloadImage>d__8::.ctor(System.Int32)
extern void U3CDownloadImageU3Ed__8__ctor_mB44D47E2F1D9CE14709A6E2FC37502B862B19709 (void);
// 0x00000028 System.Void Deneme/<DownloadImage>d__8::System.IDisposable.Dispose()
extern void U3CDownloadImageU3Ed__8_System_IDisposable_Dispose_mBFC39A7BC08FE0FF1232DB5AB415E7205715ED6D (void);
// 0x00000029 System.Boolean Deneme/<DownloadImage>d__8::MoveNext()
extern void U3CDownloadImageU3Ed__8_MoveNext_m84283DF82949E47A343C097263DDDE1B05E17BF1 (void);
// 0x0000002A System.Void Deneme/<DownloadImage>d__8::<>m__Finally1()
extern void U3CDownloadImageU3Ed__8_U3CU3Em__Finally1_m4A5C9B877623A12C508306DA2344C2327FA87039 (void);
// 0x0000002B System.Object Deneme/<DownloadImage>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadImageU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC6B0457D1A21FD2CCF37E3603BE87EC453D1907D (void);
// 0x0000002C System.Void Deneme/<DownloadImage>d__8::System.Collections.IEnumerator.Reset()
extern void U3CDownloadImageU3Ed__8_System_Collections_IEnumerator_Reset_m90D8BD962F9F5DA68C04CBE431C48BF44B3A1A52 (void);
// 0x0000002D System.Object Deneme/<DownloadImage>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadImageU3Ed__8_System_Collections_IEnumerator_get_Current_m2E510EB326C87CF989638CC12353CF91DB59CCAE (void);
// 0x0000002E System.Void LoadAssetBundles::Start()
extern void LoadAssetBundles_Start_m6BD605E2E3C7770A173A76BF286A393937FAFD51 (void);
// 0x0000002F System.Collections.IEnumerator LoadAssetBundles::GetAssetBundle()
extern void LoadAssetBundles_GetAssetBundle_m27FE7E153495443D3CBFF03C2037E3C8A0B606EA (void);
// 0x00000030 System.Void LoadAssetBundles::.ctor()
extern void LoadAssetBundles__ctor_mA525E61196A0B3518AE50EC15FEFC8A24D9A5FFC (void);
// 0x00000031 System.Void LoadAssetBundles/<GetAssetBundle>d__5::.ctor(System.Int32)
extern void U3CGetAssetBundleU3Ed__5__ctor_m6C200D07A3532CF63ACEF83DEDC7073232131D8C (void);
// 0x00000032 System.Void LoadAssetBundles/<GetAssetBundle>d__5::System.IDisposable.Dispose()
extern void U3CGetAssetBundleU3Ed__5_System_IDisposable_Dispose_m8AFB4975C4A0EE1518D06C7D78F79742DFF4C865 (void);
// 0x00000033 System.Boolean LoadAssetBundles/<GetAssetBundle>d__5::MoveNext()
extern void U3CGetAssetBundleU3Ed__5_MoveNext_m19C76EBD68162630E9D6609BD3FCF82BAB0A6F8E (void);
// 0x00000034 System.Object LoadAssetBundles/<GetAssetBundle>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetAssetBundleU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F1F4AEE1ED6DC08BD5E52424ED0F3A17A77961B (void);
// 0x00000035 System.Void LoadAssetBundles/<GetAssetBundle>d__5::System.Collections.IEnumerator.Reset()
extern void U3CGetAssetBundleU3Ed__5_System_Collections_IEnumerator_Reset_mEB1875F8A2F29AA0940F15481BE7B7E91BAAF49A (void);
// 0x00000036 System.Object LoadAssetBundles/<GetAssetBundle>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CGetAssetBundleU3Ed__5_System_Collections_IEnumerator_get_Current_m067C4E5B3A7DBBFDD8401C89FDEC9FFA81DABF28 (void);
// 0x00000037 System.Void CategoryCilckable::Start()
extern void CategoryCilckable_Start_m123AA288408CCDD2B831AFEFB8DA3449EA110E73 (void);
// 0x00000038 System.Void CategoryCilckable::RotationAdjust()
extern void CategoryCilckable_RotationAdjust_m89306E69A8213779124D3B3423112EDB2678A44B (void);
// 0x00000039 System.Void CategoryCilckable::RotationKoltuklar()
extern void CategoryCilckable_RotationKoltuklar_m505246A1847DEFBD95B548B5DDB7A993AB937897 (void);
// 0x0000003A System.Void CategoryCilckable::BackButtonClicked()
extern void CategoryCilckable_BackButtonClicked_m6854EC5E894C0EC116DBBE9B49AC3F831E0137AA (void);
// 0x0000003B System.Void CategoryCilckable::PanelsFalse()
extern void CategoryCilckable_PanelsFalse_m831A4DD7153A691B8A9489F658455220C1139785 (void);
// 0x0000003C System.Void CategoryCilckable::PanelsTrue()
extern void CategoryCilckable_PanelsTrue_mFA911DB576A59E98020E9D30BB5B458F0B20F5E1 (void);
// 0x0000003D System.Void CategoryCilckable::ActiveRecursively(UnityEngine.GameObject)
extern void CategoryCilckable_ActiveRecursively_m836E545FC59735F66741D7E9626D8DA4CC8E8103 (void);
// 0x0000003E System.Void CategoryCilckable::DisableRecursively(UnityEngine.GameObject)
extern void CategoryCilckable_DisableRecursively_m18D2E2F5E36EAB927EDC75EC465E5E341E4E5E0E (void);
// 0x0000003F System.Void CategoryCilckable::Update()
extern void CategoryCilckable_Update_m348D870B902E80F43B5794CE83B05BF93FB9A27D (void);
// 0x00000040 System.Void CategoryCilckable::cube1()
extern void CategoryCilckable_cube1_m5DD0E53766CC8454E5A459A1DD6B5F4AE53518CE (void);
// 0x00000041 System.Void CategoryCilckable::cube2()
extern void CategoryCilckable_cube2_mE9691BA98BD9C2EA9D6ACC5E65D77C57E6EA3FEF (void);
// 0x00000042 System.Void CategoryCilckable::cube3()
extern void CategoryCilckable_cube3_mCE82A1E9AD5360E2F709CC44FA50B0AE21177C97 (void);
// 0x00000043 System.Void CategoryCilckable::capsule1()
extern void CategoryCilckable_capsule1_m5AF1E18D1C830E6821ABA2802A78D54AE752A956 (void);
// 0x00000044 System.Void CategoryCilckable::capsule2()
extern void CategoryCilckable_capsule2_mAB256BB81EA214054418E0C405E712B979622719 (void);
// 0x00000045 System.Void CategoryCilckable::capsule3()
extern void CategoryCilckable_capsule3_mBC93C6CD59AEAA8C1BB7A70C4AEF1EA751A28AD3 (void);
// 0x00000046 System.Void CategoryCilckable::sphere1()
extern void CategoryCilckable_sphere1_m174D78EC4006BEDB9A90177B5EB71A14DD572C18 (void);
// 0x00000047 System.Void CategoryCilckable::sphere2()
extern void CategoryCilckable_sphere2_m717745E43B20B987D8DDD470D718DA66CA00B21A (void);
// 0x00000048 System.Void CategoryCilckable::sphere3()
extern void CategoryCilckable_sphere3_mFB12DABEBBFB2FC2A561038017FC7B523F8731CF (void);
// 0x00000049 System.Void CategoryCilckable::ActivateObject(System.Int32)
extern void CategoryCilckable_ActivateObject_m58652FCB173787A3F492223FB512B36A415B3975 (void);
// 0x0000004A System.Void CategoryCilckable::.ctor()
extern void CategoryCilckable__ctor_m24CE01BCF364EFDE02CB5F23EA73838236422F29 (void);
// 0x0000004B System.Void scrollbarSize::Start()
extern void scrollbarSize_Start_m62C051C9ABE5CAD9844046CDDE978BB2C9EBDC54 (void);
// 0x0000004C System.Void scrollbarSize::OnScrollbarValueChanged(System.Single)
extern void scrollbarSize_OnScrollbarValueChanged_mECAEC8E76FC4DB847D1E22D3FC11AD354834F3A1 (void);
// 0x0000004D System.Void scrollbarSize::.ctor()
extern void scrollbarSize__ctor_m01A5917699D1AD00D04A76ECC53A4895A742D2FA (void);
// 0x0000004E System.Void Yorumlanm??Hali::Start()
extern void YorumlanmU131U15FHali_Start_m4D5356E1B248515B6E95AB3414F2271BF5124AD5 (void);
// 0x0000004F System.Void Yorumlanm??Hali::RotationAdjust()
extern void YorumlanmU131U15FHali_RotationAdjust_mB33D2A91FDA7DB401429020E3DBE081FFBD0DD37 (void);
// 0x00000050 System.Void Yorumlanm??Hali::BackButtonClicked()
extern void YorumlanmU131U15FHali_BackButtonClicked_m3A61DBCCDC2193F35C96E9BF718753DAB5300AB3 (void);
// 0x00000051 System.Void Yorumlanm??Hali::PanelsFalse()
extern void YorumlanmU131U15FHali_PanelsFalse_mAA7C11C62A9AB61B66C000D31FA9EA81800C780E (void);
// 0x00000052 System.Void Yorumlanm??Hali::PanelsTrue()
extern void YorumlanmU131U15FHali_PanelsTrue_m15E93F83A863C9EB855BA2C3E85441EC2CC75A35 (void);
// 0x00000053 System.Void Yorumlanm??Hali::ActiveRecursively(UnityEngine.GameObject)
extern void YorumlanmU131U15FHali_ActiveRecursively_m53DA220D6265C919507064129672374BF3227337 (void);
// 0x00000054 System.Void Yorumlanm??Hali::DisableRecursively(UnityEngine.GameObject)
extern void YorumlanmU131U15FHali_DisableRecursively_mC6376D633508E9F1BEFE22E505602009401A5FFC (void);
// 0x00000055 System.Void Yorumlanm??Hali::Update()
extern void YorumlanmU131U15FHali_Update_m58E648EA369B40E147E5A70C6A7B120F92F4FAE9 (void);
// 0x00000056 System.Void Yorumlanm??Hali::cube1()
extern void YorumlanmU131U15FHali_cube1_m917ABD84792EA0444D00FDDCB1934DCD4A32A587 (void);
// 0x00000057 System.Void Yorumlanm??Hali::cube2()
extern void YorumlanmU131U15FHali_cube2_mBD96C16F2AB575192DE6AC81C0C1F6B72F7338AA (void);
// 0x00000058 System.Void Yorumlanm??Hali::cube3()
extern void YorumlanmU131U15FHali_cube3_m54E677668CF1C84F3D7E4983E349356D4884DC72 (void);
// 0x00000059 System.Void Yorumlanm??Hali::capsule1()
extern void YorumlanmU131U15FHali_capsule1_m5BF695F0F44282EE5167CD1E89B146C55970C214 (void);
// 0x0000005A System.Void Yorumlanm??Hali::capsule2()
extern void YorumlanmU131U15FHali_capsule2_m21E69F37E998133B679FEF32CB6264149E752F22 (void);
// 0x0000005B System.Void Yorumlanm??Hali::capsule3()
extern void YorumlanmU131U15FHali_capsule3_m53730054D544DC814AE4A6C997E11E6DA7768FE7 (void);
// 0x0000005C System.Void Yorumlanm??Hali::sphere1()
extern void YorumlanmU131U15FHali_sphere1_mB9E1946380A6DA0D091BD53F11FB4310A2565C4F (void);
// 0x0000005D System.Void Yorumlanm??Hali::sphere2()
extern void YorumlanmU131U15FHali_sphere2_m2BEA3C31A7AA423ADDEF5E71F7299B38A96C814B (void);
// 0x0000005E System.Void Yorumlanm??Hali::sphere3()
extern void YorumlanmU131U15FHali_sphere3_m431AC9818E719D1B6CF700979231F264DDF58938 (void);
// 0x0000005F System.Void Yorumlanm??Hali::ActivateObject(System.Int32)
extern void YorumlanmU131U15FHali_ActivateObject_m561AA5AF3666D7B7772CFA9B52DEB448C92AD0DD (void);
// 0x00000060 System.Void Yorumlanm??Hali::.ctor()
extern void YorumlanmU131U15FHali__ctor_mC20EFC3C38B7F9B412B62AA97661F9C94CD0A4BE (void);
// 0x00000061 System.Void cubePanel::Start()
extern void cubePanel_Start_mF4D22318826133A7F5144CDBA8B43532AC9C2767 (void);
// 0x00000062 System.Void cubePanel::Update()
extern void cubePanel_Update_m67CC6A13A9A076DDE269F406B5C5ABD596C7DBF5 (void);
// 0x00000063 System.Void cubePanel::.ctor()
extern void cubePanel__ctor_mD81FE00FE1883369EBFF34933BF6702C57FB0493 (void);
// 0x00000064 System.Void svipe_Menu::Update()
extern void svipe_Menu_Update_m2FCF2205C07F975B85FFE47E0CC3B1F9C922EB69 (void);
// 0x00000065 System.Void svipe_Menu::.ctor()
extern void svipe_Menu__ctor_mF15CE4C7C2C296892B4FCBB78366D8D8ACBCD89E (void);
// 0x00000066 System.Void GaugeValueDisplay::Update()
extern void GaugeValueDisplay_Update_mEC1F962782760DF946E33D0AFA05AF5D8F467770 (void);
// 0x00000067 System.Void GaugeValueDisplay::SetPointerToValue(System.Single)
extern void GaugeValueDisplay_SetPointerToValue_mB369A0554BCA47B1393B75995B07D58545E2CBE9 (void);
// 0x00000068 System.Void GaugeValueDisplay::.ctor()
extern void GaugeValueDisplay__ctor_mC7EE4BCF4A83A2D7BA95A6048B0565CF74D42EF5 (void);
// 0x00000069 System.Void ImageValueDisplay::OnValueChanged(System.Single)
extern void ImageValueDisplay_OnValueChanged_m6E2BAA209A70C46335B21D8D4C40ADA3867FAC99 (void);
// 0x0000006A System.Void ImageValueDisplay::Update()
extern void ImageValueDisplay_Update_mF4F8CDEE4940DFFF629055A3CB66BD29ABA9F2D4 (void);
// 0x0000006B System.Void ImageValueDisplay::.ctor()
extern void ImageValueDisplay__ctor_m4EA468E68D0747990C34D7B6B9EA0E015749D320 (void);
// 0x0000006C System.Void LineRendererPatternShifter::Awake()
extern void LineRendererPatternShifter_Awake_mCD0B249349AB670D4EA8F855550ADE69FF1EAE01 (void);
// 0x0000006D System.Void LineRendererPatternShifter::Update()
extern void LineRendererPatternShifter_Update_mA2E5E6FEDAB401D1EA721C15B61FE7E565898E3B (void);
// 0x0000006E System.Void LineRendererPatternShifter::.ctor()
extern void LineRendererPatternShifter__ctor_mF6FEEA94B52D6CBA258215CA641CE3C2E5D5BA68 (void);
// 0x0000006F System.Void ModularBillBoard::Awake()
extern void ModularBillBoard_Awake_m6757BEAA47DDE2958831D0BDDF21CA62FB412483 (void);
// 0x00000070 System.Void ModularBillBoard::CalculateDerivedValues()
extern void ModularBillBoard_CalculateDerivedValues_m304586089018E377063F464282F6F742413D23A4 (void);
// 0x00000071 System.Void ModularBillBoard::CalculateMaskSize()
extern void ModularBillBoard_CalculateMaskSize_m205ED3891CC3D928E62D739C5F22FD3318B3A725 (void);
// 0x00000072 System.Void ModularBillBoard::CalculateLandmarkPosition()
extern void ModularBillBoard_CalculateLandmarkPosition_mF18B8B744648A58A46029E7EF14BDA8A7AAE1A88 (void);
// 0x00000073 System.Void ModularBillBoard::Update()
extern void ModularBillBoard_Update_mE65208BAC3B6E24DB99924E5EE2733F9E9F7FAD8 (void);
// 0x00000074 System.Void ModularBillBoard::Opening(System.Single)
extern void ModularBillBoard_Opening_m89F36DB21F8B71B0248CDA19FEEDDF920A75BFDD (void);
// 0x00000075 System.Void ModularBillBoard::Closed()
extern void ModularBillBoard_Closed_m5F5017F6FA1664B16102CE6B33B2E178A7A4231B (void);
// 0x00000076 System.Void ModularBillBoard::FullyOpened()
extern void ModularBillBoard_FullyOpened_mC5A097D00A64AACA7E211727111B547BB1B5E56F (void);
// 0x00000077 System.Void ModularBillBoard::TurnContentVisible()
extern void ModularBillBoard_TurnContentVisible_mE6D2A6CB926227C3E05C1682760D1EE7494750CC (void);
// 0x00000078 System.Void ModularBillBoard::RollOutTheBackgroundForContent()
extern void ModularBillBoard_RollOutTheBackgroundForContent_m2C8A18BF0D68113A6DB6F9AAEC8C4E594E30C251 (void);
// 0x00000079 System.Void ModularBillBoard::SwitchedToOpen()
extern void ModularBillBoard_SwitchedToOpen_m840CA75958E7CA851E582EDF57625E0EE5902DE3 (void);
// 0x0000007A System.Void ModularBillBoard::SwitchToClosed()
extern void ModularBillBoard_SwitchToClosed_mBA4CA0822C9BEA0707399D099A2AD8A257D7F7DD (void);
// 0x0000007B System.Void ModularBillBoard::SwitchtoOpening()
extern void ModularBillBoard_SwitchtoOpening_m581BAED245A45CC90E55E1CF1F81F7052728353B (void);
// 0x0000007C System.Void ModularBillBoard::.ctor()
extern void ModularBillBoard__ctor_m655C0218C32BB506CA83138B9950FB10BDE3F20E (void);
// 0x0000007D System.Void NavMeshManager::Awake()
extern void NavMeshManager_Awake_m572AF86AE9D183828B704AFB063947AFE2E1A33D (void);
// 0x0000007E System.Void NavMeshManager::Update()
extern void NavMeshManager_Update_m19ADEB67446A3CB8D703ADE094F9784F03B8CC29 (void);
// 0x0000007F System.Void NavMeshManager::UpdateNavigationAgentPosition()
extern void NavMeshManager_UpdateNavigationAgentPosition_m2669F63A560D8C05D159B5DB40EEDE5E4C14E96E (void);
// 0x00000080 System.Void NavMeshManager::UpdateNavigationLineVisibility()
extern void NavMeshManager_UpdateNavigationLineVisibility_m963E721EEBDB22D45AEE441EFC107ECC544286D3 (void);
// 0x00000081 System.Void NavMeshManager::UpdateNavigationLinePath()
extern void NavMeshManager_UpdateNavigationLinePath_mD85F9A3688BD7E3CD7E1113A0FE659DC0FF8E3C4 (void);
// 0x00000082 System.Void NavMeshManager::NavigateTo(UnityEngine.Transform)
extern void NavMeshManager_NavigateTo_m177646E471BED2180F6F73E235CFD10A31DE6979 (void);
// 0x00000083 System.Void NavMeshManager::DrawPath()
extern void NavMeshManager_DrawPath_m33F350A8F8CEEA848B23E2BDACF492A02940B18C (void);
// 0x00000084 System.Void NavMeshManager::OnAreaTargetFound()
extern void NavMeshManager_OnAreaTargetFound_m5BFF3832377D807455AEE6022D925A02C58DE0D1 (void);
// 0x00000085 System.Void NavMeshManager::OnAreaTargetLost()
extern void NavMeshManager_OnAreaTargetLost_m48BF4C028ADDCBDE3B5A01688AA300D6349E23EA (void);
// 0x00000086 System.Void NavMeshManager::.ctor()
extern void NavMeshManager__ctor_mED396FC7C0CA8BAC4B9BE56ADF0B63C27C252D1B (void);
// 0x00000087 System.Void TextValueDisplay::Update()
extern void TextValueDisplay_Update_m9E6904EBEC5C5D11DB0CF4A732F6E1701F2D3A44 (void);
// 0x00000088 System.Void TextValueDisplay::.ctor()
extern void TextValueDisplay__ctor_m8585AF80692B14E32599F26337623CAEAD3908A0 (void);
// 0x00000089 System.Boolean Astronaut::get_IsDrilling()
extern void Astronaut_get_IsDrilling_m8681ACF01127B063C1748434DB35FAF31C0AFDC5 (void);
// 0x0000008A System.Void Astronaut::set_IsDrilling(System.Boolean)
extern void Astronaut_set_IsDrilling_m9B7987AFBB603AF25D2693552F1DDEDB7E81F0A7 (void);
// 0x0000008B System.Boolean Astronaut::get_IsWaving()
extern void Astronaut_get_IsWaving_m89BC6E6FA3F338FB6EAEF791AF486479B9E064F8 (void);
// 0x0000008C System.Void Astronaut::set_IsWaving(System.Boolean)
extern void Astronaut_set_IsWaving_m3C0802E1C95FB32F112ACD0459D0C711ED6FD27E (void);
// 0x0000008D System.Void Astronaut::OnEnter()
extern void Astronaut_OnEnter_m9334E905B1FDE61F91B027A2B26260584E0CDE53 (void);
// 0x0000008E System.Void Astronaut::OnExit()
extern void Astronaut_OnExit_m8EE626F5CF73E421807AB8C500DB1A62FF36DC50 (void);
// 0x0000008F System.Void Astronaut::StartDrilling()
extern void Astronaut_StartDrilling_m1D7371F76FE791BD8595F6A389DCCFDE87163E9F (void);
// 0x00000090 System.Void Astronaut::SetAnimationIsDrillingToTrue()
extern void Astronaut_SetAnimationIsDrillingToTrue_m01956B887C352F9E7F0FFC04482444AC095711E0 (void);
// 0x00000091 System.Void Astronaut::SetAnimationIsDrillingToFalse()
extern void Astronaut_SetAnimationIsDrillingToFalse_m23CFBA9BA41D33A6AE8667A3BF6975D5AAE6EE4F (void);
// 0x00000092 System.Void Astronaut::SetAnimationDrillEffectOn()
extern void Astronaut_SetAnimationDrillEffectOn_mAAF397F3F95A6E26CD77C21BEC0248BA2C78815F (void);
// 0x00000093 System.Void Astronaut::SetAnimationDrillEffectOff()
extern void Astronaut_SetAnimationDrillEffectOff_m228975142FA110BCE22046BA73A2E5C5315EE0CA (void);
// 0x00000094 System.Void Astronaut::SetDrillEffect(System.Boolean)
extern void Astronaut_SetDrillEffect_m9A8A0C7B69B3C0B6161FE5BC5118102DB761AAE3 (void);
// 0x00000095 System.Void Astronaut::SetAnimationWavingOff()
extern void Astronaut_SetAnimationWavingOff_mCEBEC5836AA4A4251E47B78DA4F0A4AB1AE878EA (void);
// 0x00000096 System.Void Astronaut::HandleVirtualButtonPressed()
extern void Astronaut_HandleVirtualButtonPressed_mACF89E79E8DDE9139C2748B561CF4DCF64831C15 (void);
// 0x00000097 System.Void Astronaut::HandleVirtualButtonReleased()
extern void Astronaut_HandleVirtualButtonReleased_m5FB053DDD93BE5EDACA44256A0A5D6EE8B3ADACA (void);
// 0x00000098 System.Void Astronaut::.ctor()
extern void Astronaut__ctor_m6381E33DD2D1F7C1B82D199D71CFE8F88DD715C5 (void);
// 0x00000099 System.Void Augmentation::Start()
extern void Augmentation_Start_mE21DA1CB62A692FBC547E844E90B8B0F5A0DFC7D (void);
// 0x0000009A System.Void Augmentation::Disable()
extern void Augmentation_Disable_mF0637A797AB77E08890D41795935DA4F9634D01A (void);
// 0x0000009B System.Void Augmentation::Restore()
extern void Augmentation_Restore_mF31863F131FFA7BF7FAB136091EBC01CBB1BFF58 (void);
// 0x0000009C System.Void Augmentation::OnEnter()
extern void Augmentation_OnEnter_m91FE2DA443BBF99ED71E7A983D5CE3091992ADC8 (void);
// 0x0000009D System.Void Augmentation::OnExit()
extern void Augmentation_OnExit_m0EB521F5F38422F6D311FBBB1AA6FF1B79D1099E (void);
// 0x0000009E System.Void Augmentation::SetRenderersEnabled(System.Boolean)
extern void Augmentation_SetRenderersEnabled_mD84974A8BA1DF952CB4ADD5FBD26E328C915D31B (void);
// 0x0000009F System.Void Augmentation::SetCollidersEnabled(System.Boolean)
extern void Augmentation_SetCollidersEnabled_mB78B15A3950D165AA5237C6848A61EEC5A1AD085 (void);
// 0x000000A0 System.Collections.IEnumerator Augmentation::WaitForThen(System.Single,System.Action)
extern void Augmentation_WaitForThen_m1BD6A6A39C07EECCB619A60F345F0427D09BCC67 (void);
// 0x000000A1 System.Void Augmentation::.ctor()
extern void Augmentation__ctor_m65729D3BDEC22EDE0744487ACB99272455CB7209 (void);
// 0x000000A2 System.Void Augmentation/<WaitForThen>d__11::.ctor(System.Int32)
extern void U3CWaitForThenU3Ed__11__ctor_m2E48FF2C72A73B696E1ED0E73CF173D329DAC524 (void);
// 0x000000A3 System.Void Augmentation/<WaitForThen>d__11::System.IDisposable.Dispose()
extern void U3CWaitForThenU3Ed__11_System_IDisposable_Dispose_m61EAA071D03DF569645AD71A898939A7AC884151 (void);
// 0x000000A4 System.Boolean Augmentation/<WaitForThen>d__11::MoveNext()
extern void U3CWaitForThenU3Ed__11_MoveNext_m1F815D7E86C3FFA9BD242074BF7B99FF48790EED (void);
// 0x000000A5 System.Object Augmentation/<WaitForThen>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForThenU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F89852EDAE0F5870DE4AAF6250600924D182936 (void);
// 0x000000A6 System.Void Augmentation/<WaitForThen>d__11::System.Collections.IEnumerator.Reset()
extern void U3CWaitForThenU3Ed__11_System_Collections_IEnumerator_Reset_m3A7E75B662E6744FE181FDACFD5E1919BB7F89F1 (void);
// 0x000000A7 System.Object Augmentation/<WaitForThen>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForThenU3Ed__11_System_Collections_IEnumerator_get_Current_m034DEB3C929740030998E473CA9EAF3B854E57D2 (void);
// 0x000000A8 System.Void Augmentation/<>c::.cctor()
extern void U3CU3Ec__cctor_m03B8750B9DE87CA8CD928629CD0C8355E2681EDF (void);
// 0x000000A9 System.Void Augmentation/<>c::.ctor()
extern void U3CU3Ec__ctor_m8B99FF087D014F058BEBB560D41E900E37DAB5E5 (void);
// 0x000000AA System.Void Augmentation/<>c::<.ctor>b__12_0()
extern void U3CU3Ec_U3C_ctorU3Eb__12_0_m9A81B09C4931C8BC1B2D56F6182EF49BAF49F579 (void);
// 0x000000AB System.Void Augmentation/<>c::<.ctor>b__12_1()
extern void U3CU3Ec_U3C_ctorU3Eb__12_1_mA2D8A32634F2EB45B4E36503E69AD487784AA10E (void);
// 0x000000AC System.Boolean Drone::get_IsFacingObject()
extern void Drone_get_IsFacingObject_mC931DA342BAFC9D26837E2BBB397E11A170C2078 (void);
// 0x000000AD System.Void Drone::set_IsFacingObject(System.Boolean)
extern void Drone_set_IsFacingObject_m23AF7D4E57643F2D0F45CD19DD266A5F8BAF1E94 (void);
// 0x000000AE System.Boolean Drone::get_IsScanning()
extern void Drone_get_IsScanning_mAA9E7FE63ED620BB1FD9F6957E40D4A5269EB4FC (void);
// 0x000000AF System.Void Drone::set_IsScanning(System.Boolean)
extern void Drone_set_IsScanning_m8E6A673E13CC28AA10E52BF8E29172F9AECAFD87 (void);
// 0x000000B0 System.Boolean Drone::get_IsShowingLaser()
extern void Drone_get_IsShowingLaser_mDC01AD879FC172721092DCEB983CD11CE5D8FFFB (void);
// 0x000000B1 System.Void Drone::set_IsShowingLaser(System.Boolean)
extern void Drone_set_IsShowingLaser_mA28697DFFAED4146DF9794DF8BCE822B5B3D351E (void);
// 0x000000B2 System.Void Drone::OnEnter()
extern void Drone_OnEnter_m6FCD958CAF0D5F5029299E0DC4D84A05A6C4B53F (void);
// 0x000000B3 System.Void Drone::OnExit()
extern void Drone_OnExit_m3AF8BFA9874A16BB99DA104BE4FBCF9A30D9E363 (void);
// 0x000000B4 System.Void Drone::SetAnimationScanning(System.Boolean)
extern void Drone_SetAnimationScanning_mCD4DD63519770001B02C9BA6602D836710487A99 (void);
// 0x000000B5 System.Void Drone::HandleVirtualButtonPressed()
extern void Drone_HandleVirtualButtonPressed_mA7A54D1E9D530FAA55D5A111186D92C55A5052AF (void);
// 0x000000B6 System.Void Drone::HandleVirtualButtonReleased()
extern void Drone_HandleVirtualButtonReleased_m3D28839860EBADF83E25D8C0874BA79233541EC4 (void);
// 0x000000B7 System.Void Drone::.ctor()
extern void Drone__ctor_mB511C14B4D1BB1DF23D9150F7D92C38AC634A0A4 (void);
// 0x000000B8 System.Void Fissure::HandleVirtualButtonPressed()
extern void Fissure_HandleVirtualButtonPressed_m8055CCE9E3FB1BBA953D6FF16E8FFB6DDB00804D (void);
// 0x000000B9 System.Void Fissure::HandleVirtualButtonReleased()
extern void Fissure_HandleVirtualButtonReleased_mE4EDF888C585BA3939B60C96BBAFDF1C02A1293C (void);
// 0x000000BA System.Void Fissure::.ctor()
extern void Fissure__ctor_m86A508DCECEE318ED08782B29B0A1D667F222D8B (void);
// 0x000000BB System.Boolean OxygenTank::get_IsDetailOn()
extern void OxygenTank_get_IsDetailOn_m0B731BFA35474DE897F55138384D96914CFEB1FB (void);
// 0x000000BC System.Void OxygenTank::set_IsDetailOn(System.Boolean)
extern void OxygenTank_set_IsDetailOn_mD22770D8C5A3623469201F270704D75AF634CA10 (void);
// 0x000000BD System.Void OxygenTank::OnEnter()
extern void OxygenTank_OnEnter_mBF99C18A977D4304A1D7E5828B65E95A0BFE9729 (void);
// 0x000000BE System.Void OxygenTank::ShowDetail()
extern void OxygenTank_ShowDetail_m182BCE68716F6EE4F5C2BCBB8BF30EE8C922B3CA (void);
// 0x000000BF System.Void OxygenTank::HideDetail()
extern void OxygenTank_HideDetail_m324E9F3606C49C51C4AAB4B5BBE09FA9D8B762D3 (void);
// 0x000000C0 System.Void OxygenTank::HandleVirtualButtonPressed()
extern void OxygenTank_HandleVirtualButtonPressed_m828C2E5A47854E454C2B0798A6C1E8791A9CAC0F (void);
// 0x000000C1 System.Void OxygenTank::HandleVirtualButtonReleased()
extern void OxygenTank_HandleVirtualButtonReleased_mD48AA97D29B7B105CFFB2B296853EC2CD356BF26 (void);
// 0x000000C2 System.Void OxygenTank::DoEnter()
extern void OxygenTank_DoEnter_mB30489B4FBDAE032AACCA24E2BBC0728CAEDD457 (void);
// 0x000000C3 System.Void OxygenTank::.ctor()
extern void OxygenTank__ctor_mE4D3317DDD1F66E6A5C719E9AAA54A75B757B3C0 (void);
// 0x000000C4 System.Void RockPileController::Awake()
extern void RockPileController_Awake_mFD3086507F05B526ACA02F646297D83085520864 (void);
// 0x000000C5 System.Void RockPileController::FadeOut()
extern void RockPileController_FadeOut_mEC6C0E5327EFF731962A6CBE07508F13551F29C8 (void);
// 0x000000C6 System.Void RockPileController::FadeIn()
extern void RockPileController_FadeIn_mE4EEE6FC62DFA4EAA579B8FAA3D022E46AD863B6 (void);
// 0x000000C7 System.Void RockPileController::.ctor()
extern void RockPileController__ctor_m9B16763B7996063BBA9832E6E69F5CD948F094E6 (void);
// 0x000000C8 System.Void DrillController::Update()
extern void DrillController_Update_m29D48B285DA0B4B2F6FD5B81A6B6A341E653420E (void);
// 0x000000C9 System.Void DrillController::.ctor()
extern void DrillController__ctor_mE6370375429CDE5CCF094DD7467617D034FA294D (void);
// 0x000000CA System.Void FadeObject::Awake()
extern void FadeObject_Awake_m48B97186BE828D1E6D1B53F05F13222829BBBA66 (void);
// 0x000000CB System.Void FadeObject::Update()
extern void FadeObject_Update_mD9640FC10774C7F9265E17A5586E72022755C2E9 (void);
// 0x000000CC System.Void FadeObject::SetInitialOpacity(System.Single)
extern void FadeObject_SetInitialOpacity_m5AB1CFD00F9C7A631702A59E84C3004B48777216 (void);
// 0x000000CD System.Void FadeObject::SetOpacity(System.Single)
extern void FadeObject_SetOpacity_m5FCB231B8523F7DA33C6AEACF5DA68DC03058FDF (void);
// 0x000000CE System.Void FadeObject::SetOpaque()
extern void FadeObject_SetOpaque_m6FAEAF02257A8EEFBD68ECF10C4CAA44D49785E5 (void);
// 0x000000CF System.Void FadeObject::SetTransparent()
extern void FadeObject_SetTransparent_mCDFA744B7E4341E165A1624F63DF155FDC8AF9C8 (void);
// 0x000000D0 System.Void FadeObject::.ctor()
extern void FadeObject__ctor_mF46268F63500A9F21A4AF3270981156B982EE72D (void);
// 0x000000D1 System.Void AstronautStateMachineBehaviour::DoStateEvent(UnityEngine.Animator,System.String)
extern void AstronautStateMachineBehaviour_DoStateEvent_m2406CCA2F1E4A0FA0D3E3A28B363A4AA12C2362B (void);
// 0x000000D2 System.Type AstronautStateMachineBehaviour::GetTargetType()
extern void AstronautStateMachineBehaviour_GetTargetType_mA155B139225EB13E1027AB95BEED492AEE4D0B93 (void);
// 0x000000D3 System.Void AstronautStateMachineBehaviour::.ctor()
extern void AstronautStateMachineBehaviour__ctor_m7EE3849128D7FC2AEECFD8EC5E879453935E962A (void);
// 0x000000D4 System.Void AugmentationStateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void AugmentationStateMachineBehaviour_OnStateEnter_m4DDEA8100084FF80736A7770FB78B5F9D4CCFBFE (void);
// 0x000000D5 System.Void AugmentationStateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void AugmentationStateMachineBehaviour_OnStateExit_mD3B2CD683F83B05D9310FB31C6B9A2270BAF2412 (void);
// 0x000000D6 System.Void AugmentationStateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void AugmentationStateMachineBehaviour_OnStateUpdate_m07AA50CAAB9EFAA102DB9EADACACC4E253EE4C5A (void);
// 0x000000D7 System.Void AugmentationStateMachineBehaviour::DoStateEvent(UnityEngine.Animator,System.String)
// 0x000000D8 System.Type AugmentationStateMachineBehaviour::GetTargetType()
// 0x000000D9 System.Action`1<T> AugmentationStateMachineBehaviour::GetMethod(T,System.String)
// 0x000000DA System.Void AugmentationStateMachineBehaviour::AddDelegateToCache(System.Action`1<T>,System.String)
// 0x000000DB System.Void AugmentationStateMachineBehaviour::.ctor()
extern void AugmentationStateMachineBehaviour__ctor_m2ACCD394E19EE3662418BF41C062FA4C06AE48B3 (void);
// 0x000000DC System.Void UIController::UygulamayiKapat()
extern void UIController_UygulamayiKapat_mEF397A9C61D824E0A899375572BCD8FDBE2F6FFF (void);
// 0x000000DD System.Void UIController::Update()
extern void UIController_Update_m8F538932F53B831D5B82191902ADADACD32B108B (void);
// 0x000000DE System.Void UIController::.ctor()
extern void UIController__ctor_mFF218DBC8CCEFE36AAC295D2376501658CD8B7A2 (void);
// 0x000000DF System.String VuforiaLicense::GetLicenseKey()
extern void VuforiaLicense_GetLicenseKey_m8E6E03709CDB6968425A93FB9D016DA6AFC6C91B (void);
// 0x000000E0 System.Void VuforiaLicense::.ctor()
extern void VuforiaLicense__ctor_m094B92B3323DFFD79ADB696474D5D158C95E291B (void);
static Il2CppMethodPointer s_methodPointers[224] = 
{
	Links_Start_mD4C7680CC2CDF8EAE0F3C3236D14989961362C90,
	Links_Update_m391FF2C1305BC193326E0568C941DB64E9C724D1,
	Links__ctor_mC64AE52DDDF32F0469AB2C82DD86822A5C6B8492,
	AssetBundleAugmenter_Start_m1F6B923C0735972FEC73227A85F3F92346B4EDF3,
	AssetBundleAugmenter_DownloadAndCache_m3A3DD8DCAB3633C590F5E0F63BC5F7C004B2D569,
	AssetBundleAugmenter_LoadAssetToTarget_mCE61E79DDCA954A8A4B452146E5D9156252E9972,
	AssetBundleAugmenter__ctor_m5733419DC571A56833902E11DB57E3E2CC11EE5D,
	U3CDownloadAndCacheU3Ed__6__ctor_m6117947E915B1F42CA8DEECBB6264AEC2B859415,
	U3CDownloadAndCacheU3Ed__6_System_IDisposable_Dispose_mC996CA3B88875585B4EAE00D4F3F71042F00EC12,
	U3CDownloadAndCacheU3Ed__6_MoveNext_m3E7DBD345B248B772991DCAED65E48BF043450A7,
	U3CDownloadAndCacheU3Ed__6_U3CU3Em__Finally1_m9A8FE3FA2D93847E735E89861E36476B9C77798E,
	U3CDownloadAndCacheU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m847B435C54A1528CB7F3D8F84E34ED64C3FD3BC1,
	U3CDownloadAndCacheU3Ed__6_System_Collections_IEnumerator_Reset_m5856F943253ABABA33AEF5EEBDCDBC7B6BC3F183,
	U3CDownloadAndCacheU3Ed__6_System_Collections_IEnumerator_get_Current_m2311484CD8028150A0EB84996A6CA04AB2B9F842,
	ButtonCreator_Start_m5BBDF3A30EF06C7B5FED8CE144882E641D5B3F38,
	ButtonCreator_buttonCreate_mD377EE34982441845258D1E28073FFA606AF3651,
	ButtonCreator_CreateButton_m8A8A11C85AAC14E8F581AFC29CA642F1661643F6,
	ButtonCreator_DownloadImage_m2C566381164718D77627D41EE907402D95C3247E,
	ButtonCreator__ctor_mC113A0EAB5109E7FCC6A5AC1BAAD2713C925EFFF,
	U3CDownloadImageU3Ed__9__ctor_m36CE121F54A66D653BBAFE86C8C3F36C3E8A9A70,
	U3CDownloadImageU3Ed__9_System_IDisposable_Dispose_mA7F09EFCD49275A39C8C430DFA64B0587992B1A8,
	U3CDownloadImageU3Ed__9_MoveNext_mCF8AF6455F99CB2C2028601DCF46BA989B720A2B,
	U3CDownloadImageU3Ed__9_U3CU3Em__Finally1_mA2E26617BD780516F14B700E3826A4FA9A32714E,
	U3CDownloadImageU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A23B36453D204DE6E28468AC35FE64A1E9FA052,
	U3CDownloadImageU3Ed__9_System_Collections_IEnumerator_Reset_m9E50E5F39F3DB7D1AF17323089D6BA3547BB4C69,
	U3CDownloadImageU3Ed__9_System_Collections_IEnumerator_get_Current_m9673B80BF60BB0331A93DE80F2CDB40B6D701811,
	Deneme_Start_m4149F1251FF4C18042F5BC5C1979EA949A36431C,
	Deneme_DownloadAssetBundle_m61277F0223991C310B1A91146A816036B1F7989C,
	Deneme_DownloadImage_m3C3B6051E8EA44F59084A49F358C69CAEAD9DAD2,
	Deneme__ctor_m6E4CE9125326146DD6DA01F856D9DBC6454146D8,
	Deneme_U3CStartU3Eb__5_0_mE1392A6512F3BBD07348E1959C379627CC2D0977,
	U3CDownloadAssetBundleU3Ed__6__ctor_m0AAEBB63E5BF0C9176ED992804997B15008AB41E,
	U3CDownloadAssetBundleU3Ed__6_System_IDisposable_Dispose_m4A6613135BA27F9A2C95943C0D4F777A4A565E71,
	U3CDownloadAssetBundleU3Ed__6_MoveNext_m555C5DAAE4653FBA530FC0E12233544E70DF2F4E,
	U3CDownloadAssetBundleU3Ed__6_U3CU3Em__Finally1_m54954E626FF2067E9489377938C29BFC5374E0E0,
	U3CDownloadAssetBundleU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFBB9345A012BBD02BEA9B3804A3CBA945C01323A,
	U3CDownloadAssetBundleU3Ed__6_System_Collections_IEnumerator_Reset_m6264A0BDFCCE071C6E0FB498626E69E0AFA7024B,
	U3CDownloadAssetBundleU3Ed__6_System_Collections_IEnumerator_get_Current_m8C70CD0FA762D9D639AA5E5E255884CA8D569483,
	U3CDownloadImageU3Ed__8__ctor_mB44D47E2F1D9CE14709A6E2FC37502B862B19709,
	U3CDownloadImageU3Ed__8_System_IDisposable_Dispose_mBFC39A7BC08FE0FF1232DB5AB415E7205715ED6D,
	U3CDownloadImageU3Ed__8_MoveNext_m84283DF82949E47A343C097263DDDE1B05E17BF1,
	U3CDownloadImageU3Ed__8_U3CU3Em__Finally1_m4A5C9B877623A12C508306DA2344C2327FA87039,
	U3CDownloadImageU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC6B0457D1A21FD2CCF37E3603BE87EC453D1907D,
	U3CDownloadImageU3Ed__8_System_Collections_IEnumerator_Reset_m90D8BD962F9F5DA68C04CBE431C48BF44B3A1A52,
	U3CDownloadImageU3Ed__8_System_Collections_IEnumerator_get_Current_m2E510EB326C87CF989638CC12353CF91DB59CCAE,
	LoadAssetBundles_Start_m6BD605E2E3C7770A173A76BF286A393937FAFD51,
	LoadAssetBundles_GetAssetBundle_m27FE7E153495443D3CBFF03C2037E3C8A0B606EA,
	LoadAssetBundles__ctor_mA525E61196A0B3518AE50EC15FEFC8A24D9A5FFC,
	U3CGetAssetBundleU3Ed__5__ctor_m6C200D07A3532CF63ACEF83DEDC7073232131D8C,
	U3CGetAssetBundleU3Ed__5_System_IDisposable_Dispose_m8AFB4975C4A0EE1518D06C7D78F79742DFF4C865,
	U3CGetAssetBundleU3Ed__5_MoveNext_m19C76EBD68162630E9D6609BD3FCF82BAB0A6F8E,
	U3CGetAssetBundleU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F1F4AEE1ED6DC08BD5E52424ED0F3A17A77961B,
	U3CGetAssetBundleU3Ed__5_System_Collections_IEnumerator_Reset_mEB1875F8A2F29AA0940F15481BE7B7E91BAAF49A,
	U3CGetAssetBundleU3Ed__5_System_Collections_IEnumerator_get_Current_m067C4E5B3A7DBBFDD8401C89FDEC9FFA81DABF28,
	CategoryCilckable_Start_m123AA288408CCDD2B831AFEFB8DA3449EA110E73,
	CategoryCilckable_RotationAdjust_m89306E69A8213779124D3B3423112EDB2678A44B,
	CategoryCilckable_RotationKoltuklar_m505246A1847DEFBD95B548B5DDB7A993AB937897,
	CategoryCilckable_BackButtonClicked_m6854EC5E894C0EC116DBBE9B49AC3F831E0137AA,
	CategoryCilckable_PanelsFalse_m831A4DD7153A691B8A9489F658455220C1139785,
	CategoryCilckable_PanelsTrue_mFA911DB576A59E98020E9D30BB5B458F0B20F5E1,
	CategoryCilckable_ActiveRecursively_m836E545FC59735F66741D7E9626D8DA4CC8E8103,
	CategoryCilckable_DisableRecursively_m18D2E2F5E36EAB927EDC75EC465E5E341E4E5E0E,
	CategoryCilckable_Update_m348D870B902E80F43B5794CE83B05BF93FB9A27D,
	CategoryCilckable_cube1_m5DD0E53766CC8454E5A459A1DD6B5F4AE53518CE,
	CategoryCilckable_cube2_mE9691BA98BD9C2EA9D6ACC5E65D77C57E6EA3FEF,
	CategoryCilckable_cube3_mCE82A1E9AD5360E2F709CC44FA50B0AE21177C97,
	CategoryCilckable_capsule1_m5AF1E18D1C830E6821ABA2802A78D54AE752A956,
	CategoryCilckable_capsule2_mAB256BB81EA214054418E0C405E712B979622719,
	CategoryCilckable_capsule3_mBC93C6CD59AEAA8C1BB7A70C4AEF1EA751A28AD3,
	CategoryCilckable_sphere1_m174D78EC4006BEDB9A90177B5EB71A14DD572C18,
	CategoryCilckable_sphere2_m717745E43B20B987D8DDD470D718DA66CA00B21A,
	CategoryCilckable_sphere3_mFB12DABEBBFB2FC2A561038017FC7B523F8731CF,
	CategoryCilckable_ActivateObject_m58652FCB173787A3F492223FB512B36A415B3975,
	CategoryCilckable__ctor_m24CE01BCF364EFDE02CB5F23EA73838236422F29,
	scrollbarSize_Start_m62C051C9ABE5CAD9844046CDDE978BB2C9EBDC54,
	scrollbarSize_OnScrollbarValueChanged_mECAEC8E76FC4DB847D1E22D3FC11AD354834F3A1,
	scrollbarSize__ctor_m01A5917699D1AD00D04A76ECC53A4895A742D2FA,
	YorumlanmU131U15FHali_Start_m4D5356E1B248515B6E95AB3414F2271BF5124AD5,
	YorumlanmU131U15FHali_RotationAdjust_mB33D2A91FDA7DB401429020E3DBE081FFBD0DD37,
	YorumlanmU131U15FHali_BackButtonClicked_m3A61DBCCDC2193F35C96E9BF718753DAB5300AB3,
	YorumlanmU131U15FHali_PanelsFalse_mAA7C11C62A9AB61B66C000D31FA9EA81800C780E,
	YorumlanmU131U15FHali_PanelsTrue_m15E93F83A863C9EB855BA2C3E85441EC2CC75A35,
	YorumlanmU131U15FHali_ActiveRecursively_m53DA220D6265C919507064129672374BF3227337,
	YorumlanmU131U15FHali_DisableRecursively_mC6376D633508E9F1BEFE22E505602009401A5FFC,
	YorumlanmU131U15FHali_Update_m58E648EA369B40E147E5A70C6A7B120F92F4FAE9,
	YorumlanmU131U15FHali_cube1_m917ABD84792EA0444D00FDDCB1934DCD4A32A587,
	YorumlanmU131U15FHali_cube2_mBD96C16F2AB575192DE6AC81C0C1F6B72F7338AA,
	YorumlanmU131U15FHali_cube3_m54E677668CF1C84F3D7E4983E349356D4884DC72,
	YorumlanmU131U15FHali_capsule1_m5BF695F0F44282EE5167CD1E89B146C55970C214,
	YorumlanmU131U15FHali_capsule2_m21E69F37E998133B679FEF32CB6264149E752F22,
	YorumlanmU131U15FHali_capsule3_m53730054D544DC814AE4A6C997E11E6DA7768FE7,
	YorumlanmU131U15FHali_sphere1_mB9E1946380A6DA0D091BD53F11FB4310A2565C4F,
	YorumlanmU131U15FHali_sphere2_m2BEA3C31A7AA423ADDEF5E71F7299B38A96C814B,
	YorumlanmU131U15FHali_sphere3_m431AC9818E719D1B6CF700979231F264DDF58938,
	YorumlanmU131U15FHali_ActivateObject_m561AA5AF3666D7B7772CFA9B52DEB448C92AD0DD,
	YorumlanmU131U15FHali__ctor_mC20EFC3C38B7F9B412B62AA97661F9C94CD0A4BE,
	cubePanel_Start_mF4D22318826133A7F5144CDBA8B43532AC9C2767,
	cubePanel_Update_m67CC6A13A9A076DDE269F406B5C5ABD596C7DBF5,
	cubePanel__ctor_mD81FE00FE1883369EBFF34933BF6702C57FB0493,
	svipe_Menu_Update_m2FCF2205C07F975B85FFE47E0CC3B1F9C922EB69,
	svipe_Menu__ctor_mF15CE4C7C2C296892B4FCBB78366D8D8ACBCD89E,
	GaugeValueDisplay_Update_mEC1F962782760DF946E33D0AFA05AF5D8F467770,
	GaugeValueDisplay_SetPointerToValue_mB369A0554BCA47B1393B75995B07D58545E2CBE9,
	GaugeValueDisplay__ctor_mC7EE4BCF4A83A2D7BA95A6048B0565CF74D42EF5,
	ImageValueDisplay_OnValueChanged_m6E2BAA209A70C46335B21D8D4C40ADA3867FAC99,
	ImageValueDisplay_Update_mF4F8CDEE4940DFFF629055A3CB66BD29ABA9F2D4,
	ImageValueDisplay__ctor_m4EA468E68D0747990C34D7B6B9EA0E015749D320,
	LineRendererPatternShifter_Awake_mCD0B249349AB670D4EA8F855550ADE69FF1EAE01,
	LineRendererPatternShifter_Update_mA2E5E6FEDAB401D1EA721C15B61FE7E565898E3B,
	LineRendererPatternShifter__ctor_mF6FEEA94B52D6CBA258215CA641CE3C2E5D5BA68,
	ModularBillBoard_Awake_m6757BEAA47DDE2958831D0BDDF21CA62FB412483,
	ModularBillBoard_CalculateDerivedValues_m304586089018E377063F464282F6F742413D23A4,
	ModularBillBoard_CalculateMaskSize_m205ED3891CC3D928E62D739C5F22FD3318B3A725,
	ModularBillBoard_CalculateLandmarkPosition_mF18B8B744648A58A46029E7EF14BDA8A7AAE1A88,
	ModularBillBoard_Update_mE65208BAC3B6E24DB99924E5EE2733F9E9F7FAD8,
	ModularBillBoard_Opening_m89F36DB21F8B71B0248CDA19FEEDDF920A75BFDD,
	ModularBillBoard_Closed_m5F5017F6FA1664B16102CE6B33B2E178A7A4231B,
	ModularBillBoard_FullyOpened_mC5A097D00A64AACA7E211727111B547BB1B5E56F,
	ModularBillBoard_TurnContentVisible_mE6D2A6CB926227C3E05C1682760D1EE7494750CC,
	ModularBillBoard_RollOutTheBackgroundForContent_m2C8A18BF0D68113A6DB6F9AAEC8C4E594E30C251,
	ModularBillBoard_SwitchedToOpen_m840CA75958E7CA851E582EDF57625E0EE5902DE3,
	ModularBillBoard_SwitchToClosed_mBA4CA0822C9BEA0707399D099A2AD8A257D7F7DD,
	ModularBillBoard_SwitchtoOpening_m581BAED245A45CC90E55E1CF1F81F7052728353B,
	ModularBillBoard__ctor_m655C0218C32BB506CA83138B9950FB10BDE3F20E,
	NavMeshManager_Awake_m572AF86AE9D183828B704AFB063947AFE2E1A33D,
	NavMeshManager_Update_m19ADEB67446A3CB8D703ADE094F9784F03B8CC29,
	NavMeshManager_UpdateNavigationAgentPosition_m2669F63A560D8C05D159B5DB40EEDE5E4C14E96E,
	NavMeshManager_UpdateNavigationLineVisibility_m963E721EEBDB22D45AEE441EFC107ECC544286D3,
	NavMeshManager_UpdateNavigationLinePath_mD85F9A3688BD7E3CD7E1113A0FE659DC0FF8E3C4,
	NavMeshManager_NavigateTo_m177646E471BED2180F6F73E235CFD10A31DE6979,
	NavMeshManager_DrawPath_m33F350A8F8CEEA848B23E2BDACF492A02940B18C,
	NavMeshManager_OnAreaTargetFound_m5BFF3832377D807455AEE6022D925A02C58DE0D1,
	NavMeshManager_OnAreaTargetLost_m48BF4C028ADDCBDE3B5A01688AA300D6349E23EA,
	NavMeshManager__ctor_mED396FC7C0CA8BAC4B9BE56ADF0B63C27C252D1B,
	TextValueDisplay_Update_m9E6904EBEC5C5D11DB0CF4A732F6E1701F2D3A44,
	TextValueDisplay__ctor_m8585AF80692B14E32599F26337623CAEAD3908A0,
	Astronaut_get_IsDrilling_m8681ACF01127B063C1748434DB35FAF31C0AFDC5,
	Astronaut_set_IsDrilling_m9B7987AFBB603AF25D2693552F1DDEDB7E81F0A7,
	Astronaut_get_IsWaving_m89BC6E6FA3F338FB6EAEF791AF486479B9E064F8,
	Astronaut_set_IsWaving_m3C0802E1C95FB32F112ACD0459D0C711ED6FD27E,
	Astronaut_OnEnter_m9334E905B1FDE61F91B027A2B26260584E0CDE53,
	Astronaut_OnExit_m8EE626F5CF73E421807AB8C500DB1A62FF36DC50,
	Astronaut_StartDrilling_m1D7371F76FE791BD8595F6A389DCCFDE87163E9F,
	Astronaut_SetAnimationIsDrillingToTrue_m01956B887C352F9E7F0FFC04482444AC095711E0,
	Astronaut_SetAnimationIsDrillingToFalse_m23CFBA9BA41D33A6AE8667A3BF6975D5AAE6EE4F,
	Astronaut_SetAnimationDrillEffectOn_mAAF397F3F95A6E26CD77C21BEC0248BA2C78815F,
	Astronaut_SetAnimationDrillEffectOff_m228975142FA110BCE22046BA73A2E5C5315EE0CA,
	Astronaut_SetDrillEffect_m9A8A0C7B69B3C0B6161FE5BC5118102DB761AAE3,
	Astronaut_SetAnimationWavingOff_mCEBEC5836AA4A4251E47B78DA4F0A4AB1AE878EA,
	Astronaut_HandleVirtualButtonPressed_mACF89E79E8DDE9139C2748B561CF4DCF64831C15,
	Astronaut_HandleVirtualButtonReleased_m5FB053DDD93BE5EDACA44256A0A5D6EE8B3ADACA,
	Astronaut__ctor_m6381E33DD2D1F7C1B82D199D71CFE8F88DD715C5,
	Augmentation_Start_mE21DA1CB62A692FBC547E844E90B8B0F5A0DFC7D,
	Augmentation_Disable_mF0637A797AB77E08890D41795935DA4F9634D01A,
	Augmentation_Restore_mF31863F131FFA7BF7FAB136091EBC01CBB1BFF58,
	Augmentation_OnEnter_m91FE2DA443BBF99ED71E7A983D5CE3091992ADC8,
	Augmentation_OnExit_m0EB521F5F38422F6D311FBBB1AA6FF1B79D1099E,
	Augmentation_SetRenderersEnabled_mD84974A8BA1DF952CB4ADD5FBD26E328C915D31B,
	Augmentation_SetCollidersEnabled_mB78B15A3950D165AA5237C6848A61EEC5A1AD085,
	Augmentation_WaitForThen_m1BD6A6A39C07EECCB619A60F345F0427D09BCC67,
	Augmentation__ctor_m65729D3BDEC22EDE0744487ACB99272455CB7209,
	U3CWaitForThenU3Ed__11__ctor_m2E48FF2C72A73B696E1ED0E73CF173D329DAC524,
	U3CWaitForThenU3Ed__11_System_IDisposable_Dispose_m61EAA071D03DF569645AD71A898939A7AC884151,
	U3CWaitForThenU3Ed__11_MoveNext_m1F815D7E86C3FFA9BD242074BF7B99FF48790EED,
	U3CWaitForThenU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F89852EDAE0F5870DE4AAF6250600924D182936,
	U3CWaitForThenU3Ed__11_System_Collections_IEnumerator_Reset_m3A7E75B662E6744FE181FDACFD5E1919BB7F89F1,
	U3CWaitForThenU3Ed__11_System_Collections_IEnumerator_get_Current_m034DEB3C929740030998E473CA9EAF3B854E57D2,
	U3CU3Ec__cctor_m03B8750B9DE87CA8CD928629CD0C8355E2681EDF,
	U3CU3Ec__ctor_m8B99FF087D014F058BEBB560D41E900E37DAB5E5,
	U3CU3Ec_U3C_ctorU3Eb__12_0_m9A81B09C4931C8BC1B2D56F6182EF49BAF49F579,
	U3CU3Ec_U3C_ctorU3Eb__12_1_mA2D8A32634F2EB45B4E36503E69AD487784AA10E,
	Drone_get_IsFacingObject_mC931DA342BAFC9D26837E2BBB397E11A170C2078,
	Drone_set_IsFacingObject_m23AF7D4E57643F2D0F45CD19DD266A5F8BAF1E94,
	Drone_get_IsScanning_mAA9E7FE63ED620BB1FD9F6957E40D4A5269EB4FC,
	Drone_set_IsScanning_m8E6A673E13CC28AA10E52BF8E29172F9AECAFD87,
	Drone_get_IsShowingLaser_mDC01AD879FC172721092DCEB983CD11CE5D8FFFB,
	Drone_set_IsShowingLaser_mA28697DFFAED4146DF9794DF8BCE822B5B3D351E,
	Drone_OnEnter_m6FCD958CAF0D5F5029299E0DC4D84A05A6C4B53F,
	Drone_OnExit_m3AF8BFA9874A16BB99DA104BE4FBCF9A30D9E363,
	Drone_SetAnimationScanning_mCD4DD63519770001B02C9BA6602D836710487A99,
	Drone_HandleVirtualButtonPressed_mA7A54D1E9D530FAA55D5A111186D92C55A5052AF,
	Drone_HandleVirtualButtonReleased_m3D28839860EBADF83E25D8C0874BA79233541EC4,
	Drone__ctor_mB511C14B4D1BB1DF23D9150F7D92C38AC634A0A4,
	Fissure_HandleVirtualButtonPressed_m8055CCE9E3FB1BBA953D6FF16E8FFB6DDB00804D,
	Fissure_HandleVirtualButtonReleased_mE4EDF888C585BA3939B60C96BBAFDF1C02A1293C,
	Fissure__ctor_m86A508DCECEE318ED08782B29B0A1D667F222D8B,
	OxygenTank_get_IsDetailOn_m0B731BFA35474DE897F55138384D96914CFEB1FB,
	OxygenTank_set_IsDetailOn_mD22770D8C5A3623469201F270704D75AF634CA10,
	OxygenTank_OnEnter_mBF99C18A977D4304A1D7E5828B65E95A0BFE9729,
	OxygenTank_ShowDetail_m182BCE68716F6EE4F5C2BCBB8BF30EE8C922B3CA,
	OxygenTank_HideDetail_m324E9F3606C49C51C4AAB4B5BBE09FA9D8B762D3,
	OxygenTank_HandleVirtualButtonPressed_m828C2E5A47854E454C2B0798A6C1E8791A9CAC0F,
	OxygenTank_HandleVirtualButtonReleased_mD48AA97D29B7B105CFFB2B296853EC2CD356BF26,
	OxygenTank_DoEnter_mB30489B4FBDAE032AACCA24E2BBC0728CAEDD457,
	OxygenTank__ctor_mE4D3317DDD1F66E6A5C719E9AAA54A75B757B3C0,
	RockPileController_Awake_mFD3086507F05B526ACA02F646297D83085520864,
	RockPileController_FadeOut_mEC6C0E5327EFF731962A6CBE07508F13551F29C8,
	RockPileController_FadeIn_mE4EEE6FC62DFA4EAA579B8FAA3D022E46AD863B6,
	RockPileController__ctor_m9B16763B7996063BBA9832E6E69F5CD948F094E6,
	DrillController_Update_m29D48B285DA0B4B2F6FD5B81A6B6A341E653420E,
	DrillController__ctor_mE6370375429CDE5CCF094DD7467617D034FA294D,
	FadeObject_Awake_m48B97186BE828D1E6D1B53F05F13222829BBBA66,
	FadeObject_Update_mD9640FC10774C7F9265E17A5586E72022755C2E9,
	FadeObject_SetInitialOpacity_m5AB1CFD00F9C7A631702A59E84C3004B48777216,
	FadeObject_SetOpacity_m5FCB231B8523F7DA33C6AEACF5DA68DC03058FDF,
	FadeObject_SetOpaque_m6FAEAF02257A8EEFBD68ECF10C4CAA44D49785E5,
	FadeObject_SetTransparent_mCDFA744B7E4341E165A1624F63DF155FDC8AF9C8,
	FadeObject__ctor_mF46268F63500A9F21A4AF3270981156B982EE72D,
	AstronautStateMachineBehaviour_DoStateEvent_m2406CCA2F1E4A0FA0D3E3A28B363A4AA12C2362B,
	AstronautStateMachineBehaviour_GetTargetType_mA155B139225EB13E1027AB95BEED492AEE4D0B93,
	AstronautStateMachineBehaviour__ctor_m7EE3849128D7FC2AEECFD8EC5E879453935E962A,
	AugmentationStateMachineBehaviour_OnStateEnter_m4DDEA8100084FF80736A7770FB78B5F9D4CCFBFE,
	AugmentationStateMachineBehaviour_OnStateExit_mD3B2CD683F83B05D9310FB31C6B9A2270BAF2412,
	AugmentationStateMachineBehaviour_OnStateUpdate_m07AA50CAAB9EFAA102DB9EADACACC4E253EE4C5A,
	NULL,
	NULL,
	NULL,
	NULL,
	AugmentationStateMachineBehaviour__ctor_m2ACCD394E19EE3662418BF41C062FA4C06AE48B3,
	UIController_UygulamayiKapat_mEF397A9C61D824E0A899375572BCD8FDBE2F6FFF,
	UIController_Update_m8F538932F53B831D5B82191902ADADACD32B108B,
	UIController__ctor_mFF218DBC8CCEFE36AAC295D2376501658CD8B7A2,
	VuforiaLicense_GetLicenseKey_m8E6E03709CDB6968425A93FB9D016DA6AFC6C91B,
	VuforiaLicense__ctor_m094B92B3323DFFD79ADB696474D5D158C95E291B,
};
static const int32_t s_InvokerIndices[224] = 
{
	7252,
	7252,
	7252,
	7252,
	7114,
	7252,
	7252,
	5725,
	7252,
	7012,
	7252,
	7114,
	7252,
	7114,
	7252,
	10683,
	5754,
	2518,
	7252,
	5725,
	7252,
	7012,
	7252,
	7114,
	7252,
	7114,
	7252,
	5117,
	5117,
	7252,
	5754,
	5725,
	7252,
	7012,
	7252,
	7114,
	7252,
	7114,
	5725,
	7252,
	7012,
	7252,
	7114,
	7252,
	7114,
	7252,
	7114,
	7252,
	5725,
	7252,
	7012,
	7114,
	7252,
	7114,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	5754,
	5754,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	5725,
	7252,
	7252,
	5812,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	5754,
	5754,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	5725,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	5812,
	7252,
	5812,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	5812,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	5754,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7012,
	5650,
	7012,
	5650,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	5650,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	5650,
	5650,
	2524,
	7252,
	5725,
	7252,
	7012,
	7114,
	7252,
	7114,
	10889,
	7252,
	7252,
	7252,
	7012,
	5650,
	7012,
	5650,
	7012,
	5650,
	7252,
	7252,
	5650,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7012,
	5650,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	7252,
	5812,
	5812,
	7252,
	7252,
	7252,
	3206,
	7114,
	7252,
	1657,
	1657,
	1657,
	0,
	0,
	0,
	0,
	7252,
	7252,
	7252,
	7252,
	10847,
	7252,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x060000D9, { 0, 5 } },
	{ 0x060000DA, { 5, 1 } },
};
extern const uint32_t g_rgctx_T_tF2FFC1FE1E22978B287C4180AA619C8445C21561;
extern const uint32_t g_rgctx_Action_1_t941A209F0EAD3D442250E42DA96ACB71D86F68C7;
extern const uint32_t g_rgctx_T_tF2FFC1FE1E22978B287C4180AA619C8445C21561;
extern const uint32_t g_rgctx_Action_1_t941A209F0EAD3D442250E42DA96ACB71D86F68C7;
extern const uint32_t g_rgctx_AugmentationStateMachineBehaviour_AddDelegateToCache_TisT_tF2FFC1FE1E22978B287C4180AA619C8445C21561_m6AC33A0FC016AFEF883B256012168FCE102FCDC2;
extern const uint32_t g_rgctx_T_tF05547864915FE65145D1E5F9714695EAF22355C;
static const Il2CppRGCTXDefinition s_rgctxValues[6] = 
{
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tF2FFC1FE1E22978B287C4180AA619C8445C21561 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Action_1_t941A209F0EAD3D442250E42DA96ACB71D86F68C7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tF2FFC1FE1E22978B287C4180AA619C8445C21561 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_Action_1_t941A209F0EAD3D442250E42DA96ACB71D86F68C7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AugmentationStateMachineBehaviour_AddDelegateToCache_TisT_tF2FFC1FE1E22978B287C4180AA619C8445C21561_m6AC33A0FC016AFEF883B256012168FCE102FCDC2 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tF05547864915FE65145D1E5F9714695EAF22355C },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	224,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	6,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

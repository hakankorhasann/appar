﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* DocumentReference_DocumentSnapshotsHandler_m39CF98A69DDA6C278BA2586D1C383F42A3A62767_RuntimeMethod_var;
extern const RuntimeMethod* FirebaseFirestore_LoadBundleTaskProgressHandler_mA5D2729E0A63DCF17A0443A2A46AA394EDBD4AC5_RuntimeMethod_var;
extern const RuntimeMethod* FirebaseFirestore_SnapshotsInSyncHandler_m63B46E06E07A5AC034332B505171FB2779C7FB36_RuntimeMethod_var;
extern const RuntimeMethod* FirestoreExceptionHelper_SetPendingFirestoreException_m87C105F291E79CF2C83734D34986011FF88B8E0E_RuntimeMethod_var;
extern const RuntimeMethod* Future_QuerySnapshot_SWIG_CompletionDispatcher_mDE52F3C13385E02F9E4DDDBAE074D02C1B90EC39_RuntimeMethod_var;
extern const RuntimeMethod* Query_QuerySnapshotsHandler_mACCC5A5D3D2ECD5DCE9DEA25B771CB448DC68715_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingApplicationException_mBF137E9081B46089C8BD08A3FFEE1E0FE1F0E3A8_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentException_m927C534E2487873AF124759896433BDAC30405C2_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentNullException_m338ED5CC0C522534644D5024F1B68F1C09E9FE75_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m7661B82740D6603B2BF70398B38A72E05D4FA57B_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArithmeticException_mF99EDA71B878CF6361E9564B89784B3A561FFA52_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingDivideByZeroException_mCE9772E2D30699F715F2661C75C5EF14C086E9E8_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIOException_m12107E67FA15993E46B0E20B06151035CCE6E143_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m921D13D0A68FB4EBC4056309B8E3449B10747CEB_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidCastException_m9947EBEA346C986795F984A0BFEE5CF35F5B6D34_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidOperationException_m4BF2D51569FE111F005303CC1F1B4092A7543587_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingNullReferenceException_m0CAF64A4594D264026F68FB9A835A0BC1FADD791_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOutOfMemoryException_m514E19096CE25864CBF0CB58DF723C735A7667D9_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOverflowException_m7B3159CEF1FD67EB6B7B517C020441417888E1AC_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingSystemException_m011EB1DF3148DBBBC161AC80CFCBB47D888CB10D_RuntimeMethod_var;
extern const RuntimeMethod* SWIGStringHelper_CreateString_m0B36085212BD3538E5F889D7EB968E3AC83D5049_RuntimeMethod_var;



// 0x00000001 System.Int32 Firebase.Firestore.Blob::get_Length()
extern void Blob_get_Length_m41B38E18238097414A3A20FD3DC2156970A31502 (void);
// 0x00000002 Firebase.Firestore.Blob Firebase.Firestore.Blob::CopyFrom(System.Byte[])
extern void Blob_CopyFrom_mACD217581997CCAFFC1F81557B64D3541041BEFD (void);
// 0x00000003 System.Boolean Firebase.Firestore.Blob::Equals(System.Object)
extern void Blob_Equals_m4BAAC02C0A8F997F10D849409E37342FBCFD5A5C (void);
// 0x00000004 System.Int32 Firebase.Firestore.Blob::GetHashCode()
extern void Blob_GetHashCode_m228609F9AD65008385472F6A0B25061FDFD851E8 (void);
// 0x00000005 System.Boolean Firebase.Firestore.Blob::Equals(Firebase.Firestore.Blob)
extern void Blob_Equals_m56D099DEE926AAE8091860F91307FCD4FA48E50C (void);
// 0x00000006 System.String Firebase.Firestore.Blob::ToString()
extern void Blob_ToString_m3561726740B5378812B18356C8998B71F99A1298 (void);
// 0x00000007 System.Void Firebase.Firestore.Blob::.ctor(System.Byte[])
extern void Blob__ctor_m1F16308CEC0FA8E3447D18B2168D01921195A6AB (void);
// 0x00000008 System.Void Firebase.Firestore.DocumentReference::.ctor(Firebase.Firestore.DocumentReferenceProxy,Firebase.Firestore.FirebaseFirestore)
extern void DocumentReference__ctor_m4AE8003DE3DCFA995D3AD9EC075F6F0E5E149F71 (void);
// 0x00000009 System.Void Firebase.Firestore.DocumentReference::ClearCallbacksForOwner(Firebase.Firestore.FirebaseFirestore)
extern void DocumentReference_ClearCallbacksForOwner_m58119A59C76C72B6AD9B42E0E4343B818D4DE11D (void);
// 0x0000000A Firebase.Firestore.FirebaseFirestore Firebase.Firestore.DocumentReference::get_Firestore()
extern void DocumentReference_get_Firestore_m02AAEC879C3F4FBECAEC1C51B3055A8F366E07C8 (void);
// 0x0000000B System.String Firebase.Firestore.DocumentReference::get_Id()
extern void DocumentReference_get_Id_m4898D340E1899D3DB5BA535BF7E4DC91FD52D3A4 (void);
// 0x0000000C System.String Firebase.Firestore.DocumentReference::get_Path()
extern void DocumentReference_get_Path_m30463C4BD4AC09361F24384728395D3866C38540 (void);
// 0x0000000D System.Int32 Firebase.Firestore.DocumentReference::GetHashCode()
extern void DocumentReference_GetHashCode_mFB550B31375A70A50DD734BC3A3C140098D49645 (void);
// 0x0000000E System.Boolean Firebase.Firestore.DocumentReference::Equals(System.Object)
extern void DocumentReference_Equals_m05422A2E8C6192D5D6EB56A76519E0C4DFB4B9D7 (void);
// 0x0000000F System.Boolean Firebase.Firestore.DocumentReference::Equals(Firebase.Firestore.DocumentReference)
extern void DocumentReference_Equals_m4EF71730722449EEF4EF38FAE6C708204351C517 (void);
// 0x00000010 System.String Firebase.Firestore.DocumentReference::ToString()
extern void DocumentReference_ToString_mE03C724E5B90C0CBB7C9A8FF6F033C59913F3F03 (void);
// 0x00000011 System.Void Firebase.Firestore.DocumentReference::DocumentSnapshotsHandler(System.Int32,System.IntPtr,Firebase.Firestore.FirestoreError,System.String)
extern void DocumentReference_DocumentSnapshotsHandler_m39CF98A69DDA6C278BA2586D1C383F42A3A62767 (void);
// 0x00000012 System.Void Firebase.Firestore.DocumentReference::.cctor()
extern void DocumentReference__cctor_mF84880296598D73EEC05AC8A1E0A7C67B31D809F (void);
// 0x00000013 System.Void Firebase.Firestore.DocumentReference/ListenerDelegate::.ctor(System.Object,System.IntPtr)
extern void ListenerDelegate__ctor_mB86641D489EFB8C7171E204F923CF10A77F34E05 (void);
// 0x00000014 System.Void Firebase.Firestore.DocumentReference/ListenerDelegate::Invoke(System.Int32,System.IntPtr,Firebase.Firestore.FirestoreError,System.String)
extern void ListenerDelegate_Invoke_m95975769948175C8DD80808529D88C6EB0F9554B (void);
// 0x00000015 System.Void Firebase.Firestore.DocumentSnapshot::.ctor(Firebase.Firestore.DocumentSnapshotProxy,Firebase.Firestore.FirebaseFirestore)
extern void DocumentSnapshot__ctor_m8825DD7CC25AF8D5285A3004BD3A7D08C5C251EF (void);
// 0x00000016 Firebase.Firestore.DocumentReference Firebase.Firestore.DocumentSnapshot::get_Reference()
extern void DocumentSnapshot_get_Reference_m7E6A35C1DC9A04B68B6178BD1AC9B744BE39C09E (void);
// 0x00000017 System.Boolean Firebase.Firestore.DocumentSnapshot::get_Exists()
extern void DocumentSnapshot_get_Exists_m133BC1857E6AC60E909CE432A31D5148395AE7CD (void);
// 0x00000018 System.Collections.Generic.Dictionary`2<System.String,System.Object> Firebase.Firestore.DocumentSnapshot::ToDictionary(Firebase.Firestore.ServerTimestampBehavior)
extern void DocumentSnapshot_ToDictionary_m641E6A4F6AA49A9CC9F5A6676ECD0354D88FF63B (void);
// 0x00000019 T Firebase.Firestore.DocumentSnapshot::ConvertTo(Firebase.Firestore.ServerTimestampBehavior)
// 0x0000001A System.Boolean Firebase.Firestore.DocumentSnapshot::Equals(System.Object)
extern void DocumentSnapshot_Equals_m8EDBB1AF9F354FC5228E3B1030AA368C2CA20B59 (void);
// 0x0000001B System.Boolean Firebase.Firestore.DocumentSnapshot::Equals(Firebase.Firestore.DocumentSnapshot)
extern void DocumentSnapshot_Equals_m50FBE8FB535767C1A1E071DCE5D6810D501CD7A8 (void);
// 0x0000001C System.Int32 Firebase.Firestore.DocumentSnapshot::GetHashCode()
extern void DocumentSnapshot_GetHashCode_m886B204B52CEEEF8F81C77E60D470DF7F8875A50 (void);
// 0x0000001D System.Void Firebase.Firestore.FirebaseFirestore::.ctor(Firebase.Firestore.FirestoreProxy,Firebase.FirebaseApp)
extern void FirebaseFirestore__ctor_mD0B5A58E35F5B6B07CA3F55BD1E3D27A31867DD6 (void);
// 0x0000001E System.Void Firebase.Firestore.FirebaseFirestore::Finalize()
extern void FirebaseFirestore_Finalize_m85A0AC3229A58F747349F1DEF71FFBC1F7C86B7D (void);
// 0x0000001F System.Void Firebase.Firestore.FirebaseFirestore::OnAppDisposed(System.Object,System.EventArgs)
extern void FirebaseFirestore_OnAppDisposed_mF07E6E9B440BE1CE3C72B44908DBDE1BEEC1284E (void);
// 0x00000020 System.Void Firebase.Firestore.FirebaseFirestore::Dispose()
extern void FirebaseFirestore_Dispose_m105A981CFC95EEAA1A4C9FF9DB24B6C873A1D5F1 (void);
// 0x00000021 Firebase.FirebaseApp Firebase.Firestore.FirebaseFirestore::get_App()
extern void FirebaseFirestore_get_App_mDDB46762F9ADDD9CB6EB445108454670850DAC89 (void);
// 0x00000022 System.Void Firebase.Firestore.FirebaseFirestore::set_App(Firebase.FirebaseApp)
extern void FirebaseFirestore_set_App_mD51E0F0E17E24C561B1122BDB86E41B6ADB26198 (void);
// 0x00000023 Firebase.Firestore.FirebaseFirestore Firebase.Firestore.FirebaseFirestore::get_DefaultInstance()
extern void FirebaseFirestore_get_DefaultInstance_m16EACB91EA3C01689A2F7707DA288177D21A67CE (void);
// 0x00000024 Firebase.Firestore.FirebaseFirestore Firebase.Firestore.FirebaseFirestore::GetInstance(Firebase.FirebaseApp)
extern void FirebaseFirestore_GetInstance_mBB395B445D67BFAF5F5583DB41802F04095EAE5B (void);
// 0x00000025 Firebase.Firestore.Query Firebase.Firestore.FirebaseFirestore::CollectionGroup(System.String)
extern void FirebaseFirestore_CollectionGroup_mE15FD3DE0F9B82C9E0ADC4517BD477E9089EE6EB (void);
// 0x00000026 System.Void Firebase.Firestore.FirebaseFirestore::SnapshotsInSyncHandler(System.Int32)
extern void FirebaseFirestore_SnapshotsInSyncHandler_m63B46E06E07A5AC034332B505171FB2779C7FB36 (void);
// 0x00000027 System.Void Firebase.Firestore.FirebaseFirestore::LoadBundleTaskProgressHandler(System.Int32,System.IntPtr)
extern void FirebaseFirestore_LoadBundleTaskProgressHandler_mA5D2729E0A63DCF17A0443A2A46AA394EDBD4AC5 (void);
// 0x00000028 T Firebase.Firestore.FirebaseFirestore::WithFirestoreProxy(System.Func`2<Firebase.Firestore.FirestoreProxy,T>)
// 0x00000029 System.Void Firebase.Firestore.FirebaseFirestore::RemoveSelfFromInstanceCache()
extern void FirebaseFirestore_RemoveSelfFromInstanceCache_m1E495D5093B00A37344C776924E3438525FFD65E (void);
// 0x0000002A System.Void Firebase.Firestore.FirebaseFirestore::.cctor()
extern void FirebaseFirestore__cctor_m2BB5153BA1F3F69AFD4B989E28BF2016A9818410 (void);
// 0x0000002B System.Void Firebase.Firestore.FirebaseFirestore/SnapshotsInSyncDelegate::.ctor(System.Object,System.IntPtr)
extern void SnapshotsInSyncDelegate__ctor_m9375B01153A11BD74F744DD15A886ED958DB772D (void);
// 0x0000002C System.Void Firebase.Firestore.FirebaseFirestore/SnapshotsInSyncDelegate::Invoke(System.Int32)
extern void SnapshotsInSyncDelegate_Invoke_mEF1345EB72B35A0543188ABA2F7B73DE825BB649 (void);
// 0x0000002D System.Void Firebase.Firestore.FirebaseFirestore/LoadBundleTaskProgressDelegate::.ctor(System.Object,System.IntPtr)
extern void LoadBundleTaskProgressDelegate__ctor_m423E84011E7F4325B1C33C35FE4048B0EF193E59 (void);
// 0x0000002E System.Void Firebase.Firestore.FirebaseFirestore/LoadBundleTaskProgressDelegate::Invoke(System.Int32,System.IntPtr)
extern void LoadBundleTaskProgressDelegate_Invoke_m4F282D6B38427407ACB3835C331140DBF948A57C (void);
// 0x0000002F System.Void Firebase.Firestore.FirebaseFirestore/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m9E0287E48CB7E94E679E8A1C82E08264734C0F01 (void);
// 0x00000030 Firebase.Firestore.Query Firebase.Firestore.FirebaseFirestore/<>c__DisplayClass22_0::<CollectionGroup>b__0(Firebase.Firestore.FirestoreProxy)
extern void U3CU3Ec__DisplayClass22_0_U3CCollectionGroupU3Eb__0_m935BEA96D22612A95485B33D464EB7D218DAD1D9 (void);
// 0x00000031 System.Void Firebase.Firestore.FirebaseFirestore/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_mC17E55D69ECC0F02E79007EAEB8B5D0571408386 (void);
// 0x00000032 System.Object Firebase.Firestore.FirebaseFirestore/<>c__DisplayClass31_0::<SnapshotsInSyncHandler>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CSnapshotsInSyncHandlerU3Eb__0_m38DF76A8BE4A295AE459B24064E05E6D2704BB1A (void);
// 0x00000033 System.Void Firebase.Firestore.FirebaseFirestoreSettings::.ctor(Firebase.Firestore.FirestoreProxy)
extern void FirebaseFirestoreSettings__ctor_m5C1599D5F3956CCDFF1D7DB8DE27D84528A2137F (void);
// 0x00000034 System.Void Firebase.Firestore.FirebaseFirestoreSettings::Dispose()
extern void FirebaseFirestoreSettings_Dispose_m226B27465D8677858FA859856A77A9702B6475FB (void);
// 0x00000035 System.String Firebase.Firestore.FirebaseFirestoreSettings::get_Host()
extern void FirebaseFirestoreSettings_get_Host_mCC2B684D5C96EA637B6D4512C06AAD52E647B27D (void);
// 0x00000036 System.Boolean Firebase.Firestore.FirebaseFirestoreSettings::get_SslEnabled()
extern void FirebaseFirestoreSettings_get_SslEnabled_m696B21567BB15D7D034E6BBE616A5D21A4D7A20A (void);
// 0x00000037 System.Boolean Firebase.Firestore.FirebaseFirestoreSettings::get_PersistenceEnabled()
extern void FirebaseFirestoreSettings_get_PersistenceEnabled_m61900571449535D5C048F9CF9776867281BC3A82 (void);
// 0x00000038 System.Int64 Firebase.Firestore.FirebaseFirestoreSettings::get_CacheSizeBytes()
extern void FirebaseFirestoreSettings_get_CacheSizeBytes_m8B5E76CA969790711A52AEDD28DA3925B7647040 (void);
// 0x00000039 T Firebase.Firestore.FirebaseFirestoreSettings::WithReadLock(System.Func`1<T>)
// 0x0000003A System.Void Firebase.Firestore.FirebaseFirestoreSettings::EnsureAppliedToFirestoreProxy()
extern void FirebaseFirestoreSettings_EnsureAppliedToFirestoreProxy_m847B8D9ECFF48F4C9D1CB42F95AA31DD535E081E (void);
// 0x0000003B System.String Firebase.Firestore.FirebaseFirestoreSettings::ToString()
extern void FirebaseFirestoreSettings_ToString_mD267EF3B7CC23C2DD75715745B6FCFA6FD51DF55 (void);
// 0x0000003C System.Void Firebase.Firestore.FirebaseFirestoreSettings::.cctor()
extern void FirebaseFirestoreSettings__cctor_mBEB58E310600334E4D80F8FA94BFC65430C80C73 (void);
// 0x0000003D System.String Firebase.Firestore.FirebaseFirestoreSettings::<get_Host>b__10_0()
extern void FirebaseFirestoreSettings_U3Cget_HostU3Eb__10_0_mF190B2CA77404F0848138EB7D8C99A3C2CE1CE4B (void);
// 0x0000003E System.Boolean Firebase.Firestore.FirebaseFirestoreSettings::<get_SslEnabled>b__13_0()
extern void FirebaseFirestoreSettings_U3Cget_SslEnabledU3Eb__13_0_m7810B99133CDAB19881ED48A45F8213A0DD90B93 (void);
// 0x0000003F System.Boolean Firebase.Firestore.FirebaseFirestoreSettings::<get_PersistenceEnabled>b__16_0()
extern void FirebaseFirestoreSettings_U3Cget_PersistenceEnabledU3Eb__16_0_mFE9EE86456A51039E9B3D34E9E349E3354834B86 (void);
// 0x00000040 System.Int64 Firebase.Firestore.FirebaseFirestoreSettings::<get_CacheSizeBytes>b__19_0()
extern void FirebaseFirestoreSettings_U3Cget_CacheSizeBytesU3Eb__19_0_m193FD30CDA85A8BB042578976606649729268F16 (void);
// 0x00000041 Firebase.Firestore.UnknownPropertyHandling Firebase.Firestore.FirestoreDataAttribute::get_UnknownPropertyHandling()
extern void FirestoreDataAttribute_get_UnknownPropertyHandling_m938FF3674501A857280E218CFF62C1FEA25D88DA (void);
// 0x00000042 System.Type Firebase.Firestore.FirestoreDataAttribute::get_ConverterType()
extern void FirestoreDataAttribute_get_ConverterType_m0E37B0C2D7BCD6EE6729784DDAEEE23F6364EFC2 (void);
// 0x00000043 System.Void Firebase.Firestore.FirestoreException::.ctor(Firebase.Firestore.FirestoreError,System.String)
extern void FirestoreException__ctor_m4CC8461F5C4686D692C1E40E683FF23B7950424D (void);
// 0x00000044 System.Void Firebase.Firestore.FirestoreException::.ctor(System.Int32,System.String)
extern void FirestoreException__ctor_m6BF2A02D4996876CF98E49AB4ECE0F2A2A455856 (void);
// 0x00000045 System.Void Firebase.Firestore.FirestoreException::set_ErrorCode(Firebase.Firestore.FirestoreError)
extern void FirestoreException_set_ErrorCode_m211D8C17633ACCF730402958534100973529B512 (void);
// 0x00000046 System.String Firebase.Firestore.FirestorePropertyAttribute::get_Name()
extern void FirestorePropertyAttribute_get_Name_mFC7ECC680AF9AED8DBBAD7A388561ADD39A19E20 (void);
// 0x00000047 System.Type Firebase.Firestore.FirestorePropertyAttribute::get_ConverterType()
extern void FirestorePropertyAttribute_get_ConverterType_m4D490204075667082A0652C96B069AD578FF81E5 (void);
// 0x00000048 System.Double Firebase.Firestore.GeoPoint::get_Latitude()
extern void GeoPoint_get_Latitude_mE91F389BEBC7A0B0D91DE25D71C232937E6F3014 (void);
// 0x00000049 System.Double Firebase.Firestore.GeoPoint::get_Longitude()
extern void GeoPoint_get_Longitude_m133E6D1B3E1991A9DE1459F186D0A9EDEA05E582 (void);
// 0x0000004A System.Void Firebase.Firestore.GeoPoint::.ctor(System.Double,System.Double)
extern void GeoPoint__ctor_mFD3EA7D3029F84DCEFF17D456E7D40D7DA648A34 (void);
// 0x0000004B System.Boolean Firebase.Firestore.GeoPoint::Equals(System.Object)
extern void GeoPoint_Equals_m0414DB7E087F0DD44CCE0742C1ED9B6D42744902 (void);
// 0x0000004C System.Int32 Firebase.Firestore.GeoPoint::GetHashCode()
extern void GeoPoint_GetHashCode_m120579279AD807DE874974BD3E15392B8BD01B51 (void);
// 0x0000004D System.Boolean Firebase.Firestore.GeoPoint::Equals(Firebase.Firestore.GeoPoint)
extern void GeoPoint_Equals_m3B2A6102D80DB864F3CE3290B0B347A879974646 (void);
// 0x0000004E System.String Firebase.Firestore.GeoPoint::ToString()
extern void GeoPoint_ToString_mE7103148304B568F0B0CFDF26C1BA558A0508248 (void);
// 0x0000004F Firebase.Firestore.GeoPoint Firebase.Firestore.GeoPoint::ConvertFromProxy(Firebase.Firestore.GeoPointProxy)
extern void GeoPoint_ConvertFromProxy_mFB2640BF73C95DEAD11FDDCDAD9F26AC10C52DA9 (void);
// 0x00000050 System.Void Firebase.Firestore.LoadBundleTaskProgress::.ctor(Firebase.Firestore.LoadBundleTaskProgressProxy)
extern void LoadBundleTaskProgress__ctor_m92EF37D44CB79ADCFE0F5EAACB9F006A7E92D976 (void);
// 0x00000051 System.Int32 Firebase.Firestore.LoadBundleTaskProgress::get_DocumentsLoaded()
extern void LoadBundleTaskProgress_get_DocumentsLoaded_mB16A2920280DA8009269A8A5B1BAC24BBC58EAB8 (void);
// 0x00000052 System.Void Firebase.Firestore.LoadBundleTaskProgress::set_DocumentsLoaded(System.Int32)
extern void LoadBundleTaskProgress_set_DocumentsLoaded_m5F3C214AE872DBD6186AE2FEE039AFEE38A99CF9 (void);
// 0x00000053 System.Int32 Firebase.Firestore.LoadBundleTaskProgress::get_TotalDocuments()
extern void LoadBundleTaskProgress_get_TotalDocuments_m879EA2D2D7843E26078931AF4675F4F3E1706A9C (void);
// 0x00000054 System.Void Firebase.Firestore.LoadBundleTaskProgress::set_TotalDocuments(System.Int32)
extern void LoadBundleTaskProgress_set_TotalDocuments_mD6C9C3C2D21233EBCF6ECFB3D4BAB95EA6F46E03 (void);
// 0x00000055 System.Int64 Firebase.Firestore.LoadBundleTaskProgress::get_BytesLoaded()
extern void LoadBundleTaskProgress_get_BytesLoaded_m8A603FB503A7E194C646B30E134A30B8F801D125 (void);
// 0x00000056 System.Void Firebase.Firestore.LoadBundleTaskProgress::set_BytesLoaded(System.Int64)
extern void LoadBundleTaskProgress_set_BytesLoaded_m12FCD36744E9B72993B9967029C7F5E4A2A12B93 (void);
// 0x00000057 System.Int64 Firebase.Firestore.LoadBundleTaskProgress::get_TotalBytes()
extern void LoadBundleTaskProgress_get_TotalBytes_m5DAD938FDF11CFAF5ED26BFC44DE60367539E843 (void);
// 0x00000058 System.Void Firebase.Firestore.LoadBundleTaskProgress::set_TotalBytes(System.Int64)
extern void LoadBundleTaskProgress_set_TotalBytes_mE69ADBE5CE262AB12939BF4D34B633444BCB8A99 (void);
// 0x00000059 Firebase.Firestore.LoadBundleTaskProgress/LoadBundleTaskState Firebase.Firestore.LoadBundleTaskProgress::get_State()
extern void LoadBundleTaskProgress_get_State_mEE05ED7DBF57672624A8B71B28EB795F2A8373C8 (void);
// 0x0000005A System.Void Firebase.Firestore.LoadBundleTaskProgress::set_State(Firebase.Firestore.LoadBundleTaskProgress/LoadBundleTaskState)
extern void LoadBundleTaskProgress_set_State_m01823E221208AF31FE733D8CDAA377C1A3605401 (void);
// 0x0000005B System.Boolean Firebase.Firestore.LoadBundleTaskProgress::Equals(System.Object)
extern void LoadBundleTaskProgress_Equals_m6CA801AE22E5D2434ED0027F2D96E9BFF09FF148 (void);
// 0x0000005C System.Int32 Firebase.Firestore.LoadBundleTaskProgress::GetHashCode()
extern void LoadBundleTaskProgress_GetHashCode_mC69576379BC4D8310EA6E605609B2016E5E77D3E (void);
// 0x0000005D System.Void Firebase.Firestore.Query::.ctor(Firebase.Firestore.QueryProxy,Firebase.Firestore.FirebaseFirestore)
extern void Query__ctor_m2CE28A7C81302467FFCB34DA3838FC5E68673073 (void);
// 0x0000005E System.Void Firebase.Firestore.Query::ClearCallbacksForOwner(Firebase.Firestore.FirebaseFirestore)
extern void Query_ClearCallbacksForOwner_m5BF92E2B453A1451E8D9614D188D8A010F554CF2 (void);
// 0x0000005F Firebase.Firestore.FirebaseFirestore Firebase.Firestore.Query::get_Firestore()
extern void Query_get_Firestore_mB059769B7B9B2B59A91CB866F26E78E3858F90CF (void);
// 0x00000060 System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot> Firebase.Firestore.Query::GetSnapshotAsync(Firebase.Firestore.Source)
extern void Query_GetSnapshotAsync_mEA039F6C70332FF8DC298DC4F8311DD281DBD22E (void);
// 0x00000061 System.Boolean Firebase.Firestore.Query::Equals(System.Object)
extern void Query_Equals_m9BC8D4041680A526881A7CAA0EA2614BCFC4F9C0 (void);
// 0x00000062 System.Boolean Firebase.Firestore.Query::Equals(Firebase.Firestore.Query)
extern void Query_Equals_m4BF263FC1B52FD81E2B179BDBFA581773782397F (void);
// 0x00000063 System.Int32 Firebase.Firestore.Query::GetHashCode()
extern void Query_GetHashCode_m4E8AAE6824FEA1B281C5635CCCA6FC122E5918BD (void);
// 0x00000064 System.Void Firebase.Firestore.Query::QuerySnapshotsHandler(System.Int32,System.IntPtr,Firebase.Firestore.FirestoreError,System.String)
extern void Query_QuerySnapshotsHandler_mACCC5A5D3D2ECD5DCE9DEA25B771CB448DC68715 (void);
// 0x00000065 System.Void Firebase.Firestore.Query::.cctor()
extern void Query__cctor_mDD820AFB130311FA76381AA271CA863441BEBEB5 (void);
// 0x00000066 Firebase.Firestore.QuerySnapshot Firebase.Firestore.Query::<GetSnapshotAsync>b__42_0(Firebase.Firestore.QuerySnapshotProxy)
extern void Query_U3CGetSnapshotAsyncU3Eb__42_0_m12810E2F3E433C7FD81E083493234AD493A26D23 (void);
// 0x00000067 System.Void Firebase.Firestore.Query/ListenerDelegate::.ctor(System.Object,System.IntPtr)
extern void ListenerDelegate__ctor_mFF7C9FAC527620912E24053626C1C60B3DA5B7A1 (void);
// 0x00000068 System.Void Firebase.Firestore.Query/ListenerDelegate::Invoke(System.Int32,System.IntPtr,Firebase.Firestore.FirestoreError,System.String)
extern void ListenerDelegate_Invoke_mB139B3530439A4BB6F54FCB1ACBEF763227AAC35 (void);
// 0x00000069 System.Void Firebase.Firestore.QuerySnapshot::.ctor(Firebase.Firestore.QuerySnapshotProxy,Firebase.Firestore.FirebaseFirestore)
extern void QuerySnapshot__ctor_m51DFA7C79E68E0D4E8F9E920A557DA951DCC03DA (void);
// 0x0000006A System.Collections.Generic.IEnumerable`1<Firebase.Firestore.DocumentSnapshot> Firebase.Firestore.QuerySnapshot::get_Documents()
extern void QuerySnapshot_get_Documents_mE070C83177195302B9D6B573553BB96EF3674BBC (void);
// 0x0000006B System.Int32 Firebase.Firestore.QuerySnapshot::get_Count()
extern void QuerySnapshot_get_Count_mA0A7BDED4057659FE283566C2B750ECD003E4B02 (void);
// 0x0000006C System.Boolean Firebase.Firestore.QuerySnapshot::Equals(System.Object)
extern void QuerySnapshot_Equals_mF61C63B4716C9BCBACC1E37BD50ACA1846A20B7A (void);
// 0x0000006D System.Boolean Firebase.Firestore.QuerySnapshot::Equals(Firebase.Firestore.QuerySnapshot)
extern void QuerySnapshot_Equals_m7D2A703274A18A6091F0B9FB4101FF752AD8294F (void);
// 0x0000006E System.Int32 Firebase.Firestore.QuerySnapshot::GetHashCode()
extern void QuerySnapshot_GetHashCode_mA6AE0D5CDEDC6B1B74039721C058EA57956B6F96 (void);
// 0x0000006F System.Collections.Generic.IEnumerator`1<Firebase.Firestore.DocumentSnapshot> Firebase.Firestore.QuerySnapshot::GetEnumerator()
extern void QuerySnapshot_GetEnumerator_m6E2B0B6A84E0DF72E23650532B483A9FD8C0BF60 (void);
// 0x00000070 System.Collections.IEnumerator Firebase.Firestore.QuerySnapshot::System.Collections.IEnumerable.GetEnumerator()
extern void QuerySnapshot_System_Collections_IEnumerable_GetEnumerator_m2DF41DC974D62F53964BDACDBE90FB17B9BA696B (void);
// 0x00000071 System.Void Firebase.Firestore.QuerySnapshot::LoadDocumentsCached()
extern void QuerySnapshot_LoadDocumentsCached_m107AD85853340D645A554665F4FAE43CA7A9F089 (void);
// 0x00000072 Firebase.Firestore.DocumentSnapshotProxy/ServerTimestampBehavior Firebase.Firestore.ServerTimestampBehaviorConverter::ConvertToProxy(Firebase.Firestore.ServerTimestampBehavior)
extern void ServerTimestampBehaviorConverter_ConvertToProxy_m92EC98E5DB85DFF5553D9A5749E86E2436502103 (void);
// 0x00000073 System.DateTime Firebase.Firestore.Timestamp::ToDateTime()
extern void Timestamp_ToDateTime_mDB1ACFC2FB995894CCED239A5E557CDA12F066FE (void);
// 0x00000074 System.DateTimeOffset Firebase.Firestore.Timestamp::ToDateTimeOffset()
extern void Timestamp_ToDateTimeOffset_mC1AB3B395A37909C8A0E01DB07C55C0E6AD3ED46 (void);
// 0x00000075 System.Boolean Firebase.Firestore.Timestamp::Equals(System.Object)
extern void Timestamp_Equals_m4662FEFEF81799C37FB99795E492F562E55CA253 (void);
// 0x00000076 System.Int32 Firebase.Firestore.Timestamp::GetHashCode()
extern void Timestamp_GetHashCode_mFF00599A75371E96B5E55636FF40902F31BCFCEC (void);
// 0x00000077 System.Boolean Firebase.Firestore.Timestamp::Equals(Firebase.Firestore.Timestamp)
extern void Timestamp_Equals_mAD77D232E60E042104147E04E8D3331CBB35D8BE (void);
// 0x00000078 System.Int32 Firebase.Firestore.Timestamp::CompareTo(Firebase.Firestore.Timestamp)
extern void Timestamp_CompareTo_mE49BD77BFD6AAD4ACEF74F1578B2ACA8929EC6A1 (void);
// 0x00000079 System.Int32 Firebase.Firestore.Timestamp::CompareTo(System.Object)
extern void Timestamp_CompareTo_m3E776402C9986CE96A20DF4812C32A70B164780E (void);
// 0x0000007A System.String Firebase.Firestore.Timestamp::ToString()
extern void Timestamp_ToString_mD0ECC96BAFA1DBE375F79E550C63D8897CD77AD8 (void);
// 0x0000007B System.Void Firebase.Firestore.Timestamp::.ctor(System.Int64,System.Int32)
extern void Timestamp__ctor_m4C5388F8B2158FA8F3A104CFE55ACD0024554D53 (void);
// 0x0000007C Firebase.Firestore.Timestamp Firebase.Firestore.Timestamp::ConvertFromProxy(Firebase.Firestore.TimestampProxy)
extern void Timestamp_ConvertFromProxy_m47D961472FC16995F06A638FF5DBB18B890641F4 (void);
// 0x0000007D System.Void Firebase.Firestore.Timestamp::.cctor()
extern void Timestamp__cctor_m06A8B890DB64CDC36D8DD69D898D88E8E4771F96 (void);
// 0x0000007E System.Collections.Generic.IDictionary`2<System.Type,Firebase.Firestore.Converters.IFirestoreInternalConverter> Firebase.Firestore.ConverterRegistry::ToConverterDictionary()
extern void ConverterRegistry_ToConverterDictionary_m490AB75121F03CD402E7D17CCD3BB2FC2218FEC6 (void);
// 0x0000007F Firebase.Firestore.FirebaseFirestore Firebase.Firestore.DeserializationContext::get_Firestore()
extern void DeserializationContext_get_Firestore_m16D946E39CF356655BAE738D16D837D69A6E8F7E (void);
// 0x00000080 Firebase.Firestore.DocumentReference Firebase.Firestore.DeserializationContext::get_DocumentReference()
extern void DeserializationContext_get_DocumentReference_mD62CCE2F430E6E159E7C53F09C3441A33CBCC297 (void);
// 0x00000081 System.Void Firebase.Firestore.DeserializationContext::.ctor(Firebase.Firestore.DocumentSnapshot)
extern void DeserializationContext__ctor_mECF86F1A451C686E47918DA12E63C27C460D95E6 (void);
// 0x00000082 Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.DeserializationContext::GetConverter(System.Type)
extern void DeserializationContext_GetConverter_m360A3FBBB3962C13463FB10B43D4B71C3A961120 (void);
// 0x00000083 T Firebase.Firestore.FirestoreConverter`1::FromFirestore(System.Object)
// 0x00000084 System.Void Firebase.Firestore.ListenerRegistrationMap`1::.ctor()
// 0x00000085 System.Void Firebase.Firestore.ListenerRegistrationMap`1::AssertGenericArgumentIsDelegate()
// 0x00000086 System.Boolean Firebase.Firestore.ListenerRegistrationMap`1::TryGetCallback(System.Int32,T&)
// 0x00000087 System.Void Firebase.Firestore.ListenerRegistrationMap`1::Unregister(System.Int32)
// 0x00000088 System.Void Firebase.Firestore.ListenerRegistrationMap`1::ClearCallbacksForOwner(System.Object)
// 0x00000089 System.Void Firebase.Firestore.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern void MonoPInvokeCallbackAttribute__ctor_mA91A7DD150D6C507BCFCA0D5B108E72B7E4D5804 (void);
// 0x0000008A Firebase.Firestore.SerializationContext Firebase.Firestore.SerializationContext::get_Default()
extern void SerializationContext_get_Default_m4D1442067DD14EC8387D05E602E8A50EA2F259DF (void);
// 0x0000008B System.Void Firebase.Firestore.SerializationContext::.ctor(Firebase.Firestore.ConverterRegistry)
extern void SerializationContext__ctor_m48DADC02D832C8298D2BB521EE091C81DA87D85D (void);
// 0x0000008C Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.SerializationContext::GetConverter(System.Type)
extern void SerializationContext_GetConverter_mFB2CEED05FDA601C9C3405C70C8E968AA7B4BE9A (void);
// 0x0000008D System.Void Firebase.Firestore.SerializationContext::.cctor()
extern void SerializationContext__cctor_m3E79E149FCBC4EAD1AFB4B8A65631D9E09432F33 (void);
// 0x0000008E System.Void Firebase.Firestore.TransactionManager::.ctor(Firebase.Firestore.FirebaseFirestore,Firebase.Firestore.FirestoreProxy)
extern void TransactionManager__ctor_m463D159697F8F9CF65D8B045F374770BC3DFC89F (void);
// 0x0000008F System.Void Firebase.Firestore.TransactionManager::Finalize()
extern void TransactionManager_Finalize_m0628FE79549C0DF4FBE2800AF8406A7485243C7E (void);
// 0x00000090 System.Void Firebase.Firestore.TransactionManager::Dispose()
extern void TransactionManager_Dispose_m9BE81C60B6A085D086D11DADF9DA64886050CFE2 (void);
// 0x00000091 System.Void Firebase.Firestore.TransactionManager::.cctor()
extern void TransactionManager__cctor_m9A843BC514CB084A996821BA57486B7EA00B8775 (void);
// 0x00000092 System.Object Firebase.Firestore.ValueDeserializer::Deserialize(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy,System.Type)
extern void ValueDeserializer_Deserialize_mF55F3A85F009C7CD0F055BC2A91591865955A094 (void);
// 0x00000093 System.Type Firebase.Firestore.ValueDeserializer::GetTargetType(Firebase.Firestore.FieldValueProxy)
extern void ValueDeserializer_GetTargetType_mE062B00693454451CC4617CD471C19AC10793C4F (void);
// 0x00000094 System.Void Firebase.Firestore.GeoPointProxy::.ctor(System.IntPtr,System.Boolean)
extern void GeoPointProxy__ctor_mB2CFD6F2A717A0C3AE22BBD4F30136799C50F391 (void);
// 0x00000095 System.Void Firebase.Firestore.GeoPointProxy::Finalize()
extern void GeoPointProxy_Finalize_m8677EF4A0CFC39D494DB49DF8F37BC027AF5F60A (void);
// 0x00000096 System.Void Firebase.Firestore.GeoPointProxy::Dispose()
extern void GeoPointProxy_Dispose_m937789FC37643980D0871200ED89367CE8FB6E6D (void);
// 0x00000097 System.Void Firebase.Firestore.GeoPointProxy::Dispose(System.Boolean)
extern void GeoPointProxy_Dispose_m92C493537FE315F4B95D545D94326301A01A5D02 (void);
// 0x00000098 System.Double Firebase.Firestore.GeoPointProxy::latitude()
extern void GeoPointProxy_latitude_mA8BBE294FE9CAB3D26516FE20DAFACEB14F90DC1 (void);
// 0x00000099 System.Double Firebase.Firestore.GeoPointProxy::longitude()
extern void GeoPointProxy_longitude_m1B4199AA7E07F3753605A3576FB273D7B547A1FF (void);
// 0x0000009A System.Void Firebase.Firestore.TimestampProxy::.ctor(System.IntPtr,System.Boolean)
extern void TimestampProxy__ctor_mEF0E31229E48079A737F1B218C19E6B77C0E2066 (void);
// 0x0000009B System.Void Firebase.Firestore.TimestampProxy::Finalize()
extern void TimestampProxy_Finalize_mB50B5DF17EDEB7D63CF0242CAC54604E8FA4E653 (void);
// 0x0000009C System.Void Firebase.Firestore.TimestampProxy::Dispose()
extern void TimestampProxy_Dispose_mDFD057DF2502704706D7DB2BC15495ABF3F61FA6 (void);
// 0x0000009D System.Void Firebase.Firestore.TimestampProxy::Dispose(System.Boolean)
extern void TimestampProxy_Dispose_m80731F1E57850DDE39A14B033B499775AB7A71B2 (void);
// 0x0000009E System.Int64 Firebase.Firestore.TimestampProxy::seconds()
extern void TimestampProxy_seconds_m8B893A897B7984C99D67307454E74CAAB56F9264 (void);
// 0x0000009F System.Int32 Firebase.Firestore.TimestampProxy::nanoseconds()
extern void TimestampProxy_nanoseconds_m3CBF873AD250722E000F1AD8B8E862F9030235CD (void);
// 0x000000A0 System.String Firebase.Firestore.TimestampProxy::ToString()
extern void TimestampProxy_ToString_m8DA845A04113BC279FBFB220898FAC774D994284 (void);
// 0x000000A1 System.Void Firebase.Firestore.Future_QuerySnapshot::.ctor(System.IntPtr,System.Boolean)
extern void Future_QuerySnapshot__ctor_m564ED70AF9BFEC3F375C3CC5D2C09103F33E2B76 (void);
// 0x000000A2 System.Void Firebase.Firestore.Future_QuerySnapshot::Dispose(System.Boolean)
extern void Future_QuerySnapshot_Dispose_mEC5BBCE648D469D2A90A9A85EB535007396C362E (void);
// 0x000000A3 System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshotProxy> Firebase.Firestore.Future_QuerySnapshot::GetTask(Firebase.Firestore.Future_QuerySnapshot)
extern void Future_QuerySnapshot_GetTask_m83FCC445EC41489278769E6CEEC6E71D9F372C2C (void);
// 0x000000A4 System.Void Firebase.Firestore.Future_QuerySnapshot::ThrowIfDisposed()
extern void Future_QuerySnapshot_ThrowIfDisposed_m946D943B65A2A94AA1E6C3C8D50CA07409CFAAC6 (void);
// 0x000000A5 System.Void Firebase.Firestore.Future_QuerySnapshot::SetOnCompletionCallback(Firebase.Firestore.Future_QuerySnapshot/Action)
extern void Future_QuerySnapshot_SetOnCompletionCallback_mD068327FDF8CFA754F98A7BF974BA6FB09AE8866 (void);
// 0x000000A6 System.Void Firebase.Firestore.Future_QuerySnapshot::SetCompletionData(System.IntPtr)
extern void Future_QuerySnapshot_SetCompletionData_m72EF16F4ED04F08C350CD20CEE2EAA945458C772 (void);
// 0x000000A7 System.Void Firebase.Firestore.Future_QuerySnapshot::SWIG_CompletionDispatcher(System.Int32)
extern void Future_QuerySnapshot_SWIG_CompletionDispatcher_mDE52F3C13385E02F9E4DDDBAE074D02C1B90EC39 (void);
// 0x000000A8 System.IntPtr Firebase.Firestore.Future_QuerySnapshot::SWIG_OnCompletion(Firebase.Firestore.Future_QuerySnapshot/SWIG_CompletionDelegate,System.Int32)
extern void Future_QuerySnapshot_SWIG_OnCompletion_m4DB13F0FA13C028FD44D5BFE0F5ABB50F1F53A41 (void);
// 0x000000A9 System.Void Firebase.Firestore.Future_QuerySnapshot::SWIG_FreeCompletionData(System.IntPtr)
extern void Future_QuerySnapshot_SWIG_FreeCompletionData_mB307080187B2F875D101A8734DD439654B3B90E3 (void);
// 0x000000AA Firebase.Firestore.QuerySnapshotProxy Firebase.Firestore.Future_QuerySnapshot::GetResult()
extern void Future_QuerySnapshot_GetResult_mAD1F3E361970A51E5C37D891C2A173426E49F9D2 (void);
// 0x000000AB System.Void Firebase.Firestore.Future_QuerySnapshot::.cctor()
extern void Future_QuerySnapshot__cctor_m63ECFE9F472DEFA4DDD39CBDB1281DC474989BD7 (void);
// 0x000000AC System.Void Firebase.Firestore.Future_QuerySnapshot/Action::.ctor(System.Object,System.IntPtr)
extern void Action__ctor_m1D782F48F4DA20E6DC65EBF500AABF4BE9E1493A (void);
// 0x000000AD System.Void Firebase.Firestore.Future_QuerySnapshot/Action::Invoke()
extern void Action_Invoke_m8E58A50FAF94C5B7B9F9F8DBC04C64CB6A20DDFF (void);
// 0x000000AE System.Void Firebase.Firestore.Future_QuerySnapshot/SWIG_CompletionDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIG_CompletionDelegate__ctor_mE93A78FEE09432728AACFB3A4A706E0AAEBE7F0C (void);
// 0x000000AF System.Void Firebase.Firestore.Future_QuerySnapshot/SWIG_CompletionDelegate::Invoke(System.Int32)
extern void SWIG_CompletionDelegate_Invoke_m42937927633E0A2CE5032FC8F5C4E9F2B04CBA82 (void);
// 0x000000B0 System.Void Firebase.Firestore.Future_QuerySnapshot/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mC1A4BD21E45EB0EDF7101788C3A142A1F24C1F5B (void);
// 0x000000B1 System.Void Firebase.Firestore.Future_QuerySnapshot/<>c__DisplayClass5_0::<GetTask>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CGetTaskU3Eb__0_mAE4853B7006F26AEA789954F76B08D4B38C8AADE (void);
// 0x000000B2 System.Void Firebase.Firestore.DocumentReferenceProxy::.ctor(System.IntPtr,System.Boolean)
extern void DocumentReferenceProxy__ctor_m9A73CC86CDB33800541C380C7842F4E8070D4A99 (void);
// 0x000000B3 System.Void Firebase.Firestore.DocumentReferenceProxy::Finalize()
extern void DocumentReferenceProxy_Finalize_mC2875BF0BCED1C3E67EBEC14E4F00473CC0F6770 (void);
// 0x000000B4 System.Void Firebase.Firestore.DocumentReferenceProxy::Dispose()
extern void DocumentReferenceProxy_Dispose_mED8393B9D472C16D3D9CF1C0BE22140AA57E9E7D (void);
// 0x000000B5 System.Void Firebase.Firestore.DocumentReferenceProxy::Dispose(System.Boolean)
extern void DocumentReferenceProxy_Dispose_m886E59219FE018A85B4278C1E8AEBBA51459F081 (void);
// 0x000000B6 System.String Firebase.Firestore.DocumentReferenceProxy::id()
extern void DocumentReferenceProxy_id_m8CDA448FDA43CB9B3C259D0B26D10ACF645862A1 (void);
// 0x000000B7 System.String Firebase.Firestore.DocumentReferenceProxy::path()
extern void DocumentReferenceProxy_path_mE140A5A650A9204B6018A7F427CDD25773FF74B0 (void);
// 0x000000B8 System.Boolean Firebase.Firestore.DocumentReferenceProxy::is_valid()
extern void DocumentReferenceProxy_is_valid_mE85E9F3E580E73407E20AF23CC853DE6FF89886C (void);
// 0x000000B9 System.Void Firebase.Firestore.DocumentSnapshotProxy::.ctor(System.IntPtr,System.Boolean)
extern void DocumentSnapshotProxy__ctor_mE5B8BE19C0F0E7DF3C550585F8AED62FDD80C9A1 (void);
// 0x000000BA System.Runtime.InteropServices.HandleRef Firebase.Firestore.DocumentSnapshotProxy::getCPtr(Firebase.Firestore.DocumentSnapshotProxy)
extern void DocumentSnapshotProxy_getCPtr_mF9F6A5AD72B52268009A8FC6C14E434D4857C86B (void);
// 0x000000BB System.Void Firebase.Firestore.DocumentSnapshotProxy::Finalize()
extern void DocumentSnapshotProxy_Finalize_mD86BC6A9386B22993B480CFFF24388E688ECFA5E (void);
// 0x000000BC System.Void Firebase.Firestore.DocumentSnapshotProxy::Dispose()
extern void DocumentSnapshotProxy_Dispose_mBAF78DEBAC840F65FB7AE9DD588E6A1FF74F2FEF (void);
// 0x000000BD System.Void Firebase.Firestore.DocumentSnapshotProxy::Dispose(System.Boolean)
extern void DocumentSnapshotProxy_Dispose_m9E318188984DEC90AE6AEC2604A4EFC7229CD32F (void);
// 0x000000BE Firebase.Firestore.DocumentReferenceProxy Firebase.Firestore.DocumentSnapshotProxy::reference()
extern void DocumentSnapshotProxy_reference_m0D32F20A6288EBC0D2A74D77AF177FCEBC8CDA4A (void);
// 0x000000BF System.Boolean Firebase.Firestore.DocumentSnapshotProxy::exists()
extern void DocumentSnapshotProxy_exists_m7CC1ACBA13375AEE10F93189D851A41276C56185 (void);
// 0x000000C0 System.Void Firebase.Firestore.FieldValueProxy::.ctor(System.IntPtr,System.Boolean)
extern void FieldValueProxy__ctor_m057A425535C2E4512342AE1C24122D2333352F8D (void);
// 0x000000C1 System.Runtime.InteropServices.HandleRef Firebase.Firestore.FieldValueProxy::getCPtr(Firebase.Firestore.FieldValueProxy)
extern void FieldValueProxy_getCPtr_mD3D0355179AEE52CCC6215A2D5E7CA94EBA02B25 (void);
// 0x000000C2 System.Void Firebase.Firestore.FieldValueProxy::Finalize()
extern void FieldValueProxy_Finalize_mD7E4217C5EE91E6796D79D72E4E068E68D9EF01C (void);
// 0x000000C3 System.Void Firebase.Firestore.FieldValueProxy::Dispose()
extern void FieldValueProxy_Dispose_m9AD56F3B3603A31A5FCE2591D954CFD53A3B4E12 (void);
// 0x000000C4 System.Void Firebase.Firestore.FieldValueProxy::Dispose(System.Boolean)
extern void FieldValueProxy_Dispose_m9C85A44E07E511F3A24594B6E910C6DD928A71CD (void);
// 0x000000C5 Firebase.Firestore.FieldValueProxy/Type Firebase.Firestore.FieldValueProxy::type()
extern void FieldValueProxy_type_m0901CADDA170E659B1C761C5E260E52916B7DBCC (void);
// 0x000000C6 System.Boolean Firebase.Firestore.FieldValueProxy::is_null()
extern void FieldValueProxy_is_null_m0A09FD8355CDBA3BA3A82A87011F220BC4D6D3FE (void);
// 0x000000C7 System.Boolean Firebase.Firestore.FieldValueProxy::boolean_value()
extern void FieldValueProxy_boolean_value_m264AF274971D95251C903E1BFE26FDEF685CE936 (void);
// 0x000000C8 System.Int64 Firebase.Firestore.FieldValueProxy::integer_value()
extern void FieldValueProxy_integer_value_m35B9C4A72E076C276D193FB54C19301B93F619CF (void);
// 0x000000C9 System.Double Firebase.Firestore.FieldValueProxy::double_value()
extern void FieldValueProxy_double_value_mC6999E0AF3DB2B290DD6876691367FCBD91E7355 (void);
// 0x000000CA Firebase.Firestore.TimestampProxy Firebase.Firestore.FieldValueProxy::timestamp_value()
extern void FieldValueProxy_timestamp_value_mD1CDBAA188F3E4DE283EFF937EA137690C60B944 (void);
// 0x000000CB System.String Firebase.Firestore.FieldValueProxy::string_value()
extern void FieldValueProxy_string_value_m02864893AA2D618F225A72078B2274EDE8C14377 (void);
// 0x000000CC Firebase.Firestore.SWIGTYPE_p_unsigned_char Firebase.Firestore.FieldValueProxy::blob_value()
extern void FieldValueProxy_blob_value_m2E9BCB9306FBDF03DDFACB654E34A4A79E3616BF (void);
// 0x000000CD System.UInt32 Firebase.Firestore.FieldValueProxy::blob_size()
extern void FieldValueProxy_blob_size_m8B795B4E09CF06ED8FB74EB921E5E123039AE729 (void);
// 0x000000CE Firebase.Firestore.DocumentReferenceProxy Firebase.Firestore.FieldValueProxy::reference_value()
extern void FieldValueProxy_reference_value_m309FF53825A808F723A7961A304A043D076E1E42 (void);
// 0x000000CF Firebase.Firestore.GeoPointProxy Firebase.Firestore.FieldValueProxy::geo_point_value()
extern void FieldValueProxy_geo_point_value_m70E9A89FDF8640B88A28240891E5CABD1BBA71AA (void);
// 0x000000D0 Firebase.Firestore.FieldValueProxy Firebase.Firestore.FieldValueProxy::ServerTimestamp()
extern void FieldValueProxy_ServerTimestamp_mD314BB0BEDECBB7AB284ED28E4C394B1EB521A67 (void);
// 0x000000D1 System.Void Firebase.Firestore.QueryProxy::.ctor(System.IntPtr,System.Boolean)
extern void QueryProxy__ctor_m54BE6259B741A4DCD15DFC8C96261065F74E7953 (void);
// 0x000000D2 System.Runtime.InteropServices.HandleRef Firebase.Firestore.QueryProxy::getCPtr(Firebase.Firestore.QueryProxy)
extern void QueryProxy_getCPtr_mA9CA81F6D1123892E1ACA6BA46D1A6F150D963F6 (void);
// 0x000000D3 System.Void Firebase.Firestore.QueryProxy::Finalize()
extern void QueryProxy_Finalize_m8B980D089AB7B4CD1517E832E55753C2173E82FF (void);
// 0x000000D4 System.Void Firebase.Firestore.QueryProxy::Dispose()
extern void QueryProxy_Dispose_mBEE2C95195CC54C0616F9AA858FA188C783B05BA (void);
// 0x000000D5 System.Void Firebase.Firestore.QueryProxy::Dispose(System.Boolean)
extern void QueryProxy_Dispose_m214839BD42DE29060A0316287562EFD81811CC0F (void);
// 0x000000D6 System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshotProxy> Firebase.Firestore.QueryProxy::GetAsync(Firebase.Firestore.SourceProxy)
extern void QueryProxy_GetAsync_m7880B56C9C11927426B2B4C86DF28E7BFDE588E8 (void);
// 0x000000D7 System.Void Firebase.Firestore.QuerySnapshotProxy::.ctor(System.IntPtr,System.Boolean)
extern void QuerySnapshotProxy__ctor_mC833FF3EFA111C2A65294D8792262DF48F4DCB7B (void);
// 0x000000D8 System.Runtime.InteropServices.HandleRef Firebase.Firestore.QuerySnapshotProxy::getCPtr(Firebase.Firestore.QuerySnapshotProxy)
extern void QuerySnapshotProxy_getCPtr_mD73C14C3A50E046B30F309615B4056857DA6524A (void);
// 0x000000D9 System.Void Firebase.Firestore.QuerySnapshotProxy::Finalize()
extern void QuerySnapshotProxy_Finalize_m626A9DA4CAB9FCBE96816B12C6660B309A345AAA (void);
// 0x000000DA System.Void Firebase.Firestore.QuerySnapshotProxy::Dispose()
extern void QuerySnapshotProxy_Dispose_m813E123BDC8BBD75FE412974B272A7413599D0AC (void);
// 0x000000DB System.Void Firebase.Firestore.QuerySnapshotProxy::Dispose(System.Boolean)
extern void QuerySnapshotProxy_Dispose_mFD4255001199C4C9C19550FB0CB9108FA7A32B3C (void);
// 0x000000DC System.UInt32 Firebase.Firestore.QuerySnapshotProxy::size()
extern void QuerySnapshotProxy_size_mD7049D777AF47CDD2B692FF277EFBCE825E61335 (void);
// 0x000000DD System.Void Firebase.Firestore.SettingsProxy::.ctor(System.IntPtr,System.Boolean)
extern void SettingsProxy__ctor_m7BE0A49E746604DAA6D4F0F4C1F11EA42149F34E (void);
// 0x000000DE System.Runtime.InteropServices.HandleRef Firebase.Firestore.SettingsProxy::getCPtr(Firebase.Firestore.SettingsProxy)
extern void SettingsProxy_getCPtr_mF852AF8700D95B0F36550C21FDA57B0B44BD9187 (void);
// 0x000000DF System.Void Firebase.Firestore.SettingsProxy::Finalize()
extern void SettingsProxy_Finalize_mC46D66BFE1A50038FCA4FD54E4D723258341275D (void);
// 0x000000E0 System.Void Firebase.Firestore.SettingsProxy::Dispose()
extern void SettingsProxy_Dispose_m442C175F98EFCE27F7A3700EEFC5B5AE36B10D11 (void);
// 0x000000E1 System.Void Firebase.Firestore.SettingsProxy::Dispose(System.Boolean)
extern void SettingsProxy_Dispose_mE119A9B43AFFD05DFC31B7D445B398D9F3E89DC5 (void);
// 0x000000E2 System.Void Firebase.Firestore.SettingsProxy::.ctor()
extern void SettingsProxy__ctor_mFF91E7269B1D8AB229B753F49B522ABDA9FAE1D2 (void);
// 0x000000E3 System.String Firebase.Firestore.SettingsProxy::host()
extern void SettingsProxy_host_m2CCB8A5A10853AF344C4D7E4017862E8D9EEDE4A (void);
// 0x000000E4 System.Boolean Firebase.Firestore.SettingsProxy::is_ssl_enabled()
extern void SettingsProxy_is_ssl_enabled_m82E8567F07C8B8040E0B3F02D0AA7F00656EB822 (void);
// 0x000000E5 System.Boolean Firebase.Firestore.SettingsProxy::is_persistence_enabled()
extern void SettingsProxy_is_persistence_enabled_mBED82AE76D1C4517662A06F8AE4F9E96F1DDB364 (void);
// 0x000000E6 System.Int64 Firebase.Firestore.SettingsProxy::cache_size_bytes()
extern void SettingsProxy_cache_size_bytes_mC70FF08213FC8A995429A00351986FD96ED338FE (void);
// 0x000000E7 System.Void Firebase.Firestore.SettingsProxy::set_host(System.String)
extern void SettingsProxy_set_host_mD0E554E2A0CBCAA1D90212E7899327F7F648BA86 (void);
// 0x000000E8 System.Void Firebase.Firestore.SettingsProxy::set_ssl_enabled(System.Boolean)
extern void SettingsProxy_set_ssl_enabled_m3BBDC250075222AFC9566F71EAD2372C8A474CAC (void);
// 0x000000E9 System.Void Firebase.Firestore.SettingsProxy::set_persistence_enabled(System.Boolean)
extern void SettingsProxy_set_persistence_enabled_m4369035C1C25C4F4D2284769D4B29815EE8BC9B3 (void);
// 0x000000EA System.Void Firebase.Firestore.SettingsProxy::set_cache_size_bytes(System.Int64)
extern void SettingsProxy_set_cache_size_bytes_m6D22F07BA542A1FC505275CFC21BBFB6637CBD2A (void);
// 0x000000EB System.Void Firebase.Firestore.SettingsProxy::.cctor()
extern void SettingsProxy__cctor_m74626ADDA32BA3FC993F46260A74675E5B0B8F2E (void);
// 0x000000EC System.Void Firebase.Firestore.LoadBundleTaskProgressProxy::.ctor(System.IntPtr,System.Boolean)
extern void LoadBundleTaskProgressProxy__ctor_m589DAC2316E4D9157F4091B1947130DDEE6DEE0B (void);
// 0x000000ED System.Void Firebase.Firestore.LoadBundleTaskProgressProxy::Finalize()
extern void LoadBundleTaskProgressProxy_Finalize_m870F582FA08EB2569BD91580A190A4FB329053B8 (void);
// 0x000000EE System.Void Firebase.Firestore.LoadBundleTaskProgressProxy::Dispose()
extern void LoadBundleTaskProgressProxy_Dispose_m7AB4AABD17FBC3BD299EBCE6758CC90FA0817028 (void);
// 0x000000EF System.Void Firebase.Firestore.LoadBundleTaskProgressProxy::Dispose(System.Boolean)
extern void LoadBundleTaskProgressProxy_Dispose_mF8BE12610E8DB30EA2283F27D0B165B47AC4A73F (void);
// 0x000000F0 System.Int32 Firebase.Firestore.LoadBundleTaskProgressProxy::documents_loaded()
extern void LoadBundleTaskProgressProxy_documents_loaded_m88A608E7A6791DD89BB77068F783296A4A7CA977 (void);
// 0x000000F1 System.Int32 Firebase.Firestore.LoadBundleTaskProgressProxy::total_documents()
extern void LoadBundleTaskProgressProxy_total_documents_m9789681C10719617FB16270DDA713DD531F13233 (void);
// 0x000000F2 System.Int64 Firebase.Firestore.LoadBundleTaskProgressProxy::bytes_loaded()
extern void LoadBundleTaskProgressProxy_bytes_loaded_m4F15F71F3EE7F7860DA16F29A3BC265D1B8796E1 (void);
// 0x000000F3 System.Int64 Firebase.Firestore.LoadBundleTaskProgressProxy::total_bytes()
extern void LoadBundleTaskProgressProxy_total_bytes_m7B0175EBAB06F5E5FB0BE40669876D62FF6A7C1C (void);
// 0x000000F4 Firebase.Firestore.LoadBundleTaskProgressProxy/State Firebase.Firestore.LoadBundleTaskProgressProxy::state()
extern void LoadBundleTaskProgressProxy_state_m57FD1F9D30D4B57833EFA33DE4B3ABE24BF142CE (void);
// 0x000000F5 System.Void Firebase.Firestore.FirestoreProxy::.ctor(System.IntPtr,System.Boolean)
extern void FirestoreProxy__ctor_m33D85CBD3680DC453D2C897DA82CB192FB65FF15 (void);
// 0x000000F6 System.Runtime.InteropServices.HandleRef Firebase.Firestore.FirestoreProxy::getCPtr(Firebase.Firestore.FirestoreProxy)
extern void FirestoreProxy_getCPtr_mE29AF87AA06BD73AEB17FABDDE15D9F03F31096B (void);
// 0x000000F7 System.Void Firebase.Firestore.FirestoreProxy::Finalize()
extern void FirestoreProxy_Finalize_m12D7E48C5AC644E05DFD54C1FAD023E1E0A832F6 (void);
// 0x000000F8 System.Void Firebase.Firestore.FirestoreProxy::Dispose()
extern void FirestoreProxy_Dispose_mDBB836BE8D77031C98289CF1272829448413FEBA (void);
// 0x000000F9 System.Void Firebase.Firestore.FirestoreProxy::Dispose(System.Boolean)
extern void FirestoreProxy_Dispose_mE1365B94489D68EC2253A5C65DDA09C32E51F5A9 (void);
// 0x000000FA Firebase.Firestore.FirestoreProxy Firebase.Firestore.FirestoreProxy::GetInstance(Firebase.FirebaseApp)
extern void FirestoreProxy_GetInstance_mA430C24698D14D60B2486E0B657907E6D95A2DC6 (void);
// 0x000000FB Firebase.Firestore.QueryProxy Firebase.Firestore.FirestoreProxy::CollectionGroup(System.String)
extern void FirestoreProxy_CollectionGroup_m6734F28BCA0A5C2DB7051F6ACD4A9CD1085E31B5 (void);
// 0x000000FC Firebase.Firestore.SettingsProxy Firebase.Firestore.FirestoreProxy::settings()
extern void FirestoreProxy_settings_mAE6DD88E6474AA4F2FAEFC42ABFD3095D8AE71C4 (void);
// 0x000000FD System.Void Firebase.Firestore.FirestoreProxy::set_settings(Firebase.Firestore.SettingsProxy)
extern void FirestoreProxy_set_settings_m55F667182BE08269B255A8BC5BFE2077D1256CBD (void);
// 0x000000FE System.Void Firebase.Firestore.ApiHeaders::SetClientLanguage(System.String)
extern void ApiHeaders_SetClientLanguage_mCEB9F0276D8609BC491E7F34D4B435987A1EF3CF (void);
// 0x000000FF System.Void Firebase.Firestore.TransactionCallbackProxy::Dispose()
extern void TransactionCallbackProxy_Dispose_mBB49019291FA4513D573EA6FC81F8CA5ED137517 (void);
// 0x00000100 System.Void Firebase.Firestore.TransactionCallbackProxy::Dispose(System.Boolean)
extern void TransactionCallbackProxy_Dispose_m0021849765F39866894838C2BF78683502E34DCC (void);
// 0x00000101 System.Void Firebase.Firestore.TransactionManagerProxy::.ctor(System.IntPtr,System.Boolean)
extern void TransactionManagerProxy__ctor_m666391B0E579026E20DAF2E9CAADEE01C9C7A7AF (void);
// 0x00000102 System.Void Firebase.Firestore.TransactionManagerProxy::Finalize()
extern void TransactionManagerProxy_Finalize_m8AEF3C30B371811F7CFE64F8BD3715ABAB8E7BCD (void);
// 0x00000103 System.Void Firebase.Firestore.TransactionManagerProxy::Dispose()
extern void TransactionManagerProxy_Dispose_m103B12BB6B4C692A90966092FFD8D331A8043A7C (void);
// 0x00000104 System.Void Firebase.Firestore.TransactionManagerProxy::Dispose(System.Boolean)
extern void TransactionManagerProxy_Dispose_mA6A6E8073B61DE5E2DD148B2C26D871698F3B2A5 (void);
// 0x00000105 System.Void Firebase.Firestore.TransactionManagerProxy::.ctor(Firebase.Firestore.FirestoreProxy)
extern void TransactionManagerProxy__ctor_m7E365B74A286FE0BA6C311750CDE2E46CA5056D5 (void);
// 0x00000106 System.Void Firebase.Firestore.TransactionManagerProxy::CppDispose()
extern void TransactionManagerProxy_CppDispose_m50AD052C5F5B93F922F93B4227C34B1A1E7A8069 (void);
// 0x00000107 System.Void Firebase.Firestore.FieldToValueMap::.ctor(System.IntPtr,System.Boolean)
extern void FieldToValueMap__ctor_mC1EF56A9227A7CDCDAFE0B998838DA471EA44419 (void);
// 0x00000108 System.Void Firebase.Firestore.FieldToValueMap::Finalize()
extern void FieldToValueMap_Finalize_m581620585CA304757491CA71FC490818D2F47B43 (void);
// 0x00000109 System.Void Firebase.Firestore.FieldToValueMap::Dispose()
extern void FieldToValueMap_Dispose_mD04D96A9A327A9C0F4175FFCD2D21FAD6F2FF5D4 (void);
// 0x0000010A System.Void Firebase.Firestore.FieldToValueMap::Dispose(System.Boolean)
extern void FieldToValueMap_Dispose_m16D179AABA2F3D3757D4A68D33B83D59110312B7 (void);
// 0x0000010B Firebase.Firestore.FieldToValueMapIterator Firebase.Firestore.FieldToValueMap::Iterator()
extern void FieldToValueMap_Iterator_mD5B928AC07D53D9FDEFE92CFE5715DC5191CCD3B (void);
// 0x0000010C System.Void Firebase.Firestore.FieldToValueMapIterator::.ctor(System.IntPtr,System.Boolean)
extern void FieldToValueMapIterator__ctor_mD6C16102FB2665B2950254CE1D11F74641BC36C3 (void);
// 0x0000010D System.Void Firebase.Firestore.FieldToValueMapIterator::Finalize()
extern void FieldToValueMapIterator_Finalize_m1C3DD3BE660FA9489D96BAFE5A0D9CEE8D9ECD1C (void);
// 0x0000010E System.Void Firebase.Firestore.FieldToValueMapIterator::Dispose()
extern void FieldToValueMapIterator_Dispose_mB8410CBEC466FF8641B455F5FDA9626D9DD7D150 (void);
// 0x0000010F System.Void Firebase.Firestore.FieldToValueMapIterator::Dispose(System.Boolean)
extern void FieldToValueMapIterator_Dispose_mBBCC0977F459B57B10FEE8BDAEDBBA5C46B0D9F0 (void);
// 0x00000110 System.Boolean Firebase.Firestore.FieldToValueMapIterator::HasMore()
extern void FieldToValueMapIterator_HasMore_m1BD9816B14E6E04626494A206038434CF531E8F5 (void);
// 0x00000111 System.Void Firebase.Firestore.FieldToValueMapIterator::Advance()
extern void FieldToValueMapIterator_Advance_m52F40CF691D69876BA083A86569DD351733ABDB8 (void);
// 0x00000112 System.String Firebase.Firestore.FieldToValueMapIterator::UnsafeKeyView()
extern void FieldToValueMapIterator_UnsafeKeyView_m21B2034F632AD3E2F7F0FFEB988553BD074D7668 (void);
// 0x00000113 Firebase.Firestore.FieldValueProxy Firebase.Firestore.FieldToValueMapIterator::UnsafeValueView()
extern void FieldToValueMapIterator_UnsafeValueView_mA83B9FE10713663B767684807396BF9B5A75370B (void);
// 0x00000114 System.Void Firebase.Firestore.DocumentSnapshotVector::.ctor(System.IntPtr,System.Boolean)
extern void DocumentSnapshotVector__ctor_mE70EB9BC37B7FECC125A7A2B6176E24D225E0E33 (void);
// 0x00000115 System.Void Firebase.Firestore.DocumentSnapshotVector::Finalize()
extern void DocumentSnapshotVector_Finalize_m10D40AFFAFA8D1BA0EC00F0EF93FD75FB0A23B9B (void);
// 0x00000116 System.Void Firebase.Firestore.DocumentSnapshotVector::Dispose()
extern void DocumentSnapshotVector_Dispose_mABA2DA4626F8EF532FBD6CC1AE05FAF52D11F9F2 (void);
// 0x00000117 System.Void Firebase.Firestore.DocumentSnapshotVector::Dispose(System.Boolean)
extern void DocumentSnapshotVector_Dispose_m84E231EDD58AAB611B30178312A49831A49249B2 (void);
// 0x00000118 Firebase.Firestore.DocumentSnapshotProxy Firebase.Firestore.DocumentSnapshotVector::GetCopy(System.UInt32)
extern void DocumentSnapshotVector_GetCopy_mEA2C2A2559038E258C51EECA4048D04079871F86 (void);
// 0x00000119 System.Void Firebase.Firestore.FieldValueVector::.ctor(System.IntPtr,System.Boolean)
extern void FieldValueVector__ctor_m470A3350660A2489949E6BDA6345AD5D389AC416 (void);
// 0x0000011A System.Void Firebase.Firestore.FieldValueVector::Finalize()
extern void FieldValueVector_Finalize_m59776D118AA87494D18AEC94F2DB8C5D61E9F944 (void);
// 0x0000011B System.Void Firebase.Firestore.FieldValueVector::Dispose()
extern void FieldValueVector_Dispose_m32C9D7BB9F8F8399AC4E289A2CCEBCB2F0DC67F8 (void);
// 0x0000011C System.Void Firebase.Firestore.FieldValueVector::Dispose(System.Boolean)
extern void FieldValueVector_Dispose_mECF6193F9BF166DEB78C934D1A5DE629CEBF9A69 (void);
// 0x0000011D System.UInt32 Firebase.Firestore.FieldValueVector::Size()
extern void FieldValueVector_Size_m6025FB407D221BE61E57DB3A5AA2FE5C3CEB1E07 (void);
// 0x0000011E Firebase.Firestore.FieldValueProxy Firebase.Firestore.FieldValueVector::GetUnsafeView(System.UInt32)
extern void FieldValueVector_GetUnsafeView_m9967EF1F902FAA891F3E31F6D4643C343C279EDD (void);
// 0x0000011F System.Void Firebase.Firestore.FirestoreCppPINVOKE::.cctor()
extern void FirestoreCppPINVOKE__cctor_m577C00A7CD5549FAA749355E70F5228065B732CF (void);
// 0x00000120 System.Double Firebase.Firestore.FirestoreCppPINVOKE::GeoPointProxy_latitude(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_GeoPointProxy_latitude_mB6CCE638AE64D742E8C9AEAC1E28A34CDEF9181D (void);
// 0x00000121 System.Double Firebase.Firestore.FirestoreCppPINVOKE::GeoPointProxy_longitude(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_GeoPointProxy_longitude_m2A842706520C78F84076A1A03B40EA083D0B6F45 (void);
// 0x00000122 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_GeoPointProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_GeoPointProxy_mD5579740F533F39F5FA4EB99E9244ECD4D7964D0 (void);
// 0x00000123 System.Int64 Firebase.Firestore.FirestoreCppPINVOKE::TimestampProxy_seconds(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_TimestampProxy_seconds_m732369B70B75B220951603FF700452DEF849C632 (void);
// 0x00000124 System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::TimestampProxy_nanoseconds(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_TimestampProxy_nanoseconds_m70B2600575E9C7ED45270ADBBF5E6A7FF3533815 (void);
// 0x00000125 System.String Firebase.Firestore.FirestoreCppPINVOKE::TimestampProxy_ToString(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_TimestampProxy_ToString_m16FDEBB1312BB0CFA83247D0A4DC7C2299E95275 (void);
// 0x00000126 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_TimestampProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_TimestampProxy_m1E5AE67122473E0908E65FFCF159C136A07027BB (void);
// 0x00000127 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::Future_QuerySnapshot_SWIG_OnCompletion(System.Runtime.InteropServices.HandleRef,Firebase.Firestore.Future_QuerySnapshot/SWIG_CompletionDelegate,System.Int32)
extern void FirestoreCppPINVOKE_Future_QuerySnapshot_SWIG_OnCompletion_m94804E41D79FF9CE795F630DB81DC0D875315133 (void);
// 0x00000128 System.Void Firebase.Firestore.FirestoreCppPINVOKE::Future_QuerySnapshot_SWIG_FreeCompletionData(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void FirestoreCppPINVOKE_Future_QuerySnapshot_SWIG_FreeCompletionData_m9559C443EA02AE48F891D9422F2342BC6DE090DB (void);
// 0x00000129 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::Future_QuerySnapshot_GetResult(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_Future_QuerySnapshot_GetResult_m52F9149B799C8ACDAABCBAC160DEE2FCD5E4A5A0 (void);
// 0x0000012A System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_Future_QuerySnapshot(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_Future_QuerySnapshot_m682269BAB71438AA9C39FF4FD1044AEF0B48AC8D (void);
// 0x0000012B System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_DocumentReferenceProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_DocumentReferenceProxy_m8A69E5674E330B61FC81A229AAAFA1D15FDB4F70 (void);
// 0x0000012C System.String Firebase.Firestore.FirestoreCppPINVOKE::DocumentReferenceProxy_id(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentReferenceProxy_id_mFAC24F2623C8E8DFF0B69840C454CC1B42053277 (void);
// 0x0000012D System.String Firebase.Firestore.FirestoreCppPINVOKE::DocumentReferenceProxy_path(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentReferenceProxy_path_m47A1E896F913044C84A569BA6008E89394029140 (void);
// 0x0000012E System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::DocumentReferenceProxy_is_valid(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentReferenceProxy_is_valid_m590FFE87FFA17163E305DE5BE6496F3060036E7E (void);
// 0x0000012F System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_DocumentSnapshotProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_DocumentSnapshotProxy_mC35E663713F62EE2DED860E6EC10A41F54418A45 (void);
// 0x00000130 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::DocumentSnapshotProxy_reference(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentSnapshotProxy_reference_mF464446F6E500A769F4A5286539E78FA13FDA2AA (void);
// 0x00000131 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::DocumentSnapshotProxy_exists(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentSnapshotProxy_exists_mBD7DA1B66D97FFB204907CD0EA16303F54097991 (void);
// 0x00000132 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_FieldValueProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_FieldValueProxy_mBA146A9FDB2756C07BC0848A04C6E4363C84ADC8 (void);
// 0x00000133 System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_type(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_type_m1D627C81F5EE1D186810E7BEB0C0502A2D1871F7 (void);
// 0x00000134 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_is_null(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_is_null_mB2A24582DD819F71F0AE35DFDD63E71490DFE9CC (void);
// 0x00000135 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_boolean_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_boolean_value_m59AF5FF06CDCCB0CD7A09F23B903EEEF84A25EA6 (void);
// 0x00000136 System.Int64 Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_integer_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_integer_value_m84495CD178F910877EA8BAF77A0684B6D1058198 (void);
// 0x00000137 System.Double Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_double_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_double_value_mE24F62B8A0A917C2ED5843A3A3110CE7A896E264 (void);
// 0x00000138 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_timestamp_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_timestamp_value_m88D7790D42AB3ED1BCFBC7EB804D491381BC70C2 (void);
// 0x00000139 System.String Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_string_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_string_value_mFDCE154C41EAF0CE78B2F03C8ACB41E4928C232E (void);
// 0x0000013A System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_blob_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_blob_value_m88E1267F2BB5F469B32811F4BC30614BC816A0C0 (void);
// 0x0000013B System.UInt32 Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_blob_size(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_blob_size_m49C2CF17E630F952A6E8323DF62FD5351CDD7765 (void);
// 0x0000013C System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_reference_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_reference_value_mA8D39A1B8741BA03492F75D72F92D2912F4C8F5E (void);
// 0x0000013D System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_geo_point_value(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueProxy_geo_point_value_m515D4D98AD1253E36CE86D37B2D71071616E3FB2 (void);
// 0x0000013E System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldValueProxy_ServerTimestamp()
extern void FirestoreCppPINVOKE_FieldValueProxy_ServerTimestamp_m34C0FBC6EC196D317BC583FEBEF5A7F8FDC09AEE (void);
// 0x0000013F System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_QueryProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_QueryProxy_m3CC14B4DE5F692426CF5AC816CF7A36594D02EC0 (void);
// 0x00000140 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::QueryProxy_Get__SWIG_0(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void FirestoreCppPINVOKE_QueryProxy_Get__SWIG_0_m974FBF73F94964C4CC4DC0864CFB360E320B3D4D (void);
// 0x00000141 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_QuerySnapshotProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_QuerySnapshotProxy_m3C340D7F4B6F3543079EADBEE7D02E1F8238D2FA (void);
// 0x00000142 System.UInt32 Firebase.Firestore.FirestoreCppPINVOKE::QuerySnapshotProxy_size(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_QuerySnapshotProxy_size_m905545AD4D76104A3EFAEE929726256DB454D2E5 (void);
// 0x00000143 System.Int64 Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_kCacheSizeUnlimited_get()
extern void FirestoreCppPINVOKE_SettingsProxy_kCacheSizeUnlimited_get_m60AB292DAC6E095B8CBC2A46DCF57657DEB14432 (void);
// 0x00000144 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::new_SettingsProxy__SWIG_0()
extern void FirestoreCppPINVOKE_new_SettingsProxy__SWIG_0_mA6BE0B0778FC09B636AF1AE99D79BF245CAD7B46 (void);
// 0x00000145 System.String Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_host(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_SettingsProxy_host_m64FAEDE9BADDECF44508FF20694283945B569644 (void);
// 0x00000146 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_is_ssl_enabled(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_SettingsProxy_is_ssl_enabled_m58F1ACF926B6EC945A1676A7BBC9ED604C5122D2 (void);
// 0x00000147 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_is_persistence_enabled(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_SettingsProxy_is_persistence_enabled_m306CFECE628778A30D17DC59BC458F091EC70C4A (void);
// 0x00000148 System.Int64 Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_cache_size_bytes(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_SettingsProxy_cache_size_bytes_m5BAAC2A21A894932F8F10F4F1F8847DE03BE53A8 (void);
// 0x00000149 System.Void Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_set_host(System.Runtime.InteropServices.HandleRef,System.String)
extern void FirestoreCppPINVOKE_SettingsProxy_set_host_m541D3AE1A6E97C0823E11E13E8F61AE8E7614E7E (void);
// 0x0000014A System.Void Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_set_ssl_enabled(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void FirestoreCppPINVOKE_SettingsProxy_set_ssl_enabled_mC673F87371DB4FDE4688240069BB0EA787063AC4 (void);
// 0x0000014B System.Void Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_set_persistence_enabled(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern void FirestoreCppPINVOKE_SettingsProxy_set_persistence_enabled_m68F926AF8EB51884E306B9623F972CCBCC2AF2EA (void);
// 0x0000014C System.Void Firebase.Firestore.FirestoreCppPINVOKE::SettingsProxy_set_cache_size_bytes(System.Runtime.InteropServices.HandleRef,System.Int64)
extern void FirestoreCppPINVOKE_SettingsProxy_set_cache_size_bytes_m12DFB5CAB7AB63957C4CB609BAB6051509EA6B1E (void);
// 0x0000014D System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_SettingsProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_SettingsProxy_mAE2D0376BE9770810CD4852E9059607BD1839890 (void);
// 0x0000014E System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::LoadBundleTaskProgressProxy_documents_loaded(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_documents_loaded_mB09A1C1F15C2A57C2AB6B0863D182DE0C054223B (void);
// 0x0000014F System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::LoadBundleTaskProgressProxy_total_documents(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_total_documents_m7FBF86A7D81E658583B4381E965A60B533321B39 (void);
// 0x00000150 System.Int64 Firebase.Firestore.FirestoreCppPINVOKE::LoadBundleTaskProgressProxy_bytes_loaded(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_bytes_loaded_m1D1E5DE2F1700B970EAA162FEC9B5A6C3308BBFE (void);
// 0x00000151 System.Int64 Firebase.Firestore.FirestoreCppPINVOKE::LoadBundleTaskProgressProxy_total_bytes(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_total_bytes_m71F3391EC2D2E204942F6F8460AB8C2DBDBA0E55 (void);
// 0x00000152 System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::LoadBundleTaskProgressProxy_state(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_state_m717C1E7627C3016E6C6FCE3A73D7973918FCD24B (void);
// 0x00000153 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_LoadBundleTaskProgressProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_LoadBundleTaskProgressProxy_m03B1E486BBD2695FC99703448344065D51FC0631 (void);
// 0x00000154 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FirestoreProxy_GetInstance__SWIG_1(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FirestoreProxy_GetInstance__SWIG_1_mE1595790722C877222C15EB939B3F08037E66D7B (void);
// 0x00000155 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_FirestoreProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_FirestoreProxy_mF71A6690E52EC06178D68E0B15A3F5B778F4BBFB (void);
// 0x00000156 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FirestoreProxy_CollectionGroup__SWIG_0(System.Runtime.InteropServices.HandleRef,System.String)
extern void FirestoreCppPINVOKE_FirestoreProxy_CollectionGroup__SWIG_0_m993D69F158A995610C9651B2E99C462548BCEF15 (void);
// 0x00000157 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FirestoreProxy_settings(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FirestoreProxy_settings_mA004D75326FF2536B748FA3BE8A708151B3B7B45 (void);
// 0x00000158 System.Void Firebase.Firestore.FirestoreCppPINVOKE::FirestoreProxy_set_settings(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FirestoreProxy_set_settings_mAA19975048F95D218741CC6C23F2F423FE673677 (void);
// 0x00000159 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::QueryEquals(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_QueryEquals_mC63706486270D02EB781865DBD61850C397C83B5 (void);
// 0x0000015A System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::QuerySnapshotEquals(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_QuerySnapshotEquals_mF4A654C5E162F6CEDB8F87B583849A70D3F88467 (void);
// 0x0000015B System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::DocumentSnapshotEquals(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentSnapshotEquals_m9AD6A0988A915B245C343B9E407E6A4844F1A4DE (void);
// 0x0000015C System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::QueryHashCode(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_QueryHashCode_mCC2B92BC7D942E9E26E1E66DC796BF4BCB02EB5B (void);
// 0x0000015D System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::QuerySnapshotHashCode(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_QuerySnapshotHashCode_m8FBF5FD1D9185A8856A2F459BAD19185B90A65AB (void);
// 0x0000015E System.Int32 Firebase.Firestore.FirestoreCppPINVOKE::DocumentSnapshotHashCode(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_DocumentSnapshotHashCode_mDC50ED8304FABD8CB65E9721BFAF976D14EF648C (void);
// 0x0000015F System.Void Firebase.Firestore.FirestoreCppPINVOKE::ApiHeaders_SetClientLanguage(System.String)
extern void FirestoreCppPINVOKE_ApiHeaders_SetClientLanguage_mAED04C8052279387DAE8A6A92B453832ECA40010 (void);
// 0x00000160 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::ConvertFieldValueToMap(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_ConvertFieldValueToMap_mA755FAB940F1313440F5CDEA38B6E976B4265D3E (void);
// 0x00000161 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::ConvertSnapshotToFieldValue(System.Runtime.InteropServices.HandleRef,System.Int32)
extern void FirestoreCppPINVOKE_ConvertSnapshotToFieldValue_m22C0FCF93B14E4F2FAF58DA4C9546414339B0DFD (void);
// 0x00000162 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_TransactionCallbackProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_TransactionCallbackProxy_m1BEC73F04259FA17229463C43167FB2FFCAC3D64 (void);
// 0x00000163 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::new_TransactionManagerProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_new_TransactionManagerProxy_m200FB31B949492773422F816C8C9D83901F46979 (void);
// 0x00000164 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_TransactionManagerProxy(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_TransactionManagerProxy_mA1BD29437A086FD0A1BFDA9E7F8AD7DD2EC1165B (void);
// 0x00000165 System.Void Firebase.Firestore.FirestoreCppPINVOKE::TransactionManagerProxy_CppDispose(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_TransactionManagerProxy_CppDispose_mA2809A796E9275BDD1F0CAFEDA9DA943B4B35B4C (void);
// 0x00000166 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldToValueMap_Iterator(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldToValueMap_Iterator_mACD511189356E195CABE8CC1874AB3298F31FE84 (void);
// 0x00000167 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_FieldToValueMap(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_FieldToValueMap_mF04DED31573FAB5955094C35EC8FD7F5BFC5E9DD (void);
// 0x00000168 System.Boolean Firebase.Firestore.FirestoreCppPINVOKE::FieldToValueMapIterator_HasMore(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldToValueMapIterator_HasMore_m4EB378D64CDD5D5893BDF41B49B2AE9145C1037C (void);
// 0x00000169 System.Void Firebase.Firestore.FirestoreCppPINVOKE::FieldToValueMapIterator_Advance(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldToValueMapIterator_Advance_mDF0C26523CA6D4AEFBE2DE328DD0E104C9316670 (void);
// 0x0000016A System.String Firebase.Firestore.FirestoreCppPINVOKE::FieldToValueMapIterator_UnsafeKeyView(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldToValueMapIterator_UnsafeKeyView_m7012DA04F689FC79C886878CF941C78C0C4BF3D1 (void);
// 0x0000016B System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldToValueMapIterator_UnsafeValueView(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldToValueMapIterator_UnsafeValueView_m3DB4723764C23CFC7861ED1F73388D6B0428569A (void);
// 0x0000016C System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_FieldToValueMapIterator(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_FieldToValueMapIterator_m03BAC72E6A785D3528B92C75DE93C9EFBE5153BC (void);
// 0x0000016D System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::ConvertFieldValueToVector(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_ConvertFieldValueToVector_mBA6762E2018B251AB202482FF23339CBD7C0C3D1 (void);
// 0x0000016E System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::QuerySnapshotDocuments(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_QuerySnapshotDocuments_m2D55EDD87FB9E2773E23A0BCD79234F588E4F9BB (void);
// 0x0000016F System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::DocumentSnapshotVector_GetCopy(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern void FirestoreCppPINVOKE_DocumentSnapshotVector_GetCopy_mA81A1E3022BC46CE132CA083513E626F30AF2BD5 (void);
// 0x00000170 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_DocumentSnapshotVector(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_DocumentSnapshotVector_mAB18D783470DA2812C15C5AED9907A5719C91125 (void);
// 0x00000171 System.UInt32 Firebase.Firestore.FirestoreCppPINVOKE::FieldValueVector_Size(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_FieldValueVector_Size_mA5E22CD841D28236FB7EE576F4BE11E80AC236C2 (void);
// 0x00000172 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::FieldValueVector_GetUnsafeView(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern void FirestoreCppPINVOKE_FieldValueVector_GetUnsafeView_m33CFD025F82EB22E7338297D8AA5FDF0D271D36B (void);
// 0x00000173 System.Void Firebase.Firestore.FirestoreCppPINVOKE::delete_FieldValueVector(System.Runtime.InteropServices.HandleRef)
extern void FirestoreCppPINVOKE_delete_FieldValueVector_m103D29FE0BB27CD7A3841121F460AB3F31BB7C66 (void);
// 0x00000174 System.IntPtr Firebase.Firestore.FirestoreCppPINVOKE::Future_QuerySnapshot_SWIGUpcast(System.IntPtr)
extern void FirestoreCppPINVOKE_Future_QuerySnapshot_SWIGUpcast_mE83DF18AE623C9F24B94CF522AE5B60892D138FB (void);
// 0x00000175 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_FirestoreCpp(Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_FirestoreCpp_mA27140A6DF79C4D73741BB8CBBA31E55EDBC3D48 (void);
// 0x00000176 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_FirestoreCpp(Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_FirestoreCpp_m286156D22FD52C780B7C76C17E7F90B597B3FBC8 (void);
// 0x00000177 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern void SWIGExceptionHelper_SetPendingApplicationException_mBF137E9081B46089C8BD08A3FFEE1E0FE1F0E3A8 (void);
// 0x00000178 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern void SWIGExceptionHelper_SetPendingArithmeticException_mF99EDA71B878CF6361E9564B89784B3A561FFA52 (void);
// 0x00000179 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern void SWIGExceptionHelper_SetPendingDivideByZeroException_mCE9772E2D30699F715F2661C75C5EF14C086E9E8 (void);
// 0x0000017A System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m921D13D0A68FB4EBC4056309B8E3449B10747CEB (void);
// 0x0000017B System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidCastException_m9947EBEA346C986795F984A0BFEE5CF35F5B6D34 (void);
// 0x0000017C System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidOperationException_m4BF2D51569FE111F005303CC1F1B4092A7543587 (void);
// 0x0000017D System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
extern void SWIGExceptionHelper_SetPendingIOException_m12107E67FA15993E46B0E20B06151035CCE6E143 (void);
// 0x0000017E System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern void SWIGExceptionHelper_SetPendingNullReferenceException_m0CAF64A4594D264026F68FB9A835A0BC1FADD791 (void);
// 0x0000017F System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern void SWIGExceptionHelper_SetPendingOutOfMemoryException_m514E19096CE25864CBF0CB58DF723C735A7667D9 (void);
// 0x00000180 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern void SWIGExceptionHelper_SetPendingOverflowException_m7B3159CEF1FD67EB6B7B517C020441417888E1AC (void);
// 0x00000181 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
extern void SWIGExceptionHelper_SetPendingSystemException_m011EB1DF3148DBBBC161AC80CFCBB47D888CB10D (void);
// 0x00000182 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentException_m927C534E2487873AF124759896433BDAC30405C2 (void);
// 0x00000183 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentNullException_m338ED5CC0C522534644D5024F1B68F1C09E9FE75 (void);
// 0x00000184 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m7661B82740D6603B2BF70398B38A72E05D4FA57B (void);
// 0x00000185 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::.cctor()
extern void SWIGExceptionHelper__cctor_mAECB4852A7ABEFD75C0F696024F095D6D78E3F93 (void);
// 0x00000186 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper::.ctor()
extern void SWIGExceptionHelper__ctor_m8120EF656206FC9E220CC23DFD3C5C28ED53CBC1 (void);
// 0x00000187 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionDelegate__ctor_m7C66927063A66072331A88F3D6D32F75A21E5FDA (void);
// 0x00000188 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
extern void ExceptionDelegate_Invoke_m56700204F1F736396D178FFE8E2FDAF3149A6AD2 (void);
// 0x00000189 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionArgumentDelegate__ctor_m65357F61679FCC25B814208939F292E587C46BC6 (void);
// 0x0000018A System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern void ExceptionArgumentDelegate_Invoke_m32DAE56901FA62335569CAF69E436140D0A90137 (void);
// 0x0000018B System.Boolean Firebase.Firestore.FirestoreCppPINVOKE/SWIGPendingException::get_Pending()
extern void SWIGPendingException_get_Pending_m214FDF3047410EA90174F6E19ABC3C2D8E2956EF (void);
// 0x0000018C System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGPendingException::Set(System.Exception)
extern void SWIGPendingException_Set_mA9B1EB08A214DA9373CBB0993566FCB150DC41D8 (void);
// 0x0000018D System.Exception Firebase.Firestore.FirestoreCppPINVOKE/SWIGPendingException::Retrieve()
extern void SWIGPendingException_Retrieve_m978AEFFD56BD51D4AD84CF3C84CE4D03E1E029ED (void);
// 0x0000018E System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGPendingException::.cctor()
extern void SWIGPendingException__cctor_m61BE33ED53C2EACB33D680F2BBE0EDDF06C3482D (void);
// 0x0000018F System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_FirestoreCpp(Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern void SWIGStringHelper_SWIGRegisterStringCallback_FirestoreCpp_m6727393A56A336E69FE33B642376FCBAA03764FE (void);
// 0x00000190 System.String Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper::CreateString(System.String)
extern void SWIGStringHelper_CreateString_m0B36085212BD3538E5F889D7EB968E3AC83D5049 (void);
// 0x00000191 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper::.cctor()
extern void SWIGStringHelper__cctor_m6CA2B13BE11737CABAF6A0B2446791854802881F (void);
// 0x00000192 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper::.ctor()
extern void SWIGStringHelper__ctor_mDAD5A170B29364E400EF22B098A5CD1474573EB9 (void);
// 0x00000193 System.Void Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIGStringDelegate__ctor_mEFB8B3340012F9D4F2DF72E8AEC8CBC918B4849C (void);
// 0x00000194 System.String Firebase.Firestore.FirestoreCppPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
extern void SWIGStringDelegate_Invoke_mA4BC2B23570BA95DB601F35A04DD2845D71A9BDD (void);
// 0x00000195 System.Void Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper::FirestoreExceptionRegisterCallback(Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper/FirestoreExceptionDelegate)
extern void FirestoreExceptionHelper_FirestoreExceptionRegisterCallback_m66A2D863877C0B04C08A4E9E0755CE5CF1F201C6 (void);
// 0x00000196 System.Void Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper::SetPendingFirestoreException(System.String)
extern void FirestoreExceptionHelper_SetPendingFirestoreException_m87C105F291E79CF2C83734D34986011FF88B8E0E (void);
// 0x00000197 System.Void Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper::.cctor()
extern void FirestoreExceptionHelper__cctor_m4B519A31665D539F158D42E743ED908E434E82D5 (void);
// 0x00000198 System.Void Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper::.ctor()
extern void FirestoreExceptionHelper__ctor_mA3DF4F6A7DE67BD7CD0232592519D59EE1783CD3 (void);
// 0x00000199 System.Void Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper/FirestoreExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern void FirestoreExceptionDelegate__ctor_mFBFC4C7ED1ECB09D7C1BF5F893FAF8BDDB1DFCE3 (void);
// 0x0000019A System.Void Firebase.Firestore.FirestoreCppPINVOKE/FirestoreExceptionHelper/FirestoreExceptionDelegate::Invoke(System.String)
extern void FirestoreExceptionDelegate_Invoke_mBB306AE8E18D8B268FD1FCD2B6310F7EC866CE6F (void);
// 0x0000019B System.Boolean Firebase.Firestore.FirestoreCpp::QueryEquals(Firebase.Firestore.QueryProxy,Firebase.Firestore.QueryProxy)
extern void FirestoreCpp_QueryEquals_m05700E01B1139E67DA2C0DB6A796FDC8E8C84A9E (void);
// 0x0000019C System.Boolean Firebase.Firestore.FirestoreCpp::QuerySnapshotEquals(Firebase.Firestore.QuerySnapshotProxy,Firebase.Firestore.QuerySnapshotProxy)
extern void FirestoreCpp_QuerySnapshotEquals_mED2AE6C7140A1FDB4E3E2EE0759382347005D443 (void);
// 0x0000019D System.Boolean Firebase.Firestore.FirestoreCpp::DocumentSnapshotEquals(Firebase.Firestore.DocumentSnapshotProxy,Firebase.Firestore.DocumentSnapshotProxy)
extern void FirestoreCpp_DocumentSnapshotEquals_m47F73AF9C8A7EFE1941809448C6D727DFFACFD4D (void);
// 0x0000019E System.Int32 Firebase.Firestore.FirestoreCpp::QueryHashCode(Firebase.Firestore.QueryProxy)
extern void FirestoreCpp_QueryHashCode_m93D9495962C8C40D5A1290F835CAD4260B9F3F98 (void);
// 0x0000019F System.Int32 Firebase.Firestore.FirestoreCpp::QuerySnapshotHashCode(Firebase.Firestore.QuerySnapshotProxy)
extern void FirestoreCpp_QuerySnapshotHashCode_m6BEFED4A64C006D3261A4497A489D5D65E3EEC72 (void);
// 0x000001A0 System.Int32 Firebase.Firestore.FirestoreCpp::DocumentSnapshotHashCode(Firebase.Firestore.DocumentSnapshotProxy)
extern void FirestoreCpp_DocumentSnapshotHashCode_m93DCEC6ABE286CC9AA3563B18869F93C04898273 (void);
// 0x000001A1 Firebase.Firestore.FieldToValueMap Firebase.Firestore.FirestoreCpp::ConvertFieldValueToMap(Firebase.Firestore.FieldValueProxy)
extern void FirestoreCpp_ConvertFieldValueToMap_mBB3C7530B1A5B61F2D5DE1C7F2CFEA8821963E7F (void);
// 0x000001A2 Firebase.Firestore.FieldValueProxy Firebase.Firestore.FirestoreCpp::ConvertSnapshotToFieldValue(Firebase.Firestore.DocumentSnapshotProxy,Firebase.Firestore.DocumentSnapshotProxy/ServerTimestampBehavior)
extern void FirestoreCpp_ConvertSnapshotToFieldValue_mF71FF010520F1F76FB334CC9F0FD79E00803C70F (void);
// 0x000001A3 Firebase.Firestore.FieldValueVector Firebase.Firestore.FirestoreCpp::ConvertFieldValueToVector(Firebase.Firestore.FieldValueProxy)
extern void FirestoreCpp_ConvertFieldValueToVector_mDF6DB3E374E8035FECADF0BA3228A5053A77D5B5 (void);
// 0x000001A4 Firebase.Firestore.DocumentSnapshotVector Firebase.Firestore.FirestoreCpp::QuerySnapshotDocuments(Firebase.Firestore.QuerySnapshotProxy)
extern void FirestoreCpp_QuerySnapshotDocuments_mCAE514D7F89F2E30F88D6D5B45B8A390EDA39426 (void);
// 0x000001A5 System.Void Firebase.Firestore.SWIGTYPE_p_unsigned_char::.ctor(System.IntPtr,System.Boolean)
extern void SWIGTYPE_p_unsigned_char__ctor_mE6B6E3D2C65338756D9BD3C80BAF63A6D6E9D16E (void);
// 0x000001A6 System.Runtime.InteropServices.HandleRef Firebase.Firestore.SWIGTYPE_p_unsigned_char::getCPtr(Firebase.Firestore.SWIGTYPE_p_unsigned_char)
extern void SWIGTYPE_p_unsigned_char_getCPtr_m5EAFEE91F7D6C12936AB5441350878116A9DC7A7 (void);
// 0x000001A7 System.Void Firebase.Firestore.Internal.AssertFailedException::.ctor(System.String)
extern void AssertFailedException__ctor_m53F4F8F9955824F936C29CFDA9347EC97BE47469 (void);
// 0x000001A8 Firebase.Firestore.SourceProxy Firebase.Firestore.Internal.Enums::Convert(Firebase.Firestore.Source)
extern void Enums_Convert_mDF7518836B489867A57B6AC55CBAB8BB97092381 (void);
// 0x000001A9 System.String Firebase.Firestore.Internal.EnvironmentVersion::GetEnvironmentVersion()
extern void EnvironmentVersion_GetEnvironmentVersion_m8079C13B590EF357E6E2D48680AE56ED262E1987 (void);
// 0x000001AA System.String Firebase.Firestore.Internal.EnvironmentVersion::FormatVersion(System.Version)
extern void EnvironmentVersion_FormatVersion_mAB4A3FA14785D545B2F17C1F01412B04AF2F71C1 (void);
// 0x000001AB System.Int32 Firebase.Firestore.Internal.Hash::LongHash(System.Int64)
extern void Hash_LongHash_mACE6AC6CFBEBFB1C97AA507D263B261F10AE52DB (void);
// 0x000001AC System.Int32 Firebase.Firestore.Internal.Hash::DoubleBitwiseHash(System.Double)
extern void Hash_DoubleBitwiseHash_m4CBF0BA79E2D5F729871F8A2DFAA16A3E9A3B6F5 (void);
// 0x000001AD T Firebase.Firestore.Internal.Preconditions::CheckNotNull(T,System.String)
// 0x000001AE System.String Firebase.Firestore.Internal.Preconditions::CheckNotNullOrEmpty(System.String,System.String)
extern void Preconditions_CheckNotNullOrEmpty_m79C2D067DA82E7E340C2B4EE3DB5869169D6A088 (void);
// 0x000001AF System.Void Firebase.Firestore.Internal.Preconditions::CheckState(System.Boolean,System.String)
extern void Preconditions_CheckState_mA468CDAAF8FBADE46412DAA9928628CBC2C506AC (void);
// 0x000001B0 System.Void Firebase.Firestore.Internal.Preconditions::CheckState(System.Boolean,System.String,T)
// 0x000001B1 System.Void Firebase.Firestore.Internal.Preconditions::CheckState(System.Boolean,System.String,T1,T2)
// 0x000001B2 System.Void Firebase.Firestore.Internal.Preconditions::CheckState(System.Boolean,System.String,T1,T2,T3)
// 0x000001B3 System.Void Firebase.Firestore.Internal.Util::Unreachable()
extern void Util_Unreachable_mD59FBB441C46A6F9107EE1DE7C1773AFC2BB8268 (void);
// 0x000001B4 System.Void Firebase.Firestore.Internal.Util::HardAssert(System.Boolean,System.String)
extern void Util_HardAssert_m2985B5883E2CDE0DEE3610911C1F1722D3383A31 (void);
// 0x000001B5 T Firebase.Firestore.Internal.Util::NotNull(T,System.String)
// 0x000001B6 System.Threading.Tasks.Task`1<U> Firebase.Firestore.Internal.Util::MapResult(System.Threading.Tasks.Task`1<T>,System.Func`2<T,U>)
// 0x000001B7 System.Void Firebase.Firestore.Internal.Util::FlattenAndThrowException(System.Threading.Tasks.Task)
extern void Util_FlattenAndThrowException_m7D52B8696CDD935D460CA99541439DF432A6F125 (void);
// 0x000001B8 System.Exception Firebase.Firestore.Internal.Util::FlattenException(System.AggregateException)
extern void Util_FlattenException_m1DCF1E810751C25ABF54EB7EC1DC96305B8D3523 (void);
// 0x000001B9 System.Void Firebase.Firestore.Internal.Util::OnPInvokeManagedException(System.Exception,System.String)
extern void Util_OnPInvokeManagedException_m1EC60F6DEA68C69DFB91142EE44AFA83C59C320D (void);
// 0x000001BA System.Void Firebase.Firestore.Internal.Util/<>c__DisplayClass5_0`2::.ctor()
// 0x000001BB U Firebase.Firestore.Internal.Util/<>c__DisplayClass5_0`2::<MapResult>b__0(System.Threading.Tasks.Task`1<T>)
// 0x000001BC System.Void Firebase.Firestore.Converters.AnonymousTypeConverter::.ctor(System.Type)
extern void AnonymousTypeConverter__ctor_mEF865BE7E57D4B16F980D653C62945FB9DC44525 (void);
// 0x000001BD System.Void Firebase.Firestore.Converters.ArrayConverter::.ctor(System.Type)
extern void ArrayConverter__ctor_mACC03F7B4B29095E912908B41C3365F1D098F777 (void);
// 0x000001BE System.Object Firebase.Firestore.Converters.ArrayConverter::DeserializeArray(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
extern void ArrayConverter_DeserializeArray_mE3E9E2742F88D61B590C14DAA61C56984CAF9B94 (void);
// 0x000001BF System.Void Firebase.Firestore.Converters.AttributedIdAssigner::.ctor(System.Collections.Generic.List`1<System.Reflection.PropertyInfo>,System.Collections.Generic.List`1<System.Reflection.PropertyInfo>)
extern void AttributedIdAssigner__ctor_m37704510524F7EED5EC0BDFC7431D280D227ADB8 (void);
// 0x000001C0 System.Void Firebase.Firestore.Converters.AttributedIdAssigner::AssignId(System.Object,Firebase.Firestore.DocumentReference)
extern void AttributedIdAssigner_AssignId_mDBDBD1986389B23C384E9E19A4FAB6843894C905 (void);
// 0x000001C1 System.Void Firebase.Firestore.Converters.AttributedIdAssigner::MaybeAssignId(System.Object,Firebase.Firestore.DocumentReference)
extern void AttributedIdAssigner_MaybeAssignId_m9995E21FE3872185FE173792B6F43F438841C5D9 (void);
// 0x000001C2 Firebase.Firestore.Converters.AttributedIdAssigner Firebase.Firestore.Converters.AttributedIdAssigner::MaybeCreateAssigner(System.Type)
extern void AttributedIdAssigner_MaybeCreateAssigner_m18F5351C199820F835442862D483F226E1BBE88F (void);
// 0x000001C3 System.Void Firebase.Firestore.Converters.AttributedIdAssigner::.cctor()
extern void AttributedIdAssigner__cctor_m535854BB99F4A88E5F3942CF02AD720A9C048DE1 (void);
// 0x000001C4 System.Void Firebase.Firestore.Converters.AttributedTypeConverter::.ctor(System.Type,Firebase.Firestore.FirestoreDataAttribute)
extern void AttributedTypeConverter__ctor_m8ED5C7FC9F0F52604F44A95E5108B68A80E8870C (void);
// 0x000001C5 System.Func`1<System.Object> Firebase.Firestore.Converters.AttributedTypeConverter::CreateObjectCreator(System.Type)
extern void AttributedTypeConverter_CreateObjectCreator_m82D8C56AFBB749282F684509F38F2F9F22F85698 (void);
// 0x000001C6 Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.Converters.AttributedTypeConverter::ForType(System.Type)
extern void AttributedTypeConverter_ForType_m07D59D9656406621D387022B649B40E618835B89 (void);
// 0x000001C7 System.Object Firebase.Firestore.Converters.AttributedTypeConverter::DeserializeMap(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
extern void AttributedTypeConverter_DeserializeMap_m2A394C4D7866BE755035DC2B4DDD1F23B821CADD (void);
// 0x000001C8 System.Boolean Firebase.Firestore.Converters.AttributedTypeConverter/AttributedProperty::get_CanRead()
extern void AttributedProperty_get_CanRead_m0ABE4E1718B14CA2B5D0298793EF68602D260ABF (void);
// 0x000001C9 System.Boolean Firebase.Firestore.Converters.AttributedTypeConverter/AttributedProperty::get_CanWrite()
extern void AttributedProperty_get_CanWrite_m7B6AE4074BCDE642DD62CED67EC91624C9554D3F (void);
// 0x000001CA System.Boolean Firebase.Firestore.Converters.AttributedTypeConverter/AttributedProperty::get_IsNullableValue()
extern void AttributedProperty_get_IsNullableValue_mD3B48A746DD86E1B133FFE657E887A0589111149 (void);
// 0x000001CB System.Void Firebase.Firestore.Converters.AttributedTypeConverter/AttributedProperty::.ctor(System.Reflection.PropertyInfo,Firebase.Firestore.FirestorePropertyAttribute)
extern void AttributedProperty__ctor_m7864D6112D7B8CB02FFDFDB470602784CBD621E6 (void);
// 0x000001CC System.Void Firebase.Firestore.Converters.AttributedTypeConverter/AttributedProperty::SetValue(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy,System.Object)
extern void AttributedProperty_SetValue_mCB4494D165F52CC87E3F1F74CFC85DBCCF0C17D1 (void);
// 0x000001CD System.Void Firebase.Firestore.Converters.AttributedTypeConverter/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m349C91DBF3C7A213C6107A73EB4FA595C59E64C7 (void);
// 0x000001CE System.Boolean Firebase.Firestore.Converters.AttributedTypeConverter/<>c__DisplayClass4_0::<.ctor>b__0(Firebase.Firestore.Converters.AttributedTypeConverter/AttributedProperty)
extern void U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__0_m14035490516D2CBF4F9D4D01C2EF2C0291E349B1 (void);
// 0x000001CF System.Void Firebase.Firestore.Converters.AttributedTypeConverter/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mCDF851508E01B21AC9D4E9EA8B267F2D6AA21B81 (void);
// 0x000001D0 System.Object Firebase.Firestore.Converters.AttributedTypeConverter/<>c__DisplayClass5_0::<CreateObjectCreator>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CCreateObjectCreatorU3Eb__0_m24C3F17F9CC5F0A16CD3CA566C88729EB1ADBC97 (void);
// 0x000001D1 System.Void Firebase.Firestore.Converters.AttributedTypeConverter/<>c__DisplayClass5_1::.ctor()
extern void U3CU3Ec__DisplayClass5_1__ctor_mC1569654AB635817480C5F88AA8AD1C3970CED69 (void);
// 0x000001D2 System.Object Firebase.Firestore.Converters.AttributedTypeConverter/<>c__DisplayClass5_1::<CreateObjectCreator>b__2()
extern void U3CU3Ec__DisplayClass5_1_U3CCreateObjectCreatorU3Eb__2_mC677382339E7FD82D5EABCC8BEEAF205C83E8168 (void);
// 0x000001D3 System.Void Firebase.Firestore.Converters.AttributedTypeConverter/<>c::.cctor()
extern void U3CU3Ec__cctor_mBEA010AF775B54CCF304C5A88CDECA9542235EF3 (void);
// 0x000001D4 System.Void Firebase.Firestore.Converters.AttributedTypeConverter/<>c::.ctor()
extern void U3CU3Ec__ctor_mF6E3ECEB7BDA7E96E52FE5485CEDBB656EE498C2 (void);
// 0x000001D5 System.Boolean Firebase.Firestore.Converters.AttributedTypeConverter/<>c::<CreateObjectCreator>b__5_1(System.Reflection.ConstructorInfo)
extern void U3CU3Ec_U3CCreateObjectCreatorU3Eb__5_1_m688C1C95364110D2733F57F69140770F0B4B4DB4 (void);
// 0x000001D6 System.Void Firebase.Firestore.Converters.ConverterBase::.ctor(System.Type)
extern void ConverterBase__ctor_m0D2AA36F632B6CAE14B9AF2F314AE5A6296790DE (void);
// 0x000001D7 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeMap(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
extern void ConverterBase_DeserializeMap_m5D4F1A2FCAA936F6626B46FB1C5BC965DA9F29F3 (void);
// 0x000001D8 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeValue(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
extern void ConverterBase_DeserializeValue_mEB14B633EF521927E9427E239DCEB7257A8E2A18 (void);
// 0x000001D9 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeArray(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
extern void ConverterBase_DeserializeArray_mAFAD2E5E37435366E56AD61031917E20298DB828 (void);
// 0x000001DA System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeBoolean(Firebase.Firestore.DeserializationContext,System.Boolean)
extern void ConverterBase_DeserializeBoolean_m1E01A7A72D9D085F2093EF95B2E5A76159781FBF (void);
// 0x000001DB System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeBytes(Firebase.Firestore.DeserializationContext,System.Byte[])
extern void ConverterBase_DeserializeBytes_mE0C65760127B880226EC869DD5A6EA448ABA8F94 (void);
// 0x000001DC System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeDouble(Firebase.Firestore.DeserializationContext,System.Double)
extern void ConverterBase_DeserializeDouble_mD02A4030DA90AD0B9FB861E2E8F4EE4C56369B96 (void);
// 0x000001DD System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeGeoPoint(Firebase.Firestore.DeserializationContext,Firebase.Firestore.GeoPoint)
extern void ConverterBase_DeserializeGeoPoint_m066E266508A487610272DE05D84144423ED59D42 (void);
// 0x000001DE System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void ConverterBase_DeserializeInteger_mD4AE7FB7A3EC050A027369BE54D71CD6971A745F (void);
// 0x000001DF System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeReference(Firebase.Firestore.DeserializationContext,Firebase.Firestore.DocumentReference)
extern void ConverterBase_DeserializeReference_mC063992C08974A1FDD8EF8204422B776682074B6 (void);
// 0x000001E0 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeString(Firebase.Firestore.DeserializationContext,System.String)
extern void ConverterBase_DeserializeString_m83BB735D46658CE5A610E1C638D32D492ACA913A (void);
// 0x000001E1 System.Object Firebase.Firestore.Converters.ConverterBase::DeserializeTimestamp(Firebase.Firestore.DeserializationContext,Firebase.Firestore.Timestamp)
extern void ConverterBase_DeserializeTimestamp_mE9EB5C9C86EF07C2BD611E4731B551AF7FFF3136 (void);
// 0x000001E2 System.Byte[] Firebase.Firestore.Converters.ConverterBase::ConvertFromProxyBlob(Firebase.Firestore.FieldValueProxy)
extern void ConverterBase_ConvertFromProxyBlob_m0A0D9B0E65EC0373F55FAD5D2F6C76F92005BB9C (void);
// 0x000001E3 Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.Converters.ConverterCache::GetConverter(System.Type)
extern void ConverterCache_GetConverter_m0B3642BC52764BACEE8ADA68CE24F68EB27C96FE (void);
// 0x000001E4 System.Void Firebase.Firestore.Converters.ConverterCache::InitializeConverterCache()
extern void ConverterCache_InitializeConverterCache_m6EE88E6B7A63506CBA8E344CBFD0C14A6D105CBE (void);
// 0x000001E5 Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.Converters.ConverterCache::CreateDictionaryConverter(System.Type)
// 0x000001E6 Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.Converters.ConverterCache::CreateConverter(System.Type)
extern void ConverterCache_CreateConverter_m0803325754F2718826081886D9C362D1EC34C8D5 (void);
// 0x000001E7 System.Boolean Firebase.Firestore.Converters.ConverterCache::TryGetStringDictionaryValueType(System.Type,System.Type&)
extern void ConverterCache_TryGetStringDictionaryValueType_m0D9AAF394A228716120BE813893CBC8938AE18F7 (void);
// 0x000001E8 System.Type Firebase.Firestore.Converters.ConverterCache::MapInterfaceToDictionaryValueTypeArgument(System.Type)
extern void ConverterCache_MapInterfaceToDictionaryValueTypeArgument_mCC59159961ECC84D5DC8E017D56F3E817B9333F0 (void);
// 0x000001E9 System.Void Firebase.Firestore.Converters.ConverterCache::.cctor()
extern void ConverterCache__cctor_mB856CC23D3F2377CEABE80DFB6E3EC8885059BF7 (void);
// 0x000001EA System.Void Firebase.Firestore.Converters.ConverterCache/<>c::.cctor()
extern void U3CU3Ec__cctor_mCB46A4DF22D85F308327070A399FD7064040DEEF (void);
// 0x000001EB System.Void Firebase.Firestore.Converters.ConverterCache/<>c::.ctor()
extern void U3CU3Ec__ctor_mEF644C9FD3E7D28E8F691132D7173170CC503D2B (void);
// 0x000001EC System.Boolean Firebase.Firestore.Converters.ConverterCache/<>c::<TryGetStringDictionaryValueType>b__6_0(System.Type)
extern void U3CU3Ec_U3CTryGetStringDictionaryValueTypeU3Eb__6_0_m5A15F1C5B946E007B2B8C8A185608DCD25035A4F (void);
// 0x000001ED Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.Converters.CustomConverter::ForConverterType(System.Type,System.Type)
extern void CustomConverter_ForConverterType_m93BA6ABC59D4BC6B9B949EA442D6E9F28CC41664 (void);
// 0x000001EE Firebase.Firestore.Converters.IFirestoreInternalConverter Firebase.Firestore.Converters.CustomConverter::CreateInstance(Firebase.Firestore.FirestoreConverter`1<T>)
// 0x000001EF System.Void Firebase.Firestore.Converters.CustomConverter::.cctor()
extern void CustomConverter__cctor_m3E7E9A0B96B44DF49C85B44048CA600069E00F2A (void);
// 0x000001F0 System.Void Firebase.Firestore.Converters.CustomConverter`1::.ctor(Firebase.Firestore.FirestoreConverter`1<T>)
// 0x000001F1 System.Object Firebase.Firestore.Converters.CustomConverter`1::DeserializeValue(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
// 0x000001F2 System.Void Firebase.Firestore.Converters.DictionaryConverter`1::.ctor(System.Type)
// 0x000001F3 System.Object Firebase.Firestore.Converters.DictionaryConverter`1::DeserializeMap(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
// 0x000001F4 System.Void Firebase.Firestore.Converters.EnumConverter::.ctor(System.Type)
extern void EnumConverter__ctor_m48BD9A0C8F8B1F089A5CBC986C4292FDF19746F4 (void);
// 0x000001F5 System.Object Firebase.Firestore.Converters.EnumConverter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void EnumConverter_DeserializeInteger_mD03C70F39CD403E43283A455D29801AA2E165CCD (void);
// 0x000001F6 System.Object Firebase.Firestore.Converters.EnumConverter::Int64ToEnumBaseType(System.Int64)
extern void EnumConverter_Int64ToEnumBaseType_mFFF3896402AAB924DF0BAA7F5321BE5460F6AA71 (void);
// 0x000001F7 System.Void Firebase.Firestore.Converters.EnumerableConverter::.ctor(System.Type)
extern void EnumerableConverter__ctor_mD759176C08F3C8F9FA3D7BA76E33A13F7AF930A5 (void);
// 0x000001F8 System.Object Firebase.Firestore.Converters.EnumerableConverter::DeserializeArray(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
extern void EnumerableConverter_DeserializeArray_m3FE8044084F9036E98E38F94C8BE5770B9AB8EF9 (void);
// 0x000001F9 System.Void Firebase.Firestore.Converters.EnumerableConverter/<>c::.cctor()
extern void U3CU3Ec__cctor_m4C61A18D8E81C13A12AA96622CF5C6CF58A9EEC8 (void);
// 0x000001FA System.Void Firebase.Firestore.Converters.EnumerableConverter/<>c::.ctor()
extern void U3CU3Ec__ctor_m97087E5D1803B984D47CAAC621D01C8810A2531B (void);
// 0x000001FB System.Boolean Firebase.Firestore.Converters.EnumerableConverter/<>c::<.ctor>b__1_0(System.Type)
extern void U3CU3Ec_U3C_ctorU3Eb__1_0_mC65EA465ADE3F30333D7A4AD80443952B0BAAF40 (void);
// 0x000001FC System.Void Firebase.Firestore.Converters.EnumerableConverterBase::.ctor(System.Type)
extern void EnumerableConverterBase__ctor_mED2B3485AD8ABC73CB9892B5BFA2A8526266783D (void);
// 0x000001FD System.Object Firebase.Firestore.Converters.IFirestoreInternalConverter::DeserializeValue(Firebase.Firestore.DeserializationContext,Firebase.Firestore.FieldValueProxy)
// 0x000001FE System.Void Firebase.Firestore.Converters.MapConverterBase::.ctor(System.Type)
extern void MapConverterBase__ctor_m9E9E142347C2CED0E133610BD909AABDF1EF1B00 (void);
// 0x000001FF System.Void Firebase.Firestore.Converters.StringConverter::.ctor()
extern void StringConverter__ctor_mB4E28DBBCB593C8B0B104E17EF3F53F8F6E2186D (void);
// 0x00000200 System.Object Firebase.Firestore.Converters.StringConverter::DeserializeString(Firebase.Firestore.DeserializationContext,System.String)
extern void StringConverter_DeserializeString_m5DBF8001A288190585CB573C94EFCA0EAAB79A37 (void);
// 0x00000201 System.Void Firebase.Firestore.Converters.IntegerConverterBase::.ctor(System.Type)
extern void IntegerConverterBase__ctor_mA9476A3DF7B140631E1386724E61EA81FFE0893F (void);
// 0x00000202 System.Object Firebase.Firestore.Converters.IntegerConverterBase::DeserializeDouble(Firebase.Firestore.DeserializationContext,System.Double)
extern void IntegerConverterBase_DeserializeDouble_mCD20BDEFC96DA44E122A69BF03A993BCA048B752 (void);
// 0x00000203 System.Void Firebase.Firestore.Converters.ByteConverter::.ctor()
extern void ByteConverter__ctor_mFC325B5FD5453EFB5D7F3A362AAD6AFB135D7BAD (void);
// 0x00000204 System.Object Firebase.Firestore.Converters.ByteConverter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void ByteConverter_DeserializeInteger_m75366C8C52D7A8E3C45B7AD38DBAE758E8C0BA9A (void);
// 0x00000205 System.Void Firebase.Firestore.Converters.SByteConverter::.ctor()
extern void SByteConverter__ctor_m8DF21A6C65A36F46C63DBBA7288B6438CB586765 (void);
// 0x00000206 System.Object Firebase.Firestore.Converters.SByteConverter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void SByteConverter_DeserializeInteger_m756B896D761C10275AE95BB4446D5872C5FD1AD7 (void);
// 0x00000207 System.Void Firebase.Firestore.Converters.Int16Converter::.ctor()
extern void Int16Converter__ctor_m5B896AE510E0FE57A2DA3F09F90706D53E01F118 (void);
// 0x00000208 System.Object Firebase.Firestore.Converters.Int16Converter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void Int16Converter_DeserializeInteger_mB51802CC353F9A8092891D6FC0FE1DE6BAD31A84 (void);
// 0x00000209 System.Void Firebase.Firestore.Converters.UInt16Converter::.ctor()
extern void UInt16Converter__ctor_m5613B6AC8405974D2174FF9D7B48397752CA1F33 (void);
// 0x0000020A System.Object Firebase.Firestore.Converters.UInt16Converter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void UInt16Converter_DeserializeInteger_mBB92F046B498A47235E6D5C61E1D4DF5D139D71A (void);
// 0x0000020B System.Void Firebase.Firestore.Converters.Int32Converter::.ctor()
extern void Int32Converter__ctor_m694D9FF17B5E75F13744C0685A0056EEAB449C79 (void);
// 0x0000020C System.Object Firebase.Firestore.Converters.Int32Converter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void Int32Converter_DeserializeInteger_m0CBD454EA743313EF45C767D24B33D90CA1837D9 (void);
// 0x0000020D System.Void Firebase.Firestore.Converters.UInt32Converter::.ctor()
extern void UInt32Converter__ctor_m3F47AFC41ABC2B161B86393D06ECD6566D6614B8 (void);
// 0x0000020E System.Object Firebase.Firestore.Converters.UInt32Converter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void UInt32Converter_DeserializeInteger_m26885822BC645607B2FB01500B8173B823A3441A (void);
// 0x0000020F System.Void Firebase.Firestore.Converters.Int64Converter::.ctor()
extern void Int64Converter__ctor_m24A6E97DFF8EC0E1647DA0CFFD9580C9FC5060CF (void);
// 0x00000210 System.Object Firebase.Firestore.Converters.Int64Converter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void Int64Converter_DeserializeInteger_m7CB1F3B783A49669BDB04A2F227633189808AFFA (void);
// 0x00000211 System.Void Firebase.Firestore.Converters.UInt64Converter::.ctor()
extern void UInt64Converter__ctor_m5B5B8D8F20ED9558D4E25A55719AE79AAA8A2C70 (void);
// 0x00000212 System.Object Firebase.Firestore.Converters.UInt64Converter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void UInt64Converter_DeserializeInteger_mC2073BA9F288A43CB50EDC0AF8D596DFD933F893 (void);
// 0x00000213 System.Void Firebase.Firestore.Converters.SingleConverter::.ctor()
extern void SingleConverter__ctor_m9086CBD151FBB44D1B11545223F5D8829E843BBC (void);
// 0x00000214 System.Object Firebase.Firestore.Converters.SingleConverter::DeserializeDouble(Firebase.Firestore.DeserializationContext,System.Double)
extern void SingleConverter_DeserializeDouble_mB961241C8778BB908BE597342F00B6D98C0C1AA1 (void);
// 0x00000215 System.Object Firebase.Firestore.Converters.SingleConverter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void SingleConverter_DeserializeInteger_m13F0D4799B0A00DC8ED8DC14B5FC787AC23A185C (void);
// 0x00000216 System.Void Firebase.Firestore.Converters.DoubleConverter::.ctor()
extern void DoubleConverter__ctor_mF5F5573F321FDA0AB37ECB051B22BE83F4CE5931 (void);
// 0x00000217 System.Object Firebase.Firestore.Converters.DoubleConverter::DeserializeDouble(Firebase.Firestore.DeserializationContext,System.Double)
extern void DoubleConverter_DeserializeDouble_mE7A5F318C5A25C9482DFF813FD9D2C43DE6D0932 (void);
// 0x00000218 System.Object Firebase.Firestore.Converters.DoubleConverter::DeserializeInteger(Firebase.Firestore.DeserializationContext,System.Int64)
extern void DoubleConverter_DeserializeInteger_mD127B2E56D574E946745ADB0F426590481C3FAE4 (void);
// 0x00000219 System.Void Firebase.Firestore.Converters.BooleanConverter::.ctor()
extern void BooleanConverter__ctor_mBDD68794FE108535DE5042C1A0CC225566321AE5 (void);
// 0x0000021A System.Object Firebase.Firestore.Converters.BooleanConverter::DeserializeBoolean(Firebase.Firestore.DeserializationContext,System.Boolean)
extern void BooleanConverter_DeserializeBoolean_mB6489961CAABF117124CFFF48E1EBF3202514795 (void);
// 0x0000021B System.Void Firebase.Firestore.Converters.TimestampConverter::.ctor()
extern void TimestampConverter__ctor_m26273B8B45C7E5B7CC011C58FE93131E2C53367B (void);
// 0x0000021C System.Object Firebase.Firestore.Converters.TimestampConverter::DeserializeTimestamp(Firebase.Firestore.DeserializationContext,Firebase.Firestore.Timestamp)
extern void TimestampConverter_DeserializeTimestamp_m70421B48E029A0B0DC1E16E22A1E79A2CFB8AB17 (void);
// 0x0000021D System.Void Firebase.Firestore.Converters.GeoPointConverter::.ctor()
extern void GeoPointConverter__ctor_mA78116EE37089A80A2CC01CFBE3E015EB11DED71 (void);
// 0x0000021E System.Object Firebase.Firestore.Converters.GeoPointConverter::DeserializeGeoPoint(Firebase.Firestore.DeserializationContext,Firebase.Firestore.GeoPoint)
extern void GeoPointConverter_DeserializeGeoPoint_m9EB968D578A7ED2DB9CCED057D36A7CED8F1D7B4 (void);
// 0x0000021F System.Void Firebase.Firestore.Converters.ByteArrayConverter::.ctor()
extern void ByteArrayConverter__ctor_mBD1F09EBAB7411960681E0A080DDC43CCB47EE5C (void);
// 0x00000220 System.Object Firebase.Firestore.Converters.ByteArrayConverter::DeserializeBytes(Firebase.Firestore.DeserializationContext,System.Byte[])
extern void ByteArrayConverter_DeserializeBytes_m8999DBC45439623A82D6BC8C3988A3389D929533 (void);
// 0x00000221 System.Void Firebase.Firestore.Converters.BlobConverter::.ctor()
extern void BlobConverter__ctor_m8142753A6C7B938E154FBFEFCC4438C53E41A0D0 (void);
// 0x00000222 System.Object Firebase.Firestore.Converters.BlobConverter::DeserializeBytes(Firebase.Firestore.DeserializationContext,System.Byte[])
extern void BlobConverter_DeserializeBytes_m9FC7288F6F81B7EBA73F3933273B09B2F56687B7 (void);
// 0x00000223 System.Void Firebase.Firestore.Converters.FieldValueProxyConverter::.ctor()
extern void FieldValueProxyConverter__ctor_m414C822AA810FFEFB76C3D47E0A7B07962D6B3F3 (void);
// 0x00000224 System.Void Firebase.Firestore.Converters.DateTimeConverter::.ctor()
extern void DateTimeConverter__ctor_m0FEEC838E0893B955D2ADA84D073F2E90D396294 (void);
// 0x00000225 System.Object Firebase.Firestore.Converters.DateTimeConverter::DeserializeTimestamp(Firebase.Firestore.DeserializationContext,Firebase.Firestore.Timestamp)
extern void DateTimeConverter_DeserializeTimestamp_m1818DB588574DAFC5EA11EEE574F2CEE4354C098 (void);
// 0x00000226 System.Void Firebase.Firestore.Converters.DateTimeOffsetConverter::.ctor()
extern void DateTimeOffsetConverter__ctor_m6692EC537B17F33E0C4C08260AD10F605CD152DB (void);
// 0x00000227 System.Object Firebase.Firestore.Converters.DateTimeOffsetConverter::DeserializeTimestamp(Firebase.Firestore.DeserializationContext,Firebase.Firestore.Timestamp)
extern void DateTimeOffsetConverter_DeserializeTimestamp_m0CAF92C2407C86FA8186C6C266762FE02EE436EB (void);
// 0x00000228 System.Void Firebase.Firestore.Converters.DocumentReferenceConverter::.ctor()
extern void DocumentReferenceConverter__ctor_m944C476BC390D26369F42B3ADC85A158A99F7059 (void);
// 0x00000229 System.Object Firebase.Firestore.Converters.DocumentReferenceConverter::DeserializeReference(Firebase.Firestore.DeserializationContext,Firebase.Firestore.DocumentReference)
extern void DocumentReferenceConverter_DeserializeReference_mEEB648CEA7BBAF559C1DA6237CEFD6517FEFBBE0 (void);
static Il2CppMethodPointer s_methodPointers[553] = 
{
	Blob_get_Length_m41B38E18238097414A3A20FD3DC2156970A31502,
	Blob_CopyFrom_mACD217581997CCAFFC1F81557B64D3541041BEFD,
	Blob_Equals_m4BAAC02C0A8F997F10D849409E37342FBCFD5A5C,
	Blob_GetHashCode_m228609F9AD65008385472F6A0B25061FDFD851E8,
	Blob_Equals_m56D099DEE926AAE8091860F91307FCD4FA48E50C,
	Blob_ToString_m3561726740B5378812B18356C8998B71F99A1298,
	Blob__ctor_m1F16308CEC0FA8E3447D18B2168D01921195A6AB,
	DocumentReference__ctor_m4AE8003DE3DCFA995D3AD9EC075F6F0E5E149F71,
	DocumentReference_ClearCallbacksForOwner_m58119A59C76C72B6AD9B42E0E4343B818D4DE11D,
	DocumentReference_get_Firestore_m02AAEC879C3F4FBECAEC1C51B3055A8F366E07C8,
	DocumentReference_get_Id_m4898D340E1899D3DB5BA535BF7E4DC91FD52D3A4,
	DocumentReference_get_Path_m30463C4BD4AC09361F24384728395D3866C38540,
	DocumentReference_GetHashCode_mFB550B31375A70A50DD734BC3A3C140098D49645,
	DocumentReference_Equals_m05422A2E8C6192D5D6EB56A76519E0C4DFB4B9D7,
	DocumentReference_Equals_m4EF71730722449EEF4EF38FAE6C708204351C517,
	DocumentReference_ToString_mE03C724E5B90C0CBB7C9A8FF6F033C59913F3F03,
	DocumentReference_DocumentSnapshotsHandler_m39CF98A69DDA6C278BA2586D1C383F42A3A62767,
	DocumentReference__cctor_mF84880296598D73EEC05AC8A1E0A7C67B31D809F,
	ListenerDelegate__ctor_mB86641D489EFB8C7171E204F923CF10A77F34E05,
	ListenerDelegate_Invoke_m95975769948175C8DD80808529D88C6EB0F9554B,
	DocumentSnapshot__ctor_m8825DD7CC25AF8D5285A3004BD3A7D08C5C251EF,
	DocumentSnapshot_get_Reference_m7E6A35C1DC9A04B68B6178BD1AC9B744BE39C09E,
	DocumentSnapshot_get_Exists_m133BC1857E6AC60E909CE432A31D5148395AE7CD,
	DocumentSnapshot_ToDictionary_m641E6A4F6AA49A9CC9F5A6676ECD0354D88FF63B,
	NULL,
	DocumentSnapshot_Equals_m8EDBB1AF9F354FC5228E3B1030AA368C2CA20B59,
	DocumentSnapshot_Equals_m50FBE8FB535767C1A1E071DCE5D6810D501CD7A8,
	DocumentSnapshot_GetHashCode_m886B204B52CEEEF8F81C77E60D470DF7F8875A50,
	FirebaseFirestore__ctor_mD0B5A58E35F5B6B07CA3F55BD1E3D27A31867DD6,
	FirebaseFirestore_Finalize_m85A0AC3229A58F747349F1DEF71FFBC1F7C86B7D,
	FirebaseFirestore_OnAppDisposed_mF07E6E9B440BE1CE3C72B44908DBDE1BEEC1284E,
	FirebaseFirestore_Dispose_m105A981CFC95EEAA1A4C9FF9DB24B6C873A1D5F1,
	FirebaseFirestore_get_App_mDDB46762F9ADDD9CB6EB445108454670850DAC89,
	FirebaseFirestore_set_App_mD51E0F0E17E24C561B1122BDB86E41B6ADB26198,
	FirebaseFirestore_get_DefaultInstance_m16EACB91EA3C01689A2F7707DA288177D21A67CE,
	FirebaseFirestore_GetInstance_mBB395B445D67BFAF5F5583DB41802F04095EAE5B,
	FirebaseFirestore_CollectionGroup_mE15FD3DE0F9B82C9E0ADC4517BD477E9089EE6EB,
	FirebaseFirestore_SnapshotsInSyncHandler_m63B46E06E07A5AC034332B505171FB2779C7FB36,
	FirebaseFirestore_LoadBundleTaskProgressHandler_mA5D2729E0A63DCF17A0443A2A46AA394EDBD4AC5,
	NULL,
	FirebaseFirestore_RemoveSelfFromInstanceCache_m1E495D5093B00A37344C776924E3438525FFD65E,
	FirebaseFirestore__cctor_m2BB5153BA1F3F69AFD4B989E28BF2016A9818410,
	SnapshotsInSyncDelegate__ctor_m9375B01153A11BD74F744DD15A886ED958DB772D,
	SnapshotsInSyncDelegate_Invoke_mEF1345EB72B35A0543188ABA2F7B73DE825BB649,
	LoadBundleTaskProgressDelegate__ctor_m423E84011E7F4325B1C33C35FE4048B0EF193E59,
	LoadBundleTaskProgressDelegate_Invoke_m4F282D6B38427407ACB3835C331140DBF948A57C,
	U3CU3Ec__DisplayClass22_0__ctor_m9E0287E48CB7E94E679E8A1C82E08264734C0F01,
	U3CU3Ec__DisplayClass22_0_U3CCollectionGroupU3Eb__0_m935BEA96D22612A95485B33D464EB7D218DAD1D9,
	U3CU3Ec__DisplayClass31_0__ctor_mC17E55D69ECC0F02E79007EAEB8B5D0571408386,
	U3CU3Ec__DisplayClass31_0_U3CSnapshotsInSyncHandlerU3Eb__0_m38DF76A8BE4A295AE459B24064E05E6D2704BB1A,
	FirebaseFirestoreSettings__ctor_m5C1599D5F3956CCDFF1D7DB8DE27D84528A2137F,
	FirebaseFirestoreSettings_Dispose_m226B27465D8677858FA859856A77A9702B6475FB,
	FirebaseFirestoreSettings_get_Host_mCC2B684D5C96EA637B6D4512C06AAD52E647B27D,
	FirebaseFirestoreSettings_get_SslEnabled_m696B21567BB15D7D034E6BBE616A5D21A4D7A20A,
	FirebaseFirestoreSettings_get_PersistenceEnabled_m61900571449535D5C048F9CF9776867281BC3A82,
	FirebaseFirestoreSettings_get_CacheSizeBytes_m8B5E76CA969790711A52AEDD28DA3925B7647040,
	NULL,
	FirebaseFirestoreSettings_EnsureAppliedToFirestoreProxy_m847B8D9ECFF48F4C9D1CB42F95AA31DD535E081E,
	FirebaseFirestoreSettings_ToString_mD267EF3B7CC23C2DD75715745B6FCFA6FD51DF55,
	FirebaseFirestoreSettings__cctor_mBEB58E310600334E4D80F8FA94BFC65430C80C73,
	FirebaseFirestoreSettings_U3Cget_HostU3Eb__10_0_mF190B2CA77404F0848138EB7D8C99A3C2CE1CE4B,
	FirebaseFirestoreSettings_U3Cget_SslEnabledU3Eb__13_0_m7810B99133CDAB19881ED48A45F8213A0DD90B93,
	FirebaseFirestoreSettings_U3Cget_PersistenceEnabledU3Eb__16_0_mFE9EE86456A51039E9B3D34E9E349E3354834B86,
	FirebaseFirestoreSettings_U3Cget_CacheSizeBytesU3Eb__19_0_m193FD30CDA85A8BB042578976606649729268F16,
	FirestoreDataAttribute_get_UnknownPropertyHandling_m938FF3674501A857280E218CFF62C1FEA25D88DA,
	FirestoreDataAttribute_get_ConverterType_m0E37B0C2D7BCD6EE6729784DDAEEE23F6364EFC2,
	FirestoreException__ctor_m4CC8461F5C4686D692C1E40E683FF23B7950424D,
	FirestoreException__ctor_m6BF2A02D4996876CF98E49AB4ECE0F2A2A455856,
	FirestoreException_set_ErrorCode_m211D8C17633ACCF730402958534100973529B512,
	FirestorePropertyAttribute_get_Name_mFC7ECC680AF9AED8DBBAD7A388561ADD39A19E20,
	FirestorePropertyAttribute_get_ConverterType_m4D490204075667082A0652C96B069AD578FF81E5,
	GeoPoint_get_Latitude_mE91F389BEBC7A0B0D91DE25D71C232937E6F3014,
	GeoPoint_get_Longitude_m133E6D1B3E1991A9DE1459F186D0A9EDEA05E582,
	GeoPoint__ctor_mFD3EA7D3029F84DCEFF17D456E7D40D7DA648A34,
	GeoPoint_Equals_m0414DB7E087F0DD44CCE0742C1ED9B6D42744902,
	GeoPoint_GetHashCode_m120579279AD807DE874974BD3E15392B8BD01B51,
	GeoPoint_Equals_m3B2A6102D80DB864F3CE3290B0B347A879974646,
	GeoPoint_ToString_mE7103148304B568F0B0CFDF26C1BA558A0508248,
	GeoPoint_ConvertFromProxy_mFB2640BF73C95DEAD11FDDCDAD9F26AC10C52DA9,
	LoadBundleTaskProgress__ctor_m92EF37D44CB79ADCFE0F5EAACB9F006A7E92D976,
	LoadBundleTaskProgress_get_DocumentsLoaded_mB16A2920280DA8009269A8A5B1BAC24BBC58EAB8,
	LoadBundleTaskProgress_set_DocumentsLoaded_m5F3C214AE872DBD6186AE2FEE039AFEE38A99CF9,
	LoadBundleTaskProgress_get_TotalDocuments_m879EA2D2D7843E26078931AF4675F4F3E1706A9C,
	LoadBundleTaskProgress_set_TotalDocuments_mD6C9C3C2D21233EBCF6ECFB3D4BAB95EA6F46E03,
	LoadBundleTaskProgress_get_BytesLoaded_m8A603FB503A7E194C646B30E134A30B8F801D125,
	LoadBundleTaskProgress_set_BytesLoaded_m12FCD36744E9B72993B9967029C7F5E4A2A12B93,
	LoadBundleTaskProgress_get_TotalBytes_m5DAD938FDF11CFAF5ED26BFC44DE60367539E843,
	LoadBundleTaskProgress_set_TotalBytes_mE69ADBE5CE262AB12939BF4D34B633444BCB8A99,
	LoadBundleTaskProgress_get_State_mEE05ED7DBF57672624A8B71B28EB795F2A8373C8,
	LoadBundleTaskProgress_set_State_m01823E221208AF31FE733D8CDAA377C1A3605401,
	LoadBundleTaskProgress_Equals_m6CA801AE22E5D2434ED0027F2D96E9BFF09FF148,
	LoadBundleTaskProgress_GetHashCode_mC69576379BC4D8310EA6E605609B2016E5E77D3E,
	Query__ctor_m2CE28A7C81302467FFCB34DA3838FC5E68673073,
	Query_ClearCallbacksForOwner_m5BF92E2B453A1451E8D9614D188D8A010F554CF2,
	Query_get_Firestore_mB059769B7B9B2B59A91CB866F26E78E3858F90CF,
	Query_GetSnapshotAsync_mEA039F6C70332FF8DC298DC4F8311DD281DBD22E,
	Query_Equals_m9BC8D4041680A526881A7CAA0EA2614BCFC4F9C0,
	Query_Equals_m4BF263FC1B52FD81E2B179BDBFA581773782397F,
	Query_GetHashCode_m4E8AAE6824FEA1B281C5635CCCA6FC122E5918BD,
	Query_QuerySnapshotsHandler_mACCC5A5D3D2ECD5DCE9DEA25B771CB448DC68715,
	Query__cctor_mDD820AFB130311FA76381AA271CA863441BEBEB5,
	Query_U3CGetSnapshotAsyncU3Eb__42_0_m12810E2F3E433C7FD81E083493234AD493A26D23,
	ListenerDelegate__ctor_mFF7C9FAC527620912E24053626C1C60B3DA5B7A1,
	ListenerDelegate_Invoke_mB139B3530439A4BB6F54FCB1ACBEF763227AAC35,
	QuerySnapshot__ctor_m51DFA7C79E68E0D4E8F9E920A557DA951DCC03DA,
	QuerySnapshot_get_Documents_mE070C83177195302B9D6B573553BB96EF3674BBC,
	QuerySnapshot_get_Count_mA0A7BDED4057659FE283566C2B750ECD003E4B02,
	QuerySnapshot_Equals_mF61C63B4716C9BCBACC1E37BD50ACA1846A20B7A,
	QuerySnapshot_Equals_m7D2A703274A18A6091F0B9FB4101FF752AD8294F,
	QuerySnapshot_GetHashCode_mA6AE0D5CDEDC6B1B74039721C058EA57956B6F96,
	QuerySnapshot_GetEnumerator_m6E2B0B6A84E0DF72E23650532B483A9FD8C0BF60,
	QuerySnapshot_System_Collections_IEnumerable_GetEnumerator_m2DF41DC974D62F53964BDACDBE90FB17B9BA696B,
	QuerySnapshot_LoadDocumentsCached_m107AD85853340D645A554665F4FAE43CA7A9F089,
	ServerTimestampBehaviorConverter_ConvertToProxy_m92EC98E5DB85DFF5553D9A5749E86E2436502103,
	Timestamp_ToDateTime_mDB1ACFC2FB995894CCED239A5E557CDA12F066FE,
	Timestamp_ToDateTimeOffset_mC1AB3B395A37909C8A0E01DB07C55C0E6AD3ED46,
	Timestamp_Equals_m4662FEFEF81799C37FB99795E492F562E55CA253,
	Timestamp_GetHashCode_mFF00599A75371E96B5E55636FF40902F31BCFCEC,
	Timestamp_Equals_mAD77D232E60E042104147E04E8D3331CBB35D8BE,
	Timestamp_CompareTo_mE49BD77BFD6AAD4ACEF74F1578B2ACA8929EC6A1,
	Timestamp_CompareTo_m3E776402C9986CE96A20DF4812C32A70B164780E,
	Timestamp_ToString_mD0ECC96BAFA1DBE375F79E550C63D8897CD77AD8,
	Timestamp__ctor_m4C5388F8B2158FA8F3A104CFE55ACD0024554D53,
	Timestamp_ConvertFromProxy_m47D961472FC16995F06A638FF5DBB18B890641F4,
	Timestamp__cctor_m06A8B890DB64CDC36D8DD69D898D88E8E4771F96,
	ConverterRegistry_ToConverterDictionary_m490AB75121F03CD402E7D17CCD3BB2FC2218FEC6,
	DeserializationContext_get_Firestore_m16D946E39CF356655BAE738D16D837D69A6E8F7E,
	DeserializationContext_get_DocumentReference_mD62CCE2F430E6E159E7C53F09C3441A33CBCC297,
	DeserializationContext__ctor_mECF86F1A451C686E47918DA12E63C27C460D95E6,
	DeserializationContext_GetConverter_m360A3FBBB3962C13463FB10B43D4B71C3A961120,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MonoPInvokeCallbackAttribute__ctor_mA91A7DD150D6C507BCFCA0D5B108E72B7E4D5804,
	SerializationContext_get_Default_m4D1442067DD14EC8387D05E602E8A50EA2F259DF,
	SerializationContext__ctor_m48DADC02D832C8298D2BB521EE091C81DA87D85D,
	SerializationContext_GetConverter_mFB2CEED05FDA601C9C3405C70C8E968AA7B4BE9A,
	SerializationContext__cctor_m3E79E149FCBC4EAD1AFB4B8A65631D9E09432F33,
	TransactionManager__ctor_m463D159697F8F9CF65D8B045F374770BC3DFC89F,
	TransactionManager_Finalize_m0628FE79549C0DF4FBE2800AF8406A7485243C7E,
	TransactionManager_Dispose_m9BE81C60B6A085D086D11DADF9DA64886050CFE2,
	TransactionManager__cctor_m9A843BC514CB084A996821BA57486B7EA00B8775,
	ValueDeserializer_Deserialize_mF55F3A85F009C7CD0F055BC2A91591865955A094,
	ValueDeserializer_GetTargetType_mE062B00693454451CC4617CD471C19AC10793C4F,
	GeoPointProxy__ctor_mB2CFD6F2A717A0C3AE22BBD4F30136799C50F391,
	GeoPointProxy_Finalize_m8677EF4A0CFC39D494DB49DF8F37BC027AF5F60A,
	GeoPointProxy_Dispose_m937789FC37643980D0871200ED89367CE8FB6E6D,
	GeoPointProxy_Dispose_m92C493537FE315F4B95D545D94326301A01A5D02,
	GeoPointProxy_latitude_mA8BBE294FE9CAB3D26516FE20DAFACEB14F90DC1,
	GeoPointProxy_longitude_m1B4199AA7E07F3753605A3576FB273D7B547A1FF,
	TimestampProxy__ctor_mEF0E31229E48079A737F1B218C19E6B77C0E2066,
	TimestampProxy_Finalize_mB50B5DF17EDEB7D63CF0242CAC54604E8FA4E653,
	TimestampProxy_Dispose_mDFD057DF2502704706D7DB2BC15495ABF3F61FA6,
	TimestampProxy_Dispose_m80731F1E57850DDE39A14B033B499775AB7A71B2,
	TimestampProxy_seconds_m8B893A897B7984C99D67307454E74CAAB56F9264,
	TimestampProxy_nanoseconds_m3CBF873AD250722E000F1AD8B8E862F9030235CD,
	TimestampProxy_ToString_m8DA845A04113BC279FBFB220898FAC774D994284,
	Future_QuerySnapshot__ctor_m564ED70AF9BFEC3F375C3CC5D2C09103F33E2B76,
	Future_QuerySnapshot_Dispose_mEC5BBCE648D469D2A90A9A85EB535007396C362E,
	Future_QuerySnapshot_GetTask_m83FCC445EC41489278769E6CEEC6E71D9F372C2C,
	Future_QuerySnapshot_ThrowIfDisposed_m946D943B65A2A94AA1E6C3C8D50CA07409CFAAC6,
	Future_QuerySnapshot_SetOnCompletionCallback_mD068327FDF8CFA754F98A7BF974BA6FB09AE8866,
	Future_QuerySnapshot_SetCompletionData_m72EF16F4ED04F08C350CD20CEE2EAA945458C772,
	Future_QuerySnapshot_SWIG_CompletionDispatcher_mDE52F3C13385E02F9E4DDDBAE074D02C1B90EC39,
	Future_QuerySnapshot_SWIG_OnCompletion_m4DB13F0FA13C028FD44D5BFE0F5ABB50F1F53A41,
	Future_QuerySnapshot_SWIG_FreeCompletionData_mB307080187B2F875D101A8734DD439654B3B90E3,
	Future_QuerySnapshot_GetResult_mAD1F3E361970A51E5C37D891C2A173426E49F9D2,
	Future_QuerySnapshot__cctor_m63ECFE9F472DEFA4DDD39CBDB1281DC474989BD7,
	Action__ctor_m1D782F48F4DA20E6DC65EBF500AABF4BE9E1493A,
	Action_Invoke_m8E58A50FAF94C5B7B9F9F8DBC04C64CB6A20DDFF,
	SWIG_CompletionDelegate__ctor_mE93A78FEE09432728AACFB3A4A706E0AAEBE7F0C,
	SWIG_CompletionDelegate_Invoke_m42937927633E0A2CE5032FC8F5C4E9F2B04CBA82,
	U3CU3Ec__DisplayClass5_0__ctor_mC1A4BD21E45EB0EDF7101788C3A142A1F24C1F5B,
	U3CU3Ec__DisplayClass5_0_U3CGetTaskU3Eb__0_mAE4853B7006F26AEA789954F76B08D4B38C8AADE,
	DocumentReferenceProxy__ctor_m9A73CC86CDB33800541C380C7842F4E8070D4A99,
	DocumentReferenceProxy_Finalize_mC2875BF0BCED1C3E67EBEC14E4F00473CC0F6770,
	DocumentReferenceProxy_Dispose_mED8393B9D472C16D3D9CF1C0BE22140AA57E9E7D,
	DocumentReferenceProxy_Dispose_m886E59219FE018A85B4278C1E8AEBBA51459F081,
	DocumentReferenceProxy_id_m8CDA448FDA43CB9B3C259D0B26D10ACF645862A1,
	DocumentReferenceProxy_path_mE140A5A650A9204B6018A7F427CDD25773FF74B0,
	DocumentReferenceProxy_is_valid_mE85E9F3E580E73407E20AF23CC853DE6FF89886C,
	DocumentSnapshotProxy__ctor_mE5B8BE19C0F0E7DF3C550585F8AED62FDD80C9A1,
	DocumentSnapshotProxy_getCPtr_mF9F6A5AD72B52268009A8FC6C14E434D4857C86B,
	DocumentSnapshotProxy_Finalize_mD86BC6A9386B22993B480CFFF24388E688ECFA5E,
	DocumentSnapshotProxy_Dispose_mBAF78DEBAC840F65FB7AE9DD588E6A1FF74F2FEF,
	DocumentSnapshotProxy_Dispose_m9E318188984DEC90AE6AEC2604A4EFC7229CD32F,
	DocumentSnapshotProxy_reference_m0D32F20A6288EBC0D2A74D77AF177FCEBC8CDA4A,
	DocumentSnapshotProxy_exists_m7CC1ACBA13375AEE10F93189D851A41276C56185,
	FieldValueProxy__ctor_m057A425535C2E4512342AE1C24122D2333352F8D,
	FieldValueProxy_getCPtr_mD3D0355179AEE52CCC6215A2D5E7CA94EBA02B25,
	FieldValueProxy_Finalize_mD7E4217C5EE91E6796D79D72E4E068E68D9EF01C,
	FieldValueProxy_Dispose_m9AD56F3B3603A31A5FCE2591D954CFD53A3B4E12,
	FieldValueProxy_Dispose_m9C85A44E07E511F3A24594B6E910C6DD928A71CD,
	FieldValueProxy_type_m0901CADDA170E659B1C761C5E260E52916B7DBCC,
	FieldValueProxy_is_null_m0A09FD8355CDBA3BA3A82A87011F220BC4D6D3FE,
	FieldValueProxy_boolean_value_m264AF274971D95251C903E1BFE26FDEF685CE936,
	FieldValueProxy_integer_value_m35B9C4A72E076C276D193FB54C19301B93F619CF,
	FieldValueProxy_double_value_mC6999E0AF3DB2B290DD6876691367FCBD91E7355,
	FieldValueProxy_timestamp_value_mD1CDBAA188F3E4DE283EFF937EA137690C60B944,
	FieldValueProxy_string_value_m02864893AA2D618F225A72078B2274EDE8C14377,
	FieldValueProxy_blob_value_m2E9BCB9306FBDF03DDFACB654E34A4A79E3616BF,
	FieldValueProxy_blob_size_m8B795B4E09CF06ED8FB74EB921E5E123039AE729,
	FieldValueProxy_reference_value_m309FF53825A808F723A7961A304A043D076E1E42,
	FieldValueProxy_geo_point_value_m70E9A89FDF8640B88A28240891E5CABD1BBA71AA,
	FieldValueProxy_ServerTimestamp_mD314BB0BEDECBB7AB284ED28E4C394B1EB521A67,
	QueryProxy__ctor_m54BE6259B741A4DCD15DFC8C96261065F74E7953,
	QueryProxy_getCPtr_mA9CA81F6D1123892E1ACA6BA46D1A6F150D963F6,
	QueryProxy_Finalize_m8B980D089AB7B4CD1517E832E55753C2173E82FF,
	QueryProxy_Dispose_mBEE2C95195CC54C0616F9AA858FA188C783B05BA,
	QueryProxy_Dispose_m214839BD42DE29060A0316287562EFD81811CC0F,
	QueryProxy_GetAsync_m7880B56C9C11927426B2B4C86DF28E7BFDE588E8,
	QuerySnapshotProxy__ctor_mC833FF3EFA111C2A65294D8792262DF48F4DCB7B,
	QuerySnapshotProxy_getCPtr_mD73C14C3A50E046B30F309615B4056857DA6524A,
	QuerySnapshotProxy_Finalize_m626A9DA4CAB9FCBE96816B12C6660B309A345AAA,
	QuerySnapshotProxy_Dispose_m813E123BDC8BBD75FE412974B272A7413599D0AC,
	QuerySnapshotProxy_Dispose_mFD4255001199C4C9C19550FB0CB9108FA7A32B3C,
	QuerySnapshotProxy_size_mD7049D777AF47CDD2B692FF277EFBCE825E61335,
	SettingsProxy__ctor_m7BE0A49E746604DAA6D4F0F4C1F11EA42149F34E,
	SettingsProxy_getCPtr_mF852AF8700D95B0F36550C21FDA57B0B44BD9187,
	SettingsProxy_Finalize_mC46D66BFE1A50038FCA4FD54E4D723258341275D,
	SettingsProxy_Dispose_m442C175F98EFCE27F7A3700EEFC5B5AE36B10D11,
	SettingsProxy_Dispose_mE119A9B43AFFD05DFC31B7D445B398D9F3E89DC5,
	SettingsProxy__ctor_mFF91E7269B1D8AB229B753F49B522ABDA9FAE1D2,
	SettingsProxy_host_m2CCB8A5A10853AF344C4D7E4017862E8D9EEDE4A,
	SettingsProxy_is_ssl_enabled_m82E8567F07C8B8040E0B3F02D0AA7F00656EB822,
	SettingsProxy_is_persistence_enabled_mBED82AE76D1C4517662A06F8AE4F9E96F1DDB364,
	SettingsProxy_cache_size_bytes_mC70FF08213FC8A995429A00351986FD96ED338FE,
	SettingsProxy_set_host_mD0E554E2A0CBCAA1D90212E7899327F7F648BA86,
	SettingsProxy_set_ssl_enabled_m3BBDC250075222AFC9566F71EAD2372C8A474CAC,
	SettingsProxy_set_persistence_enabled_m4369035C1C25C4F4D2284769D4B29815EE8BC9B3,
	SettingsProxy_set_cache_size_bytes_m6D22F07BA542A1FC505275CFC21BBFB6637CBD2A,
	SettingsProxy__cctor_m74626ADDA32BA3FC993F46260A74675E5B0B8F2E,
	LoadBundleTaskProgressProxy__ctor_m589DAC2316E4D9157F4091B1947130DDEE6DEE0B,
	LoadBundleTaskProgressProxy_Finalize_m870F582FA08EB2569BD91580A190A4FB329053B8,
	LoadBundleTaskProgressProxy_Dispose_m7AB4AABD17FBC3BD299EBCE6758CC90FA0817028,
	LoadBundleTaskProgressProxy_Dispose_mF8BE12610E8DB30EA2283F27D0B165B47AC4A73F,
	LoadBundleTaskProgressProxy_documents_loaded_m88A608E7A6791DD89BB77068F783296A4A7CA977,
	LoadBundleTaskProgressProxy_total_documents_m9789681C10719617FB16270DDA713DD531F13233,
	LoadBundleTaskProgressProxy_bytes_loaded_m4F15F71F3EE7F7860DA16F29A3BC265D1B8796E1,
	LoadBundleTaskProgressProxy_total_bytes_m7B0175EBAB06F5E5FB0BE40669876D62FF6A7C1C,
	LoadBundleTaskProgressProxy_state_m57FD1F9D30D4B57833EFA33DE4B3ABE24BF142CE,
	FirestoreProxy__ctor_m33D85CBD3680DC453D2C897DA82CB192FB65FF15,
	FirestoreProxy_getCPtr_mE29AF87AA06BD73AEB17FABDDE15D9F03F31096B,
	FirestoreProxy_Finalize_m12D7E48C5AC644E05DFD54C1FAD023E1E0A832F6,
	FirestoreProxy_Dispose_mDBB836BE8D77031C98289CF1272829448413FEBA,
	FirestoreProxy_Dispose_mE1365B94489D68EC2253A5C65DDA09C32E51F5A9,
	FirestoreProxy_GetInstance_mA430C24698D14D60B2486E0B657907E6D95A2DC6,
	FirestoreProxy_CollectionGroup_m6734F28BCA0A5C2DB7051F6ACD4A9CD1085E31B5,
	FirestoreProxy_settings_mAE6DD88E6474AA4F2FAEFC42ABFD3095D8AE71C4,
	FirestoreProxy_set_settings_m55F667182BE08269B255A8BC5BFE2077D1256CBD,
	ApiHeaders_SetClientLanguage_mCEB9F0276D8609BC491E7F34D4B435987A1EF3CF,
	TransactionCallbackProxy_Dispose_mBB49019291FA4513D573EA6FC81F8CA5ED137517,
	TransactionCallbackProxy_Dispose_m0021849765F39866894838C2BF78683502E34DCC,
	TransactionManagerProxy__ctor_m666391B0E579026E20DAF2E9CAADEE01C9C7A7AF,
	TransactionManagerProxy_Finalize_m8AEF3C30B371811F7CFE64F8BD3715ABAB8E7BCD,
	TransactionManagerProxy_Dispose_m103B12BB6B4C692A90966092FFD8D331A8043A7C,
	TransactionManagerProxy_Dispose_mA6A6E8073B61DE5E2DD148B2C26D871698F3B2A5,
	TransactionManagerProxy__ctor_m7E365B74A286FE0BA6C311750CDE2E46CA5056D5,
	TransactionManagerProxy_CppDispose_m50AD052C5F5B93F922F93B4227C34B1A1E7A8069,
	FieldToValueMap__ctor_mC1EF56A9227A7CDCDAFE0B998838DA471EA44419,
	FieldToValueMap_Finalize_m581620585CA304757491CA71FC490818D2F47B43,
	FieldToValueMap_Dispose_mD04D96A9A327A9C0F4175FFCD2D21FAD6F2FF5D4,
	FieldToValueMap_Dispose_m16D179AABA2F3D3757D4A68D33B83D59110312B7,
	FieldToValueMap_Iterator_mD5B928AC07D53D9FDEFE92CFE5715DC5191CCD3B,
	FieldToValueMapIterator__ctor_mD6C16102FB2665B2950254CE1D11F74641BC36C3,
	FieldToValueMapIterator_Finalize_m1C3DD3BE660FA9489D96BAFE5A0D9CEE8D9ECD1C,
	FieldToValueMapIterator_Dispose_mB8410CBEC466FF8641B455F5FDA9626D9DD7D150,
	FieldToValueMapIterator_Dispose_mBBCC0977F459B57B10FEE8BDAEDBBA5C46B0D9F0,
	FieldToValueMapIterator_HasMore_m1BD9816B14E6E04626494A206038434CF531E8F5,
	FieldToValueMapIterator_Advance_m52F40CF691D69876BA083A86569DD351733ABDB8,
	FieldToValueMapIterator_UnsafeKeyView_m21B2034F632AD3E2F7F0FFEB988553BD074D7668,
	FieldToValueMapIterator_UnsafeValueView_mA83B9FE10713663B767684807396BF9B5A75370B,
	DocumentSnapshotVector__ctor_mE70EB9BC37B7FECC125A7A2B6176E24D225E0E33,
	DocumentSnapshotVector_Finalize_m10D40AFFAFA8D1BA0EC00F0EF93FD75FB0A23B9B,
	DocumentSnapshotVector_Dispose_mABA2DA4626F8EF532FBD6CC1AE05FAF52D11F9F2,
	DocumentSnapshotVector_Dispose_m84E231EDD58AAB611B30178312A49831A49249B2,
	DocumentSnapshotVector_GetCopy_mEA2C2A2559038E258C51EECA4048D04079871F86,
	FieldValueVector__ctor_m470A3350660A2489949E6BDA6345AD5D389AC416,
	FieldValueVector_Finalize_m59776D118AA87494D18AEC94F2DB8C5D61E9F944,
	FieldValueVector_Dispose_m32C9D7BB9F8F8399AC4E289A2CCEBCB2F0DC67F8,
	FieldValueVector_Dispose_mECF6193F9BF166DEB78C934D1A5DE629CEBF9A69,
	FieldValueVector_Size_m6025FB407D221BE61E57DB3A5AA2FE5C3CEB1E07,
	FieldValueVector_GetUnsafeView_m9967EF1F902FAA891F3E31F6D4643C343C279EDD,
	FirestoreCppPINVOKE__cctor_m577C00A7CD5549FAA749355E70F5228065B732CF,
	FirestoreCppPINVOKE_GeoPointProxy_latitude_mB6CCE638AE64D742E8C9AEAC1E28A34CDEF9181D,
	FirestoreCppPINVOKE_GeoPointProxy_longitude_m2A842706520C78F84076A1A03B40EA083D0B6F45,
	FirestoreCppPINVOKE_delete_GeoPointProxy_mD5579740F533F39F5FA4EB99E9244ECD4D7964D0,
	FirestoreCppPINVOKE_TimestampProxy_seconds_m732369B70B75B220951603FF700452DEF849C632,
	FirestoreCppPINVOKE_TimestampProxy_nanoseconds_m70B2600575E9C7ED45270ADBBF5E6A7FF3533815,
	FirestoreCppPINVOKE_TimestampProxy_ToString_m16FDEBB1312BB0CFA83247D0A4DC7C2299E95275,
	FirestoreCppPINVOKE_delete_TimestampProxy_m1E5AE67122473E0908E65FFCF159C136A07027BB,
	FirestoreCppPINVOKE_Future_QuerySnapshot_SWIG_OnCompletion_m94804E41D79FF9CE795F630DB81DC0D875315133,
	FirestoreCppPINVOKE_Future_QuerySnapshot_SWIG_FreeCompletionData_m9559C443EA02AE48F891D9422F2342BC6DE090DB,
	FirestoreCppPINVOKE_Future_QuerySnapshot_GetResult_m52F9149B799C8ACDAABCBAC160DEE2FCD5E4A5A0,
	FirestoreCppPINVOKE_delete_Future_QuerySnapshot_m682269BAB71438AA9C39FF4FD1044AEF0B48AC8D,
	FirestoreCppPINVOKE_delete_DocumentReferenceProxy_m8A69E5674E330B61FC81A229AAAFA1D15FDB4F70,
	FirestoreCppPINVOKE_DocumentReferenceProxy_id_mFAC24F2623C8E8DFF0B69840C454CC1B42053277,
	FirestoreCppPINVOKE_DocumentReferenceProxy_path_m47A1E896F913044C84A569BA6008E89394029140,
	FirestoreCppPINVOKE_DocumentReferenceProxy_is_valid_m590FFE87FFA17163E305DE5BE6496F3060036E7E,
	FirestoreCppPINVOKE_delete_DocumentSnapshotProxy_mC35E663713F62EE2DED860E6EC10A41F54418A45,
	FirestoreCppPINVOKE_DocumentSnapshotProxy_reference_mF464446F6E500A769F4A5286539E78FA13FDA2AA,
	FirestoreCppPINVOKE_DocumentSnapshotProxy_exists_mBD7DA1B66D97FFB204907CD0EA16303F54097991,
	FirestoreCppPINVOKE_delete_FieldValueProxy_mBA146A9FDB2756C07BC0848A04C6E4363C84ADC8,
	FirestoreCppPINVOKE_FieldValueProxy_type_m1D627C81F5EE1D186810E7BEB0C0502A2D1871F7,
	FirestoreCppPINVOKE_FieldValueProxy_is_null_mB2A24582DD819F71F0AE35DFDD63E71490DFE9CC,
	FirestoreCppPINVOKE_FieldValueProxy_boolean_value_m59AF5FF06CDCCB0CD7A09F23B903EEEF84A25EA6,
	FirestoreCppPINVOKE_FieldValueProxy_integer_value_m84495CD178F910877EA8BAF77A0684B6D1058198,
	FirestoreCppPINVOKE_FieldValueProxy_double_value_mE24F62B8A0A917C2ED5843A3A3110CE7A896E264,
	FirestoreCppPINVOKE_FieldValueProxy_timestamp_value_m88D7790D42AB3ED1BCFBC7EB804D491381BC70C2,
	FirestoreCppPINVOKE_FieldValueProxy_string_value_mFDCE154C41EAF0CE78B2F03C8ACB41E4928C232E,
	FirestoreCppPINVOKE_FieldValueProxy_blob_value_m88E1267F2BB5F469B32811F4BC30614BC816A0C0,
	FirestoreCppPINVOKE_FieldValueProxy_blob_size_m49C2CF17E630F952A6E8323DF62FD5351CDD7765,
	FirestoreCppPINVOKE_FieldValueProxy_reference_value_mA8D39A1B8741BA03492F75D72F92D2912F4C8F5E,
	FirestoreCppPINVOKE_FieldValueProxy_geo_point_value_m515D4D98AD1253E36CE86D37B2D71071616E3FB2,
	FirestoreCppPINVOKE_FieldValueProxy_ServerTimestamp_m34C0FBC6EC196D317BC583FEBEF5A7F8FDC09AEE,
	FirestoreCppPINVOKE_delete_QueryProxy_m3CC14B4DE5F692426CF5AC816CF7A36594D02EC0,
	FirestoreCppPINVOKE_QueryProxy_Get__SWIG_0_m974FBF73F94964C4CC4DC0864CFB360E320B3D4D,
	FirestoreCppPINVOKE_delete_QuerySnapshotProxy_m3C340D7F4B6F3543079EADBEE7D02E1F8238D2FA,
	FirestoreCppPINVOKE_QuerySnapshotProxy_size_m905545AD4D76104A3EFAEE929726256DB454D2E5,
	FirestoreCppPINVOKE_SettingsProxy_kCacheSizeUnlimited_get_m60AB292DAC6E095B8CBC2A46DCF57657DEB14432,
	FirestoreCppPINVOKE_new_SettingsProxy__SWIG_0_mA6BE0B0778FC09B636AF1AE99D79BF245CAD7B46,
	FirestoreCppPINVOKE_SettingsProxy_host_m64FAEDE9BADDECF44508FF20694283945B569644,
	FirestoreCppPINVOKE_SettingsProxy_is_ssl_enabled_m58F1ACF926B6EC945A1676A7BBC9ED604C5122D2,
	FirestoreCppPINVOKE_SettingsProxy_is_persistence_enabled_m306CFECE628778A30D17DC59BC458F091EC70C4A,
	FirestoreCppPINVOKE_SettingsProxy_cache_size_bytes_m5BAAC2A21A894932F8F10F4F1F8847DE03BE53A8,
	FirestoreCppPINVOKE_SettingsProxy_set_host_m541D3AE1A6E97C0823E11E13E8F61AE8E7614E7E,
	FirestoreCppPINVOKE_SettingsProxy_set_ssl_enabled_mC673F87371DB4FDE4688240069BB0EA787063AC4,
	FirestoreCppPINVOKE_SettingsProxy_set_persistence_enabled_m68F926AF8EB51884E306B9623F972CCBCC2AF2EA,
	FirestoreCppPINVOKE_SettingsProxy_set_cache_size_bytes_m12DFB5CAB7AB63957C4CB609BAB6051509EA6B1E,
	FirestoreCppPINVOKE_delete_SettingsProxy_mAE2D0376BE9770810CD4852E9059607BD1839890,
	FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_documents_loaded_mB09A1C1F15C2A57C2AB6B0863D182DE0C054223B,
	FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_total_documents_m7FBF86A7D81E658583B4381E965A60B533321B39,
	FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_bytes_loaded_m1D1E5DE2F1700B970EAA162FEC9B5A6C3308BBFE,
	FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_total_bytes_m71F3391EC2D2E204942F6F8460AB8C2DBDBA0E55,
	FirestoreCppPINVOKE_LoadBundleTaskProgressProxy_state_m717C1E7627C3016E6C6FCE3A73D7973918FCD24B,
	FirestoreCppPINVOKE_delete_LoadBundleTaskProgressProxy_m03B1E486BBD2695FC99703448344065D51FC0631,
	FirestoreCppPINVOKE_FirestoreProxy_GetInstance__SWIG_1_mE1595790722C877222C15EB939B3F08037E66D7B,
	FirestoreCppPINVOKE_delete_FirestoreProxy_mF71A6690E52EC06178D68E0B15A3F5B778F4BBFB,
	FirestoreCppPINVOKE_FirestoreProxy_CollectionGroup__SWIG_0_m993D69F158A995610C9651B2E99C462548BCEF15,
	FirestoreCppPINVOKE_FirestoreProxy_settings_mA004D75326FF2536B748FA3BE8A708151B3B7B45,
	FirestoreCppPINVOKE_FirestoreProxy_set_settings_mAA19975048F95D218741CC6C23F2F423FE673677,
	FirestoreCppPINVOKE_QueryEquals_mC63706486270D02EB781865DBD61850C397C83B5,
	FirestoreCppPINVOKE_QuerySnapshotEquals_mF4A654C5E162F6CEDB8F87B583849A70D3F88467,
	FirestoreCppPINVOKE_DocumentSnapshotEquals_m9AD6A0988A915B245C343B9E407E6A4844F1A4DE,
	FirestoreCppPINVOKE_QueryHashCode_mCC2B92BC7D942E9E26E1E66DC796BF4BCB02EB5B,
	FirestoreCppPINVOKE_QuerySnapshotHashCode_m8FBF5FD1D9185A8856A2F459BAD19185B90A65AB,
	FirestoreCppPINVOKE_DocumentSnapshotHashCode_mDC50ED8304FABD8CB65E9721BFAF976D14EF648C,
	FirestoreCppPINVOKE_ApiHeaders_SetClientLanguage_mAED04C8052279387DAE8A6A92B453832ECA40010,
	FirestoreCppPINVOKE_ConvertFieldValueToMap_mA755FAB940F1313440F5CDEA38B6E976B4265D3E,
	FirestoreCppPINVOKE_ConvertSnapshotToFieldValue_m22C0FCF93B14E4F2FAF58DA4C9546414339B0DFD,
	FirestoreCppPINVOKE_delete_TransactionCallbackProxy_m1BEC73F04259FA17229463C43167FB2FFCAC3D64,
	FirestoreCppPINVOKE_new_TransactionManagerProxy_m200FB31B949492773422F816C8C9D83901F46979,
	FirestoreCppPINVOKE_delete_TransactionManagerProxy_mA1BD29437A086FD0A1BFDA9E7F8AD7DD2EC1165B,
	FirestoreCppPINVOKE_TransactionManagerProxy_CppDispose_mA2809A796E9275BDD1F0CAFEDA9DA943B4B35B4C,
	FirestoreCppPINVOKE_FieldToValueMap_Iterator_mACD511189356E195CABE8CC1874AB3298F31FE84,
	FirestoreCppPINVOKE_delete_FieldToValueMap_mF04DED31573FAB5955094C35EC8FD7F5BFC5E9DD,
	FirestoreCppPINVOKE_FieldToValueMapIterator_HasMore_m4EB378D64CDD5D5893BDF41B49B2AE9145C1037C,
	FirestoreCppPINVOKE_FieldToValueMapIterator_Advance_mDF0C26523CA6D4AEFBE2DE328DD0E104C9316670,
	FirestoreCppPINVOKE_FieldToValueMapIterator_UnsafeKeyView_m7012DA04F689FC79C886878CF941C78C0C4BF3D1,
	FirestoreCppPINVOKE_FieldToValueMapIterator_UnsafeValueView_m3DB4723764C23CFC7861ED1F73388D6B0428569A,
	FirestoreCppPINVOKE_delete_FieldToValueMapIterator_m03BAC72E6A785D3528B92C75DE93C9EFBE5153BC,
	FirestoreCppPINVOKE_ConvertFieldValueToVector_mBA6762E2018B251AB202482FF23339CBD7C0C3D1,
	FirestoreCppPINVOKE_QuerySnapshotDocuments_m2D55EDD87FB9E2773E23A0BCD79234F588E4F9BB,
	FirestoreCppPINVOKE_DocumentSnapshotVector_GetCopy_mA81A1E3022BC46CE132CA083513E626F30AF2BD5,
	FirestoreCppPINVOKE_delete_DocumentSnapshotVector_mAB18D783470DA2812C15C5AED9907A5719C91125,
	FirestoreCppPINVOKE_FieldValueVector_Size_mA5E22CD841D28236FB7EE576F4BE11E80AC236C2,
	FirestoreCppPINVOKE_FieldValueVector_GetUnsafeView_m33CFD025F82EB22E7338297D8AA5FDF0D271D36B,
	FirestoreCppPINVOKE_delete_FieldValueVector_m103D29FE0BB27CD7A3841121F460AB3F31BB7C66,
	FirestoreCppPINVOKE_Future_QuerySnapshot_SWIGUpcast_mE83DF18AE623C9F24B94CF522AE5B60892D138FB,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_FirestoreCpp_mA27140A6DF79C4D73741BB8CBBA31E55EDBC3D48,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_FirestoreCpp_m286156D22FD52C780B7C76C17E7F90B597B3FBC8,
	SWIGExceptionHelper_SetPendingApplicationException_mBF137E9081B46089C8BD08A3FFEE1E0FE1F0E3A8,
	SWIGExceptionHelper_SetPendingArithmeticException_mF99EDA71B878CF6361E9564B89784B3A561FFA52,
	SWIGExceptionHelper_SetPendingDivideByZeroException_mCE9772E2D30699F715F2661C75C5EF14C086E9E8,
	SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m921D13D0A68FB4EBC4056309B8E3449B10747CEB,
	SWIGExceptionHelper_SetPendingInvalidCastException_m9947EBEA346C986795F984A0BFEE5CF35F5B6D34,
	SWIGExceptionHelper_SetPendingInvalidOperationException_m4BF2D51569FE111F005303CC1F1B4092A7543587,
	SWIGExceptionHelper_SetPendingIOException_m12107E67FA15993E46B0E20B06151035CCE6E143,
	SWIGExceptionHelper_SetPendingNullReferenceException_m0CAF64A4594D264026F68FB9A835A0BC1FADD791,
	SWIGExceptionHelper_SetPendingOutOfMemoryException_m514E19096CE25864CBF0CB58DF723C735A7667D9,
	SWIGExceptionHelper_SetPendingOverflowException_m7B3159CEF1FD67EB6B7B517C020441417888E1AC,
	SWIGExceptionHelper_SetPendingSystemException_m011EB1DF3148DBBBC161AC80CFCBB47D888CB10D,
	SWIGExceptionHelper_SetPendingArgumentException_m927C534E2487873AF124759896433BDAC30405C2,
	SWIGExceptionHelper_SetPendingArgumentNullException_m338ED5CC0C522534644D5024F1B68F1C09E9FE75,
	SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m7661B82740D6603B2BF70398B38A72E05D4FA57B,
	SWIGExceptionHelper__cctor_mAECB4852A7ABEFD75C0F696024F095D6D78E3F93,
	SWIGExceptionHelper__ctor_m8120EF656206FC9E220CC23DFD3C5C28ED53CBC1,
	ExceptionDelegate__ctor_m7C66927063A66072331A88F3D6D32F75A21E5FDA,
	ExceptionDelegate_Invoke_m56700204F1F736396D178FFE8E2FDAF3149A6AD2,
	ExceptionArgumentDelegate__ctor_m65357F61679FCC25B814208939F292E587C46BC6,
	ExceptionArgumentDelegate_Invoke_m32DAE56901FA62335569CAF69E436140D0A90137,
	SWIGPendingException_get_Pending_m214FDF3047410EA90174F6E19ABC3C2D8E2956EF,
	SWIGPendingException_Set_mA9B1EB08A214DA9373CBB0993566FCB150DC41D8,
	SWIGPendingException_Retrieve_m978AEFFD56BD51D4AD84CF3C84CE4D03E1E029ED,
	SWIGPendingException__cctor_m61BE33ED53C2EACB33D680F2BBE0EDDF06C3482D,
	SWIGStringHelper_SWIGRegisterStringCallback_FirestoreCpp_m6727393A56A336E69FE33B642376FCBAA03764FE,
	SWIGStringHelper_CreateString_m0B36085212BD3538E5F889D7EB968E3AC83D5049,
	SWIGStringHelper__cctor_m6CA2B13BE11737CABAF6A0B2446791854802881F,
	SWIGStringHelper__ctor_mDAD5A170B29364E400EF22B098A5CD1474573EB9,
	SWIGStringDelegate__ctor_mEFB8B3340012F9D4F2DF72E8AEC8CBC918B4849C,
	SWIGStringDelegate_Invoke_mA4BC2B23570BA95DB601F35A04DD2845D71A9BDD,
	FirestoreExceptionHelper_FirestoreExceptionRegisterCallback_m66A2D863877C0B04C08A4E9E0755CE5CF1F201C6,
	FirestoreExceptionHelper_SetPendingFirestoreException_m87C105F291E79CF2C83734D34986011FF88B8E0E,
	FirestoreExceptionHelper__cctor_m4B519A31665D539F158D42E743ED908E434E82D5,
	FirestoreExceptionHelper__ctor_mA3DF4F6A7DE67BD7CD0232592519D59EE1783CD3,
	FirestoreExceptionDelegate__ctor_mFBFC4C7ED1ECB09D7C1BF5F893FAF8BDDB1DFCE3,
	FirestoreExceptionDelegate_Invoke_mBB306AE8E18D8B268FD1FCD2B6310F7EC866CE6F,
	FirestoreCpp_QueryEquals_m05700E01B1139E67DA2C0DB6A796FDC8E8C84A9E,
	FirestoreCpp_QuerySnapshotEquals_mED2AE6C7140A1FDB4E3E2EE0759382347005D443,
	FirestoreCpp_DocumentSnapshotEquals_m47F73AF9C8A7EFE1941809448C6D727DFFACFD4D,
	FirestoreCpp_QueryHashCode_m93D9495962C8C40D5A1290F835CAD4260B9F3F98,
	FirestoreCpp_QuerySnapshotHashCode_m6BEFED4A64C006D3261A4497A489D5D65E3EEC72,
	FirestoreCpp_DocumentSnapshotHashCode_m93DCEC6ABE286CC9AA3563B18869F93C04898273,
	FirestoreCpp_ConvertFieldValueToMap_mBB3C7530B1A5B61F2D5DE1C7F2CFEA8821963E7F,
	FirestoreCpp_ConvertSnapshotToFieldValue_mF71FF010520F1F76FB334CC9F0FD79E00803C70F,
	FirestoreCpp_ConvertFieldValueToVector_mDF6DB3E374E8035FECADF0BA3228A5053A77D5B5,
	FirestoreCpp_QuerySnapshotDocuments_mCAE514D7F89F2E30F88D6D5B45B8A390EDA39426,
	SWIGTYPE_p_unsigned_char__ctor_mE6B6E3D2C65338756D9BD3C80BAF63A6D6E9D16E,
	SWIGTYPE_p_unsigned_char_getCPtr_m5EAFEE91F7D6C12936AB5441350878116A9DC7A7,
	AssertFailedException__ctor_m53F4F8F9955824F936C29CFDA9347EC97BE47469,
	Enums_Convert_mDF7518836B489867A57B6AC55CBAB8BB97092381,
	EnvironmentVersion_GetEnvironmentVersion_m8079C13B590EF357E6E2D48680AE56ED262E1987,
	EnvironmentVersion_FormatVersion_mAB4A3FA14785D545B2F17C1F01412B04AF2F71C1,
	Hash_LongHash_mACE6AC6CFBEBFB1C97AA507D263B261F10AE52DB,
	Hash_DoubleBitwiseHash_m4CBF0BA79E2D5F729871F8A2DFAA16A3E9A3B6F5,
	NULL,
	Preconditions_CheckNotNullOrEmpty_m79C2D067DA82E7E340C2B4EE3DB5869169D6A088,
	Preconditions_CheckState_mA468CDAAF8FBADE46412DAA9928628CBC2C506AC,
	NULL,
	NULL,
	NULL,
	Util_Unreachable_mD59FBB441C46A6F9107EE1DE7C1773AFC2BB8268,
	Util_HardAssert_m2985B5883E2CDE0DEE3610911C1F1722D3383A31,
	NULL,
	NULL,
	Util_FlattenAndThrowException_m7D52B8696CDD935D460CA99541439DF432A6F125,
	Util_FlattenException_m1DCF1E810751C25ABF54EB7EC1DC96305B8D3523,
	Util_OnPInvokeManagedException_m1EC60F6DEA68C69DFB91142EE44AFA83C59C320D,
	NULL,
	NULL,
	AnonymousTypeConverter__ctor_mEF865BE7E57D4B16F980D653C62945FB9DC44525,
	ArrayConverter__ctor_mACC03F7B4B29095E912908B41C3365F1D098F777,
	ArrayConverter_DeserializeArray_mE3E9E2742F88D61B590C14DAA61C56984CAF9B94,
	AttributedIdAssigner__ctor_m37704510524F7EED5EC0BDFC7431D280D227ADB8,
	AttributedIdAssigner_AssignId_mDBDBD1986389B23C384E9E19A4FAB6843894C905,
	AttributedIdAssigner_MaybeAssignId_m9995E21FE3872185FE173792B6F43F438841C5D9,
	AttributedIdAssigner_MaybeCreateAssigner_m18F5351C199820F835442862D483F226E1BBE88F,
	AttributedIdAssigner__cctor_m535854BB99F4A88E5F3942CF02AD720A9C048DE1,
	AttributedTypeConverter__ctor_m8ED5C7FC9F0F52604F44A95E5108B68A80E8870C,
	AttributedTypeConverter_CreateObjectCreator_m82D8C56AFBB749282F684509F38F2F9F22F85698,
	AttributedTypeConverter_ForType_m07D59D9656406621D387022B649B40E618835B89,
	AttributedTypeConverter_DeserializeMap_m2A394C4D7866BE755035DC2B4DDD1F23B821CADD,
	AttributedProperty_get_CanRead_m0ABE4E1718B14CA2B5D0298793EF68602D260ABF,
	AttributedProperty_get_CanWrite_m7B6AE4074BCDE642DD62CED67EC91624C9554D3F,
	AttributedProperty_get_IsNullableValue_mD3B48A746DD86E1B133FFE657E887A0589111149,
	AttributedProperty__ctor_m7864D6112D7B8CB02FFDFDB470602784CBD621E6,
	AttributedProperty_SetValue_mCB4494D165F52CC87E3F1F74CFC85DBCCF0C17D1,
	U3CU3Ec__DisplayClass4_0__ctor_m349C91DBF3C7A213C6107A73EB4FA595C59E64C7,
	U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__0_m14035490516D2CBF4F9D4D01C2EF2C0291E349B1,
	U3CU3Ec__DisplayClass5_0__ctor_mCDF851508E01B21AC9D4E9EA8B267F2D6AA21B81,
	U3CU3Ec__DisplayClass5_0_U3CCreateObjectCreatorU3Eb__0_m24C3F17F9CC5F0A16CD3CA566C88729EB1ADBC97,
	U3CU3Ec__DisplayClass5_1__ctor_mC1569654AB635817480C5F88AA8AD1C3970CED69,
	U3CU3Ec__DisplayClass5_1_U3CCreateObjectCreatorU3Eb__2_mC677382339E7FD82D5EABCC8BEEAF205C83E8168,
	U3CU3Ec__cctor_mBEA010AF775B54CCF304C5A88CDECA9542235EF3,
	U3CU3Ec__ctor_mF6E3ECEB7BDA7E96E52FE5485CEDBB656EE498C2,
	U3CU3Ec_U3CCreateObjectCreatorU3Eb__5_1_m688C1C95364110D2733F57F69140770F0B4B4DB4,
	ConverterBase__ctor_m0D2AA36F632B6CAE14B9AF2F314AE5A6296790DE,
	ConverterBase_DeserializeMap_m5D4F1A2FCAA936F6626B46FB1C5BC965DA9F29F3,
	ConverterBase_DeserializeValue_mEB14B633EF521927E9427E239DCEB7257A8E2A18,
	ConverterBase_DeserializeArray_mAFAD2E5E37435366E56AD61031917E20298DB828,
	ConverterBase_DeserializeBoolean_m1E01A7A72D9D085F2093EF95B2E5A76159781FBF,
	ConverterBase_DeserializeBytes_mE0C65760127B880226EC869DD5A6EA448ABA8F94,
	ConverterBase_DeserializeDouble_mD02A4030DA90AD0B9FB861E2E8F4EE4C56369B96,
	ConverterBase_DeserializeGeoPoint_m066E266508A487610272DE05D84144423ED59D42,
	ConverterBase_DeserializeInteger_mD4AE7FB7A3EC050A027369BE54D71CD6971A745F,
	ConverterBase_DeserializeReference_mC063992C08974A1FDD8EF8204422B776682074B6,
	ConverterBase_DeserializeString_m83BB735D46658CE5A610E1C638D32D492ACA913A,
	ConverterBase_DeserializeTimestamp_mE9EB5C9C86EF07C2BD611E4731B551AF7FFF3136,
	ConverterBase_ConvertFromProxyBlob_m0A0D9B0E65EC0373F55FAD5D2F6C76F92005BB9C,
	ConverterCache_GetConverter_m0B3642BC52764BACEE8ADA68CE24F68EB27C96FE,
	ConverterCache_InitializeConverterCache_m6EE88E6B7A63506CBA8E344CBFD0C14A6D105CBE,
	NULL,
	ConverterCache_CreateConverter_m0803325754F2718826081886D9C362D1EC34C8D5,
	ConverterCache_TryGetStringDictionaryValueType_m0D9AAF394A228716120BE813893CBC8938AE18F7,
	ConverterCache_MapInterfaceToDictionaryValueTypeArgument_mCC59159961ECC84D5DC8E017D56F3E817B9333F0,
	ConverterCache__cctor_mB856CC23D3F2377CEABE80DFB6E3EC8885059BF7,
	U3CU3Ec__cctor_mCB46A4DF22D85F308327070A399FD7064040DEEF,
	U3CU3Ec__ctor_mEF644C9FD3E7D28E8F691132D7173170CC503D2B,
	U3CU3Ec_U3CTryGetStringDictionaryValueTypeU3Eb__6_0_m5A15F1C5B946E007B2B8C8A185608DCD25035A4F,
	CustomConverter_ForConverterType_m93BA6ABC59D4BC6B9B949EA442D6E9F28CC41664,
	NULL,
	CustomConverter__cctor_m3E7E9A0B96B44DF49C85B44048CA600069E00F2A,
	NULL,
	NULL,
	NULL,
	NULL,
	EnumConverter__ctor_m48BD9A0C8F8B1F089A5CBC986C4292FDF19746F4,
	EnumConverter_DeserializeInteger_mD03C70F39CD403E43283A455D29801AA2E165CCD,
	EnumConverter_Int64ToEnumBaseType_mFFF3896402AAB924DF0BAA7F5321BE5460F6AA71,
	EnumerableConverter__ctor_mD759176C08F3C8F9FA3D7BA76E33A13F7AF930A5,
	EnumerableConverter_DeserializeArray_m3FE8044084F9036E98E38F94C8BE5770B9AB8EF9,
	U3CU3Ec__cctor_m4C61A18D8E81C13A12AA96622CF5C6CF58A9EEC8,
	U3CU3Ec__ctor_m97087E5D1803B984D47CAAC621D01C8810A2531B,
	U3CU3Ec_U3C_ctorU3Eb__1_0_mC65EA465ADE3F30333D7A4AD80443952B0BAAF40,
	EnumerableConverterBase__ctor_mED2B3485AD8ABC73CB9892B5BFA2A8526266783D,
	NULL,
	MapConverterBase__ctor_m9E9E142347C2CED0E133610BD909AABDF1EF1B00,
	StringConverter__ctor_mB4E28DBBCB593C8B0B104E17EF3F53F8F6E2186D,
	StringConverter_DeserializeString_m5DBF8001A288190585CB573C94EFCA0EAAB79A37,
	IntegerConverterBase__ctor_mA9476A3DF7B140631E1386724E61EA81FFE0893F,
	IntegerConverterBase_DeserializeDouble_mCD20BDEFC96DA44E122A69BF03A993BCA048B752,
	ByteConverter__ctor_mFC325B5FD5453EFB5D7F3A362AAD6AFB135D7BAD,
	ByteConverter_DeserializeInteger_m75366C8C52D7A8E3C45B7AD38DBAE758E8C0BA9A,
	SByteConverter__ctor_m8DF21A6C65A36F46C63DBBA7288B6438CB586765,
	SByteConverter_DeserializeInteger_m756B896D761C10275AE95BB4446D5872C5FD1AD7,
	Int16Converter__ctor_m5B896AE510E0FE57A2DA3F09F90706D53E01F118,
	Int16Converter_DeserializeInteger_mB51802CC353F9A8092891D6FC0FE1DE6BAD31A84,
	UInt16Converter__ctor_m5613B6AC8405974D2174FF9D7B48397752CA1F33,
	UInt16Converter_DeserializeInteger_mBB92F046B498A47235E6D5C61E1D4DF5D139D71A,
	Int32Converter__ctor_m694D9FF17B5E75F13744C0685A0056EEAB449C79,
	Int32Converter_DeserializeInteger_m0CBD454EA743313EF45C767D24B33D90CA1837D9,
	UInt32Converter__ctor_m3F47AFC41ABC2B161B86393D06ECD6566D6614B8,
	UInt32Converter_DeserializeInteger_m26885822BC645607B2FB01500B8173B823A3441A,
	Int64Converter__ctor_m24A6E97DFF8EC0E1647DA0CFFD9580C9FC5060CF,
	Int64Converter_DeserializeInteger_m7CB1F3B783A49669BDB04A2F227633189808AFFA,
	UInt64Converter__ctor_m5B5B8D8F20ED9558D4E25A55719AE79AAA8A2C70,
	UInt64Converter_DeserializeInteger_mC2073BA9F288A43CB50EDC0AF8D596DFD933F893,
	SingleConverter__ctor_m9086CBD151FBB44D1B11545223F5D8829E843BBC,
	SingleConverter_DeserializeDouble_mB961241C8778BB908BE597342F00B6D98C0C1AA1,
	SingleConverter_DeserializeInteger_m13F0D4799B0A00DC8ED8DC14B5FC787AC23A185C,
	DoubleConverter__ctor_mF5F5573F321FDA0AB37ECB051B22BE83F4CE5931,
	DoubleConverter_DeserializeDouble_mE7A5F318C5A25C9482DFF813FD9D2C43DE6D0932,
	DoubleConverter_DeserializeInteger_mD127B2E56D574E946745ADB0F426590481C3FAE4,
	BooleanConverter__ctor_mBDD68794FE108535DE5042C1A0CC225566321AE5,
	BooleanConverter_DeserializeBoolean_mB6489961CAABF117124CFFF48E1EBF3202514795,
	TimestampConverter__ctor_m26273B8B45C7E5B7CC011C58FE93131E2C53367B,
	TimestampConverter_DeserializeTimestamp_m70421B48E029A0B0DC1E16E22A1E79A2CFB8AB17,
	GeoPointConverter__ctor_mA78116EE37089A80A2CC01CFBE3E015EB11DED71,
	GeoPointConverter_DeserializeGeoPoint_m9EB968D578A7ED2DB9CCED057D36A7CED8F1D7B4,
	ByteArrayConverter__ctor_mBD1F09EBAB7411960681E0A080DDC43CCB47EE5C,
	ByteArrayConverter_DeserializeBytes_m8999DBC45439623A82D6BC8C3988A3389D929533,
	BlobConverter__ctor_m8142753A6C7B938E154FBFEFCC4438C53E41A0D0,
	BlobConverter_DeserializeBytes_m9FC7288F6F81B7EBA73F3933273B09B2F56687B7,
	FieldValueProxyConverter__ctor_m414C822AA810FFEFB76C3D47E0A7B07962D6B3F3,
	DateTimeConverter__ctor_m0FEEC838E0893B955D2ADA84D073F2E90D396294,
	DateTimeConverter_DeserializeTimestamp_m1818DB588574DAFC5EA11EEE574F2CEE4354C098,
	DateTimeOffsetConverter__ctor_m6692EC537B17F33E0C4C08260AD10F605CD152DB,
	DateTimeOffsetConverter_DeserializeTimestamp_m0CAF92C2407C86FA8186C6C266762FE02EE436EB,
	DocumentReferenceConverter__ctor_m944C476BC390D26369F42B3ADC85A158A99F7059,
	DocumentReferenceConverter_DeserializeReference_mEEB648CEA7BBAF559C1DA6237CEFD6517FEFBBE0,
};
extern void Blob_get_Length_m41B38E18238097414A3A20FD3DC2156970A31502_AdjustorThunk (void);
extern void Blob_Equals_m4BAAC02C0A8F997F10D849409E37342FBCFD5A5C_AdjustorThunk (void);
extern void Blob_GetHashCode_m228609F9AD65008385472F6A0B25061FDFD851E8_AdjustorThunk (void);
extern void Blob_Equals_m56D099DEE926AAE8091860F91307FCD4FA48E50C_AdjustorThunk (void);
extern void Blob_ToString_m3561726740B5378812B18356C8998B71F99A1298_AdjustorThunk (void);
extern void Blob__ctor_m1F16308CEC0FA8E3447D18B2168D01921195A6AB_AdjustorThunk (void);
extern void GeoPoint_get_Latitude_mE91F389BEBC7A0B0D91DE25D71C232937E6F3014_AdjustorThunk (void);
extern void GeoPoint_get_Longitude_m133E6D1B3E1991A9DE1459F186D0A9EDEA05E582_AdjustorThunk (void);
extern void GeoPoint__ctor_mFD3EA7D3029F84DCEFF17D456E7D40D7DA648A34_AdjustorThunk (void);
extern void GeoPoint_Equals_m0414DB7E087F0DD44CCE0742C1ED9B6D42744902_AdjustorThunk (void);
extern void GeoPoint_GetHashCode_m120579279AD807DE874974BD3E15392B8BD01B51_AdjustorThunk (void);
extern void GeoPoint_Equals_m3B2A6102D80DB864F3CE3290B0B347A879974646_AdjustorThunk (void);
extern void GeoPoint_ToString_mE7103148304B568F0B0CFDF26C1BA558A0508248_AdjustorThunk (void);
extern void Timestamp_ToDateTime_mDB1ACFC2FB995894CCED239A5E557CDA12F066FE_AdjustorThunk (void);
extern void Timestamp_ToDateTimeOffset_mC1AB3B395A37909C8A0E01DB07C55C0E6AD3ED46_AdjustorThunk (void);
extern void Timestamp_Equals_m4662FEFEF81799C37FB99795E492F562E55CA253_AdjustorThunk (void);
extern void Timestamp_GetHashCode_mFF00599A75371E96B5E55636FF40902F31BCFCEC_AdjustorThunk (void);
extern void Timestamp_Equals_mAD77D232E60E042104147E04E8D3331CBB35D8BE_AdjustorThunk (void);
extern void Timestamp_CompareTo_mE49BD77BFD6AAD4ACEF74F1578B2ACA8929EC6A1_AdjustorThunk (void);
extern void Timestamp_CompareTo_m3E776402C9986CE96A20DF4812C32A70B164780E_AdjustorThunk (void);
extern void Timestamp_ToString_mD0ECC96BAFA1DBE375F79E550C63D8897CD77AD8_AdjustorThunk (void);
extern void Timestamp__ctor_m4C5388F8B2158FA8F3A104CFE55ACD0024554D53_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[22] = 
{
	{ 0x06000001, Blob_get_Length_m41B38E18238097414A3A20FD3DC2156970A31502_AdjustorThunk },
	{ 0x06000003, Blob_Equals_m4BAAC02C0A8F997F10D849409E37342FBCFD5A5C_AdjustorThunk },
	{ 0x06000004, Blob_GetHashCode_m228609F9AD65008385472F6A0B25061FDFD851E8_AdjustorThunk },
	{ 0x06000005, Blob_Equals_m56D099DEE926AAE8091860F91307FCD4FA48E50C_AdjustorThunk },
	{ 0x06000006, Blob_ToString_m3561726740B5378812B18356C8998B71F99A1298_AdjustorThunk },
	{ 0x06000007, Blob__ctor_m1F16308CEC0FA8E3447D18B2168D01921195A6AB_AdjustorThunk },
	{ 0x06000048, GeoPoint_get_Latitude_mE91F389BEBC7A0B0D91DE25D71C232937E6F3014_AdjustorThunk },
	{ 0x06000049, GeoPoint_get_Longitude_m133E6D1B3E1991A9DE1459F186D0A9EDEA05E582_AdjustorThunk },
	{ 0x0600004A, GeoPoint__ctor_mFD3EA7D3029F84DCEFF17D456E7D40D7DA648A34_AdjustorThunk },
	{ 0x0600004B, GeoPoint_Equals_m0414DB7E087F0DD44CCE0742C1ED9B6D42744902_AdjustorThunk },
	{ 0x0600004C, GeoPoint_GetHashCode_m120579279AD807DE874974BD3E15392B8BD01B51_AdjustorThunk },
	{ 0x0600004D, GeoPoint_Equals_m3B2A6102D80DB864F3CE3290B0B347A879974646_AdjustorThunk },
	{ 0x0600004E, GeoPoint_ToString_mE7103148304B568F0B0CFDF26C1BA558A0508248_AdjustorThunk },
	{ 0x06000073, Timestamp_ToDateTime_mDB1ACFC2FB995894CCED239A5E557CDA12F066FE_AdjustorThunk },
	{ 0x06000074, Timestamp_ToDateTimeOffset_mC1AB3B395A37909C8A0E01DB07C55C0E6AD3ED46_AdjustorThunk },
	{ 0x06000075, Timestamp_Equals_m4662FEFEF81799C37FB99795E492F562E55CA253_AdjustorThunk },
	{ 0x06000076, Timestamp_GetHashCode_mFF00599A75371E96B5E55636FF40902F31BCFCEC_AdjustorThunk },
	{ 0x06000077, Timestamp_Equals_mAD77D232E60E042104147E04E8D3331CBB35D8BE_AdjustorThunk },
	{ 0x06000078, Timestamp_CompareTo_mE49BD77BFD6AAD4ACEF74F1578B2ACA8929EC6A1_AdjustorThunk },
	{ 0x06000079, Timestamp_CompareTo_m3E776402C9986CE96A20DF4812C32A70B164780E_AdjustorThunk },
	{ 0x0600007A, Timestamp_ToString_mD0ECC96BAFA1DBE375F79E550C63D8897CD77AD8_AdjustorThunk },
	{ 0x0600007B, Timestamp__ctor_m4C5388F8B2158FA8F3A104CFE55ACD0024554D53_AdjustorThunk },
};
static const int32_t s_InvokerIndices[553] = 
{
	7083,
	10208,
	4133,
	7083,
	4025,
	7114,
	5754,
	3206,
	10683,
	7114,
	7114,
	7114,
	7083,
	4133,
	4133,
	7114,
	8540,
	10889,
	3202,
	1120,
	3206,
	7114,
	7012,
	5113,
	0,
	4133,
	4133,
	7083,
	3206,
	7252,
	3206,
	7252,
	7114,
	5754,
	10847,
	10466,
	5117,
	10679,
	9907,
	0,
	7252,
	10889,
	3202,
	5725,
	3202,
	2924,
	7252,
	5117,
	7252,
	7114,
	5754,
	7252,
	7114,
	7012,
	7012,
	7084,
	0,
	7252,
	7114,
	10889,
	7114,
	7012,
	7012,
	7084,
	7083,
	7114,
	2948,
	2948,
	5725,
	7114,
	7114,
	7040,
	7040,
	2636,
	4133,
	7083,
	4081,
	7114,
	10300,
	5754,
	7083,
	5725,
	7083,
	5725,
	7084,
	5726,
	7084,
	5726,
	7083,
	5725,
	4133,
	7083,
	3206,
	10683,
	7114,
	5113,
	4133,
	4133,
	7083,
	8540,
	10889,
	5117,
	3202,
	1120,
	3206,
	7114,
	7083,
	4133,
	4133,
	7083,
	7114,
	7114,
	7252,
	10333,
	7029,
	7030,
	4133,
	7083,
	4238,
	4904,
	4834,
	7114,
	3165,
	10588,
	10889,
	7114,
	7114,
	7114,
	5754,
	5117,
	0,
	0,
	0,
	0,
	0,
	0,
	5754,
	10847,
	5754,
	5117,
	10889,
	3206,
	7252,
	7252,
	10889,
	8914,
	10466,
	3171,
	7252,
	7252,
	5650,
	7040,
	7040,
	3171,
	7252,
	7252,
	5650,
	7084,
	7083,
	7114,
	3171,
	5650,
	10466,
	7252,
	5754,
	5728,
	10679,
	2476,
	5728,
	7114,
	10889,
	3202,
	7252,
	3202,
	5725,
	7252,
	7252,
	3171,
	7252,
	7252,
	5650,
	7114,
	7114,
	7012,
	3171,
	10303,
	7252,
	7252,
	5650,
	7114,
	7012,
	3171,
	10303,
	7252,
	7252,
	5650,
	7083,
	7012,
	7012,
	7084,
	7040,
	7114,
	7114,
	7114,
	7235,
	7114,
	7114,
	10847,
	3171,
	10303,
	7252,
	7252,
	5650,
	5113,
	3171,
	10303,
	7252,
	7252,
	5650,
	7235,
	3171,
	10303,
	7252,
	7252,
	5650,
	7252,
	7114,
	7012,
	7012,
	7084,
	5754,
	5650,
	5650,
	5726,
	10889,
	3171,
	7252,
	7252,
	5650,
	7083,
	7083,
	7084,
	7084,
	7083,
	3171,
	10303,
	7252,
	7252,
	5650,
	10466,
	5117,
	7114,
	5754,
	10683,
	7252,
	5650,
	3171,
	7252,
	7252,
	5650,
	5754,
	7252,
	3171,
	7252,
	7252,
	5650,
	7114,
	3171,
	7252,
	7252,
	5650,
	7012,
	7252,
	7114,
	7114,
	3171,
	7252,
	7252,
	5650,
	5131,
	3171,
	7252,
	7252,
	5650,
	7235,
	5131,
	10889,
	10281,
	10281,
	10678,
	10353,
	10331,
	10460,
	10678,
	8852,
	9901,
	10367,
	10678,
	10678,
	10460,
	10460,
	10219,
	10678,
	10367,
	10219,
	10678,
	10331,
	10219,
	10219,
	10353,
	10281,
	10367,
	10460,
	10367,
	10610,
	10367,
	10367,
	10842,
	10678,
	9600,
	10678,
	10610,
	10841,
	10842,
	10460,
	10219,
	10219,
	10353,
	9902,
	9898,
	9898,
	9900,
	10678,
	10331,
	10331,
	10353,
	10353,
	10331,
	10678,
	10367,
	10678,
	9601,
	10367,
	9899,
	9384,
	9384,
	9384,
	10331,
	10331,
	10331,
	10683,
	10367,
	9600,
	10678,
	10367,
	10678,
	10678,
	10367,
	10678,
	10219,
	10678,
	10460,
	10367,
	10678,
	10367,
	10367,
	9602,
	10678,
	10610,
	9602,
	10678,
	10370,
	7449,
	9233,
	10683,
	10683,
	10683,
	10683,
	10683,
	10683,
	10683,
	10683,
	10683,
	10683,
	10683,
	9979,
	9979,
	9979,
	10889,
	7252,
	3202,
	5754,
	3202,
	3206,
	10823,
	10683,
	10847,
	10889,
	10683,
	10466,
	10889,
	7252,
	3202,
	5117,
	10683,
	10683,
	10889,
	7252,
	3202,
	5754,
	9412,
	9412,
	9412,
	10337,
	10337,
	10337,
	10466,
	9661,
	10466,
	10466,
	3171,
	10303,
	5754,
	10333,
	10847,
	10466,
	10334,
	10330,
	0,
	9667,
	9894,
	0,
	0,
	0,
	10889,
	9894,
	0,
	0,
	10683,
	10466,
	9979,
	0,
	0,
	5754,
	5754,
	2518,
	3206,
	3206,
	9979,
	10466,
	10889,
	3206,
	10466,
	10466,
	2518,
	7012,
	7012,
	7012,
	3206,
	1700,
	7252,
	4133,
	7252,
	7114,
	7252,
	7114,
	10889,
	7252,
	4133,
	5754,
	2518,
	2518,
	2518,
	2508,
	2518,
	2512,
	2513,
	2515,
	2518,
	2518,
	2519,
	10466,
	10466,
	10889,
	0,
	10466,
	9406,
	10466,
	10889,
	10889,
	7252,
	4133,
	9667,
	0,
	10889,
	0,
	0,
	0,
	0,
	5754,
	2515,
	5114,
	5754,
	2518,
	10889,
	7252,
	4133,
	5754,
	0,
	5754,
	7252,
	2518,
	5754,
	2512,
	7252,
	2515,
	7252,
	2515,
	7252,
	2515,
	7252,
	2515,
	7252,
	2515,
	7252,
	2515,
	7252,
	2515,
	7252,
	2515,
	7252,
	2512,
	2515,
	7252,
	2512,
	2515,
	7252,
	2508,
	7252,
	2519,
	7252,
	2513,
	7252,
	2518,
	7252,
	2518,
	7252,
	7252,
	2519,
	7252,
	2519,
	7252,
	2518,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[21] = 
{
	{ 0x06000011, 5,  (void**)&DocumentReference_DocumentSnapshotsHandler_m39CF98A69DDA6C278BA2586D1C383F42A3A62767_RuntimeMethod_var, 0 },
	{ 0x06000026, 8,  (void**)&FirebaseFirestore_SnapshotsInSyncHandler_m63B46E06E07A5AC034332B505171FB2779C7FB36_RuntimeMethod_var, 0 },
	{ 0x06000027, 7,  (void**)&FirebaseFirestore_LoadBundleTaskProgressHandler_mA5D2729E0A63DCF17A0443A2A46AA394EDBD4AC5_RuntimeMethod_var, 0 },
	{ 0x06000064, 14,  (void**)&Query_QuerySnapshotsHandler_mACCC5A5D3D2ECD5DCE9DEA25B771CB448DC68715_RuntimeMethod_var, 0 },
	{ 0x060000A7, 9,  (void**)&Future_QuerySnapshot_SWIG_CompletionDispatcher_mDE52F3C13385E02F9E4DDDBAE074D02C1B90EC39_RuntimeMethod_var, 0 },
	{ 0x06000177, 38,  (void**)&SWIGExceptionHelper_SetPendingApplicationException_mBF137E9081B46089C8BD08A3FFEE1E0FE1F0E3A8_RuntimeMethod_var, 0 },
	{ 0x06000178, 42,  (void**)&SWIGExceptionHelper_SetPendingArithmeticException_mF99EDA71B878CF6361E9564B89784B3A561FFA52_RuntimeMethod_var, 0 },
	{ 0x06000179, 43,  (void**)&SWIGExceptionHelper_SetPendingDivideByZeroException_mCE9772E2D30699F715F2661C75C5EF14C086E9E8_RuntimeMethod_var, 0 },
	{ 0x0600017A, 45,  (void**)&SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m921D13D0A68FB4EBC4056309B8E3449B10747CEB_RuntimeMethod_var, 0 },
	{ 0x0600017B, 46,  (void**)&SWIGExceptionHelper_SetPendingInvalidCastException_m9947EBEA346C986795F984A0BFEE5CF35F5B6D34_RuntimeMethod_var, 0 },
	{ 0x0600017C, 47,  (void**)&SWIGExceptionHelper_SetPendingInvalidOperationException_m4BF2D51569FE111F005303CC1F1B4092A7543587_RuntimeMethod_var, 0 },
	{ 0x0600017D, 44,  (void**)&SWIGExceptionHelper_SetPendingIOException_m12107E67FA15993E46B0E20B06151035CCE6E143_RuntimeMethod_var, 0 },
	{ 0x0600017E, 48,  (void**)&SWIGExceptionHelper_SetPendingNullReferenceException_m0CAF64A4594D264026F68FB9A835A0BC1FADD791_RuntimeMethod_var, 0 },
	{ 0x0600017F, 49,  (void**)&SWIGExceptionHelper_SetPendingOutOfMemoryException_m514E19096CE25864CBF0CB58DF723C735A7667D9_RuntimeMethod_var, 0 },
	{ 0x06000180, 50,  (void**)&SWIGExceptionHelper_SetPendingOverflowException_m7B3159CEF1FD67EB6B7B517C020441417888E1AC_RuntimeMethod_var, 0 },
	{ 0x06000181, 51,  (void**)&SWIGExceptionHelper_SetPendingSystemException_m011EB1DF3148DBBBC161AC80CFCBB47D888CB10D_RuntimeMethod_var, 0 },
	{ 0x06000182, 39,  (void**)&SWIGExceptionHelper_SetPendingArgumentException_m927C534E2487873AF124759896433BDAC30405C2_RuntimeMethod_var, 0 },
	{ 0x06000183, 40,  (void**)&SWIGExceptionHelper_SetPendingArgumentNullException_m338ED5CC0C522534644D5024F1B68F1C09E9FE75_RuntimeMethod_var, 0 },
	{ 0x06000184, 41,  (void**)&SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m7661B82740D6603B2BF70398B38A72E05D4FA57B_RuntimeMethod_var, 0 },
	{ 0x06000190, 52,  (void**)&SWIGStringHelper_CreateString_m0B36085212BD3538E5F889D7EB968E3AC83D5049_RuntimeMethod_var, 0 },
	{ 0x06000196, 37,  (void**)&FirestoreExceptionHelper_SetPendingFirestoreException_m87C105F291E79CF2C83734D34986011FF88B8E0E_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[14] = 
{
	{ 0x02000020, { 6, 17 } },
	{ 0x0200004F, { 37, 4 } },
	{ 0x0200005D, { 45, 3 } },
	{ 0x0200005E, { 48, 5 } },
	{ 0x06000019, { 0, 2 } },
	{ 0x06000028, { 2, 2 } },
	{ 0x06000039, { 4, 2 } },
	{ 0x060001AD, { 23, 1 } },
	{ 0x060001B0, { 24, 1 } },
	{ 0x060001B1, { 25, 2 } },
	{ 0x060001B2, { 27, 3 } },
	{ 0x060001B6, { 30, 7 } },
	{ 0x060001E5, { 41, 2 } },
	{ 0x060001EE, { 43, 2 } },
};
extern const uint32_t g_rgctx_T_t3E7212D68C7DD33E7D2C26190DD9B4016858AC65;
extern const uint32_t g_rgctx_T_t3E7212D68C7DD33E7D2C26190DD9B4016858AC65;
extern const uint32_t g_rgctx_Func_2_t8A946466852A756BE0366A95535FF9EE16FDA290;
extern const uint32_t g_rgctx_Func_2_Invoke_m4B0466DA8F5A52B6873BD63784822A605D5ADED5;
extern const uint32_t g_rgctx_Func_1_tE5F03478C4BA45B494DA8810D42A73ED67AB035C;
extern const uint32_t g_rgctx_Func_1_Invoke_m4FFFF4970435374956D9A78F184B50F7D71C35DD;
extern const uint32_t g_rgctx_Dictionary_2_t7D06C12FC18AE9760757F8D166C6D7C36A358C94;
extern const uint32_t g_rgctx_Dictionary_2__ctor_mB359A118E487C9157D28B783B61EAE40408A5C2A;
extern const uint32_t g_rgctx_ListenerRegistrationMap_1_AssertGenericArgumentIsDelegate_mD6B4F96D0D63D3CADF3702BD7882B426B31B4BAF;
extern const uint32_t g_rgctx_T_tCBC9BA58DAD854946DE3C7CA41171422DBBE0D8F;
extern const uint32_t g_rgctx_Dictionary_2_TryGetValue_m61C2ABAB1DC6AE6E173041432A05B7BCFAF0DDAE;
extern const uint32_t g_rgctx_Tuple_2_t6D94D664A4284849AA3E81F2E5A27EB9CF14ECC3;
extern const uint32_t g_rgctx_Tuple_2_get_Item2_m981B2D13C0F66A440FFF24D96C6180B779371100;
extern const uint32_t g_rgctx_Dictionary_2_Remove_mD811A38EA841DC6161A5AA8BE4C888560CDE0444;
extern const uint32_t g_rgctx_Dictionary_2_GetEnumerator_m9DC59A61E831E266B4251AD1441A9D55D0A3C92B;
extern const uint32_t g_rgctx_Enumerator_get_Current_mDC64DF38DB7C7A83C0456DBC4F4C69B84C1A450A;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Value_m1B1157D06CAD4433D39781A56C4EA6B22505FCB9;
extern const uint32_t g_rgctx_Tuple_2_get_Item1_m9004E3E09D798C1030708E0288C921638E33B9C5;
extern const uint32_t g_rgctx_KeyValuePair_2_get_Key_m148207DC37D0E440846F91191D519A7C032EF6B1;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m32064AFDBE9B2D01DF51047E9AF062C7E9BC2A5A;
extern const uint32_t g_rgctx_Enumerator_tA500F0B6285EBD11E0B01F9F7A985B7A11EDEE08;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_tA500F0B6285EBD11E0B01F9F7A985B7A11EDEE08_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_ListenerRegistrationMap_1_Unregister_mB67FCDCD8843B5CAF11334C95C16482888B7D2E7;
extern const uint32_t g_rgctx_T_tCB31BA478D4D7BCF402ECFF6C2F58FA0A4D56E37;
extern const uint32_t g_rgctx_T_t378D896BD58394004611802B5C5267A91F5E599E;
extern const uint32_t g_rgctx_T1_tD8BB1DD0D6579459778010A51FB57B368679B4A5;
extern const uint32_t g_rgctx_T2_tBCD326D29B65D4BEFC758470EE4B1618AC9D6425;
extern const uint32_t g_rgctx_T1_tC1E61A00EE36EB7A313BB5918D583BFF65F7CFF2;
extern const uint32_t g_rgctx_T2_tC774626CEFBDCC02100F0B1B1AC1C9608A5A708F;
extern const uint32_t g_rgctx_T3_t13CA4F6A50DCDC1677FA7EA95C1CFA1D98D0211F;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass5_0_2_t2ED086AF193538A4477C8BB205494265B3C51B67;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass5_0_2__ctor_m06E3152A3D3C14AB4FA0BA7B2A9533419A77A6E8;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass5_0_2_U3CMapResultU3Eb__0_m759D6FEFE9D61CB18C21E3096CE5AE48C06E1036;
extern const uint32_t g_rgctx_Func_2_tA8863DC3CD0ACFB62DAAAC9EC7AA2A04702FCBA1;
extern const uint32_t g_rgctx_Func_2__ctor_m3979632E5786DA8E62CD9C8F4AB9E5266C8D686E;
extern const uint32_t g_rgctx_Task_1_t6509CF41C4F49CD1D39EC980993AC8BB5BF691F7;
extern const uint32_t g_rgctx_Task_1_ContinueWith_TisU_tAD8ACB709263FFB3C3DF02F1FB491000C73DD47B_m1F4FBDB5E90EBDFC640B19DB5C4ED463CE87ED06;
extern const uint32_t g_rgctx_Task_1_t790DECB3639F13AA7060708094FCE4308547BC8A;
extern const uint32_t g_rgctx_Task_1_get_Result_m6C660ED2D638A5CFAD9743D0F6ACE13EBCB33DB5;
extern const uint32_t g_rgctx_Func_2_t1ADD73436C00ADEB931ECDBC8BDD3AB19A7190BF;
extern const uint32_t g_rgctx_Func_2_Invoke_m3086ADAAAD862A73AB6503F9834C9D67F8623B07;
extern const uint32_t g_rgctx_DictionaryConverter_1_t85B99DB603EF791E9269B43F7E707FD92BDAE620;
extern const uint32_t g_rgctx_DictionaryConverter_1__ctor_m692270FBA9795DF959AA5E113F355D493EED5CC7;
extern const uint32_t g_rgctx_CustomConverter_1_t841D0A3FF5FAAE3F1B5BC3833F83E2F766F5C224;
extern const uint32_t g_rgctx_CustomConverter_1__ctor_m9118068F2CB5C232D2333C4CC8D49B0AC285F8AD;
extern const uint32_t g_rgctx_FirestoreConverter_1_t8F2FCD75933BABE314A0B90AB8E908E5437E37EC;
extern const uint32_t g_rgctx_FirestoreConverter_1_FromFirestore_m4E78EC0B41CECB79C1599657DBF4FE525794DB61;
extern const uint32_t g_rgctx_T_t263838772BE08B9F5E26FDF0618EDA4F677F3865;
extern const uint32_t g_rgctx_Dictionary_2_tFD8046C63036BF82BB0FFD0D0F1049CDE70BCFF1;
extern const uint32_t g_rgctx_IDictionary_2_tBB9E362212A706A1A86B69E81DC0EEFBAF2CCB81;
extern const uint32_t g_rgctx_TValue_t76B26C6919F1655600EB77712328A9071D8EE2C3;
extern const uint32_t g_rgctx_TValue_t76B26C6919F1655600EB77712328A9071D8EE2C3;
extern const uint32_t g_rgctx_IDictionary_2_Add_m8BAB6E3CF9A9815DA4B20BB6302E8201C5441A0A;
static const Il2CppRGCTXDefinition s_rgctxValues[53] = 
{
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_t3E7212D68C7DD33E7D2C26190DD9B4016858AC65 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t3E7212D68C7DD33E7D2C26190DD9B4016858AC65 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t8A946466852A756BE0366A95535FF9EE16FDA290 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m4B0466DA8F5A52B6873BD63784822A605D5ADED5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_tE5F03478C4BA45B494DA8810D42A73ED67AB035C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_m4FFFF4970435374956D9A78F184B50F7D71C35DD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Dictionary_2_t7D06C12FC18AE9760757F8D166C6D7C36A358C94 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2__ctor_mB359A118E487C9157D28B783B61EAE40408A5C2A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ListenerRegistrationMap_1_AssertGenericArgumentIsDelegate_mD6B4F96D0D63D3CADF3702BD7882B426B31B4BAF },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tCBC9BA58DAD854946DE3C7CA41171422DBBE0D8F },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_TryGetValue_m61C2ABAB1DC6AE6E173041432A05B7BCFAF0DDAE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Tuple_2_t6D94D664A4284849AA3E81F2E5A27EB9CF14ECC3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_get_Item2_m981B2D13C0F66A440FFF24D96C6180B779371100 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_Remove_mD811A38EA841DC6161A5AA8BE4C888560CDE0444 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Dictionary_2_GetEnumerator_m9DC59A61E831E266B4251AD1441A9D55D0A3C92B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_mDC64DF38DB7C7A83C0456DBC4F4C69B84C1A450A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Value_m1B1157D06CAD4433D39781A56C4EA6B22505FCB9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Tuple_2_get_Item1_m9004E3E09D798C1030708E0288C921638E33B9C5 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_KeyValuePair_2_get_Key_m148207DC37D0E440846F91191D519A7C032EF6B1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_m32064AFDBE9B2D01DF51047E9AF062C7E9BC2A5A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_tA500F0B6285EBD11E0B01F9F7A985B7A11EDEE08 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_tA500F0B6285EBD11E0B01F9F7A985B7A11EDEE08_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_ListenerRegistrationMap_1_Unregister_mB67FCDCD8843B5CAF11334C95C16482888B7D2E7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tCB31BA478D4D7BCF402ECFF6C2F58FA0A4D56E37 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t378D896BD58394004611802B5C5267A91F5E599E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T1_tD8BB1DD0D6579459778010A51FB57B368679B4A5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T2_tBCD326D29B65D4BEFC758470EE4B1618AC9D6425 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T1_tC1E61A00EE36EB7A313BB5918D583BFF65F7CFF2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T2_tC774626CEFBDCC02100F0B1B1AC1C9608A5A708F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T3_t13CA4F6A50DCDC1677FA7EA95C1CFA1D98D0211F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass5_0_2_t2ED086AF193538A4477C8BB205494265B3C51B67 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass5_0_2__ctor_m06E3152A3D3C14AB4FA0BA7B2A9533419A77A6E8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass5_0_2_U3CMapResultU3Eb__0_m759D6FEFE9D61CB18C21E3096CE5AE48C06E1036 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_tA8863DC3CD0ACFB62DAAAC9EC7AA2A04702FCBA1 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2__ctor_m3979632E5786DA8E62CD9C8F4AB9E5266C8D686E },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Task_1_t6509CF41C4F49CD1D39EC980993AC8BB5BF691F7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Task_1_ContinueWith_TisU_tAD8ACB709263FFB3C3DF02F1FB491000C73DD47B_m1F4FBDB5E90EBDFC640B19DB5C4ED463CE87ED06 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Task_1_t790DECB3639F13AA7060708094FCE4308547BC8A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Task_1_get_Result_m6C660ED2D638A5CFAD9743D0F6ACE13EBCB33DB5 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_2_t1ADD73436C00ADEB931ECDBC8BDD3AB19A7190BF },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_2_Invoke_m3086ADAAAD862A73AB6503F9834C9D67F8623B07 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_DictionaryConverter_1_t85B99DB603EF791E9269B43F7E707FD92BDAE620 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_DictionaryConverter_1__ctor_m692270FBA9795DF959AA5E113F355D493EED5CC7 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_CustomConverter_1_t841D0A3FF5FAAE3F1B5BC3833F83E2F766F5C224 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_CustomConverter_1__ctor_m9118068F2CB5C232D2333C4CC8D49B0AC285F8AD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_FirestoreConverter_1_t8F2FCD75933BABE314A0B90AB8E908E5437E37EC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_FirestoreConverter_1_FromFirestore_m4E78EC0B41CECB79C1599657DBF4FE525794DB61 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t263838772BE08B9F5E26FDF0618EDA4F677F3865 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_Dictionary_2_tFD8046C63036BF82BB0FFD0D0F1049CDE70BCFF1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_IDictionary_2_tBB9E362212A706A1A86B69E81DC0EEFBAF2CCB81 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_TValue_t76B26C6919F1655600EB77712328A9071D8EE2C3 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TValue_t76B26C6919F1655600EB77712328A9071D8EE2C3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_IDictionary_2_Add_m8BAB6E3CF9A9815DA4B20BB6302E8201C5441A0A },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Firebase_Firestore_CodeGenModule;
const Il2CppCodeGenModule g_Firebase_Firestore_CodeGenModule = 
{
	"Firebase.Firestore.dll",
	553,
	s_methodPointers,
	22,
	s_adjustorThunks,
	s_InvokerIndices,
	21,
	s_reversePInvokeIndices,
	14,
	s_rgctxIndices,
	53,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* LogUtil_LogMessageFromCallback_mE97D3007172D52FD246643DC17523BA77867EC1C_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingApplicationException_m6C69E5AD1E4D604354F6077B67D5772F795426E6_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentException_m7AB4E7B60FE6666951EB975CB0D7095D5FF2DDC4_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentNullException_m4B3F36D2AA2C7D504EB1822AB727B69D90C077F2_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m8C7D11A5CE60E31555D0C41E55DF4AE610052309_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArithmeticException_m4CDF27D022538556DA8E54F2DBB6957D0419523B_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingDivideByZeroException_mAF2154299A69434F0D8175854649FAEC212D6E88_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIOException_m932608D47F516BBA327D41B529BC101EC07E6176_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_mD1828B56CAF9C5635AA0F7E49B6856DE01F73DE3_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidCastException_mB702FABB41074EFCA6FFBFD8B24669CA879077DB_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidOperationException_m32C61E47D26CB43C215DC086CF5D7A4B9E853716_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingNullReferenceException_m4BE790ED7CD62E6B2281C9B040F815697FD4254D_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOutOfMemoryException_mDB9A2DA0C3243B3E047219380EF92B33F5DA575C_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOverflowException_mCA37E08F462E1BF14D59A78731D61016F5D92EA5_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingSystemException_mE70086ECFA51014FFF3D0557844E07DEF2FA8F24_RuntimeMethod_var;
extern const RuntimeMethod* SWIGStringHelper_CreateString_m4EDC140847BEAA68EFFBFC908E592173558433B9_RuntimeMethod_var;



// 0x00000001 System.Void Firebase.AppOptions::Dispose()
extern void AppOptions_Dispose_m87505E3792614A116365111E658398473782B604 (void);
// 0x00000002 System.Void Firebase.AppOptions::set_DatabaseUrl(System.Uri)
extern void AppOptions_set_DatabaseUrl_m50A56D4AFD426801574A5008562EA3683E8516C9 (void);
// 0x00000003 System.Void Firebase.AppOptions::set_AppId(System.String)
extern void AppOptions_set_AppId_m7654E20AC351E037813CCC3D88FED7BEE8DA0700 (void);
// 0x00000004 System.Void Firebase.AppOptions::set_ApiKey(System.String)
extern void AppOptions_set_ApiKey_m3010B35DBC7124293B7AC9091BA86114ED1BBF30 (void);
// 0x00000005 System.Void Firebase.AppOptions::set_MessageSenderId(System.String)
extern void AppOptions_set_MessageSenderId_mA0ABA93E9402035C509A97B3B2FFFB00B0ABFAAB (void);
// 0x00000006 System.String Firebase.AppOptions::get_StorageBucket()
extern void AppOptions_get_StorageBucket_m93625BCEE94287655E69A3ED0D145E64F66278A4 (void);
// 0x00000007 System.Void Firebase.AppOptions::set_StorageBucket(System.String)
extern void AppOptions_set_StorageBucket_m797A7F991A2674B2012887527B149846109B8E4B (void);
// 0x00000008 System.Void Firebase.AppOptions::set_ProjectId(System.String)
extern void AppOptions_set_ProjectId_m906F7A47E973C68F1BDC0697BE341394FFE32471 (void);
// 0x00000009 System.Void Firebase.AppOptions::set_PackageName(System.String)
extern void AppOptions_set_PackageName_mF0E66EF290BDA09FBE5B5108B60FBE8E24CC225A (void);
// 0x0000000A System.Void Firebase.AppOptions::.ctor(Firebase.AppOptionsInternal)
extern void AppOptions__ctor_m24702254E13B765824AB5738D6C248AE0EADAE28 (void);
// 0x0000000B System.Void Firebase.FirebaseException::.ctor(System.Int32,System.String)
extern void FirebaseException__ctor_m18D67DA955D2B4EA2BC58BCE0E96AC0A177DD70F (void);
// 0x0000000C System.Void Firebase.FirebaseException::set_ErrorCode(System.Int32)
extern void FirebaseException_set_ErrorCode_m65B2880424E85063D56405A009DAA13E3B106465 (void);
// 0x0000000D System.Void Firebase.InitializationException::set_InitResult(Firebase.InitResult)
extern void InitializationException_set_InitResult_m94032AD57F63718F6F20625FDB98958766C9D764 (void);
// 0x0000000E System.Void Firebase.InitializationException::.ctor(Firebase.InitResult,System.String)
extern void InitializationException__ctor_mC48C74EE90B137CDEA82068C2E1695D81974C5BF (void);
// 0x0000000F System.Void Firebase.InitializationException::.ctor(Firebase.InitResult,System.String,System.Exception)
extern void InitializationException__ctor_m1384021A3E1B7B0E372257380559D926BD6200BF (void);
// 0x00000010 System.String Firebase.ErrorMessages::get_DependencyNotFoundErrorMessage()
extern void ErrorMessages_get_DependencyNotFoundErrorMessage_mDFD86A03DEEC7B060CC83CEA7A0F764BCF0D294D (void);
// 0x00000011 System.String Firebase.ErrorMessages::get_DllNotFoundExceptionErrorMessage()
extern void ErrorMessages_get_DllNotFoundExceptionErrorMessage_m57791B6CE3DD0E02D5BF2423D450999E64CEA02A (void);
// 0x00000012 System.Void Firebase.ErrorMessages::.cctor()
extern void ErrorMessages__cctor_m01785142FD3B6466ED8C3D9106F8286A31357483 (void);
// 0x00000013 System.Void Firebase.LogUtil::.cctor()
extern void LogUtil__cctor_m1C7054AC325AA0382EE9C3F74303E6C0836D9BA6 (void);
// 0x00000014 System.Void Firebase.LogUtil::InitializeLogging()
extern void LogUtil_InitializeLogging_m115AF75DC00C3EB122A9232967C985A343600E0F (void);
// 0x00000015 Firebase.Platform.PlatformLogLevel Firebase.LogUtil::ConvertLogLevel(Firebase.LogLevel)
extern void LogUtil_ConvertLogLevel_mEE4246508FD42F7EB6A14DF9985479B93C7CB6D2 (void);
// 0x00000016 System.Void Firebase.LogUtil::LogMessage(Firebase.LogLevel,System.String)
extern void LogUtil_LogMessage_m59195C58FF0FC63681CED394EB6618F03A25B3B4 (void);
// 0x00000017 System.Void Firebase.LogUtil::LogMessageFromCallback(Firebase.LogLevel,System.String)
extern void LogUtil_LogMessageFromCallback_mE97D3007172D52FD246643DC17523BA77867EC1C (void);
// 0x00000018 System.Void Firebase.LogUtil::.ctor()
extern void LogUtil__ctor_mFE64F3E0CAE4C8D317093D419552825F2187F3EA (void);
// 0x00000019 System.Void Firebase.LogUtil::Finalize()
extern void LogUtil_Finalize_mA58D6095B47CD414CEED5AB924C2D53F34FF9D55 (void);
// 0x0000001A System.Void Firebase.LogUtil::Dispose()
extern void LogUtil_Dispose_m69B36B965145091F6023543E577B1D882AAD3F31 (void);
// 0x0000001B System.Void Firebase.LogUtil::Dispose(System.Boolean)
extern void LogUtil_Dispose_m97EA8C366043F8F98301F73F488901880DA431CB (void);
// 0x0000001C System.Void Firebase.LogUtil::<.ctor>b__9_0(System.Object,System.EventArgs)
extern void LogUtil_U3C_ctorU3Eb__9_0_m057EE72CCDA8877817C356F04A3FB0403BDC8268 (void);
// 0x0000001D System.Void Firebase.LogUtil/LogMessageDelegate::.ctor(System.Object,System.IntPtr)
extern void LogMessageDelegate__ctor_mB6AACCCEAE43E818C4B0DFCF6388FF4CC7200F10 (void);
// 0x0000001E System.Void Firebase.LogUtil/LogMessageDelegate::Invoke(Firebase.LogLevel,System.String)
extern void LogMessageDelegate_Invoke_m93848481738EC2A03FD8F5600C132464290BDAC8 (void);
// 0x0000001F System.Void Firebase.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern void MonoPInvokeCallbackAttribute__ctor_m4AE84268E5E69C1E4E1E8CD7AF145EF3C73DDA02 (void);
// 0x00000020 System.Void Firebase.FutureBase::.ctor(System.IntPtr,System.Boolean)
extern void FutureBase__ctor_m98C8AE4F030730C1CEE7E0B4A1816C623F2B9BE0 (void);
// 0x00000021 System.Void Firebase.FutureBase::Finalize()
extern void FutureBase_Finalize_m9CD99D25C0199A337732E16288ABCE051A4D5CB7 (void);
// 0x00000022 System.Void Firebase.FutureBase::Dispose()
extern void FutureBase_Dispose_m32193D02DE4608C6C3EDF42F3D0495707DA4D15E (void);
// 0x00000023 System.Void Firebase.FutureBase::Dispose(System.Boolean)
extern void FutureBase_Dispose_m17D716EFFAF752B7DBF402C73D757D02C34457EB (void);
// 0x00000024 Firebase.FutureStatus Firebase.FutureBase::status()
extern void FutureBase_status_mC75FD35438B176F95462D3A5D7D9194629211902 (void);
// 0x00000025 System.Int32 Firebase.FutureBase::error()
extern void FutureBase_error_m47E3B5E0A43B4C19510A77B3658EE5D7D10B6030 (void);
// 0x00000026 System.String Firebase.FutureBase::error_message()
extern void FutureBase_error_message_m5CC18319253B1ECC3C8AC675B213A08B1755D527 (void);
// 0x00000027 System.Void Firebase.StringStringMap::.ctor(System.IntPtr,System.Boolean)
extern void StringStringMap__ctor_m493F3867E24E87A4D890A56366DAE5D3E2172E35 (void);
// 0x00000028 System.Runtime.InteropServices.HandleRef Firebase.StringStringMap::getCPtr(Firebase.StringStringMap)
extern void StringStringMap_getCPtr_m04B3D3C2A8A0A7FE18E8E41C0C9B9776962545D1 (void);
// 0x00000029 System.Void Firebase.StringStringMap::Finalize()
extern void StringStringMap_Finalize_mE24B29EBA8476775366BE1E56D51757FF34412D6 (void);
// 0x0000002A System.Void Firebase.StringStringMap::Dispose()
extern void StringStringMap_Dispose_mFECCAB7DCE0572DDE5BAFE9999616BBAD5B42D12 (void);
// 0x0000002B System.Void Firebase.StringStringMap::Dispose(System.Boolean)
extern void StringStringMap_Dispose_m88AC30342C42C0575CC7029859A48F77BCCA4AC0 (void);
// 0x0000002C System.String Firebase.StringStringMap::get_Item(System.String)
extern void StringStringMap_get_Item_m01061069FC7C194E45C518987A14FA5918806BE1 (void);
// 0x0000002D System.Void Firebase.StringStringMap::set_Item(System.String,System.String)
extern void StringStringMap_set_Item_m975DA3FC714B74CB4E7D4CAAE0482D7B669D186F (void);
// 0x0000002E System.Boolean Firebase.StringStringMap::TryGetValue(System.String,System.String&)
extern void StringStringMap_TryGetValue_mEF4B761217F202E2F25001244A02516D4B85263D (void);
// 0x0000002F System.Int32 Firebase.StringStringMap::get_Count()
extern void StringStringMap_get_Count_m2B11AF48BF1530FCB3ED130712C6B5BADC76A848 (void);
// 0x00000030 System.Boolean Firebase.StringStringMap::get_IsReadOnly()
extern void StringStringMap_get_IsReadOnly_m679F53D527AD174BC0D08D0F86998D53FDA6F481 (void);
// 0x00000031 System.Collections.Generic.ICollection`1<System.String> Firebase.StringStringMap::get_Keys()
extern void StringStringMap_get_Keys_m558C6C1516539080580AB4D6F8B2905B4B604AC4 (void);
// 0x00000032 System.Collections.Generic.ICollection`1<System.String> Firebase.StringStringMap::get_Values()
extern void StringStringMap_get_Values_m55F926C89AA88AEC46FA2BE15C4812B205D35FC6 (void);
// 0x00000033 System.Void Firebase.StringStringMap::Add(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void StringStringMap_Add_mA5E43086723E81409FB93BD34211779B2B95B466 (void);
// 0x00000034 System.Boolean Firebase.StringStringMap::Remove(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void StringStringMap_Remove_m403C3C6E00AF3F626AFF1EF753E5A69AC4D4C06A (void);
// 0x00000035 System.Boolean Firebase.StringStringMap::Contains(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void StringStringMap_Contains_m5F1544CEFA19C9797BD02C53E7DD9EB2C9097916 (void);
// 0x00000036 System.Void Firebase.StringStringMap::CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,System.String>[],System.Int32)
extern void StringStringMap_CopyTo_m5D78F9003BE6B16285A5C22504731EA6D14B6454 (void);
// 0x00000037 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Firebase.StringStringMap::global::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<System.String,System.String>>.GetEnumerator()
extern void StringStringMap_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_GetEnumerator_m4B458CA8BEE64D77627CBEF5804F460379D3CCEC (void);
// 0x00000038 System.Collections.IEnumerator Firebase.StringStringMap::global::System.Collections.IEnumerable.GetEnumerator()
extern void StringStringMap_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_m9A35129110F7B5928202096310FDD2CCEBFC9CAF (void);
// 0x00000039 Firebase.StringStringMap/StringStringMapEnumerator Firebase.StringStringMap::GetEnumerator()
extern void StringStringMap_GetEnumerator_m5629FBE397B23B93415341566E71A149DBF1362A (void);
// 0x0000003A System.Void Firebase.StringStringMap::.ctor()
extern void StringStringMap__ctor_m5295C0F5394545250F06A5F0A53C9ABB48D67794 (void);
// 0x0000003B System.UInt32 Firebase.StringStringMap::size()
extern void StringStringMap_size_m9C20BFD104A8B49600AD587CC29C4A748F408DC4 (void);
// 0x0000003C System.Void Firebase.StringStringMap::Clear()
extern void StringStringMap_Clear_mC33CC5FAC952437E4A1844D77F07682AB4A440A8 (void);
// 0x0000003D System.String Firebase.StringStringMap::getitem(System.String)
extern void StringStringMap_getitem_m8DADD76F6CD52B4B98611DD4292910963C079C1B (void);
// 0x0000003E System.Void Firebase.StringStringMap::setitem(System.String,System.String)
extern void StringStringMap_setitem_mC5A0170C20E03D926C187A4B7AC6B0B96FB18C00 (void);
// 0x0000003F System.Boolean Firebase.StringStringMap::ContainsKey(System.String)
extern void StringStringMap_ContainsKey_mD13F26BD8A08E581ADB303D38074819105C605A5 (void);
// 0x00000040 System.Void Firebase.StringStringMap::Add(System.String,System.String)
extern void StringStringMap_Add_m01048CFD777D82B2F693B6D71A4D452FAF7AEAC1 (void);
// 0x00000041 System.Boolean Firebase.StringStringMap::Remove(System.String)
extern void StringStringMap_Remove_m986E61004827D7F62831AEE307E6020383C3CB4D (void);
// 0x00000042 System.IntPtr Firebase.StringStringMap::create_iterator_begin()
extern void StringStringMap_create_iterator_begin_mF1C81519248E3646D376EC9F288C1C6A2065630D (void);
// 0x00000043 System.String Firebase.StringStringMap::get_next_key(System.IntPtr)
extern void StringStringMap_get_next_key_m92C94637FF8B9A00B08BFCAD08CD41705AEAEE9B (void);
// 0x00000044 System.Void Firebase.StringStringMap::destroy_iterator(System.IntPtr)
extern void StringStringMap_destroy_iterator_m75AD241AD5316B8B09070BF04C571918FE34BA04 (void);
// 0x00000045 System.Void Firebase.StringStringMap/StringStringMapEnumerator::.ctor(Firebase.StringStringMap)
extern void StringStringMapEnumerator__ctor_m1659D491782A6E753AC1792C39802A79860F75B3 (void);
// 0x00000046 System.Collections.Generic.KeyValuePair`2<System.String,System.String> Firebase.StringStringMap/StringStringMapEnumerator::get_Current()
extern void StringStringMapEnumerator_get_Current_mE221D98D4E0B07220825ED8752B9714AADBCB04E (void);
// 0x00000047 System.Object Firebase.StringStringMap/StringStringMapEnumerator::global::System.Collections.IEnumerator.get_Current()
extern void StringStringMapEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_mF90B634B9E830DB2302FBCBC3F6DC9625AB41FBF (void);
// 0x00000048 System.Boolean Firebase.StringStringMap/StringStringMapEnumerator::MoveNext()
extern void StringStringMapEnumerator_MoveNext_m19D42D8E29467683964512499AABEF93BD9830E1 (void);
// 0x00000049 System.Void Firebase.StringStringMap/StringStringMapEnumerator::Reset()
extern void StringStringMapEnumerator_Reset_mFE28DCE2D21F6639E75B255AAF56D34BFA7A151A (void);
// 0x0000004A System.Void Firebase.StringStringMap/StringStringMapEnumerator::Dispose()
extern void StringStringMapEnumerator_Dispose_mDB957DDBEF7D7D2207BEF3C8EF98D4AF2357B9E8 (void);
// 0x0000004B System.Void Firebase.AppOptionsInternal::.ctor(System.IntPtr,System.Boolean)
extern void AppOptionsInternal__ctor_m1A801B68806227BB2756E18B7BFC46995E6768DE (void);
// 0x0000004C System.Void Firebase.AppOptionsInternal::Finalize()
extern void AppOptionsInternal_Finalize_mC4193C521BD48563554BDD3EA7DE8B6BBD2922CE (void);
// 0x0000004D System.Void Firebase.AppOptionsInternal::Dispose()
extern void AppOptionsInternal_Dispose_m69ACB4A20CD2B29F2D09D96FB7DBC1F4E4599B0A (void);
// 0x0000004E System.Void Firebase.AppOptionsInternal::Dispose(System.Boolean)
extern void AppOptionsInternal_Dispose_m7E5CDD057C84B8A6B796F044B70BDC713DAA9491 (void);
// 0x0000004F System.Uri Firebase.AppOptionsInternal::get_DatabaseUrl()
extern void AppOptionsInternal_get_DatabaseUrl_mD24AE9A589D060986270F6F7E582FC7244A99580 (void);
// 0x00000050 System.String Firebase.AppOptionsInternal::GetDatabaseUrlInternal()
extern void AppOptionsInternal_GetDatabaseUrlInternal_mBB900411ECF9D1C6759CC2BE5C59A565E0E4061E (void);
// 0x00000051 System.String Firebase.AppOptionsInternal::get_AppId()
extern void AppOptionsInternal_get_AppId_m9E10A96FC162A8F195B33B40170D42A426D3A766 (void);
// 0x00000052 System.String Firebase.AppOptionsInternal::get_ApiKey()
extern void AppOptionsInternal_get_ApiKey_m4FEA968CCE295C61306024A53B87EB462C793FDD (void);
// 0x00000053 System.String Firebase.AppOptionsInternal::get_MessageSenderId()
extern void AppOptionsInternal_get_MessageSenderId_m685FAF74B5E0F003C765471030BFEC9049B188A5 (void);
// 0x00000054 System.String Firebase.AppOptionsInternal::get_StorageBucket()
extern void AppOptionsInternal_get_StorageBucket_m990618B90CAFF7EC9D688C494C058156DC353535 (void);
// 0x00000055 System.String Firebase.AppOptionsInternal::get_ProjectId()
extern void AppOptionsInternal_get_ProjectId_m3CB8C9F459B7ABE7266E303E627FEC079DE825ED (void);
// 0x00000056 System.String Firebase.AppOptionsInternal::get_PackageName()
extern void AppOptionsInternal_get_PackageName_m44A62026BBC91627D666692A8116007B7E6FAB40 (void);
// 0x00000057 System.Void Firebase.FirebaseApp::.ctor(System.IntPtr,System.Boolean)
extern void FirebaseApp__ctor_mC539AF748C2E16CD3B7820D6039B9A29DBDF908C (void);
// 0x00000058 System.Runtime.InteropServices.HandleRef Firebase.FirebaseApp::getCPtr(Firebase.FirebaseApp)
extern void FirebaseApp_getCPtr_mCF6551417C0F1D98798ED7810553EBD977381D16 (void);
// 0x00000059 System.Void Firebase.FirebaseApp::Finalize()
extern void FirebaseApp_Finalize_mF8DA91BE30AF031A390E068301053AEF3D6B5A98 (void);
// 0x0000005A System.Void Firebase.FirebaseApp::Dispose()
extern void FirebaseApp_Dispose_mC1965A7AE8BAB834DB652BF0BACF377F3D45192B (void);
// 0x0000005B System.Void Firebase.FirebaseApp::Dispose(System.Boolean)
extern void FirebaseApp_Dispose_m7AA869727509B99D04399B9BA7F1FEEC0251974A (void);
// 0x0000005C System.Void Firebase.FirebaseApp::.cctor()
extern void FirebaseApp__cctor_m4D3A163F336E9C9A0FDB71D5484328266A6D48C5 (void);
// 0x0000005D System.Void Firebase.FirebaseApp::TranslateDllNotFoundException(System.Action)
extern void FirebaseApp_TranslateDllNotFoundException_m387A100ACF3B581E16E3248735AC6099157E0B21 (void);
// 0x0000005E Firebase.FirebaseApp Firebase.FirebaseApp::get_DefaultInstance()
extern void FirebaseApp_get_DefaultInstance_mCA6FC0DE0B25880FC6ACEAD5585ED84407690C61 (void);
// 0x0000005F Firebase.FirebaseApp Firebase.FirebaseApp::GetInstance(System.String)
extern void FirebaseApp_GetInstance_m2731CBD05B439F8E447DAA971E7E0627E0CB95D1 (void);
// 0x00000060 Firebase.FirebaseApp Firebase.FirebaseApp::Create()
extern void FirebaseApp_Create_m2BD29990D5510D08A1503739EA8D3F82186ABA69 (void);
// 0x00000061 System.String Firebase.FirebaseApp::get_Name()
extern void FirebaseApp_get_Name_m89C11F96726C8E4FD3CCAE04A5DC3129F7CD975E (void);
// 0x00000062 Firebase.LogLevel Firebase.FirebaseApp::get_LogLevel()
extern void FirebaseApp_get_LogLevel_m50774F8027C8BF6EB481CBFCCCFC266863392D83 (void);
// 0x00000063 System.Void Firebase.FirebaseApp::add_AppDisposed(System.EventHandler)
extern void FirebaseApp_add_AppDisposed_m849DD816EFE8D669DBFA139254D5E3C4D8C78F85 (void);
// 0x00000064 System.Void Firebase.FirebaseApp::remove_AppDisposed(System.EventHandler)
extern void FirebaseApp_remove_AppDisposed_mAAF77EA50314A467CBB4481448C72FA9B7173289 (void);
// 0x00000065 System.Void Firebase.FirebaseApp::AddReference()
extern void FirebaseApp_AddReference_m562BA6DFE00568AC30B15C36D8BB848F14EDED95 (void);
// 0x00000066 System.Void Firebase.FirebaseApp::RemoveReference()
extern void FirebaseApp_RemoveReference_m3C28724EDB5D9F20A2A4924E517A8FF79C7E3425 (void);
// 0x00000067 System.Void Firebase.FirebaseApp::ThrowIfNull()
extern void FirebaseApp_ThrowIfNull_mEBB4A7F4A0E30B8F6969C68C340AF30D44491B20 (void);
// 0x00000068 System.Void Firebase.FirebaseApp::InitializeAppUtilCallbacks()
extern void FirebaseApp_InitializeAppUtilCallbacks_mB4CA136547ECB2365434896EA8C1AB50904FEBD5 (void);
// 0x00000069 System.Void Firebase.FirebaseApp::OnAllAppsDestroyed()
extern void FirebaseApp_OnAllAppsDestroyed_m685A494EFE8AB255A373CE0C8ABDFD39D81A7463 (void);
// 0x0000006A System.Uri Firebase.FirebaseApp::UrlStringToUri(System.String)
extern void FirebaseApp_UrlStringToUri_m1CCEC1253A9E24DAF5BAACA87A2741C6163090D6 (void);
// 0x0000006B System.Object Firebase.FirebaseApp::WeakReferenceGetTarget(System.WeakReference)
extern void FirebaseApp_WeakReferenceGetTarget_m48BCAA7BDF44B90AFC6B68BE061F879F21BDEA5D (void);
// 0x0000006C System.Boolean Firebase.FirebaseApp::InitializeCrashlyticsIfPresent()
extern void FirebaseApp_InitializeCrashlyticsIfPresent_m03191690A38691879D03C29026593CD2684BE675 (void);
// 0x0000006D Firebase.FirebaseApp Firebase.FirebaseApp::CreateAndTrack(Firebase.FirebaseApp/CreateDelegate,Firebase.FirebaseApp)
extern void FirebaseApp_CreateAndTrack_m2A77847F5AFE916AA5E4AAA3FBD425DEF5406817 (void);
// 0x0000006E System.Void Firebase.FirebaseApp::ThrowIfCheckDependenciesRunning()
extern void FirebaseApp_ThrowIfCheckDependenciesRunning_m7E71E39E28A8732D2F4D47DB3B9B4DC7B0CF1406 (void);
// 0x0000006F System.Boolean Firebase.FirebaseApp::IsCheckDependenciesRunning()
extern void FirebaseApp_IsCheckDependenciesRunning_mB4AE050866713DDA5DABB5C81060B3F780D8CE84 (void);
// 0x00000070 Firebase.AppOptions Firebase.FirebaseApp::get_Options()
extern void FirebaseApp_get_Options_mDD04F28C88747DC1332D1A9363E7B6F6FCC7A12E (void);
// 0x00000071 Firebase.AppOptionsInternal Firebase.FirebaseApp::options()
extern void FirebaseApp_options_m18F2F0A7F1E29CDB4AD75C0746A33922B1BF7C06 (void);
// 0x00000072 System.String Firebase.FirebaseApp::get_NameInternal()
extern void FirebaseApp_get_NameInternal_m493D9AEC87709D1197A1997C7560AFEBB107FBCE (void);
// 0x00000073 Firebase.FirebaseApp Firebase.FirebaseApp::CreateInternal()
extern void FirebaseApp_CreateInternal_m0E179C78560E4F6E71A0FCEE92CA76788DA9F233 (void);
// 0x00000074 System.Void Firebase.FirebaseApp::ReleaseReferenceInternal(Firebase.FirebaseApp)
extern void FirebaseApp_ReleaseReferenceInternal_mF26428D844B9B729A421CE9C89BC3AF16F67A932 (void);
// 0x00000075 System.Void Firebase.FirebaseApp::RegisterLibrariesInternal(Firebase.StringStringMap)
extern void FirebaseApp_RegisterLibrariesInternal_mE4433124F793D190C3AB1D6D790D50A282EB3CBE (void);
// 0x00000076 System.Void Firebase.FirebaseApp::LogHeartbeatInternal(Firebase.FirebaseApp)
extern void FirebaseApp_LogHeartbeatInternal_mD0C5A3E3A7B754D36B221A326C92A605503DD1FE (void);
// 0x00000077 System.Void Firebase.FirebaseApp::AppSetDefaultConfigPath(System.String)
extern void FirebaseApp_AppSetDefaultConfigPath_m2EC0842B39AB8C4D1360CF089518FD2BDC8AE63B (void);
// 0x00000078 System.String Firebase.FirebaseApp::get_DefaultName()
extern void FirebaseApp_get_DefaultName_m4FDA39399A8C50D6A383DEC274FF208BA6BFE40D (void);
// 0x00000079 System.String Firebase.FirebaseApp/EnableModuleParams::get_CppModuleName()
extern void EnableModuleParams_get_CppModuleName_mB91981F21F3F94D82CD64DD7BD810741CBB04E3A (void);
// 0x0000007A System.Void Firebase.FirebaseApp/EnableModuleParams::set_CppModuleName(System.String)
extern void EnableModuleParams_set_CppModuleName_mF1C3FE3BBE44DEDB23AF2879630075AEAC7106DF (void);
// 0x0000007B System.String Firebase.FirebaseApp/EnableModuleParams::get_CSharpClassName()
extern void EnableModuleParams_get_CSharpClassName_m04AD392AA82FCE1E6636F812672C77F294AC16EC (void);
// 0x0000007C System.Void Firebase.FirebaseApp/EnableModuleParams::set_CSharpClassName(System.String)
extern void EnableModuleParams_set_CSharpClassName_m9152635BDD8F608352C12F3447962C10F7DF4F43 (void);
// 0x0000007D System.Boolean Firebase.FirebaseApp/EnableModuleParams::get_AlwaysEnable()
extern void EnableModuleParams_get_AlwaysEnable_mC44F8EA7A9EDCD493C6B8E04E3B3CF00D09FDEA6 (void);
// 0x0000007E System.Void Firebase.FirebaseApp/EnableModuleParams::set_AlwaysEnable(System.Boolean)
extern void EnableModuleParams_set_AlwaysEnable_m3F7638041BDA0CC3669AD7119C68ABD2B6F7C482 (void);
// 0x0000007F System.Void Firebase.FirebaseApp/EnableModuleParams::.ctor(System.String,System.String,System.Boolean)
extern void EnableModuleParams__ctor_m448B394AF46BBC2CE9C3301F732850625F6B37EF (void);
// 0x00000080 System.Void Firebase.FirebaseApp/CreateDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateDelegate__ctor_m966C39812E422F82DD3AACF101F012749B1F9E12 (void);
// 0x00000081 Firebase.FirebaseApp Firebase.FirebaseApp/CreateDelegate::Invoke()
extern void CreateDelegate_Invoke_m3C05F10053C0FD938376079571835049ADDD6186 (void);
// 0x00000082 System.Void Firebase.FirebaseApp/<>c::.cctor()
extern void U3CU3Ec__cctor_mA283C3B885447FABD62442EF65AD1CD31780A9A4 (void);
// 0x00000083 System.Void Firebase.FirebaseApp/<>c::.ctor()
extern void U3CU3Ec__ctor_m463FBDE085153371662615419AFD8228F4704F75 (void);
// 0x00000084 Firebase.FirebaseApp Firebase.FirebaseApp/<>c::<Create>b__15_0()
extern void U3CU3Ec_U3CCreateU3Eb__15_0_mF43BDAE5875C0C407791D7735DC43BB00EB29F32 (void);
// 0x00000085 System.Boolean Firebase.FirebaseApp/<>c::<CreateAndTrack>b__48_0()
extern void U3CU3Ec_U3CCreateAndTrackU3Eb__48_0_m353C5F1E7C6BDE8601757A37801E17C89CA49AC2 (void);
// 0x00000086 System.Void Firebase.AppUtilPINVOKE::.cctor()
extern void AppUtilPINVOKE__cctor_m5F99B3E75CC446C45B94B31C3FC38940090CBA0C (void);
// 0x00000087 System.Void Firebase.AppUtilPINVOKE::delete_FutureBase(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_delete_FutureBase_m35315C54A3443849BE8190F0BF2AA098038DF0BA (void);
// 0x00000088 System.Int32 Firebase.AppUtilPINVOKE::FutureBase_status(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FutureBase_status_mB318847BE2E3610D5D82665BF0C61611D550C111 (void);
// 0x00000089 System.Int32 Firebase.AppUtilPINVOKE::FutureBase_error(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FutureBase_error_m69E38EE9B25E7418CD95085AAD616EE166B43C73 (void);
// 0x0000008A System.String Firebase.AppUtilPINVOKE::FutureBase_error_message(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FutureBase_error_message_mEF99F3C887A59D7759C68A185B2DC6E721DB48C1 (void);
// 0x0000008B System.IntPtr Firebase.AppUtilPINVOKE::new_StringStringMap__SWIG_0()
extern void AppUtilPINVOKE_new_StringStringMap__SWIG_0_mDDF7009C0CA4756CB189FC4D7221E8F4806D15F8 (void);
// 0x0000008C System.UInt32 Firebase.AppUtilPINVOKE::StringStringMap_size(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_StringStringMap_size_m8A699D5B49047D52BA2BE816EFCE3086F0FCACF1 (void);
// 0x0000008D System.Void Firebase.AppUtilPINVOKE::StringStringMap_Clear(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_StringStringMap_Clear_mD6F8702554987F106A25C80AB8EB99EC65E04804 (void);
// 0x0000008E System.String Firebase.AppUtilPINVOKE::StringStringMap_getitem(System.Runtime.InteropServices.HandleRef,System.String)
extern void AppUtilPINVOKE_StringStringMap_getitem_m496E09DCEAA62B45CC6A58190C168B273B4144FA (void);
// 0x0000008F System.Void Firebase.AppUtilPINVOKE::StringStringMap_setitem(System.Runtime.InteropServices.HandleRef,System.String,System.String)
extern void AppUtilPINVOKE_StringStringMap_setitem_m7CB5926A7AF5B874DD70455F8C8C5A2FEEDEDFCF (void);
// 0x00000090 System.Boolean Firebase.AppUtilPINVOKE::StringStringMap_ContainsKey(System.Runtime.InteropServices.HandleRef,System.String)
extern void AppUtilPINVOKE_StringStringMap_ContainsKey_m42410B4C36F897A6B63E203ACFA5F99CFE0322C4 (void);
// 0x00000091 System.Void Firebase.AppUtilPINVOKE::StringStringMap_Add(System.Runtime.InteropServices.HandleRef,System.String,System.String)
extern void AppUtilPINVOKE_StringStringMap_Add_m390C9E5584D49686F57F4711DB82FCC36B2DEE2F (void);
// 0x00000092 System.Boolean Firebase.AppUtilPINVOKE::StringStringMap_Remove(System.Runtime.InteropServices.HandleRef,System.String)
extern void AppUtilPINVOKE_StringStringMap_Remove_m8BADCE6A740F08DA5164AAB5F72E66A6212CE1D0 (void);
// 0x00000093 System.IntPtr Firebase.AppUtilPINVOKE::StringStringMap_create_iterator_begin(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_StringStringMap_create_iterator_begin_mF2EB9D203087421D4AA782470B8D39F1A296EC57 (void);
// 0x00000094 System.String Firebase.AppUtilPINVOKE::StringStringMap_get_next_key(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void AppUtilPINVOKE_StringStringMap_get_next_key_mB3EE19C1F3D873D85B3812F8434844E9E247BA1A (void);
// 0x00000095 System.Void Firebase.AppUtilPINVOKE::StringStringMap_destroy_iterator(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void AppUtilPINVOKE_StringStringMap_destroy_iterator_mF7843E1738F466CE3636B6D889578EA461B6EB06 (void);
// 0x00000096 System.Void Firebase.AppUtilPINVOKE::delete_StringStringMap(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_delete_StringStringMap_m2DC911DECB71152B2C0773292BD24161F34420D3 (void);
// 0x00000097 System.String Firebase.AppUtilPINVOKE::AppOptionsInternal_GetDatabaseUrlInternal(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_AppOptionsInternal_GetDatabaseUrlInternal_mBFBA3895596D4C988482F52F1138400F540ABA89 (void);
// 0x00000098 System.String Firebase.AppUtilPINVOKE::AppOptionsInternal_AppId_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_AppOptionsInternal_AppId_get_mA507D544796377B2A97720CEF8CF9506655833D2 (void);
// 0x00000099 System.String Firebase.AppUtilPINVOKE::AppOptionsInternal_ApiKey_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_AppOptionsInternal_ApiKey_get_m50B0EFC93B31470BB5AE9774185045B8DADD8227 (void);
// 0x0000009A System.String Firebase.AppUtilPINVOKE::AppOptionsInternal_MessageSenderId_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_AppOptionsInternal_MessageSenderId_get_mA6A0233FE92A3656E1EE44BDFABCC10C1344B5D7 (void);
// 0x0000009B System.String Firebase.AppUtilPINVOKE::AppOptionsInternal_StorageBucket_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_AppOptionsInternal_StorageBucket_get_mFE5B6DC41436813F2D270C558EDE2231096021EF (void);
// 0x0000009C System.String Firebase.AppUtilPINVOKE::AppOptionsInternal_ProjectId_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_AppOptionsInternal_ProjectId_get_m5978BCDCDEF6899BEC86AF5C25085194C786978B (void);
// 0x0000009D System.String Firebase.AppUtilPINVOKE::AppOptionsInternal_PackageName_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_AppOptionsInternal_PackageName_get_m0A280356D2F7014C72F7E9A1A861B825A2B12F05 (void);
// 0x0000009E System.Void Firebase.AppUtilPINVOKE::delete_AppOptionsInternal(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_delete_AppOptionsInternal_m8493A4404C826F6CFB817346A212D8D73A1BD772 (void);
// 0x0000009F System.IntPtr Firebase.AppUtilPINVOKE::FirebaseApp_options(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FirebaseApp_options_mE445A677BA68FE0BF261BE9B4B1E059093BEE091 (void);
// 0x000000A0 System.String Firebase.AppUtilPINVOKE::FirebaseApp_NameInternal_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FirebaseApp_NameInternal_get_m9A54BA928A200855D9247957928CE38DB57F9203 (void);
// 0x000000A1 System.IntPtr Firebase.AppUtilPINVOKE::FirebaseApp_CreateInternal__SWIG_0()
extern void AppUtilPINVOKE_FirebaseApp_CreateInternal__SWIG_0_mB30D1FDF6B0ACB2860E853CBD85E627FBC6A8DD1 (void);
// 0x000000A2 System.Void Firebase.AppUtilPINVOKE::FirebaseApp_ReleaseReferenceInternal(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FirebaseApp_ReleaseReferenceInternal_m5C0ABB7130466CD7284512BDCDC0348E4990A7B4 (void);
// 0x000000A3 System.Int32 Firebase.AppUtilPINVOKE::FirebaseApp_GetLogLevelInternal()
extern void AppUtilPINVOKE_FirebaseApp_GetLogLevelInternal_mFFAA837D26D43B99A7FE761CAE23F31D8C86EFA5 (void);
// 0x000000A4 System.Void Firebase.AppUtilPINVOKE::FirebaseApp_RegisterLibrariesInternal(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FirebaseApp_RegisterLibrariesInternal_m27619F8F7884952E3B023B5B51A0B2E0B4AABCA1 (void);
// 0x000000A5 System.Void Firebase.AppUtilPINVOKE::FirebaseApp_LogHeartbeatInternal(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_FirebaseApp_LogHeartbeatInternal_m7F63F60F846CDB4F3AF3F8FD9F794C9B59BAC75F (void);
// 0x000000A6 System.Void Firebase.AppUtilPINVOKE::FirebaseApp_AppSetDefaultConfigPath(System.String)
extern void AppUtilPINVOKE_FirebaseApp_AppSetDefaultConfigPath_m6DB74FB4F54B60CBC220635058868945B35B5982 (void);
// 0x000000A7 System.String Firebase.AppUtilPINVOKE::FirebaseApp_DefaultName_get()
extern void AppUtilPINVOKE_FirebaseApp_DefaultName_get_m8B412F84603DA4E77B29DD71DB6132A6DF8F9021 (void);
// 0x000000A8 System.Void Firebase.AppUtilPINVOKE::PollCallbacks()
extern void AppUtilPINVOKE_PollCallbacks_mA6778D990E2A10564AC0D44F090CD3292D07EBB4 (void);
// 0x000000A9 System.Void Firebase.AppUtilPINVOKE::AppEnableLogCallback(System.Boolean)
extern void AppUtilPINVOKE_AppEnableLogCallback_m989A6148B22C9D62E5DB7F1AD5468CC1DABA9DBB (void);
// 0x000000AA System.Void Firebase.AppUtilPINVOKE::SetEnabledAllAppCallbacks(System.Boolean)
extern void AppUtilPINVOKE_SetEnabledAllAppCallbacks_m2C58D4C43A8974C0D122ABD92136BD229B6C3CE8 (void);
// 0x000000AB System.Void Firebase.AppUtilPINVOKE::SetEnabledAppCallbackByName(System.String,System.Boolean)
extern void AppUtilPINVOKE_SetEnabledAppCallbackByName_m04CC33BEBE5D735EF4C6F60A2BEB281FFD386812 (void);
// 0x000000AC System.Boolean Firebase.AppUtilPINVOKE::GetEnabledAppCallbackByName(System.String)
extern void AppUtilPINVOKE_GetEnabledAppCallbackByName_mBEA56AA2FC4E7B0F7AE2FC32B23009584DAF249A (void);
// 0x000000AD System.Void Firebase.AppUtilPINVOKE::SetLogFunction(Firebase.LogUtil/LogMessageDelegate)
extern void AppUtilPINVOKE_SetLogFunction_m4800E17E1145F9FCEF0FB33B411E5A83DE5D59F1 (void);
// 0x000000AE System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AppUtil_mCEE5500031BD8FB6065EC0B7D8372A6E62017F96 (void);
// 0x000000AF System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AppUtil_m5794B6B4FD801CDE8D79E9C55D91B99DA476A38E (void);
// 0x000000B0 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern void SWIGExceptionHelper_SetPendingApplicationException_m6C69E5AD1E4D604354F6077B67D5772F795426E6 (void);
// 0x000000B1 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern void SWIGExceptionHelper_SetPendingArithmeticException_m4CDF27D022538556DA8E54F2DBB6957D0419523B (void);
// 0x000000B2 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern void SWIGExceptionHelper_SetPendingDivideByZeroException_mAF2154299A69434F0D8175854649FAEC212D6E88 (void);
// 0x000000B3 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_mD1828B56CAF9C5635AA0F7E49B6856DE01F73DE3 (void);
// 0x000000B4 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidCastException_mB702FABB41074EFCA6FFBFD8B24669CA879077DB (void);
// 0x000000B5 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidOperationException_m32C61E47D26CB43C215DC086CF5D7A4B9E853716 (void);
// 0x000000B6 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
extern void SWIGExceptionHelper_SetPendingIOException_m932608D47F516BBA327D41B529BC101EC07E6176 (void);
// 0x000000B7 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern void SWIGExceptionHelper_SetPendingNullReferenceException_m4BE790ED7CD62E6B2281C9B040F815697FD4254D (void);
// 0x000000B8 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern void SWIGExceptionHelper_SetPendingOutOfMemoryException_mDB9A2DA0C3243B3E047219380EF92B33F5DA575C (void);
// 0x000000B9 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern void SWIGExceptionHelper_SetPendingOverflowException_mCA37E08F462E1BF14D59A78731D61016F5D92EA5 (void);
// 0x000000BA System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
extern void SWIGExceptionHelper_SetPendingSystemException_mE70086ECFA51014FFF3D0557844E07DEF2FA8F24 (void);
// 0x000000BB System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentException_m7AB4E7B60FE6666951EB975CB0D7095D5FF2DDC4 (void);
// 0x000000BC System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentNullException_m4B3F36D2AA2C7D504EB1822AB727B69D90C077F2 (void);
// 0x000000BD System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m8C7D11A5CE60E31555D0C41E55DF4AE610052309 (void);
// 0x000000BE System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::.cctor()
extern void SWIGExceptionHelper__cctor_mC59330BC82D37F168B01A2421230EE6920BBBEBB (void);
// 0x000000BF System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::.ctor()
extern void SWIGExceptionHelper__ctor_m4DB6794D8CB5F1A9740C37B0C257B69982C013B9 (void);
// 0x000000C0 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionDelegate__ctor_m49AB94CEC8E6544CE0D7B1E2300735728EE336D8 (void);
// 0x000000C1 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
extern void ExceptionDelegate_Invoke_mE04E9A1D96F5AE159E3D7878E87706B91A149B25 (void);
// 0x000000C2 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionArgumentDelegate__ctor_m45E4EFAE5F14FFEC5843A00ABEF4D0E1F0854629 (void);
// 0x000000C3 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern void ExceptionArgumentDelegate_Invoke_m46BEB8B17B239BFB498C54B89EB06352BD1948F8 (void);
// 0x000000C4 System.Boolean Firebase.AppUtilPINVOKE/SWIGPendingException::get_Pending()
extern void SWIGPendingException_get_Pending_m0BEE83B2B2559DE231EE2A87DF09649355775538 (void);
// 0x000000C5 System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::Set(System.Exception)
extern void SWIGPendingException_Set_mCDF526D868E4A3C95BB195595F5259755F0B2985 (void);
// 0x000000C6 System.Exception Firebase.AppUtilPINVOKE/SWIGPendingException::Retrieve()
extern void SWIGPendingException_Retrieve_m5C29154CE762C145ECCBA0E7A37F5579366647BF (void);
// 0x000000C7 System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::.cctor()
extern void SWIGPendingException__cctor_m0BC7954EFBB1D89B9D9D98858F5FCD7F62259EC1 (void);
// 0x000000C8 System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_AppUtil(Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern void SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_m6DE958A7010EB13AEC2BBFF99775C3D00493CB2B (void);
// 0x000000C9 System.String Firebase.AppUtilPINVOKE/SWIGStringHelper::CreateString(System.String)
extern void SWIGStringHelper_CreateString_m4EDC140847BEAA68EFFBFC908E592173558433B9 (void);
// 0x000000CA System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.cctor()
extern void SWIGStringHelper__cctor_mBC542C2F3D6E2B1C11343D9BDF77681891886413 (void);
// 0x000000CB System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.ctor()
extern void SWIGStringHelper__ctor_mF5EBABDC102D937A919B6A6CCA3690E2244ECE85 (void);
// 0x000000CC System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIGStringDelegate__ctor_m4D5B167B33345B58192AD3B50D1F8901A18F4F4D (void);
// 0x000000CD System.String Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
extern void SWIGStringDelegate_Invoke_m9831BD87E0EBFECFF48B4CA3FFEB118280C2ABCB (void);
// 0x000000CE System.Void Firebase.AppUtil::PollCallbacks()
extern void AppUtil_PollCallbacks_m1828733E0D5AE0AD1AF100649BE21C1580C5F274 (void);
// 0x000000CF System.Void Firebase.AppUtil::AppEnableLogCallback(System.Boolean)
extern void AppUtil_AppEnableLogCallback_mB1D2F4E350E0261617E8E903854CEF3919E8AC50 (void);
// 0x000000D0 System.Void Firebase.AppUtil::SetEnabledAllAppCallbacks(System.Boolean)
extern void AppUtil_SetEnabledAllAppCallbacks_m96646FFBAC8641D920F5A0B2DB16F25CDA74D558 (void);
// 0x000000D1 System.Void Firebase.AppUtil::SetEnabledAppCallbackByName(System.String,System.Boolean)
extern void AppUtil_SetEnabledAppCallbackByName_mBC81C70A57C6C9BEB4E239034DB74BF4A002A47E (void);
// 0x000000D2 System.Boolean Firebase.AppUtil::GetEnabledAppCallbackByName(System.String)
extern void AppUtil_GetEnabledAppCallbackByName_mAD26376340D75FE4FF7AD100E085D67513A9C152 (void);
// 0x000000D3 System.Void Firebase.AppUtil::SetLogFunction(Firebase.LogUtil/LogMessageDelegate)
extern void AppUtil_SetLogFunction_m5C269A1922672E6E2408D3C63AB0D40888CB1997 (void);
// 0x000000D4 System.String Firebase.VersionInfo::get_SdkVersion()
extern void VersionInfo_get_SdkVersion_mF27457E8D6E11FB4DE0C247AFBFAC7860A51653A (void);
// 0x000000D5 System.String Firebase.VersionInfo::get_BuildSource()
extern void VersionInfo_get_BuildSource_m70093EC2695C02267AC85D7D653526CBF1D634E7 (void);
// 0x000000D6 Firebase.Platform.FirebaseAppUtils Firebase.Platform.FirebaseAppUtils::get_Instance()
extern void FirebaseAppUtils_get_Instance_mE6801A039F44928B0917260E861F377364D031EC (void);
// 0x000000D7 System.Void Firebase.Platform.FirebaseAppUtils::TranslateDllNotFoundException(System.Action)
extern void FirebaseAppUtils_TranslateDllNotFoundException_m8D9620D2F9B093C4DBF14AD9803923F0763955B8 (void);
// 0x000000D8 System.Void Firebase.Platform.FirebaseAppUtils::PollCallbacks()
extern void FirebaseAppUtils_PollCallbacks_m94AC1FCAA3602F030E6AA26C1FD6CB03E0F7155C (void);
// 0x000000D9 Firebase.Platform.PlatformLogLevel Firebase.Platform.FirebaseAppUtils::GetLogLevel()
extern void FirebaseAppUtils_GetLogLevel_m420F7E6140E65C5494538339E1322E33F3661105 (void);
// 0x000000DA System.Void Firebase.Platform.FirebaseAppUtils::.ctor()
extern void FirebaseAppUtils__ctor_m77E9C2ADF611B1553A685AC953C5508DFD636CD4 (void);
// 0x000000DB System.Void Firebase.Platform.FirebaseAppUtils::.cctor()
extern void FirebaseAppUtils__cctor_m295E2C99A656E8A9BAD5578B53705DD1D2D601E5 (void);
static Il2CppMethodPointer s_methodPointers[219] = 
{
	AppOptions_Dispose_m87505E3792614A116365111E658398473782B604,
	AppOptions_set_DatabaseUrl_m50A56D4AFD426801574A5008562EA3683E8516C9,
	AppOptions_set_AppId_m7654E20AC351E037813CCC3D88FED7BEE8DA0700,
	AppOptions_set_ApiKey_m3010B35DBC7124293B7AC9091BA86114ED1BBF30,
	AppOptions_set_MessageSenderId_mA0ABA93E9402035C509A97B3B2FFFB00B0ABFAAB,
	AppOptions_get_StorageBucket_m93625BCEE94287655E69A3ED0D145E64F66278A4,
	AppOptions_set_StorageBucket_m797A7F991A2674B2012887527B149846109B8E4B,
	AppOptions_set_ProjectId_m906F7A47E973C68F1BDC0697BE341394FFE32471,
	AppOptions_set_PackageName_mF0E66EF290BDA09FBE5B5108B60FBE8E24CC225A,
	AppOptions__ctor_m24702254E13B765824AB5738D6C248AE0EADAE28,
	FirebaseException__ctor_m18D67DA955D2B4EA2BC58BCE0E96AC0A177DD70F,
	FirebaseException_set_ErrorCode_m65B2880424E85063D56405A009DAA13E3B106465,
	InitializationException_set_InitResult_m94032AD57F63718F6F20625FDB98958766C9D764,
	InitializationException__ctor_mC48C74EE90B137CDEA82068C2E1695D81974C5BF,
	InitializationException__ctor_m1384021A3E1B7B0E372257380559D926BD6200BF,
	ErrorMessages_get_DependencyNotFoundErrorMessage_mDFD86A03DEEC7B060CC83CEA7A0F764BCF0D294D,
	ErrorMessages_get_DllNotFoundExceptionErrorMessage_m57791B6CE3DD0E02D5BF2423D450999E64CEA02A,
	ErrorMessages__cctor_m01785142FD3B6466ED8C3D9106F8286A31357483,
	LogUtil__cctor_m1C7054AC325AA0382EE9C3F74303E6C0836D9BA6,
	LogUtil_InitializeLogging_m115AF75DC00C3EB122A9232967C985A343600E0F,
	LogUtil_ConvertLogLevel_mEE4246508FD42F7EB6A14DF9985479B93C7CB6D2,
	LogUtil_LogMessage_m59195C58FF0FC63681CED394EB6618F03A25B3B4,
	LogUtil_LogMessageFromCallback_mE97D3007172D52FD246643DC17523BA77867EC1C,
	LogUtil__ctor_mFE64F3E0CAE4C8D317093D419552825F2187F3EA,
	LogUtil_Finalize_mA58D6095B47CD414CEED5AB924C2D53F34FF9D55,
	LogUtil_Dispose_m69B36B965145091F6023543E577B1D882AAD3F31,
	LogUtil_Dispose_m97EA8C366043F8F98301F73F488901880DA431CB,
	LogUtil_U3C_ctorU3Eb__9_0_m057EE72CCDA8877817C356F04A3FB0403BDC8268,
	LogMessageDelegate__ctor_mB6AACCCEAE43E818C4B0DFCF6388FF4CC7200F10,
	LogMessageDelegate_Invoke_m93848481738EC2A03FD8F5600C132464290BDAC8,
	MonoPInvokeCallbackAttribute__ctor_m4AE84268E5E69C1E4E1E8CD7AF145EF3C73DDA02,
	FutureBase__ctor_m98C8AE4F030730C1CEE7E0B4A1816C623F2B9BE0,
	FutureBase_Finalize_m9CD99D25C0199A337732E16288ABCE051A4D5CB7,
	FutureBase_Dispose_m32193D02DE4608C6C3EDF42F3D0495707DA4D15E,
	FutureBase_Dispose_m17D716EFFAF752B7DBF402C73D757D02C34457EB,
	FutureBase_status_mC75FD35438B176F95462D3A5D7D9194629211902,
	FutureBase_error_m47E3B5E0A43B4C19510A77B3658EE5D7D10B6030,
	FutureBase_error_message_m5CC18319253B1ECC3C8AC675B213A08B1755D527,
	StringStringMap__ctor_m493F3867E24E87A4D890A56366DAE5D3E2172E35,
	StringStringMap_getCPtr_m04B3D3C2A8A0A7FE18E8E41C0C9B9776962545D1,
	StringStringMap_Finalize_mE24B29EBA8476775366BE1E56D51757FF34412D6,
	StringStringMap_Dispose_mFECCAB7DCE0572DDE5BAFE9999616BBAD5B42D12,
	StringStringMap_Dispose_m88AC30342C42C0575CC7029859A48F77BCCA4AC0,
	StringStringMap_get_Item_m01061069FC7C194E45C518987A14FA5918806BE1,
	StringStringMap_set_Item_m975DA3FC714B74CB4E7D4CAAE0482D7B669D186F,
	StringStringMap_TryGetValue_mEF4B761217F202E2F25001244A02516D4B85263D,
	StringStringMap_get_Count_m2B11AF48BF1530FCB3ED130712C6B5BADC76A848,
	StringStringMap_get_IsReadOnly_m679F53D527AD174BC0D08D0F86998D53FDA6F481,
	StringStringMap_get_Keys_m558C6C1516539080580AB4D6F8B2905B4B604AC4,
	StringStringMap_get_Values_m55F926C89AA88AEC46FA2BE15C4812B205D35FC6,
	StringStringMap_Add_mA5E43086723E81409FB93BD34211779B2B95B466,
	StringStringMap_Remove_m403C3C6E00AF3F626AFF1EF753E5A69AC4D4C06A,
	StringStringMap_Contains_m5F1544CEFA19C9797BD02C53E7DD9EB2C9097916,
	StringStringMap_CopyTo_m5D78F9003BE6B16285A5C22504731EA6D14B6454,
	StringStringMap_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_StringU3EU3E_GetEnumerator_m4B458CA8BEE64D77627CBEF5804F460379D3CCEC,
	StringStringMap_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_m9A35129110F7B5928202096310FDD2CCEBFC9CAF,
	StringStringMap_GetEnumerator_m5629FBE397B23B93415341566E71A149DBF1362A,
	StringStringMap__ctor_m5295C0F5394545250F06A5F0A53C9ABB48D67794,
	StringStringMap_size_m9C20BFD104A8B49600AD587CC29C4A748F408DC4,
	StringStringMap_Clear_mC33CC5FAC952437E4A1844D77F07682AB4A440A8,
	StringStringMap_getitem_m8DADD76F6CD52B4B98611DD4292910963C079C1B,
	StringStringMap_setitem_mC5A0170C20E03D926C187A4B7AC6B0B96FB18C00,
	StringStringMap_ContainsKey_mD13F26BD8A08E581ADB303D38074819105C605A5,
	StringStringMap_Add_m01048CFD777D82B2F693B6D71A4D452FAF7AEAC1,
	StringStringMap_Remove_m986E61004827D7F62831AEE307E6020383C3CB4D,
	StringStringMap_create_iterator_begin_mF1C81519248E3646D376EC9F288C1C6A2065630D,
	StringStringMap_get_next_key_m92C94637FF8B9A00B08BFCAD08CD41705AEAEE9B,
	StringStringMap_destroy_iterator_m75AD241AD5316B8B09070BF04C571918FE34BA04,
	StringStringMapEnumerator__ctor_m1659D491782A6E753AC1792C39802A79860F75B3,
	StringStringMapEnumerator_get_Current_mE221D98D4E0B07220825ED8752B9714AADBCB04E,
	StringStringMapEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_mF90B634B9E830DB2302FBCBC3F6DC9625AB41FBF,
	StringStringMapEnumerator_MoveNext_m19D42D8E29467683964512499AABEF93BD9830E1,
	StringStringMapEnumerator_Reset_mFE28DCE2D21F6639E75B255AAF56D34BFA7A151A,
	StringStringMapEnumerator_Dispose_mDB957DDBEF7D7D2207BEF3C8EF98D4AF2357B9E8,
	AppOptionsInternal__ctor_m1A801B68806227BB2756E18B7BFC46995E6768DE,
	AppOptionsInternal_Finalize_mC4193C521BD48563554BDD3EA7DE8B6BBD2922CE,
	AppOptionsInternal_Dispose_m69ACB4A20CD2B29F2D09D96FB7DBC1F4E4599B0A,
	AppOptionsInternal_Dispose_m7E5CDD057C84B8A6B796F044B70BDC713DAA9491,
	AppOptionsInternal_get_DatabaseUrl_mD24AE9A589D060986270F6F7E582FC7244A99580,
	AppOptionsInternal_GetDatabaseUrlInternal_mBB900411ECF9D1C6759CC2BE5C59A565E0E4061E,
	AppOptionsInternal_get_AppId_m9E10A96FC162A8F195B33B40170D42A426D3A766,
	AppOptionsInternal_get_ApiKey_m4FEA968CCE295C61306024A53B87EB462C793FDD,
	AppOptionsInternal_get_MessageSenderId_m685FAF74B5E0F003C765471030BFEC9049B188A5,
	AppOptionsInternal_get_StorageBucket_m990618B90CAFF7EC9D688C494C058156DC353535,
	AppOptionsInternal_get_ProjectId_m3CB8C9F459B7ABE7266E303E627FEC079DE825ED,
	AppOptionsInternal_get_PackageName_m44A62026BBC91627D666692A8116007B7E6FAB40,
	FirebaseApp__ctor_mC539AF748C2E16CD3B7820D6039B9A29DBDF908C,
	FirebaseApp_getCPtr_mCF6551417C0F1D98798ED7810553EBD977381D16,
	FirebaseApp_Finalize_mF8DA91BE30AF031A390E068301053AEF3D6B5A98,
	FirebaseApp_Dispose_mC1965A7AE8BAB834DB652BF0BACF377F3D45192B,
	FirebaseApp_Dispose_m7AA869727509B99D04399B9BA7F1FEEC0251974A,
	FirebaseApp__cctor_m4D3A163F336E9C9A0FDB71D5484328266A6D48C5,
	FirebaseApp_TranslateDllNotFoundException_m387A100ACF3B581E16E3248735AC6099157E0B21,
	FirebaseApp_get_DefaultInstance_mCA6FC0DE0B25880FC6ACEAD5585ED84407690C61,
	FirebaseApp_GetInstance_m2731CBD05B439F8E447DAA971E7E0627E0CB95D1,
	FirebaseApp_Create_m2BD29990D5510D08A1503739EA8D3F82186ABA69,
	FirebaseApp_get_Name_m89C11F96726C8E4FD3CCAE04A5DC3129F7CD975E,
	FirebaseApp_get_LogLevel_m50774F8027C8BF6EB481CBFCCCFC266863392D83,
	FirebaseApp_add_AppDisposed_m849DD816EFE8D669DBFA139254D5E3C4D8C78F85,
	FirebaseApp_remove_AppDisposed_mAAF77EA50314A467CBB4481448C72FA9B7173289,
	FirebaseApp_AddReference_m562BA6DFE00568AC30B15C36D8BB848F14EDED95,
	FirebaseApp_RemoveReference_m3C28724EDB5D9F20A2A4924E517A8FF79C7E3425,
	FirebaseApp_ThrowIfNull_mEBB4A7F4A0E30B8F6969C68C340AF30D44491B20,
	FirebaseApp_InitializeAppUtilCallbacks_mB4CA136547ECB2365434896EA8C1AB50904FEBD5,
	FirebaseApp_OnAllAppsDestroyed_m685A494EFE8AB255A373CE0C8ABDFD39D81A7463,
	FirebaseApp_UrlStringToUri_m1CCEC1253A9E24DAF5BAACA87A2741C6163090D6,
	FirebaseApp_WeakReferenceGetTarget_m48BCAA7BDF44B90AFC6B68BE061F879F21BDEA5D,
	FirebaseApp_InitializeCrashlyticsIfPresent_m03191690A38691879D03C29026593CD2684BE675,
	FirebaseApp_CreateAndTrack_m2A77847F5AFE916AA5E4AAA3FBD425DEF5406817,
	FirebaseApp_ThrowIfCheckDependenciesRunning_m7E71E39E28A8732D2F4D47DB3B9B4DC7B0CF1406,
	FirebaseApp_IsCheckDependenciesRunning_mB4AE050866713DDA5DABB5C81060B3F780D8CE84,
	FirebaseApp_get_Options_mDD04F28C88747DC1332D1A9363E7B6F6FCC7A12E,
	FirebaseApp_options_m18F2F0A7F1E29CDB4AD75C0746A33922B1BF7C06,
	FirebaseApp_get_NameInternal_m493D9AEC87709D1197A1997C7560AFEBB107FBCE,
	FirebaseApp_CreateInternal_m0E179C78560E4F6E71A0FCEE92CA76788DA9F233,
	FirebaseApp_ReleaseReferenceInternal_mF26428D844B9B729A421CE9C89BC3AF16F67A932,
	FirebaseApp_RegisterLibrariesInternal_mE4433124F793D190C3AB1D6D790D50A282EB3CBE,
	FirebaseApp_LogHeartbeatInternal_mD0C5A3E3A7B754D36B221A326C92A605503DD1FE,
	FirebaseApp_AppSetDefaultConfigPath_m2EC0842B39AB8C4D1360CF089518FD2BDC8AE63B,
	FirebaseApp_get_DefaultName_m4FDA39399A8C50D6A383DEC274FF208BA6BFE40D,
	EnableModuleParams_get_CppModuleName_mB91981F21F3F94D82CD64DD7BD810741CBB04E3A,
	EnableModuleParams_set_CppModuleName_mF1C3FE3BBE44DEDB23AF2879630075AEAC7106DF,
	EnableModuleParams_get_CSharpClassName_m04AD392AA82FCE1E6636F812672C77F294AC16EC,
	EnableModuleParams_set_CSharpClassName_m9152635BDD8F608352C12F3447962C10F7DF4F43,
	EnableModuleParams_get_AlwaysEnable_mC44F8EA7A9EDCD493C6B8E04E3B3CF00D09FDEA6,
	EnableModuleParams_set_AlwaysEnable_m3F7638041BDA0CC3669AD7119C68ABD2B6F7C482,
	EnableModuleParams__ctor_m448B394AF46BBC2CE9C3301F732850625F6B37EF,
	CreateDelegate__ctor_m966C39812E422F82DD3AACF101F012749B1F9E12,
	CreateDelegate_Invoke_m3C05F10053C0FD938376079571835049ADDD6186,
	U3CU3Ec__cctor_mA283C3B885447FABD62442EF65AD1CD31780A9A4,
	U3CU3Ec__ctor_m463FBDE085153371662615419AFD8228F4704F75,
	U3CU3Ec_U3CCreateU3Eb__15_0_mF43BDAE5875C0C407791D7735DC43BB00EB29F32,
	U3CU3Ec_U3CCreateAndTrackU3Eb__48_0_m353C5F1E7C6BDE8601757A37801E17C89CA49AC2,
	AppUtilPINVOKE__cctor_m5F99B3E75CC446C45B94B31C3FC38940090CBA0C,
	AppUtilPINVOKE_delete_FutureBase_m35315C54A3443849BE8190F0BF2AA098038DF0BA,
	AppUtilPINVOKE_FutureBase_status_mB318847BE2E3610D5D82665BF0C61611D550C111,
	AppUtilPINVOKE_FutureBase_error_m69E38EE9B25E7418CD95085AAD616EE166B43C73,
	AppUtilPINVOKE_FutureBase_error_message_mEF99F3C887A59D7759C68A185B2DC6E721DB48C1,
	AppUtilPINVOKE_new_StringStringMap__SWIG_0_mDDF7009C0CA4756CB189FC4D7221E8F4806D15F8,
	AppUtilPINVOKE_StringStringMap_size_m8A699D5B49047D52BA2BE816EFCE3086F0FCACF1,
	AppUtilPINVOKE_StringStringMap_Clear_mD6F8702554987F106A25C80AB8EB99EC65E04804,
	AppUtilPINVOKE_StringStringMap_getitem_m496E09DCEAA62B45CC6A58190C168B273B4144FA,
	AppUtilPINVOKE_StringStringMap_setitem_m7CB5926A7AF5B874DD70455F8C8C5A2FEEDEDFCF,
	AppUtilPINVOKE_StringStringMap_ContainsKey_m42410B4C36F897A6B63E203ACFA5F99CFE0322C4,
	AppUtilPINVOKE_StringStringMap_Add_m390C9E5584D49686F57F4711DB82FCC36B2DEE2F,
	AppUtilPINVOKE_StringStringMap_Remove_m8BADCE6A740F08DA5164AAB5F72E66A6212CE1D0,
	AppUtilPINVOKE_StringStringMap_create_iterator_begin_mF2EB9D203087421D4AA782470B8D39F1A296EC57,
	AppUtilPINVOKE_StringStringMap_get_next_key_mB3EE19C1F3D873D85B3812F8434844E9E247BA1A,
	AppUtilPINVOKE_StringStringMap_destroy_iterator_mF7843E1738F466CE3636B6D889578EA461B6EB06,
	AppUtilPINVOKE_delete_StringStringMap_m2DC911DECB71152B2C0773292BD24161F34420D3,
	AppUtilPINVOKE_AppOptionsInternal_GetDatabaseUrlInternal_mBFBA3895596D4C988482F52F1138400F540ABA89,
	AppUtilPINVOKE_AppOptionsInternal_AppId_get_mA507D544796377B2A97720CEF8CF9506655833D2,
	AppUtilPINVOKE_AppOptionsInternal_ApiKey_get_m50B0EFC93B31470BB5AE9774185045B8DADD8227,
	AppUtilPINVOKE_AppOptionsInternal_MessageSenderId_get_mA6A0233FE92A3656E1EE44BDFABCC10C1344B5D7,
	AppUtilPINVOKE_AppOptionsInternal_StorageBucket_get_mFE5B6DC41436813F2D270C558EDE2231096021EF,
	AppUtilPINVOKE_AppOptionsInternal_ProjectId_get_m5978BCDCDEF6899BEC86AF5C25085194C786978B,
	AppUtilPINVOKE_AppOptionsInternal_PackageName_get_m0A280356D2F7014C72F7E9A1A861B825A2B12F05,
	AppUtilPINVOKE_delete_AppOptionsInternal_m8493A4404C826F6CFB817346A212D8D73A1BD772,
	AppUtilPINVOKE_FirebaseApp_options_mE445A677BA68FE0BF261BE9B4B1E059093BEE091,
	AppUtilPINVOKE_FirebaseApp_NameInternal_get_m9A54BA928A200855D9247957928CE38DB57F9203,
	AppUtilPINVOKE_FirebaseApp_CreateInternal__SWIG_0_mB30D1FDF6B0ACB2860E853CBD85E627FBC6A8DD1,
	AppUtilPINVOKE_FirebaseApp_ReleaseReferenceInternal_m5C0ABB7130466CD7284512BDCDC0348E4990A7B4,
	AppUtilPINVOKE_FirebaseApp_GetLogLevelInternal_mFFAA837D26D43B99A7FE761CAE23F31D8C86EFA5,
	AppUtilPINVOKE_FirebaseApp_RegisterLibrariesInternal_m27619F8F7884952E3B023B5B51A0B2E0B4AABCA1,
	AppUtilPINVOKE_FirebaseApp_LogHeartbeatInternal_m7F63F60F846CDB4F3AF3F8FD9F794C9B59BAC75F,
	AppUtilPINVOKE_FirebaseApp_AppSetDefaultConfigPath_m6DB74FB4F54B60CBC220635058868945B35B5982,
	AppUtilPINVOKE_FirebaseApp_DefaultName_get_m8B412F84603DA4E77B29DD71DB6132A6DF8F9021,
	AppUtilPINVOKE_PollCallbacks_mA6778D990E2A10564AC0D44F090CD3292D07EBB4,
	AppUtilPINVOKE_AppEnableLogCallback_m989A6148B22C9D62E5DB7F1AD5468CC1DABA9DBB,
	AppUtilPINVOKE_SetEnabledAllAppCallbacks_m2C58D4C43A8974C0D122ABD92136BD229B6C3CE8,
	AppUtilPINVOKE_SetEnabledAppCallbackByName_m04CC33BEBE5D735EF4C6F60A2BEB281FFD386812,
	AppUtilPINVOKE_GetEnabledAppCallbackByName_mBEA56AA2FC4E7B0F7AE2FC32B23009584DAF249A,
	AppUtilPINVOKE_SetLogFunction_m4800E17E1145F9FCEF0FB33B411E5A83DE5D59F1,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AppUtil_mCEE5500031BD8FB6065EC0B7D8372A6E62017F96,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AppUtil_m5794B6B4FD801CDE8D79E9C55D91B99DA476A38E,
	SWIGExceptionHelper_SetPendingApplicationException_m6C69E5AD1E4D604354F6077B67D5772F795426E6,
	SWIGExceptionHelper_SetPendingArithmeticException_m4CDF27D022538556DA8E54F2DBB6957D0419523B,
	SWIGExceptionHelper_SetPendingDivideByZeroException_mAF2154299A69434F0D8175854649FAEC212D6E88,
	SWIGExceptionHelper_SetPendingIndexOutOfRangeException_mD1828B56CAF9C5635AA0F7E49B6856DE01F73DE3,
	SWIGExceptionHelper_SetPendingInvalidCastException_mB702FABB41074EFCA6FFBFD8B24669CA879077DB,
	SWIGExceptionHelper_SetPendingInvalidOperationException_m32C61E47D26CB43C215DC086CF5D7A4B9E853716,
	SWIGExceptionHelper_SetPendingIOException_m932608D47F516BBA327D41B529BC101EC07E6176,
	SWIGExceptionHelper_SetPendingNullReferenceException_m4BE790ED7CD62E6B2281C9B040F815697FD4254D,
	SWIGExceptionHelper_SetPendingOutOfMemoryException_mDB9A2DA0C3243B3E047219380EF92B33F5DA575C,
	SWIGExceptionHelper_SetPendingOverflowException_mCA37E08F462E1BF14D59A78731D61016F5D92EA5,
	SWIGExceptionHelper_SetPendingSystemException_mE70086ECFA51014FFF3D0557844E07DEF2FA8F24,
	SWIGExceptionHelper_SetPendingArgumentException_m7AB4E7B60FE6666951EB975CB0D7095D5FF2DDC4,
	SWIGExceptionHelper_SetPendingArgumentNullException_m4B3F36D2AA2C7D504EB1822AB727B69D90C077F2,
	SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m8C7D11A5CE60E31555D0C41E55DF4AE610052309,
	SWIGExceptionHelper__cctor_mC59330BC82D37F168B01A2421230EE6920BBBEBB,
	SWIGExceptionHelper__ctor_m4DB6794D8CB5F1A9740C37B0C257B69982C013B9,
	ExceptionDelegate__ctor_m49AB94CEC8E6544CE0D7B1E2300735728EE336D8,
	ExceptionDelegate_Invoke_mE04E9A1D96F5AE159E3D7878E87706B91A149B25,
	ExceptionArgumentDelegate__ctor_m45E4EFAE5F14FFEC5843A00ABEF4D0E1F0854629,
	ExceptionArgumentDelegate_Invoke_m46BEB8B17B239BFB498C54B89EB06352BD1948F8,
	SWIGPendingException_get_Pending_m0BEE83B2B2559DE231EE2A87DF09649355775538,
	SWIGPendingException_Set_mCDF526D868E4A3C95BB195595F5259755F0B2985,
	SWIGPendingException_Retrieve_m5C29154CE762C145ECCBA0E7A37F5579366647BF,
	SWIGPendingException__cctor_m0BC7954EFBB1D89B9D9D98858F5FCD7F62259EC1,
	SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_m6DE958A7010EB13AEC2BBFF99775C3D00493CB2B,
	SWIGStringHelper_CreateString_m4EDC140847BEAA68EFFBFC908E592173558433B9,
	SWIGStringHelper__cctor_mBC542C2F3D6E2B1C11343D9BDF77681891886413,
	SWIGStringHelper__ctor_mF5EBABDC102D937A919B6A6CCA3690E2244ECE85,
	SWIGStringDelegate__ctor_m4D5B167B33345B58192AD3B50D1F8901A18F4F4D,
	SWIGStringDelegate_Invoke_m9831BD87E0EBFECFF48B4CA3FFEB118280C2ABCB,
	AppUtil_PollCallbacks_m1828733E0D5AE0AD1AF100649BE21C1580C5F274,
	AppUtil_AppEnableLogCallback_mB1D2F4E350E0261617E8E903854CEF3919E8AC50,
	AppUtil_SetEnabledAllAppCallbacks_m96646FFBAC8641D920F5A0B2DB16F25CDA74D558,
	AppUtil_SetEnabledAppCallbackByName_mBC81C70A57C6C9BEB4E239034DB74BF4A002A47E,
	AppUtil_GetEnabledAppCallbackByName_mAD26376340D75FE4FF7AD100E085D67513A9C152,
	AppUtil_SetLogFunction_m5C269A1922672E6E2408D3C63AB0D40888CB1997,
	VersionInfo_get_SdkVersion_mF27457E8D6E11FB4DE0C247AFBFAC7860A51653A,
	VersionInfo_get_BuildSource_m70093EC2695C02267AC85D7D653526CBF1D634E7,
	FirebaseAppUtils_get_Instance_mE6801A039F44928B0917260E861F377364D031EC,
	FirebaseAppUtils_TranslateDllNotFoundException_m8D9620D2F9B093C4DBF14AD9803923F0763955B8,
	FirebaseAppUtils_PollCallbacks_m94AC1FCAA3602F030E6AA26C1FD6CB03E0F7155C,
	FirebaseAppUtils_GetLogLevel_m420F7E6140E65C5494538339E1322E33F3661105,
	FirebaseAppUtils__ctor_m77E9C2ADF611B1553A685AC953C5508DFD636CD4,
	FirebaseAppUtils__cctor_m295E2C99A656E8A9BAD5578B53705DD1D2D601E5,
};
static const int32_t s_InvokerIndices[219] = 
{
	7252,
	5754,
	5754,
	5754,
	5754,
	7114,
	5754,
	5754,
	5754,
	5754,
	2948,
	5725,
	5725,
	2948,
	1623,
	10847,
	10847,
	10889,
	10889,
	10889,
	10333,
	9909,
	9909,
	7252,
	7252,
	7252,
	5650,
	3206,
	3202,
	2948,
	5754,
	3171,
	7252,
	7252,
	5650,
	7083,
	7083,
	7114,
	3171,
	10303,
	7252,
	7252,
	5650,
	5117,
	3206,
	1963,
	7083,
	7012,
	7114,
	7114,
	5497,
	3842,
	3842,
	3199,
	7114,
	7114,
	7114,
	7252,
	7235,
	7252,
	5117,
	3206,
	4133,
	3206,
	4133,
	7086,
	5115,
	5728,
	5754,
	6872,
	7114,
	7012,
	7252,
	7252,
	3171,
	7252,
	7252,
	5650,
	7114,
	7114,
	7114,
	7114,
	7114,
	7114,
	7114,
	7114,
	3171,
	10303,
	7252,
	7252,
	5650,
	10889,
	10683,
	10847,
	10466,
	10847,
	7114,
	10840,
	5754,
	5754,
	7252,
	7252,
	7252,
	10889,
	10889,
	10466,
	10466,
	10823,
	9667,
	10889,
	10823,
	7114,
	7114,
	7114,
	10847,
	10683,
	10683,
	10683,
	10683,
	10847,
	7114,
	5754,
	7114,
	5754,
	7012,
	5650,
	1692,
	3202,
	7114,
	10889,
	7252,
	7114,
	7012,
	10889,
	10678,
	10331,
	10331,
	10460,
	10842,
	10610,
	10678,
	9642,
	9170,
	9385,
	9170,
	9385,
	10367,
	9641,
	9901,
	10678,
	10460,
	10460,
	10460,
	10460,
	10460,
	10460,
	10460,
	10678,
	10367,
	10460,
	10842,
	10678,
	10840,
	10678,
	10678,
	10683,
	10847,
	10889,
	10675,
	10675,
	9969,
	10224,
	10683,
	7449,
	9233,
	10683,
	10683,
	10683,
	10683,
	10683,
	10683,
	10683,
	10683,
	10683,
	10683,
	10683,
	9979,
	9979,
	9979,
	10889,
	7252,
	3202,
	5754,
	3202,
	3206,
	10823,
	10683,
	10847,
	10889,
	10683,
	10466,
	10889,
	7252,
	3202,
	5117,
	10889,
	10675,
	10675,
	9969,
	10224,
	10683,
	10847,
	10847,
	10847,
	5754,
	7252,
	7083,
	7252,
	10889,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[16] = 
{
	{ 0x06000017, 10,  (void**)&LogUtil_LogMessageFromCallback_mE97D3007172D52FD246643DC17523BA77867EC1C_RuntimeMethod_var, 0 },
	{ 0x060000B0, 22,  (void**)&SWIGExceptionHelper_SetPendingApplicationException_m6C69E5AD1E4D604354F6077B67D5772F795426E6_RuntimeMethod_var, 0 },
	{ 0x060000B1, 26,  (void**)&SWIGExceptionHelper_SetPendingArithmeticException_m4CDF27D022538556DA8E54F2DBB6957D0419523B_RuntimeMethod_var, 0 },
	{ 0x060000B2, 27,  (void**)&SWIGExceptionHelper_SetPendingDivideByZeroException_mAF2154299A69434F0D8175854649FAEC212D6E88_RuntimeMethod_var, 0 },
	{ 0x060000B3, 29,  (void**)&SWIGExceptionHelper_SetPendingIndexOutOfRangeException_mD1828B56CAF9C5635AA0F7E49B6856DE01F73DE3_RuntimeMethod_var, 0 },
	{ 0x060000B4, 30,  (void**)&SWIGExceptionHelper_SetPendingInvalidCastException_mB702FABB41074EFCA6FFBFD8B24669CA879077DB_RuntimeMethod_var, 0 },
	{ 0x060000B5, 31,  (void**)&SWIGExceptionHelper_SetPendingInvalidOperationException_m32C61E47D26CB43C215DC086CF5D7A4B9E853716_RuntimeMethod_var, 0 },
	{ 0x060000B6, 28,  (void**)&SWIGExceptionHelper_SetPendingIOException_m932608D47F516BBA327D41B529BC101EC07E6176_RuntimeMethod_var, 0 },
	{ 0x060000B7, 32,  (void**)&SWIGExceptionHelper_SetPendingNullReferenceException_m4BE790ED7CD62E6B2281C9B040F815697FD4254D_RuntimeMethod_var, 0 },
	{ 0x060000B8, 33,  (void**)&SWIGExceptionHelper_SetPendingOutOfMemoryException_mDB9A2DA0C3243B3E047219380EF92B33F5DA575C_RuntimeMethod_var, 0 },
	{ 0x060000B9, 34,  (void**)&SWIGExceptionHelper_SetPendingOverflowException_mCA37E08F462E1BF14D59A78731D61016F5D92EA5_RuntimeMethod_var, 0 },
	{ 0x060000BA, 35,  (void**)&SWIGExceptionHelper_SetPendingSystemException_mE70086ECFA51014FFF3D0557844E07DEF2FA8F24_RuntimeMethod_var, 0 },
	{ 0x060000BB, 23,  (void**)&SWIGExceptionHelper_SetPendingArgumentException_m7AB4E7B60FE6666951EB975CB0D7095D5FF2DDC4_RuntimeMethod_var, 0 },
	{ 0x060000BC, 24,  (void**)&SWIGExceptionHelper_SetPendingArgumentNullException_m4B3F36D2AA2C7D504EB1822AB727B69D90C077F2_RuntimeMethod_var, 0 },
	{ 0x060000BD, 25,  (void**)&SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m8C7D11A5CE60E31555D0C41E55DF4AE610052309_RuntimeMethod_var, 0 },
	{ 0x060000C9, 36,  (void**)&SWIGStringHelper_CreateString_m4EDC140847BEAA68EFFBFC908E592173558433B9_RuntimeMethod_var, 0 },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Firebase_App_CodeGenModule;
const Il2CppCodeGenModule g_Firebase_App_CodeGenModule = 
{
	"Firebase.App.dll",
	219,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	16,
	s_reversePInvokeIndices,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

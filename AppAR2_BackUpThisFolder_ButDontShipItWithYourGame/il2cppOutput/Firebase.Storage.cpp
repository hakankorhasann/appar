﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InvokerActionInvoker1;
template <typename T1>
struct InvokerActionInvoker1<T1*>
{
	static inline void Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1)
	{
		void* params[1] = { p1 };
		method->invoker_method(methodPtr, method, obj, params, NULL);
	}
};
template <typename T1, typename T2>
struct InvokerActionInvoker2;
template <typename T1, typename T2>
struct InvokerActionInvoker2<T1*, T2*>
{
	static inline void Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1, T2* p2)
	{
		void* params[2] = { p1, p2 };
		method->invoker_method(methodPtr, method, obj, params, NULL);
	}
};
template <typename T1, typename T2, typename T3>
struct InvokerActionInvoker3;
template <typename T1, typename T2, typename T3>
struct InvokerActionInvoker3<T1*, T2*, T3*>
{
	static inline void Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1, T2* p2, T3* p3)
	{
		void* params[3] = { p1, p2, p3 };
		method->invoker_method(methodPtr, method, obj, params, NULL);
	}
};
template <typename R, typename T1>
struct InvokerFuncInvoker1;
template <typename R, typename T1>
struct InvokerFuncInvoker1<R, T1*>
{
	static inline R Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1)
	{
		R ret;
		void* params[1] = { p1 };
		method->invoker_method(methodPtr, method, obj, params, &ret);
		return ret;
	}
};
template <typename R, typename T1, typename T2>
struct InvokerFuncInvoker2;
template <typename R, typename T1, typename T2>
struct InvokerFuncInvoker2<R, T1*, T2*>
{
	static inline R Invoke (Il2CppMethodPointer methodPtr, const RuntimeMethod* method, void* obj, T1* p1, T2* p2)
	{
		R ret;
		void* params[2] = { p1, p2 };
		method->invoker_method(methodPtr, method, obj, params, &ret);
		return ret;
	}
};

// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.FirebaseApp>
struct Dictionary_2_tD81F54C87D78FE70A5DE7DAA170AE5EB4E54E8C3;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA;
// System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>
struct Dictionary_2_t070EAA8A0D7DC2B4DA1223E3809A83B3933BF21A;
// System.Collections.Generic.Dictionary`2<System.String,Firebase.Storage.FirebaseStorage>
struct Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Firebase.Storage.FirebaseStorage>
struct KeyCollection_t0EE94BCBDDE933CDFC77C7A7B3FC86E99A93515E;
// System.Collections.Generic.List`1<Firebase.Storage.Internal.ModuleLogger>
struct List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.List`1<System.WeakReference>
struct List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Firebase.Storage.FirebaseStorage>
struct ValueCollection_tB95003639B4A2A78AC9792323626BC0692124E40;
// System.Collections.Generic.Dictionary`2/Entry<System.String,Firebase.Storage.FirebaseStorage>[]
struct EntryU5BU5D_tF5CA44A7FA9CFF54ADE7594D6463C5C15CFE29C4;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// Firebase.Storage.Internal.ModuleLogger[]
struct ModuleLoggerU5BU5D_t51DCE1AC000335B7B57A64FA93629D623819AFF1;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.WeakReference[]
struct WeakReferenceU5BU5D_t946B92E92B5492BD3357181A37AE958D5DED9ED3;
// Firebase.AppOptions
struct AppOptions_tC85C010A614E35ED5C64709D909D4525D9DE6D09;
// System.ApplicationException
struct ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A;
// System.ArgumentException
struct ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263;
// System.ArgumentNullException
struct ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F;
// System.ArithmeticException
struct ArithmeticException_t07E77822D0007642BC8959A671E70D1F33C84FEA;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// System.DivideByZeroException
struct DivideByZeroException_tC43171E50A38F5CD4242D258D0B0C6B74898C279;
// System.EventArgs
struct EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377;
// System.EventHandler
struct EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82;
// System.Exception
struct Exception_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// Firebase.FirebaseApp
struct FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25;
// Firebase.Platform.FirebaseAppPlatform
struct FirebaseAppPlatform_t5AD8517EA34467536BAC8C7C6EB4D4B6880312A2;
// Firebase.Storage.FirebaseStorage
struct FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1;
// Firebase.Storage.FirebaseStorageInternal
struct FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.IO.IOException
struct IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82;
// System.InvalidCastException
struct InvalidCastException_t47FC62F21A3937E814D20381DDACEF240E95AC2E;
// System.InvalidOperationException
struct InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Firebase.Storage.Internal.ModuleLogger
struct ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97;
// System.NullReferenceException
struct NullReferenceException_tBDE63A6D24569B964908408389070C6A9F5005BB;
// System.OutOfMemoryException
struct OutOfMemoryException_tE6DC2F937EC4A8699271D5151C4DF83BDE99EE7F;
// System.OverflowException
struct OverflowException_t6F6AD8CACE20C37F701C05B373A215C4802FAB0C;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// Firebase.Storage.StorageReference
struct StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019;
// Firebase.Storage.StorageReferenceInternal
struct StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4;
// System.String
struct String_t;
// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295;
// System.Uri
struct Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// System.WeakReference
struct WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E;
// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149;
// Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004;
// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6;
// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19;
// Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261;

IL2CPP_EXTERN_C RuntimeClass* ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArithmeticException_t07E77822D0007642BC8959A671E70D1F33C84FEA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DivideByZeroException_tC43171E50A38F5CD4242D258D0B0C6B74898C279_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FirebaseLogger_t17574FB770AB2CB02AF3459B2EF9D27FC9C24F42_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GC_t920F9CF6EBB7C787E5010A4352E1B587F356DC58_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidCastException_t47FC62F21A3937E814D20381DDACEF240E95AC2E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LogUtil_t004F911611FD3AE3085F5CA8159A798C3CA16D39_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NullReferenceException_tBDE63A6D24569B964908408389070C6A9F5005BB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OutOfMemoryException_tE6DC2F937EC4A8699271D5151C4DF83BDE99EE7F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OverflowException_t6F6AD8CACE20C37F701C05B373A215C4802FAB0C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0B47BC9031A69F662702621810EC706E2C081467;
IL2CPP_EXTERN_C String_t* _stringLiteral1CB4C6D8E752BD4A976FB4BF3FD6276F3FF8A77A;
IL2CPP_EXTERN_C String_t* _stringLiteral1FB9018D8BFC0FACF068B1067EF9E96C35FED1FE;
IL2CPP_EXTERN_C String_t* _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745;
IL2CPP_EXTERN_C String_t* _stringLiteral2735032CF824904F31866A3DDD0204F0B7FE22CD;
IL2CPP_EXTERN_C String_t* _stringLiteral465D8053A968C034065EEA4680E5BA0DA4E093BA;
IL2CPP_EXTERN_C String_t* _stringLiteral47B60EB9A7DD966F2D170292CD7E78CEE515C121;
IL2CPP_EXTERN_C String_t* _stringLiteral7624E265BB383FB22ED234A2DC36A755B34594EC;
IL2CPP_EXTERN_C String_t* _stringLiteralAA9E04A4F0FAA2410217C8B3DD5093BC82D04A7E;
IL2CPP_EXTERN_C String_t* _stringLiteralABBB41CC11C60D234659F31874B9FB7B8DD1259D;
IL2CPP_EXTERN_C String_t* _stringLiteralB3F14BF976EFD974E34846B742502C802FABAE9D;
IL2CPP_EXTERN_C String_t* _stringLiteralB703A2BE7C7C622C41AA27448E688623872F3623;
IL2CPP_EXTERN_C String_t* _stringLiteralBD2C0B3974A906DFD731ADCD696F181498081EC5;
IL2CPP_EXTERN_C String_t* _stringLiteralCB9902E2A4DFEBC0D9FA90B929984CE8637D6BAE;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralE5ACD853F8874D5E90622ED276C4C96160520552;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Remove_mEEC7F27A9DD6A1AEC6417F54868EAFEFA86452A6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_mDB3CF5FD5D7F94A7EFC62BB1BE6627BB9AF9805E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mBCEC6CB903889BCAF4BAAC48E21CB2EDB8A4825C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_mB06B57C1F6085A6AEA2C437FEE9AD90EEC997C71_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m09AD6CD926C4F2DA33BBE74EA133F80BC1F7AC46_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m6E9D8FDB919C8320D80F7597574FFE9F6D0D88D3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m06D75C6DF4330942019CEC566CE1F77C08873BB9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseStorageInternal_GetInstanceInternal_m671B163ECAF2D5B145D295BF2946CA42A5653F55_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseStorageInternal_GetReference_mAE4C3E5F79096BB4A0FF285DAEF8B89E98A12765_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseStorageInternal_ReleaseReferenceInternal_m1C1D57F61F7501AC8252D78E924A6CA68B2FD3F7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseStorageInternal_get_App_mA92F711407E7D883382645FCF479370018821AF3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseStorageInternal_get_Url_mB62A11FAF14E963FF2A843A45705CEE209636744_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseStorage_GetInstanceInternal_m97042020C5FF0EF33F2883A26DD3E8DCE511D553_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseStorage_OnAppDisposed_mD8FEA28499BCE475237BF7945AA3BCD6C7A2D878_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseStorage_ThrowIfNull_mD51129BAA96196E2F019C3F8402FC21A928CD4EA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m14B2E734DD76D26DE4F1C83D6EB500BE400DE1B9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m25F4DF4DE179873ACDA24F3C314479DDBCCC0BDD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m7F0C23273635833A4D046A87EF3F3731C79A22B7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m7E4BCB7F0652FE05E987A6B3EF0D4F35AA775886_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_mE10942BAF61F258192575C0A6044AADF5D08BB5B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m63EDE1AEAA4A4FCAB6203FB0C262E31C79D24F9E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mF9FFC2B904A1557D19170DC33F3A326E18082C76_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingApplicationException_m3389DA14B6470D215F411A6AB64161049D976B92_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentException_m59C2895E8334A034F0C59E34A3C14F39340478CC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentNullException_m4B7CABD3A4D1075F6323D8EB69F7E6E8667665F8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mB01500F82308A09A74F016F7671A8E282A105C55_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingArithmeticException_mDA9940C49B02A145257997D6E29939B520DECE30_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingDivideByZeroException_m2AC76C31521184D3E9CEA398D2304368C1C3F245_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingIOException_m1E8C16376B2B3701BAC90728915AAF17EC843685_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m9444EB341D36B5E18F7DD12453881B4E5D527B77_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidCastException_m56DA5D72BDA177C1E2E9FA830A03F6A315064907_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidOperationException_m8319A7B285528224F0D94E93683A03F8B2A1CF61_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingNullReferenceException_m5357E1A78F179FAE6A3EC032052906188478E50D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingOutOfMemoryException_mCED02F27DC5E0EDC86EAA55784361567D1D01CA6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingOverflowException_mCA646C70DE90380B08954D135018002CE4A99E86_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingSystemException_mE0F156F2311B8F1ECDFE4267B73A0CE00DA9A01E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGStringHelper_CreateString_m403C46AC9193D0E161E7752F5C6D16DE4F670596_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* StorageReferenceInternal_Child_mB1AFC34728CA3C083BC6FBC74AF0CE9CCE2DC375_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* StorageReferenceInternal_get_Bucket_m7C7737DC9A4D22049E4C6338515E229935A68BF5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* StorageReferenceInternal_get_FullPath_mE93027973F851741548644FA6CE90911FB02FC42_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t6078950D3D482BEE0BA62909C1242119380DF4F0 
{
};

// System.Collections.Generic.Dictionary`2<System.String,Firebase.Storage.FirebaseStorage>
struct Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_tF5CA44A7FA9CFF54ADE7594D6463C5C15CFE29C4* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_t0EE94BCBDDE933CDFC77C7A7B3FC86E99A93515E* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tB95003639B4A2A78AC9792323626BC0692124E40* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.List`1<Firebase.Storage.Internal.ModuleLogger>
struct List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ModuleLoggerU5BU5D_t51DCE1AC000335B7B57A64FA93629D623819AFF1* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ModuleLoggerU5BU5D_t51DCE1AC000335B7B57A64FA93629D623819AFF1* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.WeakReference>
struct List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	WeakReferenceU5BU5D_t946B92E92B5492BD3357181A37AE958D5DED9ED3* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	WeakReferenceU5BU5D_t946B92E92B5492BD3357181A37AE958D5DED9ED3* ___s_emptyArray_5;
};

// Firebase.AppOptions
struct AppOptions_tC85C010A614E35ED5C64709D909D4525D9DE6D09  : public RuntimeObject
{
	// System.Uri Firebase.AppOptions::<DatabaseUrl>k__BackingField
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___U3CDatabaseUrlU3Ek__BackingField_0;
	// System.String Firebase.AppOptions::<AppId>k__BackingField
	String_t* ___U3CAppIdU3Ek__BackingField_1;
	// System.String Firebase.AppOptions::<ApiKey>k__BackingField
	String_t* ___U3CApiKeyU3Ek__BackingField_2;
	// System.String Firebase.AppOptions::<MessageSenderId>k__BackingField
	String_t* ___U3CMessageSenderIdU3Ek__BackingField_3;
	// System.String Firebase.AppOptions::<StorageBucket>k__BackingField
	String_t* ___U3CStorageBucketU3Ek__BackingField_4;
	// System.String Firebase.AppOptions::<ProjectId>k__BackingField
	String_t* ___U3CProjectIdU3Ek__BackingField_5;
	// System.String Firebase.AppOptions::<PackageName>k__BackingField
	String_t* ___U3CPackageNameU3Ek__BackingField_6;
};
struct Il2CppArrayBounds;

// System.EventArgs
struct EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377  : public RuntimeObject
{
};

struct EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377_StaticFields
{
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377* ___Empty_0;
};

// Firebase.Storage.FirebaseStorage
struct FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1  : public RuntimeObject
{
	// Firebase.Storage.FirebaseStorageInternal Firebase.Storage.FirebaseStorage::storageInternal
	FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* ___storageInternal_1;
	// Firebase.FirebaseApp Firebase.Storage.FirebaseStorage::firebaseApp
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* ___firebaseApp_2;
	// System.String Firebase.Storage.FirebaseStorage::instanceKey
	String_t* ___instanceKey_3;
	// Firebase.Storage.Internal.ModuleLogger Firebase.Storage.FirebaseStorage::<Logger>k__BackingField
	ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___U3CLoggerU3Ek__BackingField_5;
};

struct FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields
{
	// System.Collections.Generic.Dictionary`2<System.String,Firebase.Storage.FirebaseStorage> Firebase.Storage.FirebaseStorage::storageByInstanceKey
	Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* ___storageByInstanceKey_0;
	// Firebase.Storage.Internal.ModuleLogger Firebase.Storage.FirebaseStorage::logger
	ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___logger_4;
	// System.Reflection.FieldInfo Firebase.Storage.FirebaseStorage::pathFieldInfo
	FieldInfo_t* ___pathFieldInfo_6;
	// System.Reflection.FieldInfo Firebase.Storage.FirebaseStorage::cachedToString
	FieldInfo_t* ___cachedToString_7;
};

// Firebase.Storage.Internal.ModuleLogger
struct ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97  : public RuntimeObject
{
	// Firebase.Storage.Internal.ModuleLogger Firebase.Storage.Internal.ModuleLogger::parent
	ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___parent_2;
	// System.Collections.Generic.List`1<Firebase.Storage.Internal.ModuleLogger> Firebase.Storage.Internal.ModuleLogger::children
	List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE* ___children_3;
	// System.String Firebase.Storage.Internal.ModuleLogger::tag
	String_t* ___tag_4;
	// Firebase.LogLevel Firebase.Storage.Internal.ModuleLogger::logLevel
	int32_t ___logLevel_5;
};

struct ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_StaticFields
{
	// System.Object Firebase.Storage.Internal.ModuleLogger::lockObject
	RuntimeObject* ___lockObject_0;
	// System.Collections.Generic.List`1<System.WeakReference> Firebase.Storage.Internal.ModuleLogger::roots
	List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9* ___roots_1;
};

// Firebase.Storage.StorageInternalPINVOKE
struct StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9  : public RuntimeObject
{
};

struct StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_StaticFields
{
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper Firebase.Storage.StorageInternalPINVOKE::swigExceptionHelper
	SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149* ___swigExceptionHelper_0;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper Firebase.Storage.StorageInternalPINVOKE::swigStringHelper
	SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004* ___swigStringHelper_1;
};

// Firebase.Storage.StorageReference
struct StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019  : public RuntimeObject
{
	// Firebase.Storage.FirebaseStorage Firebase.Storage.StorageReference::firebaseStorage
	FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* ___firebaseStorage_0;
	// Firebase.Storage.Internal.ModuleLogger Firebase.Storage.StorageReference::<Logger>k__BackingField
	ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___U3CLoggerU3Ek__BackingField_1;
	// Firebase.Storage.StorageReferenceInternal Firebase.Storage.StorageReference::<Internal>k__BackingField
	StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* ___U3CInternalU3Ek__BackingField_2;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149  : public RuntimeObject
{
};

struct SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields
{
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::applicationDelegate
	ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___applicationDelegate_0;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::arithmeticDelegate
	ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___arithmeticDelegate_1;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::divideByZeroDelegate
	ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___divideByZeroDelegate_2;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::indexOutOfRangeDelegate
	ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___indexOutOfRangeDelegate_3;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::invalidCastDelegate
	ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___invalidCastDelegate_4;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::invalidOperationDelegate
	ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___invalidOperationDelegate_5;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::ioDelegate
	ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___ioDelegate_6;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::nullReferenceDelegate
	ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___nullReferenceDelegate_7;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::outOfMemoryDelegate
	ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___outOfMemoryDelegate_8;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::overflowDelegate
	ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___overflowDelegate_9;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::systemDelegate
	ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___systemDelegate_10;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::argumentDelegate
	ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* ___argumentDelegate_11;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::argumentNullDelegate
	ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* ___argumentNullDelegate_12;
	// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::argumentOutOfRangeDelegate
	ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* ___argumentOutOfRangeDelegate_13;
};

// Firebase.Storage.StorageInternalPINVOKE/SWIGPendingException
struct SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB  : public RuntimeObject
{
};

struct SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_StaticFields
{
	// System.Int32 Firebase.Storage.StorageInternalPINVOKE/SWIGPendingException::numExceptionsPending
	int32_t ___numExceptionsPending_1;
	// System.Object Firebase.Storage.StorageInternalPINVOKE/SWIGPendingException::exceptionsLock
	RuntimeObject* ___exceptionsLock_2;
};

struct SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_ThreadStaticFields
{
	// System.Exception Firebase.Storage.StorageInternalPINVOKE/SWIGPendingException::pendingException
	Exception_t* ___pendingException_0;
};

// Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004  : public RuntimeObject
{
};

struct SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004_StaticFields
{
	// Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper/SWIGStringDelegate Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper::stringDelegate
	SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* ___stringDelegate_0;
};

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	RuntimeObject* ____current_3;
};

// System.Collections.Generic.List`1/Enumerator<System.WeakReference>
struct Enumerator_tA83B52184A87D2620C50E08DA33C36DB659322C7 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* ____current_3;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// System.Runtime.InteropServices.GCHandle
struct GCHandle_tC44F6F72EE68BD4CFABA24309DA7A179D41127DC 
{
	// System.IntPtr System.Runtime.InteropServices.GCHandle::handle
	intptr_t ___handle_0;
};

// System.Runtime.InteropServices.HandleRef
struct HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F 
{
	// System.Object System.Runtime.InteropServices.HandleRef::_wrapper
	RuntimeObject* ____wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::_handle
	intptr_t ____handle_1;
};

// System.ApplicationException
struct ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A  : public Exception_t
{
};

// Firebase.FirebaseApp
struct FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25  : public RuntimeObject
{
	// System.Runtime.InteropServices.HandleRef Firebase.FirebaseApp::swigCPtr
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___swigCPtr_0;
	// System.Boolean Firebase.FirebaseApp::swigCMemOwn
	bool ___swigCMemOwn_1;
	// System.String Firebase.FirebaseApp::name
	String_t* ___name_3;
	// System.EventHandler Firebase.FirebaseApp::AppDisposed
	EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82* ___AppDisposed_4;
	// Firebase.Platform.FirebaseAppPlatform Firebase.FirebaseApp::appPlatform
	FirebaseAppPlatform_t5AD8517EA34467536BAC8C7C6EB4D4B6880312A2* ___appPlatform_16;
};

struct FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_StaticFields
{
	// System.Object Firebase.FirebaseApp::disposeLock
	RuntimeObject* ___disposeLock_2;
	// System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp> Firebase.FirebaseApp::nameToProxy
	Dictionary_2_t070EAA8A0D7DC2B4DA1223E3809A83B3933BF21A* ___nameToProxy_5;
	// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.FirebaseApp> Firebase.FirebaseApp::cPtrToProxy
	Dictionary_2_tD81F54C87D78FE70A5DE7DAA170AE5EB4E54E8C3* ___cPtrToProxy_6;
	// System.Boolean Firebase.FirebaseApp::AppUtilCallbacksInitialized
	bool ___AppUtilCallbacksInitialized_7;
	// System.Object Firebase.FirebaseApp::AppUtilCallbacksLock
	RuntimeObject* ___AppUtilCallbacksLock_8;
	// System.Boolean Firebase.FirebaseApp::PreventOnAllAppsDestroyed
	bool ___PreventOnAllAppsDestroyed_9;
	// System.Boolean Firebase.FirebaseApp::crashlyticsInitializationAttempted
	bool ___crashlyticsInitializationAttempted_10;
	// System.Boolean Firebase.FirebaseApp::userAgentRegistered
	bool ___userAgentRegistered_11;
	// System.Int32 Firebase.FirebaseApp::CheckDependenciesThread
	int32_t ___CheckDependenciesThread_14;
	// System.Object Firebase.FirebaseApp::CheckDependenciesThreadLock
	RuntimeObject* ___CheckDependenciesThreadLock_15;
};

// Firebase.Storage.FirebaseStorageInternal
struct FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145  : public RuntimeObject
{
	// System.Runtime.InteropServices.HandleRef Firebase.Storage.FirebaseStorageInternal::swigCPtr
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___swigCPtr_0;
	// System.Boolean Firebase.Storage.FirebaseStorageInternal::swigCMemOwn
	bool ___swigCMemOwn_1;
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// Firebase.Storage.StorageReferenceInternal
struct StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4  : public RuntimeObject
{
	// System.Runtime.InteropServices.HandleRef Firebase.Storage.StorageReferenceInternal::swigCPtr
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___swigCPtr_0;
	// System.Boolean Firebase.Storage.StorageReferenceInternal::swigCMemOwn
	bool ___swigCMemOwn_1;
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// System.WeakReference
struct WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E  : public RuntimeObject
{
	// System.Boolean System.WeakReference::isLongReference
	bool ___isLongReference_0;
	// System.Runtime.InteropServices.GCHandle System.WeakReference::gcHandle
	GCHandle_tC44F6F72EE68BD4CFABA24309DA7A179D41127DC ___gcHandle_1;
};

// System.ArgumentException
struct ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
	// System.String System.ArgumentException::_paramName
	String_t* ____paramName_18;
};

// System.ArithmeticException
struct ArithmeticException_t07E77822D0007642BC8959A671E70D1F33C84FEA  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// System.EventHandler
struct EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82  : public MulticastDelegate_t
{
};

// System.IO.IOException
struct IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// System.InvalidCastException
struct InvalidCastException_t47FC62F21A3937E814D20381DDACEF240E95AC2E  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// System.InvalidOperationException
struct InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// System.NullReferenceException
struct NullReferenceException_tBDE63A6D24569B964908408389070C6A9F5005BB  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// System.OutOfMemoryException
struct OutOfMemoryException_tE6DC2F937EC4A8699271D5151C4DF83BDE99EE7F  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6  : public MulticastDelegate_t
{
};

// Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19  : public MulticastDelegate_t
{
};

// Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261  : public MulticastDelegate_t
{
};

// System.ArgumentNullException
struct ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129  : public ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263
{
};

// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F  : public ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263
{
	// System.Object System.ArgumentOutOfRangeException::_actualValue
	RuntimeObject* ____actualValue_19;
};

// System.DivideByZeroException
struct DivideByZeroException_tC43171E50A38F5CD4242D258D0B0C6B74898C279  : public ArithmeticException_t07E77822D0007642BC8959A671E70D1F33C84FEA
{
};

// System.OverflowException
struct OverflowException_t6F6AD8CACE20C37F701C05B373A215C4802FAB0C  : public ArithmeticException_t07E77822D0007642BC8959A671E70D1F33C84FEA
{
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771  : public RuntimeArray
{
	ALIGN_FIELD (8) Delegate_t* m_Items[1];

	inline Delegate_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_Remove_m5C7C45E75D951A75843F3F7AADD56ECD64F6BC86_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___key0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_mD15380A4ED7CDEE99EA45881577D26BA9CE1B849_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___key0, RuntimeObject** ___value1, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___key0, RuntimeObject* ___value1, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void System.EventHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler__ctor_m95444CE8D5A6F1AFC9793866C3FE884E732DCEB2 (EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void Firebase.FirebaseApp::add_AppDisposed(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseApp_add_AppDisposed_m849DD816EFE8D669DBFA139254D5E3C4D8C78F85 (FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* __this, EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82* ___value0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.Internal.ModuleLogger::.ctor(Firebase.Storage.Internal.ModuleLogger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModuleLogger__ctor_m64B63C3ABE700A1F9BA947FFEE57605EBB4C5850 (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* __this, ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___parentLogger0, const RuntimeMethod* method) ;
// System.String Firebase.Storage.Internal.ModuleLogger::get_Tag()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ModuleLogger_get_Tag_m9F24A98C3F04C66363235C1C339DF611D346378A (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* __this, const RuntimeMethod* method) ;
// System.String Firebase.Storage.FirebaseStorageInternal::get_Url()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseStorageInternal_get_Url_mB62A11FAF14E963FF2A843A45705CEE209636744 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806 (String_t* ___format0, RuntimeObject* ___arg01, RuntimeObject* ___arg12, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.Internal.ModuleLogger::set_Tag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModuleLogger_set_Tag_m93B4431C1F0B91205E7CC9B7EDACACA49C97E42F (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* __this, String_t* ___value0, const RuntimeMethod* method) ;
// Firebase.LogLevel Firebase.FirebaseApp::get_LogLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FirebaseApp_get_LogLevel_m50774F8027C8BF6EB481CBFCCCFC266863392D83 (const RuntimeMethod* method) ;
// System.Void Firebase.Storage.Internal.ModuleLogger::set_Level(Firebase.LogLevel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModuleLogger_set_Level_m1DC27F4C2EC96609D9050B868F8B71467AB4A916 (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.FirebaseStorage::set_Logger(Firebase.Storage.Internal.ModuleLogger)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void FirebaseStorage_set_Logger_m19C7C11321677C2320D1D066CCBBB6F37B3E00F3_inline (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___value0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.FirebaseStorageInternal::SetSwigCMemOwn(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorageInternal_SetSwigCMemOwn_mE268A0AD6F3528EC65D486EC1F0B228D84D96629 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, bool ___ownership0, const RuntimeMethod* method) ;
// System.String Firebase.Storage.FirebaseStorageInternal::get_InstanceKey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseStorageInternal_get_InstanceKey_m9F8ABBA175A15B51661EF6112C1BF06CAD267F4C (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, const RuntimeMethod* method) ;
// Firebase.Storage.Internal.ModuleLogger Firebase.Storage.FirebaseStorage::get_Logger()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* FirebaseStorage_get_Logger_m47F60ACF93F3A0BA8C660B1B566F61BBF30BE391_inline (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30 (String_t* ___format0, RuntimeObject* ___arg01, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.Internal.ModuleLogger::LogMessage(Firebase.LogLevel,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModuleLogger_LogMessage_m568B4169C0A0FA2CA4FF4E6E076870F07131DA3B (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* __this, int32_t ___level0, String_t* ___message1, const RuntimeMethod* method) ;
// System.Void System.Object::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Finalize_mC98C96301CCABFE00F1A7EF8E15DF507CACD42B2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.FirebaseStorage::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorage_Dispose_mF3522116DDF143F6DFAF767D3CBF3092FADA4A6F (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, const RuntimeMethod* method) ;
// System.Void System.GC::SuppressFinalize(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GC_SuppressFinalize_m3352E2F2119EB46913B51B7AAE2F217C63C35F2A (RuntimeObject* ___obj0, const RuntimeMethod* method) ;
// System.Void System.Threading.Monitor::Exit(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Exit_m25A154960F91391E10E4CDA245ECDF4BA94D56A9 (RuntimeObject* ___obj0, const RuntimeMethod* method) ;
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Enter_m00506757392936AA62DBE2C5FFBEE69EE920C4D4 (RuntimeObject* ___obj0, bool* ___lockTaken1, const RuntimeMethod* method) ;
// System.Runtime.InteropServices.HandleRef Firebase.Storage.FirebaseStorageInternal::getCPtr(Firebase.Storage.FirebaseStorageInternal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F FirebaseStorageInternal_getCPtr_mFB281328FEF2F13C113A67C221574881449C34E3 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* ___obj0, const RuntimeMethod* method) ;
// System.IntPtr System.Runtime.InteropServices.HandleRef::get_Handle()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR intptr_t HandleRef_get_Handle_m2055005E349E895499E1B3B826C89228FFAC4C17_inline (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F* __this, const RuntimeMethod* method) ;
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntPtr_op_Inequality_m2F715312CBFCE7E1A81D0689F68B97218E37E5D1 (intptr_t ___value10, intptr_t ___value21, const RuntimeMethod* method) ;
// System.Void Firebase.FirebaseApp::remove_AppDisposed(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseApp_remove_AppDisposed_mAAF77EA50314A467CBB4481448C72FA9B7173289 (FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* __this, EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82* ___value0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.Storage.FirebaseStorage>::Remove(TKey)
inline bool Dictionary_2_Remove_mEEC7F27A9DD6A1AEC6417F54868EAFEFA86452A6 (Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044*, String_t*, const RuntimeMethod*))Dictionary_2_Remove_m5C7C45E75D951A75843F3F7AADD56ECD64F6BC86_gshared)(__this, ___key0, method);
}
// System.Void Firebase.Storage.FirebaseStorageInternal::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorageInternal_Dispose_m8BE167067EB4C1FBF823331DE520C441E5AF0B1F (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, const RuntimeMethod* method) ;
// System.Void Firebase.Platform.FirebaseLogger::LogMessage(Firebase.Platform.PlatformLogLevel,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseLogger_LogMessage_mD75A87D37AD77AC7BF0463C48DDD46E7901B0106 (int32_t ___logLevel0, String_t* ___message1, const RuntimeMethod* method) ;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntPtr_op_Equality_m73759B51FE326460AC87A0E386480226EF2FABED (intptr_t ___value10, intptr_t ___value21, const RuntimeMethod* method) ;
// System.Void System.NullReferenceException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NullReferenceException__ctor_mD26D62094A5E49C18D817817E17FDFBC1D3BD752 (NullReferenceException_tBDE63A6D24569B964908408389070C6A9F5005BB* __this, const RuntimeMethod* method) ;
// Firebase.FirebaseApp Firebase.FirebaseApp::get_DefaultInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* FirebaseApp_get_DefaultInstance_mCA6FC0DE0B25880FC6ACEAD5585ED84407690C61 (const RuntimeMethod* method) ;
// Firebase.Storage.FirebaseStorage Firebase.Storage.FirebaseStorage::GetInstance(Firebase.FirebaseApp,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* FirebaseStorage_GetInstance_m38173F3B91A453ABF925BC906F07E10C205BDFBE (FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* ___app0, String_t* ___url1, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.FirebaseStorage::ThrowIfNull()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorage_ThrowIfNull_mD51129BAA96196E2F019C3F8402FC21A928CD4EA (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, const RuntimeMethod* method) ;
// Firebase.Storage.StorageReferenceInternal Firebase.Storage.FirebaseStorageInternal::GetReference()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* FirebaseStorageInternal_GetReference_mAE4C3E5F79096BB4A0FF285DAEF8B89E98A12765 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageReference::.ctor(Firebase.Storage.FirebaseStorage,Firebase.Storage.StorageReferenceInternal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageReference__ctor_m0D0BBD2A92AFDEF8268F669CC8D1D3E74E384C2E (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* ___storage0, StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* ___storageReferenceInternal1, const RuntimeMethod* method) ;
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A (String_t* ___value0, const RuntimeMethod* method) ;
// Firebase.AppOptions Firebase.FirebaseApp::get_Options()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AppOptions_tC85C010A614E35ED5C64709D909D4525D9DE6D09* FirebaseApp_get_Options_mDD04F28C88747DC1332D1A9363E7B6F6FCC7A12E (FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* __this, const RuntimeMethod* method) ;
// System.String Firebase.AppOptions::get_StorageBucket()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* AppOptions_get_StorageBucket_m93625BCEE94287655E69A3ED0D145E64F66278A4_inline (AppOptions_tC85C010A614E35ED5C64709D909D4525D9DE6D09* __this, const RuntimeMethod* method) ;
// Firebase.Storage.FirebaseStorage Firebase.Storage.FirebaseStorage::GetInstanceInternal(Firebase.FirebaseApp,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* FirebaseStorage_GetInstanceInternal_m97042020C5FF0EF33F2883A26DD3E8DCE511D553 (FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* ___app0, String_t* ___bucketUrl1, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.Storage.FirebaseStorage>::TryGetValue(TKey,TValue&)
inline bool Dictionary_2_TryGetValue_mDB3CF5FD5D7F94A7EFC62BB1BE6627BB9AF9805E (Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* __this, String_t* ___key0, FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1** ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044*, String_t*, FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1**, const RuntimeMethod*))Dictionary_2_TryGetValue_mD15380A4ED7CDEE99EA45881577D26BA9CE1B849_gshared)(__this, ___key0, ___value1, method);
}
// Firebase.Storage.FirebaseStorageInternal Firebase.Storage.FirebaseStorageInternal::GetInstanceInternal(Firebase.FirebaseApp,System.String,Firebase.InitResult&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* FirebaseStorageInternal_GetInstanceInternal_m671B163ECAF2D5B145D295BF2946CA42A5653F55 (FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* ___app0, String_t* ___url1, int32_t* ___init_result_out2, const RuntimeMethod* method) ;
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465 (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* __this, String_t* ___message0, const RuntimeMethod* method) ;
// Firebase.Storage.FirebaseStorage Firebase.Storage.FirebaseStorage::FindByKey(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* FirebaseStorage_FindByKey_m067B20827B3E57814ED2F4C3CDA8019EE3D2A520 (String_t* ___instanceKey0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.FirebaseStorage::.ctor(Firebase.Storage.FirebaseStorageInternal,Firebase.FirebaseApp)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorage__ctor_mD53DDAA91E21368E464FF0BA1E1B171D896C976A (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* ___storage0, FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* ___app1, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Storage.FirebaseStorage>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_mB06B57C1F6085A6AEA2C437FEE9AD90EEC997C71 (Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* __this, String_t* ___key0, FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044*, String_t*, FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1*, const RuntimeMethod*))Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Storage.FirebaseStorage>::.ctor()
inline void Dictionary_2__ctor_mBCEC6CB903889BCAF4BAAC48E21CB2EDB8A4825C (Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.Void Firebase.Storage.StorageReference::set_Internal(Firebase.Storage.StorageReferenceInternal)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StorageReference_set_Internal_m402EC4C5726D40B65F6AC62A605948E4FCBF72A2_inline (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* ___value0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageReference::set_Logger(Firebase.Storage.Internal.ModuleLogger)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StorageReference_set_Logger_mFA430AB56C163BC0EC0F4F69201D440BBF590789_inline (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___value0, const RuntimeMethod* method) ;
// Firebase.Storage.StorageReferenceInternal Firebase.Storage.StorageReference::get_Internal()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* StorageReference_get_Internal_mDD8DFC623E89E8F540685EC02832A1B31DD4B7AF_inline (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, const RuntimeMethod* method) ;
// System.String Firebase.Storage.StorageReferenceInternal::get_FullPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageReferenceInternal_get_FullPath_mE93027973F851741548644FA6CE90911FB02FC42 (StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* __this, const RuntimeMethod* method) ;
// System.String Firebase.Storage.StorageReferenceInternal::get_Bucket()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageReferenceInternal_get_Bucket_m7C7737DC9A4D22049E4C6338515E229935A68BF5 (StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* __this, const RuntimeMethod* method) ;
// Firebase.Storage.StorageReferenceInternal Firebase.Storage.StorageReferenceInternal::Child(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* StorageReferenceInternal_Child_mB1AFC34728CA3C083BC6FBC74AF0CE9CCE2DC375 (StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* __this, String_t* ___path0, const RuntimeMethod* method) ;
// System.String Firebase.Storage.StorageReference::get_Bucket()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageReference_get_Bucket_m17BFF3774E2064931D6673E7FBB05299611BDB22 (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, const RuntimeMethod* method) ;
// System.String Firebase.Storage.StorageReference::get_Path()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageReference_get_Path_m23006656BF5776DAC29630E641369A740F333C27 (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, const RuntimeMethod* method) ;
// System.Boolean System.String::Equals(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Equals_mCD5F35DEDCAFE51ACD4E033726FC2EF8DF7E9B4D (String_t* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.Void System.Runtime.InteropServices.HandleRef::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HandleRef__ctor_m0298D55E5F35F77B6A6CCA75C8E828C3F3127DE7 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F* __this, RuntimeObject* ___wrapper0, intptr_t ___handle1, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE::delete_StorageReferenceInternal(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageInternalPINVOKE_delete_StorageReferenceInternal_m6A2286A3920232B4EB017D422E4B2208BD8039E5 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) ;
// System.IntPtr Firebase.Storage.StorageInternalPINVOKE::StorageReferenceInternal_Child__SWIG_0(System.Runtime.InteropServices.HandleRef,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t StorageInternalPINVOKE_StorageReferenceInternal_Child__SWIG_0_m488BF8D5FD204F98A2CBF749913451ACC5DF5432 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, String_t* ___jarg21, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageReferenceInternal::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageReferenceInternal__ctor_m7C41AA5A3F5475ECA398D40758FF6DA37718B677 (StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method) ;
// System.Boolean Firebase.AppUtilPINVOKE/SWIGPendingException::get_Pending()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SWIGPendingException_get_Pending_m0BEE83B2B2559DE231EE2A87DF09649355775538 (const RuntimeMethod* method) ;
// System.Exception Firebase.AppUtilPINVOKE/SWIGPendingException::Retrieve()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t* SWIGPendingException_Retrieve_m5C29154CE762C145ECCBA0E7A37F5579366647BF (const RuntimeMethod* method) ;
// System.String Firebase.Storage.StorageInternalPINVOKE::StorageReferenceInternal_Bucket_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageInternalPINVOKE_StorageReferenceInternal_Bucket_get_m2ECA8DF098F05A5CFB796E7C650FAB954511A335 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) ;
// System.String Firebase.Storage.StorageInternalPINVOKE::StorageReferenceInternal_FullPath_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageInternalPINVOKE_StorageReferenceInternal_FullPath_get_m91D0E407E4D79C30F49C0B9B729906E25F942EAD (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.FirebaseStorageInternal::ReleaseReferenceInternal(Firebase.Storage.FirebaseStorageInternal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorageInternal_ReleaseReferenceInternal_m1C1D57F61F7501AC8252D78E924A6CA68B2FD3F7 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* ___instance0, const RuntimeMethod* method) ;
// Firebase.FirebaseApp Firebase.Storage.FirebaseStorageInternal::get_App()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* FirebaseStorageInternal_get_App_mA92F711407E7D883382645FCF479370018821AF3 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, const RuntimeMethod* method) ;
// System.String Firebase.FirebaseApp::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseApp_get_Name_m89C11F96726C8E4FD3CCAE04A5DC3129F7CD975E (FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method) ;
// System.IntPtr Firebase.Storage.StorageInternalPINVOKE::FirebaseStorageInternal_GetReference__SWIG_0(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t StorageInternalPINVOKE_FirebaseStorageInternal_GetReference__SWIG_0_mCC149C08BBFE6842916E89ED4FFD0D194A127609 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) ;
// System.Runtime.InteropServices.HandleRef Firebase.FirebaseApp::getCPtr(Firebase.FirebaseApp)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F FirebaseApp_getCPtr_mCF6551417C0F1D98798ED7810553EBD977381D16 (FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* ___obj0, const RuntimeMethod* method) ;
// System.IntPtr Firebase.Storage.StorageInternalPINVOKE::FirebaseStorageInternal_GetInstanceInternal(System.Runtime.InteropServices.HandleRef,System.String,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t StorageInternalPINVOKE_FirebaseStorageInternal_GetInstanceInternal_mE552EFD865D6FD1D48C7F76E33820E26BF1843F7 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, String_t* ___jarg21, int32_t* ___jarg32, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.FirebaseStorageInternal::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorageInternal__ctor_mB8F9788F57268947777E0C442C9C2BF3370CFE6E (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE::FirebaseStorageInternal_ReleaseReferenceInternal(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageInternalPINVOKE_FirebaseStorageInternal_ReleaseReferenceInternal_m9D43F4BB2235C7C1732D860C531B9BC13A5C5709 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) ;
// System.IntPtr Firebase.Storage.StorageInternalPINVOKE::FirebaseStorageInternal_App_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t StorageInternalPINVOKE_FirebaseStorageInternal_App_get_m666DC84501145FC523A16322B406F38AB58075AC (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) ;
// System.Void Firebase.FirebaseApp::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseApp__ctor_mC539AF748C2E16CD3B7820D6039B9A29DBDF908C (FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method) ;
// System.String Firebase.Storage.StorageInternalPINVOKE::FirebaseStorageInternal_Url_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageInternalPINVOKE_FirebaseStorageInternal_Url_get_mD62453262CBF244BC8AA3119C7A166FED10E1517 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper__ctor_mAD73726452D2B9A7610ED82C4B6868D0C424C849 (SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149* __this, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringHelper__ctor_m742C1F69A35C651EA7707D33CFDCF5C602DD81E3 (SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004* __this, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingApplicationException_m3389DA14B6470D215F411A6AB64161049D976B92 (String_t* ___message0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArithmeticException_mDA9940C49B02A145257997D6E29939B520DECE30 (String_t* ___message0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingDivideByZeroException_m2AC76C31521184D3E9CEA398D2304368C1C3F245 (String_t* ___message0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m9444EB341D36B5E18F7DD12453881B4E5D527B77 (String_t* ___message0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingInvalidCastException_m56DA5D72BDA177C1E2E9FA830A03F6A315064907 (String_t* ___message0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingInvalidOperationException_m8319A7B285528224F0D94E93683A03F8B2A1CF61 (String_t* ___message0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingIOException_m1E8C16376B2B3701BAC90728915AAF17EC843685 (String_t* ___message0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingNullReferenceException_m5357E1A78F179FAE6A3EC032052906188478E50D (String_t* ___message0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingOutOfMemoryException_mCED02F27DC5E0EDC86EAA55784361567D1D01CA6 (String_t* ___message0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingOverflowException_mCA646C70DE90380B08954D135018002CE4A99E86 (String_t* ___message0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingSystemException_mE0F156F2311B8F1ECDFE4267B73A0CE00DA9A01E (String_t* ___message0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArgumentException_m59C2895E8334A034F0C59E34A3C14F39340478CC (String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArgumentNullException_m4B7CABD3A4D1075F6323D8EB69F7E6E8667665F8 (String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mB01500F82308A09A74F016F7671A8E282A105C55 (String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method) ;
// System.Exception Firebase.Storage.StorageInternalPINVOKE/SWIGPendingException::Retrieve()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t* SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45 (const RuntimeMethod* method) ;
// System.Void System.ApplicationException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ApplicationException__ctor_m924E77609BAFA0595453363EB8B7BCCBA03B32DD (ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A* __this, String_t* ___message0, Exception_t* ___innerException1, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGPendingException::Set(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D (Exception_t* ___e0, const RuntimeMethod* method) ;
// System.Void System.ArithmeticException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArithmeticException__ctor_m880D58CC9B6CD2F0E867298BA748343216D89A8B (ArithmeticException_t07E77822D0007642BC8959A671E70D1F33C84FEA* __this, String_t* ___message0, Exception_t* ___innerException1, const RuntimeMethod* method) ;
// System.Void System.DivideByZeroException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DivideByZeroException__ctor_mE15900048AEEE9B66A4DD9F2ACAC4448D85D4F23 (DivideByZeroException_tC43171E50A38F5CD4242D258D0B0C6B74898C279* __this, String_t* ___message0, Exception_t* ___innerException1, const RuntimeMethod* method) ;
// System.Void System.IndexOutOfRangeException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndexOutOfRangeException__ctor_m390691571A232F79022C84ED002FDEF8974255E1 (IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82* __this, String_t* ___message0, Exception_t* ___innerException1, const RuntimeMethod* method) ;
// System.Void System.InvalidCastException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidCastException__ctor_m40BCFD6C1C79DE81191B829AF71BEB590E300396 (InvalidCastException_t47FC62F21A3937E814D20381DDACEF240E95AC2E* __this, String_t* ___message0, Exception_t* ___innerException1, const RuntimeMethod* method) ;
// System.Void System.InvalidOperationException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_m63F5561BE647F655D22C8289E53A5D3A2196B668 (InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB* __this, String_t* ___message0, Exception_t* ___innerException1, const RuntimeMethod* method) ;
// System.Void System.IO.IOException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOException__ctor_mFA9F39D1AF43FBC40BFA68A7BFE07852D1EF8B1B (IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910* __this, String_t* ___message0, Exception_t* ___innerException1, const RuntimeMethod* method) ;
// System.Void System.NullReferenceException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NullReferenceException__ctor_mD00D7FE987C285C8DB23883700F44BC0025F55EF (NullReferenceException_tBDE63A6D24569B964908408389070C6A9F5005BB* __this, String_t* ___message0, Exception_t* ___innerException1, const RuntimeMethod* method) ;
// System.Void System.OutOfMemoryException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutOfMemoryException__ctor_mC60E0CF8E50CA43F0518570ACC051F6BA8A1D1F2 (OutOfMemoryException_tE6DC2F937EC4A8699271D5151C4DF83BDE99EE7F* __this, String_t* ___message0, Exception_t* ___innerException1, const RuntimeMethod* method) ;
// System.Void System.OverflowException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OverflowException__ctor_mB00A07CDB7E5230B8D2BB31696E63F3CB1C36EF9 (OverflowException_t6F6AD8CACE20C37F701C05B373A215C4802FAB0C* __this, String_t* ___message0, Exception_t* ___innerException1, const RuntimeMethod* method) ;
// System.Void System.SystemException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SystemException__ctor_m0FC84CACD2A5D66222998AA601A5C41CEC36A611 (SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295* __this, String_t* ___message0, Exception_t* ___innerException1, const RuntimeMethod* method) ;
// System.Void System.ArgumentException::.ctor(System.String,System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m790F28810569425B0503056EF1A9CDDF9AFBB3F0 (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* __this, String_t* ___message0, String_t* ___paramName1, Exception_t* ___innerException2, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method) ;
// System.Void System.ArgumentNullException::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m6D9C7B47EA708382838B264BA02EBB7576DFA155 (ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129* __this, String_t* ___paramName0, String_t* ___message1, const RuntimeMethod* method) ;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_mE5B2755F0BEA043CACF915D5CE140859EE58FA66 (ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F* __this, String_t* ___paramName0, String_t* ___message1, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionDelegate__ctor_m86E2FD1C496126EFCE130E23D2CD3191BBA96FB7 (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionArgumentDelegate__ctor_m69BA8206F7E1253E502E36629F0AD6E37282EE6A (ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_StorageInternal(Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_StorageInternal_mC4703B7B2BCFF2FD96F4400F20636806BE454B4B (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___applicationDelegate0, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___arithmeticDelegate1, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___divideByZeroDelegate2, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___indexOutOfRangeDelegate3, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___invalidCastDelegate4, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___invalidOperationDelegate5, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___ioDelegate6, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___nullReferenceDelegate7, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___outOfMemoryDelegate8, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___overflowDelegate9, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___systemExceptionDelegate10, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_StorageInternal(Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_StorageInternal_mB84CA4E78B9898DA55BD54308320D9DAEE71D6C2 (ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* ___argumentDelegate0, ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* ___argumentNullDelegate1, ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* ___argumentOutOfRangeDelegate2, const RuntimeMethod* method) ;
// System.String Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper::CreateString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SWIGStringHelper_CreateString_m403C46AC9193D0E161E7752F5C6D16DE4F670596 (String_t* ___cString0, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringDelegate__ctor_mC5F68D7CA10A84812D4C8AF241FC7628F3EB0AD4 (SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_StorageInternal(Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper/SWIGStringDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringHelper_SWIGRegisterStringCallback_StorageInternal_mB88DFA747A335290130176E9A96A87A18A966CBB (SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* ___stringDelegate0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<Firebase.Storage.Internal.ModuleLogger>::.ctor()
inline void List_1__ctor_mF9FFC2B904A1557D19170DC33F3A326E18082C76 (List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void System.WeakReference::.ctor(System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeakReference__ctor_m8085B7DB432EB4B11F2FFDB543B3F1D05D4A8D99 (WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* __this, RuntimeObject* ___target0, bool ___trackResurrection1, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.WeakReference>::Add(T)
inline void List_1_Add_m25F4DF4DE179873ACDA24F3C314479DDBCCC0BDD_inline (List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9* __this, WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9*, WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<Firebase.Storage.Internal.ModuleLogger>::Add(T)
inline void List_1_Add_m14B2E734DD76D26DE4F1C83D6EB500BE400DE1B9_inline (List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE* __this, ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE*, ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.WeakReference>::GetEnumerator()
inline Enumerator_tA83B52184A87D2620C50E08DA33C36DB659322C7 List_1_GetEnumerator_m7F0C23273635833A4D046A87EF3F3731C79A22B7 (List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tA83B52184A87D2620C50E08DA33C36DB659322C7 (*) (List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9*, const RuntimeMethod*))List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.WeakReference>::Dispose()
inline void Enumerator_Dispose_m09AD6CD926C4F2DA33BBE74EA133F80BC1F7AC46 (Enumerator_tA83B52184A87D2620C50E08DA33C36DB659322C7* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tA83B52184A87D2620C50E08DA33C36DB659322C7*, const RuntimeMethod*))Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared)(__this, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.WeakReference>::get_Current()
inline WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* Enumerator_get_Current_m06D75C6DF4330942019CEC566CE1F77C08873BB9_inline (Enumerator_tA83B52184A87D2620C50E08DA33C36DB659322C7* __this, const RuntimeMethod* method)
{
	return ((  WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* (*) (Enumerator_tA83B52184A87D2620C50E08DA33C36DB659322C7*, const RuntimeMethod*))Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline)(__this, method);
}
// System.Object Firebase.FirebaseApp::WeakReferenceGetTarget(System.WeakReference)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FirebaseApp_WeakReferenceGetTarget_m48BCAA7BDF44B90AFC6B68BE061F879F21BDEA5D (WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* ___weakReference0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<System.WeakReference>::Remove(T)
inline bool List_1_Remove_mE10942BAF61F258192575C0A6044AADF5D08BB5B (List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9* __this, WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9*, WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E*, const RuntimeMethod*))List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.WeakReference>::MoveNext()
inline bool Enumerator_MoveNext_m6E9D8FDB919C8320D80F7597574FFE9F6D0D88D3 (Enumerator_tA83B52184A87D2620C50E08DA33C36DB659322C7* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tA83B52184A87D2620C50E08DA33C36DB659322C7*, const RuntimeMethod*))Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1<Firebase.Storage.Internal.ModuleLogger>::Remove(T)
inline bool List_1_Remove_m7E4BCB7F0652FE05E987A6B3EF0D4F35AA775886 (List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE* __this, ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE*, ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*, const RuntimeMethod*))List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared)(__this, ___item0, method);
}
// Firebase.LogLevel Firebase.Storage.Internal.ModuleLogger::get_Level()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ModuleLogger_get_Level_m0AEE610FD8DA98169E801F769BC567589DE54791 (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* __this, const RuntimeMethod* method) ;
// System.Void Firebase.LogUtil::LogMessage(Firebase.LogLevel,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LogUtil_LogMessage_m59195C58FF0FC63681CED394EB6618F03A25B3B4 (int32_t ___logLevel0, String_t* ___message1, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.WeakReference>::.ctor()
inline void List_1__ctor_m63EDE1AEAA4A4FCAB6203FB0C262E31C79D24F9E (List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Storage_CSharp_delete_StorageReferenceInternal(void*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Storage_CSharp_StorageReferenceInternal_Child__SWIG_0(void*, char*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
IL2CPP_EXTERN_C char* DEFAULT_CALL Firebase_Storage_CSharp_StorageReferenceInternal_Bucket_get(void*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
IL2CPP_EXTERN_C char* DEFAULT_CALL Firebase_Storage_CSharp_StorageReferenceInternal_FullPath_get(void*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Storage_CSharp_FirebaseStorageInternal_GetReference__SWIG_0(void*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Storage_CSharp_FirebaseStorageInternal_GetInstanceInternal(void*, char*, int32_t*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Storage_CSharp_FirebaseStorageInternal_ReleaseReferenceInternal(void*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Storage_CSharp_FirebaseStorageInternal_App_get(void*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
IL2CPP_EXTERN_C char* DEFAULT_CALL Firebase_Storage_CSharp_FirebaseStorageInternal_Url_get(void*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
IL2CPP_EXTERN_C void DEFAULT_CALL SWIGRegisterExceptionCallbacks_StorageInternal(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
IL2CPP_EXTERN_C void DEFAULT_CALL SWIGRegisterExceptionArgumentCallbacks_StorageInternal(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
IL2CPP_EXTERN_C void DEFAULT_CALL SWIGRegisterStringCallback_StorageInternal(Il2CppMethodPointer);
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Storage.FirebaseStorage::.ctor(Firebase.Storage.FirebaseStorageInternal,Firebase.FirebaseApp)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorage__ctor_mD53DDAA91E21368E464FF0BA1E1B171D896C976A (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* ___storage0, FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* ___app1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseStorage_OnAppDisposed_mD8FEA28499BCE475237BF7945AA3BCD6C7A2D878_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2735032CF824904F31866A3DDD0204F0B7FE22CD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB703A2BE7C7C622C41AA27448E688623872F3623);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_0 = ___app1;
		__this->___firebaseApp_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___firebaseApp_2), (void*)L_0);
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_1 = __this->___firebaseApp_2;
		EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82* L_2 = (EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82*)il2cpp_codegen_object_new(EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		EventHandler__ctor_m95444CE8D5A6F1AFC9793866C3FE884E732DCEB2(L_2, __this, (intptr_t)((void*)FirebaseStorage_OnAppDisposed_mD8FEA28499BCE475237BF7945AA3BCD6C7A2D878_RuntimeMethod_var), NULL);
		NullCheck(L_1);
		FirebaseApp_add_AppDisposed_m849DD816EFE8D669DBFA139254D5E3C4D8C78F85(L_1, L_2, NULL);
		FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_3 = ___storage0;
		__this->___storageInternal_1 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___storageInternal_1), (void*)L_3);
		il2cpp_codegen_runtime_class_init_inline(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_4 = ((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___logger_4;
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_5 = (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*)il2cpp_codegen_object_new(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		ModuleLogger__ctor_m64B63C3ABE700A1F9BA947FFEE57605EBB4C5850(L_5, L_4, NULL);
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_6 = L_5;
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_7 = ((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___logger_4;
		NullCheck(L_7);
		String_t* L_8;
		L_8 = ModuleLogger_get_Tag_m9F24A98C3F04C66363235C1C339DF611D346378A(L_7, NULL);
		FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_9 = __this->___storageInternal_1;
		NullCheck(L_9);
		String_t* L_10;
		L_10 = FirebaseStorageInternal_get_Url_mB62A11FAF14E963FF2A843A45705CEE209636744(L_9, NULL);
		String_t* L_11;
		L_11 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(_stringLiteral2735032CF824904F31866A3DDD0204F0B7FE22CD, L_8, L_10, NULL);
		NullCheck(L_6);
		ModuleLogger_set_Tag_m93B4431C1F0B91205E7CC9B7EDACACA49C97E42F(L_6, L_11, NULL);
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_12 = L_6;
		il2cpp_codegen_runtime_class_init_inline(FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		int32_t L_13;
		L_13 = FirebaseApp_get_LogLevel_m50774F8027C8BF6EB481CBFCCCFC266863392D83(NULL);
		NullCheck(L_12);
		ModuleLogger_set_Level_m1DC27F4C2EC96609D9050B868F8B71467AB4A916(L_12, L_13, NULL);
		FirebaseStorage_set_Logger_m19C7C11321677C2320D1D066CCBBB6F37B3E00F3_inline(__this, L_12, NULL);
		FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_14 = __this->___storageInternal_1;
		NullCheck(L_14);
		FirebaseStorageInternal_SetSwigCMemOwn_mE268A0AD6F3528EC65D486EC1F0B228D84D96629(L_14, (bool)1, NULL);
		FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_15 = __this->___storageInternal_1;
		NullCheck(L_15);
		String_t* L_16;
		L_16 = FirebaseStorageInternal_get_InstanceKey_m9F8ABBA175A15B51661EF6112C1BF06CAD267F4C(L_15, NULL);
		__this->___instanceKey_3 = L_16;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___instanceKey_3), (void*)L_16);
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_17;
		L_17 = FirebaseStorage_get_Logger_m47F60ACF93F3A0BA8C660B1B566F61BBF30BE391_inline(__this, NULL);
		String_t* L_18 = __this->___instanceKey_3;
		String_t* L_19;
		L_19 = String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30(_stringLiteralB703A2BE7C7C622C41AA27448E688623872F3623, L_18, NULL);
		NullCheck(L_17);
		ModuleLogger_LogMessage_m568B4169C0A0FA2CA4FF4E6E076870F07131DA3B(L_17, 1, L_19, NULL);
		return;
	}
}
// System.Void Firebase.Storage.FirebaseStorage::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorage_Finalize_mE086B6BB55BD1E3AFDCDAC5A5BF3AFB657D42634 (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7624E265BB383FB22ED234A2DC36A755B34594EC);
		s_Il2CppMethodInitialized = true;
	}
	{
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0028:
			{// begin finally (depth: 1)
				Object_Finalize_mC98C96301CCABFE00F1A7EF8E15DF507CACD42B2(__this, NULL);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			FirebaseStorage_Dispose_mF3522116DDF143F6DFAF767D3CBF3092FADA4A6F(__this, NULL);
			ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_0;
			L_0 = FirebaseStorage_get_Logger_m47F60ACF93F3A0BA8C660B1B566F61BBF30BE391_inline(__this, NULL);
			String_t* L_1 = __this->___instanceKey_3;
			String_t* L_2;
			L_2 = String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30(_stringLiteral7624E265BB383FB22ED234A2DC36A755B34594EC, L_1, NULL);
			NullCheck(L_0);
			ModuleLogger_LogMessage_m568B4169C0A0FA2CA4FF4E6E076870F07131DA3B(L_0, 1, L_2, NULL);
			goto IL_0030;
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0030:
	{
		return;
	}
}
// System.Void Firebase.Storage.FirebaseStorage::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorage_Dispose_mF3522116DDF143F6DFAF767D3CBF3092FADA4A6F (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Remove_mEEC7F27A9DD6A1AEC6417F54868EAFEFA86452A6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseStorage_OnAppDisposed_mD8FEA28499BCE475237BF7945AA3BCD6C7A2D878_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GC_t920F9CF6EBB7C787E5010A4352E1B587F356DC58_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F V_3;
	memset((&V_3), 0, sizeof(V_3));
	int32_t G_B4_0 = 0;
	{
		il2cpp_codegen_runtime_class_init_inline(GC_t920F9CF6EBB7C787E5010A4352E1B587F356DC58_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m3352E2F2119EB46913B51B7AAE2F217C63C35F2A(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_0 = ((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___storageByInstanceKey_0;
		V_0 = L_0;
		V_1 = (bool)0;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0087:
			{// begin finally (depth: 1)
				{
					bool L_1 = V_1;
					if (!L_1)
					{
						goto IL_0091;
					}
				}
				{
					Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_2 = V_0;
					Monitor_Exit_m25A154960F91391E10E4CDA245ECDF4BA94D56A9(L_2, NULL);
				}

IL_0091:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_3 = V_0;
				Monitor_Enter_m00506757392936AA62DBE2C5FFBEE69EE920C4D4(L_3, (&V_1), NULL);
				FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_4 = __this->___storageInternal_1;
				if (!L_4)
				{
					goto IL_0041_1;
				}
			}
			{
				FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_5 = __this->___storageInternal_1;
				HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_6;
				L_6 = FirebaseStorageInternal_getCPtr_mFB281328FEF2F13C113A67C221574881449C34E3(L_5, NULL);
				V_3 = L_6;
				intptr_t L_7;
				L_7 = HandleRef_get_Handle_m2055005E349E895499E1B3B826C89228FFAC4C17_inline((&V_3), NULL);
				bool L_8;
				L_8 = IntPtr_op_Inequality_m2F715312CBFCE7E1A81D0689F68B97218E37E5D1(L_7, (0), NULL);
				G_B4_0 = ((int32_t)(L_8));
				goto IL_0042_1;
			}

IL_0041_1:
			{
				G_B4_0 = 0;
			}

IL_0042_1:
			{
				V_2 = (bool)G_B4_0;
				bool L_9 = V_2;
				if (!L_9)
				{
					goto IL_0084_1;
				}
			}
			{
				FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_10 = __this->___firebaseApp_2;
				EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82* L_11 = (EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82*)il2cpp_codegen_object_new(EventHandler_tC6323FD7E6163F965259C33D72612C0E5B9BAB82_il2cpp_TypeInfo_var);
				NullCheck(L_11);
				EventHandler__ctor_m95444CE8D5A6F1AFC9793866C3FE884E732DCEB2(L_11, __this, (intptr_t)((void*)FirebaseStorage_OnAppDisposed_mD8FEA28499BCE475237BF7945AA3BCD6C7A2D878_RuntimeMethod_var), NULL);
				NullCheck(L_10);
				FirebaseApp_remove_AppDisposed_mAAF77EA50314A467CBB4481448C72FA9B7173289(L_10, L_11, NULL);
				il2cpp_codegen_runtime_class_init_inline(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
				Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_12 = ((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___storageByInstanceKey_0;
				String_t* L_13 = __this->___instanceKey_3;
				NullCheck(L_12);
				bool L_14;
				L_14 = Dictionary_2_Remove_mEEC7F27A9DD6A1AEC6417F54868EAFEFA86452A6(L_12, L_13, Dictionary_2_Remove_mEEC7F27A9DD6A1AEC6417F54868EAFEFA86452A6_RuntimeMethod_var);
				FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_15 = __this->___storageInternal_1;
				NullCheck(L_15);
				FirebaseStorageInternal_Dispose_m8BE167067EB4C1FBF823331DE520C441E5AF0B1F(L_15, NULL);
				__this->___storageInternal_1 = (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145*)NULL;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___storageInternal_1), (void*)(FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145*)NULL);
			}

IL_0084_1:
			{
				goto IL_0092;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0092:
	{
		return;
	}
}
// System.Void Firebase.Storage.FirebaseStorage::OnAppDisposed(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorage_OnAppDisposed_mD8FEA28499BCE475237BF7945AA3BCD6C7A2D878 (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, RuntimeObject* ___sender0, EventArgs_t37273F03EAC87217701DD431B190FBD84AD7C377* ___eventArgs1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseLogger_t17574FB770AB2CB02AF3459B2EF9D27FC9C24F42_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE5ACD853F8874D5E90622ED276C4C96160520552);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(FirebaseLogger_t17574FB770AB2CB02AF3459B2EF9D27FC9C24F42_il2cpp_TypeInfo_var);
		FirebaseLogger_LogMessage_mD75A87D37AD77AC7BF0463C48DDD46E7901B0106(3, _stringLiteralE5ACD853F8874D5E90622ED276C4C96160520552, NULL);
		FirebaseStorage_Dispose_mF3522116DDF143F6DFAF767D3CBF3092FADA4A6F(__this, NULL);
		return;
	}
}
// System.Void Firebase.Storage.FirebaseStorage::ThrowIfNull()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorage_ThrowIfNull_mD51129BAA96196E2F019C3F8402FC21A928CD4EA (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t G_B3_0 = 0;
	{
		FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_0 = __this->___storageInternal_1;
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_1 = __this->___storageInternal_1;
		HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_2;
		L_2 = FirebaseStorageInternal_getCPtr_mFB281328FEF2F13C113A67C221574881449C34E3(L_1, NULL);
		V_1 = L_2;
		intptr_t L_3;
		L_3 = HandleRef_get_Handle_m2055005E349E895499E1B3B826C89228FFAC4C17_inline((&V_1), NULL);
		bool L_4;
		L_4 = IntPtr_op_Equality_m73759B51FE326460AC87A0E386480226EF2FABED(L_3, (0), NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0029;
	}

IL_0028:
	{
		G_B3_0 = 1;
	}

IL_0029:
	{
		V_0 = (bool)G_B3_0;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		NullReferenceException_tBDE63A6D24569B964908408389070C6A9F5005BB* L_6 = (NullReferenceException_tBDE63A6D24569B964908408389070C6A9F5005BB*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NullReferenceException_tBDE63A6D24569B964908408389070C6A9F5005BB_il2cpp_TypeInfo_var)));
		NullCheck(L_6);
		NullReferenceException__ctor_mD26D62094A5E49C18D817817E17FDFBC1D3BD752(L_6, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FirebaseStorage_ThrowIfNull_mD51129BAA96196E2F019C3F8402FC21A928CD4EA_RuntimeMethod_var)));
	}

IL_0034:
	{
		return;
	}
}
// Firebase.Storage.Internal.ModuleLogger Firebase.Storage.FirebaseStorage::get_Logger()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* FirebaseStorage_get_Logger_m47F60ACF93F3A0BA8C660B1B566F61BBF30BE391 (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, const RuntimeMethod* method) 
{
	{
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_0 = __this->___U3CLoggerU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Void Firebase.Storage.FirebaseStorage::set_Logger(Firebase.Storage.Internal.ModuleLogger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorage_set_Logger_m19C7C11321677C2320D1D066CCBBB6F37B3E00F3 (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___value0, const RuntimeMethod* method) 
{
	{
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_0 = ___value0;
		__this->___U3CLoggerU3Ek__BackingField_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CLoggerU3Ek__BackingField_5), (void*)L_0);
		return;
	}
}
// Firebase.Storage.FirebaseStorage Firebase.Storage.FirebaseStorage::get_DefaultInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* FirebaseStorage_get_DefaultInstance_m4A7686FCE928EBFABEA34406F9B61DCBBAB831FC (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* V_0 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_0;
		L_0 = FirebaseApp_get_DefaultInstance_mCA6FC0DE0B25880FC6ACEAD5585ED84407690C61(NULL);
		il2cpp_codegen_runtime_class_init_inline(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_1;
		L_1 = FirebaseStorage_GetInstance_m38173F3B91A453ABF925BC906F07E10C205BDFBE(L_0, (String_t*)NULL, NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_2 = V_0;
		return L_2;
	}
}
// Firebase.Storage.StorageReference Firebase.Storage.FirebaseStorage::get_RootReference()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* FirebaseStorage_get_RootReference_mE6E3B5CBF11792AE0D6B14C90BB1182EABEA41A0 (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* V_0 = NULL;
	{
		FirebaseStorage_ThrowIfNull_mD51129BAA96196E2F019C3F8402FC21A928CD4EA(__this, NULL);
		FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_0 = __this->___storageInternal_1;
		NullCheck(L_0);
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_1;
		L_1 = FirebaseStorageInternal_GetReference_mAE4C3E5F79096BB4A0FF285DAEF8B89E98A12765(L_0, NULL);
		StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* L_2 = (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019*)il2cpp_codegen_object_new(StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		StorageReference__ctor_m0D0BBD2A92AFDEF8268F669CC8D1D3E74E384C2E(L_2, __this, L_1, NULL);
		V_0 = L_2;
		goto IL_001c;
	}

IL_001c:
	{
		StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* L_3 = V_0;
		return L_3;
	}
}
// Firebase.Storage.FirebaseStorage Firebase.Storage.FirebaseStorage::GetInstance(Firebase.FirebaseApp,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* FirebaseStorage_GetInstance_m38173F3B91A453ABF925BC906F07E10C205BDFBE (FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* ___app0, String_t* ___url1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralABBB41CC11C60D234659F31874B9FB7B8DD1259D);
		s_Il2CppMethodInitialized = true;
	}
	FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* V_0 = NULL;
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* G_B5_0 = NULL;
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* G_B1_0 = NULL;
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* G_B3_0 = NULL;
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* G_B2_0 = NULL;
	String_t* G_B4_0 = NULL;
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* G_B4_1 = NULL;
	String_t* G_B6_0 = NULL;
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* G_B6_1 = NULL;
	{
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_0 = ___app0;
		String_t* L_1 = ___url1;
		bool L_2;
		L_2 = String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A(L_1, NULL);
		G_B1_0 = L_0;
		if (!L_2)
		{
			G_B5_0 = L_0;
			goto IL_0036;
		}
	}
	{
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_3 = ___app0;
		NullCheck(L_3);
		AppOptions_tC85C010A614E35ED5C64709D909D4525D9DE6D09* L_4;
		L_4 = FirebaseApp_get_Options_mDD04F28C88747DC1332D1A9363E7B6F6FCC7A12E(L_3, NULL);
		NullCheck(L_4);
		String_t* L_5;
		L_5 = AppOptions_get_StorageBucket_m93625BCEE94287655E69A3ED0D145E64F66278A4_inline(L_4, NULL);
		bool L_6;
		L_6 = String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A(L_5, NULL);
		G_B2_0 = G_B1_0;
		if (!L_6)
		{
			G_B3_0 = G_B1_0;
			goto IL_001f;
		}
	}
	{
		G_B4_0 = ((String_t*)(NULL));
		G_B4_1 = G_B2_0;
		goto IL_0034;
	}

IL_001f:
	{
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_7 = ___app0;
		NullCheck(L_7);
		AppOptions_tC85C010A614E35ED5C64709D909D4525D9DE6D09* L_8;
		L_8 = FirebaseApp_get_Options_mDD04F28C88747DC1332D1A9363E7B6F6FCC7A12E(L_7, NULL);
		NullCheck(L_8);
		String_t* L_9;
		L_9 = AppOptions_get_StorageBucket_m93625BCEE94287655E69A3ED0D145E64F66278A4_inline(L_8, NULL);
		String_t* L_10;
		L_10 = String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30(_stringLiteralABBB41CC11C60D234659F31874B9FB7B8DD1259D, L_9, NULL);
		G_B4_0 = L_10;
		G_B4_1 = G_B3_0;
	}

IL_0034:
	{
		G_B6_0 = G_B4_0;
		G_B6_1 = G_B4_1;
		goto IL_0037;
	}

IL_0036:
	{
		String_t* L_11 = ___url1;
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
	}

IL_0037:
	{
		il2cpp_codegen_runtime_class_init_inline(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_12;
		L_12 = FirebaseStorage_GetInstanceInternal_m97042020C5FF0EF33F2883A26DD3E8DCE511D553(G_B6_1, G_B6_0, NULL);
		V_0 = L_12;
		goto IL_003f;
	}

IL_003f:
	{
		FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_13 = V_0;
		return L_13;
	}
}
// Firebase.Storage.FirebaseStorage Firebase.Storage.FirebaseStorage::FindByKey(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* FirebaseStorage_FindByKey_m067B20827B3E57814ED2F4C3CDA8019EE3D2A520 (String_t* ___instanceKey0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Remove_mEEC7F27A9DD6A1AEC6417F54868EAFEFA86452A6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_mDB3CF5FD5D7F94A7EFC62BB1BE6627BB9AF9805E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* V_0 = NULL;
	bool V_1 = false;
	FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* V_5 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_0 = ((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___storageByInstanceKey_0;
		V_0 = L_0;
		V_1 = (bool)0;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0046:
			{// begin finally (depth: 1)
				{
					bool L_1 = V_1;
					if (!L_1)
					{
						goto IL_0050;
					}
				}
				{
					Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_2 = V_0;
					Monitor_Exit_m25A154960F91391E10E4CDA245ECDF4BA94D56A9(L_2, NULL);
				}

IL_0050:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_3 = V_0;
				Monitor_Enter_m00506757392936AA62DBE2C5FFBEE69EE920C4D4(L_3, (&V_1), NULL);
				V_2 = (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1*)NULL;
				il2cpp_codegen_runtime_class_init_inline(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
				Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_4 = ((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___storageByInstanceKey_0;
				String_t* L_5 = ___instanceKey0;
				NullCheck(L_4);
				bool L_6;
				L_6 = Dictionary_2_TryGetValue_mDB3CF5FD5D7F94A7EFC62BB1BE6627BB9AF9805E(L_4, L_5, (&V_2), Dictionary_2_TryGetValue_mDB3CF5FD5D7F94A7EFC62BB1BE6627BB9AF9805E_RuntimeMethod_var);
				V_3 = L_6;
				bool L_7 = V_3;
				if (!L_7)
				{
					goto IL_0043_1;
				}
			}
			{
				FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_8 = V_2;
				V_4 = (bool)((!(((RuntimeObject*)(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1*)L_8) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
				bool L_9 = V_4;
				if (!L_9)
				{
					goto IL_0036_1;
				}
			}
			{
				FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_10 = V_2;
				V_5 = L_10;
				goto IL_0056;
			}

IL_0036_1:
			{
				il2cpp_codegen_runtime_class_init_inline(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
				Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_11 = ((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___storageByInstanceKey_0;
				String_t* L_12 = ___instanceKey0;
				NullCheck(L_11);
				bool L_13;
				L_13 = Dictionary_2_Remove_mEEC7F27A9DD6A1AEC6417F54868EAFEFA86452A6(L_11, L_12, Dictionary_2_Remove_mEEC7F27A9DD6A1AEC6417F54868EAFEFA86452A6_RuntimeMethod_var);
			}

IL_0043_1:
			{
				goto IL_0051;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0051:
	{
		V_5 = (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1*)NULL;
		goto IL_0056;
	}

IL_0056:
	{
		FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_14 = V_5;
		return L_14;
	}
}
// Firebase.Storage.FirebaseStorage Firebase.Storage.FirebaseStorage::GetInstanceInternal(Firebase.FirebaseApp,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* FirebaseStorage_GetInstanceInternal_m97042020C5FF0EF33F2883A26DD3E8DCE511D553 (FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* ___app0, String_t* ___bucketUrl1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_mB06B57C1F6085A6AEA2C437FEE9AD90EEC997C71_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBD2C0B3974A906DFD731ADCD696F181498081EC5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* V_1 = NULL;
	FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* V_2 = NULL;
	ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A* V_3 = NULL;
	bool V_4 = false;
	bool V_5 = false;
	FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* V_6 = NULL;
	Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* V_7 = NULL;
	bool V_8 = false;
	String_t* V_9 = NULL;
	bool V_10 = false;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* G_B2_0 = NULL;
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* G_B1_0 = NULL;
	String_t* G_B8_0 = NULL;
	String_t* G_B8_1 = NULL;
	String_t* G_B7_0 = NULL;
	String_t* G_B7_1 = NULL;
	String_t* G_B12_0 = NULL;
	String_t* G_B12_1 = NULL;
	int32_t G_B12_2 = 0;
	ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* G_B12_3 = NULL;
	String_t* G_B11_0 = NULL;
	String_t* G_B11_1 = NULL;
	int32_t G_B11_2 = 0;
	ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* G_B11_3 = NULL;
	{
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_0 = ___app0;
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_2;
		L_2 = FirebaseApp_get_DefaultInstance_mCA6FC0DE0B25880FC6ACEAD5585ED84407690C61(NULL);
		G_B2_0 = L_2;
	}

IL_000b:
	{
		___app0 = G_B2_0;
		V_1 = (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145*)NULL;
	}
	try
	{// begin try (depth: 1)
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_3 = ___app0;
		String_t* L_4 = ___bucketUrl1;
		FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_5;
		L_5 = FirebaseStorageInternal_GetInstanceInternal_m671B163ECAF2D5B145D295BF2946CA42A5653F55(L_3, L_4, (&V_0), NULL);
		V_1 = L_5;
		goto IL_002b;
	}// end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_001d;
		}
		throw e;
	}

CATCH_001d:
	{// begin catch(System.ApplicationException)
		V_3 = ((ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A*)IL2CPP_GET_ACTIVE_EXCEPTION(ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A*));
		ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A* L_6 = V_3;
		NullCheck(L_6);
		String_t* L_7;
		L_7 = VirtualFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Exception::get_Message() */, L_6);
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_8 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)));
		NullCheck(L_8);
		ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465(L_8, L_7, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FirebaseStorage_GetInstanceInternal_m97042020C5FF0EF33F2883A26DD3E8DCE511D553_RuntimeMethod_var)));
	}// end catch (depth: 1)

IL_002b:
	{
		int32_t L_9 = V_0;
		V_4 = (bool)((!(((uint32_t)L_9) <= ((uint32_t)0)))? 1 : 0);
		bool L_10 = V_4;
		if (!L_10)
		{
			goto IL_0050;
		}
	}
	{
		String_t* L_11 = ___bucketUrl1;
		String_t* L_12 = L_11;
		G_B7_0 = L_12;
		G_B7_1 = ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral47B60EB9A7DD966F2D170292CD7E78CEE515C121));
		if (L_12)
		{
			G_B8_0 = L_12;
			G_B8_1 = ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral47B60EB9A7DD966F2D170292CD7E78CEE515C121));
			goto IL_0045;
		}
	}
	{
		G_B8_0 = ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709));
		G_B8_1 = G_B7_1;
	}

IL_0045:
	{
		String_t* L_13;
		L_13 = String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30(G_B8_1, G_B8_0, NULL);
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_14 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)));
		NullCheck(L_14);
		ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465(L_14, L_13, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FirebaseStorage_GetInstanceInternal_m97042020C5FF0EF33F2883A26DD3E8DCE511D553_RuntimeMethod_var)));
	}

IL_0050:
	{
		FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_15 = V_1;
		V_5 = (bool)((((RuntimeObject*)(FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145*)L_15) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
		bool L_16 = V_5;
		if (!L_16)
		{
			goto IL_0080;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_17 = ((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___logger_4;
		String_t* L_18 = ___bucketUrl1;
		String_t* L_19 = L_18;
		G_B11_0 = L_19;
		G_B11_1 = _stringLiteralBD2C0B3974A906DFD731ADCD696F181498081EC5;
		G_B11_2 = 4;
		G_B11_3 = L_17;
		if (L_19)
		{
			G_B12_0 = L_19;
			G_B12_1 = _stringLiteralBD2C0B3974A906DFD731ADCD696F181498081EC5;
			G_B12_2 = 4;
			G_B12_3 = L_17;
			goto IL_0070;
		}
	}
	{
		G_B12_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		G_B12_1 = G_B11_1;
		G_B12_2 = G_B11_2;
		G_B12_3 = G_B11_3;
	}

IL_0070:
	{
		String_t* L_20;
		L_20 = String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30(G_B12_1, G_B12_0, NULL);
		NullCheck(G_B12_3);
		ModuleLogger_LogMessage_m568B4169C0A0FA2CA4FF4E6E076870F07131DA3B(G_B12_3, G_B12_2, L_20, NULL);
		V_6 = (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1*)NULL;
		goto IL_00e1;
	}

IL_0080:
	{
		V_2 = (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1*)NULL;
		il2cpp_codegen_runtime_class_init_inline(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_21 = ((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___storageByInstanceKey_0;
		V_7 = L_21;
		V_8 = (bool)0;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00cf:
			{// begin finally (depth: 1)
				{
					bool L_22 = V_8;
					if (!L_22)
					{
						goto IL_00db;
					}
				}
				{
					Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_23 = V_7;
					Monitor_Exit_m25A154960F91391E10E4CDA245ECDF4BA94D56A9(L_23, NULL);
				}

IL_00db:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_24 = V_7;
				Monitor_Enter_m00506757392936AA62DBE2C5FFBEE69EE920C4D4(L_24, (&V_8), NULL);
				FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_25 = V_1;
				NullCheck(L_25);
				String_t* L_26;
				L_26 = FirebaseStorageInternal_get_InstanceKey_m9F8ABBA175A15B51661EF6112C1BF06CAD267F4C(L_25, NULL);
				V_9 = L_26;
				String_t* L_27 = V_9;
				il2cpp_codegen_runtime_class_init_inline(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
				FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_28;
				L_28 = FirebaseStorage_FindByKey_m067B20827B3E57814ED2F4C3CDA8019EE3D2A520(L_27, NULL);
				V_2 = L_28;
				FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_29 = V_2;
				V_10 = (bool)((!(((RuntimeObject*)(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1*)L_29) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
				bool L_30 = V_10;
				if (!L_30)
				{
					goto IL_00b6_1;
				}
			}
			{
				FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_31 = V_2;
				V_6 = L_31;
				goto IL_00e1;
			}

IL_00b6_1:
			{
				FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_32 = V_1;
				FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_33 = ___app0;
				FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_34 = (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1*)il2cpp_codegen_object_new(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
				NullCheck(L_34);
				FirebaseStorage__ctor_mD53DDAA91E21368E464FF0BA1E1B171D896C976A(L_34, L_32, L_33, NULL);
				V_2 = L_34;
				il2cpp_codegen_runtime_class_init_inline(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
				Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_35 = ((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___storageByInstanceKey_0;
				String_t* L_36 = V_9;
				FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_37 = V_2;
				NullCheck(L_35);
				Dictionary_2_set_Item_mB06B57C1F6085A6AEA2C437FEE9AD90EEC997C71(L_35, L_36, L_37, Dictionary_2_set_Item_mB06B57C1F6085A6AEA2C437FEE9AD90EEC997C71_RuntimeMethod_var);
				goto IL_00dc;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00dc:
	{
		FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_38 = V_2;
		V_6 = L_38;
		goto IL_00e1;
	}

IL_00e1:
	{
		FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_39 = V_6;
		return L_39;
	}
}
// System.Void Firebase.Storage.FirebaseStorage::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorage__cctor_m046976B0802465F9B416D6C209769D88810C6BAB (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mBCEC6CB903889BCAF4BAAC48E21CB2EDB8A4825C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAA9E04A4F0FAA2410217C8B3DD5093BC82D04A7E);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044* L_0 = (Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044*)il2cpp_codegen_object_new(Dictionary_2_t6B2D56F200C03AE40F453FB3EFF2F2200DA34044_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Dictionary_2__ctor_mBCEC6CB903889BCAF4BAAC48E21CB2EDB8A4825C(L_0, Dictionary_2__ctor_mBCEC6CB903889BCAF4BAAC48E21CB2EDB8A4825C_RuntimeMethod_var);
		((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___storageByInstanceKey_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___storageByInstanceKey_0), (void*)L_0);
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_1 = (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*)il2cpp_codegen_object_new(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		ModuleLogger__ctor_m64B63C3ABE700A1F9BA947FFEE57605EBB4C5850(L_1, (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*)NULL, NULL);
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_2 = L_1;
		NullCheck(L_2);
		ModuleLogger_set_Tag_m93B4431C1F0B91205E7CC9B7EDACACA49C97E42F(L_2, _stringLiteralAA9E04A4F0FAA2410217C8B3DD5093BC82D04A7E, NULL);
		((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___logger_4 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1_il2cpp_TypeInfo_var))->___logger_4), (void*)L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Storage.StorageReference::.ctor(Firebase.Storage.FirebaseStorage,Firebase.Storage.StorageReferenceInternal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageReference__ctor_m0D0BBD2A92AFDEF8268F669CC8D1D3E74E384C2E (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* ___storage0, StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* ___storageReferenceInternal1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1CB4C6D8E752BD4A976FB4BF3FD6276F3FF8A77A);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_0 = ___storage0;
		__this->___firebaseStorage_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___firebaseStorage_0), (void*)L_0);
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_1 = ___storageReferenceInternal1;
		StorageReference_set_Internal_m402EC4C5726D40B65F6AC62A605948E4FCBF72A2_inline(__this, L_1, NULL);
		FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_2 = __this->___firebaseStorage_0;
		NullCheck(L_2);
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_3;
		L_3 = FirebaseStorage_get_Logger_m47F60ACF93F3A0BA8C660B1B566F61BBF30BE391_inline(L_2, NULL);
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_4 = (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*)il2cpp_codegen_object_new(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		ModuleLogger__ctor_m64B63C3ABE700A1F9BA947FFEE57605EBB4C5850(L_4, L_3, NULL);
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_5 = L_4;
		String_t* L_6;
		L_6 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		String_t* L_7;
		L_7 = String_Format_m8C122B26BC5AA10E2550AECA16E57DAE10F07E30(_stringLiteral1CB4C6D8E752BD4A976FB4BF3FD6276F3FF8A77A, L_6, NULL);
		NullCheck(L_5);
		ModuleLogger_set_Tag_m93B4431C1F0B91205E7CC9B7EDACACA49C97E42F(L_5, L_7, NULL);
		StorageReference_set_Logger_mFA430AB56C163BC0EC0F4F69201D440BBF590789_inline(__this, L_5, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageReference::set_Logger(Firebase.Storage.Internal.ModuleLogger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageReference_set_Logger_mFA430AB56C163BC0EC0F4F69201D440BBF590789 (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___value0, const RuntimeMethod* method) 
{
	{
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_0 = ___value0;
		__this->___U3CLoggerU3Ek__BackingField_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CLoggerU3Ek__BackingField_1), (void*)L_0);
		return;
	}
}
// System.String Firebase.Storage.StorageReference::get_Path()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageReference_get_Path_m23006656BF5776DAC29630E641369A740F333C27 (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_0;
		L_0 = StorageReference_get_Internal_mDD8DFC623E89E8F540685EC02832A1B31DD4B7AF_inline(__this, NULL);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = StorageReferenceInternal_get_FullPath_mE93027973F851741548644FA6CE90911FB02FC42(L_0, NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.String Firebase.Storage.StorageReference::get_Bucket()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageReference_get_Bucket_m17BFF3774E2064931D6673E7FBB05299611BDB22 (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_0;
		L_0 = StorageReference_get_Internal_mDD8DFC623E89E8F540685EC02832A1B31DD4B7AF_inline(__this, NULL);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = StorageReferenceInternal_get_Bucket_m7C7737DC9A4D22049E4C6338515E229935A68BF5(L_0, NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// Firebase.Storage.StorageReference Firebase.Storage.StorageReference::Child(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* StorageReference_Child_m9F88459EAF9C20A744124271AB52DCE825FD0B68 (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, String_t* ___pathString0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* V_0 = NULL;
	{
		FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* L_0 = __this->___firebaseStorage_0;
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_1;
		L_1 = StorageReference_get_Internal_mDD8DFC623E89E8F540685EC02832A1B31DD4B7AF_inline(__this, NULL);
		String_t* L_2 = ___pathString0;
		NullCheck(L_1);
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_3;
		L_3 = StorageReferenceInternal_Child_mB1AFC34728CA3C083BC6FBC74AF0CE9CCE2DC375(L_1, L_2, NULL);
		StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* L_4 = (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019*)il2cpp_codegen_object_new(StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		StorageReference__ctor_m0D0BBD2A92AFDEF8268F669CC8D1D3E74E384C2E(L_4, L_0, L_3, NULL);
		V_0 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* L_5 = V_0;
		return L_5;
	}
}
// System.String Firebase.Storage.StorageReference::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageReference_ToString_m09E63E06152F8351F89268C6DE0A35A5F4C8B0B3 (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral465D8053A968C034065EEA4680E5BA0DA4E093BA);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0;
		L_0 = StorageReference_get_Bucket_m17BFF3774E2064931D6673E7FBB05299611BDB22(__this, NULL);
		String_t* L_1;
		L_1 = StorageReference_get_Path_m23006656BF5776DAC29630E641369A740F333C27(__this, NULL);
		String_t* L_2;
		L_2 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(_stringLiteral465D8053A968C034065EEA4680E5BA0DA4E093BA, L_0, L_1, NULL);
		V_0 = L_2;
		goto IL_001a;
	}

IL_001a:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Boolean Firebase.Storage.StorageReference::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StorageReference_Equals_m12EC7D8FA3D1E7CBEA6AE21486AF60A7D733D779 (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, RuntimeObject* ___other0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	{
		RuntimeObject* L_0 = ___other0;
		V_1 = (bool)((((int32_t)((!(((RuntimeObject*)(StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019*)((StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019*)IsInstSealed((RuntimeObject*)L_0, StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019_il2cpp_TypeInfo_var))) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_0031;
	}

IL_0016:
	{
		RuntimeObject* L_2 = ___other0;
		V_0 = ((StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019*)CastclassSealed((RuntimeObject*)L_2, StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019_il2cpp_TypeInfo_var));
		StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4;
		L_4 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		String_t* L_5;
		L_5 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		NullCheck(L_4);
		bool L_6;
		L_6 = String_Equals_mCD5F35DEDCAFE51ACD4E033726FC2EF8DF7E9B4D(L_4, L_5, NULL);
		V_2 = L_6;
		goto IL_0031;
	}

IL_0031:
	{
		bool L_7 = V_2;
		return L_7;
	}
}
// System.Int32 Firebase.Storage.StorageReference::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StorageReference_GetHashCode_mD11BCEA0CB329C7FB4165BDBF6F9321992C1D25F (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		String_t* L_0;
		L_0 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		NullCheck(L_0);
		int32_t L_1;
		L_1 = VirtualFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// Firebase.Storage.StorageReferenceInternal Firebase.Storage.StorageReference::get_Internal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* StorageReference_get_Internal_mDD8DFC623E89E8F540685EC02832A1B31DD4B7AF (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, const RuntimeMethod* method) 
{
	{
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_0 = __this->___U3CInternalU3Ek__BackingField_2;
		return L_0;
	}
}
// System.Void Firebase.Storage.StorageReference::set_Internal(Firebase.Storage.StorageReferenceInternal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageReference_set_Internal_m402EC4C5726D40B65F6AC62A605948E4FCBF72A2 (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* ___value0, const RuntimeMethod* method) 
{
	{
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_0 = ___value0;
		__this->___U3CInternalU3Ek__BackingField_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CInternalU3Ek__BackingField_2), (void*)L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Storage.StorageReferenceInternal::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageReferenceInternal__ctor_m7C41AA5A3F5475ECA398D40758FF6DA37718B677 (StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		bool L_0 = ___cMemoryOwn1;
		__this->___swigCMemOwn_1 = L_0;
		intptr_t L_1 = ___cPtr0;
		HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_2;
		memset((&L_2), 0, sizeof(L_2));
		HandleRef__ctor_m0298D55E5F35F77B6A6CCA75C8E828C3F3127DE7((&L_2), __this, L_1, /*hidden argument*/NULL);
		__this->___swigCPtr_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)&(((&__this->___swigCPtr_0))->____wrapper_0), (void*)NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageReferenceInternal::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageReferenceInternal_Finalize_m841C4C27BBBDB8F3E51D1284F168B6E8AF4208F2 (StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* __this, const RuntimeMethod* method) 
{
	{
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_000c:
			{// begin finally (depth: 1)
				Object_Finalize_mC98C96301CCABFE00F1A7EF8E15DF507CACD42B2(__this, NULL);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			VirtualActionInvoker1< bool >::Invoke(5 /* System.Void Firebase.Storage.StorageReferenceInternal::Dispose(System.Boolean) */, __this, (bool)0);
			goto IL_0014;
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0014:
	{
		return;
	}
}
// System.Void Firebase.Storage.StorageReferenceInternal::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageReferenceInternal_Dispose_m8DA06D2B1F391F8A18795B32192DB3C46829A1DA (StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GC_t920F9CF6EBB7C787E5010A4352E1B587F356DC58_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		VirtualActionInvoker1< bool >::Invoke(5 /* System.Void Firebase.Storage.StorageReferenceInternal::Dispose(System.Boolean) */, __this, (bool)1);
		il2cpp_codegen_runtime_class_init_inline(GC_t920F9CF6EBB7C787E5010A4352E1B587F356DC58_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m3352E2F2119EB46913B51B7AAE2F217C63C35F2A(__this, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageReferenceInternal::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageReferenceInternal_Dispose_m8EBCA21D9ECF5936B3F95BB8068A4D2B1E685584 (StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* __this, bool ___disposing0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GC_t920F9CF6EBB7C787E5010A4352E1B587F356DC58_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	{
		il2cpp_codegen_runtime_class_init_inline(FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var))->___disposeLock_2;
		V_0 = L_0;
		V_1 = (bool)0;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0068:
			{// begin finally (depth: 1)
				{
					bool L_1 = V_1;
					if (!L_1)
					{
						goto IL_0072;
					}
				}
				{
					RuntimeObject* L_2 = V_0;
					Monitor_Exit_m25A154960F91391E10E4CDA245ECDF4BA94D56A9(L_2, NULL);
				}

IL_0072:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				RuntimeObject* L_3 = V_0;
				Monitor_Enter_m00506757392936AA62DBE2C5FFBEE69EE920C4D4(L_3, (&V_1), NULL);
				HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F* L_4 = (&__this->___swigCPtr_0);
				intptr_t L_5;
				L_5 = HandleRef_get_Handle_m2055005E349E895499E1B3B826C89228FFAC4C17_inline(L_4, NULL);
				bool L_6;
				L_6 = IntPtr_op_Inequality_m2F715312CBFCE7E1A81D0689F68B97218E37E5D1(L_5, (0), NULL);
				V_2 = L_6;
				bool L_7 = V_2;
				if (!L_7)
				{
					goto IL_005e_1;
				}
			}
			{
				bool L_8 = __this->___swigCMemOwn_1;
				V_3 = L_8;
				bool L_9 = V_3;
				if (!L_9)
				{
					goto IL_004c_1;
				}
			}
			{
				__this->___swigCMemOwn_1 = (bool)0;
				HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_10 = __this->___swigCPtr_0;
				il2cpp_codegen_runtime_class_init_inline(StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
				StorageInternalPINVOKE_delete_StorageReferenceInternal_m6A2286A3920232B4EB017D422E4B2208BD8039E5(L_10, NULL);
			}

IL_004c_1:
			{
				HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_11;
				memset((&L_11), 0, sizeof(L_11));
				HandleRef__ctor_m0298D55E5F35F77B6A6CCA75C8E828C3F3127DE7((&L_11), NULL, (0), /*hidden argument*/NULL);
				__this->___swigCPtr_0 = L_11;
				Il2CppCodeGenWriteBarrier((void**)&(((&__this->___swigCPtr_0))->____wrapper_0), (void*)NULL);
			}

IL_005e_1:
			{
				il2cpp_codegen_runtime_class_init_inline(GC_t920F9CF6EBB7C787E5010A4352E1B587F356DC58_il2cpp_TypeInfo_var);
				GC_SuppressFinalize_m3352E2F2119EB46913B51B7AAE2F217C63C35F2A(__this, NULL);
				goto IL_0073;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0073:
	{
		return;
	}
}
// Firebase.Storage.StorageReferenceInternal Firebase.Storage.StorageReferenceInternal::Child(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* StorageReferenceInternal_Child_mB1AFC34728CA3C083BC6FBC74AF0CE9CCE2DC375 (StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* __this, String_t* ___path0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* V_0 = NULL;
	bool V_1 = false;
	StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* V_2 = NULL;
	{
		HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_0 = __this->___swigCPtr_0;
		String_t* L_1 = ___path0;
		il2cpp_codegen_runtime_class_init_inline(StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		intptr_t L_2;
		L_2 = StorageInternalPINVOKE_StorageReferenceInternal_Child__SWIG_0_m488BF8D5FD204F98A2CBF749913451ACC5DF5432(L_0, L_1, NULL);
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_3 = (StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4*)il2cpp_codegen_object_new(StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		StorageReferenceInternal__ctor_m7C41AA5A3F5475ECA398D40758FF6DA37718B677(L_3, L_2, (bool)1, NULL);
		V_0 = L_3;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = SWIGPendingException_get_Pending_m0BEE83B2B2559DE231EE2A87DF09649355775538(NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0023;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var)));
		Exception_t* L_6;
		L_6 = SWIGPendingException_Retrieve_m5C29154CE762C145ECCBA0E7A37F5579366647BF(NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StorageReferenceInternal_Child_mB1AFC34728CA3C083BC6FBC74AF0CE9CCE2DC375_RuntimeMethod_var)));
	}

IL_0023:
	{
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_7 = V_0;
		V_2 = L_7;
		goto IL_0027;
	}

IL_0027:
	{
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_8 = V_2;
		return L_8;
	}
}
// System.String Firebase.Storage.StorageReferenceInternal::get_Bucket()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageReferenceInternal_get_Bucket_m7C7737DC9A4D22049E4C6338515E229935A68BF5 (StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_0 = __this->___swigCPtr_0;
		il2cpp_codegen_runtime_class_init_inline(StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = StorageInternalPINVOKE_StorageReferenceInternal_Bucket_get_m2ECA8DF098F05A5CFB796E7C650FAB954511A335(L_0, NULL);
		V_0 = L_1;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = SWIGPendingException_get_Pending_m0BEE83B2B2559DE231EE2A87DF09649355775538(NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var)));
		Exception_t* L_4;
		L_4 = SWIGPendingException_Retrieve_m5C29154CE762C145ECCBA0E7A37F5579366647BF(NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StorageReferenceInternal_get_Bucket_m7C7737DC9A4D22049E4C6338515E229935A68BF5_RuntimeMethod_var)));
	}

IL_001c:
	{
		String_t* L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		String_t* L_6 = V_2;
		return L_6;
	}
}
// System.String Firebase.Storage.StorageReferenceInternal::get_FullPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageReferenceInternal_get_FullPath_mE93027973F851741548644FA6CE90911FB02FC42 (StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_0 = __this->___swigCPtr_0;
		il2cpp_codegen_runtime_class_init_inline(StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = StorageInternalPINVOKE_StorageReferenceInternal_FullPath_get_m91D0E407E4D79C30F49C0B9B729906E25F942EAD(L_0, NULL);
		V_0 = L_1;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = SWIGPendingException_get_Pending_m0BEE83B2B2559DE231EE2A87DF09649355775538(NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var)));
		Exception_t* L_4;
		L_4 = SWIGPendingException_Retrieve_m5C29154CE762C145ECCBA0E7A37F5579366647BF(NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StorageReferenceInternal_get_FullPath_mE93027973F851741548644FA6CE90911FB02FC42_RuntimeMethod_var)));
	}

IL_001c:
	{
		String_t* L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		String_t* L_6 = V_2;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Storage.FirebaseStorageInternal::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorageInternal__ctor_mB8F9788F57268947777E0C442C9C2BF3370CFE6E (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		bool L_0 = ___cMemoryOwn1;
		__this->___swigCMemOwn_1 = L_0;
		intptr_t L_1 = ___cPtr0;
		HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_2;
		memset((&L_2), 0, sizeof(L_2));
		HandleRef__ctor_m0298D55E5F35F77B6A6CCA75C8E828C3F3127DE7((&L_2), __this, L_1, /*hidden argument*/NULL);
		__this->___swigCPtr_0 = L_2;
		Il2CppCodeGenWriteBarrier((void**)&(((&__this->___swigCPtr_0))->____wrapper_0), (void*)NULL);
		return;
	}
}
// System.Runtime.InteropServices.HandleRef Firebase.Storage.FirebaseStorageInternal::getCPtr(Firebase.Storage.FirebaseStorageInternal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F FirebaseStorageInternal_getCPtr_mFB281328FEF2F13C113A67C221574881449C34E3 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* ___obj0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F V_0;
	memset((&V_0), 0, sizeof(V_0));
	HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	{
		FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_0 = ___obj0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_1 = ___obj0;
		NullCheck(L_1);
		HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_2 = L_1->___swigCPtr_0;
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_000c:
	{
		HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_3;
		memset((&L_3), 0, sizeof(L_3));
		HandleRef__ctor_m0298D55E5F35F77B6A6CCA75C8E828C3F3127DE7((&L_3), NULL, (0), /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		goto IL_001a;
	}

IL_001a:
	{
		HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_4 = V_0;
		return L_4;
	}
}
// System.Void Firebase.Storage.FirebaseStorageInternal::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorageInternal_Finalize_m08E3C8A4DCBBEAD55985B914BCF993BEE7BE3237 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, const RuntimeMethod* method) 
{
	{
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_000c:
			{// begin finally (depth: 1)
				Object_Finalize_mC98C96301CCABFE00F1A7EF8E15DF507CACD42B2(__this, NULL);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			VirtualActionInvoker1< bool >::Invoke(5 /* System.Void Firebase.Storage.FirebaseStorageInternal::Dispose(System.Boolean) */, __this, (bool)0);
			goto IL_0014;
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0014:
	{
		return;
	}
}
// System.Void Firebase.Storage.FirebaseStorageInternal::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorageInternal_Dispose_m8BE167067EB4C1FBF823331DE520C441E5AF0B1F (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GC_t920F9CF6EBB7C787E5010A4352E1B587F356DC58_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		VirtualActionInvoker1< bool >::Invoke(5 /* System.Void Firebase.Storage.FirebaseStorageInternal::Dispose(System.Boolean) */, __this, (bool)1);
		il2cpp_codegen_runtime_class_init_inline(GC_t920F9CF6EBB7C787E5010A4352E1B587F356DC58_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m3352E2F2119EB46913B51B7AAE2F217C63C35F2A(__this, NULL);
		return;
	}
}
// System.Void Firebase.Storage.FirebaseStorageInternal::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorageInternal_Dispose_m18EF3F130CCD8C586936462F4D9C140CFC673824 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, bool ___disposing0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	bool V_1 = false;
	{
		il2cpp_codegen_runtime_class_init_inline(FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var))->___disposeLock_2;
		V_0 = L_0;
		V_1 = (bool)0;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0035:
			{// begin finally (depth: 1)
				{
					bool L_1 = V_1;
					if (!L_1)
					{
						goto IL_003f;
					}
				}
				{
					RuntimeObject* L_2 = V_0;
					Monitor_Exit_m25A154960F91391E10E4CDA245ECDF4BA94D56A9(L_2, NULL);
				}

IL_003f:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			RuntimeObject* L_3 = V_0;
			Monitor_Enter_m00506757392936AA62DBE2C5FFBEE69EE920C4D4(L_3, (&V_1), NULL);
			FirebaseStorageInternal_ReleaseReferenceInternal_m1C1D57F61F7501AC8252D78E924A6CA68B2FD3F7(__this, NULL);
			__this->___swigCMemOwn_1 = (bool)0;
			HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_4;
			memset((&L_4), 0, sizeof(L_4));
			HandleRef__ctor_m0298D55E5F35F77B6A6CCA75C8E828C3F3127DE7((&L_4), NULL, (0), /*hidden argument*/NULL);
			__this->___swigCPtr_0 = L_4;
			Il2CppCodeGenWriteBarrier((void**)&(((&__this->___swigCPtr_0))->____wrapper_0), (void*)NULL);
			goto IL_0040;
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0040:
	{
		return;
	}
}
// System.String Firebase.Storage.FirebaseStorageInternal::get_InstanceKey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseStorageInternal_get_InstanceKey_m9F8ABBA175A15B51661EF6112C1BF06CAD267F4C (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_0;
		L_0 = FirebaseStorageInternal_get_App_mA92F711407E7D883382645FCF479370018821AF3(__this, NULL);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = FirebaseApp_get_Name_m89C11F96726C8E4FD3CCAE04A5DC3129F7CD975E(L_0, NULL);
		String_t* L_2;
		L_2 = FirebaseStorageInternal_get_Url_mB62A11FAF14E963FF2A843A45705CEE209636744(__this, NULL);
		String_t* L_3;
		L_3 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_1, L_2, NULL);
		V_0 = L_3;
		goto IL_001a;
	}

IL_001a:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Void Firebase.Storage.FirebaseStorageInternal::SetSwigCMemOwn(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorageInternal_SetSwigCMemOwn_mE268A0AD6F3528EC65D486EC1F0B228D84D96629 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, bool ___ownership0, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___ownership0;
		__this->___swigCMemOwn_1 = L_0;
		return;
	}
}
// Firebase.Storage.StorageReferenceInternal Firebase.Storage.FirebaseStorageInternal::GetReference()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* FirebaseStorageInternal_GetReference_mAE4C3E5F79096BB4A0FF285DAEF8B89E98A12765 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* V_0 = NULL;
	bool V_1 = false;
	StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* V_2 = NULL;
	{
		HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_0 = __this->___swigCPtr_0;
		il2cpp_codegen_runtime_class_init_inline(StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		intptr_t L_1;
		L_1 = StorageInternalPINVOKE_FirebaseStorageInternal_GetReference__SWIG_0_mCC149C08BBFE6842916E89ED4FFD0D194A127609(L_0, NULL);
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_2 = (StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4*)il2cpp_codegen_object_new(StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		StorageReferenceInternal__ctor_m7C41AA5A3F5475ECA398D40758FF6DA37718B677(L_2, L_1, (bool)1, NULL);
		V_0 = L_2;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = SWIGPendingException_get_Pending_m0BEE83B2B2559DE231EE2A87DF09649355775538(NULL);
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var)));
		Exception_t* L_5;
		L_5 = SWIGPendingException_Retrieve_m5C29154CE762C145ECCBA0E7A37F5579366647BF(NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FirebaseStorageInternal_GetReference_mAE4C3E5F79096BB4A0FF285DAEF8B89E98A12765_RuntimeMethod_var)));
	}

IL_0022:
	{
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_6 = V_0;
		V_2 = L_6;
		goto IL_0026;
	}

IL_0026:
	{
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_7 = V_2;
		return L_7;
	}
}
// Firebase.Storage.FirebaseStorageInternal Firebase.Storage.FirebaseStorageInternal::GetInstanceInternal(Firebase.FirebaseApp,System.String,Firebase.InitResult&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* FirebaseStorageInternal_GetInstanceInternal_m671B163ECAF2D5B145D295BF2946CA42A5653F55 (FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* ___app0, String_t* ___url1, int32_t* ___init_result_out2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	intptr_t V_1;
	memset((&V_1), 0, sizeof(V_1));
	FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* V_2 = NULL;
	bool V_3 = false;
	FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* V_4 = NULL;
	FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* G_B4_0 = NULL;
	{
		V_0 = 0;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_003f:
			{// begin finally (depth: 1)
				int32_t* L_0 = ___init_result_out2;
				int32_t L_1 = V_0;
				*((int32_t*)L_0) = (int32_t)L_1;
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_2 = ___app0;
				il2cpp_codegen_runtime_class_init_inline(FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
				HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_3;
				L_3 = FirebaseApp_getCPtr_mCF6551417C0F1D98798ED7810553EBD977381D16(L_2, NULL);
				String_t* L_4 = ___url1;
				il2cpp_codegen_runtime_class_init_inline(StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
				intptr_t L_5;
				L_5 = StorageInternalPINVOKE_FirebaseStorageInternal_GetInstanceInternal_mE552EFD865D6FD1D48C7F76E33820E26BF1843F7(L_3, L_4, (&V_0), NULL);
				V_1 = L_5;
				intptr_t L_6 = V_1;
				bool L_7;
				L_7 = IntPtr_op_Equality_m73759B51FE326460AC87A0E386480226EF2FABED(L_6, (0), NULL);
				if (L_7)
				{
					goto IL_0029_1;
				}
			}
			{
				intptr_t L_8 = V_1;
				FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_9 = (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145*)il2cpp_codegen_object_new(FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145_il2cpp_TypeInfo_var);
				NullCheck(L_9);
				FirebaseStorageInternal__ctor_mB8F9788F57268947777E0C442C9C2BF3370CFE6E(L_9, L_8, (bool)0, NULL);
				G_B4_0 = L_9;
				goto IL_002a_1;
			}

IL_0029_1:
			{
				G_B4_0 = ((FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145*)(NULL));
			}

IL_002a_1:
			{
				V_2 = G_B4_0;
				il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
				bool L_10;
				L_10 = SWIGPendingException_get_Pending_m0BEE83B2B2559DE231EE2A87DF09649355775538(NULL);
				V_3 = L_10;
				bool L_11 = V_3;
				if (!L_11)
				{
					goto IL_003a_1;
				}
			}
			{
				il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var)));
				Exception_t* L_12;
				L_12 = SWIGPendingException_Retrieve_m5C29154CE762C145ECCBA0E7A37F5579366647BF(NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FirebaseStorageInternal_GetInstanceInternal_m671B163ECAF2D5B145D295BF2946CA42A5653F55_RuntimeMethod_var)));
			}

IL_003a_1:
			{
				FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_13 = V_2;
				V_4 = L_13;
				goto IL_0045;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0045:
	{
		FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_14 = V_4;
		return L_14;
	}
}
// System.Void Firebase.Storage.FirebaseStorageInternal::ReleaseReferenceInternal(Firebase.Storage.FirebaseStorageInternal)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseStorageInternal_ReleaseReferenceInternal_m1C1D57F61F7501AC8252D78E924A6CA68B2FD3F7 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* ___instance0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* L_0 = ___instance0;
		HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_1;
		L_1 = FirebaseStorageInternal_getCPtr_mFB281328FEF2F13C113A67C221574881449C34E3(L_0, NULL);
		il2cpp_codegen_runtime_class_init_inline(StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		StorageInternalPINVOKE_FirebaseStorageInternal_ReleaseReferenceInternal_m9D43F4BB2235C7C1732D860C531B9BC13A5C5709(L_1, NULL);
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = SWIGPendingException_get_Pending_m0BEE83B2B2559DE231EE2A87DF09649355775538(NULL);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var)));
		Exception_t* L_4;
		L_4 = SWIGPendingException_Retrieve_m5C29154CE762C145ECCBA0E7A37F5579366647BF(NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FirebaseStorageInternal_ReleaseReferenceInternal_m1C1D57F61F7501AC8252D78E924A6CA68B2FD3F7_RuntimeMethod_var)));
	}

IL_001c:
	{
		return;
	}
}
// Firebase.FirebaseApp Firebase.Storage.FirebaseStorageInternal::get_App()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* FirebaseStorageInternal_get_App_mA92F711407E7D883382645FCF479370018821AF3 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* V_1 = NULL;
	bool V_2 = false;
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* V_3 = NULL;
	FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* G_B3_0 = NULL;
	{
		HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_0 = __this->___swigCPtr_0;
		il2cpp_codegen_runtime_class_init_inline(StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		intptr_t L_1;
		L_1 = StorageInternalPINVOKE_FirebaseStorageInternal_App_get_m666DC84501145FC523A16322B406F38AB58075AC(L_0, NULL);
		V_0 = L_1;
		intptr_t L_2 = V_0;
		bool L_3;
		L_3 = IntPtr_op_Equality_m73759B51FE326460AC87A0E386480226EF2FABED(L_2, (0), NULL);
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		intptr_t L_4 = V_0;
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_5 = (FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25*)il2cpp_codegen_object_new(FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		FirebaseApp__ctor_mC539AF748C2E16CD3B7820D6039B9A29DBDF908C(L_5, L_4, (bool)0, NULL);
		G_B3_0 = L_5;
		goto IL_0024;
	}

IL_0023:
	{
		G_B3_0 = ((FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25*)(NULL));
	}

IL_0024:
	{
		V_1 = G_B3_0;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = SWIGPendingException_get_Pending_m0BEE83B2B2559DE231EE2A87DF09649355775538(NULL);
		V_2 = L_6;
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_0034;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var)));
		Exception_t* L_8;
		L_8 = SWIGPendingException_Retrieve_m5C29154CE762C145ECCBA0E7A37F5579366647BF(NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FirebaseStorageInternal_get_App_mA92F711407E7D883382645FCF479370018821AF3_RuntimeMethod_var)));
	}

IL_0034:
	{
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_9 = V_1;
		V_3 = L_9;
		goto IL_0038;
	}

IL_0038:
	{
		FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25* L_10 = V_3;
		return L_10;
	}
}
// System.String Firebase.Storage.FirebaseStorageInternal::get_Url()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseStorageInternal_get_Url_mB62A11FAF14E963FF2A843A45705CEE209636744 (FirebaseStorageInternal_tD6FAEA0321B7D20A820BB25CDC18EFE11F282145* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F L_0 = __this->___swigCPtr_0;
		il2cpp_codegen_runtime_class_init_inline(StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = StorageInternalPINVOKE_FirebaseStorageInternal_Url_get_mD62453262CBF244BC8AA3119C7A166FED10E1517(L_0, NULL);
		V_0 = L_1;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = SWIGPendingException_get_Pending_m0BEE83B2B2559DE231EE2A87DF09649355775538(NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SWIGPendingException_tABBC4E77437B082457DE5327A693947CE0C7CF63_il2cpp_TypeInfo_var)));
		Exception_t* L_4;
		L_4 = SWIGPendingException_Retrieve_m5C29154CE762C145ECCBA0E7A37F5579366647BF(NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&FirebaseStorageInternal_get_Url_mB62A11FAF14E963FF2A843A45705CEE209636744_RuntimeMethod_var)));
	}

IL_001c:
	{
		String_t* L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		String_t* L_6 = V_2;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Storage.StorageInternalPINVOKE::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageInternalPINVOKE__cctor_mA53C11A0824749C7CABA499F984A739053426316 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149* L_0 = (SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149*)il2cpp_codegen_object_new(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		SWIGExceptionHelper__ctor_mAD73726452D2B9A7610ED82C4B6868D0C424C849(L_0, NULL);
		((StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_StaticFields*)il2cpp_codegen_static_fields_for(StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var))->___swigExceptionHelper_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_StaticFields*)il2cpp_codegen_static_fields_for(StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var))->___swigExceptionHelper_0), (void*)L_0);
		SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004* L_1 = (SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004*)il2cpp_codegen_object_new(SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		SWIGStringHelper__ctor_m742C1F69A35C651EA7707D33CFDCF5C602DD81E3(L_1, NULL);
		((StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_StaticFields*)il2cpp_codegen_static_fields_for(StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var))->___swigStringHelper_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&((StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_StaticFields*)il2cpp_codegen_static_fields_for(StorageInternalPINVOKE_t31DA308783BDFC3D0A0C393B50092E3F4856ECC9_il2cpp_TypeInfo_var))->___swigStringHelper_1), (void*)L_1);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE::delete_StorageReferenceInternal(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageInternalPINVOKE_delete_StorageReferenceInternal_m6A2286A3920232B4EB017D422E4B2208BD8039E5 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("FirebaseCppApp-11_2_0"), "Firebase_Storage_CSharp_delete_StorageReferenceInternal", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.____handle_1;

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	reinterpret_cast<PInvokeFunc>(Firebase_Storage_CSharp_delete_StorageReferenceInternal)(____jarg10_marshaled);
	#else
	il2cppPInvokeFunc(____jarg10_marshaled);
	#endif

}
// System.IntPtr Firebase.Storage.StorageInternalPINVOKE::StorageReferenceInternal_Child__SWIG_0(System.Runtime.InteropServices.HandleRef,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t StorageInternalPINVOKE_StorageReferenceInternal_Child__SWIG_0_m488BF8D5FD204F98A2CBF749913451ACC5DF5432 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, String_t* ___jarg21, const RuntimeMethod* method) 
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*) + sizeof(char*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("FirebaseCppApp-11_2_0"), "Firebase_Storage_CSharp_StorageReferenceInternal_Child__SWIG_0", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.____handle_1;

	// Marshaling of parameter '___jarg21' to native representation
	char* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_string(___jarg21);

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Storage_CSharp_StorageReferenceInternal_Child__SWIG_0)(____jarg10_marshaled, ____jarg21_marshaled);
	#else
	intptr_t returnValue = il2cppPInvokeFunc(____jarg10_marshaled, ____jarg21_marshaled);
	#endif

	// Marshaling cleanup of parameter '___jarg21' native representation
	il2cpp_codegen_marshal_free(____jarg21_marshaled);
	____jarg21_marshaled = NULL;

	return returnValue;
}
// System.String Firebase.Storage.StorageInternalPINVOKE::StorageReferenceInternal_Bucket_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageInternalPINVOKE_StorageReferenceInternal_Bucket_get_m2ECA8DF098F05A5CFB796E7C650FAB954511A335 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) 
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("FirebaseCppApp-11_2_0"), "Firebase_Storage_CSharp_StorageReferenceInternal_Bucket_get", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.____handle_1;

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Storage_CSharp_StorageReferenceInternal_Bucket_get)(____jarg10_marshaled);
	#else
	char* returnValue = il2cppPInvokeFunc(____jarg10_marshaled);
	#endif

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
// System.String Firebase.Storage.StorageInternalPINVOKE::StorageReferenceInternal_FullPath_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageInternalPINVOKE_StorageReferenceInternal_FullPath_get_m91D0E407E4D79C30F49C0B9B729906E25F942EAD (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) 
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("FirebaseCppApp-11_2_0"), "Firebase_Storage_CSharp_StorageReferenceInternal_FullPath_get", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.____handle_1;

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Storage_CSharp_StorageReferenceInternal_FullPath_get)(____jarg10_marshaled);
	#else
	char* returnValue = il2cppPInvokeFunc(____jarg10_marshaled);
	#endif

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
// System.IntPtr Firebase.Storage.StorageInternalPINVOKE::FirebaseStorageInternal_GetReference__SWIG_0(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t StorageInternalPINVOKE_FirebaseStorageInternal_GetReference__SWIG_0_mCC149C08BBFE6842916E89ED4FFD0D194A127609 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) 
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("FirebaseCppApp-11_2_0"), "Firebase_Storage_CSharp_FirebaseStorageInternal_GetReference__SWIG_0", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.____handle_1;

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Storage_CSharp_FirebaseStorageInternal_GetReference__SWIG_0)(____jarg10_marshaled);
	#else
	intptr_t returnValue = il2cppPInvokeFunc(____jarg10_marshaled);
	#endif

	return returnValue;
}
// System.IntPtr Firebase.Storage.StorageInternalPINVOKE::FirebaseStorageInternal_GetInstanceInternal(System.Runtime.InteropServices.HandleRef,System.String,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t StorageInternalPINVOKE_FirebaseStorageInternal_GetInstanceInternal_mE552EFD865D6FD1D48C7F76E33820E26BF1843F7 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, String_t* ___jarg21, int32_t* ___jarg32, const RuntimeMethod* method) 
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, int32_t*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*) + sizeof(char*) + sizeof(int32_t*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("FirebaseCppApp-11_2_0"), "Firebase_Storage_CSharp_FirebaseStorageInternal_GetInstanceInternal", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.____handle_1;

	// Marshaling of parameter '___jarg21' to native representation
	char* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_string(___jarg21);

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Storage_CSharp_FirebaseStorageInternal_GetInstanceInternal)(____jarg10_marshaled, ____jarg21_marshaled, ___jarg32);
	#else
	intptr_t returnValue = il2cppPInvokeFunc(____jarg10_marshaled, ____jarg21_marshaled, ___jarg32);
	#endif

	// Marshaling cleanup of parameter '___jarg21' native representation
	il2cpp_codegen_marshal_free(____jarg21_marshaled);
	____jarg21_marshaled = NULL;

	return returnValue;
}
// System.Void Firebase.Storage.StorageInternalPINVOKE::FirebaseStorageInternal_ReleaseReferenceInternal(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StorageInternalPINVOKE_FirebaseStorageInternal_ReleaseReferenceInternal_m9D43F4BB2235C7C1732D860C531B9BC13A5C5709 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("FirebaseCppApp-11_2_0"), "Firebase_Storage_CSharp_FirebaseStorageInternal_ReleaseReferenceInternal", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.____handle_1;

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	reinterpret_cast<PInvokeFunc>(Firebase_Storage_CSharp_FirebaseStorageInternal_ReleaseReferenceInternal)(____jarg10_marshaled);
	#else
	il2cppPInvokeFunc(____jarg10_marshaled);
	#endif

}
// System.IntPtr Firebase.Storage.StorageInternalPINVOKE::FirebaseStorageInternal_App_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t StorageInternalPINVOKE_FirebaseStorageInternal_App_get_m666DC84501145FC523A16322B406F38AB58075AC (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) 
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("FirebaseCppApp-11_2_0"), "Firebase_Storage_CSharp_FirebaseStorageInternal_App_get", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.____handle_1;

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Storage_CSharp_FirebaseStorageInternal_App_get)(____jarg10_marshaled);
	#else
	intptr_t returnValue = il2cppPInvokeFunc(____jarg10_marshaled);
	#endif

	return returnValue;
}
// System.String Firebase.Storage.StorageInternalPINVOKE::FirebaseStorageInternal_Url_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* StorageInternalPINVOKE_FirebaseStorageInternal_Url_get_mD62453262CBF244BC8AA3119C7A166FED10E1517 (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F ___jarg10, const RuntimeMethod* method) 
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("FirebaseCppApp-11_2_0"), "Firebase_Storage_CSharp_FirebaseStorageInternal_Url_get", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.____handle_1;

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Storage_CSharp_FirebaseStorageInternal_Url_get)(____jarg10_marshaled);
	#else
	char* returnValue = il2cppPInvokeFunc(____jarg10_marshaled);
	#endif

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingApplicationException_m3389DA14B6470D215F411A6AB64161049D976B92(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingApplicationException_m3389DA14B6470D215F411A6AB64161049D976B92(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArithmeticException_mDA9940C49B02A145257997D6E29939B520DECE30(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArithmeticException_mDA9940C49B02A145257997D6E29939B520DECE30(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingDivideByZeroException_m2AC76C31521184D3E9CEA398D2304368C1C3F245(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingDivideByZeroException_m2AC76C31521184D3E9CEA398D2304368C1C3F245(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m9444EB341D36B5E18F7DD12453881B4E5D527B77(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m9444EB341D36B5E18F7DD12453881B4E5D527B77(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidCastException_m56DA5D72BDA177C1E2E9FA830A03F6A315064907(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingInvalidCastException_m56DA5D72BDA177C1E2E9FA830A03F6A315064907(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidOperationException_m8319A7B285528224F0D94E93683A03F8B2A1CF61(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingInvalidOperationException_m8319A7B285528224F0D94E93683A03F8B2A1CF61(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIOException_m1E8C16376B2B3701BAC90728915AAF17EC843685(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingIOException_m1E8C16376B2B3701BAC90728915AAF17EC843685(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingNullReferenceException_m5357E1A78F179FAE6A3EC032052906188478E50D(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingNullReferenceException_m5357E1A78F179FAE6A3EC032052906188478E50D(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOutOfMemoryException_mCED02F27DC5E0EDC86EAA55784361567D1D01CA6(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingOutOfMemoryException_mCED02F27DC5E0EDC86EAA55784361567D1D01CA6(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOverflowException_mCA646C70DE90380B08954D135018002CE4A99E86(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingOverflowException_mCA646C70DE90380B08954D135018002CE4A99E86(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingSystemException_mE0F156F2311B8F1ECDFE4267B73A0CE00DA9A01E(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingSystemException_mE0F156F2311B8F1ECDFE4267B73A0CE00DA9A01E(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentException_m59C2895E8334A034F0C59E34A3C14F39340478CC(char* ___message0, char* ___paramName1)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArgumentException_m59C2895E8334A034F0C59E34A3C14F39340478CC(____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentNullException_m4B7CABD3A4D1075F6323D8EB69F7E6E8667665F8(char* ___message0, char* ___paramName1)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArgumentNullException_m4B7CABD3A4D1075F6323D8EB69F7E6E8667665F8(____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mB01500F82308A09A74F016F7671A8E282A105C55(char* ___message0, char* ___paramName1)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mB01500F82308A09A74F016F7671A8E282A105C55(____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_StorageInternal(Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_StorageInternal_mC4703B7B2BCFF2FD96F4400F20636806BE454B4B (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___applicationDelegate0, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___arithmeticDelegate1, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___divideByZeroDelegate2, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___indexOutOfRangeDelegate3, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___invalidCastDelegate4, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___invalidOperationDelegate5, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___ioDelegate6, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___nullReferenceDelegate7, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___outOfMemoryDelegate8, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___overflowDelegate9, ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* ___systemExceptionDelegate10, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*) + sizeof(void*) + sizeof(void*) + sizeof(void*) + sizeof(void*) + sizeof(void*) + sizeof(void*) + sizeof(void*) + sizeof(void*) + sizeof(void*) + sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("FirebaseCppApp-11_2_0"), "SWIGRegisterExceptionCallbacks_StorageInternal", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___applicationDelegate0' to native representation
	Il2CppMethodPointer ____applicationDelegate0_marshaled = NULL;
	____applicationDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___applicationDelegate0));

	// Marshaling of parameter '___arithmeticDelegate1' to native representation
	Il2CppMethodPointer ____arithmeticDelegate1_marshaled = NULL;
	____arithmeticDelegate1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___arithmeticDelegate1));

	// Marshaling of parameter '___divideByZeroDelegate2' to native representation
	Il2CppMethodPointer ____divideByZeroDelegate2_marshaled = NULL;
	____divideByZeroDelegate2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___divideByZeroDelegate2));

	// Marshaling of parameter '___indexOutOfRangeDelegate3' to native representation
	Il2CppMethodPointer ____indexOutOfRangeDelegate3_marshaled = NULL;
	____indexOutOfRangeDelegate3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___indexOutOfRangeDelegate3));

	// Marshaling of parameter '___invalidCastDelegate4' to native representation
	Il2CppMethodPointer ____invalidCastDelegate4_marshaled = NULL;
	____invalidCastDelegate4_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___invalidCastDelegate4));

	// Marshaling of parameter '___invalidOperationDelegate5' to native representation
	Il2CppMethodPointer ____invalidOperationDelegate5_marshaled = NULL;
	____invalidOperationDelegate5_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___invalidOperationDelegate5));

	// Marshaling of parameter '___ioDelegate6' to native representation
	Il2CppMethodPointer ____ioDelegate6_marshaled = NULL;
	____ioDelegate6_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___ioDelegate6));

	// Marshaling of parameter '___nullReferenceDelegate7' to native representation
	Il2CppMethodPointer ____nullReferenceDelegate7_marshaled = NULL;
	____nullReferenceDelegate7_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___nullReferenceDelegate7));

	// Marshaling of parameter '___outOfMemoryDelegate8' to native representation
	Il2CppMethodPointer ____outOfMemoryDelegate8_marshaled = NULL;
	____outOfMemoryDelegate8_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___outOfMemoryDelegate8));

	// Marshaling of parameter '___overflowDelegate9' to native representation
	Il2CppMethodPointer ____overflowDelegate9_marshaled = NULL;
	____overflowDelegate9_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___overflowDelegate9));

	// Marshaling of parameter '___systemExceptionDelegate10' to native representation
	Il2CppMethodPointer ____systemExceptionDelegate10_marshaled = NULL;
	____systemExceptionDelegate10_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___systemExceptionDelegate10));

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	reinterpret_cast<PInvokeFunc>(SWIGRegisterExceptionCallbacks_StorageInternal)(____applicationDelegate0_marshaled, ____arithmeticDelegate1_marshaled, ____divideByZeroDelegate2_marshaled, ____indexOutOfRangeDelegate3_marshaled, ____invalidCastDelegate4_marshaled, ____invalidOperationDelegate5_marshaled, ____ioDelegate6_marshaled, ____nullReferenceDelegate7_marshaled, ____outOfMemoryDelegate8_marshaled, ____overflowDelegate9_marshaled, ____systemExceptionDelegate10_marshaled);
	#else
	il2cppPInvokeFunc(____applicationDelegate0_marshaled, ____arithmeticDelegate1_marshaled, ____divideByZeroDelegate2_marshaled, ____indexOutOfRangeDelegate3_marshaled, ____invalidCastDelegate4_marshaled, ____invalidOperationDelegate5_marshaled, ____ioDelegate6_marshaled, ____nullReferenceDelegate7_marshaled, ____outOfMemoryDelegate8_marshaled, ____overflowDelegate9_marshaled, ____systemExceptionDelegate10_marshaled);
	#endif

}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_StorageInternal(Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_StorageInternal_mB84CA4E78B9898DA55BD54308320D9DAEE71D6C2 (ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* ___argumentDelegate0, ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* ___argumentNullDelegate1, ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* ___argumentOutOfRangeDelegate2, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*) + sizeof(void*) + sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("FirebaseCppApp-11_2_0"), "SWIGRegisterExceptionArgumentCallbacks_StorageInternal", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___argumentDelegate0' to native representation
	Il2CppMethodPointer ____argumentDelegate0_marshaled = NULL;
	____argumentDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___argumentDelegate0));

	// Marshaling of parameter '___argumentNullDelegate1' to native representation
	Il2CppMethodPointer ____argumentNullDelegate1_marshaled = NULL;
	____argumentNullDelegate1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___argumentNullDelegate1));

	// Marshaling of parameter '___argumentOutOfRangeDelegate2' to native representation
	Il2CppMethodPointer ____argumentOutOfRangeDelegate2_marshaled = NULL;
	____argumentOutOfRangeDelegate2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___argumentOutOfRangeDelegate2));

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	reinterpret_cast<PInvokeFunc>(SWIGRegisterExceptionArgumentCallbacks_StorageInternal)(____argumentDelegate0_marshaled, ____argumentNullDelegate1_marshaled, ____argumentOutOfRangeDelegate2_marshaled);
	#else
	il2cppPInvokeFunc(____argumentDelegate0_marshaled, ____argumentNullDelegate1_marshaled, ____argumentOutOfRangeDelegate2_marshaled);
	#endif

}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingApplicationException_m3389DA14B6470D215F411A6AB64161049D976B92 (String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_1;
		L_1 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A* L_2 = (ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A*)il2cpp_codegen_object_new(ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		ApplicationException__ctor_m924E77609BAFA0595453363EB8B7BCCBA03B32DD(L_2, L_0, L_1, NULL);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_2, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArithmeticException_mDA9940C49B02A145257997D6E29939B520DECE30 (String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArithmeticException_t07E77822D0007642BC8959A671E70D1F33C84FEA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_1;
		L_1 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		ArithmeticException_t07E77822D0007642BC8959A671E70D1F33C84FEA* L_2 = (ArithmeticException_t07E77822D0007642BC8959A671E70D1F33C84FEA*)il2cpp_codegen_object_new(ArithmeticException_t07E77822D0007642BC8959A671E70D1F33C84FEA_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		ArithmeticException__ctor_m880D58CC9B6CD2F0E867298BA748343216D89A8B(L_2, L_0, L_1, NULL);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_2, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingDivideByZeroException_m2AC76C31521184D3E9CEA398D2304368C1C3F245 (String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DivideByZeroException_tC43171E50A38F5CD4242D258D0B0C6B74898C279_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_1;
		L_1 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		DivideByZeroException_tC43171E50A38F5CD4242D258D0B0C6B74898C279* L_2 = (DivideByZeroException_tC43171E50A38F5CD4242D258D0B0C6B74898C279*)il2cpp_codegen_object_new(DivideByZeroException_tC43171E50A38F5CD4242D258D0B0C6B74898C279_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		DivideByZeroException__ctor_mE15900048AEEE9B66A4DD9F2ACAC4448D85D4F23(L_2, L_0, L_1, NULL);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_2, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m9444EB341D36B5E18F7DD12453881B4E5D527B77 (String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_1;
		L_1 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82* L_2 = (IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82*)il2cpp_codegen_object_new(IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		IndexOutOfRangeException__ctor_m390691571A232F79022C84ED002FDEF8974255E1(L_2, L_0, L_1, NULL);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_2, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingInvalidCastException_m56DA5D72BDA177C1E2E9FA830A03F6A315064907 (String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InvalidCastException_t47FC62F21A3937E814D20381DDACEF240E95AC2E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_1;
		L_1 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		InvalidCastException_t47FC62F21A3937E814D20381DDACEF240E95AC2E* L_2 = (InvalidCastException_t47FC62F21A3937E814D20381DDACEF240E95AC2E*)il2cpp_codegen_object_new(InvalidCastException_t47FC62F21A3937E814D20381DDACEF240E95AC2E_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		InvalidCastException__ctor_m40BCFD6C1C79DE81191B829AF71BEB590E300396(L_2, L_0, L_1, NULL);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_2, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingInvalidOperationException_m8319A7B285528224F0D94E93683A03F8B2A1CF61 (String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_1;
		L_1 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB* L_2 = (InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB*)il2cpp_codegen_object_new(InvalidOperationException_t5DDE4D49B7405FAAB1E4576F4715A42A3FAD4BAB_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		InvalidOperationException__ctor_m63F5561BE647F655D22C8289E53A5D3A2196B668(L_2, L_0, L_1, NULL);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_2, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingIOException_m1E8C16376B2B3701BAC90728915AAF17EC843685 (String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_1;
		L_1 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910* L_2 = (IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910*)il2cpp_codegen_object_new(IOException_t5D599190B003D41D45D4839A9B6B9AB53A755910_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		IOException__ctor_mFA9F39D1AF43FBC40BFA68A7BFE07852D1EF8B1B(L_2, L_0, L_1, NULL);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_2, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingNullReferenceException_m5357E1A78F179FAE6A3EC032052906188478E50D (String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NullReferenceException_tBDE63A6D24569B964908408389070C6A9F5005BB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_1;
		L_1 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		NullReferenceException_tBDE63A6D24569B964908408389070C6A9F5005BB* L_2 = (NullReferenceException_tBDE63A6D24569B964908408389070C6A9F5005BB*)il2cpp_codegen_object_new(NullReferenceException_tBDE63A6D24569B964908408389070C6A9F5005BB_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		NullReferenceException__ctor_mD00D7FE987C285C8DB23883700F44BC0025F55EF(L_2, L_0, L_1, NULL);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_2, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingOutOfMemoryException_mCED02F27DC5E0EDC86EAA55784361567D1D01CA6 (String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OutOfMemoryException_tE6DC2F937EC4A8699271D5151C4DF83BDE99EE7F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_1;
		L_1 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		OutOfMemoryException_tE6DC2F937EC4A8699271D5151C4DF83BDE99EE7F* L_2 = (OutOfMemoryException_tE6DC2F937EC4A8699271D5151C4DF83BDE99EE7F*)il2cpp_codegen_object_new(OutOfMemoryException_tE6DC2F937EC4A8699271D5151C4DF83BDE99EE7F_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		OutOfMemoryException__ctor_mC60E0CF8E50CA43F0518570ACC051F6BA8A1D1F2(L_2, L_0, L_1, NULL);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_2, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingOverflowException_mCA646C70DE90380B08954D135018002CE4A99E86 (String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OverflowException_t6F6AD8CACE20C37F701C05B373A215C4802FAB0C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_1;
		L_1 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		OverflowException_t6F6AD8CACE20C37F701C05B373A215C4802FAB0C* L_2 = (OverflowException_t6F6AD8CACE20C37F701C05B373A215C4802FAB0C*)il2cpp_codegen_object_new(OverflowException_t6F6AD8CACE20C37F701C05B373A215C4802FAB0C_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		OverflowException__ctor_mB00A07CDB7E5230B8D2BB31696E63F3CB1C36EF9(L_2, L_0, L_1, NULL);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_2, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingSystemException_mE0F156F2311B8F1ECDFE4267B73A0CE00DA9A01E (String_t* ___message0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_1;
		L_1 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295* L_2 = (SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295*)il2cpp_codegen_object_new(SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		SystemException__ctor_m0FC84CACD2A5D66222998AA601A5C41CEC36A611(L_2, L_0, L_1, NULL);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_2, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArgumentException_m59C2895E8334A034F0C59E34A3C14F39340478CC (String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		String_t* L_1 = ___paramName1;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_2;
		L_2 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_3 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		ArgumentException__ctor_m790F28810569425B0503056EF1A9CDDF9AFBB3F0(L_3, L_0, L_1, L_2, NULL);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_3, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArgumentNullException_m4B7CABD3A4D1075F6323D8EB69F7E6E8667665F8 (String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0B47BC9031A69F662702621810EC706E2C081467);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t* V_0 = NULL;
	bool V_1 = false;
	{
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_0;
		L_0 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		V_0 = L_0;
		Exception_t* L_1 = V_0;
		V_1 = (bool)((!(((RuntimeObject*)(Exception_t*)L_1) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_3 = ___message0;
		Exception_t* L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtualFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Exception::get_Message() */, L_4);
		String_t* L_6;
		L_6 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_3, _stringLiteral0B47BC9031A69F662702621810EC706E2C081467, L_5, NULL);
		___message0 = L_6;
	}

IL_0022:
	{
		String_t* L_7 = ___paramName1;
		String_t* L_8 = ___message0;
		ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129* L_9 = (ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129*)il2cpp_codegen_object_new(ArgumentNullException_t327031E412FAB2351B0022DD5DAD47E67E597129_il2cpp_TypeInfo_var);
		NullCheck(L_9);
		ArgumentNullException__ctor_m6D9C7B47EA708382838B264BA02EBB7576DFA155(L_9, L_7, L_8, NULL);
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_9, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mB01500F82308A09A74F016F7671A8E282A105C55 (String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0B47BC9031A69F662702621810EC706E2C081467);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t* V_0 = NULL;
	bool V_1 = false;
	{
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_0;
		L_0 = SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45(NULL);
		V_0 = L_0;
		Exception_t* L_1 = V_0;
		V_1 = (bool)((!(((RuntimeObject*)(Exception_t*)L_1) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_3 = ___message0;
		Exception_t* L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5;
		L_5 = VirtualFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Exception::get_Message() */, L_4);
		String_t* L_6;
		L_6 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_3, _stringLiteral0B47BC9031A69F662702621810EC706E2C081467, L_5, NULL);
		___message0 = L_6;
	}

IL_0022:
	{
		String_t* L_7 = ___paramName1;
		String_t* L_8 = ___message0;
		ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F* L_9 = (ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F*)il2cpp_codegen_object_new(ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F_il2cpp_TypeInfo_var);
		NullCheck(L_9);
		ArgumentOutOfRangeException__ctor_mE5B2755F0BEA043CACF915D5CE140859EE58FA66(L_9, L_7, L_8, NULL);
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D(L_9, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper__cctor_m51A53BA70DD46CD99F6A4970114FB6735A3D8EC7 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingApplicationException_m3389DA14B6470D215F411A6AB64161049D976B92_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingArgumentException_m59C2895E8334A034F0C59E34A3C14F39340478CC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingArgumentNullException_m4B7CABD3A4D1075F6323D8EB69F7E6E8667665F8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mB01500F82308A09A74F016F7671A8E282A105C55_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingArithmeticException_mDA9940C49B02A145257997D6E29939B520DECE30_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingDivideByZeroException_m2AC76C31521184D3E9CEA398D2304368C1C3F245_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingIOException_m1E8C16376B2B3701BAC90728915AAF17EC843685_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m9444EB341D36B5E18F7DD12453881B4E5D527B77_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingInvalidCastException_m56DA5D72BDA177C1E2E9FA830A03F6A315064907_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingInvalidOperationException_m8319A7B285528224F0D94E93683A03F8B2A1CF61_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingNullReferenceException_m5357E1A78F179FAE6A3EC032052906188478E50D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingOutOfMemoryException_mCED02F27DC5E0EDC86EAA55784361567D1D01CA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingOverflowException_mCA646C70DE90380B08954D135018002CE4A99E86_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_SetPendingSystemException_mE0F156F2311B8F1ECDFE4267B73A0CE00DA9A01E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_0 = (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19*)il2cpp_codegen_object_new(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		ExceptionDelegate__ctor_m86E2FD1C496126EFCE130E23D2CD3191BBA96FB7(L_0, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingApplicationException_m3389DA14B6470D215F411A6AB64161049D976B92_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___applicationDelegate_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___applicationDelegate_0), (void*)L_0);
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_1 = (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19*)il2cpp_codegen_object_new(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		ExceptionDelegate__ctor_m86E2FD1C496126EFCE130E23D2CD3191BBA96FB7(L_1, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingArithmeticException_mDA9940C49B02A145257997D6E29939B520DECE30_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___arithmeticDelegate_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___arithmeticDelegate_1), (void*)L_1);
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_2 = (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19*)il2cpp_codegen_object_new(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		ExceptionDelegate__ctor_m86E2FD1C496126EFCE130E23D2CD3191BBA96FB7(L_2, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingDivideByZeroException_m2AC76C31521184D3E9CEA398D2304368C1C3F245_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___divideByZeroDelegate_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___divideByZeroDelegate_2), (void*)L_2);
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_3 = (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19*)il2cpp_codegen_object_new(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		ExceptionDelegate__ctor_m86E2FD1C496126EFCE130E23D2CD3191BBA96FB7(L_3, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m9444EB341D36B5E18F7DD12453881B4E5D527B77_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___indexOutOfRangeDelegate_3 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___indexOutOfRangeDelegate_3), (void*)L_3);
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_4 = (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19*)il2cpp_codegen_object_new(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		ExceptionDelegate__ctor_m86E2FD1C496126EFCE130E23D2CD3191BBA96FB7(L_4, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingInvalidCastException_m56DA5D72BDA177C1E2E9FA830A03F6A315064907_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___invalidCastDelegate_4 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___invalidCastDelegate_4), (void*)L_4);
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_5 = (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19*)il2cpp_codegen_object_new(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		ExceptionDelegate__ctor_m86E2FD1C496126EFCE130E23D2CD3191BBA96FB7(L_5, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingInvalidOperationException_m8319A7B285528224F0D94E93683A03F8B2A1CF61_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___invalidOperationDelegate_5 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___invalidOperationDelegate_5), (void*)L_5);
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_6 = (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19*)il2cpp_codegen_object_new(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		ExceptionDelegate__ctor_m86E2FD1C496126EFCE130E23D2CD3191BBA96FB7(L_6, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingIOException_m1E8C16376B2B3701BAC90728915AAF17EC843685_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___ioDelegate_6 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___ioDelegate_6), (void*)L_6);
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_7 = (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19*)il2cpp_codegen_object_new(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19_il2cpp_TypeInfo_var);
		NullCheck(L_7);
		ExceptionDelegate__ctor_m86E2FD1C496126EFCE130E23D2CD3191BBA96FB7(L_7, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingNullReferenceException_m5357E1A78F179FAE6A3EC032052906188478E50D_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___nullReferenceDelegate_7 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___nullReferenceDelegate_7), (void*)L_7);
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_8 = (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19*)il2cpp_codegen_object_new(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19_il2cpp_TypeInfo_var);
		NullCheck(L_8);
		ExceptionDelegate__ctor_m86E2FD1C496126EFCE130E23D2CD3191BBA96FB7(L_8, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingOutOfMemoryException_mCED02F27DC5E0EDC86EAA55784361567D1D01CA6_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___outOfMemoryDelegate_8 = L_8;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___outOfMemoryDelegate_8), (void*)L_8);
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_9 = (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19*)il2cpp_codegen_object_new(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19_il2cpp_TypeInfo_var);
		NullCheck(L_9);
		ExceptionDelegate__ctor_m86E2FD1C496126EFCE130E23D2CD3191BBA96FB7(L_9, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingOverflowException_mCA646C70DE90380B08954D135018002CE4A99E86_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___overflowDelegate_9 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___overflowDelegate_9), (void*)L_9);
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_10 = (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19*)il2cpp_codegen_object_new(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19_il2cpp_TypeInfo_var);
		NullCheck(L_10);
		ExceptionDelegate__ctor_m86E2FD1C496126EFCE130E23D2CD3191BBA96FB7(L_10, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingSystemException_mE0F156F2311B8F1ECDFE4267B73A0CE00DA9A01E_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___systemDelegate_10 = L_10;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___systemDelegate_10), (void*)L_10);
		ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* L_11 = (ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6*)il2cpp_codegen_object_new(ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6_il2cpp_TypeInfo_var);
		NullCheck(L_11);
		ExceptionArgumentDelegate__ctor_m69BA8206F7E1253E502E36629F0AD6E37282EE6A(L_11, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingArgumentException_m59C2895E8334A034F0C59E34A3C14F39340478CC_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___argumentDelegate_11 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___argumentDelegate_11), (void*)L_11);
		ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* L_12 = (ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6*)il2cpp_codegen_object_new(ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6_il2cpp_TypeInfo_var);
		NullCheck(L_12);
		ExceptionArgumentDelegate__ctor_m69BA8206F7E1253E502E36629F0AD6E37282EE6A(L_12, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingArgumentNullException_m4B7CABD3A4D1075F6323D8EB69F7E6E8667665F8_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___argumentNullDelegate_12 = L_12;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___argumentNullDelegate_12), (void*)L_12);
		ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* L_13 = (ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6*)il2cpp_codegen_object_new(ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6_il2cpp_TypeInfo_var);
		NullCheck(L_13);
		ExceptionArgumentDelegate__ctor_m69BA8206F7E1253E502E36629F0AD6E37282EE6A(L_13, NULL, (intptr_t)((void*)SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mB01500F82308A09A74F016F7671A8E282A105C55_RuntimeMethod_var), NULL);
		((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___argumentOutOfRangeDelegate_13 = L_13;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___argumentOutOfRangeDelegate_13), (void*)L_13);
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_14 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___applicationDelegate_0;
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_15 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___arithmeticDelegate_1;
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_16 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___divideByZeroDelegate_2;
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_17 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___indexOutOfRangeDelegate_3;
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_18 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___invalidCastDelegate_4;
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_19 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___invalidOperationDelegate_5;
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_20 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___ioDelegate_6;
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_21 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___nullReferenceDelegate_7;
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_22 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___outOfMemoryDelegate_8;
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_23 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___overflowDelegate_9;
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* L_24 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___systemDelegate_10;
		SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_StorageInternal_mC4703B7B2BCFF2FD96F4400F20636806BE454B4B(L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, L_22, L_23, L_24, NULL);
		ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* L_25 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___argumentDelegate_11;
		ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* L_26 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___argumentNullDelegate_12;
		ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* L_27 = ((SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149_il2cpp_TypeInfo_var))->___argumentOutOfRangeDelegate_13;
		SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_StorageInternal_mB84CA4E78B9898DA55BD54308320D9DAEE71D6C2(L_25, L_26, L_27, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper__ctor_mAD73726452D2B9A7610ED82C4B6868D0C424C849 (SWIGExceptionHelper_t5BA09F4EB956CA01C11D6B0C4F1BE79602082149* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
void ExceptionDelegate_Invoke_m7248F2A105990113CFEAB586BF0E4F868E753F27_Multicast(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* __this, String_t* ___message0, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* currentDelegate = reinterpret_cast<ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19*>(delegatesToInvoke[i]);
		typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
		((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___message0, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
}
void ExceptionDelegate_Invoke_m7248F2A105990113CFEAB586BF0E4F868E753F27_OpenInst(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* __this, String_t* ___message0, const RuntimeMethod* method)
{
	NullCheck(___message0);
	typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___message0, method);
}
void ExceptionDelegate_Invoke_m7248F2A105990113CFEAB586BF0E4F868E753F27_OpenStatic(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* __this, String_t* ___message0, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___message0, method);
}
void ExceptionDelegate_Invoke_m7248F2A105990113CFEAB586BF0E4F868E753F27_OpenStaticInvoker(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* __this, String_t* ___message0, const RuntimeMethod* method)
{
	InvokerActionInvoker1< String_t* >::Invoke(__this->___method_ptr_0, method, NULL, ___message0);
}
void ExceptionDelegate_Invoke_m7248F2A105990113CFEAB586BF0E4F868E753F27_ClosedStaticInvoker(ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* __this, String_t* ___message0, const RuntimeMethod* method)
{
	InvokerActionInvoker2< RuntimeObject*, String_t* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___message0);
}
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19 (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* __this, String_t* ___message0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_reverse_pinvoke_function_ptr(__this));
	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionDelegate__ctor_m86E2FD1C496126EFCE130E23D2CD3191BBA96FB7 (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___method1);
	__this->___method_3 = ___method1;
	__this->___m_target_2 = ___object0;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___object0);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___method1);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___method1))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___method1))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&ExceptionDelegate_Invoke_m7248F2A105990113CFEAB586BF0E4F868E753F27_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&ExceptionDelegate_Invoke_m7248F2A105990113CFEAB586BF0E4F868E753F27_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&ExceptionDelegate_Invoke_m7248F2A105990113CFEAB586BF0E4F868E753F27_OpenStatic;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 0;
		if (isOpen)
		{
			__this->___invoke_impl_1 = (intptr_t)&ExceptionDelegate_Invoke_m7248F2A105990113CFEAB586BF0E4F868E753F27_OpenInst;
		}
		else
		{
			if (___object0 == NULL)
				il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&ExceptionDelegate_Invoke_m7248F2A105990113CFEAB586BF0E4F868E753F27_Multicast;
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionDelegate_Invoke_m7248F2A105990113CFEAB586BF0E4F868E753F27 (ExceptionDelegate_t864B45B2FEC8AA06D829B518F169BE4212EF7D19* __this, String_t* ___message0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___message0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
void ExceptionArgumentDelegate_Invoke_m33E23E680B50CD43FEB46921260C0E9C4AA8DE56_Multicast(ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* currentDelegate = reinterpret_cast<ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6*>(delegatesToInvoke[i]);
		typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, String_t*, const RuntimeMethod*);
		((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___message0, ___paramName1, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
}
void ExceptionArgumentDelegate_Invoke_m33E23E680B50CD43FEB46921260C0E9C4AA8DE56_OpenInst(ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	NullCheck(___message0);
	typedef void (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___message0, ___paramName1, method);
}
void ExceptionArgumentDelegate_Invoke_m33E23E680B50CD43FEB46921260C0E9C4AA8DE56_OpenStatic(ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	typedef void (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___method_ptr_0)(___message0, ___paramName1, method);
}
void ExceptionArgumentDelegate_Invoke_m33E23E680B50CD43FEB46921260C0E9C4AA8DE56_OpenStaticInvoker(ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	InvokerActionInvoker2< String_t*, String_t* >::Invoke(__this->___method_ptr_0, method, NULL, ___message0, ___paramName1);
}
void ExceptionArgumentDelegate_Invoke_m33E23E680B50CD43FEB46921260C0E9C4AA8DE56_ClosedStaticInvoker(ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	InvokerActionInvoker3< RuntimeObject*, String_t*, String_t* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___message0, ___paramName1);
}
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6 (ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_reverse_pinvoke_function_ptr(__this));
	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Marshaling of parameter '___paramName1' to native representation
	char* ____paramName1_marshaled = NULL;
	____paramName1_marshaled = il2cpp_codegen_marshal_string(___paramName1);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled, ____paramName1_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramName1' native representation
	il2cpp_codegen_marshal_free(____paramName1_marshaled);
	____paramName1_marshaled = NULL;

}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionArgumentDelegate__ctor_m69BA8206F7E1253E502E36629F0AD6E37282EE6A (ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___method1);
	__this->___method_3 = ___method1;
	__this->___m_target_2 = ___object0;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___object0);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___method1);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___method1))
	{
		bool isOpen = parameterCount == 2;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___method1))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&ExceptionArgumentDelegate_Invoke_m33E23E680B50CD43FEB46921260C0E9C4AA8DE56_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&ExceptionArgumentDelegate_Invoke_m33E23E680B50CD43FEB46921260C0E9C4AA8DE56_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&ExceptionArgumentDelegate_Invoke_m33E23E680B50CD43FEB46921260C0E9C4AA8DE56_OpenStatic;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 1;
		if (isOpen)
		{
			__this->___invoke_impl_1 = (intptr_t)&ExceptionArgumentDelegate_Invoke_m33E23E680B50CD43FEB46921260C0E9C4AA8DE56_OpenInst;
		}
		else
		{
			if (___object0 == NULL)
				il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&ExceptionArgumentDelegate_Invoke_m33E23E680B50CD43FEB46921260C0E9C4AA8DE56_Multicast;
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionArgumentDelegate_Invoke_m33E23E680B50CD43FEB46921260C0E9C4AA8DE56 (ExceptionArgumentDelegate_tDDBE478926FBDBFB7AF2E13ECD8995E728DD58F6* __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, String_t*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___message0, ___paramName1, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGPendingException::Set(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D (Exception_t* ___e0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RuntimeObject* V_1 = NULL;
	bool V_2 = false;
	{
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_0 = ((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___pendingException_0;
		V_0 = (bool)((!(((RuntimeObject*)(Exception_t*)L_0) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var)));
		Exception_t* L_2 = ((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))))->___pendingException_0;
		NullCheck(L_2);
		String_t* L_3;
		L_3 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		String_t* L_4;
		L_4 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralCB9902E2A4DFEBC0D9FA90B929984CE8637D6BAE)), L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralB3F14BF976EFD974E34846B742502C802FABAE9D)), NULL);
		Exception_t* L_5 = ___e0;
		ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A* L_6 = (ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ApplicationException_tA744BED4E90266BD255285CD4CF909BAB3EE811A_il2cpp_TypeInfo_var)));
		NullCheck(L_6);
		ApplicationException__ctor_m924E77609BAFA0595453363EB8B7BCCBA03B32DD(L_6, L_4, L_5, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SWIGPendingException_Set_m6D152D0AC36275EEC6C61496F301B837C45DB93D_RuntimeMethod_var)));
	}

IL_002d:
	{
		Exception_t* L_7 = ___e0;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___pendingException_0 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___pendingException_0), (void*)L_7);
		RuntimeObject* L_8 = ((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___exceptionsLock_2;
		V_1 = L_8;
		V_2 = (bool)0;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0054:
			{// begin finally (depth: 1)
				{
					bool L_9 = V_2;
					if (!L_9)
					{
						goto IL_005e;
					}
				}
				{
					RuntimeObject* L_10 = V_1;
					Monitor_Exit_m25A154960F91391E10E4CDA245ECDF4BA94D56A9(L_10, NULL);
				}

IL_005e:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			RuntimeObject* L_11 = V_1;
			Monitor_Enter_m00506757392936AA62DBE2C5FFBEE69EE920C4D4(L_11, (&V_2), NULL);
			il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
			int32_t L_12 = ((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___numExceptionsPending_1;
			((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___numExceptionsPending_1 = ((int32_t)il2cpp_codegen_add(L_12, 1));
			goto IL_005f;
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_005f:
	{
		return;
	}
}
// System.Exception Firebase.Storage.StorageInternalPINVOKE/SWIGPendingException::Retrieve()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t* SWIGPendingException_Retrieve_m8FD402E8AB2565284626548D7D8AC15F6CB5EC45 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	RuntimeObject* V_3 = NULL;
	bool V_4 = false;
	Exception_t* V_5 = NULL;
	{
		V_0 = (Exception_t*)NULL;
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		int32_t L_0 = ((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___numExceptionsPending_1;
		V_1 = (bool)((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_0059;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_2 = ((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___pendingException_0;
		V_2 = (bool)((!(((RuntimeObject*)(Exception_t*)L_2) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_3 = V_2;
		if (!L_3)
		{
			goto IL_0058;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		Exception_t* L_4 = ((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___pendingException_0;
		V_0 = L_4;
		((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___pendingException_0 = (Exception_t*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___pendingException_0), (void*)(Exception_t*)NULL);
		RuntimeObject* L_5 = ((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___exceptionsLock_2;
		V_3 = L_5;
		V_4 = (bool)0;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_004b:
			{// begin finally (depth: 1)
				{
					bool L_6 = V_4;
					if (!L_6)
					{
						goto IL_0056;
					}
				}
				{
					RuntimeObject* L_7 = V_3;
					Monitor_Exit_m25A154960F91391E10E4CDA245ECDF4BA94D56A9(L_7, NULL);
				}

IL_0056:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			RuntimeObject* L_8 = V_3;
			Monitor_Enter_m00506757392936AA62DBE2C5FFBEE69EE920C4D4(L_8, (&V_4), NULL);
			il2cpp_codegen_runtime_class_init_inline(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
			int32_t L_9 = ((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___numExceptionsPending_1;
			((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___numExceptionsPending_1 = ((int32_t)il2cpp_codegen_subtract(L_9, 1));
			goto IL_0057;
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0057:
	{
	}

IL_0058:
	{
	}

IL_0059:
	{
		Exception_t* L_10 = V_0;
		V_5 = L_10;
		goto IL_005e;
	}

IL_005e:
	{
		Exception_t* L_11 = V_5;
		return L_11;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGPendingException::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGPendingException__cctor_mACA1C5A30DD4976578EF0D4D472519B2FDF35BF1 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RuntimeObject_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___pendingException_0 = (Exception_t*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___pendingException_0), (void*)(Exception_t*)NULL);
		((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___numExceptionsPending_1 = 0;
		((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___exceptionsLock_2 = NULL;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___exceptionsLock_2), (void*)NULL);
		RuntimeObject* L_0 = (RuntimeObject*)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(L_0, NULL);
		((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___exceptionsLock_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t82C1E0B8C275723AC4C5A3E6C83B9DBACF4256CB_il2cpp_TypeInfo_var))->___exceptionsLock_2), (void*)L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_SWIGStringHelper_CreateString_m403C46AC9193D0E161E7752F5C6D16DE4F670596(char* ___cString0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___cString0' to managed representation
	String_t* ____cString0_unmarshaled = NULL;
	____cString0_unmarshaled = il2cpp_codegen_marshal_string_result(___cString0);

	// Managed method invocation
	String_t* returnValue;
	returnValue = SWIGStringHelper_CreateString_m403C46AC9193D0E161E7752F5C6D16DE4F670596(____cString0_unmarshaled, NULL);

	// Marshaling of return value back from managed representation
	char* _returnValue_marshaled = NULL;
	_returnValue_marshaled = il2cpp_codegen_marshal_string(returnValue);

	return _returnValue_marshaled;
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_StorageInternal(Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper/SWIGStringDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringHelper_SWIGRegisterStringCallback_StorageInternal_mB88DFA747A335290130176E9A96A87A18A966CBB (SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* ___stringDelegate0, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("FirebaseCppApp-11_2_0"), "SWIGRegisterStringCallback_StorageInternal", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___stringDelegate0' to native representation
	Il2CppMethodPointer ____stringDelegate0_marshaled = NULL;
	____stringDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___stringDelegate0));

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_FirebaseCppApp_11_2_0_INTERNAL
	reinterpret_cast<PInvokeFunc>(SWIGRegisterStringCallback_StorageInternal)(____stringDelegate0_marshaled);
	#else
	il2cppPInvokeFunc(____stringDelegate0_marshaled);
	#endif

}
// System.String Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper::CreateString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SWIGStringHelper_CreateString_m403C46AC9193D0E161E7752F5C6D16DE4F670596 (String_t* ___cString0, const RuntimeMethod* method) 
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___cString0;
		V_0 = L_0;
		goto IL_0005;
	}

IL_0005:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringHelper__cctor_m79D42DDCD1ABD1AE40DD550F8CCACC5D497C1963 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGStringHelper_CreateString_m403C46AC9193D0E161E7752F5C6D16DE4F670596_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* L_0 = (SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261*)il2cpp_codegen_object_new(SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		SWIGStringDelegate__ctor_mC5F68D7CA10A84812D4C8AF241FC7628F3EB0AD4(L_0, NULL, (intptr_t)((void*)SWIGStringHelper_CreateString_m403C46AC9193D0E161E7752F5C6D16DE4F670596_RuntimeMethod_var), NULL);
		((SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004_StaticFields*)il2cpp_codegen_static_fields_for(SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004_il2cpp_TypeInfo_var))->___stringDelegate_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004_StaticFields*)il2cpp_codegen_static_fields_for(SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004_il2cpp_TypeInfo_var))->___stringDelegate_0), (void*)L_0);
		SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* L_1 = ((SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004_StaticFields*)il2cpp_codegen_static_fields_for(SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004_il2cpp_TypeInfo_var))->___stringDelegate_0;
		SWIGStringHelper_SWIGRegisterStringCallback_StorageInternal_mB88DFA747A335290130176E9A96A87A18A966CBB(L_1, NULL);
		return;
	}
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringHelper__ctor_m742C1F69A35C651EA7707D33CFDCF5C602DD81E3 (SWIGStringHelper_t47662AC3D77A20894A881C537230C8309E556004* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
String_t* SWIGStringDelegate_Invoke_m3D7648538AED70BD4A337DCE5D52E729A9F061C7_Multicast(SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* __this, String_t* ___message0, const RuntimeMethod* method)
{
	il2cpp_array_size_t length = __this->___delegates_13->max_length;
	Delegate_t** delegatesToInvoke = reinterpret_cast<Delegate_t**>(__this->___delegates_13->GetAddressAtUnchecked(0));
	String_t* retVal = NULL;
	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* currentDelegate = reinterpret_cast<SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261*>(delegatesToInvoke[i]);
		typedef String_t* (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
		retVal = ((FunctionPointerType)currentDelegate->___invoke_impl_1)((Il2CppObject*)currentDelegate->___method_code_6, ___message0, reinterpret_cast<RuntimeMethod*>(currentDelegate->___method_3));
	}
	return retVal;
}
String_t* SWIGStringDelegate_Invoke_m3D7648538AED70BD4A337DCE5D52E729A9F061C7_OpenInst(SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* __this, String_t* ___message0, const RuntimeMethod* method)
{
	NullCheck(___message0);
	typedef String_t* (*FunctionPointerType) (String_t*, const RuntimeMethod*);
	return ((FunctionPointerType)__this->___method_ptr_0)(___message0, method);
}
String_t* SWIGStringDelegate_Invoke_m3D7648538AED70BD4A337DCE5D52E729A9F061C7_OpenStatic(SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* __this, String_t* ___message0, const RuntimeMethod* method)
{
	typedef String_t* (*FunctionPointerType) (String_t*, const RuntimeMethod*);
	return ((FunctionPointerType)__this->___method_ptr_0)(___message0, method);
}
String_t* SWIGStringDelegate_Invoke_m3D7648538AED70BD4A337DCE5D52E729A9F061C7_OpenStaticInvoker(SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* __this, String_t* ___message0, const RuntimeMethod* method)
{
	return InvokerFuncInvoker1< String_t*, String_t* >::Invoke(__this->___method_ptr_0, method, NULL, ___message0);
}
String_t* SWIGStringDelegate_Invoke_m3D7648538AED70BD4A337DCE5D52E729A9F061C7_ClosedStaticInvoker(SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* __this, String_t* ___message0, const RuntimeMethod* method)
{
	return InvokerFuncInvoker2< String_t*, RuntimeObject*, String_t* >::Invoke(__this->___method_ptr_0, method, NULL, __this->___m_target_2, ___message0);
}
IL2CPP_EXTERN_C  String_t* DelegatePInvokeWrapper_SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261 (SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* __this, String_t* ___message0, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_reverse_pinvoke_function_ptr(__this));
	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	char* returnValue = il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.Void Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringDelegate__ctor_mC5F68D7CA10A84812D4C8AF241FC7628F3EB0AD4 (SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) 
{
	__this->___method_ptr_0 = il2cpp_codegen_get_virtual_call_method_pointer((RuntimeMethod*)___method1);
	__this->___method_3 = ___method1;
	__this->___m_target_2 = ___object0;
	Il2CppCodeGenWriteBarrier((void**)(&__this->___m_target_2), (void*)___object0);
	int parameterCount = il2cpp_codegen_method_parameter_count((RuntimeMethod*)___method1);
	__this->___method_code_6 = (intptr_t)__this;
	if (MethodIsStatic((RuntimeMethod*)___method1))
	{
		bool isOpen = parameterCount == 1;
		if (il2cpp_codegen_call_method_via_invoker((RuntimeMethod*)___method1))
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&SWIGStringDelegate_Invoke_m3D7648538AED70BD4A337DCE5D52E729A9F061C7_OpenStaticInvoker;
			else
				__this->___invoke_impl_1 = (intptr_t)&SWIGStringDelegate_Invoke_m3D7648538AED70BD4A337DCE5D52E729A9F061C7_ClosedStaticInvoker;
		else
			if (isOpen)
				__this->___invoke_impl_1 = (intptr_t)&SWIGStringDelegate_Invoke_m3D7648538AED70BD4A337DCE5D52E729A9F061C7_OpenStatic;
			else
				{
					__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
					__this->___method_code_6 = (intptr_t)__this->___m_target_2;
				}
	}
	else
	{
		bool isOpen = parameterCount == 0;
		if (isOpen)
		{
			__this->___invoke_impl_1 = (intptr_t)&SWIGStringDelegate_Invoke_m3D7648538AED70BD4A337DCE5D52E729A9F061C7_OpenInst;
		}
		else
		{
			if (___object0 == NULL)
				il2cpp_codegen_raise_exception(il2cpp_codegen_get_argument_exception(NULL, "Delegate to an instance method cannot have null 'this'."), NULL);
			__this->___invoke_impl_1 = (intptr_t)__this->___method_ptr_0;
			__this->___method_code_6 = (intptr_t)__this->___m_target_2;
		}
	}
	__this->___extra_arg_5 = (intptr_t)&SWIGStringDelegate_Invoke_m3D7648538AED70BD4A337DCE5D52E729A9F061C7_Multicast;
}
// System.String Firebase.Storage.StorageInternalPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SWIGStringDelegate_Invoke_m3D7648538AED70BD4A337DCE5D52E729A9F061C7 (SWIGStringDelegate_tA75D6F6E5765D9464CB42CC82F834CACB639C261* __this, String_t* ___message0, const RuntimeMethod* method) 
{
	typedef String_t* (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
	return ((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___message0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Storage.Internal.ModuleLogger::.ctor(Firebase.Storage.Internal.ModuleLogger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModuleLogger__ctor_m64B63C3ABE700A1F9BA947FFEE57605EBB4C5850 (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* __this, ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___parentLogger0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m14B2E734DD76D26DE4F1C83D6EB500BE400DE1B9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m25F4DF4DE179873ACDA24F3C314479DDBCCC0BDD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mF9FFC2B904A1557D19170DC33F3A326E18082C76_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	{
		List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE* L_0 = (List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE*)il2cpp_codegen_object_new(List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		List_1__ctor_mF9FFC2B904A1557D19170DC33F3A326E18082C76(L_0, List_1__ctor_mF9FFC2B904A1557D19170DC33F3A326E18082C76_RuntimeMethod_var);
		__this->___children_3 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___children_3), (void*)L_0);
		il2cpp_codegen_runtime_class_init_inline(FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = FirebaseApp_get_LogLevel_m50774F8027C8BF6EB481CBFCCCFC266863392D83(NULL);
		__this->___logLevel_5 = L_1;
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		RuntimeObject* L_2 = ((ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_StaticFields*)il2cpp_codegen_static_fields_for(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var))->___lockObject_0;
		V_0 = L_2;
		V_1 = (bool)0;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_006c:
			{// begin finally (depth: 1)
				{
					bool L_3 = V_1;
					if (!L_3)
					{
						goto IL_0076;
					}
				}
				{
					RuntimeObject* L_4 = V_0;
					Monitor_Exit_m25A154960F91391E10E4CDA245ECDF4BA94D56A9(L_4, NULL);
				}

IL_0076:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				RuntimeObject* L_5 = V_0;
				Monitor_Enter_m00506757392936AA62DBE2C5FFBEE69EE920C4D4(L_5, (&V_1), NULL);
				ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_6 = ___parentLogger0;
				V_2 = (bool)((((RuntimeObject*)(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*)L_6) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
				bool L_7 = V_2;
				if (!L_7)
				{
					goto IL_004e_1;
				}
			}
			{
				il2cpp_codegen_runtime_class_init_inline(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
				List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9* L_8 = ((ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_StaticFields*)il2cpp_codegen_static_fields_for(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var))->___roots_1;
				WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* L_9 = (WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E*)il2cpp_codegen_object_new(WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E_il2cpp_TypeInfo_var);
				NullCheck(L_9);
				WeakReference__ctor_m8085B7DB432EB4B11F2FFDB543B3F1D05D4A8D99(L_9, __this, (bool)0, NULL);
				NullCheck(L_8);
				List_1_Add_m25F4DF4DE179873ACDA24F3C314479DDBCCC0BDD_inline(L_8, L_9, List_1_Add_m25F4DF4DE179873ACDA24F3C314479DDBCCC0BDD_RuntimeMethod_var);
				goto IL_0069_1;
			}

IL_004e_1:
			{
				ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_10 = ___parentLogger0;
				__this->___parent_2 = L_10;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___parent_2), (void*)L_10);
				ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_11 = __this->___parent_2;
				NullCheck(L_11);
				List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE* L_12 = L_11->___children_3;
				NullCheck(L_12);
				List_1_Add_m14B2E734DD76D26DE4F1C83D6EB500BE400DE1B9_inline(L_12, __this, List_1_Add_m14B2E734DD76D26DE4F1C83D6EB500BE400DE1B9_RuntimeMethod_var);
			}

IL_0069_1:
			{
				goto IL_0077;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0077:
	{
		return;
	}
}
// System.Void Firebase.Storage.Internal.ModuleLogger::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModuleLogger_Finalize_mC6B7BF79CCD9A25BA7A164BB1531360A4068036D (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m09AD6CD926C4F2DA33BBE74EA133F80BC1F7AC46_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m6E9D8FDB919C8320D80F7597574FFE9F6D0D88D3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m06D75C6DF4330942019CEC566CE1F77C08873BB9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7F0C23273635833A4D046A87EF3F3731C79A22B7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m7E4BCB7F0652FE05E987A6B3EF0D4F35AA775886_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_mE10942BAF61F258192575C0A6044AADF5D08BB5B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	Enumerator_tA83B52184A87D2620C50E08DA33C36DB659322C7 V_3;
	memset((&V_3), 0, sizeof(V_3));
	WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* V_4 = NULL;
	ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* V_5 = NULL;
	bool V_6 = false;
	{
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00ac:
			{// begin finally (depth: 1)
				Object_Finalize_mC98C96301CCABFE00F1A7EF8E15DF507CACD42B2(__this, NULL);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				il2cpp_codegen_runtime_class_init_inline(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
				RuntimeObject* L_0 = ((ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_StaticFields*)il2cpp_codegen_static_fields_for(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var))->___lockObject_0;
				V_0 = L_0;
				V_1 = (bool)0;
			}
			{
				auto __finallyBlock = il2cpp::utils::Finally([&]
				{

FINALLY_009f_1:
					{// begin finally (depth: 2)
						{
							bool L_1 = V_1;
							if (!L_1)
							{
								goto IL_00a9_1;
							}
						}
						{
							RuntimeObject* L_2 = V_0;
							Monitor_Exit_m25A154960F91391E10E4CDA245ECDF4BA94D56A9(L_2, NULL);
						}

IL_00a9_1:
						{
							return;
						}
					}// end finally (depth: 2)
				});
				try
				{// begin try (depth: 2)
					{
						RuntimeObject* L_3 = V_0;
						Monitor_Enter_m00506757392936AA62DBE2C5FFBEE69EE920C4D4(L_3, (&V_1), NULL);
						ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_4 = __this->___parent_2;
						V_2 = (bool)((((RuntimeObject*)(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*)L_4) == ((RuntimeObject*)(RuntimeObject*)NULL))? 1 : 0);
						bool L_5 = V_2;
						if (!L_5)
						{
							goto IL_0081_2;
						}
					}
					{
						il2cpp_codegen_runtime_class_init_inline(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
						List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9* L_6 = ((ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_StaticFields*)il2cpp_codegen_static_fields_for(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var))->___roots_1;
						NullCheck(L_6);
						Enumerator_tA83B52184A87D2620C50E08DA33C36DB659322C7 L_7;
						L_7 = List_1_GetEnumerator_m7F0C23273635833A4D046A87EF3F3731C79A22B7(L_6, List_1_GetEnumerator_m7F0C23273635833A4D046A87EF3F3731C79A22B7_RuntimeMethod_var);
						V_3 = L_7;
					}
					{
						auto __finallyBlock = il2cpp::utils::Finally([&]
						{

FINALLY_006f_2:
							{// begin finally (depth: 3)
								Enumerator_Dispose_m09AD6CD926C4F2DA33BBE74EA133F80BC1F7AC46((&V_3), Enumerator_Dispose_m09AD6CD926C4F2DA33BBE74EA133F80BC1F7AC46_RuntimeMethod_var);
								return;
							}// end finally (depth: 3)
						});
						try
						{// begin try (depth: 3)
							{
								goto IL_0064_3;
							}

IL_0030_3:
							{
								WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* L_8;
								L_8 = Enumerator_get_Current_m06D75C6DF4330942019CEC566CE1F77C08873BB9_inline((&V_3), Enumerator_get_Current_m06D75C6DF4330942019CEC566CE1F77C08873BB9_RuntimeMethod_var);
								V_4 = L_8;
								WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* L_9 = V_4;
								il2cpp_codegen_runtime_class_init_inline(FirebaseApp_tD23C437863A3502177988D1382B58820B0571A25_il2cpp_TypeInfo_var);
								RuntimeObject* L_10;
								L_10 = FirebaseApp_WeakReferenceGetTarget_m48BCAA7BDF44B90AFC6B68BE061F879F21BDEA5D(L_9, NULL);
								V_5 = ((ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*)IsInstClass((RuntimeObject*)L_10, ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var));
								ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_11 = V_5;
								V_6 = (bool)((((RuntimeObject*)(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*)L_11) == ((RuntimeObject*)(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*)__this))? 1 : 0);
								bool L_12 = V_6;
								if (!L_12)
								{
									goto IL_0063_3;
								}
							}
							{
								il2cpp_codegen_runtime_class_init_inline(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
								List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9* L_13 = ((ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_StaticFields*)il2cpp_codegen_static_fields_for(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var))->___roots_1;
								WeakReference_tD4B0518CE911FFD9FAAB3FCD492644A354312D8E* L_14 = V_4;
								NullCheck(L_13);
								bool L_15;
								L_15 = List_1_Remove_mE10942BAF61F258192575C0A6044AADF5D08BB5B(L_13, L_14, List_1_Remove_mE10942BAF61F258192575C0A6044AADF5D08BB5B_RuntimeMethod_var);
								goto IL_006d_3;
							}

IL_0063_3:
							{
							}

IL_0064_3:
							{
								bool L_16;
								L_16 = Enumerator_MoveNext_m6E9D8FDB919C8320D80F7597574FFE9F6D0D88D3((&V_3), Enumerator_MoveNext_m6E9D8FDB919C8320D80F7597574FFE9F6D0D88D3_RuntimeMethod_var);
								if (L_16)
								{
									goto IL_0030_3;
								}
							}

IL_006d_3:
							{
								goto IL_007e_2;
							}
						}// end try (depth: 3)
						catch(Il2CppExceptionWrapper& e)
						{
							__finallyBlock.StoreException(e.ex);
						}
					}

IL_007e_2:
					{
						goto IL_009c_2;
					}

IL_0081_2:
					{
						ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_17 = __this->___parent_2;
						NullCheck(L_17);
						List_1_tF997183A3F3A4ECD79ABEBF865E954B8D93D73FE* L_18 = L_17->___children_3;
						NullCheck(L_18);
						bool L_19;
						L_19 = List_1_Remove_m7E4BCB7F0652FE05E987A6B3EF0D4F35AA775886(L_18, __this, List_1_Remove_m7E4BCB7F0652FE05E987A6B3EF0D4F35AA775886_RuntimeMethod_var);
						__this->___parent_2 = (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*)NULL;
						Il2CppCodeGenWriteBarrier((void**)(&__this->___parent_2), (void*)(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*)NULL);
					}

IL_009c_2:
					{
						goto IL_00aa_1;
					}
				}// end try (depth: 2)
				catch(Il2CppExceptionWrapper& e)
				{
					__finallyBlock.StoreException(e.ex);
				}
			}

IL_00aa_1:
			{
				goto IL_00b4;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00b4:
	{
		return;
	}
}
// Firebase.LogLevel Firebase.Storage.Internal.ModuleLogger::get_Level()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ModuleLogger_get_Level_m0AEE610FD8DA98169E801F769BC567589DE54791 (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject* V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	int32_t V_4 = 0;
	bool V_5 = false;
	int32_t V_6 = 0;
	{
		int32_t L_0 = __this->___logLevel_5;
		V_0 = L_0;
		il2cpp_codegen_runtime_class_init_inline(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = ((ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_StaticFields*)il2cpp_codegen_static_fields_for(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var))->___lockObject_0;
		V_1 = L_1;
		V_2 = (bool)0;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0047:
			{// begin finally (depth: 1)
				{
					bool L_2 = V_2;
					if (!L_2)
					{
						goto IL_0051;
					}
				}
				{
					RuntimeObject* L_3 = V_1;
					Monitor_Exit_m25A154960F91391E10E4CDA245ECDF4BA94D56A9(L_3, NULL);
				}

IL_0051:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				RuntimeObject* L_4 = V_1;
				Monitor_Enter_m00506757392936AA62DBE2C5FFBEE69EE920C4D4(L_4, (&V_2), NULL);
				ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_5 = __this->___parent_2;
				V_3 = (bool)((!(((RuntimeObject*)(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*)L_5) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
				bool L_6 = V_3;
				if (!L_6)
				{
					goto IL_0044_1;
				}
			}
			{
				ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_7 = __this->___parent_2;
				NullCheck(L_7);
				int32_t L_8;
				L_8 = ModuleLogger_get_Level_m0AEE610FD8DA98169E801F769BC567589DE54791(L_7, NULL);
				V_4 = L_8;
				int32_t L_9 = V_4;
				int32_t L_10 = V_0;
				V_5 = (bool)((((int32_t)L_9) < ((int32_t)L_10))? 1 : 0);
				bool L_11 = V_5;
				if (!L_11)
				{
					goto IL_0043_1;
				}
			}
			{
				int32_t L_12 = V_4;
				V_0 = L_12;
			}

IL_0043_1:
			{
			}

IL_0044_1:
			{
				goto IL_0052;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0052:
	{
		int32_t L_13 = V_0;
		V_6 = L_13;
		goto IL_0057;
	}

IL_0057:
	{
		int32_t L_14 = V_6;
		return L_14;
	}
}
// System.Void Firebase.Storage.Internal.ModuleLogger::set_Level(Firebase.LogLevel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModuleLogger_set_Level_m1DC27F4C2EC96609D9050B868F8B71467AB4A916 (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___value0;
		__this->___logLevel_5 = L_0;
		return;
	}
}
// System.String Firebase.Storage.Internal.ModuleLogger::get_Tag()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ModuleLogger_get_Tag_m9F24A98C3F04C66363235C1C339DF611D346378A (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	{
		String_t* L_0 = __this->___tag_4;
		V_0 = (bool)((!(((RuntimeObject*)(String_t*)L_0) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		String_t* L_2 = __this->___tag_4;
		V_1 = L_2;
		goto IL_0058;
	}

IL_0017:
	{
		il2cpp_codegen_runtime_class_init_inline(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		RuntimeObject* L_3 = ((ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_StaticFields*)il2cpp_codegen_static_fields_for(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var))->___lockObject_0;
		V_2 = L_3;
		V_3 = (bool)0;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0049:
			{// begin finally (depth: 1)
				{
					bool L_4 = V_3;
					if (!L_4)
					{
						goto IL_0053;
					}
				}
				{
					RuntimeObject* L_5 = V_2;
					Monitor_Exit_m25A154960F91391E10E4CDA245ECDF4BA94D56A9(L_5, NULL);
				}

IL_0053:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				RuntimeObject* L_6 = V_2;
				Monitor_Enter_m00506757392936AA62DBE2C5FFBEE69EE920C4D4(L_6, (&V_3), NULL);
				ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_7 = __this->___parent_2;
				V_4 = (bool)((!(((RuntimeObject*)(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97*)L_7) <= ((RuntimeObject*)(RuntimeObject*)NULL)))? 1 : 0);
				bool L_8 = V_4;
				if (!L_8)
				{
					goto IL_0046_1;
				}
			}
			{
				ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_9 = __this->___parent_2;
				NullCheck(L_9);
				String_t* L_10;
				L_10 = ModuleLogger_get_Tag_m9F24A98C3F04C66363235C1C339DF611D346378A(L_9, NULL);
				V_1 = L_10;
				goto IL_0058;
			}

IL_0046_1:
			{
				goto IL_0054;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0054:
	{
		V_1 = (String_t*)NULL;
		goto IL_0058;
	}

IL_0058:
	{
		String_t* L_11 = V_1;
		return L_11;
	}
}
// System.Void Firebase.Storage.Internal.ModuleLogger::set_Tag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModuleLogger_set_Tag_m93B4431C1F0B91205E7CC9B7EDACACA49C97E42F (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* __this, String_t* ___value0, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = ___value0;
		__this->___tag_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___tag_4), (void*)L_0);
		return;
	}
}
// System.Void Firebase.Storage.Internal.ModuleLogger::LogMessage(Firebase.LogLevel,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModuleLogger_LogMessage_m568B4169C0A0FA2CA4FF4E6E076870F07131DA3B (ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* __this, int32_t ___level0, String_t* ___message1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LogUtil_t004F911611FD3AE3085F5CA8159A798C3CA16D39_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1FB9018D8BFC0FACF068B1067EF9E96C35FED1FE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	String_t* G_B2_0 = NULL;
	int32_t G_B2_1 = 0;
	String_t* G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	int32_t G_B4_2 = 0;
	{
		int32_t L_0 = ___level0;
		int32_t L_1;
		L_1 = ModuleLogger_get_Level_m0AEE610FD8DA98169E801F769BC567589DE54791(__this, NULL);
		V_0 = (bool)((((int32_t)((((int32_t)L_0) < ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_3 = ___level0;
		String_t* L_4;
		L_4 = ModuleLogger_get_Tag_m9F24A98C3F04C66363235C1C339DF611D346378A(__this, NULL);
		G_B2_0 = _stringLiteral1FB9018D8BFC0FACF068B1067EF9E96C35FED1FE;
		G_B2_1 = L_3;
		if (L_4)
		{
			G_B3_0 = _stringLiteral1FB9018D8BFC0FACF068B1067EF9E96C35FED1FE;
			G_B3_1 = L_3;
			goto IL_0027;
		}
	}
	{
		G_B4_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0037;
	}

IL_0027:
	{
		String_t* L_5;
		L_5 = ModuleLogger_get_Tag_m9F24A98C3F04C66363235C1C339DF611D346378A(__this, NULL);
		String_t* L_6;
		L_6 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_5, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745, NULL);
		G_B4_0 = L_6;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0037:
	{
		String_t* L_7 = ___message1;
		String_t* L_8;
		L_8 = String_Format_m9499958F4B0BB6089C75760AB647AB3CA4D55806(G_B4_1, G_B4_0, L_7, NULL);
		il2cpp_codegen_runtime_class_init_inline(LogUtil_t004F911611FD3AE3085F5CA8159A798C3CA16D39_il2cpp_TypeInfo_var);
		LogUtil_LogMessage_m59195C58FF0FC63681CED394EB6618F03A25B3B4(G_B4_2, L_8, NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void Firebase.Storage.Internal.ModuleLogger::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModuleLogger__cctor_m0EA7B8F386D2ACF2E361D39CF99FB41F227F3BA9 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m63EDE1AEAA4A4FCAB6203FB0C262E31C79D24F9E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RuntimeObject_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(L_0, NULL);
		((ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_StaticFields*)il2cpp_codegen_static_fields_for(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var))->___lockObject_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_StaticFields*)il2cpp_codegen_static_fields_for(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var))->___lockObject_0), (void*)L_0);
		List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9* L_1 = (List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9*)il2cpp_codegen_object_new(List_1_t99645769CE679BC507C0D8F0C572B4E324C834D9_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		List_1__ctor_m63EDE1AEAA4A4FCAB6203FB0C262E31C79D24F9E(L_1, List_1__ctor_m63EDE1AEAA4A4FCAB6203FB0C262E31C79D24F9E_RuntimeMethod_var);
		((ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_StaticFields*)il2cpp_codegen_static_fields_for(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var))->___roots_1 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&((ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_StaticFields*)il2cpp_codegen_static_fields_for(ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97_il2cpp_TypeInfo_var))->___roots_1), (void*)L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void FirebaseStorage_set_Logger_m19C7C11321677C2320D1D066CCBBB6F37B3E00F3_inline (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___value0, const RuntimeMethod* method) 
{
	{
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_0 = ___value0;
		__this->___U3CLoggerU3Ek__BackingField_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CLoggerU3Ek__BackingField_5), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* FirebaseStorage_get_Logger_m47F60ACF93F3A0BA8C660B1B566F61BBF30BE391_inline (FirebaseStorage_t7EA8F2DF39C94049CBEAACE46B64E4034C3E90C1* __this, const RuntimeMethod* method) 
{
	{
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_0 = __this->___U3CLoggerU3Ek__BackingField_5;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR intptr_t HandleRef_get_Handle_m2055005E349E895499E1B3B826C89228FFAC4C17_inline (HandleRef_t4B05E32B68797F702257D4E838B85A976313F08F* __this, const RuntimeMethod* method) 
{
	{
		intptr_t L_0 = __this->____handle_1;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* AppOptions_get_StorageBucket_m93625BCEE94287655E69A3ED0D145E64F66278A4_inline (AppOptions_tC85C010A614E35ED5C64709D909D4525D9DE6D09* __this, const RuntimeMethod* method) 
{
	{
		String_t* L_0 = __this->___U3CStorageBucketU3Ek__BackingField_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StorageReference_set_Internal_m402EC4C5726D40B65F6AC62A605948E4FCBF72A2_inline (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* ___value0, const RuntimeMethod* method) 
{
	{
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_0 = ___value0;
		__this->___U3CInternalU3Ek__BackingField_2 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CInternalU3Ek__BackingField_2), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StorageReference_set_Logger_mFA430AB56C163BC0EC0F4F69201D440BBF590789_inline (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* ___value0, const RuntimeMethod* method) 
{
	{
		ModuleLogger_tC5B741625D75BCB0E54696B8C07E113E68C93D97* L_0 = ___value0;
		__this->___U3CLoggerU3Ek__BackingField_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CLoggerU3Ek__BackingField_1), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* StorageReference_get_Internal_mDD8DFC623E89E8F540685EC02832A1B31DD4B7AF_inline (StorageReference_t33F43540227458BFCCC2980F614D70CD663C9019* __this, const RuntimeMethod* method) 
{
	{
		StorageReferenceInternal_t400CD0CF0BE9054DBAF3387B5D281C915B1234A4* L_0 = __this->___U3CInternalU3Ek__BackingField_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) 
{
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_6 = V_0;
		int32_t L_7 = V_1;
		RuntimeObject* L_8 = ___item0;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (RuntimeObject*)L_8);
		return;
	}

IL_0034:
	{
		RuntimeObject* L_9 = ___item0;
		((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, RuntimeObject*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->____current_3;
		return L_0;
	}
}

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.AI.NavMeshPath::.ctor()
extern void NavMeshPath__ctor_mEA40BFC2492814FFC97A71C3AEC2154A9415C37F (void);
// 0x00000002 System.Void UnityEngine.AI.NavMeshPath::Finalize()
extern void NavMeshPath_Finalize_mB151BFBD5D7E65C343415B6B332A58504F12AF77 (void);
// 0x00000003 System.IntPtr UnityEngine.AI.NavMeshPath::InitializeNavMeshPath()
extern void NavMeshPath_InitializeNavMeshPath_m91B9A02C11B0C86F33F726A047B11D015230E9C2 (void);
// 0x00000004 System.Void UnityEngine.AI.NavMeshPath::DestroyNavMeshPath(System.IntPtr)
extern void NavMeshPath_DestroyNavMeshPath_mE7FD23F7D0456507277BDC8ED868A7C6888796EB (void);
// 0x00000005 UnityEngine.Vector3[] UnityEngine.AI.NavMeshPath::CalculateCornersInternal()
extern void NavMeshPath_CalculateCornersInternal_m59C910C60F8C28D4E387C93B299B7D3F76213850 (void);
// 0x00000006 System.Void UnityEngine.AI.NavMeshPath::CalculateCorners()
extern void NavMeshPath_CalculateCorners_mFC7D66E9304DE81D6030B3FEB237754DFFFDF548 (void);
// 0x00000007 UnityEngine.Vector3[] UnityEngine.AI.NavMeshPath::get_corners()
extern void NavMeshPath_get_corners_m469237DE430A001BE7050BEF5D05A20B94D76F3E (void);
// 0x00000008 System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination(UnityEngine.Vector3)
extern void NavMeshAgent_SetDestination_mD5D960933827F1F14B29CF4A3B6F305C064EBF46 (void);
// 0x00000009 System.Boolean UnityEngine.AI.NavMeshAgent::get_pathPending()
extern void NavMeshAgent_get_pathPending_mA806A4DC1E06CA32A4E7E71B6846B516EC8C2487 (void);
// 0x0000000A UnityEngine.AI.NavMeshPath UnityEngine.AI.NavMeshAgent::get_path()
extern void NavMeshAgent_get_path_m2E4BD1AC2DA5207EC5DF9E36F3147397AF039C45 (void);
// 0x0000000B System.Void UnityEngine.AI.NavMeshAgent::CopyPathTo(UnityEngine.AI.NavMeshPath)
extern void NavMeshAgent_CopyPathTo_m7C3236E78650923CCC3EA239F40755610A997F6C (void);
// 0x0000000C System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_SetDestination_Injected_mC3EF405F5AAFF9F98C5D5AECAD641525CDF742EA (void);
// 0x0000000D System.Void UnityEngine.AI.NavMesh::Internal_CallOnNavMeshPreUpdate()
extern void NavMesh_Internal_CallOnNavMeshPreUpdate_m80148CFDD0C6F1DDDE5B3DA67A8D9613043A4233 (void);
// 0x0000000E System.Void UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::.ctor(System.Object,System.IntPtr)
extern void OnNavMeshPreUpdate__ctor_m7142A3AA991BE50B637A16D946AB7604C64EF9BA (void);
// 0x0000000F System.Void UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::Invoke()
extern void OnNavMeshPreUpdate_Invoke_mFB224B9BBF9C78B7F39AA91A047F175C69897914 (void);
static Il2CppMethodPointer s_methodPointers[15] = 
{
	NavMeshPath__ctor_mEA40BFC2492814FFC97A71C3AEC2154A9415C37F,
	NavMeshPath_Finalize_mB151BFBD5D7E65C343415B6B332A58504F12AF77,
	NavMeshPath_InitializeNavMeshPath_m91B9A02C11B0C86F33F726A047B11D015230E9C2,
	NavMeshPath_DestroyNavMeshPath_mE7FD23F7D0456507277BDC8ED868A7C6888796EB,
	NavMeshPath_CalculateCornersInternal_m59C910C60F8C28D4E387C93B299B7D3F76213850,
	NavMeshPath_CalculateCorners_mFC7D66E9304DE81D6030B3FEB237754DFFFDF548,
	NavMeshPath_get_corners_m469237DE430A001BE7050BEF5D05A20B94D76F3E,
	NavMeshAgent_SetDestination_mD5D960933827F1F14B29CF4A3B6F305C064EBF46,
	NavMeshAgent_get_pathPending_mA806A4DC1E06CA32A4E7E71B6846B516EC8C2487,
	NavMeshAgent_get_path_m2E4BD1AC2DA5207EC5DF9E36F3147397AF039C45,
	NavMeshAgent_CopyPathTo_m7C3236E78650923CCC3EA239F40755610A997F6C,
	NavMeshAgent_SetDestination_Injected_mC3EF405F5AAFF9F98C5D5AECAD641525CDF742EA,
	NavMesh_Internal_CallOnNavMeshPreUpdate_m80148CFDD0C6F1DDDE5B3DA67A8D9613043A4233,
	OnNavMeshPreUpdate__ctor_m7142A3AA991BE50B637A16D946AB7604C64EF9BA,
	OnNavMeshPreUpdate_Invoke_mFB224B9BBF9C78B7F39AA91A047F175C69897914,
};
static const int32_t s_InvokerIndices[15] = 
{
	7252,
	7252,
	10842,
	10681,
	7114,
	7252,
	7114,
	4256,
	7012,
	7114,
	5754,
	4006,
	10889,
	3202,
	7252,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_AIModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AIModule_CodeGenModule = 
{
	"UnityEngine.AIModule.dll",
	15,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
